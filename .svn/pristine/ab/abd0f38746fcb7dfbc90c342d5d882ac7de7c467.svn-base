﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ITELCA_CLASSLIBRARY.Models;
using ITELCA_CONTROLESTUDIOS.Models.ViewModels;
using ITELCA_CLASSLIBRARY.Services;
using Utilidades;
using ITELCA_CLASSLIBRARY.CustomClasses;

namespace ITELCA_CONTROLESTUDIOS.Controllers
{
     [Authorize(Roles = "ADMINISTRADOR,COORDINADOR,EMPLEADO")]
    public class ReciboCajaController : Controller
    {
        //
        // GET: /Rol/
        ReciboCajaViewModel vistaModeloReciboCaja;
        SelectList _activo;
        SelectList _tipoPago;
        SelectList _tipoDocumento;
        SelectList _conceptosCursos;
        SelectList _conceptosTalleres;
        ServicioReciboCaja _servicio = new ServicioReciboCaja();

        public void IniciarVista(RECIBOS recibo) {
            ListasGenerales listaGenerales = new ListasGenerales();
            Dictionary<int, string> lista = listaGenerales.ListaActivo();
            Dictionary<string, string> tipo = listaGenerales.ListaCondicionPago();
            _tipoPago = new SelectList(tipo, "Key", "Value");
            Dictionary<string, string> tipoDocumento = listaGenerales.ListaTipoDocumento();
            _tipoDocumento = new SelectList(tipoDocumento, "Key", "Value");
            bool cajaAbierta = false;
            CAJAS caja = null;
            MANEJOCAJAS manejoCaja = new ServicioManejoCaja().ObtenerManejoCajaAbierta(User.Identity.Name);
            if (manejoCaja != null)
            {
                cajaAbierta = true;
                caja = manejoCaja.CAJAS;
            }
            
            if (recibo != null)
            {
                _activo = new SelectList(lista, "Key", "Value", recibo.ESTADO);
                _conceptosCursos = new SelectList(new ServicioProducto().ObtenerTodos(), "INV_ITEM_ID", "DESCR");
                _conceptosTalleres = new SelectList(new ServicioConcepto().ObtenerTodos(), "Key", "Value");
            }
            else
            {
                _activo = new SelectList(lista, "Key", "Value");
                _conceptosCursos = new SelectList(new ServicioProducto().ObtenerTodos(), "INV_ITEM_ID", "DESCR");
                _conceptosTalleres = new SelectList(new ServicioConcepto().ObtenerTodos(), "Key", "Value");
            
            }
            vistaModeloReciboCaja = new ReciboCajaViewModel(recibo, _tipoPago,_tipoDocumento, _conceptosCursos, _conceptosTalleres, cajaAbierta, caja);
        }

        public ActionResult Index()
        {
            IniciarVista(null);
            return View(vistaModeloReciboCaja);
        }

        [HttpPost]
        public JsonResult ObtenerConceptoCurso(string id_curso)
        {

            return Json(new ServicioConceptoCurso().ListaConceptoCursosCursos(id_curso));
        }

        [HttpGet]
        public string ObtenerMontoLineaDetale(string id_conceptoCurso, string cantidad)
        {
            decimal monto = new ServicioPrecioProducto().ObtenerPrecioProducto(id_conceptoCurso) * decimal.Parse(cantidad);
            return (monto.ToString());
        }

        public ActionResult EstadoCuenta(int id)
        {
            return View(new ServicioConceptoEstudiante().ObtenerPorUsuario(decimal.Parse(id.ToString())));
        }

         [HttpPost]
        public JsonResult ObtenerEstadoCuenta(int alumno_id)
        {
            List<CONCEPTOS_ESTUDIANTES> lista = new ServicioConceptoEstudiante().ObtenerPorUsuario(decimal.Parse(alumno_id.ToString()));
            List<LISTADETALLERECIBO> edoCuenta = new List<LISTADETALLERECIBO>();
             foreach (var concepto in lista)
             {
                 LISTADETALLERECIBO ld = new LISTADETALLERECIBO();
                 ld.cantidad = "1";
                 ld.desc_concepto = concepto.DESCRIPCION;
                 ld.id_concepto = concepto.CONCEPTO_ESTUDIANTE_ID.ToString();
                 ld.monto = concepto.MONTO.ToString();
                 ld.tipo = (concepto.MATRICULA_CURSO_ID.HasValue) ? "C" : ((concepto.MATRICULATALLER_ID.HasValue) ? "T" : "S");
                 ld.vencido = (concepto.FECHA_VENCIMIENTO <= DateTime.Now) ? "S" : "N";
                 edoCuenta.Add(ld);
             }
            return Json(edoCuenta);
        }


        [HttpPost]
        public JsonResult ObtenerRecibo(string nro_recibo)
        {
            object resultado = new ServicioReciboCaja().ObtenerPorNroRecibo(Decimal.Parse(nro_recibo));
            return Json(resultado);

        }

        public JsonResult ObtenerDisponibilidad(string inv_item_id)
        {
            USUARIOS usuario = new ServicioUsuario().ObtenerPorLogin(User.Identity.Name);
            MANEJOCAJAS mc = new ServicioManejoCaja().ObtenerManejoCajaAbierta(User.Identity.Name);

            INVENTARIO inv = new InterfazPeople().ObtenerInventarioProducto(inv_item_id, mc.CAJAS.SEDES.CODIGO);
            string disp = (inv.QTY_AVAILABLE != null) ? inv.QTY_AVAILABLE : "0";
            return Json(disp);
        }

        [HttpPost]
        public JsonResult Emitir([Bind(Prefix = "RECIBOS")] RECIBOS recibo, FormCollection form)
        {
            USUARIOS usuario = new ServicioUsuario().ObtenerPorLogin(User.Identity.Name);
            MANEJOCAJAS caja = new ServicioManejoCaja().ObtenerManejoCajaAbierta(User.Identity.Name);
            recibo.FECHADOCUMENTO = DateTime.Now;
            recibo.ESTADO = 0;
            recibo.CONDICION = "0";
            recibo.CAJERO_ID = usuario.USUARIO_ID;
            recibo.CAJA_ID = caja.CAJA_ID;
            //defino cual es el tipo de documento no se que sera 0 y 1 para ti 
            string tipoDoc = form["tipoReport"];
            string numero = form["correlativo"];
            if (numero != "0")
            {
                recibo.NRORECIBO = decimal.Parse(numero);
                ServicioReciboCaja servicioReciboCaja = new ServicioReciboCaja();
                new ServicioCorrelativo().IncrementarCorrelativo(tipoDoc, caja.CAJAS.SEDE_ID);
                return Json(servicioReciboCaja.Guardar(recibo, form));
            }
            else
            {
                List<object> resp = new List<object>();
                resp.Add(new { id = "-1", control = "0", msg = "No existen Correlativos Configurados para El recibo" });
                return Json(resp);
            }
        }


        /*[HttpPost]
        public string Editar([Bind(Prefix = "RECIBOS")] RECIBOS recibo, FormCollection form)
        {
            ServicioReciboCaja servicioReciboCaja = new ServicioReciboCaja();
            return servicioReciboCaja.Modificar(recibo, form).ToString();
        }*/

        [HttpPost]
        public JsonResult ObtenerDetallesRecibo(string id_recibo)
        {
            return Json(new ServicioReciboCaja().ObtenerDetallesPorRecibo(Decimal.Parse(id_recibo)));
        }

        [HttpPost]
        public JsonResult ObtenerCorrelativo()
        {
            List<object> resp = new List<object>();
            try
            {
                ServicioCorrelativo servicio=new ServicioCorrelativo();
                MANEJOCAJAS caja = new ServicioManejoCaja().ObtenerManejoCajaAbierta(User.Identity.Name);
                string pm = servicio.ObtenerCorrelativo("PM", caja.CAJAS.SEDE_ID);
                string pp = servicio.ObtenerCorrelativo("PP", caja.CAJAS.SEDE_ID);

               
                resp.Add(new {ok=1, pm = pm, pp = pp, msg = "" });
                return Json(resp);
            }
            catch {
                resp.Clear();
                resp.Add(new {ok=0, pm = "0", pp = "0", msg = "Error Consultando Correlativos" });
                return Json(resp);
                
            }
        }
    }
}
