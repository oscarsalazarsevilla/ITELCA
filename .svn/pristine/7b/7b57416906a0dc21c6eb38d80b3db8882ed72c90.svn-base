﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ITELCA_CLASSLIBRARY.Models;
using ITELCA_CONTROLESTUDIOS.Models.ViewModels;
using ITELCA_CLASSLIBRARY.Services;

namespace ITELCA_CONTROLESTUDIOS.Controllers
{
     [Authorize(Roles = "ADMINISTRADOR,COORDINADOR")]
    public class ConceptoTallerController : Controller
    {
        //
        // GET: /Rol/
        ConceptoTallerViewModel vistaModelo;
        ServicioConceptoTaller servicioConceptoTaller = new ServicioConceptoTaller();
        SelectList activo;
        SelectList ListaTaller;
        SelectList ListaConceptos;
        SelectList ListaTurnos;
        
        public void IniciarVista(CONCEPTOS_TALLERES conceptoTaller) {

            Dictionary<int, string> lista = new Dictionary<int, string>();
            lista.Add(1, "SI");
            lista.Add(0, "NO");

 
            if (conceptoTaller != null)
            {
                activo = new SelectList(lista, "Key", "Value", conceptoTaller.ESTADO);
                ListaTaller = new SelectList(new ServicioTaller().ObtenerTodos(), "TALLER_ID", "NOMBRE", conceptoTaller.TALLER_ID);
                ListaConceptos = new SelectList(new ServicioConcepto().ObtenerTodos(), "CONCEPTO_ID", "NOMBRE", conceptoTaller.CONCEPTO_ID);
            }
            else
            {
                activo = new SelectList(lista, "Key", "Value");
                ListaTaller = new SelectList(new ServicioTaller().ObtenerTodos(), "TALLER_ID", "NOMBRE");
                ListaConceptos = new SelectList(new ServicioConcepto().ObtenerTodos(), "CONCEPTO_ID", "NOMBRE");
            }
            vistaModelo = new ConceptoTallerViewModel(conceptoTaller, activo, ListaTaller,ListaConceptos);
        }

        public ActionResult Crear()
        {
            IniciarVista(null);
            return View(vistaModelo);
        }
     
        public ActionResult Editar(int id) {
            CONCEPTOS_TALLERES conceptoTaller = servicioConceptoTaller.ObtenerPorClave(id);
            if (conceptoTaller != null)
            {
                IniciarVista(conceptoTaller);
                return View(vistaModelo);
            }
            else
            {
                TempData["mensaje"] = "Concepto Taller no Encontrado.";
                return RedirectToAction("Listar");
            }
        }

        public ActionResult Listar() {
            return View();
        }

        [HttpPost]
        public ActionResult Crear([Bind(Prefix = "CONCEPTOS_TALLERES")] CONCEPTOS_TALLERES conceptoTaller)
        {
            conceptoTaller.USUARIO_ID = new ServicioUsuario().ObtenerPorLogin(User.Identity.Name).USUARIO_ID;
            if (ModelState.IsValid) {
                if (!servicioConceptoTaller.Duplicado(conceptoTaller))
                {
                    if (servicioConceptoTaller.Guardar(conceptoTaller))
                    {
                        TempData["mensaje"] = "Concepto Taller Guardado Satisfactoriamente.";
                        return RedirectToAction("Listar");
                    }
                    else
                        ModelState.AddModelError(string.Empty, "Error Guardando Concepto Taller");
                }
                else 
                    ModelState.AddModelError(string.Empty, "Concepto Taller Duplicado.");
                
            }
            IniciarVista(conceptoTaller);
            return View(vistaModelo);

        }

        [HttpPost]
        public ActionResult Editar([Bind(Prefix = "CONCEPTOS_TALLERES")] CONCEPTOS_TALLERES conceptoTaller)
        {
            if (ModelState.IsValid)
            {
                if (!servicioConceptoTaller.Duplicado(conceptoTaller))
                {
    
                    conceptoTaller.USUARIO_ID = new ServicioUsuario().ObtenerPorLogin(User.Identity.Name).USUARIO_ID;
                    if (servicioConceptoTaller.Modificar(conceptoTaller))
                    {
                        TempData["mensaje"] = "ConceptoTaller Modificado Satisfactoriamente";
                        return RedirectToAction("Listar");
                    }
                    else
                        ModelState.AddModelError(string.Empty, "Error Modificando ConceptoTaller");
                }
                else
                    ModelState.AddModelError(string.Empty, "ConceptoTaller Duplicado.");

            }
            IniciarVista(conceptoTaller);
            return View(vistaModelo);

        }

       
        [HttpPost]
        public JsonResult Eliminar(int id)
        {
            return Json(servicioConceptoTaller.Eliminar(id));
        }

        [HttpPost]
        public JsonResult ObtenerData(string sidx, string sord, int page, int rows, string filters)
        {
            object resultado = servicioConceptoTaller.ObtenerDataGrid(sidx, sord, page, rows, filters);
            return Json(resultado);

        }

    }
}
