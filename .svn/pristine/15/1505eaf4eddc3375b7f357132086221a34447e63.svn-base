﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ITELCA_CLASSLIBRARY.Models;
using ITELCA_CONTROLESTUDIOS.Models.ViewModels;
using ITELCA_CLASSLIBRARY.Services;
using ITELCA_CLASSLIBRARY.CustomClasses;

namespace ITELCA_CONTROLESTUDIOS.Controllers
{
     [Authorize(Roles = "ADMINISTRADOR,COORDINADOR")]
    public class OfertaCursoController : Controller
    {
        //
        // GET: /Rol/
        OfertaCursoViewModel vistaModelo;
        OfertasModulosViewModel viewModelModulo;

        ServicioOfertaCurso servicioOfertaCurso = new ServicioOfertaCurso();
        SelectList aulas;
        SelectList turnos;
        SelectList modulos;
        SelectList sedes;
        SelectList cursos;
        SelectList cursosGrid;
        public void IniciarVista(OFERTASCURSOS ofertaCurso,decimal idOfertaCurso) {
            modulos = new SelectList(new ServicioModulo().ObtenerDiccionarioModulo(), "Key", "Value");
            aulas = new SelectList(new ServicioAula().ObtenerDiccionarioAulas(), "Key", "Value");
            if (ofertaCurso != null)
            {
                turnos = new SelectList(new ServicioTurno().ObtenerDiccionarioTurno(), "Key", "Value", ofertaCurso.TURNO_ID);
                sedes = new SelectList(new ServicioSede().ObtenerTodos(), "SEDE_ID", "NOMBRE",ofertaCurso.SEDE_ID);
                cursos = new SelectList(new ServicioCursos().ObtenerTodos(), "CURSO_ID", "NOMBRE",ofertaCurso.CURSO_ID);
               
               
            }else{
               
                turnos = new SelectList(new ServicioTurno().ObtenerDiccionarioTurno(), "Key", "Value");
                sedes = new SelectList(new ServicioSede().ObtenerTodos(), "SEDE_ID", "NOMBRE");
                cursos = new SelectList(new ServicioCursos().ObtenerTodos(), "CURSO_ID", "NOMBRE");
                cursosGrid = new SelectList(new ServicioCursos().ObtenerTodos(), "CURSO_ID", "NOMBRE"); 
            }
            if(idOfertaCurso!=0)
                cursosGrid = new SelectList(new ServicioCursos().ObtenerTodos(), "CURSO_ID", "NOMBRE", idOfertaCurso); 
            else
                cursosGrid = new SelectList(new ServicioCursos().ObtenerTodos(), "CURSO_ID", "NOMBRE"); 
            vistaModelo = new OfertaCursoViewModel(ofertaCurso,turnos,  sedes, cursos, cursosGrid,viewModelModulo);
        }

        public void IniciarViewModelOfertasModulos(string id_oferta_curso)
        {
            OFERTASCURSOS ofertaCurso = new ServicioOfertaCurso().ObtenerPorClave(long.Parse(id_oferta_curso));
            SelectList aulas = new SelectList(new ServicioAula().ObtenerDiccionarioAulasPorSede(ofertaCurso.SEDE_ID.Value), "Key", "Value");
            SelectList modulos = new SelectList(new ServicioModulo().ObtenerDiccionarioModuloPorCurso(ofertaCurso.CURSO_ID.Value), "Key", "Value");
            SelectList cursosGrid = new SelectList(new ServicioCursos().ObtenerTodos(), "CURSO_ID", "NOMBRE");
            SelectList profesores = new SelectList(new ServicioUsuario().ObtenerListaProfesores(), "Key","Value" );
            viewModelModulo = new OfertasModulosViewModel(null,  aulas,modulos, cursosGrid, ofertaCurso,profesores);

        }

        public ActionResult Crear()
        {
            IniciarVista(null,0);
            return View(vistaModelo);
        }
     
        public ActionResult Editar(int id) {
            OFERTASCURSOS ofertaCurso = servicioOfertaCurso.ObtenerPorClave(id);
            if (ofertaCurso != null)
            {
                IniciarVista(ofertaCurso,0);
                return View(vistaModelo);
            }
            else
            {
                TempData["mensaje"] = "Oferta Curso no Encontrado.";
                return RedirectToAction("Listar");
            }
        }

        public ActionResult Listar() {
            return View();
        }

        [HttpPost]
        public ActionResult Crear([Bind(Prefix="OFERTASCURSOS")] OFERTASCURSOS ofertaCurso)
        {
            if (ModelState.IsValid) {
                USUARIOS usuario = new ServicioUsuario().ObtenerPorLogin(User.Identity.Name);
                ofertaCurso.USUARIO_ID = usuario.USUARIO_ID;
                if (!servicioOfertaCurso.ValidarOferta(ofertaCurso))
                {
                    if (servicioOfertaCurso.Guardar(ofertaCurso))
                    {
                        TempData["mensaje"] = "Oferta Guardado Satisfactoriamente.";
                        IniciarVista(null,ofertaCurso.CURSO_ID.Value);
                        return View(vistaModelo);

                    }
                    else
                        ModelState.AddModelError(string.Empty, "Error Guardando Oferta");
                }
                else {
                    ModelState.AddModelError(string.Empty, "Ya existe esa Oferta ");
                }
                          
            }

            IniciarVista(ofertaCurso,ofertaCurso.CURSO_ID.HasValue?ofertaCurso.CURSO_ID.Value:0);
            return View(vistaModelo);

        }

      

        [HttpGet]
        public ActionResult Modulo(string id_oferta_curso)
        {
            IniciarViewModelOfertasModulos(id_oferta_curso);
            return View(viewModelModulo);
        }
       
        [HttpGet]
        public ActionResult ObtenerSelect(){

            return PartialView("Select");
        }

        [HttpGet]
        public JsonResult ObtenerSede()
        {

            return Json( new ServicioSede().ObtenerObjeto(),JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult ObtenerTurno()
        {

            return Json(new ServicioTurno().ObtenerObjeto(), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult ObtenerAula(string sede)
        {

            return Json(new ServicioAula().ObtenerObjetoPorSede(decimal.Parse(sede)), JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult ObtenerCurso()
        {

            return Json(new ServicioCostosCursos().ObtenerObjeto(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Eliminar(string id)
        {
            return Json(servicioOfertaCurso.Eliminar(decimal.Parse(id)));
        }

        [HttpPost]
        public JsonResult Modulo([Bind(Prefix = "OFERTASMODULOS")] OFERTASMODULOS ofertaModulo)
        {
            List<object> lista = new List<object>();
            try
            {
                OFERTASCURSOS oferta = new ServicioOfertaCurso().ObtenerPorClave(Int32.Parse(ofertaModulo.OFERTA_CURSO_ID.ToString()));
                USUARIOS usuario = new ServicioUsuario().ObtenerPorLogin(User.Identity.Name);
                ofertaModulo.USUARIO_ID = usuario.USUARIO_ID;
                var resp=new ServicioOfertasModulos().ValidarModulo(ofertaModulo);
                if (resp=="")
                {
                    if (new ServicioOfertasModulos().Guardar(ofertaModulo))
                    {

                        lista.Add(new
                        {
                            success = true,
                            mensaje = "Oferta Modulo creada con exito."
                        });
                        return Json(lista, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {

                        lista.Add(new
                        {
                            success = false,
                            mensaje = "Ocurrio un error al almacenar los datos."
                        });
                    }
                }
                else
                {

                    lista.Add(new
                    {
                        success = false,
                        mensaje = "Modulo en conflicto con: "+resp+ "Verifique las Fechas"
                    });

                }
                return Json(lista, JsonRequestBehavior.AllowGet);
            }
            catch {
                lista.Add(new
                {
                    success = false,
                    mensaje = "Modulo No Guardado."
                });
                return Json(lista, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult ObtenerData(string id, string sidx, string sord, int page, int rows, string filters)
        {
            try
            {
                object resultado = servicioOfertaCurso.ObtenerDataGrid(decimal.Parse(id), sidx, sord, page, rows, filters);
                return Json(resultado);
            }
            catch {
                return Json("");
            }

        }

        [HttpPost]
        public JsonResult ObtenerDataIndex(string id, string sidx, string sord, int page, int rows, string filters)
        {
            object resultado = servicioOfertaCurso.ObtenerDataGrid(sidx, sord, page, rows, filters);
            return Json(resultado);

        }
        [HttpPost]
        public string ObtenerModulo(string curso)
        {

            return new ServicioModulo().ObtenerSelectPorCurso(decimal.Parse(curso));
        }

        [HttpPost]
        public string ObtenerAulaPorSede(string sede)
        {

            return new ServicioAula().ObtenerAulaPorSede(decimal.Parse(sede));
        }

        [HttpPost]
        public JsonResult ObtenerListaSede(string sidx, string sord, int page, int rows, string filters)
        {
            object resultado = new ServicioModulo().ObtenerDataGrid(sidx, sord, page, rows, filters);
            return Json(resultado);

        }
        [HttpPost]
        public JsonResult GuardarFila(FormCollection form)
        {

            servicioOfertaCurso.Modificar(generarObjeto(form));
            //object resultado = new ServicioModulo().ObtenerDataGrid(si);
            return Json("");

        }
        [HttpPost]
        public JsonResult CalcularFecha(FormCollection form)
        {
            //fechaInicio.
            if (form["fechaInicio"] != null && form["idModulo"] != null)
            {
                DateTime fechaIni = DateTime.Parse(form["fechaInicio"].ToString());
                long idModulo = long.Parse(form["idModulo"].ToString());
                long idturno = long.Parse(form["idTurno"].ToString());
                MODULOS modulo = new ServicioModulo().ObtenerPorClave(idModulo);
                TURNOS turno = new ServicioTurno().ObtenerPorClave(idturno);
                MODULOS_TURNOS mt = new ServicioModuloTurno().ObtenerPorClave(modulo.MODULO_ID, turno.TURNO_ID);
                decimal cantidadClases = (mt != null) ? mt.NRO_CLASES : 0;
                decimal claseEncontrada=0M;
                int cont = 0;
                bool enc = false;
                int diaSemana=0;
                int diaAux=0;
                List<DIAClASE> list = new List<DIAClASE>();
                foreach (var ope in turno.DIAS_TURNOS)
                    list.Add(new DIAClASE { dia = ope.DIAS.NOMBRE, periodo = ope.PERIODICIDAD ,habilitado=1});

                //fechaIni.ToString("dddd");//DIA DE MI PRIMERA CLASE
                DateTime fechaAux=fechaIni;
                ServicioFeriado feriado = new ServicioFeriado();
                while (cantidadClases > claseEncontrada) {            
                    string dia =EliminaAcentos (fechaAux.ToString("dddd"));//DIA DE MI PRIMERA CLASE
                    enc = false;
                    if(feriado.VerificarFeriado(fechaAux))
                        foreach (var ope in list) {
                            if (dia.ToLower() == ope.dia.ToLower()){
                                if (ope.periodo > 7) { 
                                    double resp=fechaAux.Subtract(fechaIni).TotalDays;
                                    if (resp < 7)//primera clase quincenal
                                    {
                                        claseEncontrada++;
                                        diaSemana = (int)fechaAux.DayOfYear;
                                    }
                                    else {
                                        int diasTranscurridos=fechaAux.DayOfYear-diaSemana;
                                        if (diasTranscurridos == 14)
                                            diaAux=fechaAux.DayOfYear;
                                        if (diasTranscurridos >= 14 && fechaAux.DayOfYear - diaSemana < 21)
                                            claseEncontrada++;
                                        if (diasTranscurridos == 20)
                                            diaSemana = diaAux+1;
                                    }
                                }
                                else
                                    claseEncontrada++;
                                enc = true;
                            }
                            if (enc)
                                break;
                        }
                    if (cantidadClases != claseEncontrada)
                    {
                        fechaAux = fechaAux.AddDays(1);
                        cont++;
                    }
                }
                return Json(fechaAux.ToShortDateString());
            }
            return Json("");
        
        }
        public static string EliminaAcentos(string texto)
        {
            if (string.IsNullOrEmpty(texto))
                return texto;

            byte[] tempBytes = System.Text.Encoding.GetEncoding("ISO-8859-8").GetBytes(texto);
            return System.Text.Encoding.UTF8.GetString(tempBytes);
        }
        public OFERTASCURSOS generarObjeto(FormCollection form) {
            int id = int.Parse(form["OFERTA_CURSO_ID"].ToString());
            OFERTASCURSOS obj = servicioOfertaCurso.ObtenerPorClave(id);
            if (form["TURNO_ID"]!=null)
                obj.TURNO_ID = decimal.Parse(form["TURNO_ID"].ToString());
            if (form["CURSO_ID"] != null)
                obj.CURSO_ID = decimal.Parse(form["CURSO_ID"].ToString());            
            if (form["SEDE_ID"]!=null)
                obj.SEDE_ID = decimal.Parse(form["SEDE_ID"].ToString());
            return obj;
        }

        [HttpPost]
        public JsonResult ObtenerTurnosPorCurso(string id_curso)
        {

            return Json(new ServicioOfertaCurso().ObtenerTurnosPorCurso(Decimal.Parse(id_curso)));
        }

        [HttpPost]
        public string ObtenerFechaMaxProximoInicio(string ofertaCursoId) {

            return new ServicioOfertaCurso().ObtenerMaxProximoInicio(decimal.Parse(ofertaCursoId));
        }

          [HttpPost]
        public JsonResult ObtenerCursosActivos()
        {

            return Json(new ServicioOfertaCurso().ObtenerCursosActivos());
        }

    }
}
