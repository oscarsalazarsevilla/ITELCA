﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ITELCA_CLASSLIBRARY.Models;
using ITELCA_CONTROLESTUDIOS.Models.ViewModels;
using ITELCA_CLASSLIBRARY.Services;
using ITELCA_CLASSLIBRARY.CustomClasses;
using System.Collections.Specialized;

namespace ITELCA_CONTROLESTUDIOS.Controllers
{
     [Authorize(Roles = "ADMINISTRADOR,COORDINADOR")]
    public class OfertasModulosController : Controller
    {
        //
        // GET: /Rol/
        OfertasModulosViewModel vistaModelo;
        ServicioOfertasModulos servicioOfertasModulos = new ServicioOfertasModulos();
        SelectList activo;
        SelectList aulas;
        SelectList turnos;
        SelectList modulos;
        SelectList sedes;
        SelectList cursos;
        SelectList cursosGrid;
        public void IniciarVista(OFERTASMODULOS ofertaModulo) {

            Dictionary<int, string> lista = new Dictionary<int, string>();
            lista.Add(1, "SI");
            lista.Add(0, "NO");

            activo = new SelectList(lista, "Key", "Value");
            if (ofertaModulo != null){
                aulas = new SelectList(new ServicioAula().ObtenerObjetoPorSede(ofertaModulo.SEDE_ID), "Key", "Value", ofertaModulo.AULA_ID);
                turnos = new SelectList(new ServicioTurno().ObtenerDiccionarioTurno(), "Key", "Value", ofertaModulo.TURNO_ID);
                modulos = new SelectList(new ServicioModulo().ObtenerDiccionarioModuloPorCurso(ofertaModulo.CURSO_ID), "Key", "Value", ofertaModulo.MODULO_ID);
            }else{ 
                aulas = new SelectList(new ServicioAula().ObtenerDiccionarioAulas(), "Key", "Value");
                turnos = new SelectList(new ServicioTurno().ObtenerDiccionarioTurno(), "Key", "Value");
                modulos = new SelectList(new ServicioModulo().ObtenerDiccionarioModulo(), "Key", "Value");
                sedes = new SelectList(new ServicioSede().ObtenerTodos(), "SEDE_ID", "NOMBRE");
                cursos = new SelectList(new ServicioCursos().ObtenerTodos(), "CURSO_ID", "NOMBRE");
                cursosGrid = new SelectList(new ServicioCursos().ObtenerTodos(), "CURSO_ID", "NOMBRE"); 
            }
            vistaModelo = new OfertasModulosViewModel(ofertaModulo,  aulas, modulos, cursosGrid,null,null,null);
        }

        public ActionResult Crear()
        {
            IniciarVista(null);
            return View(vistaModelo);
        }
     
        public ActionResult Editar(int id) {
            OFERTASMODULOS ofertaModulo = servicioOfertasModulos.ObtenerPorClave(id);
            if (ofertaModulo != null)
            {
                IniciarVista(ofertaModulo);
                return View(vistaModelo);
            }
            else
            {
                TempData["mensaje"] = "Oferta de Modulo no Encontrado.";
                return RedirectToAction("Listar");
            }
        }

        public ActionResult Listar() {
            return View();
        }

        [HttpPost]
        public ActionResult Crear([Bind(Prefix="OFERTASMODULOS")] OFERTASMODULOS ofertaModulo)
        {
            if (ModelState.IsValid) {
                USUARIOS usuario = new ServicioUsuario().ObtenerPorLogin(User.Identity.Name);
                ofertaModulo.USUARIO_ID = usuario.USUARIO_ID;
                if (servicioOfertasModulos.Guardar(ofertaModulo))
                    {
                        TempData["mensaje"] = "Oferta Guardado Satisfactoriamente.";
                        return RedirectToAction("Listar");
                    }
                    else
                        ModelState.AddModelError(string.Empty, "Error Guardando Oferta");                
            }
            IniciarVista(ofertaModulo);
            return View(vistaModelo);

        }

        [HttpPost]
        public ActionResult Editar(OFERTASMODULOS OFERTASMODULOS)
        {
            if (ModelState.IsValid)
            {

                    USUARIOS usuario = new ServicioUsuario().ObtenerPorLogin(User.Identity.Name);
                    //ofertaModulo.USUARIO_ID = usuario.USUARIO_ID;
                    OFERTASMODULOS.USUARIO_ID = 1;
                    if (servicioOfertasModulos.Modificar(OFERTASMODULOS))
                    {
                        TempData["mensaje"] = "Oferta Modificado Satisfactoriamente";
                        return RedirectToAction("Listar");
                    }
                    else
                        ModelState.AddModelError(string.Empty, "Error Modificando Oferta");
                

            }
            IniciarVista(OFERTASMODULOS);
            return View(vistaModelo);

        }

       
        [HttpGet]
        public ActionResult ObtenerSelect(){

            return PartialView("Select");
        }

        [HttpGet]
        public JsonResult ObtenerSede()
        {

            return Json( new ServicioSede().ObtenerObjeto(),JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult ObtenerProfesor()
        {

            return Json(new ServicioUsuario().ObtenerObjeto(), JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public JsonResult ObtenerTurno()
        {

            return Json(new ServicioTurno().ObtenerObjeto(), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult ObtenerAula(string sede)
        {

            return Json(new ServicioAula().ObtenerObjetoPorSede(decimal.Parse(sede)), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult ObtenerModuloGrid(string curso)
        {

            return Json(new ServicioModulo().ObtenerObjetoModuloPorCurso(decimal.Parse(curso)), JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult Eliminar(string id)
        {
            return Json(servicioOfertasModulos.Eliminar(decimal.Parse(id)));
        }

        [HttpPost]
        public JsonResult ObtenerData(string id, string profesorId, string sidx, string sord, int page, int rows, string filters)
        {
            try
            {
                object resultado = servicioOfertasModulos.ObtenerDataGrid(decimal.Parse(id), profesorId,sidx, sord, page, rows, filters);
                return Json(resultado);
            }
            catch {
                return Json("");
            }
        }
       

        [HttpPost]
        public string ObtenerAulaPorSede(string sede)
        {

            return new ServicioAula().ObtenerAulaPorSede(decimal.Parse(sede));
        }

        [HttpPost]
        public JsonResult ObtenerListaSede(string sidx, string sord, int page, int rows, string filters)
        {
            object resultado = new ServicioModulo().ObtenerDataGrid(sidx, sord, page, rows, filters);
            return Json(resultado);

        }
    
        [HttpPost]
         public JsonResult GuardarOfertaAutomatica (string form){

             OFERTASMODULOS oferta = GenerarObjetoModulo(form);
             return Json(servicioOfertasModulos.GenerarOfertaAutomatica(oferta));
         
        }
        [HttpPost]
        public JsonResult GuardarFila(FormCollection form)
        {
            try
            {
                DateTime fechaInicial=DateTime.Now;
                decimal moduloInicialId=0;
                OFERTASMODULOS ofertaM = generarObjeto(form,ref fechaInicial,ref moduloInicialId);
                servicioOfertasModulos.ActualizarFechasProximosModulos(ofertaM.OFERTA_MODULO_ID,fechaInicial, ofertaM.FECHA_FIN.Value,ofertaM.PROFESOR_ID,moduloInicialId);
                
                //object resultado = new ServicioModulo().ObtenerDataGrid(si);
                return Json("");
            }
            catch {
                return Json("Ocurrio Un Error Modificando REGISTRO");
            }

        }

        [HttpPost]
        public string ObtenerFechaUltimoModulo(string idOferta,string idProfesor) {
            try
            {
                IEnumerable<OFERTASMODULOS> oferta= new ServicioOfertasModulos().ObtenerTodosModulosPorOfertaPorProfesor(decimal.Parse(idOferta), decimal.Parse(idProfesor));
                if (oferta.Count() > 0)
                    return oferta.Max(u => u.FECHA_FIN).Value.ToShortDateString();
                else
                    return "0";
            }
            catch {
                return "0";
            }
        }

        [HttpPost]
        public JsonResult CalcularFecha(FormCollection form)
        {
            try{
            //fechaInicio.
                if (form["fechaInicio"] != null && form["idModulo"] != null)
                {
                    DateTime fechaIni = DateTime.Parse(form["fechaInicio"].ToString());
                    long idModulo = long.Parse(form["idModulo"].ToString());
                    long idturno = long.Parse(form["idTurno"].ToString());


                    return Json(new ServicioOfertasModulos().CalcularFechaOferta(fechaIni, idModulo, idturno));
                }
                else
                {
                    return Json(new
                    {
                        msg = "Por Favor Ingrese Fecha Inicio Y modulo",
                        ok = 0
                    });
                }
            }catch{
                return Json(new
                {
                    msg = "No se pudo Calcular Fecha Fin... :(",
                    ok = 0
                });
            }
        }
        
        public OFERTASMODULOS generarObjeto(FormCollection form,ref DateTime fechaInicial,ref decimal moduloInicialId) {
            int id = int.Parse(form["OFERTA_MODULO_ID"].ToString());
            OFERTASMODULOS obj= servicioOfertasModulos.ObtenerPorClave(id);
            fechaInicial = obj.FECHA_INICIO;
            moduloInicialId = obj.MODULO_ID.HasValue?obj.MODULO_ID.Value:0;
            if (form["MATRICULA_MINIMA"]!=null)
                obj.MATRICULA_MINIMA = decimal.Parse(form["MATRICULA_MINIMA"].ToString());
            if (form["MODULO_ID"]!=null)
                obj.MODULO_ID = decimal.Parse(form["MODULO_ID"].ToString());
            if (form["PROXIMO_INICIO"]!=null)
                obj.PROXIMO_INICIO = DateTime.Parse(form["PROXIMO_INICIO"].ToString());
            if (form["FECHA_INICIO"]!=null)
                obj.FECHA_INICIO = DateTime.Parse(form["FECHA_INICIO"].ToString());
            if (form["TURNO_ID"]!=null)
                obj.TURNO_ID = decimal.Parse(form["TURNO_ID"].ToString());
            if (form["AULA_ID"]!=null)
                obj.AULA_ID = decimal.Parse(form["AULA_ID"].ToString());
            if (form["FECHA_FIN"]!=null)
                obj.FECHA_FIN = DateTime.Parse(form["FECHA_FIN"].ToString());
            if (form["SEDE_ID"]!=null)
                obj.SEDE_ID = decimal.Parse(form["SEDE_ID"].ToString());
            if (form["MATRICULA_MINIMA"]!=null)
                obj.MATRICULA_MINIMA = decimal.Parse(form["MATRICULA_MINIMA"].ToString());
            servicioOfertasModulos.Modificar(obj);
            return obj;
        }

        public OFERTASMODULOS GenerarObjetoModulo(string urlSerialized)
        {
            OFERTASMODULOS obj;
            USUARIOS usuario = new ServicioUsuario().ObtenerPorLogin(User.Identity.Name);
            string query = System.Web.HttpUtility.UrlDecode(urlSerialized);
            NameValueCollection result = System.Web.HttpUtility.ParseQueryString(query);
            obj = new OFERTASMODULOS();
            obj.OFERTA_CURSO_ID = decimal.Parse(result["AUTOFERTAMOD.OFERTA_CURSO_ID"]);
            obj.CURSO_ID = decimal.Parse(result["AUTOFERTAMOD.CURSO_ID"]);
            obj.SEDE_ID = decimal.Parse(result["AUTOFERTAMOD.SEDE_ID"]);
            obj.TURNO_ID = decimal.Parse(result["AUTOFERTAMOD.TURNO_ID"]);
            obj.AULA_ID = decimal.Parse(result["AUTOFERTAMOD.AULA_ID"]);
            obj.FECHA_INICIO = DateTime.Parse(result["AUTOFERTAMOD.FECHA_INICIO"]);
            obj.MATRICULA_MINIMA = decimal.Parse(result["AUTOFERTAMOD.MATRICULA_MINIMA"]);
            obj.PROFESOR_ID = decimal.Parse(result["AUTOFERTAMOD.PROFESOR_ID"]);
            obj.USUARIO_ID = usuario.USUARIO_ID;
            return obj;
        }
    }
}
