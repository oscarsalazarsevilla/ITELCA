﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using System.Data.Entity;
using ITELCA_CLASSLIBRARY.Models;
using ITELCA_CLASSLIBRARY.Repositories;
using ITELCA_CLASSLIBRARY.CustomClasses;
using System.Collections;
using System.Web;
using UmbrellaClassLibrary.CustomClasses;


namespace ITELCA_CLASSLIBRARY.Services
{
     public class ServicioTaller
    {
        private UnitOfWork unidad;
        public ServicioTaller()
        {
            this.unidad = new UnitOfWork();
        }
        public bool Guardar(TALLERES taller)
        {
            try
            {
                taller.CONDICION = "C";
                unidad.RepositorioTaller.Add(taller);
                
                unidad.Save();
                return true;
            }
            catch
            {
                return false;
            }
        }
        public bool Modificar(TALLERES entity)
        {
            try
            {
                entity.CONDICION = "A";
                unidad.RepositorioTaller.Modify(entity);
                unidad.Save();
                return true;
            }
            catch
            {
                return false;
            }
        }


        public List<object> Eliminar(decimal id)
        {
            List<object> lista = new List<object>();
            try
            {
                var aux = unidad.RepositorioTaller.GetById((int)id);
                if (aux != null)
                {
                    lista.Add(new
                    {
                        ok = 1,
                        msg = "Taller Eliminado"
                    });
                    aux.CONDICION = "E";
                    aux.ESTADO = 0;
                    unidad.RepositorioTaller.Modify(aux);
                    unidad.Save();
                    return lista;
                }
                else
                {
                    lista.Add(new
                    {
                        ok = 0,
                        msg = "Taller No encontrado"
                    });
                    return lista;
                }
            }
            catch
            {
                lista.Clear();
                lista.Add(new
                {
                    ok = 0,
                    msg = "Error Eliminando Taller"
                });
                return lista;
            }

        }

        public object ObtenerDataGrid( string sidx, string sord, int page, int rows, string filters)
        {
            int totalPages = 0;
            int totalRecords = 0;
            IEnumerable<TALLERES> talleres;
            talleres = unidad.RepositorioTaller.GetAll().Where(u => u.ESTADO == 1 ||(u.ESTADO==0 && u.CONDICION != "E"));
            talleres = JqGrid<TALLERES>.GetFilteredContent(sidx, sord, page, rows, filters, talleres.AsQueryable(), ref totalPages, ref totalRecords);
           
            var rowsModel = (
                from taller in talleres.ToList()
                select new
                {
                    i = taller.TALLER_ID,
                    cell = new string[] {
                           taller.TALLER_ID.ToString(),
                           taller.NOMBRE,
                           taller.TIPOSTALLERES.NOMBRE,
                           taller.DESCRIPCION,
                           (taller.ESTADO == 1) ? "Activo" : "Inactivo",
                            "<a title=\"Editar\" href=\"/Taller/Editar/"+ taller.TALLER_ID+"\"><span id=\""+taller.TALLER_ID+"\" class=\"ui-icon ui-icon-pencil\"></a>",                          
                            "<span id=\""+taller.TALLER_ID+"\"  class=\"ui-icon ui-icon-close\" ></span>" }
                }).ToArray();
            return JqGrid<TALLERES>.SetJsonData(totalPages, totalRecords, page, rowsModel);
        }

        public object ObtenerDataGridProximosTalleres(decimal tipoTallerId, string sidx, string sord, int page, int rows, string filters)
        {
            int totalPages = 0;
            int totalRecords = 0;
            IEnumerable<OFERTASTALLERES> talleres;
            talleres = unidad.RepositorioOfertasTalleres.GetAll().Where(u => u.TALLERES.TIPO_TALLER_ID == tipoTallerId);
            talleres = JqGrid<OFERTASTALLERES>.GetFilteredContent(sidx, sord, page, rows, filters, talleres.AsQueryable(), ref totalPages, ref totalRecords);

            var rowsModel = (
                from taller in talleres.ToList()
                select new
                {
                    i = taller.TALLER_ID,
                    cell = new string[] {
                           taller.TALLER_ID.ToString(),
                           taller.TALLERES.NOMBRE,
                           taller.TALLERES.DESCRIPCION,
                           taller.FECHA_INICIO.ToString(),
                           taller.FECHA_FIN.ToString()
                    }
                }).ToArray();
            return JqGrid<OFERTASTALLERES>.SetJsonData(totalPages, totalRecords, page, rowsModel);
        }

        public TALLERES ObtenerPorClave(int id)
        {
            try
            {
                return unidad.RepositorioTaller.GetById(id);
                
            }
            catch
            {
                return null;
            }
        }

       
        public IEnumerable<TALLERES> ObtenerTodos()
        {
            try
            {
                IEnumerable<TALLERES> taller= unidad.RepositorioTaller.GetAll().Where(u=>u.ESTADO==1).OrderBy(u=>u.NOMBRE);
                if (taller == null)
                    return Enumerable.Empty<TALLERES>();
                else
                    return taller;
            }
            catch  {
                return Enumerable.Empty<TALLERES>();
            }
        }

        public Dictionary<decimal, string> ObtenerTallerPorCurso(decimal cursoId)
        {
            Dictionary<decimal, string> lista = new Dictionary<decimal, string>();
            try
            {
                CURSOS curso = unidad.RepositorioCurso.GetAll().Where(u => u.CURSO_ID == cursoId && u.ESTADO==1).FirstOrDefault();
                if (curso != null && curso.TALLERES != null)
                {
                    foreach (var ope in curso.TALLERES)
                    {
                        lista.Add(ope.TALLER_ID, ope.NOMBRE);
                    }
                }

                return lista;
            }
            catch
            {
                return lista;
            }
        }
        public bool Duplicado(TALLERES taller)
        {
            try
            {
                if (unidad.RepositorioTaller.GetAll().Where(u => u.NOMBRE.ToLower() == taller.NOMBRE.ToLower() &&
                                                                 u.TALLER_ID != taller.TALLER_ID && u.ESTADO==1).Count() > 0)
                    return true;
                else
                    return false;

            }
            catch 
            {
                return false;
            }
        }

        public Dictionary<decimal, string> ObtenerDiccionarioTaller()
        {
            Dictionary<decimal, string> lista = new Dictionary<decimal, string>();
            foreach (var entity in ObtenerTodos().ToList())
            {
                lista.Add(entity.TALLER_ID, entity.NOMBRE);
            }
            return lista;
        }

        public string ImprimirTalleresParaMenu()
        {
            string menu = "", enlace = "", tipoTaller = "";
            int cont = 0;
            List<TIPOSTALLERES> tipos = new ServicioTipoTaller().ObtenerTodos().ToList();

            foreach (var tipo in tipos)
            {
                if (cont == 0)
                {
                    menu += "<div>";
                }
                enlace = "/Home/TiposTalleres/" + tipo.TIPO_TALLER_ID;
                tipoTaller = tipo.NOMBRE;
                menu += "<strong><a style='color:#B6EFFC;text-decoration: underline; font-size:13px;' href=\"" + enlace + "\">" + tipoTaller + "</a></strong>";
                cont++;

                // Obtenemos la cantidad de talleres
                foreach (var taller in ObtenerPorTipos(tipo.TIPO_TALLER_ID))
                {
                    enlace = "/Home/Talleres/" + taller.TALLER_ID;
                    string tallerEtiqueta = taller.NOMBRE;
                    menu += "<a href=\"" + enlace + "\">" + tallerEtiqueta + "</a>";

                    if ((cont % 35 == 0) && (cont != 0))
                    {
                        menu += "</div><div>";
                    }
                    cont++;
                }

                if ((cont % 35 == 0) && (cont != 0))
                {
                    menu += "</div><div>";
                    cont++;
                }
            }
            return menu;
        }

        public List<TALLERES> ObtenerPorTipos(decimal tipo)
        {
            return ObtenerTodos().Where(c => c.TIPO_TALLER_ID == tipo).OrderBy(c => c.NOMBRE).ToList();
        }
       
    }
}
