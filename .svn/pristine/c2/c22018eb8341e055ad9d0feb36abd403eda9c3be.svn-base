﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Security.Cryptography;
using System.Web.Security;
using System.Web.Configuration;
using System.Configuration;
using System.Collections.Specialized;
using System.Configuration.Provider;
using System.Data;
using System.Data.SqlClient;
using ITELCA_CLASSLIBRARY.Models;
using ITELCA_CLASSLIBRARY.Services;
using ITELCA_CLASSLIBRARY.Repositories;

namespace CustomMembershipProvider
{
    class CustomMembershipRepository : ICustomMembershipRepository
    {
        #region Attributes and Constructor
        private UnitOfWork _unidad; 
        private  ServicioRol _servicioRoles;

        public CustomMembershipRepository()
        {
            _servicioRoles = new ServicioRol();
            _unidad = new UnitOfWork();
        }

        #endregion

        #region Role Provider Methods
        
        public void CreateRole(string name)
        {
            try
            {
                ROLES role = new ROLES();
                role.NOMBRE = name;

                if (!IsRoleDuplicated(name))
                {
                    _servicioRoles.Guardar(role);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            } 
        }

        public bool DeleteRole(string name)
        {
            
            ROLES rol = _servicioRoles.ObtenerTodos().Where(r => r.NOMBRE == name).SingleOrDefault();

            try
            {
                _servicioRoles.Eliminar((int)rol.ROL_ID);               
                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }  
        }

        public bool IsRoleDuplicated(string name)
        {
            try
            {
                return _servicioRoles.ObtenerTodos().Where(c => c.NOMBRE == name).Count() > 0;
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }
        }

        public string[] GetAllRoles()
        {
            
            try
            {
                var rs = _servicioRoles.ObtenerTodos().Select(r => r.NOMBRE).ToArray();                
                return rs;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string [] GetUsersByRole(string name)
        {

            try
            {
                var rs = new ServicioUsuario().ObtenerTodos().Where(u =>u.ROLES.Any(r=>r.NOMBRE==name)).Select(u=>u.NOMBRE).ToArray();
                return rs;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string[] GetUserRole(string login)
        {

            try
            {
                var role = new ServicioUsuario().ObtenerPorLogin(login).ROLES.Select(c=>c.NOMBRE).ToArray();
                
                return role;
            }
            catch (Exception ex)
            {
                
                
                throw ex;
            }
        }

        public bool IsUserInRole(string login, string role)
        {
            try
            {
                var user = new ServicioUsuario().ObtenerPorLogin(login);
                if (user != null)
                    return (user.ROLES.Any(u=>u.NOMBRE == role));
                else
                    return false;
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }
        }

        #endregion

        #region Membership Provider Methods

        public bool ChangePassword(string login, string oldPassword, string newPassword)
        {
            ServicioUsuario userService = new ServicioUsuario();
            USUARIOS user = userService.ObtenerPorLogin(login);

            if (user != null)
            {
                if (ValidatePassword(oldPassword, user.PASSWORD))
                {
                    user.PASSWORD = EncryptPassword(newPassword);
                    _unidad.RepositorioUsuario.Modify(user);                    
                    return true;
                }
            }
            
            return false;
        }

        public int CountOnLineUsers()
        {
            return new ServicioUsuario().ObtenerTodos().Where(u => Convert.ToDateTime(u.FECHAULTIMOLOGUEO).Date == DateTime.Now.Date && Convert.ToDateTime(u.FECHAULTIMOLOGUEO).Hour == DateTime.Now.Hour).Count();
        }
       
        public MembershipUser CreateUser(string login, string password, string email, out MembershipCreateStatus status)
        {
            ServicioUsuario userService = new ServicioUsuario();

           /* if (userService.GetByEmail(email).Email != String.Empty)
            {
                status = MembershipCreateStatus.DuplicateEmail;
                return null;
            }*/
            if (userService.ObtenerPorLogin(login).NOMBREUSUARIO != String.Empty)
            {
                status = MembershipCreateStatus.DuplicateUserName;
                return null;
            }
            else
            {
                USUARIOS user = new    USUARIOS();
                user.NOMBREUSUARIO = login;
                user.PASSWORD = EncryptPassword(password);
                user.EMAIL = email;
                user.FECHAULTIMOBLOQUEO = DateTime.Now;
                user.FECHAULTIMOCAMBIOPASSWORD = DateTime.Now;
                user.FECHAULTIMOLOGUEO = DateTime.Now;
                user. FECHAULTIMOCIERRESESION= DateTime.Now;
                user.CONTADORINICIOFALLIDO = 0;
                user.ESTADO = 1;
                user.ESTABLOQUEADO = 0;
                user.ESTADO = 1;
                userService.Guardar(user);
                status = MembershipCreateStatus.Success;

                return GetMembershipUser(login);
            }
        }

        public bool DeleteUser(string login)
        {
            try
            {
                ServicioUsuario userService = new ServicioUsuario();
                USUARIOS user = userService.ObtenerPorLogin(login);
                userService.Eliminar(user.USUARIO_ID);
                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }
        }
        
        public string EncryptPassword(string originalPassword)
        {
            Byte[] originalBytes;
            Byte[] encodedBytes;
            SHA1 sha1;

            //Instantiate MD5CryptoServiceProvider, get bytes for original password and compute hash (encoded password)
            sha1 = new SHA1CryptoServiceProvider();
            originalBytes = ASCIIEncoding.Default.GetBytes(originalPassword);
            encodedBytes = sha1.ComputeHash(originalBytes);

            //Convert encoded bytes back to a 'readable' string
            
            return BitConverter.ToString(encodedBytes);
        }

        public string GenerateNewPassword(string username)
        {
            ServicioUsuario userService = new ServicioUsuario();
            string randomString = "";
            string pwd = "";
            Random randObj = new Random();

            randomString = randomString + randObj.Next().ToString();

            USUARIOS user = userService.ObtenerPorLogin(username);
            pwd = EncryptPassword(randomString);

            user.PASSWORD = pwd;
            user.FECHAULTIMOCAMBIOPASSWORD = DateTime.Now;
            user.CONTADORINICIOFALLIDO = 0;
            user.ESTADO = 1;
            user.ESTABLOQUEADO = 0;
            userService.Modificar(user);

            return randomString;
        }

        public MembershipUser GetMembershipUser(string login)
        {
             try
            {
                USUARIOS user = new ServicioUsuario().ObtenerPorLogin(login);
                MembershipUser membershipUser = new MembershipUser(
                    "CustomMembershipProvider", user.NOMBREUSUARIO,
                    user.USUARIO_ID,
                    user.EMAIL,
                    string.Empty,
                    string.Empty,
                    user.ESTADO==1 ? true:false,
                    user.ESTABLOQUEADO==1 ? true:false,
                    (DateTime)user.FECHA_CREACION,
                    (DateTime)user.FECHAULTIMOLOGUEO,
                    (DateTime)user.FECHAULTIMOCIERRESESION,
                    (DateTime)user.FECHAULTIMOCAMBIOPASSWORD,
                    (DateTime)user.FECHAULTIMOBLOQUEO
                );
                
                return membershipUser;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        public int GetMinimumPasswordLength()
        {
            return 6;
        }
        
        public string GetUserByEmail(string email) 
        {
            string username = String.Empty;
            try
            {
                USUARIOS user = new ServicioUsuario().ObtenerPorCorreo(email);

                if (user != null)
                {
                    username = user.NOMBREUSUARIO.ToString();
                }
                
                return username;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetUserByLogin(string login)
        {
            string username = String.Empty;
            try
            {
                USUARIOS user = new ServicioUsuario().ObtenerPorLogin(login);

                if (user != null)
                {
                    username = user.NOMBREUSUARIO.ToString();
                }
                
                return username;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateUserLogout(string login)
        {
            try
            {
                ServicioUsuario userService = new ServicioUsuario();
                USUARIOS user = userService.ObtenerPorLogin(login);
                user.ESTADO = 0;
                user.FECHAULTIMOCIERRESESION = DateTime.Now;
                userService.Modificar(user);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ValidatePassword(string inputPassword, string dbPassword)
        {
            string encodedPassword = EncryptPassword(inputPassword);

            if (dbPassword == encodedPassword)
            {
                return true;
            }
            else
                return false;
        }

        public bool ValidateUser(string login, string password, int attemptsCounter)
        {
            try
            {
                ServicioUsuario userService = new ServicioUsuario();
                USUARIOS user = userService.ObtenerPorLogin(login);
                if (user.ESTABLOQUEADO==1)
                {                    
                    return false;
                }
                if (ValidatePassword(password, user.PASSWORD))
                {
                    user.CONTADORINICIOFALLIDO = 0;
                    user.FECHAULTIMOLOGUEO = DateTime.Now;
                    user.ESTADO = 1;
                    userService.Modificar(user);                 
                    return true;
                }
                else
                {
                    int previousLoginAttempts =(int) user.CONTADORINICIOFALLIDO;
                    previousLoginAttempts = previousLoginAttempts + 1;

                    if (previousLoginAttempts >= attemptsCounter)
                    {
                        user.ESTABLOQUEADO = 1;
                        user.FECHAULTIMOBLOQUEO = DateTime.Now;
                    }
                    else
                        user.CONTADORINICIOFALLIDO = previousLoginAttempts;
                    userService.Modificar(user);
                    
                    return false;
                }
            }
            catch
            {
                return false;
            }
        }
        #endregion

    }
}
