﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ITELCA_CLASSLIBRARY.Models;
using ITELCA_CONTROLESTUDIOS.Models.ViewModels;
using ITELCA_CLASSLIBRARY.Services;
using ITELCA_CLASSLIBRARY.CustomClasses;

namespace ITELCA_CONTROLESTUDIOS.Controllers
{
     [Authorize(Roles = "ADMINISTRADOR,COORDINADOR,ESTUDIANTE,EMPLEADO")]
    public class MatriculaCursoController : Controller
    {
        //
        // GET: /Rol/
        MatriculaCursoViewModel vistaModelo;
        ServicioMatriculaCurso servicioMatriculaCurso = new ServicioMatriculaCurso();
        ServicioMatriculaModulo servicioMatriculaModulo = new ServicioMatriculaModulo();
        ServicioAsistencia servicioAsistencia = new ServicioAsistencia();
        ServicioMensualidad servicioMesualidad = new ServicioMensualidad();

        SelectList activo;
        SelectList aulas;
        SelectList turnos;
        SelectList modulos;
        SelectList sedes;
        SelectList cursos;
        SelectList cursosGrid;
        SelectList generos;
        public void IniciarVista(MATRICULASCURSOS matriculaCurso) {
            USUARIOS usuario = new ServicioUsuario().ObtenerPorLogin(User.Identity.Name);
            Dictionary<int, string> lista = new Dictionary<int, string>();
            lista.Add(1, "SI");
            lista.Add(0, "NO");
            generos = new SelectList(new ServicioGenero().ObtenerLista(), "GENERO_ID", "NOMBRE", usuario.GENERO_ID);
           
            activo = new SelectList(lista, "Key", "Value");
            aulas = new SelectList(new ServicioAula().ObtenerDiccionarioAulas(), "Key", "Value");
            turnos = new SelectList(new ServicioTurno().ObtenerDiccionarioTurno(), "Key", "Value");
            modulos = new SelectList(new ServicioModulo().ObtenerDiccionarioModulo(), "Key", "Value");
            sedes = new SelectList(new ServicioSede().ObtenerTodos(), "SEDE_ID", "NOMBRE");
            cursos = new SelectList(new ServicioCursos().ObtenerCursosNoIscritos(usuario.USUARIO_ID), "CURSO_ID", "NOMBRE");
            cursosGrid = new SelectList(new ServicioCursos().ObtenerTodos(), "CURSO_ID", "NOMBRE"); 
            
            vistaModelo = new MatriculaCursoViewModel(matriculaCurso, activo, turnos, sedes, cursos, cursosGrid,generos);
        }

        public ActionResult Inscripcion()
        {
            IniciarVista(null);
            return View(vistaModelo);
        }

        public ActionResult InscripcionManualCurso()
        {
            IniciarVista(null);
            return View(vistaModelo);
        }

     
        public ActionResult Editar(int id) {
            MATRICULASCURSOS matriculaCurso = servicioMatriculaCurso.ObtenerPorClave(id);
            if (matriculaCurso != null)
            {
                IniciarVista(matriculaCurso);
                return View(vistaModelo);
            }
            else
            {
                TempData["mensaje"] = "Oferta Curso no Encontrado.";
                return RedirectToAction("Listar");
            }
        }

        public ActionResult Listar() {
            return View();
        }

        [HttpPost]
        public ActionResult Crear([Bind(Prefix = "MATRICULASCURSOS")] MATRICULASCURSOS matriculaCurso)
        {
            if (ModelState.IsValid) {
                USUARIOS usuario = new ServicioUsuario().ObtenerPorLogin(User.Identity.Name);
                matriculaCurso.USUARIO_ID = usuario.USUARIO_ID;
                if (servicioMatriculaCurso.Guardar(matriculaCurso))
                    {
                        TempData["mensaje"] = "Oferta Guardado Satisfactoriamente.";
                        return RedirectToAction("Listar");
                    }
                    else
                        ModelState.AddModelError(string.Empty, "Error Guardando Oferta");                
            }
            IniciarVista(matriculaCurso);
            return View(vistaModelo);

        }

        [HttpPost]
        public JsonResult Eliminar(int id)
        {
            return Json(servicioMatriculaCurso.Eliminar(id));
        }

        [HttpPost]
        public JsonResult ObtenerData( string sidx, string sord, int page, int rows, string filters)
        {
             USUARIOS usuario = new ServicioUsuario().ObtenerPorLogin(User.Identity.Name);
            object resultado = servicioMatriculaCurso.ObtenerDataGrid(usuario.USUARIO_ID, sidx, sord, page, rows, filters);
            return Json(resultado);

        }
        [HttpPost]
        public JsonResult ObtenerSede(string curso)
        {

            return Json(new ServicioOfertaCurso().ObtenerSede(long.Parse(curso)));
        }

        [HttpPost]
        public JsonResult ObtenerTurno(string curso,string sede)
        {
            try
            {

                return Json(new ServicioOfertaCurso().ObtenerTurno(long.Parse(curso), long.Parse(sede)));
            }
            catch {
                return Json("");
            }
        }

        [HttpPost]
        public JsonResult ObtenerOferta(string cursoId, string sedeId, string turnoId)
        {
            try
            {
                return Json(new ServicioOfertasModulos().ObtenerProximoModulo(long.Parse(cursoId), long.Parse(sedeId), long.Parse(turnoId)));

            }
            catch {
                return Json("");
            }
        }

        [HttpPost]
        public JsonResult ObtenerInfoModuloPorId(string id)
        {
            return Json(new ServicioOfertasModulos().ObtenerObjetoPorId(long.Parse(id)));
        }
        [HttpPost]
        public JsonResult GuardarMatricula(string idOfertaModulo,decimal monto,string formaP)
        {

            MATRICULASCURSOS matriculaCurso;
            MATRICULASMODULOS modulo;
            OFERTASMODULOS oferta = new ServicioOfertasModulos().ObtenerPorClave(long.Parse(idOfertaModulo));
            IEnumerable<OFERTASMODULOS> listaModulos = new ServicioOfertasModulos().ObtenerProximosModulos(long.Parse(idOfertaModulo));
            USUARIOS usuario = new ServicioUsuario().ObtenerPorLogin(User.Identity.Name);

            //guardo el modulo seleccionado
            matriculaCurso = GenerarObjetoMatriculaCurso(usuario, oferta);
            servicioMatriculaCurso.Guardar(matriculaCurso);
            //ingreso modulo seleccionado por el alumno
            modulo = GenerarObjetoMatriculaModulo(usuario, oferta);
            servicioMatriculaModulo.Guardar(modulo);
            List<DateTime> lista = servicioAsistencia.ObtenerListaClases(oferta);
            //GuardarAsistencia(lista, modulo,oferta);
            //guardo los modulos complementarios del curso
            foreach (var ope in listaModulos) {
                MATRICULASMODULOS moduloN = GenerarObjetoMatriculaModulo(usuario, ope);
                servicioMatriculaModulo.Guardar(moduloN);
                lista = servicioAsistencia.ObtenerListaClases(ope);
                //GuardarAsistencia(lista, moduloN,ope);
                
            }

            //guardo las cuotas que debe cancelar el alumno por el curso;
            int cuotas = obtenerCuotasPorCurso(idOfertaModulo, oferta);
            for (int i = 0; i < cuotas; i++)
            { 
                        
            }
                //object resultado = new string ().ObtenerDataGg.parrid(si);
                return Json("");

        }
         

        [HttpPost]
        public JsonResult ObtenerCursosPreInscritosPorAlumno(string documento)
        {

            return Json(new ServicioMatriculaCurso().ObtenerCursosPreInscritosPorAlumno(Decimal.Parse(documento)));
        }

         [HttpPost]
        public JsonResult obtenerPagosCurso(string idOfertaModulo)
        {
            OFERTASMODULOS modulo = new ServicioOfertasModulos().ObtenerPorClave(long.Parse(idOfertaModulo));
            List<object> lista = new List<object>();
            USUARIOS usuario = new ServicioUsuario().ObtenerPorLogin(User.Identity.Name);
            int mensualidad = obtenerCuotasPorCurso(idOfertaModulo,modulo);
            lista.Add(new
            {
                pagos = mensualidad,
                monto = modulo.OFERTASCURSOS.MONTO.HasValue?modulo.OFERTASCURSOS.MONTO.Value.ToString():"0,00"

            });
            return Json(lista);
       
        }
         public int obtenerCuotasPorCurso(string idOfertaModulo, OFERTASMODULOS modulo)
         {
            IEnumerable<OFERTASMODULOS> oferta = new ServicioOfertasModulos().ObtenerProximosModulos(long.Parse(idOfertaModulo));

            DateTime fechaFinCurso;
            if (oferta.Count()>0)
                fechaFinCurso = oferta.Max(u => u.FECHA_FIN).HasValue ? oferta.Max(u => u.FECHA_FIN).Value : DateTime.Now;
            else
                fechaFinCurso = modulo.FECHA_FIN.HasValue?modulo.FECHA_FIN.Value:DateTime.Now;
            int dias=(fechaFinCurso-modulo.FECHA_INICIO).Days;
            decimal resta = ((decimal)dias / (decimal)30);
            int mensualidad=(int)Math.Ceiling(resta);

            return mensualidad;
        }
        public MATRICULASCURSOS GenerarObjeto(FormCollection form) {
            int id = int.Parse(form["OFERTA_MODULO_ID"].ToString());
            MATRICULASCURSOS obj = servicioMatriculaCurso.ObtenerPorClave(id);
            if (form["TURNO_ID"]!=null)
                obj.TURNO_ID = decimal.Parse(form["TURNO_ID"].ToString());
            if (form["SEDE_ID"]!=null)
                obj.SEDE_ID = decimal.Parse(form["SEDE_ID"].ToString());
            return obj;
        }

        public MATRICULASCURSOS GenerarObjetoMatriculaCurso(USUARIOS usuario,OFERTASMODULOS oferta)
        {
            MATRICULASCURSOS matriculaCurso = new MATRICULASCURSOS();
            matriculaCurso.ALUMNO_ID = usuario.USUARIO_ID;
            matriculaCurso.CONDICION = "PRE-INSCRITO";
            matriculaCurso.COSTO_MENSUALIDAD = oferta.OFERTASCURSOS.MONTO;
            matriculaCurso.DIA_CORTE = oferta.FECHA_INICIO.Day;
            matriculaCurso.ESTADO = 1;
            matriculaCurso.FECHA_INSCRIPCION = DateTime.Now;
            matriculaCurso.OFERTA_CURSO_ID = oferta.OFERTA_CURSO_ID.HasValue ? oferta.OFERTA_CURSO_ID.Value : 0;
            matriculaCurso.SEDE_ID = oferta.SEDE_ID;
            matriculaCurso.TURNO_ID = oferta.TURNO_ID;
            matriculaCurso.CURSO_ID = oferta.CURSO_ID;
            matriculaCurso.DESCUENTO = 0;
            matriculaCurso.TIENE_DCTO = 0;
            matriculaCurso.USUARIO_ID = usuario.USUARIO_ID;
            return matriculaCurso;
        }

        public MATRICULASMODULOS GenerarObjetoMatriculaModulo(USUARIOS usuario, OFERTASMODULOS oferta)
        {
            MATRICULASMODULOS modulo = new MATRICULASMODULOS();
            modulo.ALUMNO_ID = usuario.USUARIO_ID;
            modulo.AULA_ID = oferta.AULA_ID;
            modulo.CONDICION = "I";
            modulo.ESTADO = 1;
            //modulo.FEC_INSC_MAT_CUR = oferta.FECHA_INSCRIPCION.HasValue ? matriculaCurso.FECHA_INSCRIPCION.Value : DateTime.Now;
            modulo.FECHA_INICIO = oferta.FECHA_INICIO;
            modulo.FECHA_FIN = oferta.FECHA_FIN.HasValue ? oferta.FECHA_FIN.Value : DateTime.Now;
            modulo.MODULO_ID = oferta.MODULO_ID.HasValue ? oferta.MODULO_ID.Value : 0;
            modulo.OFERTA_CURSO_ID = oferta.OFERTA_CURSO_ID.HasValue ? oferta.OFERTA_CURSO_ID.Value : 0;
            modulo.SEDE_ID = oferta.SEDE_ID;
            modulo.USUARIO_ID = usuario.USUARIO_ID;

            return modulo;
        }
        public void GuardarAsistencia(List<DateTime> lista,MATRICULASMODULOS moduloN,OFERTASMODULOS oferta )
        {
            if (lista != null)
            {
                foreach (var clase in lista)
                {
                    ASISTENCIAS asistencia = new ASISTENCIAS();
                    asistencia.ALUMNO_ID = moduloN.ALUMNO_ID;
                    asistencia.ASISTIO = 0;
                    asistencia.OFERTA_CURSO_ID = moduloN.OFERTA_CURSO_ID;
                    asistencia.AULA_ID = moduloN.AULA_ID.Value;
                    asistencia.ESTADO = 1;
                    asistencia.FECHA_CLASE = clase;
                    asistencia.MODULO_ID = moduloN.MODULO_ID;
                    asistencia.SEDE_ID = moduloN.SEDE_ID.HasValue ? moduloN.SEDE_ID.Value : 0;
                    asistencia.TIPO_AULA_ID =oferta .AULAS.TIPO_AULA_ID;
                    asistencia.USUARIO_ID = moduloN.USUARIO_ID;
                    servicioAsistencia.Guardar(asistencia);
                }

            }
        }
        

    }
}
