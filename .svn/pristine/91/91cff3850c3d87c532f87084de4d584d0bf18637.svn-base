﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using System.Data.Entity;
using ITELCA_CLASSLIBRARY.Models;
using ITELCA_CLASSLIBRARY.Repositories;
using ITELCA_CLASSLIBRARY.CustomClasses;
using System.Collections;
using System.Web;
using UmbrellaClassLibrary.CustomClasses;
using System.Data.Entity.Validation;


namespace ITELCA_CLASSLIBRARY.Services
{
     public class ServicioMatriculaTaller
    {
        private UnitOfWork unidad;
        public ServicioMatriculaTaller()
        {
            this.unidad = new UnitOfWork();
        }
        public bool Guardar(MATRICULASTALLERES matriculaTaller)
        {
            try
            {

                unidad.RepositorioMatriculaTalleres.Add(matriculaTaller);
                
                unidad.Save();
                return true;
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                return false;
            }
        }
        public bool Modificar(MATRICULASTALLERES matriculaTaller)
        {
            try
            {

                unidad.RepositorioMatriculaTalleres.Modify(matriculaTaller);
                unidad.Save();
                return true;
            }
            catch
            {
                return false;
            }
        }


        public List<object> Eliminar(decimal id)
        {
            List<object> lista = new List<object>();
            try
            {
                var aux = unidad.RepositorioMatriculaTalleres.GetById((long)id);
                if (aux != null)
                {
                    lista.Add(new
                    {
                        ok = 1,
                        msg = "Matricula Eliminada"
                    });
                    unidad.RepositorioMatriculaTalleres.Delete(aux);
                    unidad.Save();
                    return lista;
                }
                else
                {
                    lista.Add(new
                    {
                        ok = 0,
                        msg = "Matricula No encontrado"
                    });
                    return lista;
                }
            }
            catch
            {
                lista.Clear();
                lista.Add(new
                {
                    ok = 0,
                    msg = "Error Eliminando Matricula"
                });
                return lista;
            }

        }

        public object ObtenerDataGrid( decimal idAlumno,string sidx, string sord, int page, int rows, string filters)
        {
            int totalPages = 0;
            int totalRecords = 0;
            IEnumerable<MATRICULASTALLERES> matriculaTalleres;
            matriculaTalleres =unidad.RepositorioMatriculaTalleres.GetAll().Where(u=>u.ALUMNO_ID==idAlumno);
            var rowsModel = (
                from matricula in matriculaTalleres.ToList()
                select new
                {
                    i = matricula.OFERTA_TALLER_ID,
                    cell = new string[] {
                           matricula.OFERTA_TALLER_ID.ToString(), 
                           matricula.OFERTASTALLERES.TALLERES.NOMBRE.ToString(),
                           matricula.OFERTASTALLERES.AULAS.SEDES.NOMBRE,
                           matricula.OFERTASTALLERES.FECHA_INICIO.ToShortDateString(),
                           matricula.OFERTASTALLERES.HORA,
                           matricula.CONDICION,
                          "<span id=\""+matricula.OFERTA_TALLER_ID+"_"+matricula.ALUMNO_ID+"\"  class=\"ui-icon ui-icon-circle-plus\" ></span>"}
                }).ToArray();
            return JqGrid<OFERTASTALLERES>.SetJsonData(totalPages, totalRecords, page, rowsModel);
        }

        public MATRICULASTALLERES ObtenerPorClave(decimal ofertaTallerId,decimal alumnoId)
        {
            try
            {
                return unidad.RepositorioMatriculaTalleres.GetAll().Where(u => u.OFERTA_TALLER_ID == ofertaTallerId &&
                                                                                u.ALUMNO_ID==alumnoId).FirstOrDefault();
                
            }
            catch 
            {
                return null;
            }
        }

        
        public IEnumerable<MATRICULASTALLERES> ObtenerTodos()
        {
            try
            {
                IEnumerable<MATRICULASTALLERES> matriculaTaller = unidad.RepositorioMatriculaTalleres.GetAll();
                if (matriculaTaller == null)
                    return Enumerable.Empty<MATRICULASTALLERES>();
                else
                    return matriculaTaller;
            }
            catch  {
                return Enumerable.Empty<MATRICULASTALLERES>();
            }
        }

        public Dictionary<string, string> ObtenerTodosPorAlumno(decimal idAlumno)
        {
            Dictionary<string, string> Dictionary = new Dictionary<string, string>();
            var lista = unidad.RepositorioMatriculaTalleres.GetAll().Where(u=>u.ALUMNO_ID==idAlumno);
            foreach (var matricula in lista)
            {
                Dictionary.Add(matricula.OFERTA_TALLER_ID.ToString(), matricula.OFERTASTALLERES.TALLERES.NOMBRE.ToString() + " - "+ matricula.OFERTASTALLERES.HORA);

            }
            return Dictionary;
        }

        public IEnumerable<decimal> ObtenerTalleresActivosPorAlumno(decimal idAlumno)
        {
            try
            {
                //condicion de curso:pre-inscrito,cursando,anulado,aprobado
                IEnumerable<decimal> idTalleres = unidad.RepositorioMatriculaTalleres.GetAll().Where(u=>u.ALUMNO_ID==idAlumno).Select(u=>u.OFERTA_TALLER_ID);
                if (idTalleres == null)
                    return Enumerable.Empty<decimal>();
                else
                    return idTalleres;
            }
            catch
            {
                return Enumerable.Empty<decimal>();
            }
        }
        public List<LISTACURSOS> ObtenerTalleresInscritosPorAlumno(decimal documento)
        {

            USUARIOS usuario = new ServicioUsuario().ObtenerObjetoPorCedula(documento.ToString());
            IEnumerable<MATRICULASTALLERES> matriculaTalleres = unidad.RepositorioMatriculaTalleres.GetAll().Where(u => u.ALUMNO_ID == usuario.USUARIO_ID && u.CONDICION == "C").OrderBy(u => u.OFERTASTALLERES.TALLERES.NOMBRE);

            List<LISTACURSOS> listaTalleres = new List<LISTACURSOS>();


            if (usuario != null && matriculaTalleres != null)
            {
                foreach (var ope in matriculaTalleres)
                {
                    LISTACURSOS obj = new LISTACURSOS();
                    obj.id = ope.OFERTA_TALLER_ID.ToString();
                    obj.nombre = ope.OFERTASTALLERES.TALLERES.NOMBRE.ToString();
                    listaTalleres.Add(obj);
                }

                return listaTalleres;
            }
            else
            {
                if (usuario == null)
                {
                    LISTACURSOS obj = new LISTACURSOS();
                    obj.id = "error";
                    obj.nombre = "Usuario No encontrado.";
                    listaTalleres.Add(obj);
                    return listaTalleres;
                }
                else
                {
                    LISTACURSOS obj = new LISTACURSOS();
                    obj.id = "error";
                    obj.nombre = "Matriculas Talleres No encontrada.";
                    listaTalleres.Add(obj);
                    return listaTalleres;
                }
            }

        }
        public bool ValidarTallerInscrito(decimal ofertaTallerId) {
            try {
                return unidad.RepositorioMatriculaTalleres.GetAll().Where(u => u.OFERTA_TALLER_ID == ofertaTallerId && u.CONDICION=="C").Count() > 0;
            }
            catch { 
                return false;
            }
            
        }

  
    }
}
