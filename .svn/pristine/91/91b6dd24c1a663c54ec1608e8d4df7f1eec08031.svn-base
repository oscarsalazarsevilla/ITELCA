﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using System.Data.Entity;
using ITELCA_CLASSLIBRARY.Models;
using ITELCA_CLASSLIBRARY.Repositories;
using ITELCA_CLASSLIBRARY.CustomClasses;
using System.Collections;
using System.Web;
using UmbrellaClassLibrary.CustomClasses;
using System.Data.Entity.Validation;

namespace ITELCA_CLASSLIBRARY.Services
{
     public class ServicioBanner
    {
        private UnitOfWork unidad;
        public ServicioBanner()
        {
            this.unidad = new UnitOfWork();
        }
        public bool Guardar(BANNERS banner)
        {
            try
            {
                banner.CONDICION = "C";
                unidad.RepositorioBanners.Add(banner);
                unidad.Save();
                return true;
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                return false;

            }
        }
        public bool Modificar(BANNERS banner)
        {
            try
            {
                banner.CONDICION = "A";
                unidad.RepositorioBanners.Modify(banner);
                unidad.Save();
                return true;
            }
            catch
            {
                return false;
            }
        }


        public List<object> Eliminar(decimal id)
        {
            List<object> lista = new List<object>();
            try
            {
                var aux = unidad.RepositorioBanners.GetById((int)id);
                if (aux != null)
                {
                    lista.Add(new
                    {
                        ok = 1,
                        msg = "Banner eliminado"
                    });
                    unidad.RepositorioBanners.Delete(aux);
                    unidad.Save();
                    return lista;
                }
                else
                {
                    lista.Add(new
                    {
                        ok = 0,
                        msg = "Banner no encontrado"
                    });
                    return lista;
                }
            }
            catch
            {
                lista.Add(new
                {
                    ok = 0,
                    msg = "Error eliminando banner"
                });
                return lista;
            }

        }

        public Dictionary<decimal, string> ObtenerDiccionarioCategorias()
        {
            Dictionary<decimal, string> lista = new Dictionary<decimal, string>();
            foreach (var entity in ObtenerTodos().ToList())
            {
                lista.Add(entity.BANNER_ID, entity.NOMBRE);
            }
            return lista;
        }

        public object ObtenerDataGrid( string sidx, string sord, int page, int rows, string filters)
        {
            int totalPages = 0;
            int totalRecords = 0;
            IEnumerable<BANNERS> banners;
            banners = unidad.RepositorioBanners.GetAll().Where(u => u.ESTADO == 1 || (u.ESTADO == 0 && u.CONDICION != "E"));
            banners = JqGrid<BANNERS>.GetFilteredContent(sidx, sord, page, rows, filters, banners.AsQueryable(), ref totalPages, ref totalRecords);
            
            var rowsModel = (
                from banner in banners.ToList()
                select new
                {
                    i = banner.BANNER_ID,
                    cell = new string[] {
                           banner.BANNER_ID.ToString(),
                           banner.NOMBRE.ToString(),
                           banner.TEXTO.ToString(),
                           banner.ORDEN.ToString(),
                           banner.URL.ToString(),
                           banner.FECHA_PUBLICACION.ToString("dd/MM/yyyy"),
                           (banner.FECHA_FIN_PUBLICACION.HasValue) ? banner.FECHA_FIN_PUBLICACION.Value.ToString("dd/MM/yyyy") : "---",
                           (banner.ESTADO == 1) ? "Activo" : "Inactivo",
                            "<a title=\"Editar\" href=\"/Banner/Editar/"+ banner.BANNER_ID+"\"><span id=\""+banner.BANNER_ID+"\" class=\"ui-icon ui-icon-pencil\"></a>",                          
                            "<span id=\""+banner.BANNER_ID+"\"  class=\"ui-icon ui-icon-close\" ></span>" }
                }).ToArray();
            return JqGrid<BANNERS>.SetJsonData(totalPages, totalRecords, page, rowsModel);
        }

        public BANNERS ObtenerPorClave(int id)
        {
            try
            {
                return unidad.RepositorioBanners.GetById(id);
                
            }
            catch 
            {
                return null;
            }
        }

       
        public IEnumerable<BANNERS> ObtenerTodos()
        {
            try
            {
                IEnumerable<BANNERS> sedes= unidad.RepositorioBanners.GetAll();
                if (sedes == null)
                    return Enumerable.Empty<BANNERS>();
                else
                    return sedes;
            }
            catch {
                return Enumerable.Empty<BANNERS>();
            }
        }

        public List<BANNERS> ObtenerTodos_Activos()
        {
            try
            {
                DateTime now = DateTime.Now;
                return unidad.RepositorioBanners.GetAll().
                        Where(b =>  b.ESTADO == 1 && 
                                    b.FECHA_PUBLICACION<= now && 
                                    (!b.FECHA_FIN_PUBLICACION.HasValue || b.FECHA_FIN_PUBLICACION.Value >= now))
                        .OrderBy(b=>b.ORDEN).ToList();
            }
            catch
            {
                return null;
            }
        }

        public bool Duplicado(BANNERS banner)
        {
            try
            {
                if (unidad.RepositorioBanners.GetAll().Where(u => u.NOMBRE.ToLower() == banner.NOMBRE.ToLower() && u.BANNER_ID!=banner.BANNER_ID).Count() > 0)
                    return true;
                else
                    return false;

            }
            catch 
            {
                return false;
            }
        }

        
        public List<object> ObtenerObjeto()
        {
            List<object> lista = new List<object>();
            try
            {

                foreach (var ope in unidad.RepositorioBanners.GetAll())
                    lista.Add(new
                    {
                        key = ope.BANNER_ID,
                        value = ope.NOMBRE
                    });




                return lista;
            }
            catch
            {
                return null;
            }
        }
       
    }
}
