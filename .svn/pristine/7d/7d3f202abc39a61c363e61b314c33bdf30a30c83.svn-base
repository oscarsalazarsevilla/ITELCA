﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Data.Objects;
using System.Security.Cryptography;
using System.Web;
using System.IO;
using System.Globalization;
using ITELCA_CLASSLIBRARY.Models.Classes;
using ITELCA_CLASSLIBRARY.Repositories;
using ITELCA_CLASSLIBRARY.Models.ReportModel;
using ITELCA_CLASSLIBRARY.Models;
using ITELCA_CLASSLIBRARY.Services;
using ITELCA_CLASSLIBRARY.CustomClasses;
using System.Threading;

namespace ITELCA_CLASSLIBRARY.Services
{
    public class ServicioSolicitudesReportes
    {
        private UnitOfWork unidad;
        private MontoEnLetras letras = new MontoEnLetras();

        public ServicioSolicitudesReportes()
        {
            this.unidad = new UnitOfWork();
     
        }


        public IQueryable<ReporteSolicitudes> ObtenerData(string id_solicitud)
        {
            SOLICITUDES solicitud = new ServicioSolicitudes().ObtenerPorClave(Int32.Parse(id_solicitud));
            List<ReporteSolicitudes> data = new List<ReporteSolicitudes>();

            USUARIOS usuario = new ServicioUsuario().ObtenerPorClave(solicitud.MATRICULASCURSOS.ALUMNO_ID);
            OFERTASCURSOS curso = new ServicioOfertaCurso().ObtenerPorClave(Int32.Parse(solicitud.MATRICULASCURSOS.OFERTA_CURSO_ID.ToString()));

            string hora_turno = "", dias_turno = "",  desde = "", hasta = "";
            int duracion = 0;

            int cont = 0;
            foreach(var dia in curso.TURNOS.DIAS_TURNOS) 
            {
                if (cont > 0)
                    dias_turno += ", " ;
                dias_turno += dia.DIAS.NOMBRE;
                cont++;
            }

            var configuraciones = new ServicioConfiguracion().ObtenerConfiguracion();
            var primerModulo = new ServicioMatriculaModulo().ObtenerPrimerModuloMatricula(solicitud.MATRICULA_CURSO_ID);
            var ultimoModulo = new ServicioMatriculaModulo().ObtenerUltimoModuloMatricula(solicitud.MATRICULA_CURSO_ID);
            desde = (primerModulo != null) ? primerModulo.OFERTASMODULOS.FECHA_INICIO.ToString("D", new CultureInfo("es-VE")).ToUpper() : "";
            hasta = (ultimoModulo != null) ? ultimoModulo.OFERTASMODULOS.FECHA_INICIO.ToString("y", new CultureInfo("es-VE")).ToUpper() : "";

            hora_turno = solicitud.MATRICULASCURSOS.TURNOS.DIAS_TURNOS.FirstOrDefault().HORA_INICIO.ToString() +
                         " a " + solicitud.MATRICULASCURSOS.TURNOS.DIAS_TURNOS.FirstOrDefault().HORA_FIN.ToString();
            duracion = new ServicioCursoDuracion().ObtenerDuracionCurso(decimal.Parse(curso.CURSO_ID.ToString()), decimal.Parse(curso.TURNO_ID.ToString())); 
            data.Add(new ReporteSolicitudes(usuario.USUARIO_ID.ToString(), usuario.APELLIDO.ToUpper(), usuario.NOMBRE.ToUpper(), 
                                            usuario.NRO_DOCUMENTO,
                                            curso.CURSO_ID.ToString(), curso.CURSOS.NOMBRE.ToUpper(), hora_turno, dias_turno,
                                            this.ObtenerDiaEnLetras(DateTime.Now.Day).ToUpper(),
                                            DateTime.Now.ToString("MMMM", new CultureInfo("es-VE")).ToUpper(),
                                            letras.Convertir(DateTime.Now.Year.ToString(),false),duracion.ToString(), desde, hasta,
                                            configuraciones != null ? configuraciones.DIRECTOR.ToString() : "LIC. JOSE LUIS SERRANO", solicitud.MATRICULA_CURSO_ID.ToString()));
                              
          return data.AsQueryable();                           
        }

        public IQueryable<DataPasantias> ObtenerDataPasantias(string id_solicitud)
        {


            SOLICITUDES solicitud = new ServicioSolicitudes().ObtenerPorClave(Int32.Parse(id_solicitud));
            List<DataPasantias> data = new List<DataPasantias>();


            OFERTASCURSOS curso = new ServicioOfertaCurso().ObtenerPorClave(Int32.Parse(solicitud.MATRICULASCURSOS.OFERTA_CURSO_ID.ToString()));

           
            data.Add(new DataPasantias(solicitud.SOLICITUD_ID.ToString(), solicitud.NOMBRE_INSTITUCION_PASANTIA.ToUpper(), solicitud.DIRECTOR_PASANTIA.ToUpper(),
                                            DateTime.Now.Day.ToString(),
                                            DateTime.Now.ToString("MMMM", new System.Globalization.CultureInfo("es-ES")).ToUpper(),
                                            DateTime.Now.Year.ToString(),solicitud.MATRICULA_CURSO_ID.ToString()));

            return data.AsQueryable();
        }

        public IQueryable<DataPasantias> ObtenerDataConstanciaNotas(string id_solicitud)
        {


            SOLICITUDES solicitud = new ServicioSolicitudes().ObtenerPorClave(Int32.Parse(id_solicitud));
            List<DataPasantias> data = new List<DataPasantias>();


            OFERTASCURSOS curso = new ServicioOfertaCurso().ObtenerPorClave(Int32.Parse(solicitud.MATRICULASCURSOS.OFERTA_CURSO_ID.ToString()));
           
            data.Add(new DataPasantias(     solicitud.SOLICITUD_ID.ToString(), solicitud.NOMBRE_INSTITUCION_PASANTIA.ToUpper(), 
                                            solicitud.DIRECTOR_PASANTIA.ToUpper(),
                                            DateTime.Now.Day.ToString(),
                                            DateTime.Now.ToString("MMMM", new System.Globalization.CultureInfo("es-ES")).ToUpper(),
                                            DateTime.Now.Year.ToString(),solicitud.MATRICULA_CURSO_ID.ToString()));

            return data.AsQueryable();
        }

        public IQueryable<DataConstanciaNotas> ObtenerNotasConstanciaNotas(string matriculacurso_id)
        {
            ServicioModulo _servicioModulo = new ServicioModulo();
            //SOLICITUDES solicitud = new ServicioSolicitudes().ObtenerPorClave(Int32.Parse(id_solicitud));
            MATRICULASCURSOS matriculaCurso = new ServicioMatriculaCurso().ObtenerPorClave(Int32.Parse(matriculacurso_id));
            List<DataConstanciaNotas> data = new List<DataConstanciaNotas>();

            OFERTASCURSOS ofertaCurso = new ServicioOfertaCurso().ObtenerPorClave(long.Parse(matriculaCurso.OFERTA_CURSO_ID.ToString()));
            
            foreach (var ofertaMod in ofertaCurso.OFERTASMODULOS) {  // Todos los módulos del curso
                MODULOS modulo = _servicioModulo.ObtenerPorClave(long.Parse(ofertaMod.MODULO_ID.ToString()));
                var moduloCursado = matriculaCurso.MATRICULASMODULOS.Where(u => u.OFERTA_MODULO_ID == ofertaMod.OFERTA_MODULO_ID).FirstOrDefault();
                if (moduloCursado != null)
                {
                    if (moduloCursado.NOTASMODULO.Count != 0)
                    {
                        if (moduloCursado.NOTASMODULO.FirstOrDefault().NOTA < moduloCursado.MODULOS.MINIMO_APROBATORIO) // Si no aprobo el módulo
                        {
                            data.Add(new DataConstanciaNotas(modulo.NOMBRE.ToString(), "NP"));
                        }
                        else //Si aprobo el módulo
                            data.Add(new DataConstanciaNotas(moduloCursado.MODULOS.NOMBRE.ToString(), moduloCursado.NOTASMODULO.FirstOrDefault().NOTA.ToString()));
                    }
                    else {  // Si esta cursando el módulo pero no tiene aun la nota definitiva.
                        data.Add(new DataConstanciaNotas(modulo.NOMBRE.ToString(), "-"));
                    }
                }
                else {
                    data.Add(new DataConstanciaNotas(modulo.NOMBRE.ToString(), "NC"));
                }
            }
            
            return data.AsQueryable();
        }

        public IQueryable<DataReciboCaja> ObtenerDataReciboCaja(string id_recibo)
        {
            List<DataReciboCaja> data = new List<DataReciboCaja>();
            try
            {
                ServicioReciboCaja servicioReciboCaja = new ServicioReciboCaja();
                RECIBOS recibo = servicioReciboCaja.ObtenerPorClave(Int32.Parse(id_recibo));
                USUARIOS alumno = new ServicioUsuario().ObtenerObjetoPorCedula(recibo.DOCUMENTO);
                List<DETALLESRECIBOS> detalle = servicioReciboCaja.ObtenerDetallePorRecibo(Int32.Parse(id_recibo));
                /*CURSOS curso = servicioReciboCaja.ObtenerCursoPorRecibo(Int32.Parse(id_recibo));
                MATRICULASCURSOS matricula = new ServicioMatriculaCurso().ObtenerMatriculaCursoPreInscritoPorAlumno(Decimal.Parse(recibo.DOCUMENTO), Decimal.Parse(detalle.CONCEPTOS_CURSOS.CURSO_ID.ToString()), Decimal.Parse(detalle.CONCEPTOS_CURSOS.TURNO_ID.ToString()));*/
                //MATRICULASCURSOS matricula = new ServicioMatriculaCurso().ObtenerPorClave(Int32.Parse(detalle.MATRICULACURSO_ID.ToString()));
                DateTime fechaInicioCurso=DateTime.Now;
                if (detalle != null)
                {
                    fechaInicioCurso = new ServicioConceptoEstudiante().ObtenerPrimeraMensualidad(detalle.FirstOrDefault().CONCEPTOS_ESTUDIANTES.MATRICULA_CURSO_ID.HasValue ? detalle.FirstOrDefault().CONCEPTOS_ESTUDIANTES.MATRICULA_CURSO_ID.Value : 0);
                }
                foreach (var det in detalle)
                {
                    string monto_letra = Utilidades.Conversiones.NumeroALetras(det.MONTO.ToString());
                    string descripcion = (det.CONCEPTO_ESTUDIANTE_ID.HasValue) ? det.CONCEPTOS_ESTUDIANTES.DESCRIPCION : det.PRODUCTOS.DESCR;
                    //string descripcion = (detalle.CONCEPTO_ESTUDIANTE_ID.HasValue) ? detalle.CONCEPTOS_ESTUDIANTES.DESCRIPCION : detalle.PRODUCTOS.DESCR;
                    data.Add(new DataReciboCaja((alumno.NOMBRE + " " + alumno.APELLIDO).ToUpper().ToString(), alumno.NRO_DOCUMENTO.ToString().ToUpper(),
                                                det.MONTO.ToString().ToUpper(), monto_letra, recibo.RECIBO_ID.ToString(),
                                                (det.CONCEPTO_ESTUDIANTE_ID.HasValue) ? det.CONCEPTOS_ESTUDIANTES.FECHA_VENCIMIENTO.AddMonths(-1).ToString("dd/MM/yyyy") + " a " + det.CONCEPTOS_ESTUDIANTES.FECHA_VENCIMIENTO.ToString("dd/MM/yyyy") : "",
                        //(det.CONCEPTO_ESTUDIANTE_ID.HasValue) ? det.CONCEPTOS_ESTUDIANTES.MATRICULASCURSOS.TURNOS.NOMBRE : "", 
                                                det.CONCEPTOS_ESTUDIANTES.MATRICULASCURSOS.TURNOS.BLOQUE!=null?det.CONCEPTOS_ESTUDIANTES.MATRICULASCURSOS.TURNOS.BLOQUE.Substring(0, 3):"",
                                                 det.CONCEPTOS_ESTUDIANTES!=null ?det.CONCEPTOS_ESTUDIANTES.ORDEN.Value.ToString():"1",
                        //matricula.CURSOS.NOMBRE.ToString().ToUpper(), "",
                                                (det.CONCEPTO_ESTUDIANTE_ID.HasValue) ? det.CONCEPTOS_ESTUDIANTES.MATRICULASCURSOS.CURSOS.NOMBRE : "",
                                                fechaInicioCurso.AddMonths(-1).ToString("dd-MMM-yyyy"),
                                                det.CONCEPTOS_ESTUDIANTES.FECHA_VENCIMIENTO.ToString("dd/MM/yyyy"),
                                                DateTime.Now.ToString("dd-MMM-yyyy")));
                }
                return data.AsQueryable();
            }catch{
                return data.AsQueryable();
            }
            
        }

        public IQueryable<DataReciboProducto> ObtenerDataReciboProducto(string id_recibo)
        {

            ServicioReciboCaja servicioReciboCaja = new ServicioReciboCaja();
            RECIBOS recibo = servicioReciboCaja.ObtenerPorClave(Int32.Parse(id_recibo));
            USUARIOS alumno = new ServicioUsuario().ObtenerObjetoPorCedula(recibo.DOCUMENTO);
            List<DETALLESRECIBOS> detalle = servicioReciboCaja.ObtenerDetallePorRecibo(Int32.Parse(id_recibo));
            /*CURSOS curso = servicioReciboCaja.ObtenerCursoPorRecibo(Int32.Parse(id_recibo));
            MATRICULASCURSOS matricula = new ServicioMatriculaCurso().ObtenerMatriculaCursoPreInscritoPorAlumno(Decimal.Parse(recibo.DOCUMENTO), Decimal.Parse(detalle.CONCEPTOS_CURSOS.CURSO_ID.ToString()), Decimal.Parse(detalle.CONCEPTOS_CURSOS.TURNO_ID.ToString()));*/
            //MATRICULASCURSOS matricula = new ServicioMatriculaCurso().ObtenerPorClave(Int32.Parse(detalle.MATRICULACURSO_ID.ToString()));


            List<DataReciboProducto> data = new List<DataReciboProducto>();



            foreach (var det in detalle)
            {
                string monto_letra = Utilidades.Conversiones.NumeroALetras(det.MONTO.ToString());
                string descripcion = (det.CONCEPTO_ESTUDIANTE_ID.HasValue) ?"": det.CANTIDAD.ToString()+" ";
                descripcion += (det.CONCEPTO_ESTUDIANTE_ID.HasValue) ? det.CONCEPTOS_ESTUDIANTES.DESCRIPCION : det.PRODUCTOS.DESCR;
                if (det.CONCEPTO_ESTUDIANTE_ID.HasValue)
                    if (det.MONTO < det.CONCEPTOS_ESTUDIANTES.MONTO)
                        descripcion = "ABONO " + descripcion;
                string curso="CURSO INSCRITO NO ENCONTRADO!!!";
                MATRICULASMODULOS matModulo= alumno.MATRICULASMODULOS.Where(u=>u.OFERTASMODULOS.FECHA_FIN>=DateTime.Today).FirstOrDefault();
                if(matModulo!=null)
                    curso=matModulo.MATRICULASCURSOS.CURSOS.NOMBRE;
                //string descripcion = (detalle.CONCEPTO_ESTUDIANTE_ID.HasValue) ? detalle.CONCEPTOS_ESTUDIANTES.DESCRIPCION : detalle.PRODUCTOS.DESCR;
                data.Add(new DataReciboProducto(alumno.NRO_DOCUMENTO,det.RECIBOS.NRORECIBO.ToString(),
                                               (alumno.NOMBRE + " " + alumno.APELLIDO).ToUpper().ToString(),det.MONTO.ToString("N"),
                                                curso,monto_letra+" BOLIVARES",descripcion, DateTime.Now.Day.ToString(),
                                                DateTime.Now.ToString("MMMM", new System.Globalization.CultureInfo("es-ES")).ToUpper(),
                                                DateTime.Now.Year.ToString()));            
            }
            return data.AsQueryable();
        }

        public string ObtenerDiaEnLetras(int dia)
        {
            switch (dia)
            {
                case 1:
                    return "al primer día";
                case 2:
                    return "a los dos días";
                case 3:
                    return "a los tres días";
                case 4:
                    return "a los cuatro días";
                case 5:
                    return "a los cinco días";
                case 6:
                    return "a los seis días";
                case 7:
                    return "a los siete días";
                case 8:
                    return "a los ocho días";
                case 9:
                    return "a los nueve días";
                case 10:
                    return "a los diez días";
                case 11:
                    return "a los once días";
                case 12:
                    return "a los doce días";
                case 13:
                    return "a los trece días";
                case 14:
                    return "a los catorce días";
                case 15:
                    return "a los quince días";
                case 16:
                    return "a los dieciséis días";
                case 17:
                    return "a los diecisiete días";
                case 18:
                    return "a los dieciocho días";
                case 19:
                    return "a los diecinueve días";
                case 20:
                    return "a los veinte días";
                case 21:
                    return "a los veintiún días";
                case 22:
                    return "a los veintidós días";
                case 23:
                    return "a los veintirés días";
                case 24:
                    return "a los veinticuatro días";
                case 25:
                    return "a los veinticinco días";
                case 26:
                    return "a los veintiséis días";
                case 27:
                    return "a los veintisiete días";
                case 28:
                    return "a los veintiocho días";
                case 29:
                    return "a los veintinueve días";
                case 30:
                    return "a los treinta días";
                default:
                    return "a los treinta y un días";
            }

        }


        public IQueryable<DataPlanificacion> ObtenerPlanificacion(string ofertaCurso)
        {
            var array = ofertaCurso.Split('_');

            OFERTASCURSOS oferta = new ServicioOfertaCurso().ObtenerPorClave(Int32.Parse(array[0]));
            List<DataPlanificacion> data = new List<DataPlanificacion>();

            if (array[2] == "Todos")
            {
                foreach (var prof_id in oferta.OFERTASMODULOS.Select(u => u.PROFESOR_ID).Distinct())
                {
                    string variable = array[0]+"_"+array[1]+"_"+prof_id; 
                    data.Add(new DataPlanificacion(oferta.SEDES.NOMBRE, oferta.CURSOS.NOMBRE, oferta.TURNOS.NOMBRE, variable));
                }
                
            }
            else { 
                data.Add(new DataPlanificacion(oferta.SEDES.NOMBRE, oferta.CURSOS.NOMBRE, oferta.TURNOS.NOMBRE , ofertaCurso));
            }

            return data.AsQueryable();
        }

        public IQueryable<ModulosPlanificacion> ObtenerModulosPlanificacion(string ofertaCurso)
        {
            var array = ofertaCurso.Split('_');
            OFERTASCURSOS oferta = new ServicioOfertaCurso().ObtenerPorClave(Int32.Parse(array[0]));
            List<ModulosPlanificacion> data = new List<ModulosPlanificacion>();
            DateTime desde = DateTime.ParseExact(array[1], "dd-MM-yyyy", CultureInfo.InvariantCulture);
   
            foreach (var modulo in oferta.OFERTASMODULOS.Where(u => u.PROFESOR_ID == decimal.Parse(array[2]) && u.FECHA_INICIO > desde).OrderBy(u=> u.FECHA_INICIO))
            {
                DateTime hasta = DateTime.Parse(modulo.FECHA_FIN.ToString());
                data.Add(new ModulosPlanificacion(modulo.CURSOS_MODULOS.MODULOS.NOMBRE,
                                                    modulo.FECHA_INICIO.Day+"/"+modulo.FECHA_INICIO.Month+"/"+modulo.FECHA_INICIO.Year,
                                                    hasta.Day+"/"+hasta.Month+"/"+hasta.Year,
                                                    modulo.MATRICULA_MINIMA.ToString(),
                                                    modulo.USUARIOS.NOMBRE+" "+modulo.USUARIOS.APELLIDO));
            }
            
            

            return data.AsQueryable();
        }
       

    }
}

