﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ITELCA_CLASSLIBRARY.Services;
using ITELCA_CONTROLESTUDIOS.Models.ViewModels;
using ITELCA_CLASSLIBRARY.Models;
using ITELCA_CONTROLESTUDIOS.Models;
using System.Web.Security;

namespace ITELCA_CONTROLESTUDIOS.Controllers
{
    [Authorize(Roles = "ADMINISTRADOR,COORDINADOR")]
    public class UsuarioController : Controller
    {
        //
        // GET: /Usuario/
        UsuarioViewModel vistaModeloUsuario;
        MultiSelectList roles;
        SelectList bloqueado;
        SelectList generos;

        public void IniciarVista(USUARIOS usuario) {
            Dictionary<int, string> lista = new Dictionary<int, string>();
            lista.Add(0, "NO");
            lista.Add(1, "SI");
           
            if (usuario != null)
            {
                roles = new MultiSelectList(new ServicioRol().ObtenerTodos(), "ROL_ID", "NOMBRE", usuario.ROLES.Select(u => u.ROL_ID));
                bloqueado = new SelectList(lista, "Key", "Value", usuario.ESTABLOQUEADO);
                generos = new SelectList(new ServicioGenero().ObtenerLista(), "GENERO_ID", "NOMBRE", usuario.GENERO_ID);
               
            }
            else
            {
                usuario = new USUARIOS();
                roles = new MultiSelectList(new ServicioRol().ObtenerTodos(), "ROL_ID", "NOMBRE");
                bloqueado = new SelectList(lista, "Key", "Value");
                generos = new SelectList(new ServicioGenero().ObtenerLista(), "GENERO_ID", "NOMBRE");
            }
            vistaModeloUsuario = new UsuarioViewModel(usuario, roles,bloqueado,generos);
        }
        [Authorize(Roles = "ADMINISTRADOR,COORDINADOR")]
        public ActionResult Crear()
        {
            IniciarVista(null);
            return View(vistaModeloUsuario);
        }
        [Authorize(Roles = "ADMINISTRADOR,COORDINADOR")]
        public ActionResult Editar(decimal id)
        {
            USUARIOS usuario = new ServicioUsuario().ObtenerPorClave(id);
            if (usuario != null)
            {
                IniciarVista(usuario);
                return View(vistaModeloUsuario);
            } 
            else
            {
                TempData["mensaje"] = "Usuario no Encontrado";
                return RedirectToAction("Listar");
            }

        }
        [Authorize(Roles = "ADMINISTRADOR,COORDINADOR")]
        public ActionResult Listar()
        {
            
            return View(); 
        }
        [Authorize(Roles = "ADMINISTRADOR,COORDINADOR,ESTUDIANTE")]
        public ActionResult Registro() {
            IniciarVista(null);
            return View(vistaModeloUsuario);
        }
        [Authorize(Roles = "ADMINISTRADOR,COORDINADOR,ESTUDIANTE")]
        public ActionResult RegistroEditar(decimal id)
        {
            USUARIOS usuario = new ServicioUsuario().ObtenerPorClave(id);
            if (usuario != null)
            {
                IniciarVista(usuario);
                return View(vistaModeloUsuario);
            }
            else
            {
                TempData["mensaje"] = "Usuario no Encontrado";
                return RedirectToAction("Listar");
            }
        }
        [Authorize(Roles = "ADMINISTRADOR,COORDINADOR,ESTUDIANTE")]
        public ActionResult CambiarClave()
        {
            CambiarClaveModels cambio = new CambiarClaveModels();
            return View(cambio);
        }

        [Authorize(Roles = "ADMINISTRADOR,COORDINADOR")]
        [HttpPost]
        public ActionResult Crear(FormCollection form , USUARIOS usuario)
        {
            ModelState.Remove("usuario.NRO_DOCUMENTO");
            if (ModelState.IsValid) {
                string aux=form["USUARIO.Rol_Id"];

                if (new ServicioUsuario().Guardar(usuario, aux))
                {
                    TempData["mensaje"] = "Usuario Creado Satisfatoriamente";
                    return RedirectToAction("Listar");
                }
                else 
                    ModelState.AddModelError(string.Empty, "Error Guardando Usuario");
                
            }
            IniciarVista(usuario);
            return View();
        }

        [Authorize(Roles = "ADMINISTRADOR,COORDINADOR")]
        [HttpPost]
        public ActionResult Editar(FormCollection form, USUARIOS usuario)
        {
            ModelState.Remove("usuario.PASSWORD");
            ModelState.Remove("usuario.NOMBREUSUARIO");
            ModelState.Remove("usuario.CONFIRMARPASSWORD");
            ModelState.Remove("usuario.NRO_DOCUMENTO");
            if (ModelState.IsValid)
            {
                string aux = form["USUARIO.Rol_Id"];

                if (new ServicioUsuario().Modificar(usuario, aux))
                {
                    TempData["mensaje"] = "Usuario Modificado Satisfatoriamente";
                    return RedirectToAction("Listar");
                }
                else
                    ModelState.AddModelError(string.Empty, "Error Guardando Usuario");

            }
            IniciarVista(usuario);
            return View(vistaModeloUsuario);
        }
        [Authorize(Roles = "ADMINISTRADOR,COORDINADOR,ESTUDIANTE")]
        [HttpPost]
        public ActionResult Registro(USUARIOS usuario)
        {
            ModelState.Remove("usuario.NOMBREUSUARIO");
            ModelState.Remove("usuario.CONFIRMARPASSWORD");
            ModelState.Remove("usuario.PASSWORD");
            ModelState.Remove("usuario.Rol_Id");
            if (ModelState.IsValid)
            {
               
                usuario.NOMBREUSUARIO = usuario.NRO_DOCUMENTO;
                usuario.PASSWORD = usuario.NOMBREUSUARIO;
                if (new ServicioUsuario().GuardarEstudiante(usuario))
                {
                    TempData["mensaje"] = "Usuario Creado Satisfatoriamente";
                    LogOnModel model = new LogOnModel();
                    model.UserName =usuario.NRO_DOCUMENTO;
                    model.Password = usuario.NRO_DOCUMENTO;

                    FormsAuthentication.SetAuthCookie(model.UserName, model.RememberMe);

                    Session["InicioSesion"] = "Si";
                    Session["IdUsuario"] = new ServicioUsuario().ObtenerPorLogin(model.UserName.ToString()).USUARIO_ID.ToString();

                    //new AccountController().LogOn(model, "/Home/Index");
                    return RedirectToAction("Index","Home");
                }
                else
                    ModelState.AddModelError(string.Empty, "Error Guardando Usuario");

            }
           
        
            IniciarVista(usuario);
            return View(vistaModeloUsuario);
        }
        [Authorize(Roles = "ADMINISTRADOR,COORDINADOR,ESTUDIANTE")]
        [HttpPost]
        public ActionResult RegistroEditar(USUARIOS usuario)
        {
            ModelState.Remove("usuario.NOMBREUSUARIO");
            ModelState.Remove("usuario.CONFIRMARPASSWORD");
            ModelState.Remove("usuario.PASSWORD");
            ModelState.Remove("usuario.Rol_Id");
            if (ModelState.IsValid)
            {

                if (new ServicioUsuario().ModificarEstudiante(usuario))
                {
                    TempData["mensaje"] = "Usuario Modificado Satisfatoriamente";
                    return RedirectToAction("Index","Home");
                }
                else
                    ModelState.AddModelError(string.Empty, "Error Guardando Usuario");

            }
            

            IniciarVista(usuario);
            return View(vistaModeloUsuario);
        }

        [HttpPost]
        public JsonResult ObtenerData(string sidx, string sord, int page, int rows, string filters) {
            object resultado = new ServicioUsuario().ObtenerDataGrid(sidx, sord, page, rows, filters);
            return Json(resultado);

        }

        [HttpPost]
        public JsonResult Eliminar(int id) {
             
            return Json(new ServicioUsuario().Eliminar(id)); 
        }

      
        [HttpPost]
        public ActionResult CambiarClave(FormCollection form)
        {
            var actual = form["passActual"];
            var nueva = form["passNueva"];
            string log = User.Identity.Name;
            CambiarClaveModels cambio = new CambiarClaveModels();
            List<object> lista = new List<object>();
            ServicioUsuario _servicioUsuario=new ServicioUsuario();
            USUARIOS usuario = _servicioUsuario.ObtenerPorLogin(log);
            if (usuario != null)
            {
                actual = _servicioUsuario.EncriptarClave(actual);
                if (usuario.PASSWORD == actual)
                {
                    nueva = _servicioUsuario.EncriptarClave(nueva);
                    usuario.PASSWORD = nueva;
                    usuario.FECHAULTIMOCAMBIOPASSWORD = DateTime.Now.Date;
                    try
                    {
                        if (_servicioUsuario.Modificar(usuario))
                        {
                            TempData["mensaje"] = "Clave Modificada Satisfactoriamente";
                            return View(cambio);
                        }
                        else
                        {
                            TempData["mensaje"] = "Error Modificando Clave ";
                            return View(cambio);
                        }
                    }
                    catch
                    {
                        ModelState.AddModelError(string.Empty, "Error Modificando Registro");
                       
                        return View (cambio);
                    }
                }
                else
                    ModelState.AddModelError("passActual", "Clave Actual Incorrecta");
               
                return  View (cambio);
            }
            else
            {
                ModelState.AddModelError(string.Empty, "Usuario no encontrado");
               
                return View(cambio);
            }

        }

    }
}
