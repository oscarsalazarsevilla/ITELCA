using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ITELCA_CLASSLIBRARY.Models;

namespace ITELCA_CLASSLIBRARY.Repositories
{
    public class UnitOfWork:IDisposable
    {
        private ItelcaEntities contexto = new ItelcaEntities();
        private RepositoryBase<USUARIOS> repositorioUsuario;
        private RepositoryBase<ROLES> repositorioRoles;
        private RepositoryBase<GENEROS> repositorioGeneros;
        private RepositoryBase<TIPOSCURSOS> repositorioTipoCurso;
        private RepositoryBase<TIPOSAULAS> repositorioTipoAula;
        private RepositoryBase<SEDES> repositorioSede;
        private RepositoryBase<MODULOS> repositorioModulo;
        private RepositoryBase<AULAS> repositorioAula;
        private RepositoryBase<CURSOS> repositorioCurso;
        private RepositoryBase<TURNOS> repositorioTurno;

        public RepositoryBase<USUARIOS> RepositorioUsuario
        {
            get
            {
                if (this.repositorioUsuario == null)
                {
                    this.repositorioUsuario = new RepositoryBase<USUARIOS>(contexto);
                }
                return repositorioUsuario;
            }
        }

        public RepositoryBase<ROLES> RepositorioRoles
        {
            get
            {
                if (this.repositorioRoles == null)
                {
                    this.repositorioRoles = new RepositoryBase<ROLES>(contexto);
                }
                return repositorioRoles;
            }
        }

        public RepositoryBase<GENEROS> RepositorioGeneros
        {
            get
            {
                if (this.repositorioGeneros == null)
                {
                    this.repositorioGeneros = new RepositoryBase<GENEROS>(contexto);
                }
                return repositorioGeneros;
            }
        }

        public RepositoryBase<TIPOSCURSOS> RepositorioTipoCurso
        {
            get
            {
                if (this.repositorioTipoCurso == null)
                {
                    this.repositorioTipoCurso = new RepositoryBase<TIPOSCURSOS>(contexto);
                }
                return repositorioTipoCurso;
            }
        }
        public RepositoryBase<TIPOSAULAS> RepositorioTipoAula
        {
            get
            {
                if (this.repositorioTipoAula == null)
                {
                    this.repositorioTipoAula = new RepositoryBase<TIPOSAULAS>(contexto);
                }
                return repositorioTipoAula;
            }
        }
        public RepositoryBase<SEDES> RepositorioSede
        {
            get
            {
                if (this.repositorioSede == null)
                {
                    this.repositorioSede = new RepositoryBase<SEDES>(contexto);
                }
                return repositorioSede;
            }
        }
        public RepositoryBase<MODULOS> RepositorioModulo
        {
            get
            {
                if (this.repositorioModulo == null)
                {
                    this.repositorioModulo = new RepositoryBase<MODULOS>(contexto);
                }
                return repositorioModulo;
            }
        }

        public RepositoryBase<AULAS> RepositorioAula
        {
            get
            {
                if (this.repositorioAula == null)
                {
                    this.repositorioAula = new RepositoryBase<AULAS>(contexto);
                }
                return repositorioAula;
            }
        }
        public RepositoryBase<CURSOS> RepositorioCurso { 
            get
            {
                if(this.repositorioCurso==null)
                {
                    this.repositorioCurso=new RepositoryBase<CURSOS>(contexto);
                }
                return repositorioCurso;
            }
        }

        public RepositoryBase<TURNOS> RepositorioTurno {
            get
            {
                if (this.repositorioTurno == null) {
                    this.repositorioTurno = new RepositoryBase<TURNOS>(contexto);
                }
                return repositorioTurno;
            }
        }
        public void Save()
        {
            try
            {
                contexto.SaveChanges();
            }
            catch (InvalidCastException e)
            {
                throw (e);    // Rethrowing exception e
            }
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    contexto.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
