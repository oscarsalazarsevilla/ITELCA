﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ITELCA_CLASSLIBRARY.Models;
using ITELCA_CONTROLESTUDIOS.Models.ViewModels;
using ITELCA_CLASSLIBRARY.Services;

namespace ITELCA_CONTROLESTUDIOS.Controllers
{
    public class ModuloController : Controller
    {
        //
        // GET: /Rol/
        ModuloViewModel vistaModeloModulo;
        SelectList activo;
        MultiSelectList curso;
        public void IniciarVista(MODULOS modulo) {
            ServicioUsuario servicioUsuario = new ServicioUsuario();
            Dictionary<int, string> lista = new Dictionary<int, string>();
            lista.Add(1, "SI");
            lista.Add(0, "NO");
            if (modulo != null)
            {
                activo = new SelectList(lista, "Key", "Value", modulo.ESTADO);
                curso = new MultiSelectList(new ServicioCurso().ObtenerTodos(), "CURSO_ID", "NOMBRE", modulo.CURSOS_MODULOS.Select(u => u.CURSO_ID));
            }
            else
            {
                activo = new SelectList(lista, "Key", "Value");
                curso = new MultiSelectList(new ServicioCurso().ObtenerTodos(), "CURSO_ID", "NOMBRE");
            }
            vistaModeloModulo = new ModuloViewModel(modulo, activo,curso);
        }

        public ActionResult Crear()
        {
            IniciarVista(null);
            return View(vistaModeloModulo);
        }
        
        public ActionResult Editar(int id) { 
            MODULOS modulo=new ServicioModulo().ObtenerPorClave(id);
            if (modulo != null)
            {
                IniciarVista(modulo);
                return View(vistaModeloModulo);
            }
            else
            {
                TempData["mensaje"] = "Modulo no Encontrado";
                return RedirectToAction("Listar");
            }
        }

        public ActionResult Listar() {
            return View();
        }

        [HttpPost]
        public ActionResult Crear(MODULOS modulo,FormCollection form) {
            if (ModelState.IsValid) { 
                ServicioModulo servicioModulo=new ServicioModulo();
                if (!servicioModulo.Duplicado(modulo))
                {
                    string curso = form["CURSO_ID"];
                    if (curso != "" && curso != null)
                    {
                        USUARIOS usuario = new ServicioUsuario().ObtenerPorLogin(User.Identity.Name);
                        modulo.USUARIO_ID = usuario.USUARIO_ID;

                        if (servicioModulo.Guardar(modulo,curso))
                        {
                            TempData["mensaje"] = "Modulo Guardado Satisfactoriamente";
                            return RedirectToAction("Listar");
                        }
                        else
                            ModelState.AddModelError(string.Empty, "Error Guardando Modulo");
                    }
                    else {
                        ModelState.AddModelError("CURSO_ID", "Debe Ingresar Curso");
                    }
                }
                else 
                    ModelState.AddModelError(string.Empty, "Sede ya existe");
                
            }
            IniciarVista(modulo);
            return View(vistaModeloModulo);

        }

        [HttpPost]
        public ActionResult Editar(MODULOS modulo,FormCollection form)
        {
            if (ModelState.IsValid)
            {
                ServicioModulo servicioModulo = new ServicioModulo();
                if (!servicioModulo.Duplicado(modulo))
                {
                    string curso = form["CURSO_ID"];
                    if (curso != "" && curso != null)
                    {
                        if (servicioModulo.Modificar(modulo,curso))
                        {
                            TempData["mensaje"] = "Modulo Modificado Satisfactoriamente";
                            return RedirectToAction("Listar");
                        }
                        else
                            ModelState.AddModelError(string.Empty, "Error Modificando Modulo");
                    }
                    else
                        ModelState.AddModelError(string.Empty, "Modulo ya existe");
                }
                else {
                    ModelState.AddModelError("CURSO_ID", "Debe Seleccionar Curso");
                }

            }
            IniciarVista(modulo);
            return View(vistaModeloModulo);

        }

        [HttpPost]
        public JsonResult Eliminar(int id)
        {
            return Json(new ServicioModulo().Eliminar(id));
        }
        [HttpPost]
        public JsonResult ObtenerData(string sidx, string sord, int page, int rows, string filters)
        {
            object resultado = new ServicioModulo().ObtenerDataGrid(sidx, sord, page, rows, filters);
            return Json(resultado);

        }

        [HttpPost]
        public string ObtenerLista(int curso)
        {
            return new ServicioModulo().ObtenerDiccionarioPorCurso(curso);
        }

    }
}
