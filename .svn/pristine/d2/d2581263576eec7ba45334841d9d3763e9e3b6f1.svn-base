﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ITELCA_CLASSLIBRARY.Models;
using ITELCA_CONTROLESTUDIOS.Models.ViewModels;
using ITELCA_CLASSLIBRARY.Services;
using Utilidades;

namespace ITELCA_CONTROLESTUDIOS.Controllers
{
     [Authorize(Roles = "ADMINISTRADOR,COORDINADOR")]
    public class ConceptosTiposSolicitudesController : Controller
    {
        ConceptoTipoSolicitudViewModel _vistaModelo;
        ServicioConceptoTipoSolicitud _servicio = new ServicioConceptoTipoSolicitud();
        SelectList _activo, _listaTiposSolicitudes, _listaConceptos;
        MultiSelectList _tiposSolicitudes;
        
        public void IniciarVista(CONCEPTOS_TIPOSSOLICITUDES conceptoTipoSolicitud) {

            Dictionary<int, string> lista = new ListasGenerales().ListaActivo();
 
            if (conceptoTipoSolicitud != null)
            {
                _activo = new SelectList(lista, "Key", "Value", conceptoTipoSolicitud.ESTADO);
                _listaTiposSolicitudes = new SelectList(new ServicioTipoSolicitud().ObtenerTodos(), "TIPOSOLICITUD_ID", "NOMBRE", conceptoTipoSolicitud.TIPOSOLICITUD_ID);
                _listaConceptos = new SelectList(new ServicioConcepto().ObtenerTodos(), "CONCEPTO_ID", "NOMBRE", conceptoTipoSolicitud.CONCEPTO_ID);
            }
            else
            {
                _activo = new SelectList(lista, "Key", "Value");
                _tiposSolicitudes = new MultiSelectList(new ServicioTipoSolicitud().ObtenerTodos(), "TIPOSOLICITUD_ID", "NOMBRE");
                _listaConceptos = new SelectList(new ServicioConcepto().ObtenerTodos(), "CONCEPTO_ID", "NOMBRE");
            }
            _vistaModelo = new ConceptoTipoSolicitudViewModel(conceptoTipoSolicitud, _activo, _listaTiposSolicitudes,_listaConceptos, _tiposSolicitudes);
        }

        public ActionResult Crear()
        {
            IniciarVista(null);
            return View(_vistaModelo);
        }
     
        public ActionResult Editar(int id) {
            CONCEPTOS_TIPOSSOLICITUDES conceptoTipoSolicitud = _servicio.ObtenerPorClave(id);
            if (conceptoTipoSolicitud != null)
            {
                IniciarVista(conceptoTipoSolicitud);
                return View(_vistaModelo);
            }
            else
            {
                TempData["mensaje"] = "Concepto - Tipo solicitud no encontrado.";
                return RedirectToAction("Listar");
            }
        }

        public ActionResult Listar() {
            return View();
        }

        [HttpPost]
        public ActionResult Crear([Bind(Prefix = "CONCEPTOS_TIPOSSOLICITUDES")] CONCEPTOS_TIPOSSOLICITUDES conceptoTipoSolicitud)
        {
            conceptoTipoSolicitud.USUARIO_ID = new ServicioUsuario().ObtenerPorLogin(User.Identity.Name).USUARIO_ID;
            if (ModelState.IsValid) {
                conceptoTipoSolicitud.MULTI_TIPOSOLICITUD_ID = Request["CONCEPTOS_TIPOSSOLICITUDES.TIPOSOLICITUD_ID"].ToString();
                if (_servicio.Guardar(conceptoTipoSolicitud))
                {
                    TempData["mensaje"] = "Concepto - Tipo solicitud Guardado Satisfactoriamente.";
                    return RedirectToAction("Listar");
                }
                else
                    ModelState.AddModelError(string.Empty, "Error Guardando Concepto - Tipo solicitud");                
            }
            IniciarVista(conceptoTipoSolicitud);
            return View(_vistaModelo);

        }

        [HttpPost]
        public ActionResult Editar([Bind(Prefix = "CONCEPTOS_TIPOSSOLICITUDES")] CONCEPTOS_TIPOSSOLICITUDES conceptoTipoSolicitud)
        {
            if (ModelState.IsValid)
            {
                if (!_servicio.Duplicado(conceptoTipoSolicitud))
                {
    
                    conceptoTipoSolicitud.USUARIO_ID = new ServicioUsuario().ObtenerPorLogin(User.Identity.Name).USUARIO_ID;
                    if (_servicio.Modificar(conceptoTipoSolicitud))
                    {
                        TempData["mensaje"] = "Concepto - Tipo solicitud modificado satisfactoriamente";
                        return RedirectToAction("Listar");
                    }
                    else
                        ModelState.AddModelError(string.Empty, "Error modificando Concepto - Tipo solicitud");
                }
                else
                    ModelState.AddModelError(string.Empty, "Concepto - Tipo solicitud duplicado.");

            }
            IniciarVista(conceptoTipoSolicitud);
            return View(_vistaModelo);

        }

       
        [HttpPost]
        public JsonResult Eliminar(int id)
        {
            return Json(_servicio.Eliminar(id));
        }

        [HttpPost]
        public JsonResult ObtenerData(string sidx, string sord, int page, int rows, string filters)
        {
            object resultado = _servicio.ObtenerDataGrid(sidx, sord, page, rows, filters);
            return Json(resultado);

        }

    }
}
