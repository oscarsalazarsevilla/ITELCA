/**
 *  PreviewPaginator v1.1
 *  Author: Lucas Forchino
 *  WebSite: http://www.jqueryload.com
 */
$('document').ready(function(){
    var outer= $('.page-previous-outer') ;
    var preview_pos;
    var preview_els	= $(".page-previous-inner div");
    var div_width	= preview_els.eq(0).width();
    $('.paginator a').bind('mouseover', function(event){
        var link = event.currentTarget;
        var pos= $(link).attr('page');
        pos=(pos-1)*div_width;
        outer.stop().animate( {
            'scrollLeft' : pos
        },300 );
        var left=$(link).position().left;
        left=left+8;
        $("#arrow").stop().animate( {
            'left' : left
        },300 );
    })
    outer.animate( {
        'scrollLeft' : 0
    }, 0 );
    $(".page-previous-inner").css('width', preview_els.length * div_width);
    $("#arrow").css('left', 344);
})