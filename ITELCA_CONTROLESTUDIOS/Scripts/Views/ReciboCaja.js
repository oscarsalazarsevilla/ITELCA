﻿var tablaDetalleM,tablaDetalleP,tablaPago,imprimirOpcion='M',tablaBuscar,tablaMovimiento;//M:MENSUALIDAD

function obtenerCursosAlumno(documento) {
    $.post("/MatriculaCurso/ObtenerCursosPreInscritosPorAlumno", { documento: documento }, function (data) {
        if (data != null) {
            var tam = data.length;
            var lista = "";
            for (var i = 0; i < tam; i++)
                lista += ('<option value="' + data[i].id + '">' + data[i].nombre + '</option>');
            $('#SelectCurso').empty();
            $("#SelectCurso").html(lista);
            $('#SelectCurso option:first-child').attr("selected", "selected");
            obtenerConceptosCurso();
        }
    }, 'json');
}

function obtenerTalleresAlumno(documento) {
    $.post("/MatriculaCurso/ObtenerCursosPreInscritosPorAlumno", { documento: documento }, function (data) {
        if (data != null) {
            var tam = data.length;
            var lista = "";
            for (var i = 0; i < tam; i++)
                lista += ('<option value="' + data[i].id + '">' + data[i].nombre + '</option>');
            $('#SelectCurso').empty();
            $("#SelectCurso").html(lista);
            $('#SelectCurso option:first-child').attr("selected", "selected");
            obtenerConceptosCurso();
        }
    }, 'json');
}

function obtenerConceptosCurso() {
    var id_curso = $("#SelectCurso option:selected").val();
$.post("/ReciboCaja/ObtenerConceptoCurso", { id_curso: id_curso }, function (data) {
        if (data != null) {            
            var tam = data.length;
            var lista = "";
            for (var i = 0; i < tam; i++)
                lista += ('<option value="' + data[i].id + '">' + data[i].nombre + '</option>');
            $('#SelectConceptos').empty();
            $("#SelectConceptos").html(lista);
        }
    }, 'json');
}

function obtenerConceptosTaller() {
    var id_curso = $("#SelectCurso option:selected").val();
    $.post("/ReciboCaja/ObtenerConceptoCurso", { id_curso: id_curso }, function (data) {
        if (data != null) {
            var tam = data.length;
            var lista = "";
            for (var i = 0; i < tam; i++)
                lista += ('<option value="' + data[i].id + '">' + data[i].nombre + '</option>');
            $('#SelectConceptos').empty();
            $("#SelectConceptos").html(lista);
        }
    }, 'json');
}

function obtenerTiposSolicitudes() {
    var lista;
    lista += ('<option value="">Constancia de Notas</option>');
    lista += ('<option value="">Constancia de Estudios</option>');
    lista += ('<option value="">Carta de Pasantias</option>');
    lista += ('<option value="">Cambio de Turno</option>');
    $('#SelectConceptos').empty();
    $("#SelectConceptos").html(lista);
}

function LimpiarCamposConcepto() {
    $("#SelectConceptos").val(0);
    $("#CantidadConcepto").val(1);
}

function CalcularMontoTotalM() {
    var trs = tablaDetalleM.fnGetNodes();
    var cont = 0;
    var tam = trs.length;
    var monto = 0;
    $.each(trs, function () {
        var data = tablaDetalleM.fnGetData(this);
        var check = data[7];
        if (check == "1")
            monto = parseFloat(monto) + parseFloat(data[4]);
    });
  //  $('#RECIBOS_MONTOTOTAL').val('init', { aSep: '', aDec: '.' });
    $("#RECIBOS_MONTOTOTAL").autoNumeric('set', monto);
}
function CalcularMontoTotalP() {
    var trs = tablaDetalleP.fnGetNodes();
    var cont = 0;
    var tam = trs.length;
    var monto = 0;
    $.each(trs, function () {
        var data = tablaDetalleP.fnGetData(this);
            monto = parseFloat(monto) + parseFloat(data[4]);
    });

        $('#RECIBOS_MONTOTOTAL').autoNumeric('init', { aSep: '', aDec: '.' });
        $("#RECIBOS_MONTOTOTAL").autoNumeric('set',monto);
}

function validarChequeado(event,sender,row) {
    //obtengo todos los chequeados de la tabla
    var tam=$("#TablaDetallesMensualidad input:checked").length;
    if(tam<=1){
        if (sender.checked){
            tablaDetalleM.fnUpdate('1', row, 7); // Single cell
             $("#RECIBOS_ABONO").prop("disabled",false);
        }
        else {
            tablaDetalleM.fnUpdate('0', row, 7); // Single cell
            $("#RECIBOS_ABONO").prop("disabled",true);
        }
         $("#RECIBOS_ABONO").autoNumeric('set',0);
    }else{
        event.preventDefault();
        $("#delete-message").text("Solo Un Pago de mensualidad a la vez ");
        $("#dialog-message").dialog({
            modal: true,
            buttons: {
                Ok: function () {
                    $(this).dialog("close");
                }
            }
        });
    }
    CalcularMontoTotalM();
}

function eliminarDetalle(index, event) {
    var anSelected = tablaDetalleP.$('tr.row_delete');
    tablaDetalleP.fnDeleteRow(anSelected[0]);
    CalcularMontoTotalP(); 
    event.preventDefault();
}

function eliminarPago(event) {
    var anSelected = tablaPago.$('tr.row_delete');
    tablaPago.fnDeleteRow(anSelected[0]);
    event.preventDefault();
}



function editarDetalle(index, event) {
    var data = tablaDetalleP.fnGetData(index);
    event.preventDefault();
    $("#SelectConceptos").val(data[0]);
    $("#CantidadConcepto").val(data[3]);
    $("#EditarDetalle").show();
    $("#AgregarDetalle").hide();
}

function ObtenerMontoConcepto(concepto_id,cantidad) {
    var monto;
    $.ajax({
        type: "GET",
        url: "/ReciboCaja/ObtenerMontoLineaDetale",
        data: { id_conceptoCurso: concepto_id, cantidad: cantidad }
    }).done(function (data) {
        return data;
    });
}

function procesar(tabla,urlReporte) {
    var trs = tabla.fnGetNodes();
    var cont = 0;
    var tam = trs.length;
    var lista = "[";
    var ban=false;
    $.each(trs, function () {
        var data = tabla.fnGetData(this);
        if (data[7] == "1")
        {
            if(data[9]!="C" || $("#RECIBOS_ABONO").val()>0){//PARA sacar el recibo de producto
               urlReporte="/Reporte/EmitirReciboCajaProducto?id_recibo=";
                $("#tipoReport").val("PP");
                $("#correlativo").val($("#nroReciboP").val());
            }
            if($("#RECIBOS_ABONO").val()>0)
                data[2]="ABONO "+data[2];
            if (cont > 0)
                lista += ",";
            lista += '{"id_concepto":"' + data[1] + '","nombre_concepto":"' + data[2] + '","cantidad":"' + data[3] + '", "monto":"' + data[4] + '", "tipo": "' + data[9] + '"}';
            cont++;
            if(data[9]!="C")//PARA sacar el recibo de producto
               urlReporte="/Reporte/EmitirReciboCajaProducto?id_recibo=";
        }
    });
    lista += "]";
    $("#detallesRecibo").val(lista);
    // aqui deberia guardar en un campo los datos de la forma de pago
    //falta cambiarlo segun las columnas de la tabla de pagos y agregar un campo hidden para guardar el json
    
    lista = "[";
    cont=0;
    trs=tablaPago.fnGetNodes();
    tam=trs.length;
        $.each(trs, function () {
        var data = tablaPago.fnGetData(this);
        lista += '{"idFormaPago":"' + data[0] + '","descFormapago":"' + data[1] + '","monto":"' + data[2] + '", "idBanco":"' + data[3] + '", "banco": "' + data[4] + '", "numero": "' + data[5] + '"}';
        cont++;
        if (cont < tam)
            lista += ",";
    });
    lista += "]";
     $("#formaPagoRecibo").val(lista);
    $("#RECIBOS_MONTOTOTAL").val($('#RECIBOS_MONTOTOTAL').autoNumeric('get'));
    $("#RECIBOS_ABONO").val($('#RECIBOS_ABONO').autoNumeric('get'));
    $("#RECIBOS_COLETILLA").val();
    $.ajax({
        type: "POST",
        url: "/ReciboCaja/Emitir",
        data: $("#formReciboCaja").serialize()
    }).done(function (data) {
        if (data[0].id != "-1") {
            $("#RECIBOS_RECIBO_ID").val(data[0].id);
            $("#RECIBOS_ABONO").val("0.00");
            $("#RECIBOS_ABONO").prop("disabled",true);
            imprimirRecibo(urlReporte);
        } else {
           alert(data[0].msg);
        }

    });

}
function imprimirRecibo(url) {
    var id_recibo = $("#RECIBOS_RECIBO_ID").val();
    url +=id_recibo;
    var windowName = "Recibo Caja";
    var ventana = window.open(url, windowName, "width=1024,height=768,fullscreen=yes,scrollbars=yes");
    //ventana.resizeTo(screen.width, screen.height);
    ventana.focus();
    limpiarTablas();
    obtenerEstadoCuenta( $("#alumno_id").val());
    obtenerCorrelativo();

}
function obtenerEstadoCuenta(alumnoId){
     $.post("/ReciboCaja/ObtenerEstadoCuenta", { alumno_id: alumnoId}, function (edoCuenta) {
            var ban=false;
             var lista = new Array();
            for (var i = 0; i < edoCuenta.length; i++) {
                var trs = tablaDetalleM.fnGetData().length;
                //var opcionEditar = "<a href='#' onclick='$(this).parent().parent().addClass(\"row_selected\");editarDetalle(" + trs + ",event);'>Editar</a>";
                var opcionEliminar = "<a href='#' onclick='$(this).parent().parent().addClass(\"row_delete\");eliminarDetalle(" + trs + ",event);'>Eliminar</a>";
                var checked, checkedLabel;
                if (edoCuenta[i].vencido == "S" && !ban) {
                    checked = 1;
                    checkedLabel = "checked";
                    ban=true;
                }
                else {
                    checked = 0;
                    checkedLabel = "";
                }
                var opcionCheck = "<input type='checkbox' id='c_" + edoCuenta[i].id_concepto + "' " + checkedLabel + " onclick='validarChequeado(event,this, " + i + ");' />";
                lista[i] = [i, edoCuenta[i].id_concepto,edoCuenta[i].desc_concepto, "1", edoCuenta[i].monto, opcionCheck, "", checked, edoCuenta[i].vencido, edoCuenta[i].tipo];
               
              
                //LimpiarCamposConcepto();
                //lista += '<option value="' + data[i].optionValue + '">' + data[i].optionDisplay + '</option>';
            }
             tablaDetalleM.fnAddData(lista);
            tablaDetalleM.fnSort( [ [3,'asc'] ] );
            CalcularMontoTotalM();
    });

}
function obtenerCorrelativo() {
    $.ajax({
        type: "POST",
        url: "/ReciboCaja/ObtenerCorrelativo",
    }).done(function (data) {
        if (data[0].ok ==1) {
            $("#nroReciboM").val(data[0].pp);
            $("#nroReciboP").val(data[0].pp);
        } else {
            $("#delete-message").text(data[0].msg);
                    $("#dialog-message").dialog({
                        modal: true,
                        buttons: {
                            Ok: function () {
                                $(this).dialog("close");
                            }
                        }
                    });
        }

    });

}
function limpiarTablas(){
     tablaDetalleM.fnClearTable();
     tablaDetalleP.fnClearTable();
     tablaPago.fnClearTable();

}

function limpiarFormaPago(){
    $("#PAGO_FORMAPAGO_ID").val("");
    $("#PAGO_BANCO_ID").val("");
    $("#PAGO_MONTO").val("");
    $("#PAGO_NUMERO").val();
}
function validarPago(){
    var cont = 0;
    var msg="";
    var mensaje="";
    var trs = tablaPago.fnGetNodes();
    var tam = trs.length;
    var monto = 0;
    var total=parseFloat($("#RECIBOS_MONTOTOTAL").autoNumeric('get'));
    $.each(trs, function () {
        var data = tablaPago.fnGetData(this);
            monto = parseFloat(monto) + parseFloat(data[2]);
    });

    if(monto<total)
        msg="Monto recibido es Menor a monto a cancelar"
    else
        if(monto>total)
            msg="Monto Recibido es Mayor a monto a cancelar";

    if(msg!=""){
        $("#delete-message").text(msg);
        $("#dialog-message").dialog({
            modal: true,
            buttons: {
                Ok: function () {
                    $(this).dialog("close");
                }
            }
        });

        return false;
    }else
    return true;

        
}

function validarFormaPago(){
   var trs = tablaPago.fnGetNodes();
    var cont = 0;
    var tam = trs.length;
    var monto = 0;
    var ban=true;
    var idFP=$("#PAGO_FORMAPAGO_ID option:selected").val();
    $.each(trs, function () {
        var data = tablaPago.fnGetData(this);
            if(data[0]==idFP){
                ban=false;
                return false;
            }
    });
    return ban;
        
}
function calcularTotalPagar(){
    var trs = tablaPago.fnGetNodes();
         var tam = trs.length;
         var monto = 0;
         $.each(trs, function () {
            var data = tablaPago.fnGetData(this);
                monto = parseFloat(monto) + parseFloat(data[2]);
        });
        var resta=parseFloat($("#RECIBOS_MONTOTOTAL").autoNumeric('get')-monto);
       return resta;
}

function obtenerAlumno(documento){
        $.post("/Usuario/ObtenerAlumno", { identidad: documento }, function (data) {
            if (data.length > 0) {
                obtenerCorrelativo();
                $("#RECIBOS_NOMBRECLIENTE").val(data[0].nombre + " " + data[0].apellido);
                $("#RECIBOS_DIRECCIONFISCAL").val(data[0].direccion);
                $("#RECIBOS_TELEFONO").val(data[0].telefono1);
                $("#alumno_id").val(data[0].id);
                $("#RECIBOS_DOCUMENTO").val(data[0].cedula);
                $("#DivDetallesRecibo").show();
                $("#btnMovimiento").button("option","disabled",false);
                 $("#btnPenalidad").button("option","disabled",false);
                $(window).scrollTop($("#DivDetallesRecibo").offset().top);

                //Buscamos el estado de cuenta del alumno
                limpiarTablas();
                obtenerEstadoCuenta(data[0].id);

            } else {
                limpiar();
                 $("#btnMovimiento").button("option","disabled",true);
                  $("#btnPenalidad").button("option","disabled",true);
                $("#delete-message").text("Usuario no encontrado.");
                $("#dialog-message").dialog({
                    modal: true,
                    buttons: {
                        Ok: function () {
                            $(this).dialog("close");
                        }
                    }
                });
                $("#DivDetallesRecibo").hide();

            }
        });

}

function busquedaGeneral() {
    var term = $("#buscarGeneral").val();
    if(term!=""){
        $.post("/Usuario/BuscarAlumno", {term:term.toUpperCase()  }, function (data) {
            if (data != null) {
                var tam = data.length;
                var lista = new Array();
                var index=0;
                for (var i = 0; i < tam; i++){
                      opcionSeleccionar = "<a href='#' onclick='$(this).parent().parent().addClass(\"row_selected\");seleccionarAlumno("+data[i].id+","+data[i].cedula+",event);'>Seleccionar</a>";
                    lista[index]= [data[i].id,data[i].cedula,data[i].nombre+" "+data[i].apellido,opcionSeleccionar];
                    index++;
               }
                tablaBuscar.fnClearTable();
                tablaBuscar.fnAddData(lista);
                  $("#DivDetallesRecibo").show();
            }else{
                limpiar();
                 $("#delete-message").text("Alumno no encontrado.");
                    $("#dialog-message").dialog({
                        modal: true,
                        buttons: {
                            Ok: function () {
                                $(this).dialog("close");
                            }
                        }
                    });
                     $("#DivDetallesRecibo").hide();        
            }
        }, 'json');
    }
}
function seleccionarAlumno(id,cedula,event){
    event.preventDefault();
    obtenerAlumno(cedula);
    tablaBuscar.fnClearTable();
    $("#buscarGeneral").val("");
    $("#dialogBuscar").dialog('close');

}

function limpiar(){
      $("input[type='text'],textarea").val("");
        $('.montos').val("0");
       limpiarTablas();
        $(".BuscarUsuario").prop("disabled",false);
         $("#DivDetallesRecibo").hide();

}
function limpiarBusquedaGeneral(){
    $("#buscarGeneral").val("");
     tablaBuscar.fnClearTable();
}

function limpiarInscrito(){
    $("#inscBuscar").val("");
    $("#inscAlumno").val("");
    $("#inscSede").val("");
    $("#inscCurso").val("");
    $("#inscTurno").val("");
    $("#inscModulo").val("");
    $("#cuota").val("");
    $("#inicio").val("");
    $("#incsMonto").autoNumeric('set',0);
}
$(document).ready(function () {
   
    $("#RECIBOS_DOCUMENTO").focus();
    //Chequeamos si hay una caja abierta
    if ($("#cajaAbierta").val() == "0") {
        $("#delete-message").text("Debe abrir la caja antes de poder emitir un recibo. Al cerrar esta ventana será dirigido al proceso de apertura de caja.");
        $("#dialog-message").dialog({
            modal: true,
            buttons: {
                Ok: function () {
                    $(this).dialog("close");
                }
            },
            close: function () {
                location.href = "/ManejoCajas/Crear";
            }
        });
    }

     $("#nroReciboM,#nroReciboP,#Disponibilidad").keypress(function (event) {
        if ((event.ctrlKey == true && (event.which == '118' || event.which == '86')) || (event.which > 0 )) {
            event.preventDefault();
        }
    });

    $("#AgregarProducto").keydown(function(event){
            if(event.keyCode==9){
                event.preventDefault();
                $("#EnviarFormularioP").focus();
            } 
    });

    $("#DivDetallesRecibo").hide();
    $("#EditarDetalle").hide();

    $("#EnviarFormularioM").show();
    $("#EditarFormularioM").hide();
    $("#ImprimirReciboCajaM").hide();

    $('.datepicker').datepicker({ dateFormat: 'dd/mm/yy' })
     $(".datepicker").mask("99/99/9999", { placeholder: "_" });
    $("#RECIBOS_ABONO").val(0);
    $("#CantidadConcepto").val(1);

    $(".lectura").on('click', function (event) {
        event.preventDefault();

    });
    $('.montos').val("0");
    $('.montos').autoNumeric('init', { aSep: '', aDec: '.' });
    $("#TipoCT").val("C");

    $("button:first").button({
        icons: {
            primary: "ui-icon-search"
        }
    }).click(function (event) {
        event.preventDefault();
    });
     $(".NuevoRecibo").button({
        icons: {
            primary: "ui-icon-arrowrefresh-1-e"
        }
    }).click(function (event) {
        event.preventDefault();
    });
     $("#GuardarPago").button({
        icons: {
            primary: "ui-icon-disk"
        }
    }).click(function (event) {
        event.preventDefault();
    });

     $("#inscribir").button({
        icons: {
            primary: "ui-icon-person"
        }
    }).click(function (event) {
        event.preventDefault();
    });
     $("#btnPenalidad").button({
        icons: {
            primary: " ui-icon-arrowthick-1-e"
        }
    }).click(function (event) {
        event.preventDefault();
    });
   

     $(".button").button({
        icons: {
            primary: "ui-icon-print"
        }
    }).click(function (event) {
        event.preventDefault();
    });
     $("#btnMovimiento").button({
        icons: {
            primary: "ui-icon-search"
        }
    }).click(function (event) {
        event.preventDefault();
    });
      $("#inscGuardar").button({
        icons: {
            primary: "ui-icon-disk"
        }
    }).click(function (event) {
        event.preventDefault();
    });

    $(".boton").button();
     $("#btnMovimiento").button("option","disabled",true);
     $("#btnPenalidad").button("option","disabled",false);

    tablaDetalleM = $("#TablaDetallesMensualidad").dataTable({
        "bPaginate": false,
        "bFilter": true,
        "bInfo": false,
        "bJQueryUI": true,
        "bAutoWidth": false,
        "aaSorting": [[ 3, "asc" ]],
        "aoColumns": [
         { "bVisible": false },
        { "bVisible": false },
        { sWidth: '60%',sClass: "descripcion" },
        { sWidth: '10%' },
        { sWidth: '10%',sClass: "numero" },
        { sWidth: '10%' },
        { sWidth: '10%' },
        { "bVisible": false },
        { "bVisible": false },
        { "bVisible": false }
    ],
        "oLanguage": {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }
    });

    tablaDetalleP = $("#TablaDetallesProducto").dataTable({
        "bPaginate": false,
        "bFilter": true,
        "bInfo": false,
        "bJQueryUI": true,
        "bAutoWidth": false,
        "aoColumns": [
         { "bVisible": false },
        { "bVisible": false },
        { sWidth: '60%' },
        { sWidth: '10%' },
        { sWidth: '10%' },
        { "bVisible": false },
        { sWidth: '10%' },
        { "bVisible": false },
        { "bVisible": false },
        { "bVisible": false }
    ],
        "oLanguage": {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }
    });

     tablaPago = $("#TablaPagos").dataTable({
        "bPaginate": false,
        "bFilter": true,
        "bInfo": false,
        "bJQueryUI": true,
        "bAutoWidth": false,
        "aoColumns": [
            { "bVisible": false },//id forma de pago
            { sWidth: '30%' },//forma de pago
            { sWidth: '10%' },//monto,
            { "bVisible": false },//id forma de pago
            { sWidth: '30%' },//monto
            { sWidth: '10%' },//numero
            { sWidth: '10%' }//elminar
        ],
        "oLanguage": {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }
    });

        tablaBuscar = $("#tablaBuscar").dataTable({
        "bPaginate": false,
        "bFilter": true,
        "bInfo": false,
        "bJQueryUI": true,
        "bAutoWidth": false,
        "aoColumns": [
        { sWidth: '10%' },
        { sWidth: '20%' },
        { sWidth: '60%' },
        { sWidth: '10%' }
    ],
        "oLanguage": {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }
    });

    tablaMovimiento = $("#tablaMovimiento").dataTable({
        "bPaginate": false,
        "bFilter": true,
        "bInfo": false,
        "bJQueryUI": true,
        "bAutoWidth": false,
        "aaSorting": [[0, "desc"]],
        "aoColumns": [
        { sWidth: '100px' },
        { sWidth: '200px' },
        { sWidth: '200px' },
        { sWidth: '200px' },
        { sWidth: '300px' },
        { sWidth: '100px' },
        { sWidth: '100px' },
        { sWidth: '100px' }
    ],
        "oLanguage": {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }
    });

    $("#tabRecibo").tabs();

    //dialog para el cliente
    $("#estado_cuenta").click(function () {
        $('#dialog-edocuenta').dialog({
            autoOpen: false,
            width: '85%',
            resizable: true,
            title: 'Estado de cuenta',
            modal: true,
            open: function (event, ui) {
                $("body").addClass("loading");
                $(this).load("/ReciboCaja/EstadoCuenta/" + $("#alumno_id").val());
                $("body").removeClass("loading");
            },
            position: "top",
            buttons: {
                "Cerrar": function () {
                    $(this).dialog("close");
                }
            }
        });
        $('#dialog-edocuenta').dialog('open');

        $("#dialog-confirmacion").dialog({
            autoOpen: false
        });
    });

      $('#dialogBuscar').dialog({
            autoOpen: false,
            width: '55%',
            resizable: true,
            title: 'Buscar',
            modal: true,
            position: "top",
            buttons: {
                 "Cerrar": function () {
                      tablaBuscar.fnClearTable();
                      $('#dialogBuscar').dialog('close');
                }
            },
            close:function(event, ui){
                 tablaBuscar.fnClearTable();
            }
        });
     $('#dialogMovimientos').dialog({
            autoOpen: false,
            width: '55%',
            resizable: true,
            title: 'Movimientos',
            modal: true,
            position: "top",
            buttons: {
                 "Cerrar": function () {
                      tablaMovimiento.fnClearTable();
                      $('#dialogMovimientos').dialog('close');
                }
            },
            close:function(event, ui){
                    tablaMovimiento.fnClearTable();
                    $('#dialogMovimientos').dialog('close');
            }
        });
    $('#dialogPenalidad').dialog({
            autoOpen: false,
            width: '55%',
            resizable: true,
            title: 'Penalidad',
            modal: true,
            buttons: {
                 "Cerrar": function () {
                     $('#dialogPenalidad').dialog('close');
                }
            },
            close:function(event, ui){
                    $('#dialogPenalidad').dialog('close');
            }
        });
    $("#btnPenalidad").click(function () {
         $('#dialogPenalidad').dialog("open");
    });

    $("#btnMovimiento").click(function () {
        var alumno= $("#alumno_id").val();
        if(alumno!=""){
            $.post("/ReciboCaja/ObtenerTransacciones",{nro:$("#RECIBOS_DOCUMENTO").val()},function(data){
                var tam=data.length;
                if(tam>0){
                    var lista = new Array();
                    var index=0;
                    for (var i = 0; i < tam; i++){
                        opcionSeleccionar = "<a href='#' onclick='$(this).parent().parent().addClass(\"row_selected\");seleccionarAlumno("+data[i].id+","+data[i].cedula+",event);'>Seleccionar</a>";
                        lista[index]= [data[i].fecha,data[i].sede,data[i].tipoRecibo,data[i].nroRecibo,data[i].coletilla,data[i].montoTotal,data[i].cajero,data[i].condicion];
                        index++;
                    }
                tablaMovimiento.fnClearTable();
                tablaMovimiento.fnAddData(lista);
                 $('#dialogMovimientos').dialog("open");
            }else{
            }
            },"json");
        }
        
    });
    //dialog forma de pago
     $('#dialogPago').dialog({
            autoOpen: false,
            width: '55%',
            resizable: true,
            title: 'Forma de Pago',
            modal: true,
            position: "top",
            buttons: {
                "Imprimir":function(){
                     //llamo a ventana de confirmar si desea realizar la operacion
                     if(validarPago()){
                        if(imprimirOpcion=="M")
                            $("#confirmar").dialog("open");
                       else
                            $("#confirmarProducto").dialog("open");
                    }
                },
                 "Cerrar": function () {
                      tablaPago.fnClearTable();
                      $('#dialogPago').dialog('close');
                }
            },
            close:function(event, ui){
                 tablaPago.fnClearTable();
            }
        }); 
    $("#confirmar").dialog({
        resizable: false,
        height: 180,
        autoOpen: false,
        modal: true,
        buttons: {
            "Si": function () {
                $("#tipoReport").val("PM");
                $("#correlativo").val($("#nroReciboM").val());
                $(".BuscarUsuario").prop("disabled",true);
                //confirmo que desea realizar operacion
                procesar(tablaDetalleM, "/Reporte/EmitirReciboCaja?id_recibo=");
                $('#dialogPago').dialog('close');
                $(this).dialog("close");
                
            },
            "No": function () {
                $(this).dialog("close");
            }
        }
    });

    $("#confirmarProducto").dialog({
        resizable: false,
        height: 180,
        autoOpen: false,
        modal: true,
        buttons: {
            "Si": function () {
                $("#tipoReport").val("PP");
                $("#correlativo").val($("#nroReciboP").val());
                procesar(tablaDetalleP, "/Reporte/EmitirReciboCajaProducto?id_recibo=");
                $('#dialogPago').dialog('close');
                $(this).dialog("close");
            },
            "No": function () {
                $(this).dialog("close");
            }
        }
    });

    $("#dialogInscribir").dialog({
        resizable: true,
        height: 'auto',
        width:'800px',
        autoOpen: false,
        modal: true,
        buttons: {
            "Cancelar": function () {
                $(this).dialog("close");
            }
        }
    });
    $(".BuscarUsuario").click(function () {
        limpiarBusquedaGeneral();
       $("#dialogBuscar").dialog('open');
    });
    $("#RECIBOS_DOCUMENTO").keydown(function(event){
      if( event.keyCode==13){
             event.preventDefault();
            var documento = $("#RECIBOS_DOCUMENTO").val();
            if (documento != "") {
                obtenerAlumno(documento);
            } else {
                limpiar();
                $("#delete-message").text("Debe ingresar un número de documento válido.");
                $("#dialog-message").dialog({
                    modal: true,
                    buttons: {
                        Ok: function () {
                            $(this).dialog("close");
                        }
                    }
                });
                $("#DivDetallesRecibo").hide();
            }
        }
    });
    
    $("#buscarGeneral").keydown(function(event){
        if( event.keyCode==13){
            event.preventDefault();
            busquedaGeneral();
        }
    });

     $(".NuevoRecibo").click(function () {
       $("input[type='text'],textarea").val("");
        $('.montos').val("0");
       limpiarTablas();
        $(".BuscarUsuario").prop("disabled",false);
         $("#DivDetallesRecibo").hide();
    });

    $("#AgregarProducto").click(function () {
        var concepto_id = $("#Productos option:selected").val();
        var concepto_detalle = $("#Productos option:selected").text();
        var cantidad = $("#CantidadConcepto").val();

        $.ajax({
            type: "GET",
            url: "/ReciboCaja/ObtenerMontoLineaDetale",
            data: { id_conceptoCurso: concepto_id, cantidad: cantidad }
        }).done(function (data) {
            var trs = tablaDetalleP.fnGetData().length;
            var opcionEditar = "<a href='#' onclick='$(this).parent().parent().addClass(\"row_selected\");editarDetalle(" + trs + ",event);'>Editar</a>";
            var opcionEliminar = "<a href='#' onclick='$(this).parent().parent().addClass(\"row_delete\");eliminarDetalle(" + trs + ",event);'>Eliminar</a>";
            var lista = ["1", concepto_id, concepto_detalle, cantidad, data, opcionEditar, opcionEliminar, "1", "1", "P"];
            tablaDetalleP.fnAddData(lista);
            LimpiarCamposConcepto();
            CalcularMontoTotalP();
        });
    });

    $("#EditarDetalle").click(function () {
        var concepto_id = $("#SelectConceptos option:selected").val();
        var concepto_detalle = $("#SelectConceptos option:selected").text();
        var cantidad = $("#CantidadConcepto").val();
        $.ajax({
            type: "GET",
            url: "/ReciboCaja/ObtenerMontoLineaDetale",
            data: { id_conceptoCurso: concepto_id, cantidad: cantidad }
        }).done(function (data) {
            var trs = tablaDetalleP.fnGetData().length;
            var opcionEditar = "<a href='#' onclick='$(this).parent().parent().addClass(\"row_selected\");editarDetalle(" + trs + ",event);'>Editar</a>";
            var opcionEliminar = "<a href='#' onclick='$(this).parent().parent().addClass(\"row_delete\");eliminarDetalle(" + trs + ",event);'>Eliminar</a>";
            var anSelected = tablaDetalleP.$('tr.row_selected');
            if (anSelected.length !== 0) {
                tablaDetalleP.fnUpdate([concepto_id, concepto_detalle, cantidad, data, opcionEditar, opcionEliminar, "1", "1", "P"], anSelected[0]);
                LimpiarCamposConcepto();
                tablaDetalleP.$('tr.row_selected').removeClass('row_selected');
            }

            $("#EditarDetalle").hide();
            $("#AgregarDetalle").show();
            LimpiarCamposConcepto();
            CalcularMontoTotalP();
        });

    });
   
    $("#EnviarFormularioM").click(function (event) {
        event.preventDefault();
        var monto=$("#RECIBOS_MONTOTOTAL").autoNumeric('get');
        imprimirOpcion="M";
        if(parseFloat(monto)>0){
        //llamo al dialogo de pago
            $('#dialogPago').dialog('open');
         }else{
             $("#delete-message").text("Seleccione un Concepto o Mensualidad");
            $("#dialog-message").dialog({
                modal: true,
                buttons: {
                    Ok: function () {
                        $(this).dialog("close");
                    }
                }
            });
         }
       
        //location.href = "/ReciboCaja/Index";
    });

    $("#EnviarFormularioP").click(function (event) {
        event.preventDefault();
        var monto=$("#RECIBOS_MONTOTOTAL").autoNumeric('get');
        imprimirOpcion="P";
        if(parseFloat(monto)>0){
        //llamo al dialogo de pago
            $('#dialogPago').dialog('open');
         }else{
             $("#delete-message").text("Seleccione un Concepto o Mensualidad");
            $("#dialog-message").dialog({
                modal: true,
                buttons: {
                    Ok: function () {
                        $(this).dialog("close");
                    }
                }
            });
         }
    });
    $("#EditarFormularioM").click(function (event) {

        event.preventDefault();
        var trs = tablaDetalleM.fnGetNodes();
        var cont = 0;
        var tam = trs.length;
        var lista = "["; ;
        $.each(trs, function () {
            var data = tablaDetalleM.fnGetData(this);
            lista += '{"id_concepto":"' + data[0] + '","nombre_concepto":"' + data[1] + '","cantidad":"' + data[2] + '"}';
            cont++;
            if (cont < tam)
                lista += ",";
        });
        lista += "]";

        $("#detallesRecibo").val(lista);

        $("#RECIBOS_MONTOTOTAL").val($('#RECIBOS_MONTOTOTAL').autoNumeric('get'));
        $("#RECIBOS_ABONO").val($('#RECIBOS_ABONO').autoNumeric('get'));

        $.ajax({
            type: "POST",
            url: "/ReciboCaja/Editar",
            data: $("#formReciboCaja").serialize()
        }).done(function (data) {
            if (data != "-1") {
                $("#RECIBOS_RECIBO_ID").val(data);
                $("#EnviarFormularioM").hide();
                $("#EditarFormularioM").show();
                $("#ImprimirReciboCajaM").show();
            } else {

            }

        });
    });

    $("input[name='Tipo']").change(function () {

        var documento = $("#RECIBOS_DOCUMENTO").val();
        if ($(this).val() == 'Curso') {
            $("#TipoCT").val("Curso");
            obtenerCursosAlumno(documento);
        } else if ($(this).val() == 'Taller') {
            $("#TipoCT").val("Taller");
            obtenerTalleresAlumno(documento);
        } else {
            $("#TipoCT").val("Solicitud");
            $(".div_cursos_talleres").hide();
            obtenerTiposSolicitudes();
        }
    });

    $("#Productos").change(function () {
        var id = $(this).val();
        if (id != "") {
            $.post("/ReciboCaja/ObtenerDisponibilidad", { inv_item_id: id }, function (data) {
                $("#Disponibilidad").val(data);
            });
        }
    });

    $("#PAGO_MONTO").focus(function(){
         var resta=calcularTotalPagar();
          if(resta<0)
            $(this).autoNumeric('set',0);
        else
             $(this).autoNumeric('set',resta);
    });
     $("#PAGO_NUMERO").keydown(function(event){
         if(event.keyCode==9){
            event.preventDefault();
            $("#GuardarPago").focus();
        }
    });
    $("#GuardarPago").click(function(){
        var monto=$("#PAGO_MONTO").val();
        var idFormadePago=$("#PAGO_FORMAPAGO_ID option:selected").val();
        var descFP=$("#PAGO_FORMAPAGO_ID option:selected").text();
        var idBanco=$("#PAGO_BANCO_ID option:selected").val();
        var descBanco=$("#PAGO_BANCO_ID option:selected").text();
        var numero=$("#PAGO_NUMERO").val();
        var opcionEliminar = "<a href='#' onclick='$(this).parent().parent().addClass(\"row_delete\");eliminarPago(event);'>Eliminar</a>";
        if(idBanco=="")
            descBanco="";
        var lista = [idFormadePago, descFP,monto, idBanco, descBanco,numero,opcionEliminar];
        if(parseFloat(monto)>0 && idFormadePago!=""){
            if(validarFormaPago()){
                tablaPago.fnAddData(lista);
                limpiarFormaPago();
                if(calcularTotalPagar()<=0)
                    $(":button:contains('Imprimir')").focus();
                else
                    $("#PAGO_FORMAPAGO_ID").focus();
            }else{
                $("#delete-message").text("Forma de pago ya ingresada");
                $("#dialog-message").dialog({
                    modal: true,
                    buttons: {
                        Ok: function () {      
                            $(this).dialog("close");
                        }
                    }
                });
            }
        }else{
             $("#delete-message").text("Monto debe ser mayor a Cero( 0.00) o Forma de Pago Vacia");
            $("#dialog-message").dialog({
                modal: true,
                buttons: {
                    Ok: function () {
                        $(this).dialog("close");
                    }
                }
            });
        
        }      

    });

    $("#PAGO_FORMAPAGO_ID").change(function(){
        var valor=$("#PAGO_FORMAPAGO_ID option:selected").val();
        if(valor!="" && valor!="0")
            $("#adicional").show();
        else{
            $("#adicional").hide();
            $("#PAGO_BANCO_ID").val("");
            $("#PAGO_NUMERO").val("");
        }
    
    });


    /*inscripcion en curso recibo*/
      /**Metodos para oferta de cursos**/
    $('#inscCurso').change(function () {
        var idCurso = $("#inscCurso option:selected").val();
        var idSede = $("#inscSede option:selected").val();
        if (idCurso != "") {
            //            $('#divModulos').html('<div align="center"><img src="/Content/Imagenes/cargando.gif" alt="gif"  /></div>');
            $('#inscTurno').html("<select><option>Cargando...</option></select>");
            $.post("/MatriculaCurso/ObtenerTurno", { curso: idCurso, sede: idSede }, function (response) {
                if (response != "") {
                    var s = '<select><option value="">--Seleccione--</option>';
                    if (response && response.length>0) {
                        for (var i = 0; i < response.length; i++) {
                            s += '<option value="' + response[i].key + '">' + response[i].value + '</option>';
                        }
                    }
                    s += "</select>";

                    $('#inscTurno').html(s);
                } else {
                    $('#inscTurno').html('<select><option value="">Sin Turno</option></select>');
                    $('#inscModulo').html('<select><option value="">--Seleccione--</option></select>');
                }
            });
        } else {
            $('#inscTurno').html('<select><option value="">--Seleccione--</option></select>');
        }
    });

    $('#inscSede').change(function () {
        var id = $("#inscSede option:selected").val();
        if (id != "") {
            $('#inscCurso').html('<select><option value="">Cargando...</option></select>');
            $.post("/MatriculaCurso/ObtenerCurso", { sede: id }, function (response) {
                if (response != "") {
                    var s = '<select><option value="">--Seleccione--</option>';
                    if (response && response.length) {
                        for (var i = 0; i < response.length; i++) {
                            s += '<option value="' + response[i].key + '">' + response[i].value + '</option>';
                        }
                    }
                    s += "</select>";
                    $('#inscCurso').html(s);
                } else {
                    $('#inscCurso').html('<select><option value="">Sin Cursos</option></select>');
                }
            });
        } else {
            $('#inscCurso').html('<select><option value="">--Seleccione--</option></select>');
             $('#inscModulo').html('<select><option value="">--Seleccione--</option></select>');
            $('#inscTurno').html('<select><option value="">--Seleccione--</option></select>');
        }

    });

    $('#inscTurno').change(function () {
        //        $('#divModulos').html('<div align="center"><img src="/Content/Imagenes/cargando.gif" alt="gif"  /></div>');
        if ($(this).val() != "") {
            var idCurso = $("#inscCurso option:selected").val();
            var idSede = $("#inscSede option:selected").val();
            $.post("/MatriculaCurso/ObtenerModulos", { cursoId:idCurso, sedeId: idSede, turnoId: $("#inscTurno option:selected").val() },
              function (data) {
                   var lista = '<select><option value="">--Seleccione--</option>';
                  if (data.length > 0) {
                      for (var i = 0; i < data.length; i++) {
                           lista += '<option value="' + data[i].ofertaModuloId + '">' + data[i].nombre +" Profesor:"+data[i].profesor+"  Inicia:"+data[i].inicio +" Finaliza:"+data[0].fin+ '</option>';
                      }
                      $('#inscModulo').html(lista);
                  }
                  else {
                      $('#inscModulo').html('<select><option value="">Sin Modulos Programados</option></select>');
                  }
              });
        }else{
             $('#inscModulo').html('<select><option value="">--Seleccione--</option></select>');
        }
        
    });

    $("#inscribir").click(function(){
        limpiarInscrito();
        $("#dialogInscribir").dialog('open');
    
    });

    $("#incsMonto").focusout(function(){
        if($(this).val()=="")
            $(this).autoNumeric('set',0);
    });
    $("#inscGuardar").click(function(){
         var idCurso = $("#inscCurso option:selected").val();
         var idSede = $("#inscSede option:selected").val();
         var idTurno= $("#inscTurno option:selected").val();
         var idOfertaModulo=$("#inscModulo option:selected").val();
         var idAlumno=$("#inscIdEstudiante").val();
         var inicio=$("#inicio").val();
         var cuota=$("#cuota").val();
         var monto=$("#incsMonto").autoNumeric('get');
         if(idCurso!="" && idSede!="" && idTurno!="" && idOfertaModulo!="" && idAlumno!="" && cuota!="" && inicio!="" && monto!=""){
             var resp=confirm("¿Desea Pre-Inscribir Alumno?");
             if(resp){
                var nroControl=$("#nroControl").val();
                $.post("/MatriculaCurso/InscribirAlumnoRecibo",{idAlumno: idAlumno,idSede:idSede,idCurso:idCurso,idTurno:idTurno, idOfertaModulo:idOfertaModulo,cuota:cuota,inicio:inicio,auxMonto:monto,codigo:nroControl},function(data){
                         if(data[0].ok==1){
                            limpiarTablas();
                            obtenerAlumno($("#inscBuscar").val());
                            limpiarInscrito();
                             $("#dialogInscribir").dialog('close');
                        }else{
                             $("#delete-message").text(data[0].msg);
                             $("#dialog-message").dialog({
                                modal: true,
                                buttons: {
                                    Ok: function () {
                                        $(this).dialog("close");
                                    }
                                }
                            });
                        }
                });
            }
        }else{
            if(idAlumno=="")
                $("#delete-message").text("Alumno No Registrado en el sistema, Por favor Vaya a Inscripción Asistida");
            else
                $("#delete-message").text("Ingrese Todos Los Datos del Formulario.");
           
                    $("#dialog-message").dialog({
                        modal: true,
                        buttons: {
                            Ok: function () {
                                $(this).dialog("close");
                            }
                        }
                    });
        }
    });

    $("#inscBuscar").keydown(function(event){
        if( event.keyCode==13){
            event.preventDefault();
             $.post("/Usuario/ObtenerAlumno", { identidad: $(this).val() }, function (data) {
                if (data.length > 0) {
                        $("#inscAlumno").val(data[0].nombre + " " + data[0].apellido);
                        $("#inscIdEstudiante").val(data[0].id);
                } else {
                     $("#inscIdEstudiante").val("");
                    $("#delete-message").text("Estudiante no encontrado");
                     $("#inscAlumno").val("");
                    $("#dialog-message").dialog({
                        modal: true,
                        buttons: {
                            Ok: function () {
                                $(this).dialog("close");
                            }
                        }
                    });
                }
            });
        }
    });

    $("#RECIBOS_ABONO").focusout(function(){
        var monto=$(this).autoNumeric('get');
        if(monto>0)
            $("#RECIBOS_MONTOTOTAL").autoNumeric('set',monto);
        else
            CalcularMontoTotalM();
    });

    
	    tableprincipalfactura = $("#tablaCaja").dataTable({
        "bPaginate": false,
        "bFilter": false,
        "bInfo": false,
        "sScrollX": "100%",
        "bRetrieve": true,
        "bJQueryUI": true,
        "aoColumns": [
                 { "bSearchable": false,
                     "sWidth": '5%'
                 },
                 { "bSearchable": false,
                     "bVisible": false,
                     "sWidth": '0%'
                 },
                 { sWidth: '50%' },
                 { sWidth: '6%' },
                 { sWidth: '5%' },
                 { sWidth: '5%' },
                 { sWidth: '5%' },
                 { sWidth: '5%' },
                 { sWidth: '5%' },
                 { sWidth: '5%' },
                 { sWidth: '5%' },
                 { sWidth: '5%' }
                 ]
    });
    $('#factura_listadetalle tbody td img').on('click', function () {
        //alert("asda");
        var nTr = this.parentNode.parentNode;
        if (this.src.match('cerrar')) {
            /* This row is already open - close it */
            this.src = "/Content/images/detalle.png";
            tableprincipalfactura.fnClose(nTr);
        }
        else {
            /* Open this row */
            if (this.src.match('detalle')) {
                this.src = "/Content/images/cerrar.png";
                var linea = $(this).attr("rel");
                $.get("/Facturacion/DetalleLineaFactura?id=" + linea + "&consecutivo=" + $("#preDocumentos").val(), function (data) {

                    tableprincipalfactura.fnOpen(nTr, data, 'details');
                });
            } else {
                var nTr2 = this.parentNode.parentNode.parentNode.parentNode.id;
                if (this.src.match('minPago')) {
                    var pago = $(this).attr("rel");
                    this.src = "/Content/images/infoPago.png";
                    $("#" + nTr2).dataTable().fnClose(nTr);
                }
                else {
                    if (this.src.match('infoPago')) {
                        this.src = "/Content/images/minPago.png";
                        var pago = $(this).attr("rel");
                        $.get("/Facturacion/DetalleOrdenPago?id=" + pago, function (data2) {
                            $("#" + nTr2).dataTable().fnOpen(nTr, data2, 'details2');
                        });
                    }
                }
            }
        }
    });

    $("#inicio").focusout(function(){
        var val=$(this).val();
        if(val!="")
            if(!existeFecha(val)){
                $(this).val("");
                $("#delete-message").text("FECHA NO EXISTE EN CALENDARIO:"+val);
                    $("#dialog-message").dialog({
                        modal: true,
                        buttons: {
                            Ok: function () {
                                $(this).dialog("close");
                            }
                        }
                    });
            }
    });

    //penalidades
    $("#penTipo").change(function(){
        var val=$(this).val();
        var alumno=$("#RECIBOS_DOCUMENTO").val();
        if(val!="" && alumno!=""){
            var url="";
            if(val=="1")
                url="/MatriculaCurso/ObtenerCursosInscritosPorAlumno";
            else
                url="/MatriculaTaller/ObtenerTallerInscritosPorAlumno"
            $.post(url,{documento:alumno},function(data){
                var lista='<option value="">Seleccione</option>';
                var tam=data.length;
                if(tam>0){
                    for(var i=0;i<tam;i++){
                        lista+='<option value="'+data[i].id+'">'+data[i].nombre+'</option>';
                    }
                    $("#penMatricula").html(lista);
                }else{
                     $("#penMatricula").html('<option value="">No Tiene '+$("#penTipo option:selected").text()+' Inscritos</option>');
                }
            });
        }
    });

     $("#penGuardar").click(function(){
        var monto=$("#penMonto").autoNumeric('get');
        var idTipo=$("#penTipo option:selected").val();
        var idMatricula=$("#penMatricula option:selected").val();
        var desc=$("#penDescripcion").val();
        var alumnoId=$("#alumno_id").val();
       if(idTipo!="" && idMatricula!="" && desc!="" && alumnoId!="" && monto>0){
            $.post("/ReciboCaja/GuardarPenalidad",{tipo:idTipo,alumno:alumnoId,descripcion:desc,concepto:idMatricula,monto:monto},function(data){
                $("#delete-message").text(data[0].msg);
                $("#dialog-message").dialog({
                    modal: true,
                    buttons: {
                        Ok: function () {      
                            $(this).dialog("close");
                        }
                    }
                });
            
            });
       }else{
         $("#delete-message").text("Llene todos los campos por favor");
                $("#dialog-message").dialog({
                    modal: true,
                    buttons: {
                        Ok: function () {      
                            $(this).dialog("close");
                        }
                    }
                });
       }
      

    });

    $("#penMonto").focusin(function(){
        if($(this).autoNumeric('get')==0)
            $(this).val("");
    });
     $("#penMonto").focusout(function(){
        if($(this).autoNumeric('get')==0)
            $(this).autoNumeric('set',0);
    });
});
