﻿var listaAsistencia;
var identificador = new Array();
var estado = new Array();

function marcarAsistencia() {
    if (identificador.length > 0) {
        var data = JSON.stringify({ nota: identificador, valor: estado });
        return $.ajax({
            type: "POST",
            url: "/NotaModulo/GuardarNota",
            contentType: "application/json; charset=utf-8",
            data: data,
            dataType: "json",
            success: function (result) {
                refrescarTabla();
                identificador.length = 0;
                estado.length = 0;
                alert('Nota Guardada');
            },
            error: function (result) {
                alert('Error');
            }
        });
    }
    else
        return false;
}
function agregarCambio(id, asistio) {
    var enc = 0;
    for (var i = 0; i < identificador.lenght && enc == 0; i++) {
        if (identificador[i] == id) {
            enc = 1;
            identificador.splice(i, 1);
            estado.splice(i, 1);
        }
    }

    identificador.push(id);
    estado.push(asistio);
}
function refrescarTabla() {
    if (listaAsistencia != null) {
        listaAsistencia.fnDestroy();
        $('#tablaNota').empty();
    }
    var oferta = $("#curso option:selected").val();
    var resp = $("#modulo option:selected").val();
    var numCol = 2;
    var i = 0;
    var clases = 0;
    var tam = 0;
    var cont = 0;
    var tabla = '<thead><tr><th>Alumno</th><th>Nota</th></tr></tr></thead>';
    if (resp != "") {
        var idSede = $("#sede option:selected").val();
        var idCurso = $("#curso option:selected").val();
        var idTurno = $("#turno option:selected").val();
        var idModulo = $("#modulo option:selected").val();
        var profesor = $("#idProfesor").val();

        if (profesor == "NO") {
            profesor = $("#profesor option:selected").val();
        }
        
        $.post("/NotaModulo/ObtenerTabla", { sede: idSede, curso: idCurso, turno: idTurno, modulo: idModulo ,profesor:profesor}, function (data) {
            tam = data.length;
            if (tam > 0) {
                tabla += '<tbody>'
                while (i < tam && data[i].tipo == "detalle") {
                    if (cont == 0)
                        tabla += '<tr><td>' + data[i].nombre + '</td>';
                    tabla += '<td><div style="text-align: center;"><input type="text" id="' + data[i].id + '"  value="' + data[i].nota + '" maxlength="3"/></div></td></tr>';

                    i++;

                }
            }
            tabla += "</tbody>";
            $('#tablaNota').append(tabla);
            inicializar();
        });
    } else {
        tabla += "<tbody></tbody>";
        $('#tablaNota').append(tabla);
        inicializar();
    }
}

function limpiarTabla() {
    /*var tabla = '<thead><tr><th>Alumno</th><th>Nota</th></tr></tr></thead>';
    tabla += "<tbody></tbody>";
    $('#tablaNota').append(tabla); **/
    $('#tablaNota').empty();

}
function inicializar() {
    listaAsistencia = $('#tablaNota').dataTable({
        "bPaginate": false,
        "bLengthChange": false,
        "bFilter": false,
        "bSort": true,
        "bJQueryUI": true,
        "sScrollY": "150px",
        "aaSorting": [[0, "desc"]],
        "bInfo": false,
        "bAutoWidth": false,
        "bSort": false,
        "aoColumns": [
                { "sWidth": "70%" },
                { "sWidth": "30%" },
                ],
        "oLanguage": {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }
    });
}
$(document).ready(function () {
    inicializar();
    $(document).on('keypress', "input:text", function (event) {
        if ((event.ctrlKey == true && (event.which == '118' || event.which == '86')) || ((event.charCode > 0 && event.charCode < 48) || event.charCode > 57)) {
            event.preventDefault();
        }


    });

    $('#sede').change(function () {
        var idSede = $("#sede option:selected").val();
        var profesor = $("#idProfesor").val();
        if (idSede != "") {
            $('#curso').html("<select><option>Cargando...</option></select>");
            $('#turno').html("<select><option>--Seleccione--</option></select>");
            $('#modulo').html("<select><option>--Seleccione--</option></select>");
            $('#profesor').html("<select><option>--Selecciones--</option></select>");
            limpiarTabla();
            $.post("/NotaModulo/ObtenerCursosEnOfertaModulo", { sede: idSede, profesor: profesor }, function (response) {

                if (response != "") {
                    var s = '<select><option>--Seleccione--</option>';
                    if (response && response.length) {
                        for (var i = 0; i < response.length; i++) {
                            s += '<option value="' + response[i].key + '">' + response[i].value + '</option>';
                        }
                    }
                    s += "</select>";

                    $('#curso').html(s);
                } else {
                    $('#curso').html("<select><option>Sin Cursos Activos</option></select>");
                }
            });
        } else {
            $('#sede').html("<select></select>");
        }

    });


    $('#curso').change(function () {
        var idSede = $("#sede option:selected").val();
        var idCurso = $("#curso option:selected").val();
        var profesor = $("#idProfesor").val();
        if (idSede != "" && idCurso != "") {
            $('#turno').html("<select><option>Cargando...</option></select>");
            $('#modulo').html("<select><option>--Seleccione--</option></select>");
            $('#profesor').html("<select><option>--Selecciones--</option></select>");
            limpiarTabla();

            $.post("/NotaModulo/ObtenerTurnosEnOfertaModulo", { sede: idSede, curso: idCurso, profesor: profesor }, function (response) {
                if (response != "") {
                    var s = '<select><option>--Seleccione--</option>';
                    if (response && response.length) {
                        for (var i = 0; i < response.length; i++) {
                            s += '<option value="' + response[i].key + '">' + response[i].value + '</option>';
                        }
                    }
                    s += "</select>";

                    $('#turno').html(s);
                } else {
                    $('#turno').html("<select><option>Sin Turnos Activos</option></select>");
                }
            });
        } else {
            $('#curso').html("<select></select>");
        }
    });


    $('#turno').change(function () {
        var idSede = $("#sede option:selected").val();
        var idCurso = $("#curso option:selected").val();
        var idTurno = $("#turno option:selected").val();
        var profesor = $("#idProfesor").val();
        if (idSede != "" && idCurso != "" && turno != "") {
            $('#modulo').html("<select><option>Cargando...</option></select>");
            $('#profesor').html("<select><option>--Selecciones--</option></select>");
            limpiarTabla();
            $.post("/NotaModulo/ObtenerModulosEnOfertaModulo", { sede: idSede, curso: idCurso, turno: idTurno, profesor: profesor }, function (response) {
                if (response != "") {
                    var s = '<select><option>--Seleccione--</option>';
                    if (response && response.length) {
                        for (var i = 0; i < response.length; i++) {
                            s += '<option value="' + response[i].id + '">' + response[i].nombre + '</option>';
                        }
                    }
                    s += "</select>";

                    $('#modulo').html(s);
                } else {
                    $('#modulo').html("<select><option>Sin Módulos Disponibles</option></select>");
                }
            });
        } else {
            $('#turno').html("<select></select>");
        }
    });

    $('#modulo').change(function () {
        var idSede = $("#sede option:selected").val();
        var idCurso = $("#curso option:selected").val();
        var idTurno = $("#turno option:selected").val();
        var idModulo = $("#modulo option:selected").val();
        var profesor = $("#idProfesor").val();

        if (idSede != "" && idCurso != "" && turno != "" && idModulo != "") {
            if (profesor == "NO") {
                $('#profesor').html("<select><option>Cargando...</option></select>");
                limpiarTabla();
                $.post("/NotaModulo/ObtenerProfesoresPorModulo", { sede: idSede, curso: idCurso, turno: idTurno, modulo: idModulo }, function (response) {
                    if (response != "") {
                        var s = '<select><option>--Seleccione--</option>';
                        if (response && response.length) {
                            for (var i = 0; i < response.length; i++) {
                                s += '<option value="' + response[i].id + '">' + response[i].nombre + '</option>';
                            }
                        }
                        s += "</select>";

                        $('#profesor').html(s);
                    } else {
                        $('#profesor').html("<select><option>Sin Turnos Activos</option></select>");
                    }
                });
            } else {
                refrescarTabla();   
            }
        } else {
            $('#modulo').html("<select></select>");
        }
    });


    /*
    $("#curso").change(function () {
    var resp = $(this).find("option:selected").val();
    if (resp != "") {

    $.post("/NotaModulo/obtenerModulo", { idOfertaCurso: resp }, function (data) {
    var tam = data.length;
    var lista = '<option value="">--Seleccione--</option>"';
    for (var i = 0; i < tam; i++)
    lista += '<option value="' + data[i].id + '">' + data[i].nombre + ' </option>';

    $("#modulo").html(lista);
    });
    }
    }); */

    $("#profesor").change(function () {
        refrescarTabla();
    });

    $("#guardar").click(function () {
        $('table input[type="text"]').each(function (index) {
            agregarCambio($(this).attr("id"), $(this).val());
        });
        marcarAsistencia();
    });
});

    