﻿var colSede, colAula, colTurno, colModulo, colFechaFin, colProximo, colProfesor;

//para el grid
function calcularFechaModulo(feIni, idM, idT, colFechaFin, colProximo) {
    $.post("/OfertasModulos/CalcularFecha", { fechaInicio: feIni, idModulo: idM, idTurno: idT }, function (data) {
        if (data != "") {
            $(colFechaFin).val(data[0].inicio);
            $(colProximo).val(data[0].fin);

            $(colFechaFin).focus();
        } else {
            alert("Error al calcular Fecha Fin");
        }
    });
} 
//para el formulario
function calcularFecha(feIni, idM, idT) {

    $.post("/OfertasModulos/CalcularFecha", { fechaInicio: feIni, idModulo: idM, idTurno: idT }, function (data) {
        if (data[0].ok == 1) {
            $("#OFERTASMODULOS_FECHA_FIN").val(data[0].inicio);
            $("#OFERTASMODULOS_PROXIMO_INICIO").val(data[0].fin);
            $("#OFERTASMODULOS_PROXIMO_INICIO").focus();
        } else {
            alert(data[0].msg);
        }
    });
}

function deshabilitarDias(date) {
    var day = date.getDay();
    var mes = parseInt(date.getMonth() + 1);
    var diaMostrar = date.getDate() + "/" + mes + "/" + date.getFullYear();
    var resp = false;
    var esFeriado = false;
    for (var i = 0; i < dias.length; i++)
        if (day == dias[i]) {
            resp = true;
            break;
        }

    for (var j = 0; j < feriado.length; j++) {
        var res = feriado[j].split("/");
        if (date.getDate() == parseInt(res[0]) && mes == parseInt(res[1])) {
            esFeriado = true;
            break;
        }

    }
    if (esFeriado)
        return [false, "Highlighted", "Feriado"];
    else
        return [resp, ""];

}
$(document).ready(function () {

    $('.datepickerM').datepicker({
        dateFormat: 'dd/mm/yy',
        yearRange: "-1:+10",
        numberOfMonths: 2,
        minDate: -60,
        beforeShowDay: deshabilitarDias
    });

    $('.datepicker').datepicker({
        dateFormat: 'dd/mm/yy',
        yearRange: "-1:+10",
        numberOfMonths: 2,
        altFormat: "dd-mm-yy",
        altField: "#alt-date",
        minDate: -60
    });


    $.post("/OfertaCurso/ObtenerFechaMaxProximoInicio/", { ofertaCursoId: $("#OFERTASMODULOS_OFERTA_CURSO_ID").val() }, function (data) {
        var prox = -60;
        if (data != "")
            prox = data;
        $('.datepickerMA').datepicker({
            dateFormat: 'dd/mm/yy',
            yearRange: "-1:+10",
            numberOfMonths: 2,
            minDate: prox,
            beforeShowDay: deshabilitarDias
        });

    });


    $('.datepicker').mask("99/99/9999");
    $('.numerico').mask("99");

    $('#cursoGrid').change(function () {
        jQuery("#OfertasModulosGrid").trigger("reloadGrid");
    });
    $('#OFERTASMODULOS_FECHA_INICIO').change(function () {
        calcularFecha($(this).val(), $("#OFERTASMODULOS_MODULO_ID option:selected").val(), $("#OFERTASMODULOS_TURNO_ID").val());
    });

    $('#OFERTASMODULOS_CURSO_ID').change(function () {
        $('#OFERTASMODULOS_MODULO_ID').html("<select><option>Cargando...</option></select>");
        $.post("/OfertasModulos/ObtenerModulo", { curso: $("#cursoGrid option:selected").val() }, function (data) {
            if (data != "")
                $('#OFERTASMODULOS_MODULO_ID').html(data);
            else
                $('#OFERTASMODULOS_MODULO_ID').html("<select><option>Sin Modulos</option></select>");

        });

    });

    $('#OFERTASMODULOS_SEDE_ID').change(function () {
        $('#OFERTASMODULOS_AULA_ID').html("<select><option>Cargando...</option></select>");
        $.post("/OfertasModulos/ObtenerAulaPorSede", { sede: $("#OFERTASMODULOS_SEDE_ID option:selected").val() }, function (data) {
            if (data != "")
                $('#OFERTASMODULOS_AULA_ID').html(data);
            else
                $('#OFERTASMODULOS_AULA_ID').html("<select><option>Sin Aulas</option></select>");

        });

    });

    $("#formOfertaModulo").submit(function (event) {
        event.preventDefault();
        var form = $("#formOfertaModulo").serialize();
        var curso_id = $("#OFERTASMODULOS_CURSO_ID").val();
        $.validator.unobtrusive.parse("#formOfertaModulo");
        if ($("#formOfertaModulo").valid()) {
            var oferta_curso_id = $("#OFERTASMODULOS_OFERTA_CURSO_ID").val();
            $.post("/OfertaCurso/Modulo", form, function (data) {
                {
                    $("#dialog-message").empty().append(data[0].mensaje);
                    $("#dialog-message").dialog({
                        modal: true,
                        buttons: {
                            Ok: function () {
                                $(this).dialog("close");
                            }
                        }
                    });
                    $("select").val("");
                    $("input[type=text]").val("");
                    jQuery("#OfertasModulosGrid").trigger("reloadGrid");
                }
            });
        }
    });

    function reload(rowid, result) {
        jQuery("#OfertasModulosGrid").trigger("reloadGrid");
    }
    resetStatesValues = function () {
        // set 'value' property of the editoptions to initial state
        gridModulo.jqGrid('setColProp', 'Aula', { editoptions: { dataUrl: "/OfertasModulos/ObtenerSelect"} });
    };
    var lastSelection, gridModulo = jQuery("#OfertasModulosGrid");
    gridModulo.jqGrid({
        url: '/OfertasModulos/ObtenerData',
        mtype: 'POST',
        postData: { id: cursoOfertado, profId: function () { var prof = $("#buscarGridProfesor option:selected").val(); return prof; } },
        datatype: 'json',
        colNames: ['Id', 'Sede', 'Turno', 'Aula', 'Modulo', 'Fecha Inicio', 'Fecha Fin', 'Matricula', 'Profesor', 'Eliminar'],
        colModel: [
			    { name: 'OFERTA_MODULO_ID',
			        index: 'OFERTA_MODULO_ID',
			        editable: true,
			        editrules: { edithidden: true, required: false },
			        hidden: true,
			        width: 40
			    },
			    {
			        hidden: true,
			        name: 'SEDE_ID',
			        index: 'SEDE_ID',
			        editable: true,
			        editrules: { edithidden: true, required: false },
			        edittype: 'select',
			        //formatter: 'select',
			        editoptions: {
			            dataInit: function (elem) { colSede = elem; $(elem).html("<select><option value=''>Cargando...</option></select>"); },
			            dataUrl: "/OfertasModulos/ObtenerSede",
			            buildSelect: function (data) {
			                var response = jQuery.parseJSON(data);
			                var s = '<select>';
			                if (response && response.length) {
			                    for (var i = 0, l = response.length; i < l; i++) {
			                        s += '<option value="' + response[i].key + '">' + response[i].value + '</option>';
			                    }
			                }
			                var cm = $("#OfertasModulosGrid").jqGrid('getColProp', 'SEDE_ID');
			                $(colSede).empty();
			                return s + "</select>";
			            },
			            dataEvents: [{
			                type: 'change',
			                fn: function (e) {
			                    var sedeId = $(e.target).val(),
                                row = $(e.target).closest('tr.jqgrow'),
                                form = $(e.target).closest('form.FormGrid'),
                                evento = $(e.target);
			                    if ($(e.target).is('.FormElement'))
			                    // si estoy editando desde el formulario
			                        $("select#AULA_ID.FormElement", form[0]).html("<select><option value=''>Cargando...</option></select>");
			                    else
			                    // si estoy editando en la misma linea
			                        $("select#" + $.jgrid.jqID(row.attr('id')) + "_AULA_ID", row[0]).html("<select><option value=''>Cargando...</option></select>");

			                    $.get('/Aula/ObtenerListaAula', { idSede: "20" }, function (data2) {
			                        if ($(e.target).is('.FormElement'))
			                            $("select#AULA_ID.FormElement", form[0]).html(data2);
			                        else
			                            $("select#" + $.jgrid.jqID(row.attr('id')) + "_AULA_ID", row[0]).html(data2);
			                    });
			                }
			            }]
			        },
			        width: 300
			    },
                 {
                     hidden: true,
                     name: 'TURNO_ID',
                     index: 'TURNO_ID',
                     editrules: { edithidden: true, required: false },
                     edittype: 'select',
                     //formatter: 'select',
                     editoptions: {
                         dataInit: function (elem) { colTurno = elem; $(elem).html("<select><option value=''>Cargando...</option></select>"); },
                         dataUrl: "/OfertasModulos/ObtenerTurno",
                         buildSelect: function (data) {
                             var response = jQuery.parseJSON(data);
                             var s = '<select>';
                             if (response && response.length) {
                                 for (var i = 0, l = response.length; i < l; i++) {
                                     s += '<option value="' + response[i].key + '">' + response[i].value + '</option>';
                                 }
                             }
                             $(colTurno).empty();
                             return s + "</select>";
                         }
                     },
                     width: 250
                 },
                 {
                     name: 'AULA_ID',
                     index: 'AULA_ID',
                     editable: true,
                     editrules: { required: true },
                     edittype: 'select',
                     //formatter: 'select',
                     editoptions: {
                         dataInit: function (elem) { colAula = elem; $(elem).html("<select><option value=''>Cargando...</option></select>"); },
                         dataUrl: "/OfertasModulos/ObtenerAula?sede=" + $("#OFERTASMODULOS_SEDE_ID").val(),
                         buildSelect: function (data) {
                             var response = jQuery.parseJSON(data);
                             var s = '<select>';
                             if (response && response.length) {
                                 for (var i = 0, l = response.length; i < l; i++) {
                                     s += '<option value="' + response[i].key + '">' + response[i].value + '</option>';
                                 }
                             }
                             $(colAula).empty();
                             return s + "</select>";
                         }
                     }

                 },
                  {
                      name: 'MODULO_ID',
                      index: 'MODULO_ID',
                      editable: true,
                      editrules: { required: true },
                      edittype: 'select',
                      //formatter: 'select',
                      editoptions: {
                          dataInit: function (elem) { colModulo = elem; $(elem).html("<select><option value=''>Cargando...</option></select>"); },
                          dataUrl: "/OfertasModulos/ObtenerModuloGrid?curso=" + $("#OFERTASMODULOS_CURSO_ID").val(),
                          buildSelect: function (data) {
                              var response = jQuery.parseJSON(data);
                              var s = '<select>';
                              if (response && response.length) {
                                  for (var i = 0, l = response.length; i < l; i++) {
                                      s += '<option value="' + response[i].key + '">' + response[i].value + '</option>';
                                  }
                              }
                              $(colModulo).empty();
                              return s + "</select>";
                          }
                      },
                      width: 200
                  },
                 { name: 'FECHA_INICIO',
                     index: 'FECHA_INICIO',
                     width: 105,
                     editable: true,
                     formatter: 'date', formatoptions: { srcformat: 'd-m-Y', newformat: 'd/m/Y' },
                     searchoptions: { dataInit: function (elem) {
                         $(elem).datepicker({
                             changeYear: true,
                             changeMonth: true,
                             showButtonPanel: true,
                             onSelect: function () {
                                 if (this.id.substr(0, 3) === "gs_") {
                                     // in case of searching toolbar
                                     setTimeout(function () {
                                         myGrid[0].triggerToolbar();
                                     }, 50);
                                 } else {
                                     // refresh the filter in case of
                                     // searching dialog
                                     $(this).trigger('change');
                                 }
                             }
                         });
                     }
                     },
                     editrules: { required: true, date: true },
                     editoptions: {
                         dataInit: function (element) {
                             $(element).datepicker({
                                 dateFormat: 'dd/mm/yy',
                                 yearRange: "0:+10",
                                 numberOfMonths: 2,
                                 minDate: element.value,
                                 autoSize: true,
                                 changeYear: false,
                                 changeMonth: true,
                                 showButtonPanel: true,
                                 beforeShowDay: deshabilitarDias,
                                 onClose: function (dateText, inst) {
                                     $(this).focus();
                                     //  calcularFechaModulo(dateText, $(colModulo).find("option:selected").val(), $("#OFERTASMODULOS_TURNO_ID").val(), colFechaFin, colProximo);
                                 }
                             });
                         }
                     }

                 },
                   { name: 'FECHA_FIN',
                       index: 'FECHA_FIN',
                       width: 105,
                       editable: true,
                       searchoptions: { dataInit: function (elem) {
                           $(elem).datepicker({
                               changeYear: true,
                               changeMonth: true,
                               showButtonPanel: true,
                               onSelect: function () {
                                   if (this.id.substr(0, 3) === "gs_") {
                                       // in case of searching toolbar
                                       setTimeout(function () {
                                           myGrid[0].triggerToolbar();
                                       }, 50);
                                   } else {
                                       // refresh the filter in case of
                                       // searching dialog
                                       $(this).trigger('change');
                                   }
                               }
                           });
                       }
                       },
                       formatter: 'date', formatoptions: { srcformat: 'd-m-Y', newformat: 'd/m/Y' },
                       editrules: { required: true, date: true },
                       editoptions: {
                           dataInit: function (element) {
                               colFechaFin = element;
                               //alert(element.value);
                               $(element).datepicker({
                                   dateFormat: 'dd/mm/yy',
                                   yearRange: "-1:+10",
                                   numberOfMonths: 2,
                                   minDate: element.value,
                                   autoSize: true,
                                   changeYear: false,
                                   changeMonth: true,
                                   showButtonPanel: true,
                                   beforeShowDay: deshabilitarDias,
                                   onClose: function (dateText, inst) {
                                       $(colProximo).focus();
                                   }
                               });
                           }
                       }

                   },
                     { name: 'MATRICULA_MINIMA',
                         index: 'MATRICULA_MINIMA',
                         editable: true,
                         width: 80,
                         align: 'right',
                         sortable: false//,
                         //cellattr: function (rowId, tv, rawObject, cm, rdata) { return 'class="BankTranEdit"' },
                         //formatter: 'currency',
                         //formatoptions: { decimalSeparator: '.', thousandsSeparator: ',', decimalPlaces: 2, defaultValue: '&nbsp;' } 
                     },
                      {
                          name: 'oferta.USUARIOS.NOMBRE+" "+oferta.USUARIOS.APELLIDO,',
                          index: 'oferta.USUARIOS.NOMBRE+" "+oferta.USUARIOS.APELLIDO,',
                         editable: true,
                         editrules: { required: true },
                         edittype: 'select',
                         //formatter: 'select',
                         editoptions: {
                             dataInit: function (elem) { colProfesor = elem; $(elem).html("<select><option value=''>Cargando...</option></select>"); },
                             dataUrl: "/OfertasModulos/ObtenerProfesor",
                             buildSelect: function (data) {
                                 var response = jQuery.parseJSON(data);
                                 var s = '<select>';
                                 if (response && response.length) {
                                     for (var i = 0, l = response.length; i < l; i++) {
                                         s += '<option value="' + response[i].key + '">' + response[i].value + '</option>';
                                     }
                                 }
                                 $(colProfesor).empty();
                                 return s + "</select>";
                             }
                         }
                         
                         
                          

                      },
                    { name: 'Action2',
                        index: 'Action2',
                        align: "left",
                        width: 50,
                        resizable: false,
                        sortable: false,
                        search: false
                    }
		    ],
        pager: '#pagerModulo', //jQuery('#pager'),
        rowNum: 500,
        rowList: [500, 1000, 5000],
        editurl: "/OfertasModulos/GuardarFila", // 'clientArray'
        sortname: 'FECHA_INICIO',
        sortorder: "asc",
        viewrecords: true,
        // width: 450,
        height: 300, //'auto', //450,
        onCellSelect: function (rowid, iCol, cellContent, e) {

            //                     if (lastSelection) {
            //                         guardar(lastSelection);
            //                     }

        },
        ondblClickRow: function (rowid, iCol, cellContent, e) {
            var date = new Date();
            var mes = date.getMonth() + 1;
            var dia = date.getDate();
            if (dia < 10)
                dia = "0" + dia;
            if (mes < 10)
                mes = "0" + mes;

            var hoy = dia + "/" + mes + "/" + date.getFullYear();
            var rowData = gridModulo.jqGrid('getRowData', rowid);

            var inicioModulo = rowData.FECHA_INICIO;
            var resultado = comparar(hoy, inicioModulo);
            if (resultado == 1) {
                //  gridModulo.setColProp('FECHA_INICIO', { editable: false }); 20/08/2014
                gridModulo.setColProp('PROFESOR_ID', { editable: false });
                gridModulo.setColProp('MODULO_ID', { editable: false });
                // gridModulo.setColProp('MATRICULA_MINIMA', { editable: false });

            } else {
                //  gridModulo.setColProp('FECHA_INICIO', { editable: true }); 20/08/2014
                gridModulo.setColProp('PROFESOR_ID', { editable: true });
                gridModulo.setColProp('MODULO_ID', { editable: true });
                //gridModulo.setColProp('MATRICULA_MINIMA', { editable: true });
            }
            gridModulo.jqGrid('restoreRow', lastSelection);
            lastSelection = rowid;
            gridModulo.jqGrid('editRow', rowid, true, null, null, null, null, reload);

        },
        onSelectRow: function (id) {
            gridModulo.jqGrid('restoreRow', lastSelection);
        },
        loadComplete: function () {
            $(".ui-icon-close").click(function () {
                var gr = $(this).attr("id");
                var r = confirm("Desea Eliminar El Modulo ");
                if (r == true) {
                    $.post("/OfertasModulos/Eliminar", { id: gr }, function (data) {
                        alert(data[0].msg);
                        gridModulo.trigger('reloadGrid');

                    });

                }
                else {
                    x = "Cancelar";
                }
                // alert("adas");
            });
        }
    }).navGrid('#pagerModulo',
            {
                edit: true, add: false, del: false, search: true, refresh: true
            },
            {
        }, //edit options
            {}, //add options
            {}, //del options
            {multipleSearch: true} // search options
            );
    //             $("#pager_left").hide(); // hide unused place in the pager


    $("#tabPlanModulo").tabs();
    $("#formOfertaAuto").submit(function (event) {
        event.preventDefault();
        if ($("#AUTOFERTAMOD_AULA_ID option:selected").val() != "" && $("#AUTOFERTAMOD_PROFESOR_ID option:selected").val() != "") {
            $.validator.unobtrusive.parse("#formOfertaAuto");
            if ($("#formOfertaAuto").valid()) {
                var form = $("#formOfertaAuto").serialize();
                $.post("/OfertasModulos/GuardarOfertaAutomatica", { form: form }, function (data) {
                    if (data[0].ok == 1) {
                        alert(data[0].msg);
                        $("select").val("");
                        $("input[type=text]").val("");
                        gridModulo.trigger('reloadGrid');
                    } else
                        alert(data[0].msg);
                });
            }
        } else
            alert("Aula y Profesor Requeridos");
    });

    $("#AUTOFERTAMOD_PROFESOR_ID").change(function () {
        var id = $(this).val();
        if (id != "") {
            $.post("/OfertasModulos/ObtenerFechaUltimoModulo", { idOferta: $("#OFERTASMODULOS_OFERTA_CURSO_ID").val(), idProfesor: id }, function (data) {
                // alert(data);
                $("#OFERTASMODULOS_FECHA_FIN").datepicker("option", "minDate", data);
            });
        }
    });

    $("#ImprimirBtn").click(function () {
        var desde = $("#alt-date").val();
        var ofertaCursoId = $("#OFERTASMODULOS_OFERTA_CURSO_ID").val();
        var profesorId = $("#imprimirProfesor option:selected").val();
        if (desde != "" && profesorId != "") {
            var url = "/Reporte/EmitirReportePlanificacion?ofertaCurso=" + ofertaCursoId + "&desde=" + desde + "&profesor=" + profesorId;
            var ventana = window.open(url, "Planificación ", "width=1024,height=768,fullscreen=yes,scrollbars=yes");
            ventana.resizeTo(screen.width / 2, screen.height);
            ventana.focus();
        }
    });

    $("#buscarGridProfesor").change(function () {
         gridModulo.trigger("reloadGrid");
    });

});
 