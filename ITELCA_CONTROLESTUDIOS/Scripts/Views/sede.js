﻿var latitud;
var longitud;
var map;
var marcador ;
function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition, showError);
    }
    else { alert("Geolocalización  no es soportada por este Navegador."); }
}
function showPosition(position) {

    if ($("#sede_LATITUD").val() == "") {
        latitud = position.coords.latitude;
        longitud = position.coords.longitude;
        $("#sede_LATITUD").val(latitud);
        $("#sede_LONGITUD").val(longitud);
    } else {
        latitud=$("#sede_LATITUD").val();
        longitud=$("#sede_LONGITUD").val();
    }
    cargarMapa();
}
function showError(error) {
    switch (error.code) {
        case error.PERMISSION_DENIED:
            alert("Te encontrare...");
            break;
        case error.POSITION_UNAVAILABLE:
            alert("Ubicación No disponible");
            break;
        case error.TIMEOUT:
            alert("Tiempo de espera para geolocalizacion agotado");
            break;
        case error.UNKNOWN_ERROR:
            alert("Upps Lo sentimos :(.");
            break;
    }
}

function cargarMapa() {
    var latlon = new google.maps.LatLng(latitud, longitud); /* Creamos un punto con nuestras coordenadas */
    var myOptions = {
        zoom: 17,
        center: latlon, /* Definimos la posicion del mapa con el punto */
        mapTypeId: google.maps.MapTypeId.ROADMAP
    }; /*Configuramos una serie de opciones como el zoom del mapa y el tipo.*/
        map = new google.maps.Map($("#map_canvas").get(0), myOptions); /*Creamos el mapa y lo situamos en su capa */

    var coorMarcador = new google.maps.LatLng(latitud, longitud); /*Un nuevo punto con nuestras coordenadas para el marcador (flecha) */

        marcador = new google.maps.Marker({
        /*Creamos un marcador*/
        position: coorMarcador, /*Lo situamos en nuestro punto */
        map: map, /* Lo vinculamos a nuestro mapa */
        title: "Dónde estoy?",
        draggable: true,
        animation: google.maps.Animation.DROP
    });
     google.maps.event.addListener(marcador, 'dragend', function() {
	      var point = marcador.getPosition();
	      $("#sede_LATITUD").val(point.lat());
          $("#sede_LONGITUD").val(point.lng());

      });
}

$(document).ready(function () {
    $(".telefono").mask("0999-9999999", { placeholder: "_" });
    getLocation();


    var puntoA = new google.maps.LatLng(10.4988853, -66.7889156000000);
    var puntoB = new google.maps.LatLng(10.49890639843677, -66.78872248095092);

    var distancia = google.maps.geometry.spherical.computeDistanceBetween(puntoA, puntoB);
   // alert(distancia);
});
 
