﻿$(document).ready(function () {
    $("#manejoCaja_SEDE_ID").change(function () {
        var id = $(this).val();
        if (id != "") {
            $("#manejoCaja_CAJA_ID").find('option').remove().end();
            $.post("/Caja/ListarPorSedeDDL", { sede_id: id }, function (data) {
                var lista = '<option value="">-- Seleccione --</option>';
                if (data.length > 0) {
                    for (var i = 0; i < data.length; i++) {
                        lista += '<option value="' + data[i].optionValue + '">' + data[i].optionDisplay + '</option>';
                    }
                    $("#manejoCaja_CAJA_ID").find('option').remove().end().append(lista);
                } else {
                    $("#manejoCaja_CAJA_ID").find('option').remove().end().append('<option value="">No existen cajas registradas para la sede seleccionada</option>');
                }
            }, 'json');
        }
    });
});
 
