﻿$(document).ready(function () {
    try {
        CKEDITOR.replace('faq_PREGUNTA',
		        {
		            extraPlugins: 'wordcount,about,a11yhelp,basicstyles,bidi,blockquote,clipboard,colorbutton,colordialog,contextmenu,dialogadvtab,div,elementspath,enterkey,entities,filebrowser,find,flash,floatingspace,font,format,forms,horizontalrule,htmlwriter,image,iframe,indentlist,indentblock,justify,language,link,list,liststyle,magicline,maximize,newpage,pagebreak,pastefromword,pastetext,preview,print,removeformat,resize,save,scayt,selectall,showblocks,showborders,smiley,sourcearea,specialchar,stylescombo,tab,table,tabletools,templates,toolbar,undo,wsc,wysiwygarea',
		            filebrowserBrowseUrl: "/Image/Browse",
		            filebrowserWindowWidth: 500,
		            filebrowserWindowHeight: 650,
		            filebrowserUploadUrl: "/Image/Upload",
		            on:
                {
                    instanceReady: function (ev) {
                        // The character sequence to use for every indentation step.
                        this.indentationChars = '\t';

                        // The way to close self closing tags, like <br />.
                        this.selfClosingEnd = ' />';

                        // The character sequence to be used for line breaks.
                        this.lineBreakChars = '\n';
                        // Output paragraphs as <p>Text</p>.
                        this.dataProcessor.writer.setRules('p',
                            {
                                indent: false,
                                breakBeforeOpen: true,
                                breakAfterOpen: false,
                                breakBeforeClose: false,
                                breakAfterClose: true
                            });
                    }
                }
		        });
    }
    catch (err) { }

    try {
        CKEDITOR.replace('faq_RESPUESTA',
		{
		    extraPlugins: 'wordcount,about,a11yhelp,basicstyles,bidi,blockquote,clipboard,colorbutton,colordialog,contextmenu,dialogadvtab,div,elementspath,enterkey,entities,filebrowser,find,flash,floatingspace,font,format,forms,horizontalrule,htmlwriter,image,iframe,indentlist,indentblock,justify,language,link,list,liststyle,magicline,maximize,newpage,pagebreak,pastefromword,pastetext,preview,print,removeformat,resize,save,scayt,selectall,showblocks,showborders,smiley,sourcearea,specialchar,stylescombo,tab,table,tabletools,templates,toolbar,undo,wsc,wysiwygarea',
		    filebrowserBrowseUrl: "/Image/Browse",
		    filebrowserWindowWidth: 500,
		    filebrowserWindowHeight: 650,
		    filebrowserUploadUrl: "/Image/Upload",
		    on:
        {
            instanceReady: function (ev) {
                // The character sequence to use for every indentation step.
                this.indentationChars = '\t';

                // The way to close self closing tags, like <br />.
                this.selfClosingEnd = ' />';

                // The character sequence to be used for line breaks.
                this.lineBreakChars = '\n';
                // Output paragraphs as <p>Text</p>.
                this.dataProcessor.writer.setRules('p',
                    {
                        indent: false,
                        breakBeforeOpen: true,
                        breakAfterOpen: false,
                        breakBeforeClose: false,
                        breakAfterClose: true
                    });
            }
        }
		});
    }
    catch (err) { }

});