﻿var listaAsistencia;
var identificador = new Array();
var estado = new Array();

function marcarAsistencia() {
    if (identificador.length > 0) {
        var data = JSON.stringify({ asistenciaId: identificador, valor: estado });
        identificador.length = 0;
        estado.length = 0;

        $.ajax({
            type: "POST",
            url: "/AsistenciaTaller/GuardarAsistencia",
            contentType: "application/json; charset=utf-8",
            data: data,
            dataType: "json",
            success: function (result) {
                alert('Asistencia Guardada');
            },
            error: function (result) {
                alert('Error');
            }
        });
    }
}
function agregarCambio(id, asistio) {
    var enc = 0;
    for (var i = 0; i < identificador.lenght && enc == 0; i++) {
        if (identificador[i] == id) {
            enc = 1;
            identificador.splice(i, 1);
            estado.splice(i, 1);
        }
    }

    identificador.push(id);
    estado.push(asistio);
}
function inicializar() {
    listaAsistencia = $('#tablaAsistencia').dataTable({
        "bPaginate": false,
        "bLengthChange": false,
        "bFilter": false,
        "bSort": true,
        "bJQueryUI": true,
        "sScrollY": "150px",
        "aaSorting": [[0, "desc"]],
        "bInfo": false,
        "bAutoWidth": false,
        "bSort": false,
        "oLanguage": {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }
    });
}
$(document).ready(function () {
    inicializar();
    $("#USUARIO_NOMBRE").keypress(function (event) {
        if ((event.ctrlKey == true && (event.which == '118' || event.which == '86')) || event.charCode >= 0) {
            event.preventDefault();
        }
    });
    $(document).on('click', "input:checkbox", function (e) {
        var valor = $(this).attr("id");
        var clase = $(this).attr("class");
        var posicion = ($(this).parent().parent().index()) + 1;
        if ($(this).prop('checked')) {
            if (clase == "all") {
                $('table td:nth-child(' + valor + ')').find('input[type=checkbox]').each(function (index) {
                    agregarCambio($(this).attr("id"), 1);
                    $(this).prop('checked', true);
                });

                // marcarAsistencia($(this).attr("name"), 1, 1);

            } else {
                var enc = 1;
                $('table td:nth-child(' + posicion + ')').find('input[type=checkbox]').each(function (index) {
                    if ($(this).attr("class") != "all" && !$(this).prop('checked') && enc == 1)
                        enc = 0;
                });
                if (enc == 1)
                    $('input.all[id=' + posicion + ']').prop('checked', true);
                agregarCambio(valor, 1);
            }
        } else {
            if (clase == "all") {
                $('table td:nth-child(' + valor + ')').find('input[type=checkbox]').each(function (index) {
                    $(this).prop('checked', false);
                    agregarCambio($(this).attr("id"), 0);
                });
                // marcarAsistencia($(this).attr("name"), 0, 1);

            } else {

                $('input.all[id=' + posicion + ']').prop('checked', false);
                agregarCambio(valor, 0);

            }

        }
    });

    $("#sede").change(function () {
        var resp = $(this).find("option:selected").val();
        if (listaAsistencia != null) {
            listaAsistencia.fnDestroy();
            $('#tablaAsistencia').empty();
        }
        if (resp != "") {

            $.post("/AsistenciaTaller/ObtenerTaller", { idSede: resp }, function (data) {
                var tam = data.length;
                var lista = '<option value="">--Seleccione--</option>"';
                if(tam>0)
                    for (var i = 0; i < tam; i++)
                        lista += '<option value="' + data[i].id + '">' + data[i].nombre + ' </option>';
                else
                    lista='<option value="">--Sin Talleres--</option>';
                $("#talleres").html(lista);
            });
        }
       
        $('#tablaAsistencia').append("<thead><tr><th>Alumno</th></thead><tbody></tbody>");
        inicializar();
    });

    $("#talleres").change(function () {

        if (listaAsistencia != null) {
            listaAsistencia.fnDestroy();
            $('#tablaAsistencia').empty();
        }
        var resp = $(this).find("option:selected").val();
        var numCol = 2;
        var i = 0;
        var clases = 0;
        var tam = 0;
        var cont = 0;
        var tabla = '<thead><tr><th>Alumno</th>';
        if (resp != "") {
            $.post("/AsistenciaTaller/ObtenerTabla", { idAsistencia: resp }, function (data) {

                tam = data.length;
                if (tam > 0) {
                    while (data[i].tipo == "cabecera") {
                        if (data[i].all == 0)
                            tabla += '<th>' + data[i].fecha + '<br><input type="checkbox" class="all" name="' + data[i].id + '" id="' + numCol + '" ></th>';
                        else
                            tabla += '<th>' + data[i].fecha + '<br><input type="checkbox" class="all" name="' + data[i].id + '" id="' + numCol + '" checked="checked" ></th>';
                        numCol++;
                        i++;
                        clases++;
                    }
                    tabla += '</tr></thead><tbody>'
                    while (i < tam && data[i].tipo == "detalle") {
                        if (cont == 0)
                            tabla += '<tr><td>' + data[i].nombre + '</td>';
                        if (data[i].asistio == 0) {
                            tabla += '<td><div style="text-align: center;"><input type="checkbox" id="' + data[i].id + '"  value="0"/></div></td>';
                        }
                        else {
                            tabla += '<td><div style="text-align: center;"><input type="checkbox" id="' + data[i].id + '"  value="1" checked="checked"/></div></td>';
                        }
                        cont++;
                        i++;
                        if (cont == clases) {
                            tabla += "</tr>";
                            cont = 0;
                        }
                    }
                }
                tabla += "</tbody>";
                $('#tablaAsistencia').append(tabla);
                inicializar();
            });
        } else {
            tabla += "</tbody>";
            $('#tablaAsistencia').append(tabla);
            inicializar();
        }
    });

    $("#guardar").click(function () {
        marcarAsistencia();
    });
});

    
