﻿$(document).ready(function () {
    $('.datepicker').datepicker({ dateFormat: 'dd/mm/yy' });
    $('.datepicker').mask("99/99/9999");
    $('.montos').autoNumeric('init', { aSep: ',', aDec: '.' });

    $("#CONCEPTOS_TALLERES_TALLER_ID").multiselect({
        selectedText: "# de # seleccionado",
        selectedList: 5,
        checkAll: function (event, ui) {
        },
        click: function (event, ui) {
        }

    });

    $("#formConceptoTaller").submit(function (event) {
        $("#CONCEPTOS_TALLERES_MONTO").val($('.montos').autoNumeric('get'));
    });

});
 
