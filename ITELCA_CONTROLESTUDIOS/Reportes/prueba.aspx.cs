﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using ITELCA_CLASSLIBRARY.Services;
using System.Text;
using Microsoft.Reporting.WebForms;
using System.Drawing.Printing;
using System.Drawing.Imaging;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace ITELCA_CONTROLESTUDIOS.Reportes
{
    public partial class prueba : System.Web.UI.Page
    {
        private int m_currentPageIndex;
        private IList<Stream> m_streams;
        public string url="";
        protected void Page_Load(object sender, EventArgs e)
        {
            string recibo = Request.QueryString["id_recibo"];
            string sede = Request.QueryString["sede"];
            if (recibo != null)
            {

                    string file = sede + "_" + DateTime.Now.Day + "_" + DateTime.Now.Month + "_" + DateTime.Now.Year + ".pdf";
                    string salida = "out_" + sede + "_" + DateTime.Now.Day + "_" + DateTime.Now.Month + "_" + DateTime.Now.Year + ".pdf";   
                    LocalReport report = new LocalReport();
                    report.ReportPath = @"Reportes/Recibo_Caja_ProductoDirecto.rdlc";
                    report.DataSources.Add(
                    new ReportDataSource("DataSet1", LoadData(recibo)));
                    string deviceInfo =
                    @"<DeviceInfo>
                        <OutputFormat>EMF</OutputFormat>
                        <PageWidth>25cm</PageWidth>
                        <PageHeight>16cm</PageHeight>
                        <MarginTop>3cm</MarginTop>
                        <MarginLeft>3cm</MarginLeft>
                        <MarginRight>3cm</MarginRight>
                        <MarginBottom>3cm</MarginBottom>
                    </DeviceInfo>";
                    Warning[] warnings;
                    string[] streamids;
                    string mimeType;
                    string encoding;
                    string extension;
                    m_streams = new List<Stream>();
                    //byte[] bytes =report.Render("Image", null, CreateStream,out warnings);
                    byte[] bytes = report.Render("PDF", null, out mimeType,
                             out encoding, out extension, out streamids, out warnings);
                    FileStream fs = new FileStream(HttpContext.Current.Server.MapPath("pdf/"+salida), FileMode.Create);
                    fs.Write(bytes, 0, bytes.Length);
                    fs.Close();

                    //Open exsisting pdf
                    var pgSize = new iTextSharp.text.Rectangle(790,297);
                    Document document = new iTextSharp.text.Document(pgSize, 3, 3, 1, 1);
                    //Document document = new Document(doc, 0, 0, 0, 0);
                    PdfReader reader = new PdfReader(HttpContext.Current.Server.MapPath("pdf/" + salida));
                    //Getting a instance of new pdf wrtiter
                    PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(
                    HttpContext.Current.Server.MapPath("pdf/" + file), FileMode.Create));
                    document.Open();
                    PdfContentByte cb = writer.DirectContent;

                    int i = 0;
                    int p = 0;
                    int n = reader.NumberOfPages;
                    Rectangle psize = reader.GetPageSize(1);

                    //float width = psize.Width;
                    //float height = psize.Height;

                    //Add Page to new document
                    while (i < n)
                    {
                        document.NewPage();
                        p++;
                        i++;

                        PdfImportedPage page1 = writer.GetImportedPage(reader, i);
                        cb.AddTemplate(page1, 0, 0);
                    }

                    //Attach javascript to the document
                    writer.AddJavaScript("this.print();");
                    document.Close();
                    url = "http://bspitelcawebp.bsp.com.ve:8080/Reportes/pdf/" + file;
            }
            else
            {
                string sJavaScript = "<script language=javascript>\n";
                sJavaScript += "alert('Sin Número de Recibo');\n";
                sJavaScript += "</script>";
                Response.Write(sJavaScript);
            }
        }
        private IQueryable LoadData(string recibo)
        {
            // Create a new DataSet and read sales data file 
            //    data.xml into the first DataTable.
            //DataSet dataSet = new DataSet();
            //dataSet. ReadXml(@"..\..\data.xml");
            //return dataSet.Tables[0];

            return new ServicioSolicitudesReportes().ObtenerDataReciboProducto(recibo);

        }
    }

    public class Demo : IDisposable
    {
        private int m_currentPageIndex;
        private IList<Stream> m_streams;

        private IQueryable LoadData(string recibo)
        {
            // Create a new DataSet and read sales data file 
            //    data.xml into the first DataTable.
            //DataSet dataSet = new DataSet();
            //dataSet. ReadXml(@"..\..\data.xml");
            //return dataSet.Tables[0];

            return new ServicioSolicitudesReportes().ObtenerDataReciboProducto(recibo); 
            
        }
        // Routine to provide to the report renderer, in order to
        //    save an image for each page of the report.
        private Stream CreateStream(string name,
          string fileNameExtension, Encoding encoding,
          string mimeType, bool willSeek)
        {
            Stream stream = new MemoryStream();
            m_streams.Add(stream);
            return stream;
        }
        // Export the given report as an EMF (Enhanced Metafile) file.
        private void Export(LocalReport report)
        {
            string deviceInfo =
              @"<DeviceInfo>
                <OutputFormat>EMF</OutputFormat>
                <PageWidth>25cm</PageWidth>
                <PageHeight>16cm</PageHeight>
                <MarginTop>1cm</MarginTop>
                <MarginLeft>3cm</MarginLeft>
                <MarginRight>3cm</MarginRight>
                <MarginBottom>2cm</MarginBottom>
            </DeviceInfo>";
            Warning[] warnings;
            m_streams = new List<Stream>();
            report.Render("Image", deviceInfo, CreateStream,
               out warnings);
            foreach (Stream stream in m_streams)
                stream.Position = 0;
        }
        // Handler for PrintPageEvents
        private void PrintPage(object sender, PrintPageEventArgs ev)
        {
            Metafile pageImage = new
               Metafile(m_streams[m_currentPageIndex]);

            // Adjust rectangular area with printer margins.
            Rectangle adjustedRect = new Rectangle(
                ev.PageBounds.Left - (int)ev.PageSettings.HardMarginX,
                ev.PageBounds.Top - (int)ev.PageSettings.HardMarginY,
                ev.PageBounds.Width,
                ev.PageBounds.Height);

            // Draw a white background for the report
           // ev.Graphics.FillRectangle(Brushes.White, adjustedRect);

            // Draw the report content
            //ev.Graphics.DrawImage(pageImage, adjustedRect);

            // Prepare for the next page. Make sure we haven't hit the end.
            m_currentPageIndex++;
            ev.HasMorePages = (m_currentPageIndex < m_streams.Count);
        }

        private void Print()
        {
            if (m_streams == null || m_streams.Count == 0)
                throw new Exception("Error: no stream to print.");
            PrintDocument printDoc = new PrintDocument();
            if (!printDoc.PrinterSettings.IsValid)
            {
                throw new Exception("Error: cannot find the default printer.");
            }
            else
            {
                printDoc.PrintPage += new PrintPageEventHandler(PrintPage);
                m_currentPageIndex = 0;
                printDoc.Print();
            }
        }
        // Create a local report for Report.rdlc, load the data,
        //    export the report to an .emf file, and print it.
        public void Run(string recibo)
        {
            LocalReport report = new LocalReport();
            report.ReportPath = @"Reportes/Recibo_Caja_ProductoDirecto.rdlc";
            report.DataSources.Add(
               new ReportDataSource("DataSet1", LoadData(recibo)));
          //  Export(report);
            //Print();
            ImprimirFull(report);
        }

        public void Dispose()
        {
            if (m_streams != null)
            {
                foreach (Stream stream in m_streams)
                    stream.Close();
                m_streams = null;
            }
        }

        public void ImprimirFull(LocalReport report) {

          

            //using (FileStream msReport = new FileStream(HttpContext.Current.Server.MapPath("output.pdf"), FileMode.Create))
            //{
            //    msReport.Write(bytes, 0, bytes.Length);
            //    fs.Close();
            //}
            ////step 1
            //using (Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 100f, 10f))
            //{
            //    try
            //    {
            //        // step 2
            //        PdfWriter pdfWriter = PdfWriter.GetInstance(pdfDoc, msReport);
 
            //        //open the stream
            //        pdfDoc.Open();
 
            //        Phrase phrase1 = new Phrase("Hello world. Checking Print Functionality");
 
            //        pdfDoc.Add(phrase1);
 
            //        pdfWriter.AddJavaScript("this.print();");
 
            //        pdfDoc.Close();
 
            //    }
            //    catch (Exception ex)
            //    {
            //        //handle exception
            //    }
 
            //    finally
            //    {
 
 
            //    }
 
            //}
        }

     
    }
}