﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Constancia_Estudio.aspx.cs" Inherits="ITELCA_CONTROLESTUDIOS.Reportes.Constancia_Estudio" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head runat="server">
    <title></title>
</head>
<body>
    
    <form id="form1" runat="server">
    <div align="center">
    
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <br />
        <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" 
            Font-Size="8pt" Height="1056px" InteractiveDeviceInfos="(Colección)"  
            WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" Width="780px" >
            <LocalReport ReportPath="Reportes\Constancia_Estudio.rdlc" >
                <DataSources>
                     <rsweb:ReportDataSource DataSourceId="ObjectDataSource1" Name="DataSet1" />
                </DataSources>
            </LocalReport>
             
        </rsweb:ReportViewer>
         <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" 
            SelectMethod="ObtenerData" 
            TypeName="ITELCA_CLASSLIBRARY.Services.ServicioSolicitudesReportes">
            <SelectParameters>
                <asp:QueryStringParameter Name="id_solicitud" QueryStringField="id_solicitud" 
                    Type="String" />
            </SelectParameters>
        </asp:ObjectDataSource>
    </div>
    </form>
</body>
</html>
