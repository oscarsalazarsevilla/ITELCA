﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing.Printing;
using Microsoft.Reporting.WebForms;
using ITELCA_CLASSLIBRARY.Services;

namespace ITELCA_CONTROLESTUDIOS.Reportes
{
    public partial class Constancia_Notas : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                ReportViewer1.LocalReport.SubreportProcessing +=
                    new SubreportProcessingEventHandler(LocalReport_SubreportProcessing);
            }
        }

        private void LocalReport_SubreportProcessing(object sender, SubreportProcessingEventArgs e)
        {

            // get empID from the parameters
            string ofertaCurso = e.Parameters[0].Values[0];

            // remove all previously attached Datasources, since we want to attach a
            // new one
            e.DataSources.Clear();

            // Retrieve employeeFamily list based on EmpID
            var enteDocumento = new ServicioSolicitudesReportes().ObtenerNotasConstanciaNotas(ofertaCurso);


            // add retrieved dataset or you can call it list to data source
            e.DataSources.Add(new Microsoft.Reporting.WebForms.ReportDataSource()
            {
                Name = "DsNotas",
                Value = enteDocumento

            });

        }
    }
}