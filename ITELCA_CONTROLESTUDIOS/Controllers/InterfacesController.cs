﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ITELCA_CLASSLIBRARY.CustomClasses;
using ITELCA_CLASSLIBRARY.Models;
using ITELCA_CLASSLIBRARY.Services;
using ITELCA_CONTROLESTUDIOS.Models.ViewModels;

namespace ITELCA_CONTROLESTUDIOS.Controllers
{
    public class InterfacesController : Controller
    {
        public InterfazPeople interfaz = new InterfazPeople();

        public ActionResult Index()
        {

            return View();
        }

        public ActionResult GenerarCorridaInterfaz()
        {
            InterfazPeople interfaz = new InterfazPeople();
            InterfazViewModel modeloVista;
            string mensaje = "";
            USUARIOS usuario = new ServicioUsuario().ObtenerPorLogin(User.Identity.Name);

            if (interfaz.ChequearDataProductos() &&
                interfaz.ObtenerInventarioDiarioPeople(DateTime.Now, usuario.USUARIO_ID) &&
                interfaz.ChequearDataBancos(usuario.USUARIO_ID))
            {
                mensaje = "Interfaz ejecutada de manera satisfactoria";
                modeloVista = new InterfazViewModel(new ServicioInventario().ObtenerInventarioPorFecha(DateTime.Now),
                                                    new ServicioCuenta().ObtenerTodos().ToList(),
                                                    new ServicioBanco().ObtenerTodos().ToList(),
                                                    mensaje);
                return View(modeloVista);
            }
            else
            {
                mensaje = "La ejecución de la interfaz culminó de manera inesperada. Por favor, revise el log de eventos y contacte al personal de soporte de BSP";
                modeloVista = new InterfazViewModel(new List<INVENTARIOS>(),
                                                    new ServicioCuenta().ObtenerTodos().ToList(),
                                                    new ServicioBanco().ObtenerTodos().ToList(),
                                                    mensaje);
                return View(modeloVista);
            }
        }
    }
}
