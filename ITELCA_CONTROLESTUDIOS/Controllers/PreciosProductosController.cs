﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ITELCA_CLASSLIBRARY.Models;
using ITELCA_CONTROLESTUDIOS.Models.ViewModels;
using ITELCA_CLASSLIBRARY.Services;
using Utilidades;

namespace ITELCA_CONTROLESTUDIOS.Controllers
{
      [Authorize(Roles = "ADMINISTRADOR,COORDINADOR")]
    public class PreciosProductosController : Controller
    {
        //
        // GET: /Rol/
        PrecioProductoViewModel _precioProductoViewModel;
        ServicioPrecioProducto _servicio = new ServicioPrecioProducto();
        SelectList _activo;
        SelectList _productos;

        public void IniciarVista(PRECIOSPRODUCTOS precioProducto) {
            Dictionary<int, string> lista = new ListasGenerales().ListaActivo();
            
            if (precioProducto != null)
            {
                _activo = new SelectList(lista, "Key", "Value", precioProducto.ESTADO);
                _productos = new SelectList(new ServicioProducto().ObtenerTodos(), "INV_ITEM_ID", "DESCR",precioProducto.INV_ITEM_ID);
            }
            else
            {
                _activo = new SelectList(lista, "Key", "Value");
                _productos = new SelectList(new ServicioProducto().ObtenerTodos(), "INV_ITEM_ID", "DESCR");

            }
            _precioProductoViewModel = new PrecioProductoViewModel(precioProducto, _activo,_productos);
        }

        public ActionResult Crear()
        {
            IniciarVista(null);
            return View(_precioProductoViewModel);
        }
      
        public ActionResult Editar(string setid, string inv_item_id, string fecha_efectiva) {
            PRECIOSPRODUCTOS precioProducto = _servicio.ObtenerPorClave(setid, inv_item_id, DateTime.Parse(fecha_efectiva));
            if (precioProducto != null)
            {
                IniciarVista(precioProducto);
                return View(_precioProductoViewModel);
            }
            else
            {
                TempData["mensaje"] = "Precio del producto no encontrado";
                return RedirectToAction("Listar");
            }
        }

        public ActionResult Listar() {
            return View();
        }

        [HttpPost]
        public ActionResult Crear(PRECIOSPRODUCTOS precioProducto) {
            if (ModelState.IsValid) {
                USUARIOS usuario = new ServicioUsuario().ObtenerPorLogin(User.Identity.Name);
                precioProducto.USUARIO_ID = usuario.USUARIO_ID;
                if (_servicio.Guardar(precioProducto))
                {
                    TempData["mensaje"] = "Precio del producto guardado de manera satisfactoria";
                    return RedirectToAction("Listar");
                }
                else
                    ModelState.AddModelError(string.Empty, "Error guardando precio del producto");
                
            }
            IniciarVista(precioProducto);
            return View(_precioProductoViewModel);

        }

        [HttpPost]
        public ActionResult Editar(PRECIOSPRODUCTOS precioProducto)
        {
            if (ModelState.IsValid)
            {
                if (_servicio.Modificar(precioProducto))
                {
                    TempData["mensaje"] = "Precio del producto modificado de manera satisfactoria";
                    return RedirectToAction("Listar");
                }
                else
                    ModelState.AddModelError(string.Empty, "Error modificando precio del producto");

            }
            IniciarVista(precioProducto);
            return View(_precioProductoViewModel);

        }

        [HttpPost]
        public JsonResult Eliminar(string id)
        {
            string setid, inv_item_id;
            DateTime fecha_efectiva;
            string[] array = id.Split('|');
            setid = array[0];
            inv_item_id = array[1];
            fecha_efectiva = DateTime.Parse(array[2]);

            return Json(_servicio.Eliminar(setid,inv_item_id,fecha_efectiva));
        }

        [HttpPost]
        public JsonResult ObtenerData(string sidx, string sord, int page, int rows, string filters)
        {
            object resultado = _servicio.ObtenerDataGrid(sidx, sord, page, rows, filters);
            return Json(resultado);
        }
    }
}
