﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ITELCA_CLASSLIBRARY.Models;
using ITELCA_CONTROLESTUDIOS.Models.ViewModels;
using ITELCA_CLASSLIBRARY.Services;

namespace ITELCA_CONTROLESTUDIOS.Controllers
{
     [Authorize(Roles = "ADMINISTRADOR,COORDINADOR")]
    public class DescuentosCursosController : Controller
    {
 
        DescuentosCursosViewModel vistaModeloDescuentosCursos;
        //ServicioDescuentosCursos servicioCostosCursos;
        SelectList ListaTurnos;
        SelectList ListaCursos;
        string listaCursosSeleccionados="";
        
        public void IniciarVista(DESCUENTOS_CURSOS descuentos_cursos)
        {

            if (descuentos_cursos != null)
            {
                ListaTurnos = new SelectList(new ServicioTurno().ObtenerTodos(), "TURNO_ID", "NOMBRE", descuentos_cursos.TURNO_ID);
                ListaCursos = new SelectList(new ServicioCursos().ObtenerTodos(), "CURSO_ID", "NOMBRE", descuentos_cursos.CURSO_ID);
                
            }
            else
            {
                ListaTurnos = new SelectList(new ServicioTurno().ObtenerTodos(), "TURNO_ID", "NOMBRE");
                ListaCursos = new SelectList(new ServicioCursos().ObtenerTodos(), "CURSO_ID", "NOMBRE");
            }
            vistaModeloDescuentosCursos = new DescuentosCursosViewModel(descuentos_cursos, ListaCursos, ListaTurnos, listaCursosSeleccionados);
        }

        public ActionResult Crear()
        {
            IniciarVista(null);
            return View(vistaModeloDescuentosCursos);
        }
        
        public ActionResult Editar(int id) { 
            DESCUENTOS_CURSOS curso=new ServicioDescuentosCursos().ObtenerPorClave(id);
            if (curso != null)
            {
                IniciarVista(curso);
                return View(vistaModeloDescuentosCursos);
            }
            else
            {
                TempData["mensaje"] = "Costos Cursos no Encontrado";
                return RedirectToAction("Listar");
            }
        }

        public ActionResult Listar() {

            IniciarVista(null);
            return View(vistaModeloDescuentosCursos);
        }

        [HttpPost]
        public ActionResult Crear(DESCUENTOS_CURSOS DESCUENTOS_CURSOS, FormCollection form)
        {
            if (ModelState.IsValid) { 
                ServicioDescuentosCursos servicioCostoCursos=new ServicioDescuentosCursos();
                if (!servicioCostoCursos.Duplicado(DESCUENTOS_CURSOS))
                {
                    USUARIOS usuario = new ServicioUsuario().ObtenerPorLogin(User.Identity.Name);
                    DESCUENTOS_CURSOS.USUARIO_ID = usuario.USUARIO_ID;
                    if (servicioCostoCursos.Guardar(DESCUENTOS_CURSOS,form))
                    {
                        TempData["mensaje"] = "Costo Curso Guardado Satisfactoriamente";
                        return RedirectToAction("Listar");
                    }
                    else
                        ModelState.AddModelError(string.Empty, "Error Guardando Costo Curso");
                }
                else 
                    ModelState.AddModelError(string.Empty, "Costo Curso ya existe");
                
            }
            IniciarVista(DESCUENTOS_CURSOS);
            return View(vistaModeloDescuentosCursos);

        }

        [HttpPost]
        public ActionResult Editar(DESCUENTOS_CURSOS DESCUENTOS_CURSOS)
        {
            if (ModelState.IsValid)
            {
                ServicioDescuentosCursos servicioCostosCursos = new ServicioDescuentosCursos();
                if (!servicioCostosCursos.Duplicado(DESCUENTOS_CURSOS))
                {
                    USUARIOS usuario = new ServicioUsuario().ObtenerPorLogin(User.Identity.Name);
                    DESCUENTOS_CURSOS.USUARIO_ID = usuario.USUARIO_ID;
                    if (servicioCostosCursos.Modificar(DESCUENTOS_CURSOS))
                    {
                        TempData["mensaje"] = "Costo Curso Modificado Satisfactoriamente";
                        return RedirectToAction("Listar");
                    }
                    else
                        ModelState.AddModelError(string.Empty, "Error Modificando Costo Curso");
                }
                else
                    ModelState.AddModelError(string.Empty, "Costo Curso ya existe");

            }
            IniciarVista(DESCUENTOS_CURSOS);
            return View(vistaModeloDescuentosCursos);

        }

        [HttpPost]
        public JsonResult Eliminar(int id)
        {
            return Json(new ServicioDescuentosCursos().Eliminar(id));
        }


        [HttpPost]
        public JsonResult ObtenerData(string cursoId, string sidx, string sord, int page, int rows, string filters)
        {
            object resultado = new ServicioDescuentosCursos().ObtenerDataGrid(cursoId, sidx, sord, page, rows, filters);
            return Json(resultado);

        }

    }
}
