﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ITELCA_CLASSLIBRARY.Models;
using ITELCA_CONTROLESTUDIOS.Models.ViewModels;
using ITELCA_CLASSLIBRARY.Services;

namespace ITELCA_CONTROLESTUDIOS.Controllers
{
     [Authorize(Roles = "ADMINISTRADOR")]
    public class RolController : Controller
    {
        //
        // GET: /Rol/
        RolViewModel vistaModeloRol;
        SelectList activo;
        public void IniciarVista(ROLES rol) {
            Dictionary<int, string> lista = new Dictionary<int, string>();
            lista.Add(1, "SI");
            lista.Add(0, "NO");
            if (rol != null)
                activo = new SelectList(lista, "Key", "Value", rol.ESTADO);
            else
                activo = new SelectList(lista, "Key", "Value");
            vistaModeloRol = new RolViewModel(rol,activo);
        }

        public ActionResult Crear()
        {
            IniciarVista(null);
            return View(vistaModeloRol);
        }
        
        public ActionResult Editar(int id) { 
            ROLES rol=new ServicioRol().ObtenerPorClave(id);
            if (rol != null)
            {
                IniciarVista(rol);
                return View(vistaModeloRol);
            }
            else
            {
                TempData["mensaje"] = "Rol no Encontrado";
                return RedirectToAction("Listar");
            }
        }

        public ActionResult Listar() {
            return View();
        }

        [HttpPost]
        public ActionResult Crear(ROLES rol) {
            if (ModelState.IsValid) { 
                ServicioRol servicioRol=new ServicioRol();
                if (!servicioRol.Duplicado(rol))
                {
                    if (servicioRol.Guardar(rol))
                    {
                        TempData["mensaje"] = "Rol Guardado Satisfactoriamente";
                        return RedirectToAction("Listar");
                    }
                    else
                        ModelState.AddModelError(string.Empty, "Error Guardando Rol");
                }
                else 
                    ModelState.AddModelError(string.Empty, "Rol ya existe");
                
            }
            IniciarVista(rol);
            return View(vistaModeloRol);

        }

        [HttpPost]
        public ActionResult Editar(ROLES rol)
        {
            if (ModelState.IsValid)
            {
                ServicioRol servicioRol = new ServicioRol();
                if (!servicioRol.Duplicado(rol))
                {
                    if (servicioRol.Modificar(rol))
                    {
                        TempData["mensaje"] = "Rol Modificado Satisfactoriamente";
                        return RedirectToAction("Listar");
                    }
                    else
                        ModelState.AddModelError(string.Empty, "Error Modificando Rol");
                }
                else
                    ModelState.AddModelError(string.Empty, "Rol ya existe");

            }
            IniciarVista(rol);
            return View(vistaModeloRol);

        }

        [HttpPost]
        public JsonResult Eliminar(int id)
        {
            return Json(new ServicioRol().Eliminar(id));
        }
        [HttpPost]
        public JsonResult ObtenerData(string sidx, string sord, int page, int rows, string filters)
        {
            object resultado = new ServicioRol().ObtenerDataGrid(sidx, sord, page, rows, filters);
            return Json(resultado);

        }

    }
}
