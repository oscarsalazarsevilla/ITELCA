﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ITELCA_CLASSLIBRARY.Models;
using ITELCA_CONTROLESTUDIOS.Models.ViewModels;
using ITELCA_CLASSLIBRARY.Services;

namespace ITELCA_CONTROLESTUDIOS.Controllers
{
    
    public class TestimoniosController : Controller
    {
        //
        // GET: /Testimonios/
        TestimonioViewModel vistaModeloTestimonio;
        SelectList activo;

        public void IniciarVista(TESTIMONIOS testimonio)
        {
            Dictionary<int, string> lista = new Dictionary<int, string>();
            lista.Add(1, "SI");
            lista.Add(0, "NO");
            if (testimonio != null)
            {
                activo = new SelectList(lista, "Key", "Value", testimonio.ESTADO);
            }
            else
            {
                testimonio = new TESTIMONIOS();
                activo = new SelectList(lista, "Key", "Value");
            }
            vistaModeloTestimonio = new TestimonioViewModel(testimonio, activo, null);
        }

        public ActionResult Crear()
        {
            IniciarVista(null);
            return View(vistaModeloTestimonio);
        }

        [Authorize(Roles = "ADMINISTRADOR,COORDINADOR,WEB")]
        public ActionResult Editar(int id)
        {
            TESTIMONIOS testimonios = new ServicioTestimonio().ObtenerPorClave(id);
            
            if (testimonios != null)
            {
                IniciarVista(testimonios);
                return View(vistaModeloTestimonio);
            }
            else
            {
                TempData["mensaje"] = "Testimonio no Encontrado";
                return RedirectToAction("Listar");
            }
        }

        [Authorize(Roles = "ADMINISTRADOR,COORDINADOR,WEB")]
        public ActionResult Listar()
        {
            return View();
        }

        public ActionResult Index() 
        {
            
            ServicioTestimonio servicio = new ServicioTestimonio();
            TESTIMONIOS[] testimonios_ver = new TESTIMONIOS[servicio.ObtenerOrdenados().Count()];
            int i=0;
            foreach (var item in servicio.ObtenerOrdenados())
            {
                testimonios_ver[i] = item;
                i++;
            }
            TestimonioViewModel _ViewModel = new TestimonioViewModel(null, null, testimonios_ver);
            return PartialView(_ViewModel);
        }

        [HttpPost]
        [Authorize(Roles = "ADMINISTRADOR,COORDINADOR,WEB")]
        public ActionResult Crear(TESTIMONIOS testimonio)
        {
            if (ModelState.IsValid)
            {
                ServicioTestimonio servicioTestimonio = new ServicioTestimonio();
               
                    USUARIOS usuario = new ServicioUsuario().ObtenerPorLogin(User.Identity.Name);
                    testimonio.USUARIO_ID = usuario.USUARIO_ID;
                    testimonio.TEXTO = System.Net.WebUtility.HtmlDecode(testimonio.TEXTO);
                    
                    if (servicioTestimonio.Guardar(testimonio))
                    {
                        TempData["mensaje"] = "Testimonio Guardado Satisfactoriamente";
                        return RedirectToAction("Listar");
                    }
                    else
                        ModelState.AddModelError(string.Empty, "Error Guardando Testimonios");
               

            }
            IniciarVista(testimonio);
            return View(vistaModeloTestimonio);

        }

        [HttpPost]
        [Authorize(Roles = "ADMINISTRADOR,COORDINADOR,WEB")]
        public ActionResult Editar(TESTIMONIOS testimonio)
        {
            if (ModelState.IsValid)
            {
                ServicioTestimonio servicioTestimonio = new ServicioTestimonio();
                
                    testimonio.TEXTO = System.Net.WebUtility.HtmlDecode(testimonio.TEXTO);
                    if (servicioTestimonio.Modificar(testimonio))
                    {
                        TempData["mensaje"] = "Testimonio Modificado Satisfactoriamente";
                        return RedirectToAction("Listar");
                    }
                    else
                        ModelState.AddModelError(string.Empty, "Error Modificando Testimonio");
                
            }

            IniciarVista(testimonio);
            return View(vistaModeloTestimonio);
        }

        [HttpPost]
        [Authorize(Roles = "ADMINISTRADOR,COORDINADOR,WEB")]
        public JsonResult Eliminar(int id)
        {
            return Json(new ServicioTestimonio().Eliminar(id));
        }

        [HttpPost]
        [Authorize(Roles = "ADMINISTRADOR,COORDINADOR,WEB")]
        public JsonResult ObtenerData(string sidx, string sord, int page, int rows, string filters)
        {
            object resultado = new ServicioTestimonio().ObtenerDataGrid(sidx, sord, page, rows, filters);
            return Json(resultado);
        }

        [HttpPost]
        public JsonResult guardarTestimonio(string texto, string nombre)
        {

            TESTIMONIOS testimonio = new TESTIMONIOS();
            testimonio.ESTADO = 0;
            testimonio.FECHA_CREACION = DateTime.Now;
            testimonio.TEXTO = texto;
            testimonio.NOMBRE = nombre;
            testimonio.CONDICION = "C";
            testimonio.USUARIO_ID = 49;
          
            return Json(new ServicioTestimonio().Guardar(testimonio));
        }
    }
}
