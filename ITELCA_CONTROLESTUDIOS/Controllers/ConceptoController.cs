﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ITELCA_CLASSLIBRARY.Models;
using ITELCA_CONTROLESTUDIOS.Models.ViewModels;
using ITELCA_CLASSLIBRARY.Services;
using Utilidades;

namespace ITELCA_CONTROLESTUDIOS.Controllers
{
     [Authorize(Roles = "ADMINISTRADOR,COORDINADOR")]
    public class ConceptoController : Controller
    {
        //
        // GET: /Rol/
        ConceptoViewModel vistaModelo;
        ServicioConcepto servicioConcepto = new ServicioConcepto();
        SelectList activo, tipoPago;
        
        public void IniciarVista(CONCEPTOS concepto) {

            Dictionary<int, string> lista = new ListasGenerales().ListaActivo();
            Dictionary<int, string> tipoDDL = new ListasGenerales().ListaTipoPago();

            if (concepto != null)
            {
                activo = new SelectList(lista, "Key", "Value", concepto.ESTADO);
                tipoPago = new SelectList(tipoDDL, "Key", "Value", concepto.TIPO_PAGO);
            }
            else
            {
                activo = new SelectList(lista, "Key", "Value");
                tipoPago = new SelectList(tipoDDL, "Key", "Value");
            }
            vistaModelo = new ConceptoViewModel(concepto, activo, tipoPago);
        }

        public ActionResult Crear()
        {
            IniciarVista(null);
            return View(vistaModelo);
        }
     
        public ActionResult Editar(int id) {
            CONCEPTOS concepto = servicioConcepto.ObtenerPorClave(id);
            if (concepto != null)
            {
                IniciarVista(concepto);
                return View(vistaModelo);
            }
            else
            {
                TempData["mensaje"] = "Concepto no Encontrado.";
                return RedirectToAction("Listar");
            }
        }

        public ActionResult Listar() {
            return View();
        }

        [HttpPost]
        public ActionResult Crear(CONCEPTOS concepto)
        {
            if (ModelState.IsValid) {
                if (!servicioConcepto.Duplicado(concepto))
                {
                    if (servicioConcepto.Guardar(concepto))
                    {
                        TempData["mensaje"] = "Concepto Guardado Satisfactoriamente.";
                        return RedirectToAction("Listar");
                    }
                    else
                        ModelState.AddModelError(string.Empty, "Error Guardando Concepto");
                }
                else 
                    ModelState.AddModelError(string.Empty, "Concepto Duplicado.");
                
            }
            IniciarVista(concepto);
            return View(vistaModelo);

        }

        [HttpPost]
        public ActionResult Editar(CONCEPTOS concepto)
        {
            if (ModelState.IsValid)
            {
                if (!servicioConcepto.Duplicado(concepto))
                {
                    //USUARIOS usuario = new ServicioUsuario().ObtenerPorLogin(User.Identity.Name);
                    //concepto.USUARIO_ID = usuario.USUARIO_ID;
                    if (servicioConcepto.Modificar(concepto))
                    {
                        TempData["mensaje"] = "Concepto Modificado Satisfactoriamente";
                        return RedirectToAction("Listar");
                    }
                    else
                        ModelState.AddModelError(string.Empty, "Error Modificando Concepto");
                }
                else
                    ModelState.AddModelError(string.Empty, "Concepto Duplicado.");

            }
            IniciarVista(concepto);
            return View(vistaModelo);

        }

        [HttpPost]
        public JsonResult Eliminar(int id)
        {
            return Json(servicioConcepto.Eliminar(id));
        }

        [HttpPost]
        public JsonResult ObtenerData(string sidx, string sord, int page, int rows, string filters)
        {
            object resultado = servicioConcepto.ObtenerDataGrid(sidx, sord, page, rows, filters);
            return Json(resultado);

        }

    }
}
