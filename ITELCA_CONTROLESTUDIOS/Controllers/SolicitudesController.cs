﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ITELCA_CLASSLIBRARY.Models;
using ITELCA_CONTROLESTUDIOS.Models.ViewModels;
using ITELCA_CLASSLIBRARY.Services;


namespace ITELCA_CONTROLESTUDIOS.Controllers
{
     [Authorize(Roles = "ADMINISTRADOR,COORDINADOR,ESTUDIANTE,CAJERO")]
    public class SolicitudesController : Controller
    {
        SolicitudesViewModel vistaModeloSolicitud;
        //SelectList activo;
        SelectList sede;
        OFERTASCURSOS ofertaCurso;
        
        public void IniciarVista(SOLICITUDES solicitud) 
        {
            USUARIOS usuario = new ServicioUsuario().ObtenerPorLogin(User.Identity.Name);
            string esEstudiante="No";
            SelectList ListaCursosActivos = new SelectList(new ServicioOfertaCurso().ObtenerDiccionarioCursosActivos(), "Key", "Value");
            
            if (solicitud != null)
            {
                ofertaCurso = new ServicioOfertaCurso().ObtenerPorClave(Int32.Parse(solicitud.MATRICULASCURSOS.OFERTA_CURSO_ID.ToString()));
                sede = new SelectList(new ServicioSede().ObtenerTodos(), "SEDE_ID", "NOMBRE");
            }
            else
            {
                sede = new SelectList(new ServicioSede().ObtenerTodos(), "SEDE_ID", "NOMBRE");
            }

            vistaModeloSolicitud = new SolicitudesViewModel(solicitud, sede, usuario, null, ofertaCurso, esEstudiante, ListaCursosActivos);
        }

        public ActionResult Listar()
        {
            return View();
        }

        public ActionResult Procesar(int id)
        {
            SOLICITUDES solic = new ServicioSolicitudes().ObtenerPorClave(id);
            if (solic != null)
            {
                IniciarVista(solic);
                return View(vistaModeloSolicitud);
            }
            else
            {
                TempData["mensaje"] = "Solicitud no Encontrada";
                return RedirectToAction("Listar");
            }
        }

        [HttpPost]
        public JsonResult CambiarEstadoSolicitud(string id_solicitud)
        {
            List<object> lista = new List<object>();
            ServicioSolicitudes servicioSolicitudes = new ServicioSolicitudes();
            SOLICITUDES solicitud = servicioSolicitudes.ObtenerPorClave(Int32.Parse(id_solicitud));
            solicitud.ESTADO_SOLICITUD = 2;
            
            if (servicioSolicitudes.Modificar(solicitud))
            {
                lista.Add(new
                {
                    success = true,
                    mensaje = "Solicitud modificada con exito."
                });
                return Json(lista, JsonRequestBehavior.AllowGet);
            }

            lista.Add(new
            {
                success = false,
                mensaje = "Ocurrio un error al procesar la Solicitud."
            });


            return Json(lista, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Nueva()
        {
            IniciarVista(null);
            return View(vistaModeloSolicitud);
        }



        [HttpGet]
        public ActionResult ConstanciaNotas()
        {
            IniciarViewModelFormularioSolicitud();
            return View(vistaModeloSolicitud);
        }

        [HttpPost]
        public JsonResult ConstanciaNotas([Bind(Prefix = "solicitud")] SOLICITUDES solicitud, FormCollection form)
        {
            List<object> lista = new List<object>();
            solicitud.TIPOSOLICITUD_ID = 1; // Constancia Notas
            solicitud.USUARIO_ID = new ServicioUsuario().ObtenerPorLogin(User.Identity.Name).USUARIO_ID;

            string msg = new ServicioSolicitudes().Guardar(solicitud);
            if (msg  == "true")
            {

                lista.Add(new
                {
                    success = true,
                    mensaje = "Solicitud creada con exito."
                });
                return Json(lista, JsonRequestBehavior.AllowGet);
            }

            lista.Add(new
            {
                success = false,
                mensaje = "Ocurrio un error al procesar la Solicitud. " + msg
            });

            return Json(lista, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public JsonResult ConstanciaEstudios([Bind(Prefix = "solicitud")] SOLICITUDES solicitud, FormCollection form)
        {
            List<object> lista = new List<object>();
            solicitud.TIPOSOLICITUD_ID = 2; // Constancia Estudios
            solicitud.USUARIO_ID = new ServicioUsuario().ObtenerPorLogin(User.Identity.Name).USUARIO_ID;

            string msg = new ServicioSolicitudes().Guardar(solicitud);
            if (msg  == "true"){

                lista.Add(new
                {
                    success = true,
                    mensaje = "Solicitud creada con exito."
                });
                return Json(lista, JsonRequestBehavior.AllowGet);
            }

            lista.Add(new
            {
                success = false,
                mensaje = "Ocurrio un error al procesar la Solicitud. "+msg
            });

            return Json(lista, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult CartaPasantias([Bind(Prefix = "solicitud")] SOLICITUDES solicitud, FormCollection form)
        {
            List<object> lista = new List<object>();
            //solicitud.MATRICULASCURSOS.ALUMNO_ID = new ServicioUsuario().ObtenerPorLogin(User.Identity.Name).USUARIO_ID;
            solicitud.TIPOSOLICITUD_ID = 3; // Carta Pasantias
            solicitud.USUARIO_ID = new ServicioUsuario().ObtenerPorLogin(User.Identity.Name).USUARIO_ID;

            string msg = new ServicioSolicitudes().Guardar(solicitud);
            if (msg == "true")
            {

                lista.Add(new
                {
                    success = true,
                    mensaje = "Solicitud creada con exito."
                });
                return Json(lista, JsonRequestBehavior.AllowGet);
            }

            lista.Add(new
            {
                success = false,
                mensaje = "Ocurrio un error al procesar la Solicitud. "+msg
            });

            return Json(lista, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public JsonResult CambioTurno([Bind(Prefix = "solicitud")] SOLICITUDES solicitud, FormCollection form)
        {
            List<object> lista = new List<object>();
            /*solicitud.MATRICULASCURSOS.ALUMNO_ID = new ServicioUsuario().ObtenerPorLogin(User.Identity.Name).USUARIO_ID;
            MATRICULASCURSOS matriculaCurso = new ServicioMatriculaCurso().ObtenerPorClaves(int.Parse(solicitud.MATRICULA_CURSO_ID.ToString()), int.Parse(solicitud.MATRICULASCURSOS.ALUMNO_ID.ToString()));
            solicitud.TURNO_ID = Decimal.Parse(matriculaCurso.TURNO_ID.ToString());*/
            solicitud.TIPOSOLICITUD_ID = 6; // Cambio Turno
            solicitud.USUARIO_ID = new ServicioUsuario().ObtenerPorLogin(User.Identity.Name).USUARIO_ID;

            string msg = new ServicioSolicitudes().Guardar(solicitud);
            if (msg == "true")
            {

                lista.Add(new
                {
                    success = true,
                    mensaje = "Solicitud creada con exito."
                });
                return Json(lista, JsonRequestBehavior.AllowGet);
            }

            lista.Add(new
            {
                success = false,
                mensaje = "Ocurrio un error al procesar la Solicitud. "+msg
            });
            

            return Json(lista, JsonRequestBehavior.AllowGet);

        }

        [HttpGet]
        public ActionResult ConstanciaEstudios()
        {
            IniciarViewModelFormularioSolicitud();
            return View(vistaModeloSolicitud);
        }

        [HttpGet]
        public ActionResult CartaPasantias()
        {
            IniciarViewModelFormularioSolicitud();
            return View(vistaModeloSolicitud);
        }

        [HttpGet]
        public ActionResult CambioTurno()
        {
            IniciarViewModelFormularioSolicitud();
            return View(vistaModeloSolicitud);
        }


        public void IniciarViewModelFormularioSolicitud()
        {
            string esEstudiante = "No";
            SelectList ListaMatriculaCursos;
            SelectList ListaCursosActivos = new SelectList(new ServicioOfertaCurso().ObtenerDiccionarioCursosActivos(), "Key", "Value");
            USUARIOS usuario = new ServicioUsuario().ObtenerPorLogin(User.Identity.Name);
            if (usuario.ROLES.Any(b => b.NOMBRE == "ESTUDIANTE"))
            {
                esEstudiante = "Si";
                ListaMatriculaCursos = new SelectList(new ServicioMatriculaCurso().ObtenerTodosPorAlumno(usuario.USUARIO_ID), "Key", "Value");
                
            }
            else {
                usuario = null;
                Dictionary<string, string> Dictionary = new Dictionary<string, string>();
                ListaMatriculaCursos = new SelectList(Dictionary, "Key", "Value");
               
            }
            vistaModeloSolicitud = new SolicitudesViewModel(null, null, usuario, ListaMatriculaCursos, null, esEstudiante, ListaCursosActivos);
        }

        [HttpPost]
        public JsonResult Eliminar(int id)
        {
            return Json(new ServicioSolicitudes().Eliminar(id));
        }

        [HttpPost]
        public JsonResult ObtenerData(string sidx, string sord, int page, int rows, string filters)
        {
            object resultado = new ServicioSolicitudes().ObtenerDataGrid(sidx, sord, page, rows, filters);
            return Json(resultado);

        }

         [HttpPost]
        public JsonResult ObtenerCursosInscritosPorAlumno(string USUARIO_ID)
        {
            object resultado = new ServicioMatriculaCurso().ObtenerCursosInscritosPorAlumno(decimal.Parse(USUARIO_ID));
            return Json(resultado);

        }

         [HttpPost]
         public bool ObtenerNotasporAlumnoyCurso(string cedula, string matriculaCurso)
         {
             return new ServicioMatriculaCurso().ObtenerNotasporAlumnoyCurso(decimal.Parse(cedula), Int32.Parse(matriculaCurso));
         }

         [HttpPost]
         public bool validarReimpresion(string solicitud_id)
         {
             return new ServicioSolicitudes().ValidarReimpresion(Int32.Parse(solicitud_id), Session["NombreUsuario"].ToString());
  

         }

    }
}
