﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ITELCA_CLASSLIBRARY.Models;
using ITELCA_CONTROLESTUDIOS.Models.ViewModels;
using ITELCA_CLASSLIBRARY.Services;
using ITELCA_CLASSLIBRARY.CustomClasses;
using System.Web.Script.Serialization;

namespace ITELCA_CONTROLESTUDIOS.Controllers
{
    [Authorize(Roles = "PROFESOR,COORDINADOR,ADMINISTRADOR")]
    public class AsistenciaTallerController : Controller
    {
        //
        // GET: /Asistencia/
        private AsistenciaTallerViewModel vistaModeloAsistenciaTaller;
        private SelectList taller;
        private SelectList sede;
        private SelectList profesor;
        public void iniciarVista()
        {
            USUARIOS usuario=new ServicioUsuario().ObtenerPorLogin(User.Identity.Name);
            if (usuario.ROLES.Where(u => u.NOMBRE == "PROFESOR").Count() > 0)
            {
                taller = new SelectList(new ServicioOfertasTalleres().ObtenerOfertaPorProfesor(usuario.USUARIO_ID), "Key", "Value");
                profesor = new SelectList(new Dictionary<decimal, string> { }, "Key", "Value");
            }
            else
            {
                sede = new SelectList(new ServicioSede().ObtenerTodos(), "SEDE_ID", "NOMBRE");
                profesor = new SelectList(new ServicioUsuario().ObtenerListaProfesores(), "Key", "Value");
                taller= new SelectList(new Dictionary<decimal, string> { }, "Key", "Value");
            }

            vistaModeloAsistenciaTaller = new AsistenciaTallerViewModel(taller, usuario,sede,profesor);
        }
                
        public ActionResult Index()
        {
            iniciarVista();
            return View(vistaModeloAsistenciaTaller);
        }
 
        [HttpPost]
        public JsonResult ObtenerTaller(string idSede)
        {
            return Json(new ServicioOfertasTalleres().ObtenerTodosTalleresActivos(decimal.Parse(idSede )));
        }
         
        [HttpPost]
        public JsonResult ObtenerTabla(string idAsistencia)
        {
            string[] id = idAsistencia.Split('_');
            return Json(new ServicioAsistenciaTaller().ObtenerTablaAsistencia(decimal.Parse(id[0])));
        }

        [HttpPost]
        [ValidateInput(false)]
        public JsonResult GuardarAsistencia(List<string> asistenciaId, List<string> valor)
        {

            try
            {

                ServicioAsistenciaTaller servicioAsistenciaTaller = new ServicioAsistenciaTaller();
                ServicioMatriculaTaller servicioMatriculaTaller = new ServicioMatriculaTaller();
                USUARIOS usuario = new ServicioUsuario().ObtenerPorLogin(User.Identity.Name);
                for (int i = 0; i < asistenciaId.Count; i++)
                {
                    string[] id = asistenciaId[i].Split('_');
                    ASISTENCIASTALLER asistencia = servicioAsistenciaTaller.ObtenerPorClave(decimal.Parse(id[0]), decimal.Parse(id[1]));
                    if (asistencia == null)
                    {
                        MATRICULASTALLERES oferta = servicioMatriculaTaller.ObtenerPorClave(decimal.Parse(id[0]), decimal.Parse(id[1]));
                        asistencia = new ASISTENCIASTALLER();
                        asistencia.ALUMNO_ID = oferta.ALUMNO_ID.Value;
                        asistencia.OFERTA_TALLER_ID = oferta.OFERTA_TALLER_ID;
                        asistencia.FECHA_CLASE = oferta.OFERTASTALLERES.FECHA_INICIO;
                        asistencia.USUARIO_ID = usuario.USUARIO_ID;
                        servicioAsistenciaTaller.Guardar(asistencia);
                    }
                    else
                    {

                        asistencia.ASISTIO = decimal.Parse(valor[i]);
                        servicioAsistenciaTaller.Modificar(asistencia);
                    }
                }
                return Json("Ok");
            }
            catch
            {
                return Json("");
            }
        }
    }
}
