﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ITELCA_CLASSLIBRARY.Services;
using ITELCA_CLASSLIBRARY.Models;
using ITELCA_CONTROLESTUDIOS.Models.ViewModels;
using System.IO;

namespace ITELCA_CLASSLIBRARY.Controllers
{
     [Authorize(Roles = "ADMINISTRADOR,COORDINADOR,WEB")]
    public class NoticiaController : Controller
    {
        private NoticiaViewModel vistaModeloNoticia;
        SelectList activo;

        private void IniciarVista(NOTICIAS noticia)
        {
            Dictionary<int, string> lista = new Dictionary<int, string>();
            lista.Add(1, "SI");
            lista.Add(0, "NO");
            if (noticia == null)
            {
                noticia = new NOTICIAS();
                noticia.FECHA_PUBLICACION = DateTime.Now;
                activo = new SelectList(lista, "Key", "Value");
            }
            else
            {
                activo = new SelectList(lista, "Key", "Value", noticia.ESTADO);
            }

            vistaModeloNoticia = new NoticiaViewModel(noticia,activo);
        }

        public ActionResult Crear()
        {
            IniciarVista(null);
            return View(vistaModeloNoticia);
        }


        public ActionResult Editar(int id)
        {
            NOTICIAS noticia = new ServicioNoticia().ObtenerPorClave(id);
            if (noticia != null)
            {
                IniciarVista(noticia);
                return View(vistaModeloNoticia);
            }
            else
            {
                TempData["mensaje"] = "Noticia no Encontrada";
                return RedirectToAction("Listar");
            }
        }

        public ActionResult Index()
        {
            ServicioNoticia servicio = new ServicioNoticia();
            NOTICIAS[] vector = new NOTICIAS[servicio.ObtenerTodos_Activos().Count()];
            int i = 0;
            foreach (var item in servicio.ObtenerTodos_Activos())
            {
                vector[i] = item;
                i++;
            }
            vistaModeloNoticia = new NoticiaViewModel(vector, null);
            return PartialView(vistaModeloNoticia);
        }

        public ActionResult Listar()
        {
            return View();
        }

        public ActionResult DetalleNoticia(int id)
        {
            ServicioNoticia servicio = new ServicioNoticia();
            NOTICIAS noticia = servicio.ObtenerPorClave(id);
            vistaModeloNoticia = new NoticiaViewModel(null, noticia);
            return PartialView(vistaModeloNoticia);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Crear([Bind(Prefix = "noticiaCMS")]NOTICIAS noticia, HttpPostedFileBase[] file, FormCollection form)
        {
            string path = "";
            if (file != null && file[0] != null)
            {
                path = System.IO.Path.Combine(Server.MapPath("~/Content/Web/Noticias/"), System.IO.Path.GetFileName(file[0].FileName));
                //---Añadiendo el nombre de la imagen amostrar 
                noticia.IMAGEN_RESUMEN = file[0].FileName;
                file[0].SaveAs(path);
            }
            else
            {
                ModelState.AddModelError("ImagenResumen", "Es necesario agregar la imagen resumen");
            }
           
            ModelState.Remove("noticiaCMS.IMAGEN_RESUMEN");
            if (ModelState.IsValid)
            {
                USUARIOS usuario = new ServicioUsuario().ObtenerPorLogin(User.Identity.Name);
                noticia.USUARIO_ID = usuario.USUARIO_ID;
                if (new ServicioNoticia().Guardar(noticia))
                {
                    TempData["mensaje"] = "Registro creado satisfactoriamente";
                    return RedirectToAction("Listar");

                    //return RedirectToAction("Index", new { message = "Registro creado satisfactoriamente" });

                }
                else
                {
                    ModelState.AddModelError("UserName", "Error creando el registro. Valide la información e intente de nuevo");
                }
            }
            IniciarVista(noticia);
            return View(vistaModeloNoticia);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Editar([Bind(Prefix = "noticiaCMS")] NOTICIAS noticia, HttpPostedFileBase[] file, FormCollection form)
        {
            string path = "";
            if (file != null && file[0] != null)
            {
                path = System.IO.Path.Combine(Server.MapPath("~/Content/Web/Noticias/"), System.IO.Path.GetFileName(file[0].FileName));
                //---Añadiendo el nombre de la imagen amostrar 
                noticia.IMAGEN_RESUMEN = file[0].FileName;
                file[0].SaveAs(path);
            }
            
            if (ModelState.IsValid)
            {
                ServicioNoticia servicioNoticia = new ServicioNoticia();

                if (servicioNoticia.Modificar(noticia))
                {
                    TempData["mensaje"] = "Noticia Modificada Satisfactoriamente";
                    return RedirectToAction("Listar");
                }
                else
                    ModelState.AddModelError(string.Empty, "Error Modificando Noticia");
            }

            IniciarVista(noticia);
            return View(vistaModeloNoticia);
        }

        [HttpPost]
        public JsonResult Eliminar(int id)
        {
            return Json(new ServicioNoticia().Eliminar(id));
        }

        [HttpPost]
        public JsonResult ObtenerData(string sidx, string sord, int page, int rows, string filters)
        {
            object resultado = new ServicioNoticia().ObtenerDataGrid(sidx, sord, page, rows, filters);
            return Json(resultado);
        }

        #region AjaxMethods

        //public ActionResult GetForGrid(string sidx, string sord, int page, int rows, string filters)
        //{
        //    object resultado = _bannerService.ObtenerParaMatriz(sidx, sord, page, rows, filters);
        //    return Json(resultado);
        //}


        #endregion
    }
}
