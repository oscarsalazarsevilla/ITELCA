﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ITELCA_CLASSLIBRARY.Models;
using ITELCA_CONTROLESTUDIOS.Models.ViewModels;
using ITELCA_CLASSLIBRARY.Services;
using System.Globalization;

namespace ITELCA_CONTROLESTUDIOS.Controllers
{
     [Authorize(Roles = "ADMINISTRADOR,COORDINADOR")]
    public class FeriadoController : Controller
    {
        //
        // GET: /Rol/
        FeriadoViewModel vistaModelo;
        ServicioFeriado ServicioFeriado = new ServicioFeriado();
        SelectList activo;
        SelectList listaMeses;
        SelectList listaDias;
        SelectList listaPeriodicidad;
        
        public void IniciarVista(FERIADOS feriado) {

            Dictionary<int, string> lista = new Dictionary<int, string>();
            Dictionary<int, string> meses = new Dictionary<int, string>();
            Dictionary<int, string> dias = new Dictionary<int, string>();
            Dictionary<int, string> periodo = new Dictionary<int, string>();
            
            lista.Add(1, "SI");
            lista.Add(0, "NO");

            periodo.Add(7, "Semanal");
            periodo.Add(14, "Quincenal");

            for (int i = 0; i < 12; i++)
            {
                meses.Add(i+1, CultureInfo.CurrentUICulture.DateTimeFormat.MonthNames[i]);
            }

            for (int i = 1; i <= 31; i++)
            {
                dias.Add(i, i.ToString());
            }

            if (feriado != null)
            {
                activo = new SelectList(lista, "Key", "Value", feriado.ESTADO);
                listaMeses = new SelectList(meses, "Key", "Value", feriado.MES);
                listaDias = new SelectList(dias, "Key", "Value", feriado.DIA);
                listaPeriodicidad = new SelectList(periodo, "Key", "Value", feriado.PERIODICO);
            }
            else
            {
                feriado = new FERIADOS();
                activo = new SelectList(lista, "Key", "Value");
                listaMeses = new SelectList(meses, "Key", "Value");
                listaDias = new SelectList(dias, "Key", "Value");
                listaPeriodicidad = new SelectList(periodo, "Key", "Value");
            }
            vistaModelo = new FeriadoViewModel(feriado, activo,listaMeses, listaDias, listaPeriodicidad);
        }

        public ActionResult Crear()
        {
            IniciarVista(null);
            return View(vistaModelo);
        }
     
        public ActionResult Editar(int id) {
            FERIADOS feriado = ServicioFeriado.ObtenerPorClave(id);
            if (feriado != null)
            {
                IniciarVista(feriado);
                return View(vistaModelo);
            }
            else
            {
                TempData["mensaje"] = "Feriado no Encontrado.";
                return RedirectToAction("Listar");
            }
        }

        public ActionResult Listar() {
            return View();
        }

        [HttpPost]
        public ActionResult Crear(FERIADOS FERIADOS)
        {
            FERIADOS.USUARIO_ID = new ServicioUsuario().ObtenerPorLogin(User.Identity.Name).USUARIO_ID;
                    
            if (ModelState.IsValid) {
                if (!ServicioFeriado.Duplicado(FERIADOS.FERIADO_ID, FERIADOS.NOMBRE))
                {
                    if (ServicioFeriado.Guardar(FERIADOS))
                    {
                        TempData["mensaje"] = "Feriado Guardado Satisfactoriamente.";
                        return RedirectToAction("Listar");
                    }
                    else
                        ModelState.AddModelError(string.Empty, "Error Guardando Feriado");
                }
                else 
                    ModelState.AddModelError(string.Empty, "Feriado Duplicado.");
                
            }
            IniciarVista(FERIADOS);
            return View(vistaModelo);

        }

        [HttpPost]
        public ActionResult Editar(FERIADOS FERIADOS)
        {
            if (ModelState.IsValid)
            {
                if (!ServicioFeriado.Duplicado(FERIADOS.FERIADO_ID, FERIADOS.NOMBRE))
                {
                  
                    FERIADOS.USUARIO_ID = new ServicioUsuario().ObtenerPorLogin(User.Identity.Name).USUARIO_ID;
                    
                    if (ServicioFeriado.Modificar(FERIADOS))
                    {
                        TempData["mensaje"] = "Feriado Modificado Satisfactoriamente";
                        return RedirectToAction("Listar");
                    }
                    else
                        ModelState.AddModelError(string.Empty, "Error Modificando Feriado");
                }
                else
                    ModelState.AddModelError(string.Empty, "Feriado Duplicado.");

            }
            IniciarVista(FERIADOS);
            return View(vistaModelo);

        }

        [HttpPost]
        public JsonResult Eliminar(int id)
        {
            return Json(ServicioFeriado.Eliminar(id));
        }

        [HttpPost]
        public JsonResult ObtenerData(string sidx, string sord, int page, int rows, string filters)
        {
            object resultado = ServicioFeriado.ObtenerDataGrid(sidx, sord, page, rows, filters);
            return Json(resultado);

        }

        [HttpPost]
        public JsonResult ObtenerFeriado()
        {
            return Json(ServicioFeriado.ObtenerListaFeriados());
          
        }

    }
}
