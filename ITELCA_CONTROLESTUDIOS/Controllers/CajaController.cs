﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ITELCA_CLASSLIBRARY.Models;
using ITELCA_CONTROLESTUDIOS.Models.ViewModels;
using ITELCA_CLASSLIBRARY.Services;

namespace ITELCA_CONTROLESTUDIOS.Controllers
{
    public class CajaController : Controller
    {
        //
        // GET: /Rol/
        CajaViewModel vistaModeloCaja;
        SelectList activo;
        SelectList sede;
        ServicioCaja _servicio = new ServicioCaja();

        public void IniciarVista(CAJAS caja) {
            Dictionary<int, string> lista = new Dictionary<int, string>();
            lista.Add(1, "SI");
            lista.Add(0, "NO");
            if (caja != null)
            {
                activo = new SelectList(lista, "Key", "Value", caja.ESTADO);
                sede = new SelectList(new ServicioSede().ObtenerTodos(), "SEDE_ID", "NOMBRE", caja.SEDE_ID);
            }
            else
            {
                activo = new SelectList(lista, "Key", "Value");
                sede = new SelectList(new ServicioSede().ObtenerTodos(), "SEDE_ID", "NOMBRE");
            
            }
            vistaModeloCaja = new CajaViewModel(caja, activo, sede);
        }

        [Authorize(Roles = "ADMINISTRADOR")]
        public ActionResult Crear()
        {
            IniciarVista(null);
            return View(vistaModeloCaja);
        }

        [Authorize(Roles = "ADMINISTRADOR")]
        public ActionResult Editar(int id) {
            CAJAS caja = _servicio.ObtenerPorClave(id);
            if (caja != null)
            {
                IniciarVista(caja);
                return View(vistaModeloCaja);
            }
            else
            {
                TempData["mensaje"] = "Caja no Encontrado";
                return RedirectToAction("Listar");
            }
        }

        [Authorize(Roles = "ADMINISTRADOR,COORDINADOR,EMPLEADO,CAJERO")]
        public ActionResult Listar() {
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "ADMINISTRADOR")]
        public ActionResult Crear(CAJAS caja) {
            if (ModelState.IsValid) {
                if (!_servicio.Duplicado(caja, "C"))
                {
                    USUARIOS usuario = new ServicioUsuario().ObtenerPorLogin(User.Identity.Name);
                    caja.USUARIO_ID = usuario.USUARIO_ID;
                    if (_servicio.Guardar(caja))
                    {
                        TempData["mensaje"] = "Caja Guardada Satisfactoriamente";
                        return RedirectToAction("Listar");
                    }
                    else
                        ModelState.AddModelError(string.Empty, "Error Guardando Caja");
                }
                else 
                    ModelState.AddModelError(string.Empty, "Caja ya existe");
                
            }
            IniciarVista(caja);
            return View(vistaModeloCaja);

        }

        [HttpPost]
        [Authorize(Roles = "ADMINISTRADOR")]
        public ActionResult Editar(CAJAS caja)
        {
            if (ModelState.IsValid)
            {
                if (!_servicio.Duplicado(caja, "A"))
                {
                    if (_servicio.Modificar(caja))
                    {
                        TempData["mensaje"] = "Caja Modificada Satisfactoriamente";
                        return RedirectToAction("Listar");
                    }
                    else
                        ModelState.AddModelError(string.Empty, "Error Modificando Caja");
                }
                else
                    ModelState.AddModelError(string.Empty, "Caja ya existe");

            }
            IniciarVista(caja);
            return View(vistaModeloCaja);

        }

        [HttpPost]
        [Authorize(Roles = "ADMINISTRADOR")]
        public JsonResult Eliminar(int id)
        {
            return Json(_servicio.Eliminar(id));
        }
        [HttpPost]
        public JsonResult ObtenerData(string sidx, string sord, int page, int rows, string filters)
        {
            object resultado = _servicio.ObtenerDataGrid(sidx, sord, page, rows, filters);
            return Json(resultado);

        }

        [HttpGet]
        [Authorize(Roles = "ADMINISTRADOR,COORDINADOR,EMPLEADO,CAJERO")]
        public string ObtenerListaCaja(decimal idSede)
        {
            return _servicio.ObtenerCajaPorSede(idSede);
        }

        [HttpPost]
        [Authorize(Roles = "ADMINISTRADOR,COORDINADOR,EMPLEADO,CAJERO")]
        public JsonResult ListarPorSedeDDL(int sede_id)
        {
            List<object> cajas = new List<object>();

            foreach (var caja in _servicio.ObtenerParaListaPorSede(sede_id))
            {
                cajas.Add(new
                {
                    optionValue = caja.CAJA_ID,
                    optionDisplay = caja.NOMBRE
                });
            }

            return Json(cajas);

        }

    }
}
