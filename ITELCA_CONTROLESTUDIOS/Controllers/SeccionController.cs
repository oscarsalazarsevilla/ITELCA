﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ITELCA_CLASSLIBRARY.Models;
using ITELCA_CONTROLESTUDIOS.Models.ViewModels;
using ITELCA_CLASSLIBRARY.Services;

namespace ITELCA_CONTROLESTUDIOS.Controllers
{
     [Authorize(Roles = "ADMINISTRADOR,COORDINADOR,WEB")]
    public class SeccionController : Controller
    {

        SeccionViewModel vistaModeloSeccion;
        SelectList activo, categorias;

        public void IniciarVista(SECCIONES seccion) {
            Dictionary<int, string> lista = new Dictionary<int, string>();
            lista.Add(1, "SI");
            lista.Add(0, "NO");
            if (seccion != null)
            {
                activo = new SelectList(lista, "Key", "Value", seccion.ESTADO);
                categorias = new SelectList(new ServicioCategoria().ObtenerDiccionarioCategorias(), "Key", "Value", seccion.CATEGORIA_ID);
            }
            else
            {
                activo = new SelectList(lista, "Key", "Value");
                categorias = new SelectList(new ServicioCategoria().ObtenerDiccionarioCategorias(), "Key", "Value");
            }
            vistaModeloSeccion = new SeccionViewModel(seccion, categorias, activo);
        }

        public ActionResult Crear()
        {
            SECCIONES seccion = new SECCIONES();
            seccion.FECHA_PUBLICACION = DateTime.Now;
            IniciarVista(seccion);
            return View(vistaModeloSeccion);
        }
      
        public ActionResult Editar(int id) { 
            SECCIONES seccion=new ServicioSeccion().ObtenerPorClave(id);
            if (seccion != null)
            {
                IniciarVista(seccion);
                return View(vistaModeloSeccion);
            }
            else
            {
                TempData["mensaje"] = "Seccion no Encontrado";
                return RedirectToAction("Listar");
            }
        }

        public ActionResult Listar() {
            return View();
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Crear(SECCIONES seccion, HttpPostedFileBase[] file)
        {

            if (ModelState.IsValid)
            {

                string path = "";
                if (file != null && file[0] != null)
                {
                    path = System.IO.Path.Combine(Server.MapPath("~/Content/Web/Seccion/"), System.IO.Path.GetFileName(file[0].FileName));
                    //---Añadiendo el nombre de la imagen amostrar 
                    seccion.IMAGEN_RESUMEN = file[0].FileName;
                    file[0].SaveAs(path);


                    ServicioSeccion servicioSeccion = new ServicioSeccion();
                    if (!servicioSeccion.Duplicado(seccion))
                    {
                        USUARIOS usuario = new ServicioUsuario().ObtenerPorLogin(User.Identity.Name);
                        seccion.USUARIO_ID = usuario.USUARIO_ID;
                        if (servicioSeccion.Guardar(seccion))
                        {
                            TempData["mensaje"] = "Seccion Guardada Satisfactoriamente";
                            return RedirectToAction("Listar");
                        }
                        else
                            ModelState.AddModelError(string.Empty, "Error Guardando Seccion");
                    }
                    else
                        ModelState.AddModelError(string.Empty, "Seccion ya existe");


                }
                else
                {
                    ModelState.AddModelError("Url", "Es necesario agregar el url");
                }
            }
            
            IniciarVista(seccion);
            return View(vistaModeloSeccion);

        }

        [HttpPost]
         [ValidateInput(false)]
        public ActionResult Editar(SECCIONES seccion, HttpPostedFileBase[] file)
        {
            if (ModelState.IsValid)
            {

                if (file[0] != null)
                {
                    file[0].SaveAs(System.IO.Path.Combine(Server.MapPath("~/Content/Web/Seccion/"), System.IO.Path.GetFileName(file[0].FileName)));
                    seccion.IMAGEN_RESUMEN = file[0].FileName;
                }

                ServicioSeccion servicioSeccion = new ServicioSeccion();
                /*if (!servicioSeccion.Duplicado(seccion))
                {*/
                    if (servicioSeccion.Modificar(seccion))
                    {
                        TempData["mensaje"] = "Seccion Modificada Satisfactoriamente";
                        return RedirectToAction("Listar");
                    }
                    else
                        ModelState.AddModelError(string.Empty, "Error Modificando Seccion");
                /*}
                else
                    ModelState.AddModelError(string.Empty, "Seccion ya existe");*/

            }
            IniciarVista(seccion);
            return View(vistaModeloSeccion);

        }

        [HttpPost]
        public JsonResult Eliminar(int id)
        {
            return Json(new ServicioSeccion().Eliminar(id));
        }
        [HttpPost]
        public JsonResult ObtenerData(string sidx, string sord, int page, int rows, string filters)
        {
            object resultado = new ServicioSeccion().ObtenerDataGrid(sidx, sord, page, rows, filters);
            return Json(resultado);

        }

       

    }
}
