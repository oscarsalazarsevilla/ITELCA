﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ITELCA_CLASSLIBRARY.Models;
using ITELCA_CONTROLESTUDIOS.Models.ViewModels;
using ITELCA_CLASSLIBRARY.Services;
using Utilidades;

namespace ITELCA_CONTROLESTUDIOS.Controllers
{
    public class ManejoCajasController : Controller
    {
        //
        // GET: /Rol/
        ServicioManejoCaja _servicio = new ServicioManejoCaja();
        ManejoCajaViewModel _modeloVistaManejoCaja;
        SelectList _sedes;
        SelectList _cajas;
        SelectList _condicion;

        [Authorize(Roles = "ADMINISTRADOR,CAJERO,COORDINADOR")]
        public void IniciarVista(MANEJOCAJAS manejoCajas, decimal? sede_id) {
            
            if (manejoCajas != null && sede_id.HasValue)
            {
                _sedes = new SelectList(new ServicioSede().ObtenerTodos(), "SEDE_ID", "NOMBRE", sede_id.Value);
                _cajas = new SelectList(new ServicioCaja().ObtenerCajasPorSede(sede_id.Value), "CAJA_ID", "NOMBRE", manejoCajas.CAJA_ID);
                _condicion = new SelectList(new ListasGenerales().ListaCondicionCaja(), "Key", "Value", manejoCajas.CONDICION);
            }
            else
            {
                _sedes = new SelectList(new ServicioSede().ObtenerTodos(), "SEDE_ID", "NOMBRE");
                _cajas = new SelectList(new ServicioCaja().ObtenerCajasPorSede(-1), "CAJA_ID", "NOMBRE");
                _condicion = new SelectList(new ListasGenerales().ListaCondicionCaja(), "Key", "Value");
            }
            _modeloVistaManejoCaja = new ManejoCajaViewModel(manejoCajas, _cajas, _sedes, _condicion);
        }

        [Authorize(Roles = "ADMINISTRADOR,CAJERO,COORDINADOR")]
        public ActionResult Crear()
        {
            IniciarVista(null,null);
            return View(_modeloVistaManejoCaja);
        }

        [Authorize(Roles = "ADMINISTRADOR")]
        public ActionResult Editar(int id) { 
            MANEJOCAJAS manejoCaja= _servicio.ObtenerPorClave(id);
            if (manejoCaja != null)
            {
                IniciarVista(manejoCaja, null);
                return View(_modeloVistaManejoCaja);
            }
            else
            {
                TempData["mensaje"] = "Registro del manejo de caja no encontrado";
                return RedirectToAction("Listar");
            }
        }

        [Authorize(Roles = "ADMINISTRADOR,CAJERO,COORDINADOR")]
        public ActionResult Cerrar()
        {
            MANEJOCAJAS manejoCaja = _servicio.ObtenerManejoCajaAbierta(User.Identity.Name);
            if (manejoCaja != null)
            {
                IniciarVista(manejoCaja, null);
                return View(_modeloVistaManejoCaja);
            }
            else
            {
                TempData["mensaje"] = "Registro del manejo de caja no encontrado";
                return RedirectToAction("Listar");
            }
        }

        [Authorize(Roles = "ADMINISTRADOR,CAJERO,COORDINADOR")]
        public ActionResult Listar() {
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "ADMINISTRADOR,CAJERO,COORDINADOR")]
        public ActionResult Crear(MANEJOCAJAS manejoCaja, FormCollection collection) {
            if (ModelState.IsValid) {
                if (_servicio.EstaCerrada(manejoCaja))
                {
                    USUARIOS usuario = new ServicioUsuario().ObtenerPorLogin(User.Identity.Name);
                    manejoCaja.USUARIO_ID = usuario.USUARIO_ID;
                    if (_servicio.Guardar(manejoCaja))
                    {
                        TempData["mensaje"] = "Caja abierta de manera satisfactoria";
                        return RedirectToAction("Listar");
                    }
                    else
                        ModelState.AddModelError(string.Empty, "Error abriendo caja");
                }
                else 
                    ModelState.AddModelError(string.Empty, "No se puede abrir la caja, ya que no ha sido cerrada por el último usuario que la abrió");
                
            }
            IniciarVista(manejoCaja, decimal.Parse(collection["manejoCaja.SEDE_ID"]));
            return View(_modeloVistaManejoCaja);
        }

        [HttpPost]
        [Authorize(Roles = "ADMINISTRADOR")]
        public ActionResult Editar(MANEJOCAJAS manejoCaja, FormCollection collection)
        {
            if (ModelState.IsValid)
            {
                if (_servicio.Modificar(manejoCaja))
                {
                    TempData["mensaje"] = "Manejo de caja modificado de manera satisfactoria";
                    return RedirectToAction("Listar");
                }
                else
                    ModelState.AddModelError(string.Empty, "Error modificando manejo de caja");
            }
            IniciarVista(manejoCaja, decimal.Parse(collection["manejoCaja.SEDE_ID"]));
            return View(_modeloVistaManejoCaja);

        }

        [HttpPost]
        [Authorize(Roles = "ADMINISTRADOR,CAJERO,COORDINADOR")]
        public ActionResult Cerrar(MANEJOCAJAS manejoCaja)
        {
            if (ModelState.IsValid)
            {
                if (_servicio.Cerrar(manejoCaja))
                {
                    TempData["mensaje"] = "Caja cerrada de manera satisfactoria";
                    return RedirectToAction("Listar");
                }
                else
                    ModelState.AddModelError(string.Empty, "Error cerrando manejo de caja");
            }
            IniciarVista(manejoCaja, null);
            return View(_modeloVistaManejoCaja);
        }

        [HttpPost]
        [Authorize(Roles = "ADMINISTRADOR")]
        public JsonResult Eliminar(int id)
        {
            return Json(_servicio.Eliminar(id));
        }

        [HttpPost]
        [Authorize(Roles = "ADMINISTRADOR,CAJERO,COORDINADOR")]
        public JsonResult ObtenerData(string sidx, string sord, int page, int rows, string filters)
        {
            object resultado = new ServicioManejoCaja().ObtenerDataGrid(sidx, sord, page, rows, filters);
            return Json(resultado);
        }


    }
}
