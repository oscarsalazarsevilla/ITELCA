﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ITELCA_CLASSLIBRARY.Models;
using ITELCA_CONTROLESTUDIOS.Models.ViewModels;
using ITELCA_CLASSLIBRARY.Services;
using ITELCA_CLASSLIBRARY.CustomClasses;
using System.Web.Script.Serialization;

namespace ITELCA_CONTROLESTUDIOS.Controllers
{
    [Authorize(Roles = "PROFESOR,COORDINADOR,ADMINISTRADOR")]
    public class NotaModuloController : Controller
    {
        //
        // GET: /Asistencia/
        private NotaModuloViewModel vistaModeloNota;
        private SelectList curso;
        private SelectList turno;
        private SelectList modulo;
        private SelectList sede;
        private SelectList profesor;
        SelectList listaVacia;
        private bool esAdmin;

        public void iniciarVista(decimal idOfertaCurso,decimal idModulo)
        {
            USUARIOS usuario=new ServicioUsuario().ObtenerPorLogin(User.Identity.Name);
            OFERTASCURSOS ofertaCurso = new ServicioOfertaCurso().ObtenerPorClave(long.Parse(idOfertaCurso.ToString()));
            esAdmin = usuario.ROLES.Any(a => a.NOMBRE.ToUpper() == "COORDINADOR" || a.NOMBRE.ToUpper() == "ADMINISTRADOR");
            Dictionary<int, string> listaV = new Dictionary<int, string>();
            listaVacia = new SelectList(listaV, "Key", "Value");

            if (idOfertaCurso != 0 && idModulo != 0)
            {
                
                sede = new SelectList(new ServicioSede().ObtenerSedePorCurso(long.Parse(ofertaCurso.CURSO_ID.ToString())), "SEDE_ID", "NOMBRE", ofertaCurso.SEDE_ID);
                curso = new SelectList(new ServicioOfertaCurso().ObternerCursoPorProfesor(usuario.USUARIO_ID), "CURSO_ID", "NOMBRE",ofertaCurso.CURSO_ID);
                turno = new SelectList(new ServicioTurno().ObtenerDiccionarioTurno(), "TURNO_ID", "NOMBRE", ofertaCurso.TURNO_ID);
                modulo = new SelectList(new ServicioOfertasModulos().ObtenerDiccionarioModulosPorOferta(idOfertaCurso),"Key","Value",idModulo.ToString());
            }
            else {

                if(esAdmin) // Consulto Todas Las Sedes
                    sede = new SelectList(new ServicioSede().ObtenerTodos(), "SEDE_ID", "NOMBRE");
                else // Consulto Sedes por Profesor
                    sede = new SelectList(new ServicioSede().ObtenerSedesPorProfesor(usuario.USUARIO_ID), "SEDE_ID", "NOMBRE");
                curso = listaVacia;
                turno = listaVacia;
                modulo = listaVacia;
                profesor = listaVacia;
            }

            vistaModeloNota = new NotaModuloViewModel(sede,curso, modulo,turno, profesor, usuario, esAdmin);
        }
                
        public ActionResult Index()
        {
            iniciarVista(0, 0);
            return View(vistaModeloNota);
        }
      
        [HttpPost]
        public ActionResult Index(FormCollection form)
        {
            try
            {
                decimal idOferta = decimal.Parse(form["curso"]);
                decimal modulo = decimal.Parse(form["modulo"]);
                iniciarVista(idOferta, modulo);
            }
            catch {
                iniciarVista(0, 0);
            }
            return View(vistaModeloNota);
        }
        

        [HttpPost]
        public JsonResult ObtenerTabla(string sede, string curso, string turno, string modulo, string profesor)
        {
            OFERTASCURSOS ofertaCurso = new ServicioOfertaCurso().ObtenerPorSedeCursoTurno(decimal.Parse(sede), decimal.Parse(curso), decimal.Parse(turno));
            decimal profe = decimal.Parse(profesor);
            decimal mod = decimal.Parse(modulo);
            OFERTASMODULOS ofertaModulo = new ServicioOfertasModulos().ObtenerTodos().Where(  u=> u.OFERTA_CURSO_ID == ofertaCurso.OFERTA_CURSO_ID && u.PROFESOR_ID == profe && u.MODULO_ID == mod ).FirstOrDefault();
           
            return Json(new ServicioNotaModulo().ObtenerTablaNota(ofertaModulo.OFERTA_MODULO_ID));
        }

        [HttpPost]
        [ValidateInput(false)]
        public JsonResult GuardarNota(List<string> nota, List<string> valor)
        {

            try{

                ServicioNotaModulo servicioNotaModulo = new ServicioNotaModulo();
                ServicioMatriculaModulo servicioMatriculaModulo=new ServicioMatriculaModulo();
                USUARIOS usuario=new ServicioUsuario().ObtenerPorLogin(User.Identity.Name);
                for (int i = 0; i < nota.Count; i++)
                {
                    string[] id = nota[i].Split('_');
                    NOTASMODULO notaModulo = servicioNotaModulo.ObtenerPorClave(decimal.Parse(id[0]), decimal.Parse(id[1]));
                    if (notaModulo==null)
                    {
                        MATRICULASMODULOS oferta = servicioMatriculaModulo.ObtenerPorClave(decimal.Parse(id[1]));
                        notaModulo = new NOTASMODULO();
                        notaModulo.ALUMNO_ID = oferta.ALUMNO_ID;
                        notaModulo.NOTA = decimal.Parse(valor[i]);
                        notaModulo.USUARIO_ID = usuario.USUARIO_ID;
                        notaModulo.MATRICULAMODULO_ID = oferta.MATRICULAMODULO_ID;
                        servicioNotaModulo.Guardar(notaModulo);
                    }
                    else {
                       
                        notaModulo.NOTA = decimal.Parse(valor[i]);
                        servicioNotaModulo.Modificar(notaModulo);
                    }
                }
                return Json("Ok");
            }catch{
                return Json("");
            }
        }

        [HttpPost]
        public JsonResult ObtenerCursosEnOfertaModulo(string sede, string profesor)
        {

            return Json(new ServicioOfertasModulos().ObtenerCursosPorSedeEnOfertaModulo(long.Parse(sede), profesor));
        }

        [HttpPost]
        public JsonResult ObtenerTurnosEnOfertaModulo(string curso, string sede, string profesor)
        {
            try
            {

                return Json(new ServicioOfertasModulos().ObtenerTurnoEnOfertaModulo(long.Parse(curso), long.Parse(sede), profesor));
            }
            catch
            {
                return Json("");
            }
        }

        [HttpPost]
        public JsonResult ObtenerModulo(string idOfertaCurso)
        {

            //USUARIOS usuario = new ServicioUsuario().ObtenerPorLogin(User.Identity.Name);
            //return Json(new ServicioOfertasModulos().ObtenerTodosModulosPorOfertaPorProfesor(decimal.Parse(idOfertaCurso), usuario.USUARIO_ID));
            return Json(new ServicioOfertasModulos().ObtenerTodosModulosPorOferta(decimal.Parse(idOfertaCurso)));
        }

        [HttpPost]
        public JsonResult ObtenerProfesoresPorModulo(string sede, string curso, string turno, string modulo)
        {

            OFERTASCURSOS ofertaCurso = new ServicioOfertaCurso().ObtenerPorSedeCursoTurno(decimal.Parse(sede), decimal.Parse(curso), decimal.Parse(turno));
            return Json(new ServicioOfertasModulos().ObtenerProfesoresPorOfertaModulo(ofertaCurso.OFERTA_CURSO_ID, decimal.Parse(modulo)));
        }

        [HttpPost]
        public JsonResult ObtenerModulosEnOfertaModulo(string sede, string curso, string turno, string profesor)
        {

            USUARIOS usuario = new ServicioUsuario().ObtenerPorLogin(User.Identity.Name);
            OFERTASCURSOS ofertaCurso = new ServicioOfertaCurso().ObtenerPorSedeCursoTurno(decimal.Parse(sede), decimal.Parse(curso), decimal.Parse(turno));
            return Json(new ServicioOfertasModulos().ObtenerModulosEnOfertaModulo(ofertaCurso.OFERTA_CURSO_ID, profesor));
        }

    }


        

}
