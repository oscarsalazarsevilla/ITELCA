﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ITELCA_CLASSLIBRARY.Models;
using ITELCA_CONTROLESTUDIOS.Models.ViewModels;
using ITELCA_CLASSLIBRARY.Services;
using ITELCA_CLASSLIBRARY.CustomClasses;
using System.Collections.Specialized;

namespace ITELCA_CONTROLESTUDIOS.Controllers
{
     [Authorize(Roles = "ADMINISTRADOR,COORDINADOR,ESTUDIANTE,EMPLEADO,CAJERO")]
    public class MatriculaTallerController : Controller
    {
        //
        // GET: /Rol/
        MatriculaTallerViewModel vistaModelo;
        ServicioMatriculaTaller servicioMatriculaTaller = new ServicioMatriculaTaller();
        ServicioMatriculaModulo servicioMatriculaModulo = new ServicioMatriculaModulo();
        ServicioAsistencia servicioAsistencia = new ServicioAsistencia();
        //ServicioMensualidad servicioMesualidad = new ServicioMensualidad();

        SelectList activo;
        SelectList listaTaller;
        SelectList listaSedes;
        SelectList listaTalleresGrid;
        SelectList generos;
        public void IniciarVista(MATRICULASTALLERES matriculaTaller) {
            USUARIOS usuario = new ServicioUsuario().ObtenerPorLogin(User.Identity.Name);
            Dictionary<int, string> lista = new Dictionary<int, string>();
            lista.Add(1, "SI");
            lista.Add(0, "NO");
            listaTaller=new SelectList( new ServicioTaller().ObtenerTodos(),"TALLER_ID","NOMBRE");
            generos = new SelectList(new ServicioGenero().ObtenerLista(), "GENERO_ID", "NOMBRE", usuario.GENERO_ID);
            
            vistaModelo = new MatriculaTallerViewModel(matriculaTaller, activo,listaSedes, listaTaller,listaTalleresGrid,generos);
        }

        public ActionResult Inscripcion()
        {
            IniciarVista(null);
            return View(vistaModelo);
        }

        public ActionResult InscripcionManualTaller()
        {
            IniciarVista(null);
            return View(vistaModelo);
        }


        public ActionResult Listar() {
            return View();
        }


        [HttpPost]
        public JsonResult Eliminar(int id)
        {
            return Json(servicioMatriculaTaller.Eliminar(id));
        }

        [HttpPost]
        public JsonResult ObtenerData( string sidx, string sord, int page, int rows, string filters)
        {
             USUARIOS usuario = new ServicioUsuario().ObtenerPorLogin(User.Identity.Name);
            object resultado = servicioMatriculaTaller.ObtenerDataGrid(usuario.USUARIO_ID, sidx, sord, page, rows, filters);
            return Json(resultado);

        }
        [HttpPost]
        public JsonResult ObtenerSede(string curso)
        {

            return Json(new ServicioOfertaCurso().ObtenerSede(long.Parse(curso)));
        }

        [HttpPost]
        public JsonResult ObtenerTurno(string curso,string sede)
        {
            try
            {

                return Json(new ServicioOfertaCurso().ObtenerTurno(long.Parse(curso), long.Parse(sede)));
            }
            catch {
                return Json("");
            }
        }

        [HttpPost]
        public JsonResult ObtenerOfertaTaller(string tallerId)
        {
            try
            {
                return Json(new ServicioOfertasTalleres().ObtenerOfertaTalleres(decimal.Parse(tallerId)));

            }
            catch {
                return Json("");
            }
        }

        [HttpPost]
        public JsonResult ObtenerInfoTallerPorId(string idOfertaTaller)
        {

            string[] aux = idOfertaTaller.Split('_');
            return Json(new ServicioOfertasTalleres().ObtenerObjetoPorId(decimal.Parse(idOfertaTaller)));
        }
        [HttpPost]
        public JsonResult GuardarMatricula(string idOfertaTaller, decimal monto, string formaP)
        {

            MATRICULASTALLERES matriculaTaller;
            
            OFERTASTALLERES oferta = new ServicioOfertasTalleres().ObtenerPorClave(decimal.Parse(idOfertaTaller));
            USUARIOS usuario = new ServicioUsuario().ObtenerPorLogin(User.Identity.Name);

            //guardo el modulo seleccionado
            matriculaTaller = GenerarObjetoMatriculaTaller(usuario, oferta);
            servicioMatriculaTaller.Guardar(matriculaTaller);
            //ingreso modulo seleccionado por el alumno
                return Json("");

        }

        [HttpPost]
        public JsonResult GuardarMatriculaManual(string idOfertaTaller,string idPromotor,string form)
        {
            List<object> lista = new List<object>();
            try{

                MATRICULASTALLERES matriculaTaller;
                USUARIOS usuario,promotor;
                ServicioUsuario servicioUsuario = new ServicioUsuario();
                usuario = servicioUsuario.ObtenerPorLogin(User.Identity.Name); 
                ServicioConceptoTaller servicioConceptoTaller=new ServicioConceptoTaller();
                OFERTASTALLERES oferta = new ServicioOfertasTalleres().ObtenerPorClave(decimal.Parse(idOfertaTaller));
            
                
                if (idPromotor != null && idPromotor != "")
                    promotor = servicioUsuario.ObtenerPorClave(decimal.Parse(idPromotor));
                else
                    promotor = null;
                USUARIOS alumno = GenerarObjetoAlumno(form,0);
                //guardo el modulo seleccionado
                matriculaTaller = GenerarObjetoMatriculaTaller(alumno, oferta);
                if(promotor!=null)
                    matriculaTaller.EMPLEADO_ID = promotor.USUARIO_ID;
                matriculaTaller.USUARIO_ID = usuario.USUARIO_ID;
                servicioMatriculaTaller.Guardar(matriculaTaller);
                try
                {
                    if (servicioMatriculaTaller.ValidarTallerInscrito(matriculaTaller.OFERTA_TALLER_ID))
                    {
                        IEnumerable <CONCEPTOS_TALLERES> listaConceptos=servicioConceptoTaller.ObtenerConceptoTallerPorIdDeTaller(oferta.TALLER_ID);
                        if (listaConceptos.Count()>0 )
                        {
                            foreach (var concepto in listaConceptos)
                            {
                                CONCEPTOS_ESTUDIANTES conc_est = new CONCEPTOS_ESTUDIANTES();
                                conc_est.FECHA_VENCIMIENTO = DateTime.Now;
                                conc_est.MATRICULATALLER_ID = matriculaTaller.MATRICULATALLER_ID;
                                conc_est.CONCEPTO_TALLER_ID = concepto.CONCEPTO_TALLER_ID;
                                conc_est.MONTO = concepto.MONTO;
                                conc_est.CONDICION = "P"; //Pendiente por pagar
                                conc_est.DESCRIPCION = "MATRICULA TALLER " + concepto.TALLERES.NOMBRE;
                                conc_est.FECHA_CREACION = DateTime.Now.ToString();
                                new ServicioConceptoEstudiante().Guardar(conc_est);
                            }
                            lista.Add(new { ok = 1, msg = "Taller Inscrito :)" });
                        }else
                            lista.Add(new { ok = 0, msg = "Concepto talleres no encontrado" });
                    }
                    else {
                        lista.Add(new { ok = 0, msg = "Alumno Ya esta Inscrito En ese Taller :(" });
                    }
                }
                catch {
                    lista.Clear();
                    lista.Add(new { ok = 0, msg = "Error Guardando Concepto Estudiante" });
                   return Json(lista);
                }
                //ingreso modulo seleccionado por el alumno
                return Json( lista);
            }catch{
                 lista.Clear();
                lista.Add(new { ok = 0, msg = "Error,Taller No Inscrito :(" });
                return Json(lista);
            }

        }
        [HttpPost]
        public JsonResult obtenerPago(string idOfertaTaller)
        {
            string[] aux = idOfertaTaller.Split('_');
            OFERTASTALLERES oferta = new ServicioOfertasTalleres().ObtenerPorClave(decimal.Parse(idOfertaTaller));
            List<object> lista = new List<object>();
            USUARIOS usuario = new ServicioUsuario().ObtenerPorLogin(User.Identity.Name);
            lista.Add(new
            {
                pagos = 1,
                monto = oferta.MONTO 

            });
            return Json(lista);

        }

        [HttpPost]
        public JsonResult GuardarNuevoEstudiante(string form)
        {
            List<object> lista = new List<object>();
            try
            {
                USUARIOS user = GenerarObjetoAlumno(form,1);
                lista.Add(new { id = user.USUARIO_ID });
            }
            catch
            {
                lista.Clear();
                lista.Add(new { id = "" });
            }
            return Json(lista);

        }

        [HttpPost]
        public JsonResult ObtenerTallerInscritosPorAlumno(string documento)
        {

            return Json(servicioMatriculaTaller.ObtenerTalleresInscritosPorAlumno(Decimal.Parse(documento)));
        }
        public USUARIOS GenerarObjetoAlumno(string urlSerialized,int ban)
        {
            USUARIOS obj;

            string query = System.Web.HttpUtility.UrlDecode(urlSerialized);
            NameValueCollection result = System.Web.HttpUtility.ParseQueryString(query);
            ServicioUsuario servicioUsuario = new ServicioUsuario();
            if (result["USUARIOTALLER.USUARIO_ID"] != null && result["USUARIOTALLER.USUARIO_ID"] != "")
            {
                obj = servicioUsuario.ObtenerPorClave(decimal.Parse(result["USUARIOTALLER.USUARIO_ID"].ToString()));
                if (ban == 1)
                {//si quiero modificar el alumno
                    if (result["USUARIOTALLER.NRO_DOCUMENTO"] != "")
                        obj.NRO_DOCUMENTO = result["USUARIOTALLER.NRO_DOCUMENTO"].Trim();
                    obj.APELLIDO = result["USUARIOTALLER.APELLIDO"];
                    obj.NOMBRE = result["USUARIOTALLER.NOMBRE"];
                    obj.TELEFONO = result["USUARIOTALLER.TELEFONO"];
                    obj.TELEFONO2 = result["USUARIOTALLER.TELEFONO2"];
                    obj.EMAIL = result["USUARIOTALLER.EMAIL"];
                    obj.DIRECCION = result["USUARIOTALLER.DIRECCION"];
                    if (result["USUARIOTALLER.FECHADENACIMIENTO"] != null)
                        obj.FECHADENACIMIENTO = DateTime.Parse(result["USUARIOTALLER.FECHADENACIMIENTO"]);
                    obj.GENERO_ID = decimal.Parse(result["USUARIOTALLER.GENERO_ID"]);
                    servicioUsuario.Modificar(obj);

                }
                return obj;
            }
            else
            {
                obj = new USUARIOS();
                if (result["USUARIOTALLER.NRO_DOCUMENTO"] != "")
                    obj.NRO_DOCUMENTO = result["USUARIOTALLER.NRO_DOCUMENTO"].Trim();
                obj.NOMBREUSUARIO = result["USUARIOTALLER.NRO_DOCUMENTO"];
                obj.APELLIDO = result["USUARIOTALLER.APELLIDO"];
                obj.NOMBRE = result["USUARIOTALLER.NOMBRE"];
                obj.TELEFONO = result["USUARIOTALLER.TELEFONO"];
                obj.TELEFONO2 = result["USUARIOTALLER.TELEFONO2"];
                obj.EMAIL = result["USUARIOTALLER.EMAIL"];
                obj.DIRECCION = result["USUARIOTALLER.DIRECCION"];
                if (result["USUARIOTALLER.FECHADENACIMIENTO"] != null)
                    obj.FECHADENACIMIENTO = DateTime.Parse(result["USUARIOTALLER.FECHADENACIMIENTO"]);
                obj.GENERO_ID = decimal.Parse(result["USUARIOTALLER.GENERO_ID"]);
                obj.ESTABLOQUEADO = 0;
                obj.ESTAENLINEA = 0;
                new ServicioUsuario().GuardarEstudiante(obj);
                return obj;
            }

        }

        public MATRICULASTALLERES GenerarObjetoMatriculaTaller(USUARIOS usuario,OFERTASTALLERES oferta)
        {
            MATRICULASTALLERES matriculaTaller = new MATRICULASTALLERES();
            matriculaTaller.ALUMNO_ID = usuario.USUARIO_ID;
            matriculaTaller.CONDICION = "C";
            matriculaTaller.OFERTA_TALLER_ID = oferta.OFERTA_TALLER_ID;
            matriculaTaller.MONTO = oferta.MONTO;
            matriculaTaller.FECHA_INSCRIPCION = DateTime.Now;
            matriculaTaller.TIENEDCTO  = 0;
            matriculaTaller.FECHA_PAGO=DateTime.Now;
            matriculaTaller.ALUMNO_ID = usuario.USUARIO_ID;
            return matriculaTaller;
        }
    }
}
