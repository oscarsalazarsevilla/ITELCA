﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ITELCA_CLASSLIBRARY.Models;
using ITELCA_CONTROLESTUDIOS.Models.ViewModels;
using ITELCA_CLASSLIBRARY.Services;
using Utilidades;

namespace ITELCA_CONTROLESTUDIOS.Controllers
{
     [Authorize(Roles = "ADMINISTRADOR,COORDINADOR")]
    public class ConceptoCursoController : Controller
    {
        ConceptoCursoViewModel vistaModelo;
        ServicioConceptoCurso servicioConceptoCurso = new ServicioConceptoCurso();
        SelectList activo, ListaCursos, ListaConceptos, ListaTurnos;
        MultiSelectList multiCursos, multiTurnos;
        
        public void IniciarVista(CONCEPTOS_CURSOS conceptoCurso) {

            Dictionary<int, string> lista = new ListasGenerales().ListaActivo();

            if (conceptoCurso != null)
            {
                activo = new SelectList(lista, "Key", "Value", conceptoCurso.ESTADO);
                ListaCursos = new SelectList(new ServicioCursos().ObtenerTodos(), "CURSO_ID", "NOMBRE", conceptoCurso.CURSO_ID);
                ListaTurnos = new SelectList(new ServicioTurno().ObtenerTodos(), "TURNO_ID", "NOMBRE", conceptoCurso.TURNO_ID);
                ListaConceptos = new SelectList(new ServicioConcepto().ObtenerTodos(), "CONCEPTO_ID", "NOMBRE", conceptoCurso.CONCEPTO_ID);
            }
            else
            {
                activo = new SelectList(lista, "Key", "Value");
                ListaCursos = new SelectList(new Dictionary<int, string>(), "Key", "Value");
                multiCursos = new MultiSelectList(new ServicioCursos().ObtenerTodos(), "CURSO_ID", "NOMBRE");
                multiTurnos = new MultiSelectList(new ServicioTurno().ObtenerTodos(), "TURNO_ID", "NOMBRE");
                ListaConceptos = new SelectList(new ServicioConcepto().ObtenerTodos(), "CONCEPTO_ID", "NOMBRE");
            }
            vistaModelo = new ConceptoCursoViewModel(conceptoCurso, activo, ListaCursos,ListaConceptos,ListaTurnos,multiCursos, multiTurnos);
        }

        public ActionResult Crear()
        {
            IniciarVista(null);
            return View(vistaModelo);
        }
     
        public ActionResult Editar(int id) {
            CONCEPTOS_CURSOS conceptoCurso = servicioConceptoCurso.ObtenerPorClave(id);
            if (conceptoCurso != null)
            {
                IniciarVista(conceptoCurso);
                return View(vistaModelo);
            }
            else
            {
                TempData["mensaje"] = "Concepto Curso no Encontrado.";
                return RedirectToAction("Listar");
            }
        }

        public ActionResult Listar() {
            return View();
        }

        [HttpPost]
        public ActionResult Crear([Bind(Prefix = "CONCEPTOS_CURSOS")] CONCEPTOS_CURSOS conceptoCurso, FormCollection collection)
        {
            conceptoCurso.USUARIO_ID = new ServicioUsuario().ObtenerPorLogin(User.Identity.Name).USUARIO_ID;
            if (ModelState.IsValid) {
                if (!String.IsNullOrEmpty(collection["CONCEPTOS_CURSOS.CURSO_ID"]) ||
                    !String.IsNullOrEmpty(collection["CONCEPTOS_CURSOS.TURNO_ID"]))
                {
                    conceptoCurso.MULTI_CURSO_ID = Request["CONCEPTOS_CURSOS.CURSO_ID"].ToString();
                    conceptoCurso.MULTI_TURNO_ID = Request["CONCEPTOS_CURSOS.TURNO_ID"].ToString();
                    USUARIOS usuario = new ServicioUsuario().ObtenerPorLogin(User.Identity.Name);
                    conceptoCurso.USUARIO_ID = usuario.USUARIO_ID;

                    if (servicioConceptoCurso.Guardar(conceptoCurso))
                    {
                        TempData["mensaje"] = "Concepto Curso Guardado Satisfactoriamente.";
                        return RedirectToAction("Listar");
                    }
                    else
                        ModelState.AddModelError(string.Empty, "Error Guardando Concepto Curso");
                }
                else
                    ModelState.AddModelError("CURSO_ID", "Debe seleccionar al menos un curso y un turno.");
                
            }
            IniciarVista(null);
            return View(vistaModelo);

        }

        [HttpPost]
        public ActionResult Editar([Bind(Prefix = "CONCEPTOS_CURSOS")] CONCEPTOS_CURSOS conceptoCurso)
        {
            if (ModelState.IsValid)
            {
                //if (!servicioConceptoCurso.Duplicado(conceptoCurso))
                //{
    
                    conceptoCurso.USUARIO_ID = new ServicioUsuario().ObtenerPorLogin(User.Identity.Name).USUARIO_ID;
                    if (servicioConceptoCurso.Modificar(conceptoCurso))
                    {
                        TempData["mensaje"] = "ConceptoCurso Modificado Satisfactoriamente";
                        return RedirectToAction("Listar");
                    }
                    else
                        ModelState.AddModelError(string.Empty, "Error Modificando ConceptoCurso");
                /*}
                else
                    ModelState.AddModelError(string.Empty, "ConceptoCurso Duplicado.");*/

            }
            IniciarVista(conceptoCurso);
            return View(vistaModelo);

        }

       
        [HttpPost]
        public JsonResult Eliminar(int id)
        {
            return Json(servicioConceptoCurso.Eliminar(id));
        }

        [HttpPost]
        public JsonResult ObtenerData(string sidx, string sord, int page, int rows, string filters)
        {
            object resultado = servicioConceptoCurso.ObtenerDataGrid(sidx, sord, page, rows, filters);
            return Json(resultado);

        }

    }
}
