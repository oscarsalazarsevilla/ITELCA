﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ITELCA_CLASSLIBRARY.Models;
using ITELCA_CONTROLESTUDIOS.Models.ViewModels;
using ITELCA_CLASSLIBRARY.Services;

namespace ITELCA_CONTROLESTUDIOS.Controllers
{
     [Authorize(Roles = "ADMINISTRADOR,COORDINADOR")]
    public class DiasController : Controller
    {
        //
        // GET: /Dias/
        DiaViewModel vistaModeloDias;
        SelectList activo;

        public void IniciarVista(DIAS dia)
        {
            Dictionary<int, string> lista = new Dictionary<int, string>();
            lista.Add(1, "SI");
            lista.Add(0, "NO");
            if (dia != null)
            {
                activo = new SelectList(lista, "Key", "Value", dia.ESTADO);
            }
            else
            {
                dia = new DIAS();
                activo = new SelectList(lista, "Key", "Value");
            }
            vistaModeloDias = new DiaViewModel(dia, activo);
        }

        public ActionResult Crear()
        {
            IniciarVista(null);
            return View(vistaModeloDias);
        }

        public ActionResult Editar(int id)
        {
            DIAS dias = new ServicioDia().ObtenerPorClave(id);
            if (dias != null)
            {
                IniciarVista(dias);
                return View(vistaModeloDias);
            }
            else
            {
                TempData["mensaje"] = "Día no Encontrado";
                return RedirectToAction("Listar");
            }
        }

        public ActionResult Listar()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Crear(DIAS dia)
        {
            if (ModelState.IsValid)
            {
                ServicioDia servicioDia = new ServicioDia();
                if (!servicioDia.Duplicado(dia))
                {
                    USUARIOS usuario = new ServicioUsuario().ObtenerPorLogin(User.Identity.Name);
                    dia.USUARIO_ID = usuario.USUARIO_ID;
                    if (servicioDia.Guardar(dia))
                    {
                        TempData["mensaje"] = "Día Guardado Satisfactoriamente";
                        return RedirectToAction("Listar");
                    }
                    else
                        ModelState.AddModelError(string.Empty, "Error Guardando el Día");
                }
                else
                    ModelState.AddModelError(string.Empty, "El día ya existe");

            }
            IniciarVista(dia);
            return View(vistaModeloDias);

        }

        [HttpPost]
        public ActionResult Editar(DIAS dia)
        {
            if (ModelState.IsValid)
            {
                ServicioDia servicioDia = new ServicioDia();
                if (!servicioDia.Duplicado(dia))
                {
                    if (servicioDia.Modificar(dia))
                    {
                        TempData["mensaje"] = "Día Modificado Satisfactoriamente";
                        return RedirectToAction("Listar");
                    }
                    else
                        ModelState.AddModelError(string.Empty, "Error Modificando Dias");
                }
                else
                    ModelState.AddModelError(string.Empty, "Día ya existe");
            }

            IniciarVista(dia);
            return View(vistaModeloDias);
        }

        [HttpPost]
        public JsonResult Eliminar(int id)
        {
            return Json(new ServicioDia().Eliminar(id));
        }
        [HttpPost]
        public JsonResult ObtenerData(string sidx, string sord, int page, int rows, string filters)
        {
            object resultado = new ServicioDia().ObtenerDataGrid(sidx, sord, page, rows, filters);
            return Json(resultado);
        }
    }
}
