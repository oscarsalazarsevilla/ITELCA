﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ITELCA_CLASSLIBRARY.Models;
using ITELCA_CONTROLESTUDIOS.Models.ViewModels;
using ITELCA_CLASSLIBRARY.Services;

namespace ITELCA_CONTROLESTUDIOS.Controllers
{
    [Authorize(Roles = "ADMINISTRADOR,COORDINADOR")]
    public class CostosCursosController : Controller
    {
        //
        // GET: /Rol/
        CostosCursosViewModel vistaModeloCostosCursos;
        //ServicioCostosCursos servicioCostosCursos;
        SelectList ListaTurnos;
        SelectList ListaCursos;
        
        public void IniciarVista(COSTOS_CURSOS costos_cursos)
        {

            if (costos_cursos != null)
            {
                ListaTurnos = new SelectList(new ServicioTurno().ObtenerTodos(), "TURNO_ID", "NOMBRE", costos_cursos.TURNO_ID);
                ListaCursos = new SelectList(new ServicioCursos().ObtenerTodos(), "CURSO_ID", "NOMBRE", costos_cursos.CURSO_ID);
            }
            else
            {
                ListaTurnos = new SelectList(new ServicioTurno().ObtenerTodos(), "TURNO_ID", "NOMBRE");
                ListaCursos = new SelectList(new ServicioCursos().ObtenerTodos(), "CURSO_ID", "NOMBRE");
            }
            vistaModeloCostosCursos = new CostosCursosViewModel(costos_cursos,ListaCursos,ListaTurnos);
        }

        public ActionResult Crear()
        {
            IniciarVista(null);
            return View(vistaModeloCostosCursos);
        }
        
        public ActionResult Editar(int id) { 
            COSTOS_CURSOS curso=new ServicioCostosCursos().ObtenerPorClave(id);
            if (curso != null)
            {
                IniciarVista(curso);
                return View(vistaModeloCostosCursos);
            }
            else
            {
                TempData["mensaje"] = "Costos Cursos no Encontrado";
                return RedirectToAction("Listar");
            }
        }

        public ActionResult Listar() {
            IniciarVista(null);
            return View(vistaModeloCostosCursos);
        }

        [HttpPost]
        public ActionResult Crear(COSTOS_CURSOS COSTOS_CURSOS, FormCollection form)
        {
            if (ModelState.IsValid) { 
                ServicioCostosCursos servicioCostoCursos=new ServicioCostosCursos();
                if (!servicioCostoCursos.Duplicado(COSTOS_CURSOS))
                {
                    USUARIOS usuario = new ServicioUsuario().ObtenerPorLogin(User.Identity.Name);
                    COSTOS_CURSOS.USUARIO_ID = usuario.USUARIO_ID;
                    if (servicioCostoCursos.Guardar(COSTOS_CURSOS))
                    {
                        TempData["mensaje"] = "Costo Curso Guardado Satisfactoriamente";
                        return RedirectToAction("Listar");
                    }
                    else
                        ModelState.AddModelError(string.Empty, "Error Guardando Costo Curso");
                }
                else 
                    ModelState.AddModelError(string.Empty, "Costo Curso ya existe");
                
            }
            IniciarVista(COSTOS_CURSOS);
            return View(vistaModeloCostosCursos);

        }

        [HttpPost]
        public ActionResult Editar(COSTOS_CURSOS COSTOS_CURSOS)
        {
            if (ModelState.IsValid)
            {
                ServicioCostosCursos servicioCostosCursos = new ServicioCostosCursos();
                if (!servicioCostosCursos.Duplicado(COSTOS_CURSOS))
                {
                    USUARIOS usuario = new ServicioUsuario().ObtenerPorLogin(User.Identity.Name);
                    COSTOS_CURSOS.USUARIO_ID = usuario.USUARIO_ID;
                    if (servicioCostosCursos.Modificar(COSTOS_CURSOS))
                    {
                        TempData["mensaje"] = "Costo Curso Modificado Satisfactoriamente";
                        return RedirectToAction("Listar");
                    }
                    else
                        ModelState.AddModelError(string.Empty, "Error Modificando Costo Curso");
                }
                else
                    ModelState.AddModelError(string.Empty, "Costo Curso ya existe");

            }
            IniciarVista(COSTOS_CURSOS);
            return View(vistaModeloCostosCursos);

        }

        [HttpPost]
        public JsonResult Eliminar(int id)
        {
            return Json(new ServicioCostosCursos().Eliminar(id));
        }
        [HttpPost]
        public JsonResult ObtenerData(string id, string sidx, string sord, int page, int rows, string filters)
        {
            if (id == "")
                id = "0";
            object resultado = new ServicioCostosCursos().ObtenerDataGrid(Decimal.Parse(id),sidx, sord, page, rows, filters);
            return Json(resultado);

        }

        [HttpPost]
        public string ObtenerMontoPorCurso(string CURSO_ID, string TURNO_ID)
        {
            try
            {
                COSTOS_CURSOS costos_cursos = new ServicioCostosCursos().ObtenerPorCursoyTurno(Int32.Parse(CURSO_ID), Int32.Parse(TURNO_ID));
                if (costos_cursos == null)
                    return "0";
                return costos_cursos.MONTO.ToString();
            }
            catch {
                return "";
            }
        }



    }
}
