﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ITELCA_CLASSLIBRARY.Models;
using ITELCA_CONTROLESTUDIOS.Models.ViewModels;
using ITELCA_CLASSLIBRARY.Services;
using ITELCA_CLASSLIBRARY.CustomClasses;
using System.Web.Script.Serialization;

namespace ITELCA_CONTROLESTUDIOS.Controllers
{
    [Authorize(Roles = "PROFESOR,COORDINADOR,ADMINISTRADOR")]
    public class AsistenciaController : Controller
    {
        //
        // GET: /Asistencia/
        private AsistenciaViewModel vistaModeloAsistencia;
        private SelectList curso;
        private SelectList modulo;
        private SelectList sede;
        private SelectList profesor;
        public void iniciarVista()
        {
            USUARIOS usuario=new ServicioUsuario().ObtenerPorLogin(User.Identity.Name);
            if (usuario.ROLES.Where(u => u.NOMBRE == "PROFESOR").Count() > 0)
            {
                curso = new SelectList(new ServicioOfertaCurso().ObternerCursoPorProfesor(usuario.USUARIO_ID), "Key", "Value");
                profesor = new SelectList(new Dictionary<decimal, string> { }, "Key", "Value");
            }
            else
            {
                curso = new  SelectList(new Dictionary<decimal, string> { },"Key","Value");
                sede = new SelectList(new ServicioSede().ObtenerTodos(), "SEDE_ID", "NOMBRE");
                profesor = new SelectList(new ServicioUsuario().ObtenerListaProfesores(), "Key", "Value");
            }
            modulo = new SelectList(new Dictionary<decimal, string> { },"Key","Value");
            vistaModeloAsistencia = new AsistenciaViewModel(curso,modulo,usuario,profesor,sede);
        }
                
        public ActionResult Index()
        {
            iniciarVista();
            return View(vistaModeloAsistencia);
        }

        [HttpPost]
        public JsonResult ObtenerCurso(string idSede)
        {
            return Json(new ServicioOfertaCurso().ObtenerTodosCursosActivosPorSede(decimal.Parse(idSede)));
        }
        [HttpPost]
        public JsonResult  ObtenerModulo(string idOfertaCurso){
            return Json(new ServicioOfertasModulos().ObtenerTodosModulosPorOferta(decimal.Parse(idOfertaCurso)));
        }
         
        [HttpPost]
        public JsonResult ObtenerTabla(string idOfertaModulo)
        {
            return Json(new ServicioAsistencia().ObtenerTablaAsistencia(decimal.Parse(idOfertaModulo)));
        }

        [HttpPost]
        [ValidateInput(false)]
        public JsonResult GuardarAsistencia(List<string> asistenciaId, List<string> valor)
        {

            try
            {
                
                ServicioAsistencia servicioAsistencia = new ServicioAsistencia();
                ServicioMatriculaModulo servicioMatriculaModulo = new ServicioMatriculaModulo();
                USUARIOS usuario = new ServicioUsuario().ObtenerPorLogin(User.Identity.Name);
                for (int i = 0; i < asistenciaId.Count; i++)
                {
                    string[] id = asistenciaId[i].Split('_');
                    DateTime fecha=DateTime.Parse(id[2]+"/"+id[3]+"/"+id[4]);
                    ASISTENCIAS asistencia = servicioAsistencia.ObtenerPorClave(decimal.Parse(id[0]), decimal.Parse(id[1]),fecha);
                    if (asistencia == null)
                    {
                        MATRICULASMODULOS oferta = servicioMatriculaModulo.ObtenerPorClave(decimal.Parse(id[1]));
                        asistencia = new ASISTENCIAS();
                        asistencia.ALUMNO_ID = oferta.ALUMNO_ID;
                        asistencia.FECHA_CLASE = fecha;
                        asistencia.MATRICULAMODULO_ID = oferta.MATRICULAMODULO_ID;
                        asistencia.USUARIO_ID = usuario.USUARIO_ID;
                        servicioAsistencia.Guardar(asistencia);
                    }
                    else
                    {

                        asistencia.ASISTIO = decimal.Parse(valor[i]);
                        servicioAsistencia.Modificar(asistencia);
                    }
                }
                return Json("Ok");
            }
            catch
            {
                return Json("");
            }
        }
    }
}
