﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ITELCA_CLASSLIBRARY.Models;
using ITELCA_CONTROLESTUDIOS.Models.ViewModels;
using ITELCA_CLASSLIBRARY.Services;

namespace ITELCA_CONTROLESTUDIOS.Controllers
{
     [Authorize(Roles = "ADMINISTRADOR,COORDINADOR")]
    public class SedeController : Controller
    {
        //
        // GET: /Rol/
        SedeViewModel vistaModeloSede;
        SelectList activo;
        public void IniciarVista(SEDES sede) {
            Dictionary<int, string> lista = new Dictionary<int, string>();
            lista.Add(1, "SI");
            lista.Add(0, "NO");
            if (sede != null)
                activo = new SelectList(lista, "Key", "Value", sede.ESTADO);
            else
                activo = new SelectList(lista, "Key", "Value");
            vistaModeloSede = new SedeViewModel(sede, activo);
        }

        public ActionResult Crear()
        {
            IniciarVista(null);
            return View(vistaModeloSede);
        }
     
        public ActionResult Editar(int id) { 
            SEDES sede=new ServicioSede().ObtenerPorClave(id);
            if (sede != null)
            {
                IniciarVista(sede);
                return View(vistaModeloSede);
            }
            else
            {
                TempData["mensaje"] = "Sede no Encontrada";
                return RedirectToAction("Listar");
            }
        }

        public ActionResult Listar() {
            return View();
        }

        [HttpPost]
        public ActionResult Crear(SEDES sede) {
            if (ModelState.IsValid) { 
                ServicioSede servicioSede=new ServicioSede();
                if (!servicioSede.Duplicado(sede))
                {
                    if (servicioSede.Guardar(sede))
                    {
                        TempData["mensaje"] = "Sede Guardada Satisfactoriamente";
                        return RedirectToAction("Listar");
                    }
                    else
                        ModelState.AddModelError(string.Empty, "Error Guardando Sede");
                }
                else 
                    ModelState.AddModelError(string.Empty, "Sede ya existe");
                
            }
            IniciarVista(sede);
            return View(vistaModeloSede);

        }

        [HttpPost]
        public ActionResult Editar(SEDES sede)
        {
            if (ModelState.IsValid)
            {
                ServicioSede servicioSede = new ServicioSede();
                if (!servicioSede.Duplicado(sede))
                {
                    USUARIOS usuario = new ServicioUsuario().ObtenerPorLogin(User.Identity.Name);
                    sede.USUARIO_ID = usuario.USUARIO_ID;
                    if (servicioSede.Modificar(sede))
                    {
                        TempData["mensaje"] = "Sede Modificada Satisfactoriamente";
                        return RedirectToAction("Listar");
                    }
                    else
                        ModelState.AddModelError(string.Empty, "Error Modificando Sede");
                }
                else
                    ModelState.AddModelError(string.Empty, "Sede ya existe");

            }
            IniciarVista(sede);
            return View(vistaModeloSede);

        }

        [HttpPost]
        public JsonResult Eliminar(int id)
        {
            return Json(new ServicioSede().Eliminar(id));
        }
        [HttpPost]
        public JsonResult ObtenerData(string sidx, string sord, int page, int rows, string filters)
        {
            object resultado = new ServicioSede().ObtenerDataGrid(sidx, sord, page, rows, filters);
            return Json(resultado);

        }




    }
}
