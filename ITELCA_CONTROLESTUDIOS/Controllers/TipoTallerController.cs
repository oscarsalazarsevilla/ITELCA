﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ITELCA_CLASSLIBRARY.Models;
using ITELCA_CONTROLESTUDIOS.Models.ViewModels;
using ITELCA_CLASSLIBRARY.Services;

namespace ITELCA_CONTROLESTUDIOS.Controllers
{
     [Authorize(Roles = "ADMINISTRADOR,COORDINADOR")]
    public class TipoTallerController : Controller
    {
        //
        // GET: /Rol/
        TipoTallerViewModel vistaModeloTipoTaller;
        SelectList activo;
        public void IniciarVista(TIPOSTALLERES tipoTaller) {
            Dictionary<int, string> lista = new Dictionary<int, string>();
            lista.Add(1, "SI");
            lista.Add(0, "NO");
            if (tipoTaller != null)
                activo = new SelectList(lista, "Key", "Value", tipoTaller.ESTADO);
            else
                activo = new SelectList(lista, "Key", "Value");
            vistaModeloTipoTaller = new TipoTallerViewModel(tipoTaller, activo);
        }

        public ActionResult Crear()
        {
            IniciarVista(null);
            return View(vistaModeloTipoTaller);
        }
        
        public ActionResult Editar(int id) { 
            TIPOSTALLERES tipoTaller=new ServicioTipoTaller().ObtenerPorClave(id);
            if (tipoTaller != null)
            {
                IniciarVista(tipoTaller);
                return View(vistaModeloTipoTaller);
            }
            else
            {
                TempData["mensaje"] = "Tipo Taller no Encontrada";
                return RedirectToAction("Listar");
            }
        }

        public ActionResult Listar() {
            return View();
        }

        [HttpPost]
        public ActionResult Crear(TIPOSTALLERES tipoTaller)
        {
            if (ModelState.IsValid) {
                ServicioTipoTaller servicioTipoTaller = new ServicioTipoTaller();
                if (!servicioTipoTaller.Duplicado(tipoTaller))
                {

                    USUARIOS usuario = new ServicioUsuario().ObtenerPorLogin(User.Identity.Name);
                    tipoTaller.USUARIO_ID = usuario.USUARIO_ID;
                    tipoTaller.ESTADO = 1;
                    tipoTaller.FECHA_CREACION = DateTime.Now;
                   
                    if (servicioTipoTaller.Guardar(tipoTaller))
                    {
                        TempData["mensaje"] = "Tipo Taller Guardado Satisfactoriamente";
                        return RedirectToAction("Listar");
                    }
                    else
                        ModelState.AddModelError(string.Empty, "Error Guardando Tipo Taller");
                }
                else 
                    ModelState.AddModelError(string.Empty, "Tipo Taller ya existe");
                
            }
            IniciarVista(tipoTaller);
            return View(vistaModeloTipoTaller);

        }

        [HttpPost]
        public ActionResult Editar(TIPOSTALLERES tipoTaller)
        {
            if (ModelState.IsValid)
            {
                ServicioTipoTaller servicioTipoTaller = new ServicioTipoTaller();
                if (!servicioTipoTaller.Duplicado(tipoTaller))
                {
                    if (servicioTipoTaller.Modificar(tipoTaller))
                    {
                        TempData["mensaje"] = "Tipo Taller Modificada Satisfactoriamente";
                        return RedirectToAction("Listar");
                    }
                    else
                        ModelState.AddModelError(string.Empty, "Error Modificando TipoTaller");
                }
                else
                    ModelState.AddModelError(string.Empty, "Tipo Taller ya existe");

            }
            IniciarVista(tipoTaller);
            return View(vistaModeloTipoTaller);

        }

        [HttpPost]
        public JsonResult Eliminar(int id)
        {
            return Json(new ServicioTipoTaller().Eliminar(id));
        }

        [HttpPost]
        public JsonResult ObtenerData(string sidx, string sord, int page, int rows, string filters)
        {
            object resultado = new ServicioTipoTaller().ObtenerDataGrid(sidx, sord, page, rows, filters);
            return Json(resultado);

        }

       

    }
}
