﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ITELCA_CLASSLIBRARY.Models;
using ITELCA_CONTROLESTUDIOS.Models.ViewModels;
using ITELCA_CLASSLIBRARY.Services;
using ITELCA_CLASSLIBRARY.CustomClasses;
using System.Web.Script.Serialization;

namespace ITELCA_CONTROLESTUDIOS.Controllers
{
     [Authorize(Roles = "ADMINISTRADOR,COORDINADOR")]
    public class TurnoViernesController : Controller
    {
        //
        // GET: /Rol/
        TurnoViernesViewModel vistaModeloTurnoViernes;
        ServicioTurnoViernes _servicio = new ServicioTurnoViernes();
        SelectList activo;
        public void IniciarVista(TURNOSVIERNES turnoViernes)
        {
            SelectList listaAnios;
            Dictionary<int, string> lista = new Dictionary<int, string>();
            SelectList ListaTurnos;
            lista.Add(1, "SI");
            lista.Add(0, "NO");
            if (turnoViernes != null)
            {

                activo = new SelectList(lista, "Key", "Value", turnoViernes.ESTADO);
                listaAnios = new SelectList(LLenarListaAnios(), "Key", "Value", turnoViernes.ANIO);
                ListaTurnos = new SelectList(new ServicioTurno().ObtenerDiccionarioTurnoSemanales(), "Key", "Value", turnoViernes.TURNO_ID);
            }
            else
            {
                activo = new SelectList(lista, "Key", "Value");
                listaAnios = new SelectList(LLenarListaAnios(), "Key", "Value");
                ListaTurnos = new SelectList(new ServicioTurno().ObtenerDiccionarioTurnoSemanales(), "Key", "Value");
            }
            vistaModeloTurnoViernes = new TurnoViernesViewModel(turnoViernes, activo, listaAnios, ListaTurnos);
        }

        
        public ActionResult Crear()
        {
            IniciarVista(null);
            return View(vistaModeloTurnoViernes);
        }


        public ActionResult Editar(int id)
        {
            TURNOSVIERNES turnoViernes = _servicio.ObtenerPorClave(id);
            if (turnoViernes != null)
            {
                IniciarVista(turnoViernes);
                return View(vistaModeloTurnoViernes);
            }
            else
            {
                TempData["mensaje"] = "Turno Viernes no Encontrado";
                return RedirectToAction("Listar");
            }
        }

        public ActionResult Listar() {
            return View();
        }

        [HttpPost]
        public ActionResult Crear([Bind(Prefix = "TURNOSVIERNES")] TURNOSVIERNES turnosViernes, FormCollection collection)
        {
            
            if (ModelState.IsValid) {
                if (!String.IsNullOrEmpty(collection["TURNOSVIERNES.MULTI_TURNO_ID"]) ||
                    !String.IsNullOrEmpty(collection["TURNOSVIERNES.MULTI_VIERNES"]))
                {
                    turnosViernes.MULTI_TURNO_ID = Request["TURNOSVIERNES.MULTI_TURNO_ID"].ToString();
                    turnosViernes.MULTI_VIERNES = Request["TURNOSVIERNES.MULTI_VIERNES"].ToString();

                    ServicioTurnoViernes servicioTurnoViernes = new ServicioTurnoViernes();

                        USUARIOS usuario = new ServicioUsuario().ObtenerPorLogin(User.Identity.Name);
                        turnosViernes.USUARIO_ID = usuario.USUARIO_ID;
                        if (servicioTurnoViernes.Guardar(turnosViernes))
                        {
                            TempData["mensaje"] = "Turno Viernes Guardado Satisfactoriamente";
                            return RedirectToAction("Listar");
                        }
                        else
                            ModelState.AddModelError(string.Empty, "Error Guardando Turno Viernes");
                }
            }
            IniciarVista(turnosViernes);
            return View(vistaModeloTurnoViernes);

        }

        [HttpPost]
        public ActionResult Editar(TURNOSVIERNES TURNOSVIERNES, FormCollection form)
        {

            if (ModelState.IsValid)
            {
              
                ServicioTurnoViernes servicioTurnoViernes = new ServicioTurnoViernes();
                if (!servicioTurnoViernes.Duplicado(TURNOSVIERNES))
                {
                    if (servicioTurnoViernes.Modificar(TURNOSVIERNES))
                    {
                        TempData["mensaje"] = "TurnoViernes Modificado Satisfactoriamente";
                        return RedirectToAction("Listar");
                    }
                    else
                        ModelState.AddModelError(string.Empty, "Error Modificando TurnoViernes");
                }
                else
                    ModelState.AddModelError(string.Empty, "TurnoViernes ya existe");

            }
            IniciarVista(TURNOSVIERNES);
            return View(vistaModeloTurnoViernes);

        } 

        [HttpPost]
        public JsonResult Eliminar(int id)
        {
            return Json(_servicio.Eliminar(id));
        }


        [HttpPost]
        public JsonResult ObtenerData(string sidx, string sord, int page, int rows, string filters)
        {
            object resultado = _servicio.ObtenerDataGrid(sidx, sord, page, rows, filters);
            return Json(resultado);

        }

        public Dictionary<int, int> LLenarListaAnios()
        {
            Dictionary<int, int> lista = new Dictionary<int, int>();
            int anioActual = DateTime.Now.Year;
            for (int i = (anioActual - 1); i <= (anioActual + 9); i++)
            {
                lista.Add(i, i);
            }
            return lista;
        }
    }
}
