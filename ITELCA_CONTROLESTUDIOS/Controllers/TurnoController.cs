﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ITELCA_CLASSLIBRARY.Models;
using ITELCA_CONTROLESTUDIOS.Models.ViewModels;
using ITELCA_CLASSLIBRARY.Services;
using ITELCA_CLASSLIBRARY.CustomClasses;
using System.Web.Script.Serialization;

namespace ITELCA_CONTROLESTUDIOS.Controllers
{
     [Authorize(Roles = "ADMINISTRADOR,COORDINADOR")]
    public class TurnoController : Controller
    {
        //
        // GET: /Rol/
        TurnoViewModel vistaModeloTurno;
        ServicioTurno _servicio = new ServicioTurno();
        SelectList activo;
        MultiSelectList dias;
        public void IniciarVista(TURNOS turno, List<DIATURNO> diaTurno)
        {
            Dictionary<int, string> lista = new Dictionary<int, string>();
            SelectList hora_inicio;
            SelectList hora_fin;
            SelectList bloque;
            SelectList tipoTurno;
            lista.Add(1, "SI");
            lista.Add(0, "NO");
           
            if (turno != null)
            {
                activo = new SelectList(lista, "Key", "Value", turno.ESTADO);
                dias = new MultiSelectList(new ServicioDia().ObtenerTodos(), "DIA_ID", "NOMBRE");
                hora_inicio = new SelectList(new ServicioLista().ObtenerDiccionarioHoras(), "Key", "Value");
                hora_fin = new SelectList(new ServicioLista().ObtenerDiccionarioHoras(), "Key", "Value");
                diaTurno = new ServicioDia().obtenerDiaTurno(turno.TURNO_ID);
                bloque = new SelectList(_servicio.ObtenerDiccionarioBloque(), "Key", "Value", turno.BLOQUE);
                tipoTurno=new SelectList(new ServicioTipoTurno().ObtenerLista(),"Key","Value",turno.TIPOTURNO_ID);
            }
            else
            {
                activo = new SelectList(lista, "Key", "Value");
                dias = new MultiSelectList(new ServicioDia().ObtenerTodos(), "DIA_ID", "NOMBRE");
                hora_inicio = new SelectList(new ServicioLista().ObtenerDiccionarioHoras(), "Key", "Value");
                hora_fin = new SelectList(new ServicioLista().ObtenerDiccionarioHoras(), "Key", "Value");
                diaTurno = new ServicioDia().obtenerDiaTurno(-1);
                bloque = new SelectList(_servicio.ObtenerDiccionarioBloque(), "Key", "Value");
                tipoTurno = new SelectList(new ServicioTipoTurno().ObtenerLista(), "Key", "Value");
            }
            vistaModeloTurno = new TurnoViewModel(turno, activo, dias, hora_inicio, hora_fin, diaTurno, bloque,tipoTurno);
        }

        public ActionResult Crear()
        {
            IniciarVista(null,null);
            return View(vistaModeloTurno);
        }
        public ActionResult Editar(int id)
        {
            TURNOS turno = _servicio.ObtenerPorClave(id);
            if (turno != null)
            {
                IniciarVista(turno,null);
                return View(vistaModeloTurno);
            }
            else
            {
                TempData["mensaje"] = "Turno no Encontrado";
                return RedirectToAction("Listar");
            }
        }

        public ActionResult Listar() {
            return View();
        }

        [HttpPost]
        public ActionResult Crear(TURNOS turno,FormCollection form) {
            List<DIATURNO> obj=null;
            if (ModelState.IsValid) {
                string dia = form["diaTurno"];
                obj = new JavaScriptSerializer().Deserialize<List<DIATURNO>>(dia );
                ServicioTurno servicioTurno = new ServicioTurno();
                if (!servicioTurno.Duplicado(turno))
                {
                    USUARIOS usuario = new ServicioUsuario().ObtenerPorLogin(User.Identity.Name);
                    turno.USUARIO_ID = usuario.USUARIO_ID;
                    if (servicioTurno.Guardar(turno,obj))
                    {
                        TempData["mensaje"] = "Turno Guardado Satisfactoriamente";
                        return RedirectToAction("Listar");
                    }
                    else
                        ModelState.AddModelError(string.Empty, "Error Guardando Turno");
                }
                else 
                    ModelState.AddModelError(string.Empty, "Turno ya existe");
                
            }
            IniciarVista(turno,obj);
            return View(vistaModeloTurno);

        }

        [HttpPost]
        public ActionResult Editar(TURNOS turno,FormCollection form)
        {
            List<DIATURNO> obj = null;
            if (ModelState.IsValid)
            {
                string dia = form["diaTurno"];
                obj = new JavaScriptSerializer().Deserialize<List<DIATURNO>>(dia);
                ServicioTurno servicioTurno = new ServicioTurno();
                if (!servicioTurno.Duplicado(turno))
                {
                    if (servicioTurno.Modificar(turno,obj))
                    {
                        TempData["mensaje"] = "Turno Modificado Satisfactoriamente";
                        return RedirectToAction("Listar");
                    }
                    else
                        ModelState.AddModelError(string.Empty, "Error Modificando Turno");
                }
                else
                    ModelState.AddModelError(string.Empty, "Turno ya existe");

            }
            IniciarVista(turno,obj);
            return View(vistaModeloTurno);

        }

        [HttpPost]
        public JsonResult Eliminar(int id)
        {
            return Json(_servicio.Eliminar(id));
        }
        [HttpPost]
        public JsonResult ObtenerData(string sidx, string sord, int page, int rows, string filters)
        {
            object resultado = _servicio.ObtenerDataGrid(sidx, sord, page, rows, filters);
            return Json(resultado);

        }
        [HttpPost]
        public JsonResult ObtenerDiasPorTurno(string idOfertaCurso)
        {
            object resultado = _servicio.ObtenerListaDias(idOfertaCurso);
            return Json(resultado);

        }
    }
}
