﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ITELCA_CLASSLIBRARY.Models;
using ITELCA_CONTROLESTUDIOS.Models.ViewModels;
using ITELCA_CLASSLIBRARY.Services;

namespace ITELCA_CONTROLESTUDIOS.Controllers
{
     [Authorize(Roles = "ADMINISTRADOR,COORDINADOR")]
    public class TipoCursoController : Controller
    {
        //
        // GET: /Rol/
        TipoCursoViewModel vistaModeloTipoCurso;
        SelectList activo;
        public void IniciarVista(TIPOSCURSOS tipoCurso)
        {
            Dictionary<int, string> lista = new Dictionary<int, string>();
            lista.Add(1, "SI");
            lista.Add(0, "NO");
            if (tipoCurso != null)
                activo = new SelectList(lista, "Key", "Value", tipoCurso.ESTADO);
            else
                activo = new SelectList(lista, "Key", "Value");
            vistaModeloTipoCurso = new TipoCursoViewModel(tipoCurso, activo);
        }

        public ActionResult Crear()
        {
            IniciarVista(null);
            return View(vistaModeloTipoCurso);
        }
        
        public ActionResult Editar(int id) {
            TIPOSCURSOS tipoCurso = new ServicioTipoCurso().ObtenerPorClave(id);
            if (tipoCurso != null)
            {
                IniciarVista(tipoCurso);
                return View(vistaModeloTipoCurso);
            }
            else
            {
                TempData["mensaje"] = "Tipo Curso no Encontrado";
                return RedirectToAction("Listar");
            }
        }

        public ActionResult Listar() {
            return View();
        }

        [HttpPost]
        public ActionResult Crear(TIPOSCURSOS tipoCurso)
        {
            if (ModelState.IsValid) {
                ServicioTipoCurso servicioTipoCurso = new ServicioTipoCurso();
                if (!servicioTipoCurso.Duplicado(tipoCurso))
                {

                    USUARIOS usuario = new ServicioUsuario().ObtenerPorLogin(User.Identity.Name);
                    tipoCurso.USUARIO_ID = usuario.USUARIO_ID;
                    if (servicioTipoCurso.Guardar(tipoCurso))
                    {
                        TempData["mensaje"] = "Tipo Curso Guardado Satisfactoriamente";
                        return RedirectToAction("Listar");
                    }
                    else
                        ModelState.AddModelError(string.Empty, "Error Guardando Tipo Curso");
                }
                else 
                    ModelState.AddModelError(string.Empty, "Tipo Curso ya existe");
                
            }
            IniciarVista(tipoCurso);
            return View(vistaModeloTipoCurso);

        }

        [HttpPost]
        public ActionResult Editar(TIPOSCURSOS tipoCurso)
        {
            if (ModelState.IsValid)
            {
                ServicioTipoCurso servicioTipoCurso = new ServicioTipoCurso();
                if (!servicioTipoCurso.Duplicado(tipoCurso))
                {
                    if (servicioTipoCurso.Modificar(tipoCurso))
                    {
                        TempData["mensaje"] = "Tipo Curso Modificado Satisfactoriamente";
                        return RedirectToAction("Listar");
                    }
                    else
                        ModelState.AddModelError(string.Empty, "Error Modificando Tipo Curso");
                }
                else
                    ModelState.AddModelError(string.Empty, "Tipo Curso ya existe");

            }
            IniciarVista(tipoCurso);
            return View(vistaModeloTipoCurso);

        }

        [HttpPost]
        public JsonResult Eliminar(int id)
        {
            return Json(new ServicioTipoCurso().Eliminar(id));
        }
        [HttpPost]
        public JsonResult ObtenerData(string sidx, string sord, int page, int rows, string filters)
        {
            object resultado = new ServicioTipoCurso().ObtenerDataGrid(sidx, sord, page, rows, filters);
            return Json(resultado);

        }

    }
}
