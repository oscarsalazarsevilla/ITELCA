﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.Reporting.WebForms;
using System.ComponentModel;
using System.IO;
using ITELCA_CLASSLIBRARY.CustomClasses;
using ITELCA_CLASSLIBRARY.Services;
using ITELCA_CLASSLIBRARY.Controllers;
using ITELCA_CLASSLIBRARY.Models;
using ITELCA_CONTROLESTUDIOS.Models.ViewModels;

namespace ITELCA_CONTROLESTUDIOS.Controllers
{
    public class ReporteController : Controller
    {
        ReporteViewModel modeloVista;
        
        [HttpGet]
        public ActionResult EmitirConstanciaEstudios(string id_solicitud)
        {
            return Redirect("/Reportes/Constancia_Estudio.aspx?id_solicitud=" + id_solicitud);
        
        }

        [HttpGet]
        public ActionResult EmitirCartaPasantias(string id_solicitud)
        {
            return Redirect("/Reportes/Carta_Pasantias.aspx?id_solicitud=" + id_solicitud);

        }

        [HttpGet]
        public ActionResult EmitirConstanciaNotas(string id_solicitud)
        {
            return Redirect("/Reportes/Constancia_Notas.aspx?id_solicitud=" + id_solicitud);

        }

        [HttpGet]
        public ActionResult EmitirReciboCaja(string id_recibo)
        {
            MANEJOCAJAS mc = new ServicioManejoCaja().ObtenerManejoCajaAbierta(User.Identity.Name);
            return Redirect("/Reportes/Recibo_Mensualidad.aspx?id_recibo=" + id_recibo + "&sede=" + mc.CAJAS.SEDES.NOMBRE);

        }
         [HttpGet]
        public ActionResult EmitirReciboCajaProducto(string id_recibo)
        {
            MANEJOCAJAS mc = new ServicioManejoCaja().ObtenerManejoCajaAbierta(User.Identity.Name);
            return Redirect("/Reportes/prueba.aspx?id_recibo=" + id_recibo+"&sede="+mc.CAJAS.SEDES.NOMBRE);

        }
       
        public ActionResult Comisiones()
        {
            return View();
        }

        [HttpPost]
        public JsonResult ObtenerReporteComisiones(string desde, string hasta)
        {


            List<REPORTECOMISIONES> listaComisiones = new List<REPORTECOMISIONES>();
            var listaAgrupada =  new ServicioMatriculaCurso().ObtenerComisionesTodos(desde, hasta);
            
            if (listaAgrupada != null)
            {

                foreach (IGrouping<decimal?, MATRICULASCURSOS> empleado in listaAgrupada){ // Para Cada Empleado
          
                    USUARIOS emp = new ServicioUsuario().ObtenerPorClave(Decimal.Parse(empleado.Key.ToString()));
                    REPORTECOMISIONES obj = new REPORTECOMISIONES();
                    obj.EMPLEADO_ID = emp.USUARIO_ID.ToString();
                    obj.NOMBRE = emp.NOMBRE;
                    obj.APELLIDO = emp.APELLIDO;
                    obj.NRO_DOCUMENTO = emp.NRO_DOCUMENTO;
                    obj.INSCRITOS = empleado.Count().ToString();
                    listaComisiones.Add(obj);
                }
            }



            return Json(listaComisiones);
        }

        public ActionResult VentaDiariaCaja() {
            IniciarVista();
            return View(modeloVista);
        }

        public void IniciarVista(){
            USUARIOS usuario = new ServicioUsuario().ObtenerPorLogin(User.Identity.Name);
            bool ban=false;
            if (usuario.ROLES.Count(u => u.ROL_ID == 1) > 0)
                ban = true;
            MANEJOCAJAS manejo = new ServicioManejoCaja().ObtenerManejoCajaAbierta(User.Identity.Name);
            ServicioReciboCaja servicio = new ServicioReciboCaja();
            string totalReciboBlanco = servicio.ObtenerTotalPorTipoRecibo(0,ban,usuario.USUARIO_ID);
            string totalReciboAzul = servicio.ObtenerTotalPorTipoRecibo(1,ban,usuario.USUARIO_ID);
            List<PAGOS> reciboBlanco = new ServicioPago().ObtenerPagosPorTipoRecibo(0,ban,usuario.USUARIO_ID);
            List<PAGOS> reciboAzul = new ServicioPago().ObtenerPagosPorTipoRecibo(1,ban,usuario.USUARIO_ID);
            IEnumerable<MATRICULASCURSOS> totalInscritos = new ServicioMatriculaCurso().ObtenerInscritosDelDia(ban, usuario.USUARIO_ID);
            modeloVista = new ReporteViewModel(totalReciboBlanco, totalReciboAzul,  reciboBlanco,reciboAzul,totalInscritos);
        }

        [HttpGet]
        public ActionResult EmitirReportePlanificacion(string ofertaCurso,string desde, string profesor)
        {

            return Redirect("/Reportes/Planificacion.aspx?ofertaCurso=" + ofertaCurso + "_" + desde + "_" + profesor);

        }
         
    }
}
