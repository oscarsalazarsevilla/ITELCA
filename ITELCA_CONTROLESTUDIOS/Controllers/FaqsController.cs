﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ITELCA_CLASSLIBRARY.Models;
using ITELCA_CONTROLESTUDIOS.Models.ViewModels;
using ITELCA_CLASSLIBRARY.Services;

namespace ITELCA_CONTROLESTUDIOS.Controllers
{
     [Authorize(Roles = "ADMINISTRADOR,COORDINADOR,WEB")]
    public class FaqsController : Controller
    {
        //
        // GET: /Faqs/
        FaqViewModel vistaModeloFaq;
        SelectList activo;
        SelectList listaTiposFaqs;

        public void IniciarVista(FAQS faq)
        {
            Dictionary<int, string> lista = new Dictionary<int, string>();
            lista.Add(1, "SI");
            lista.Add(0, "NO");
            if (faq != null)
            {
                activo = new SelectList(lista, "Key", "Value", faq.ESTADO);
                listaTiposFaqs = new SelectList( new ServicioTipoFaqs().ObtenerTodos(),"TIPO_FAQS_ID", "NOMBRE",faq.TIPO_FAQS_ID);
            }
            else
            {
                faq = new FAQS();
                activo = new SelectList(lista, "Key", "Value");
                listaTiposFaqs = new SelectList(new ServicioTipoFaqs().ObtenerTodos(), "TIPO_FAQS_ID", "NOMBRE");
            }
            vistaModeloFaq = new FaqViewModel(faq, activo, listaTiposFaqs);
        }

        public ActionResult Crear()
        {
            IniciarVista(null);
            return View(vistaModeloFaq);
        }

        public ActionResult Editar(int id)
        {
            FAQS faqs = new ServicioFaq().ObtenerPorClave(id);
            if (faqs != null)
            {
                IniciarVista(faqs);
                return View(vistaModeloFaq);
            }
            else
            {
                TempData["mensaje"] = "Faq no Encontrada";
                return RedirectToAction("Listar");
            }
        }

        public ActionResult Listar()
        {
            return View();
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Crear(FAQS faq)
        {
            if (ModelState.IsValid)
            {
                ServicioFaq servicioFaq = new ServicioFaq();
                if (!servicioFaq.Duplicado(faq))
                {
                    USUARIOS usuario = new ServicioUsuario().ObtenerPorLogin(User.Identity.Name);
                    faq.USUARIO_ID = usuario.USUARIO_ID;
                    faq.PREGUNTA = System.Net.WebUtility.HtmlDecode(faq.PREGUNTA);
                    faq.RESPUESTA = System.Net.WebUtility.HtmlDecode(faq.RESPUESTA);
                    if (servicioFaq.Guardar(faq))
                    {
                        TempData["mensaje"] = "Faq Guardada Satisfactoriamente";
                        return RedirectToAction("Listar");
                    }
                    else
                        ModelState.AddModelError(string.Empty, "Error Guardando Faqs");
                }
                else
                    ModelState.AddModelError(string.Empty, "Faq ya existe");

            }
            IniciarVista(faq);
            return View(vistaModeloFaq);

        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Editar(FAQS faq)
        {
            if (ModelState.IsValid)
            {
                ServicioFaq servicioFaq = new ServicioFaq();
                if (!servicioFaq.Duplicado(faq))
                {
                    faq.PREGUNTA = System.Net.WebUtility.HtmlDecode(faq.PREGUNTA);
                    faq.RESPUESTA = System.Net.WebUtility.HtmlDecode(faq.RESPUESTA);
                    if (servicioFaq.Modificar(faq))
                    {
                        TempData["mensaje"] = "Faq Modificada Satisfactoriamente";
                        return RedirectToAction("Listar");
                    }
                    else
                        ModelState.AddModelError(string.Empty, "Error Modificando Faq");
                }
                else
                    ModelState.AddModelError(string.Empty, "Faq ya existe");
            }

            IniciarVista(faq);
            return View(vistaModeloFaq);
        }

        [HttpPost]
        public JsonResult Eliminar(int id)
        {
            return Json(new ServicioFaq().Eliminar(id));
        }
        [HttpPost]
        public JsonResult ObtenerData(string sidx, string sord, int page, int rows, string filters)
        {
            object resultado = new ServicioFaq().ObtenerDataGrid(sidx, sord, page, rows, filters);
            return Json(resultado);
        }
    }
}
