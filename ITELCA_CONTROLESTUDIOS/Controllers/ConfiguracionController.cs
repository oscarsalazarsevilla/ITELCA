﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ITELCA_CLASSLIBRARY.Models;
using ITELCA_CONTROLESTUDIOS.Models.ViewModels;
using ITELCA_CLASSLIBRARY.Services;

namespace ITELCA_CONTROLESTUDIOS.Controllers
{
    [Authorize(Roles = "ADMINISTRADOR,COORDINADOR,WEB")]
    public class ConfiguracionController : Controller
    {

        ConfiguracionViewModel _vistaModeloConfiguracion;
        
        public void IniciarVista(CONFIGURACIONES configuracion) {
            if (configuracion == null) {
                configuracion = new CONFIGURACIONES();
            }
            _vistaModeloConfiguracion = new ConfiguracionViewModel(configuracion);
        }
      
        public ActionResult Editar() {
            CONFIGURACIONES configuracion = new ServicioConfiguracion().ObtenerConfiguracion();
       
            IniciarVista(configuracion);
            return View(_vistaModeloConfiguracion);
        }


        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Editar(CONFIGURACIONES configuracion, HttpPostedFileBase[] file)
        {
            string path = "";
            if (file != null && file[0] != null)
            {
                path = System.IO.Path.Combine(Server.MapPath("~/Content/Logo/"), System.IO.Path.GetFileName(file[0].FileName));
                //---Añadiendo el nombre de la imagen amostrar 
                configuracion.LOGO = file[0].FileName;
                file[0].SaveAs(path);
            }
            else
            {
                ModelState.AddModelError("LOGO", "Es necesario agregar el logo.");
            }

            if (ModelState.IsValid)
            {
                    if (new ServicioConfiguracion().Modificar(configuracion))
                    {
                        TempData["mensaje"] = "Configuración Modificada Satisfactoriamente";
                        return RedirectToAction("Editar");
                    }
                    else
                        ModelState.AddModelError(string.Empty, "Error Modificando Configuración");
            }
            IniciarVista(configuracion);
            return View(_vistaModeloConfiguracion);

        }       

    }
}
