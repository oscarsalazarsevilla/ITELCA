﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ITELCA_CONTROLESTUDIOS.Models.ViewModels;
using ITELCA_CLASSLIBRARY.Services;
using ITELCA_CLASSLIBRARY.Models;
using ITELCA_CONTROLESTUDIOS.Models;

namespace ITELCA_CONTROLESTUDIOS.Controllers
{

    public class MasterController : BaseMasterController<MasterViewModel>
    {
        ServicioCategoria _servicioCategoria = new ServicioCategoria();
        ServicioConfiguracion _servicioConfiguracion = new ServicioConfiguracion();
        LogOnModel logOn = new LogOnModel();
            
        protected override MasterViewModel GetMasterViewModel()
        {
            return new MasterViewModel(ObtenerConfiguracion(), CategoriasPrincipales(), logOn, ObtenerMenu());
        }

        public string ObtenerMenu()
        {
            return _servicioCategoria.ObtenerMenu();
        }

        public List<CATEGORIAS> CategoriasPrincipales()
        {
            return _servicioCategoria.ObtenerOrdenadas();
        }

        public CONFIGURACIONES ObtenerConfiguracion()
        {
            return _servicioConfiguracion.ObtenerConfiguracion();
        }

        /*public List<RedSocial> Redes_Sociales() 
        {
            List<RedSocial> Lista = new List<RedSocial>();
            RedSocialServices servicio = new RedSocialServices();
            foreach (var item in servicio.ObtenerPublicado())
            {
                Lista.Add(item);
            }
            return Lista;
        }*/

        /*public ActionResult Contactenos() 
        {
            ContactoViewModel Contactenos = new ContactoViewModel(null, null);

            return View(Contactenos);
        }*/

    }
}
