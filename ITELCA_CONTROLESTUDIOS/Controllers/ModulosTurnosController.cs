﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ITELCA_CLASSLIBRARY.Models;
using ITELCA_CONTROLESTUDIOS.Models.ViewModels;
using ITELCA_CLASSLIBRARY.Services;

namespace ITELCA_CONTROLESTUDIOS.Controllers
{
    [Authorize(Roles = "ADMINISTRADOR,COORDINADOR")]
    public class ModulosTurnosController : Controller
    {
        ModuloTurnoViewModel vistaModeloModulo;
        SelectList activo;
        SelectList listaCurso;
        MultiSelectList modulos, turnos;
        ServicioModuloTurno _servicio = new ServicioModuloTurno();

        public void IniciarVista(MODULOS_TURNOS modulo_turno) {
            ServicioUsuario servicioUsuario = new ServicioUsuario();
            Dictionary<int, string> lista = new Dictionary<int, string>();
            lista.Add(1, "SI");
            lista.Add(0, "NO");

            if (modulo_turno != null)
            {
                activo = new SelectList(lista, "Key", "Value", modulo_turno.ESTADO);
            }
            else
            {
                listaCurso = new SelectList(new ServicioCursos().ObtenerTodos(), "CURSO_ID", "NOMBRE");
                activo = new SelectList(lista, "Key", "Value");
                modulos = new MultiSelectList(new ServicioModulo().ObtenerTodos(), "MODULO_ID", "NOMBRE");
                turnos = new MultiSelectList(new ServicioTurno().ObtenerTodos(), "TURNO_ID", "NOMBRE");
            }
            vistaModeloModulo = new ModuloTurnoViewModel(modulo_turno, activo, modulos, turnos, listaCurso);
        }

        public ActionResult Crear()
        {
            IniciarVista(null);
            return View(vistaModeloModulo);
        }
        
        public ActionResult Editar(int modulo_id, int turno_id) { 
            MODULOS_TURNOS modulo_turno=new ServicioModuloTurno().ObtenerPorClave(modulo_id,turno_id);
            if (modulo_turno != null)
            {
                IniciarVista(modulo_turno);
                return View(vistaModeloModulo);
            }
            else
            {
                TempData["mensaje"] = "Módulo-Turno no encontrado";
                return RedirectToAction("Listar");
            }
        }

        public ActionResult Listar() {
            IniciarVista(null);
            return View(vistaModeloModulo);
        }

        [HttpPost]
        public ActionResult Crear(MODULOS_TURNOS modulo_turno, FormCollection form) {
            // validamos que no se repita la combinació módulo-turno
            modulo_turno.MULTI_MODULO_ID = Request["modulo_turno.MODULO_ID"].ToString();
            modulo_turno.MULTI_TURNO_ID = Request["modulo_turno.TURNO_ID"].ToString();
            ValidarModelo(modulo_turno);

            if (ModelState.IsValid) 
            { 
                USUARIOS usuario = new ServicioUsuario().ObtenerPorLogin(User.Identity.Name);
                modulo_turno.USUARIO_ID = usuario.USUARIO_ID;

                if (_servicio.Guardar(modulo_turno))
                {
                    TempData["mensaje"] = "Módulo - Turno Guardado Satisfactoriamente";
                    return RedirectToAction("Listar");
                }
                else
                    ModelState.AddModelError(string.Empty, "Error Guardando Módulo - Turno"); 
                
            }
            IniciarVista(null);
            return View(vistaModeloModulo);
        }

        [HttpPost]
        public ActionResult Editar(MODULOS_TURNOS modulo_turno)
        {
            if (ModelState.IsValid)
            {
                if (_servicio.Modificar(modulo_turno))
                {
                    TempData["mensaje"] = "Módulo - Turno Modificado Satisfactoriamente";
                    return RedirectToAction("Listar");
                }
                else
                    ModelState.AddModelError(string.Empty, "Error Modificando Módulo - Turno");
            }
            IniciarVista(modulo_turno);
            return View(vistaModeloModulo);

        }

        [HttpPost]
        public JsonResult Eliminar(string id)
        {   
            int modulo_id, turno_id;
            string[] array = id.Split('|');
            modulo_id = int.Parse(array[0]);
            turno_id = int.Parse(array[1]);
            
            MODULOS_TURNOS modulo_turno = _servicio.ObtenerPorClave(modulo_id, turno_id);
            return Json(_servicio.Eliminar(modulo_turno));
        }

        [HttpPost]
        public JsonResult ObtenerData(string sidx, string sord, int page, int rows, string filters , string CursoId)
        {
            object resultado = new ServicioModuloTurno().ObtenerDataGrid(sidx, sord, page, rows, filters, CursoId);
            return Json(resultado);

        }

        public void ValidarModelo(MODULOS_TURNOS modulo_turno)
        {
            string[] arregloModulos = modulo_turno.MULTI_MODULO_ID.Split(',');
            string[] arregloTurnos = modulo_turno.MULTI_TURNO_ID.Split(',');

            foreach (var modulo in arregloModulos)
            {
                foreach (var turno in arregloTurnos)
                {
                    if (_servicio.EsDuplicado(decimal.Parse(modulo), decimal.Parse(turno)))
                    {
                        ModelState.AddModelError("MODULO_ID", "Ya existe un registro creado para el módulo " + new ServicioModulo().ObtenerPorClave(long.Parse(modulo)).NOMBRE +
                                                                " y el turno " + new ServicioTurno().ObtenerPorClave(long.Parse(turno)).NOMBRE);
                    }
                }
            }
        }

    }
}
