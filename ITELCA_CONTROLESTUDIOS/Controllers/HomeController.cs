﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ITELCA_CONTROLESTUDIOS.Models.ViewModels;
using ITELCA_CONTROLESTUDIOS.Models;
using ITELCA_CLASSLIBRARY.Services;
using ITELCA_CLASSLIBRARY.Models;
using System.Net.Mail;
using System.Net;

namespace ITELCA_CONTROLESTUDIOS.Controllers
{
    
    public class HomeController : MasterController
    {
        public HomeViewModel _homeViewModel;
        private NoticiaViewModel vistaModeloNoticia;

        public HomeController()
        {
        }
        public void IniciarVista(string vista, int? seccion_id, CONTACTO contacto)
        {
            ServicioBanner _servicioBanner = new ServicioBanner();
            ServicioTestimonio _servicioTestimonio = new ServicioTestimonio();
            ServicioNoticia _servicioNoticia = new ServicioNoticia();
            ServicioCategoria _servicioCategoria = new ServicioCategoria();
            ServicioConfiguracion _servicioConfiguracion = new ServicioConfiguracion();
            ServicioSeccion _servicioSeccion = new ServicioSeccion();

            if (vista == "home")
            {
                List<BANNERS> banners = _servicioBanner.ObtenerTodos_Activos();
                List<TESTIMONIOS> testimonios = _servicioTestimonio.ObtenerOrdenados();
                CONFIGURACIONES config = _servicioConfiguracion.ObtenerConfiguracion();
                List<CATEGORIAS> categorias = _servicioCategoria.ObtenerOrdenadas();
                List<SECCIONES> principales = _servicioSeccion.ObtenerPrincipales();

                _homeViewModel = new HomeViewModel(banners, testimonios, config, categorias, principales);
            }
            if (vista == "faqs")
            {
                _homeViewModel = new HomeViewModel(new ServicioFaq().ObtenerTodosOrdenados());
            }
            if (vista == "seccion")
            {
                _homeViewModel = new HomeViewModel(new ServicioSeccion().ObtenerPorClave(seccion_id.Value));
            }
            if (vista == "categoria") {
                ServicioTipoCurso _servicioTipoCurso = new ServicioTipoCurso();
                _homeViewModel = new HomeViewModel(_servicioTipoCurso.ObtenerPorClave(seccion_id.Value), _servicioTipoCurso.ObtenerProximosCursos(seccion_id.Value));
                
            }
            if (vista == "curso")
            {
                _homeViewModel = new HomeViewModel(new ServicioCursos().ObtenerPorClave(seccion_id.Value));

            }
            if (vista == "categoriaTaller")
            {
                ServicioTipoTaller _servicioTipoTaller = new ServicioTipoTaller();
                _homeViewModel = new HomeViewModel(_servicioTipoTaller.ObtenerPorClave(seccion_id.Value), _servicioTipoTaller.ObtenerProximosTalleres(seccion_id.Value));

            }
            if (vista == "talleres")
            {
                _homeViewModel = new HomeViewModel(new ServicioTaller().ObtenerPorClave(seccion_id.Value));

            }
            if (vista == "noticias") {
               _homeViewModel = new HomeViewModel(new ServicioNoticia().ObtenerTodos_Activos().ToList());
            }
            if (vista == "contactenos")
            {
                _homeViewModel = new HomeViewModel(new ServicioCategoria().ObtenerPorNombre("CONTACTO"), new ServicioSede().ObtenerTodos().ToList());
            }

            if (contacto != null) {
                _homeViewModel.CONTACTO = contacto;
            }

        }
        public ActionResult Index()
        {
            IniciarVista("home",null,null);
            return View(_homeViewModel);
        }

        public ActionResult Seccion(int seccion_id)
        {
            IniciarVista("seccion", seccion_id, null);
            return View(_homeViewModel);
        }

        public ActionResult TiposCursos(int Id)
        {
            IniciarVista("categoria", Id, null);
            return View(_homeViewModel);
        }
        public ActionResult Cursos(int Id)
        {
            IniciarVista("curso", Id, null);
            return View(_homeViewModel);
        }

        public ActionResult TiposTalleres(int Id)
        {
            IniciarVista("categoriaTaller", Id, null);
            return View(_homeViewModel);
        }
        public ActionResult Talleres(int Id)
        {
            IniciarVista("talleres", Id, null);
            return View(_homeViewModel);
        }

        public ActionResult Faqs()
        {
            IniciarVista("faqs", null, null);
            return View(_homeViewModel);
        }
       

        public ActionResult IndexCDE() {
            ViewBag.Mensaje = "CMS ITELCA";
            ViewBag.Titulo = "Accesos directos";
            return View();
        }

        public ActionResult Contactenos()
        {
            IniciarVista("contactenos", null, null);
            return View(_homeViewModel);
        }

        [HttpPost]
        public ActionResult Contactenos([Bind(Prefix = "View.CONTACTO")] CONTACTO CONTACTO, FormCollection collection)
        {

            if (ModelState.IsValid)
            {
                if (new ServicioContacto().Guardar(CONTACTO))
                {
                    TempData["mensaje"] = "Mensaje enviado Satisfactoriamente";
                    return RedirectToAction("Contactenos");
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Error Guardando Mensaje");

                }
            }
            IniciarVista("contactenos", null, CONTACTO);
            return View(_homeViewModel);
        } 

        public ActionResult LogOn()
        {
            return View(new LogOnModel());
        }

        public ActionResult Noticias()
        {
            IniciarVista("noticias", null, null);
            return View(_homeViewModel);
        }


        public ActionResult DetalleNoticia(int id)
        {
            ServicioNoticia servicio = new ServicioNoticia();
            NOTICIAS noticia = servicio.ObtenerPorClave(id);
            vistaModeloNoticia = new NoticiaViewModel(null, noticia);
            return PartialView(vistaModeloNoticia);
        }
        public ActionResult VerificarEstudiante() {

            return View();
        }
        public JsonResult SendEmail(string login)
        {
            List<object> lista = new List<object>();

            CONFIGURACIONES config = new ServicioConfiguracion().ObtenerConfiguracion();
            USUARIOS usuario = new ServicioUsuario().ObtenerPorLogin(login);
            if (usuario != null) {

                var fromAddress = new MailAddress(config.EMAIL, "ITELCA");
                var toAddress = new MailAddress(usuario.EMAIL, usuario.NOMBRE +" "+ usuario.APELLIDO);
                string subject = "Recuperación Contraseña";
                string body = "Hola "+usuario.NOMBRE+" "+ usuario.APELLIDO +"\n \n" + "Su contraseña para ingresar en ITELCA Control de estudios es:";

                var smtp = new SmtpClient
                {
                    Host = "smtp.gmail.com",
                    Port = 587,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(fromAddress.Address, config.PWDEMAIL)
                };
                using (var message = new MailMessage(fromAddress, toAddress)
                {
                    Subject = subject,
                    Body = body
                })
                {
                    smtp.Send(message);


                    lista.Add(new
                    {
                        ok = 1,
                        msg = "Su clave ha sido enviada a su dirección de correo electrónico."
                    });


                }
            
            }else
            {
                lista.Add(new
                {
                    ok = 0,
                    msg = "El nombre de usuario no se encuentra registrado."
                });
            }


            return Json(lista);
        }

        [HttpPost]
        public JsonResult guardarContacto(string nombre, string email, string telefono, string asunto, string texto)
        {

            CONTACTO contacto = new CONTACTO();
            contacto.NOMBRE = nombre;
            contacto.ASUNTO = asunto;
            contacto.EMAIL = email;
            contacto.TELEFONO = telefono;
            contacto.ASUNTO = asunto;
            contacto.TEXTO = texto;
            contacto.FECHA = DateTime.Now;
            JsonResult resp; 
            if (new ServicioContacto().Guardar(contacto))
            {
                return Json( EnviarEmailContacto(contacto));
            }
            else {
                List<object> lista = new List<object>();
                lista.Add(new
                {
                    ok = 0,
                    msg = "Ocurrio un error al almacenar la información."
                });
                return Json(lista);
            }
          
        }

        public JsonResult EnviarEmailContacto(CONTACTO contacto)
        {
            List<object> lista = new List<object>();

            CONFIGURACIONES config = new ServicioConfiguracion().ObtenerConfiguracion();
            //USUARIOS usuario = new ServicioUsuario().ObtenerPorLogin(login);
            if (config != null)
            {

                var fromAddress = new MailAddress("noreply@Itelca.com", "ITELCA");
                var toAddress = new MailAddress(config.EMAIL,"ITELCA");
                string subject = "Nuevo Mensaje de Contacto";
                string body = "Usuario:" + contacto.NOMBRE + "\n \n"
                                +  "Fecha:" + contacto.FECHA + "\n \n"
                                + "Email:" + contacto.EMAIL + "\n \n"
                                + "Teléfono:" + contacto.TELEFONO + "\n \n"
                                + "Asunto:" + contacto.ASUNTO + "\n \n"
                                + "Mensaje:" + contacto.TEXTO + "\n \n";

                var smtp = new SmtpClient
                {
                    Host = "smtp.gmail.com",
                    Port = 587,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(fromAddress.Address, config.PWDEMAIL)
                };
                using (var message = new MailMessage(fromAddress, toAddress)
                {
                    Subject = subject,
                    Body = body
                })
                {
                    smtp.Send(message);


                    lista.Add(new
                    {
                        ok = 1,
                        msg = "Su mensaje ha sido enviado satisfactoriamente."
                    });


                }

            }
            else
            {
                lista.Add(new
                {
                    ok = 0,
                    msg = "Ocurrio un error al enviar la información."
                });
            }


            return Json(lista);
        }
    }
}
