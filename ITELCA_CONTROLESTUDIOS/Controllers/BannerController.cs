﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ITELCA_CLASSLIBRARY.Models;
using ITELCA_CONTROLESTUDIOS.Models.ViewModels;
using ITELCA_CLASSLIBRARY.Services;

namespace ITELCA_CONTROLESTUDIOS.Controllers
{
    [Authorize(Roles = "ADMINISTRADOR,COORDINADOR,WEB")]
    public class BannerController : Controller
    {

        BannerViewModel vistaModeloBanner;
        SelectList activo;

        public void IniciarVista(BANNERS banner) {
            Dictionary<int, string> lista = new Dictionary<int, string>();
            lista.Add(1, "SI");
            lista.Add(0, "NO");
            if (banner != null)
            {
                activo = new SelectList(lista, "Key", "Value", banner.ESTADO);              
            }
            else
            {
                activo = new SelectList(lista, "Key", "Value");
            
            }
            vistaModeloBanner = new BannerViewModel(banner, activo);
        }

        public ActionResult Crear()
        {
            BANNERS banner = new BANNERS();
            banner.FECHA_PUBLICACION = DateTime.Now;
            IniciarVista(banner);
            return View(vistaModeloBanner);
        }
      
        public ActionResult Editar(int id) { 
            BANNERS banner=new ServicioBanner().ObtenerPorClave(id);
            if (banner != null)
            {
                IniciarVista(banner);
                return View(vistaModeloBanner);
            }
            else
            {
                TempData["mensaje"] = "Banner no Encontrado";
                return RedirectToAction("Listar");
            }
        }

        public ActionResult Listar() {
            return View();
        }

        [HttpPost]
        public ActionResult Crear(BANNERS banner, HttpPostedFileBase[] file)
        {

            if (ModelState.IsValid)
            {

                string path = "";
                if (file != null && file[0] != null)
                {
                    path = System.IO.Path.Combine(Server.MapPath("~/Content/Web/Banner/"), System.IO.Path.GetFileName(file[0].FileName));
                    //---Añadiendo el nombre de la imagen amostrar 
                    banner.URL = file[0].FileName;
                    file[0].SaveAs(path);


                    ServicioBanner servicioBanner = new ServicioBanner();
                    if (!servicioBanner.Duplicado(banner))
                    {
                        USUARIOS usuario = new ServicioUsuario().ObtenerPorLogin(User.Identity.Name);
                        banner.USUARIO_ID = usuario.USUARIO_ID;
                        if (servicioBanner.Guardar(banner))
                        {
                            TempData["mensaje"] = "Banner Guardada Satisfactoriamente";
                            return RedirectToAction("Listar");
                        }
                        else
                            ModelState.AddModelError(string.Empty, "Error Guardando Banner");
                    }
                    else
                        ModelState.AddModelError(string.Empty, "Banner ya existe");


                }
                else
                {
                    ModelState.AddModelError("Url", "Es necesario agregar el url");
                }
            }
            
            IniciarVista(banner);
            return View(vistaModeloBanner);

        }

        [HttpPost]
        public ActionResult Editar(BANNERS banner, HttpPostedFileBase[] file)
        {
            if (ModelState.IsValid)
            {

                if (file[0] != null)
                {
                    file[0].SaveAs(System.IO.Path.Combine(Server.MapPath("~/Content/Web/Banner/"), System.IO.Path.GetFileName(file[0].FileName)));
                    banner.URL = file[0].FileName;
                }

                ServicioBanner servicioBanner = new ServicioBanner();
                if (!servicioBanner.Duplicado(banner))
                {
                    if (servicioBanner.Modificar(banner))
                    {
                        TempData["mensaje"] = "Banner Modificada Satisfactoriamente";
                        return RedirectToAction("Listar");
                    }
                    else
                        ModelState.AddModelError(string.Empty, "Error Modificando Banner");
                }
                else
                    ModelState.AddModelError(string.Empty, "Banner ya existe");

            }
            IniciarVista(banner);
            return View(vistaModeloBanner);

        }

        [HttpPost]
        public JsonResult Eliminar(int id)
        {
            return Json(new ServicioBanner().Eliminar(id));
        }
        [HttpPost]
        public JsonResult ObtenerData(string sidx, string sord, int page, int rows, string filters)
        {
            object resultado = new ServicioBanner().ObtenerDataGrid(sidx, sord, page, rows, filters);
            return Json(resultado);

        }

       

    }
}
