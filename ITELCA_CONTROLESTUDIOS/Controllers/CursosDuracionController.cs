﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ITELCA_CLASSLIBRARY.Models;
using ITELCA_CONTROLESTUDIOS.Models.ViewModels;
using ITELCA_CLASSLIBRARY.Services;
using Utilidades;

namespace ITELCA_CONTROLESTUDIOS.Controllers
{
     [Authorize(Roles = "ADMINISTRADOR,COORDINADOR")]
    public class CursosDuracionController : Controller
    {
        CursosDuracionViewModel vistaModelo;
        ServicioCursoDuracion servicioCursoDuracion = new ServicioCursoDuracion();
        SelectList ListaCursos, ListaTurnos;
        MultiSelectList multiCursos, multiTurnos;
        
        public void IniciarVista(CURSOSDURACIONES cursosDuracion) {

            Dictionary<int, string> lista = new ListasGenerales().ListaActivo();

            if (cursosDuracion != null)
            {
                ListaCursos = new SelectList(new ServicioCursos().ObtenerTodos(), "CURSO_ID", "NOMBRE", cursosDuracion.CURSO_ID);
                ListaTurnos = new SelectList(new ServicioTipoTurno().ObtenerTodos(), "TIPOTURNO_ID", "NOMBRE", cursosDuracion.TIPOTURNO_ID);
            }
            else
            {
                ListaCursos = new SelectList(new Dictionary<int, string>(), "Key", "Value");
                multiCursos = new MultiSelectList(new ServicioCursos().ObtenerTodos(), "CURSO_ID", "NOMBRE");
                multiTurnos = new MultiSelectList(new ServicioTipoTurno().ObtenerTodos(), "TIPOTURNO_ID", "NOMBRE");
            }
            vistaModelo = new CursosDuracionViewModel(cursosDuracion, ListaCursos,ListaTurnos,multiCursos, multiTurnos);
        }

        public ActionResult Crear()
        {
            IniciarVista(null);
            return View(vistaModelo);
        }

        public ActionResult Editar(string id)
        {
            var ids = id.Split('_');
            decimal curso_id = Decimal.Parse(ids[0]);
            decimal turno_id = Decimal.Parse(ids[1]);
            CURSOSDURACIONES cursosDuracion = servicioCursoDuracion.ObtenerPorId(curso_id, turno_id);
            if (cursosDuracion != null)
            {
                IniciarVista(cursosDuracion);
                return View(vistaModelo);
            }
            else
            {
                TempData["mensaje"] = "Concepto Curso no Encontrado.";
                return RedirectToAction("Listar");
            }
        }

        public ActionResult Listar() {
            return View();
        }

        [HttpPost]
        public ActionResult Crear([Bind(Prefix = "CURSOSDURACIONES")] CURSOSDURACIONES cursosDuracion, FormCollection collection)
        {
            if (ModelState.IsValid) {
                if (!String.IsNullOrEmpty(collection["CURSOSDURACIONES.CURSO_ID"]) ||
                    !String.IsNullOrEmpty(collection["CURSOSDURACIONES.TIPOTURNO_ID"]))
                {
                    cursosDuracion.MULTI_CURSO_ID = Request["CURSOSDURACIONES.CURSO_ID"].ToString();
                    cursosDuracion.MULTI_TIPOTURNO_ID = Request["CURSOSDURACIONES.TIPOTURNO_ID"].ToString();
                    
                    if (servicioCursoDuracion.Guardar(cursosDuracion))
                    {
                        TempData["mensaje"] = "Concepto Curso Guardado Satisfactoriamente.";
                        return RedirectToAction("Listar");
                    }
                    else
                        ModelState.AddModelError(string.Empty, "Error Guardando Concepto Curso");
                }
                else
                    ModelState.AddModelError("CURSO_ID", "Debe seleccionar al menos un curso y un turno.");
                
            }
            IniciarVista(null);
            return View(vistaModelo);

        }

        [HttpPost]
        public ActionResult Editar([Bind(Prefix = "CURSOSDURACIONES")] CURSOSDURACIONES cursosDuracion, FormCollection collection)
        {
            if (ModelState.IsValid)
            {

                if (servicioCursoDuracion.Modificar(cursosDuracion))
                {
                    TempData["mensaje"] = "Duración Curso Modificada Satisfactoriamente";
                    return RedirectToAction("Listar");
                }
                else
                    ModelState.AddModelError(string.Empty, "Error Modificando Duración Curso");
            }
            IniciarVista(cursosDuracion);
            return View(vistaModelo);

        }


        [HttpPost]
        public JsonResult Eliminar(string id)
        {
            var ids = id.Split('_');
            decimal curso_id = Decimal.Parse(ids[0]);
            decimal turno_id = Decimal.Parse(ids[1]);
            return Json(servicioCursoDuracion.Eliminar(curso_id, turno_id));
        }

        [HttpPost]
        public JsonResult ObtenerData(string sidx, string sord, int page, int rows, string filters)
        {
            object resultado = servicioCursoDuracion.ObtenerDataGrid(sidx, sord, page, rows, filters);
            return Json(resultado);

        }

    }
}
