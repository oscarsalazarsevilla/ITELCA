﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ITELCA_CLASSLIBRARY.Models;
using ITELCA_CONTROLESTUDIOS.Models.ViewModels;
using ITELCA_CLASSLIBRARY.Services;

namespace ITELCA_CONTROLESTUDIOS.Controllers
{
     [Authorize(Roles = "ADMINISTRADOR,COORDINADOR,WEB")]
    public class TipoFaqController : Controller
    {
        //
        // GET: /Rol/
        TipoFaqViewModel vistaModeloTipoFaq;
        SelectList activo;

        public void IniciarVista(TIPOS_FAQS tipoFaq) {
            Dictionary<int, string> lista = new Dictionary<int, string>();
            lista.Add(1, "SI");
            lista.Add(0, "NO");
            if (tipoFaq != null)
                activo = new SelectList(lista, "Key", "Value", tipoFaq.ESTADO);
            else
                activo = new SelectList(lista, "Key", "Value");
            vistaModeloTipoFaq = new TipoFaqViewModel(tipoFaq, activo);
        }

        public ActionResult Crear()
        {
            IniciarVista(null);
            return View(vistaModeloTipoFaq);
        }
        
        public ActionResult Editar(int id) { 
            TIPOS_FAQS tipoFaq=new ServicioTipoFaqs().ObtenerPorClave(id);
            if (tipoFaq != null)
            {
                IniciarVista(tipoFaq);
                return View(vistaModeloTipoFaq);
            }
            else
            {
                TempData["mensaje"] = "Tipo Faq no Encontrada";
                return RedirectToAction("Listar");
            }
        }

        public ActionResult Listar() {
            return View();
        }

        [HttpPost]
         [ValidateInput(false)]
        public ActionResult Crear(TIPOS_FAQS TIPOS_FAQS)
        {
            if (ModelState.IsValid) {
                ServicioTipoFaqs servicioTipoFaq = new ServicioTipoFaqs();
                if (!servicioTipoFaq.Duplicado(TIPOS_FAQS))
                {

                    USUARIOS usuario = new ServicioUsuario().ObtenerPorLogin(User.Identity.Name);
                    TIPOS_FAQS.USUARIO_ID = usuario.USUARIO_ID;
                    TIPOS_FAQS.ESTADO = 1;
                    TIPOS_FAQS.FECHA_CREACION = DateTime.Now;
                    if (servicioTipoFaq.Guardar(TIPOS_FAQS))
                    {
                        TempData["mensaje"] = "Tipo Faq Guardado Satisfactoriamente";
                        return RedirectToAction("Listar");
                    }
                    else
                        ModelState.AddModelError(string.Empty, "Error Guardando Tipo Faq");
                }
                else 
                    ModelState.AddModelError(string.Empty, "Tipo Faq ya existe");
                
            }
            IniciarVista(TIPOS_FAQS);
            return View(vistaModeloTipoFaq);

        }

        [HttpPost]
         [ValidateInput(false)]
        public ActionResult Editar(TIPOS_FAQS TIPOS_FAQS)
        {
            if (ModelState.IsValid)
            {
                ServicioTipoFaqs servicioTipoFaq = new ServicioTipoFaqs();
                if (!servicioTipoFaq.Duplicado(TIPOS_FAQS))
                {
                    if (servicioTipoFaq.Modificar(TIPOS_FAQS))
                    {
                        TempData["mensaje"] = "Tipo Faq Modificada Satisfactoriamente";
                        return RedirectToAction("Listar");
                    }
                    else
                        ModelState.AddModelError(string.Empty, "Error Modificando TipoFaq");
                }
                else
                    ModelState.AddModelError(string.Empty, "Tipo Faq ya existe");

            }
            IniciarVista(TIPOS_FAQS);
            return View(vistaModeloTipoFaq);

        }

        [HttpPost]
        public JsonResult Eliminar(int id)
        {
            return Json(new ServicioTipoFaqs().Eliminar(id));
        }
        [HttpPost]
        public JsonResult ObtenerData(string sidx, string sord, int page, int rows, string filters)
        {
            object resultado = new ServicioTipoFaqs().ObtenerDataGrid(sidx, sord, page, rows, filters);
            return Json(resultado);

        }

    }
}
