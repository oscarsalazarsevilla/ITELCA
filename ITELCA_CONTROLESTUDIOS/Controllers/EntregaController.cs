﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ITELCA_CLASSLIBRARY.Models;
using ITELCA_CONTROLESTUDIOS.Models.ViewModels;
using ITELCA_CLASSLIBRARY.Services;

namespace ITELCA_CONTROLESTUDIOS.Controllers
{
     [Authorize(Roles = "ADMINISTRADOR,COORDINADOR,EMPLEADO")]
    public class EntregaController : Controller
    {
        EntregaViewModel vistaModeloEntrega;
        ServicioEntrega servicioEntrega = new ServicioEntrega();
        SelectList recibo;
        SelectList sede;
        SelectList usuario;

        public void IniciarVista(ENTREGAS entregas)
        {

            //Dictionary<int, string> lista = new Dictionary<int, string>();
            //lista.Add(1, "SI");
            //lista.Add(0, "NO");

            if (entregas != null)
            {
                recibo = new SelectList(new ServicioDetalleRecibo().ObtenerPendientesPorEntregar(), "Key", "Value", entregas.DETALLERECIBO_ID);
                sede = new SelectList(new ServicioSede().ObtenerTodos(), "SEDE_ID", "NOMBRE", entregas.SEDE_ID);
                usuario = new SelectList(new ServicioTipoAula().ObtenerTodos(), "USUARIO_ID", "NOMBRE", entregas.USUARIO_ID);
            }
            else
            {
                recibo = new SelectList(new ServicioDetalleRecibo().ObtenerPendientesPorEntregar(), "Key", "Value");
                //activo = new SelectList(lista, "Key", "Value");
                sede = new SelectList(new ServicioSede().ObtenerTodos(), "SEDE_ID", "NOMBRE");
                usuario = new SelectList(new ServicioTipoAula().ObtenerTodos(), "USUARIO_ID", "NOMBRE");

            }
            vistaModeloEntrega = new EntregaViewModel(entregas, recibo, sede, usuario);
        }

        public ActionResult Crear()
        {
            IniciarVista(null);
            return View(vistaModeloEntrega);
        }

        public ActionResult Editar(int id)
        {
            ENTREGAS entrega = servicioEntrega.ObtenerPorClave(id);
            if (entrega != null)
            {
                IniciarVista(entrega);
                return View(vistaModeloEntrega);
            }
            else
            {
                TempData["mensaje"] = "Entrega no Encontrada";
                return RedirectToAction("Listar");
            }
        }

        public ActionResult Listar()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Crear(ENTREGAS entrega)
        {
            if (ModelState.IsValid)
            {
                

                USUARIOS usuario = new ServicioUsuario().ObtenerPorLogin(User.Identity.Name);
                entrega.USUARIO_ID = usuario.USUARIO_ID;
                if (servicioEntrega.Guardar(entrega))
                {
                    TempData["mensaje"] = "Entrega Guardada Satisfactoriamente";
                    return RedirectToAction("Listar");
                }
                else
                    ModelState.AddModelError(string.Empty, "Error Guardando Entrega");



            }
            IniciarVista(entrega);
            return View(vistaModeloEntrega);

        }

        [HttpPost]
        public ActionResult Editar(ENTREGAS entrega)
        {
            if (ModelState.IsValid)
            {
               
                if (servicioEntrega.Modificar(entrega))
                {
                    TempData["mensaje"] = "Entrega Modificada Satisfactoriamente";
                    return RedirectToAction("Listar");
                }
                else
                    ModelState.AddModelError(string.Empty, "Error Modificando Entrega");


            }
            IniciarVista(entrega);
            return View(vistaModeloEntrega);

        }

        [HttpPost]
        public JsonResult Eliminar(int id)
        {
            return Json(new ServicioEntrega().Eliminar(id));
        }

        [HttpPost]
        public JsonResult ObtenerData(string sidx, string sord, int page, int rows, string filters)
        {
            object resultado = servicioEntrega.ObtenerDataGrid(sidx, sord, page, rows, filters);
            return Json(resultado);

        }


    }
}
