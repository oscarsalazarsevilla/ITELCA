﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ITELCA_CLASSLIBRARY.Models;
using ITELCA_CONTROLESTUDIOS.Models.ViewModels;
using ITELCA_CLASSLIBRARY.Services;

namespace ITELCA_CONTROLESTUDIOS.Controllers
{
     [Authorize(Roles = "ADMINISTRADOR,COORDINADOR")]
    public class CursoController : Controller
    {
        //
        // GET: /Rol/
        CursoViewModel vistaModeloCurso;
        SelectList activo;
        SelectList duracion;
        SelectList tipoCurso;
        //MultiSelectList talleres;
        MultiSelectList listaTalleres;
        
        public void IniciarVista(CURSOS curso) {
            Dictionary<int, string> lista = new Dictionary<int, string>();
            lista.Add(1, "SI");
            lista.Add(0, "NO");

            Dictionary<string, string> listaDuracion = new Dictionary<string, string>();
            listaDuracion.Add("C", "Corto");
            listaDuracion.Add("L", "Largo");

            
            if (curso != null){
                activo = new SelectList(lista, "Key", "Value", curso.ESTADO);
                duracion = new SelectList(listaDuracion, "Key", "Value", curso.DURACION);
                tipoCurso=new SelectList(new ServicioTipoCurso().ObtenerTodos(),"TIPO_CURSO_ID","NOMBRE",curso.TIPO_CURSO_ID);
                listaTalleres = new MultiSelectList(new ServicioTaller().ObtenerTodos(), "TALLER_ID", "NOMBRE", curso.TALLERES.Select(u => u.TALLER_ID));
                
            }else{
                activo = new SelectList(lista, "Key", "Value");
                duracion = new SelectList(listaDuracion, "Key", "Value");
                tipoCurso=new SelectList(new ServicioTipoCurso().ObtenerTodos(),"TIPO_CURSO_ID","NOMBRE");
                listaTalleres = new MultiSelectList(new ServicioTaller().ObtenerTodos(), "TALLER_ID", "NOMBRE");
            }
            vistaModeloCurso = new CursoViewModel(curso, activo, tipoCurso, listaTalleres,duracion);
        }

        public ActionResult Crear()
        {
            IniciarVista(null);
            return View(vistaModeloCurso);
        }
        
        public ActionResult Editar(string id) { 
            CURSOS curso=new ServicioCursos().ObtenerPorClave(long.Parse(id));
            if (curso != null)
            {
                IniciarVista(curso);
                return View(vistaModeloCurso);
            }
            else
            {
                TempData["mensaje"] = "Curso no Encontrado";
                return RedirectToAction("Listar");
            }
        }

        public ActionResult Listar() {
            return View();
        }

        public ActionResult Index() 
        {
            ServicioCursos servicio = new ServicioCursos();
            List<String> lista = new List<string>();
            foreach (var item in servicio.Lista_Cursos_Index())
            {
                lista.Add(item);
            }
            CursoViewModel _viewModel = new CursoViewModel(null,null,null,null,null);
            return PartialView(_viewModel);
        }

        [HttpPost]
        public ActionResult Crear(CURSOS curso,FormCollection form) {
            if (ModelState.IsValid) {
                string talleres = "";
                if (form["tallerMultiselect"] != null)
                {
                    talleres = form["tallerMultiselect"].ToString();
                }
                
                    ServicioCursos servicioCurso = new ServicioCursos();

                    if (!servicioCurso.Duplicado(curso))
                    {
                        USUARIOS usuario = new ServicioUsuario().ObtenerPorLogin(User.Identity.Name);
                        curso.USUARIO_ID = usuario.USUARIO_ID;
                        if (servicioCurso.Guardar(curso,talleres))
                        {
                            TempData["mensaje"] = "Curso Guardado Satisfactoriamente";
                            return RedirectToAction("Listar");
                        }
                        else
                            ModelState.AddModelError(string.Empty, "Error Guardando Curso");
                    }
                    else 
                        ModelState.AddModelError(string.Empty, "Curso ya existe");
                }else{
                     ModelState.AddModelError(string.Empty, "Debe Elegir taller");
                }
                
            
            IniciarVista(curso);
            return View(vistaModeloCurso);

        }

        [HttpPost]
        public ActionResult Editar(CURSOS curso,FormCollection form)
        {
            if (ModelState.IsValid)
            {
                string talleres = "";
                if (form["tallerMultiselect"] != null)
                {
                    talleres = form["tallerMultiselect"].ToString();
                }
                    ServicioCursos servicioCurso = new ServicioCursos();
                    if (!servicioCurso.Duplicado(curso))
                    {
                        USUARIOS usuario = new ServicioUsuario().ObtenerPorLogin(User.Identity.Name);
                        curso.USUARIO_ID = usuario.USUARIO_ID;
                        if (servicioCurso.Modificar(curso,talleres))
                        {
                            TempData["mensaje"] = "Curso Modificado Satisfactoriamente";
                            return RedirectToAction("Listar");
                        }
                        else
                            ModelState.AddModelError(string.Empty, "Error Modificando Curso");
                    }
                    else
                        ModelState.AddModelError(string.Empty, "Curso ya existe");
                }
                else {
                    ModelState.AddModelError(string.Empty, "Debe Seleccionar Talleres");
                }
            
            IniciarVista(curso);
            return View(vistaModeloCurso);

        }

        [HttpPost]
        public JsonResult Eliminar(int id)
        {
            return Json(new ServicioCursos().Eliminar(id));
        }
        [HttpPost]
        public JsonResult ObtenerData(string sidx, string sord, int page, int rows, string filters)
        {
            object resultado = new ServicioCursos().ObtenerDataGrid(sidx, sord, page, rows, filters);
            return Json(resultado);

        }

    }
}
