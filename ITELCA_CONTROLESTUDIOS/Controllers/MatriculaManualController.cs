﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ITELCA_CLASSLIBRARY.Models;
using ITELCA_CONTROLESTUDIOS.Models.ViewModels;
using ITELCA_CLASSLIBRARY.Services;
using ITELCA_CLASSLIBRARY.CustomClasses;
namespace ITELCA_CONTROLESTUDIOS.Controllers
{
    public class MatriculaManualController : Controller
    {
        //
        // GET: /MatriculaManual/
        SelectList aulas;
        SelectList turnos;
        SelectList modulos;
        SelectList sedes;
        SelectList cursos;
        SelectList generos;
        SelectList listaTaller;
        SelectList listaEmpleados;
        MatriculaManualViewModel vistaModelo;

        public void IniciarVista()
        {
            USUARIOS usuario = new ServicioUsuario().ObtenerPorLogin(User.Identity.Name);
            generos = new SelectList(new ServicioGenero().ObtenerLista(), "GENERO_ID", "NOMBRE", usuario.GENERO_ID);
            aulas = new SelectList(new ServicioAula().ObtenerDiccionarioAulas(), "Key", "Value");
            turnos = new SelectList(new ServicioTurno().ObtenerDiccionarioTurno(), "Key", "Value");
            modulos = new SelectList(new ServicioModulo().ObtenerDiccionarioModulo(), "Key", "Value");
            sedes = new SelectList(new ServicioSede().ObtenerTodos(), "SEDE_ID", "NOMBRE");
            cursos = new SelectList(new ServicioCursos().ObtenerTodos(), "CURSO_ID", "NOMBRE");
            listaTaller=new SelectList( new ServicioTaller().ObtenerTodos(),"TALLER_ID","NOMBRE");
            listaEmpleados = new SelectList(new ServicioUsuario().ObtenerListaEmpleados(), "Key", "Value");
            vistaModelo = new MatriculaManualViewModel(turnos, sedes, cursos, generos,listaTaller,listaEmpleados);

        }

        public ActionResult Index()
        {
            IniciarVista();
            return View(vistaModelo);
        }

    }
}
