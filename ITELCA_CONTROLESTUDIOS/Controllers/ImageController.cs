﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ITELCA_CLASSLIBRARY.CustomClasses;
using ITELCA_CONTROLESTUDIOS.Models.ViewModels;
using ITELCA_CLASSLIBRARY.Models;
using System.IO;
using ITELCA_CLASSLIBRARY.Models.CustomClasses;
using ITELCA_CONTROLESTUDIOS.Models;


namespace ITELCA_CONTROLESTUDIOS.Controllers
{
    public class ImageController : Controller
    {
        //ImageServices _imageservice;
        public ImageController()
        {
            //_imageservice = new ImageServices();
        }

        public ActionResult Index()
        {
            return View();
        }

        //public JsonResult Delete(int id)
        //{
        //    int message = 0;
        //    if (_imageservice.Delete(id))
        //        message = 1;
        //    return Json(message);
        //}

        public ActionResult Browse(string CKEditorFuncNum)
        {
            List<FileInformation> fileInfoList = GetCurrentFiles();

            var model = new FileListingViewModel
            {
                Files = fileInfoList,
                CKEditorFuncNum = CKEditorFuncNum
            };

            return View(model);
        }

        public ActionResult Upload(HttpPostedFileBase upload, string CKEditorFuncNum, string CKEditor, string langCode)
        {
            string fileName = upload.FileName;

            string basePath = Server.MapPath("~/Uploads");
            upload.SaveAs(basePath + "\\" + fileName);

            return View();
        }

        private List<FileInformation> GetCurrentFiles()
        {
            string basePath = Server.MapPath("~/Uploads");

            List<FileInformation> fileInfoList = new List<FileInformation>();

            string[] files = Directory.GetFiles(basePath);

            files.ToList().ForEach(file =>
            {
                fileInfoList.Add(new FileInformation { FileName = Path.GetFileName(file) });
            });
            return fileInfoList;
        }
    }
}
