﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ITELCA_CLASSLIBRARY.Models;
using ITELCA_CONTROLESTUDIOS.Models.ViewModels;
using ITELCA_CLASSLIBRARY.Services;

namespace ITELCA_CONTROLESTUDIOS.Controllers
{
    [Authorize(Roles = "ADMINISTRADOR,COORDINADOR,WEB")]
    public class CategoriaController : Controller
    {

        CategoriaViewModel _vistaModeloCategoria;
        SelectList _listaCategorias, _listaTipos, _activo;

        public void IniciarVista(CATEGORIAS categoria) {
            Dictionary<int, string> lista = new Dictionary<int, string>();
            lista.Add(1, "SI");
            lista.Add(0, "NO");
            if (categoria != null)
            {
                _activo = new SelectList(lista, "Key", "Value", categoria.ESTADO);
                _listaCategorias = new SelectList(new ServicioCategoria().ObtenerDiccionarioCategorias(), "Key", "Value", categoria.PADRE_ID);
                _listaTipos = new SelectList(new ServicioTipoCategoria().ObtenerDiccionarioTiposCategorias(), "Key", "Value", categoria.TIPOCATEGORIA_ID);
            }
            else
            {
                _activo = new SelectList(lista, "Key", "Value");
                _listaCategorias = new SelectList(new ServicioCategoria().ObtenerDiccionarioCategorias(), "Key", "Value");
                _listaTipos = new SelectList(new ServicioTipoCategoria().ObtenerDiccionarioTiposCategorias(), "Key", "Value");
            
            }
            _vistaModeloCategoria = new CategoriaViewModel(categoria, _activo, _listaCategorias, _listaTipos);
        }

        public ActionResult Crear()
        {
            IniciarVista(null);
            return View(_vistaModeloCategoria);
        }
      
        public ActionResult Editar(int id) { 
            CATEGORIAS categoria=new ServicioCategoria().ObtenerPorClave(id);
            if (categoria != null)
            {
                IniciarVista(categoria);
                return View(_vistaModeloCategoria);
            }
            else
            {
                TempData["mensaje"] = "Categoria no Encontrado";
                return RedirectToAction("Listar");
            }
        }

        public ActionResult Listar() {
            return View();
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Crear(CATEGORIAS categoria, HttpPostedFileBase[] file)
        {

            if (ModelState.IsValid)
            {
                ServicioCategoria servicioCategoria = new ServicioCategoria();
                    if (!servicioCategoria.Duplicado(categoria))
                    {
                        USUARIOS usuario = new ServicioUsuario().ObtenerPorLogin(User.Identity.Name);
                        categoria.USUARIO_ID = usuario.USUARIO_ID;
                        if (servicioCategoria.Guardar(categoria))
                        {
                            TempData["mensaje"] = "Categoria Guardada Satisfactoriamente";
                            return RedirectToAction("Listar");
                        }
                        else
                            ModelState.AddModelError(string.Empty, "Error Guardando Categoria");
                    }
                    else
                        ModelState.AddModelError(string.Empty, "Categoria ya existe");
                }
            IniciarVista(categoria);
            return View(_vistaModeloCategoria);

        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Editar(CATEGORIAS categoria, HttpPostedFileBase[] file)
        {
            if (ModelState.IsValid)
            {
                ServicioCategoria servicioCategoria = new ServicioCategoria();
                if (!servicioCategoria.Duplicado(categoria))
                {
                    if (servicioCategoria.Modificar(categoria))
                    {
                        TempData["mensaje"] = "Categoria Modificada Satisfactoriamente";
                        return RedirectToAction("Listar");
                    }
                    else
                        ModelState.AddModelError(string.Empty, "Error Modificando Categoria");
                }
                else
                    ModelState.AddModelError(string.Empty, "Categoria ya existe");

            }
            IniciarVista(categoria);
            return View(_vistaModeloCategoria);

        }

        [HttpPost]
        public JsonResult Eliminar(int id)
        {
            return Json(new ServicioCategoria().Eliminar(id));
        }
        [HttpPost]
        public JsonResult ObtenerData(string sidx, string sord, int page, int rows, string filters)
        {
            object resultado = new ServicioCategoria().ObtenerDataGrid(sidx, sord, page, rows, filters);
            return Json(resultado);

        }

       

    }
}
