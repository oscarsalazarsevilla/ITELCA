﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
namespace ITELCA_CONTROLESTUDIOS.Models
{

    public class CambiarClaveModels
    {
        [Required(ErrorMessage = "Contraseña Actual Requerida")]
        [StringLength(100, ErrorMessage = "El {0} debe ser mayor a 3 caracteres  y menor a 20", MinimumLength = 3)]
        [DataType(DataType.Password)]
        [Display(Name = "Contraseña Actual")]
        public string passActual { get; set;}

        [Required(ErrorMessage = "Contraseña Nueva Requerida")]
        [StringLength(20, ErrorMessage = "El {0} debe ser mayor a 3 caracteres  y menor a 20", MinimumLength = 3)]
        [DataType(DataType.Password)]
        [Display(Name = "Contraseña Nueva")]
        public string passNueva { get; set; }

        [Required(ErrorMessage = "Confirmación Requerida")]
        [StringLength(20, ErrorMessage = "El {0} debe ser mayor a 3 caracteres  y menor a 20", MinimumLength = 3)]
        [DataType(DataType.Password)]
        [Display(Name = "Confirmar Nueva Contraseña")]
        [Compare("passNueva", ErrorMessage = "Contraseñas Distintas")]
        public string confirmPass { get; set; }

    }
}
