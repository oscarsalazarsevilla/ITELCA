﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ITELCA_CLASSLIBRARY.Models;
using System.Web.Mvc;

namespace ITELCA_CONTROLESTUDIOS.Models.ViewModels
{
    public class CategoriaViewModel
    {
        public CATEGORIAS categoria { get; set; }
        public SelectList activo { get; set; }
        public SelectList listaCategorias { get; set; }
        public SelectList listaTipos { get; set; }

        public CategoriaViewModel(  CATEGORIAS categoria, SelectList activo,
                                    SelectList listaCategorias, SelectList listaTipos)
        {
            this.categoria = categoria;
            this.activo = activo;
            this.listaCategorias = listaCategorias;
            this.listaTipos = listaTipos;
        }
    }
}