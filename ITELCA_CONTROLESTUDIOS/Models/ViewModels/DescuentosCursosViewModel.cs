﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ITELCA_CLASSLIBRARY.Models;
using System.Web.Mvc;

namespace ITELCA_CONTROLESTUDIOS.Models.ViewModels
{
    public class DescuentosCursosViewModel
    {
        public DESCUENTOS_CURSOS DESCUENTOS_CURSOS { get; set; }
        public SelectList ListaCursos { get; set; }
        public SelectList ListaTurnos { get; set; }
        public string ListaCursosSeleccionados { get; set; }


        public DescuentosCursosViewModel(DESCUENTOS_CURSOS DESCUENTOS_CURSOS, SelectList ListaCursos, SelectList ListaTurnos, string ListaCursosSeleccionados)
        {
            this.DESCUENTOS_CURSOS = DESCUENTOS_CURSOS;
            this.ListaCursos = ListaCursos;
            this.ListaTurnos = ListaTurnos;
            this.ListaCursosSeleccionados = ListaCursosSeleccionados;
        }
    }
}