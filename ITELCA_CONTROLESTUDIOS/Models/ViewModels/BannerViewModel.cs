﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ITELCA_CLASSLIBRARY.Models;
using System.Web.Mvc;

namespace ITELCA_CONTROLESTUDIOS.Models.ViewModels
{
    public class BannerViewModel
    {
        public BANNERS banner { get; set; }
        public SelectList activo { get; set; }
        

        public BannerViewModel(BANNERS banner, SelectList activo)
        {
            this.banner = banner;
            this.activo = activo;
            
        }
    }
}