﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ITELCA_CLASSLIBRARY.Models;
using System.Web.Mvc;

namespace ITELCA_CONTROLESTUDIOS.Models.ViewModels
{
    public class OfertasTalleresViewModel
    {
        public OFERTASTALLERES OFERTASTALLERES { get; set; }
        public SelectList activo { get; set; }
        public SelectList listaAulas { get; set; }
        public SelectList listaTalleres { get; set; }
        public SelectList profesores { get; set; }
        public OfertasTalleresViewModel(OFERTASTALLERES ofertaTalleres, SelectList activo, SelectList aulas, SelectList talleres,SelectList profesores)
        {
            this.OFERTASTALLERES = ofertaTalleres;
            this.activo = activo;
            this.listaAulas = aulas;
            this.listaTalleres = talleres;
            this.profesores = profesores;
        }
    }
}