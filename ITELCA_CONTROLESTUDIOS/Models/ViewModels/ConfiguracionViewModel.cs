﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ITELCA_CLASSLIBRARY.Models;
using System.Web.Mvc;

namespace ITELCA_CONTROLESTUDIOS.Models.ViewModels
{
    public class ConfiguracionViewModel
    {
        public CONFIGURACIONES configuracion { get; set; }

        public ConfiguracionViewModel(CONFIGURACIONES configuracion)
        {
            this.configuracion = configuracion;
        }
    }
}