﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ITELCA_CLASSLIBRARY.Models;
using System.Web.Mvc;

namespace ITELCA_CONTROLESTUDIOS.Models.ViewModels
{
    public class SeccionViewModel
    {
        public SECCIONES seccion { get; set; }
        public SelectList categorias { get; set; }
        public SelectList activo { get; set; }

        public SeccionViewModel(SECCIONES seccion, SelectList categorias, SelectList activo)
        {
            this.seccion = seccion;
            this.categorias = categorias;
            this.activo = activo;
        }
    }
}