﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ITELCA_CLASSLIBRARY.Models;
using System.Web.Mvc;

namespace ITELCA_CONTROLESTUDIOS.Models.ViewModels
{
    public class RolViewModel
    {
        public ROLES rol { get;set; }
        public SelectList activo { get; set; } 
        public RolViewModel(ROLES rol,SelectList activo) {
            this.rol = rol;
            this.activo = activo;
        }
    }
}