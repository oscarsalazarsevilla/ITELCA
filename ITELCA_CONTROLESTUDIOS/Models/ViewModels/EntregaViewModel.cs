﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ITELCA_CLASSLIBRARY.Models;
using System.Web.Mvc;

namespace ITELCA_CONTROLESTUDIOS.Models.ViewModels
{
    public class EntregaViewModel
    {
        public ENTREGAS entrega { get; set; }
        public SelectList recibo { get; set; }
        public SelectList sedes { get; set; }
        public SelectList usuario { get; set; }


        public EntregaViewModel(ENTREGAS entrega, SelectList recibo, SelectList sedes, SelectList usuario)
        {
            this.entrega = entrega;
            this.recibo = recibo;
            this.sedes = sedes;
            this.usuario = usuario;
        }
    }
}