﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ITELCA_CONTROLESTUDIOS.Models.ViewModels
{
    public class ViewModelForMasterWrapper<TMasterModel>
    {
        public ViewModelForMasterWrapper(TMasterModel masterModel)
        {
            Master = masterModel;
        }

        /// <summary>
        /// Gets or sets the master model.
        /// </summary>
        /// <value>The master model.</value>
        public TMasterModel Master { get; set; }
    }
}