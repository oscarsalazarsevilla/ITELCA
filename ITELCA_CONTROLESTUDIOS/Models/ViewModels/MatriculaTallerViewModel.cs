﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ITELCA_CLASSLIBRARY.Models;
using System.Web.Mvc;

namespace ITELCA_CONTROLESTUDIOS.Models.ViewModels
{
    public class MatriculaTallerViewModel
    {
        public MATRICULASTALLERES MATRICULASTALLERES { get; set; }
        public SelectList activo { get; set; }
        public SelectList listaSedes { get; set; }
        public SelectList listaTaller { get; set; }
        public SelectList listaTallerGrid { get; set; }
        public SelectList generos { get; set; }
        public USUARIOS USUARIO { get; set; }
        public MatriculaTallerViewModel(MATRICULASTALLERES matriculaTaller, SelectList activo,  SelectList listaSedes, SelectList listaTaller, SelectList listaTalleresGrid,SelectList genero)
        {
            this.MATRICULASTALLERES = matriculaTaller;
            this.activo = activo;
            this.listaSedes = listaSedes;
            this.listaTaller = listaTaller;
            this.listaTallerGrid =listaTalleresGrid;
            this.generos = genero;
            this.USUARIO = new USUARIOS();
        }
    }
}