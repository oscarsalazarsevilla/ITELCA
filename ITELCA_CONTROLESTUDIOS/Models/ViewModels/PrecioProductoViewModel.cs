﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ITELCA_CLASSLIBRARY.Models;
using System.Web.Mvc;

namespace ITELCA_CONTROLESTUDIOS.Models.ViewModels
{
    public class PrecioProductoViewModel
    {
        public PRECIOSPRODUCTOS precioProducto { get;set; }
        public SelectList productos { get; set; }
        public SelectList activo { get; set; }
        
        public PrecioProductoViewModel(PRECIOSPRODUCTOS precioProducto, SelectList activo,SelectList productos)
        {
            this.precioProducto = precioProducto;
            this.activo = activo;
            this.productos = productos;
        }
    }
}