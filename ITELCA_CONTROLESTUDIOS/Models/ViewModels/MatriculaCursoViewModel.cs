﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ITELCA_CLASSLIBRARY.Models;
using System.Web.Mvc;

namespace ITELCA_CONTROLESTUDIOS.Models.ViewModels
{
    public class MatriculaCursoViewModel
    {
        public MATRICULASCURSOS MATRICULASCURSOS { get; set; }
        public SelectList activo { get; set; }
        public SelectList listaTurnos { get; set; }
        public SelectList listaSedes { get; set; }
        public SelectList listaCursos { get; set; }
        public SelectList listaCursosGrid { get; set; }
        public SelectList generos { get; set; }
        public SelectList listaVacia { get; set; }
        public USUARIOS USUARIO;
        public MatriculaCursoViewModel(MATRICULASCURSOS matriculaCurso, SelectList activo, SelectList turnos, SelectList listaSedes, SelectList listaCursos, SelectList listaCursosGrid, SelectList generos, SelectList listaVacia)
        {
            this.MATRICULASCURSOS = matriculaCurso;
            this.activo = activo;
            this.listaTurnos = turnos;
            this.listaSedes = listaSedes;
            this.listaCursos = listaCursos;
            this.listaCursosGrid = listaCursosGrid;
            this.generos = generos;
            this.listaVacia = listaVacia;
            this.USUARIO = new USUARIOS();
        }
    }
}