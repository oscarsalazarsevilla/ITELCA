﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ITELCA_CLASSLIBRARY.Models;
using System.Web.Mvc;

namespace ITELCA_CONTROLESTUDIOS.Models.ViewModels
{
    public class ConceptoCursoViewModel
    {
        public CONCEPTOS_CURSOS CONCEPTOS_CURSOS { get;set; }
        public SelectList activo { get; set; }
        public SelectList ListaCursos { get; set; }
        public SelectList ListaConceptos { get; set; }
        public SelectList ListaTurnos { get; set; }
        public MultiSelectList MultiCursos { get; set; }
        public MultiSelectList MultiTurnos { get; set; }
        
        public ConceptoCursoViewModel(  CONCEPTOS_CURSOS CONCEPTOS_CURSOS, SelectList activo, 
                                        SelectList ListaCursos, SelectList ListaConceptos, SelectList ListaTurnos,
                                        MultiSelectList MultiCursos, MultiSelectList MultiTurnos)
        {
            this.CONCEPTOS_CURSOS = CONCEPTOS_CURSOS;
            this.activo = activo;
            this.ListaConceptos = ListaConceptos;
            this.ListaCursos = ListaCursos;
            this.MultiCursos = MultiCursos;
            this.MultiTurnos = MultiTurnos;
            this.ListaTurnos = ListaTurnos;
        }
    }
}