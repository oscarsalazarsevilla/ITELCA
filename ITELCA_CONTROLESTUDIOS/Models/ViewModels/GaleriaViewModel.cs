﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ITELCA_CLASSLIBRARY.Models;
using System.Web.Mvc;

namespace ITELCA_CONTROLESTUDIOS.Models.ViewModels
{
    public class GaleriaViewModel
    {
        public GALERIAS galeria { get;set; }
        public SelectList activo { get; set; }
        
        public GaleriaViewModel(GALERIAS galeria, SelectList activo)
        {
            this.galeria = galeria;
            this.activo = activo;
        }
    }
}