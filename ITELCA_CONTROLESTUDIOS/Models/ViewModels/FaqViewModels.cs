﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ITELCA_CLASSLIBRARY.Models;
using System.Web.Mvc;

namespace ITELCA_CONTROLESTUDIOS.Models.ViewModels
{
    public class FaqViewModel
    {
        public FAQS faq { get; set; }
        public SelectList activo { get; set; }
        public SelectList listaTiposFaqs { get; set; }

        public FaqViewModel(FAQS faq, SelectList activo, SelectList listaTiposFaqs)
        {
            this.faq = faq;
            this.activo = activo;
            this.listaTiposFaqs = listaTiposFaqs;
        }
    }
}