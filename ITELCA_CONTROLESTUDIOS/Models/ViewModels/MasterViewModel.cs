﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ITELCA_CLASSLIBRARY.Models;

namespace ITELCA_CONTROLESTUDIOS.Models.ViewModels
{
    public class MasterViewModel
    {
        public CONFIGURACIONES configuracion { get; private set; }
        public List<CATEGORIAS> categoriasPadres { get; private set; }
        public LogOnModel logOn { get; private set; }
        public string menu { get; private set; }
             
        public MasterViewModel( CONFIGURACIONES configuracion,
                                List<CATEGORIAS> categoriasPadres,
                                LogOnModel logOn,
                                string menu)
        {
            this.configuracion = configuracion;
            this.categoriasPadres = categoriasPadres;
            this.menu = menu;
            this.logOn = logOn;
        } 
    }
}