﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ITELCA_CLASSLIBRARY.Models;
using System.Web.Mvc;

namespace ITELCA_CONTROLESTUDIOS.Models.ViewModels
{
    public class FeriadoViewModel
    {
        public FERIADOS FERIADOS { get;set; }
        public SelectList activo { get; set; }
        public SelectList listaMeses { get; set; }
        public SelectList listaDias { get; set; }
        public SelectList listaPeriodicidad{ get; set; }

        public FeriadoViewModel(FERIADOS feriado, SelectList activo, SelectList meses,SelectList dias ,SelectList periodico)
        {
            this.FERIADOS = feriado;
            this.activo = activo;
            this.listaMeses = meses;
            this.listaDias = dias;
            this.listaPeriodicidad = periodico;
        }
    }
}