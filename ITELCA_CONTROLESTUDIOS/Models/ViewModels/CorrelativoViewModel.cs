﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ITELCA_CLASSLIBRARY.Models;
using System.Web.Mvc;

namespace ITELCA_CONTROLESTUDIOS.Models.ViewModels
{
    public class CorrelativoViewModel
    {
        public CORRELATIVOS correlativo { get; set; }
        public SelectList sedes { get; set; }
        public SelectList tipoDocumento { get; set; }
        public CorrelativoViewModel(CORRELATIVOS correlativo,  SelectList sedes,SelectList tipoDocumento)
        {
            this.correlativo = correlativo;
            this.sedes = sedes;
            this.tipoDocumento = tipoDocumento;
        }
    }
}