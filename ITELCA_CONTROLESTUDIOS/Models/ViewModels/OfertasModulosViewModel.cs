﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ITELCA_CLASSLIBRARY.Models;
using System.Web.Mvc;

namespace ITELCA_CONTROLESTUDIOS.Models.ViewModels
{
    public class OfertasModulosViewModel
    {
        public OFERTASMODULOS OFERTASMODULOS { get; set; }
        public OFERTASMODULOS AUTOFERTAMOD { get; set; }
        public OFERTASCURSOS OFERTASCURSOS { get; set; }
        public SelectList listaAulas { get; set; }
        public SelectList listaModulos { get; set; }
        public SelectList listaCursosGrid { get; set; }
        public SelectList listaProfesores { get; set; }
        public SelectList listaProfesoresCurso { get; set; }
        public string id_oferta_curso { get; set; }
        public string curso_id { get; set; }

        public OfertasModulosViewModel(OFERTASMODULOS ofertaModulos, SelectList aulas,  SelectList modulos, SelectList listaCursosGrid, OFERTASCURSOS ofertaCurso, SelectList listaProfesores, SelectList listaProfesoresCurso)
        {
            this.OFERTASMODULOS = ofertaModulos;
            this.OFERTASCURSOS = ofertaCurso;
            this.listaAulas = aulas;
            this.listaModulos = modulos;
            this.listaCursosGrid = listaCursosGrid;
            this.id_oferta_curso = id_oferta_curso;
            this.curso_id = curso_id;
            this.listaProfesores = listaProfesores;
            this.listaProfesoresCurso = listaProfesoresCurso;
            this.AUTOFERTAMOD = null;
        }
    }
}