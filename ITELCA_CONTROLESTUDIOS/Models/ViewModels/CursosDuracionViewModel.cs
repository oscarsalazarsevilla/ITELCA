﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ITELCA_CLASSLIBRARY.Models;
using System.Web.Mvc;

namespace ITELCA_CONTROLESTUDIOS.Models.ViewModels
{
    public class CursosDuracionViewModel
    {
        public CURSOSDURACIONES CURSOSDURACIONES { get; set; }
        public SelectList ListaCursos { get; set; }
        public SelectList ListaTurnos { get; set; }
        public MultiSelectList MultiCursos { get; set; }
        public MultiSelectList MultiTurnos { get; set; }

        public CursosDuracionViewModel(CURSOSDURACIONES CURSOSDURACIONES, 
                                        SelectList ListaCursos, SelectList ListaTurnos,
                                        MultiSelectList MultiCursos, MultiSelectList MultiTurnos)
        {
            this.CURSOSDURACIONES = CURSOSDURACIONES;
            this.ListaCursos = ListaCursos;
            this.MultiCursos = MultiCursos;
            this.MultiTurnos = MultiTurnos;
            this.ListaTurnos = ListaTurnos;
        }
    }
}