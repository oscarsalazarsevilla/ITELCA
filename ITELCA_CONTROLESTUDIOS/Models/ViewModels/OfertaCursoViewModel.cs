﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ITELCA_CLASSLIBRARY.Models;
using System.Web.Mvc;

namespace ITELCA_CONTROLESTUDIOS.Models.ViewModels
{
    public class OfertaCursoViewModel
    {
        public OFERTASCURSOS OFERTASCURSOS { get; set; }
        public SelectList listaTurnos { get; set; }
        public SelectList listaSedes { get; set; }
        public SelectList listaCursos { get; set; }
        public SelectList listaCursosGrid { get; set; }
        public OfertasModulosViewModel viewModelModulo { get; set; }

        public OfertaCursoViewModel(OFERTASCURSOS ofertaCurso, SelectList turnos, SelectList listaSedes, SelectList listaCursos, SelectList listaCursosGrid, OfertasModulosViewModel viewModelModulo)
        {
            this.OFERTASCURSOS = ofertaCurso;
            this.listaTurnos = turnos;
            this.listaSedes = listaSedes;
            this.listaCursos = listaCursos;
            this.listaCursosGrid = listaCursosGrid;
            this.viewModelModulo = viewModelModulo;
        }
    }
}