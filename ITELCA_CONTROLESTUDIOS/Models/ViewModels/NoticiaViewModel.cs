﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ITELCA_CLASSLIBRARY.Repositories;
using System.Web.Mvc;
using ITELCA_CLASSLIBRARY.Models;

namespace ITELCA_CONTROLESTUDIOS.Models.ViewModels
{
    public class NoticiaViewModel
    {
        public NOTICIAS[] noticia { get; set; }
        public NOTICIAS noticia_detalle { get; set; }
        public NOTICIAS noticiaCMS { get; set; }
        public SelectList activo { get; set; }
        public NoticiaViewModel(NOTICIAS[] noticia, NOTICIAS noticia_detalle)
        {
            this.noticia = noticia;
            this.noticia_detalle = noticia_detalle;
            
        }
        public NoticiaViewModel(NOTICIAS noticia, SelectList activo)
        {
            this.noticiaCMS = noticia;
            this.activo = activo;
        }
        
    }
}
