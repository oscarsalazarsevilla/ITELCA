﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ITELCA_CLASSLIBRARY.Models;
using System.Web.Mvc;

namespace ITELCA_CONTROLESTUDIOS.Models.ViewModels
{
    public class TipoCursoViewModel
    {
        public TIPOSCURSOS tipoCurso { get; set; }
        public SelectList activo { get; set; }
        public TipoCursoViewModel(TIPOSCURSOS tipoCurso, SelectList activo)
        {
            this.tipoCurso = tipoCurso;
            this.activo = activo;
        }
    }
}