﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ITELCA_CLASSLIBRARY.Models;
using System.Web.Mvc;

namespace ITELCA_CONTROLESTUDIOS.Models.ViewModels
{
    public class TipoFaqViewModel
    {
        public TIPOS_FAQS TIPOS_FAQS { get;set; }
        public SelectList activo { get; set; }
        public TipoFaqViewModel(TIPOS_FAQS TIPOS_FAQS, SelectList activo)
        {
            this.TIPOS_FAQS = TIPOS_FAQS;
            this.activo = activo;
        }
    }
}