﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ITELCA_CLASSLIBRARY.Models;
using System.Web.Mvc;

namespace ITELCA_CONTROLESTUDIOS.Models.ViewModels
{
    public class ReciboCajaViewModel
    {
        public RECIBOS RECIBOS { get;set; }
        public SelectList TipoPago { get; set; }
        public SelectList TipoDocumento { get; set; }
        public SelectList conceptosCursos { get; set; }
        public SelectList conceptosTalleres { get; set; }
        public bool cajaAbierta { get; set; }
        public CAJAS caja { get; set; }
        public PAGOS PAGO { get; set; }
        public SelectList formaPago { get; set; }
        public SelectList banco { get; set; }
        public SelectList sedes { get; set; }
        public IEnumerable<RECIBOS> ListaRecibos { get; set; }
        public ReciboCajaViewModel( RECIBOS RECIBOS, SelectList TipoPago, SelectList TipoDocumento, 
                                    SelectList conceptosCursos, SelectList conceptosTalleres, 
                                    bool cajaAbierta, CAJAS caja,SelectList formaPago,SelectList banco,SelectList sedes)
        {
            this.RECIBOS = RECIBOS;
            this.TipoPago = TipoPago;
            this.TipoDocumento = TipoDocumento;
            this.conceptosCursos = conceptosCursos;
            this.conceptosTalleres = conceptosTalleres;
            this.cajaAbierta = cajaAbierta;
            this.caja = caja;
            this.formaPago = formaPago;
            this.banco = banco;
            this.sedes = sedes;
        }

        public ReciboCajaViewModel(IEnumerable<RECIBOS> ListaRecibos)
        {
            this.ListaRecibos = ListaRecibos;
        }
    }
}