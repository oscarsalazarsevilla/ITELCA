﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ITELCA_CLASSLIBRARY.Models;
using System.Web.Mvc;

namespace ITELCA_CONTROLESTUDIOS.Models.ViewModels
{
    public class ManejoCajaViewModel
    {
        public MANEJOCAJAS manejoCaja { get;set; }
        public SelectList _sedes { get; set; }
        public SelectList _cajas { get; set; }
        public SelectList _condicion { get; set; }
        
        public ManejoCajaViewModel(MANEJOCAJAS manejoCaja, SelectList cajas,SelectList sedes, SelectList condicion)
        {
            this.manejoCaja = manejoCaja;
            this._cajas = cajas;
            this._sedes = sedes;
            this._condicion = condicion;
        }
    }
}