﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ITELCA_CLASSLIBRARY.Models;
using System.Web.Mvc;

namespace ITELCA_CONTROLESTUDIOS.Models.ViewModels
{
    public class CursoViewModel
    {
        public CURSOS curso { get;set; }
        public SelectList activo { get; set; }
        public SelectList duracion { get; set; }
        public SelectList tipoCurso { get; set; }
        public MultiSelectList listaTalleres{get;set;}

        public CursoViewModel(CURSOS curso, SelectList activo, SelectList tipoCurso, MultiSelectList listaTalleres, SelectList duracion)
        {
            this.curso =  curso;
            this.duracion = duracion;
            this.activo = activo;
            this.tipoCurso = tipoCurso;
            this.listaTalleres = listaTalleres;
        }
    }
}