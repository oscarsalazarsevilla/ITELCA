﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ITELCA_CONTROLESTUDIOS.Models.ViewModels
{
    public class ViewModelForViewWrapper<TMasterModel, TViewModel> : ViewModelForMasterWrapper<TMasterModel>
    {
        public ViewModelForViewWrapper(TMasterModel masterModel, TViewModel viewModel)
            : base(masterModel)
        {
            View = viewModel;
        }

        /// <summary>
        /// Gets or sets the view model.
        /// </summary>
        /// <value>The view model.</value>
        public TViewModel View { get; set; }
    }
}