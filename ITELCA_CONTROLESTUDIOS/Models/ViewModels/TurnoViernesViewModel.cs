﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ITELCA_CLASSLIBRARY.Models;
using System.Web.Mvc;
using ITELCA_CLASSLIBRARY.CustomClasses;

namespace ITELCA_CONTROLESTUDIOS.Models.ViewModels
{
    public class TurnoViernesViewModel
    {
        public TURNOSVIERNES TURNOSVIERNES { get; set; }
        public SelectList activo { get; set; }
        public SelectList listaAnios { get; set; }
        public SelectList listaTurnos { get; set; }

        public TurnoViernesViewModel(TURNOSVIERNES TURNOSVIERNES, SelectList activo, SelectList listaAnios, SelectList listaTurnos)
        {
            this.TURNOSVIERNES = TURNOSVIERNES;
            this.activo = activo;
            this.listaAnios = listaAnios;
            this.listaTurnos = listaTurnos;
        }
    }
}