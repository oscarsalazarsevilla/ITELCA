﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ITELCA_CLASSLIBRARY.Models;
using System.Web.Mvc;

namespace ITELCA_CONTROLESTUDIOS.Models.ViewModels
{
    public class SedeViewModel
    {
        public SEDES sede { get;set; }
        public SelectList activo { get; set; }
        public SedeViewModel(SEDES sede, SelectList activo)
        {
            this.sede = sede;
            this.activo = activo;
        }
    }
}