﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ITELCA_CLASSLIBRARY.Models;
using System.Web.Mvc;

namespace ITELCA_CONTROLESTUDIOS.Models.ViewModels
{
    public class ConceptoTipoSolicitudViewModel
    {
        public CONCEPTOS_TIPOSSOLICITUDES CONCEPTOS_TIPOSSOLICITUDES { get;set; }
        public SelectList activo { get; set; }
        public SelectList ListaTiposSolicitudes { get; set; }
        public SelectList ListaConceptos { get; set; }
        public MultiSelectList tiposSolicitudes { get; set; }

        public ConceptoTipoSolicitudViewModel(CONCEPTOS_TIPOSSOLICITUDES CONCEPTOS_TIPOSSOLICITUDES, SelectList activo, SelectList ListaTiposSolicitudes,
                                        SelectList ListaConceptos, MultiSelectList tiposSolicitudes)
        {
            this.CONCEPTOS_TIPOSSOLICITUDES = CONCEPTOS_TIPOSSOLICITUDES;
            this.activo = activo;
            this.ListaConceptos = ListaConceptos;
            this.ListaTiposSolicitudes = ListaTiposSolicitudes;
            this.tiposSolicitudes = tiposSolicitudes;
        }
    }
}