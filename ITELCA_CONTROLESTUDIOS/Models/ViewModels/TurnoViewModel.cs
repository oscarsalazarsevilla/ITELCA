﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ITELCA_CLASSLIBRARY.Models;
using System.Web.Mvc;
using ITELCA_CLASSLIBRARY.CustomClasses;

namespace ITELCA_CONTROLESTUDIOS.Models.ViewModels
{
    public class TurnoViewModel
    {
        public TURNOS turno { get;set; }
        public SelectList activo { get; set; }
        public MultiSelectList dias { get; set; }
        public SelectList horaInicio { get; set; }
        public SelectList horaFin { get; set; }
        public SelectList bloque { get; set; }
        public SelectList tipoTurno { get; set; }
        public List<DIATURNO> diaTurno { get; set; }
        public TurnoViewModel(  TURNOS turno, SelectList activo, MultiSelectList dias, 
                                SelectList horaInicio, SelectList horaFin, 
                                List<DIATURNO> diaTurno, SelectList bloque,SelectList tipoTurno)
        {
            this.turno =  turno;
            this.activo = activo;
            this.dias = dias;
            this.horaFin = horaFin;
            this.horaInicio = horaInicio;
            this.diaTurno = diaTurno;
            this.bloque = bloque;
            this.tipoTurno = tipoTurno;
        }
    }
}