﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ITELCA_CLASSLIBRARY.Models;

namespace ITELCA_CONTROLESTUDIOS.Models.ViewModels
{
    public class UsuarioViewModel
    {
        public USUARIOS USUARIO  { get; set; }
        public MultiSelectList listaRoles  { get; set; }
        public SelectList activo  { get; set; }
        public SelectList bloqueado  { get; set; }
        public SelectList genero { get; set; }

         public UsuarioViewModel(USUARIOS usuario, MultiSelectList roles,SelectList bloqueado,SelectList genero)
        {
            this.USUARIO = usuario;
            this.activo = activo;
            this.listaRoles = roles;
            this.bloqueado = bloqueado;
            this.genero = genero;
        }


    }
 }
