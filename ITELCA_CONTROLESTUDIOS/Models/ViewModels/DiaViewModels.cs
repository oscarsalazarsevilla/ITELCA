﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ITELCA_CLASSLIBRARY.Models;
using System.Web.Mvc;

namespace ITELCA_CONTROLESTUDIOS.Models.ViewModels
{
    public class DiaViewModel
    {
        public DIAS dia { get; set; }
        public SelectList activo { get; set; }

        public DiaViewModel(DIAS dia, SelectList activo)
        {
            this.dia = dia;
            this.activo = activo;
        }
    }
}