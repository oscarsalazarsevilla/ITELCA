﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ITELCA_CLASSLIBRARY.Models;
using System.Web.Mvc;

namespace ITELCA_CONTROLESTUDIOS.Models.ViewModels
{
    public class InterfazViewModel
    {
        public List<INVENTARIOS> inventario { get;set; }
        public List<CUENTAS> cuentas { get; set; }
        public List<BANCOS> bancos { get; set; }
        public string mensaje { get; set; }
        
        public InterfazViewModel(List<INVENTARIOS> inventario, List<CUENTAS> cuentas,List<BANCOS> bancos, string mensaje)
        {
            this.inventario = inventario;
            this.cuentas = cuentas;
            this.bancos = bancos;
            this.mensaje = mensaje;
        }
    }
}