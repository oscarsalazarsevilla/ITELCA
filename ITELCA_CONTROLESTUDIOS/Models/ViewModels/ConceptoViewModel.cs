﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ITELCA_CLASSLIBRARY.Models;
using System.Web.Mvc;

namespace ITELCA_CONTROLESTUDIOS.Models.ViewModels
{
    public class ConceptoViewModel
    {
        public CONCEPTOS CONCEPTO { get;set; }
        public SelectList activo { get; set; }
        public SelectList tipoPago { get; set; }
        
        public ConceptoViewModel(CONCEPTOS concepto, SelectList activo, SelectList tipoPago)
        {
            this.CONCEPTO = concepto;
            this.activo = activo;
            this.tipoPago = tipoPago;
        }
    }
}