﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ITELCA_CLASSLIBRARY.Models;
using System.Web.Mvc;

namespace ITELCA_CONTROLESTUDIOS.Models.ViewModels
{
    public class TipoTallerViewModel
    {
        public TIPOSTALLERES tipoTaller { get;set; }
        public SelectList activo { get; set; }

        public TipoTallerViewModel(TIPOSTALLERES tipoTaller, SelectList activo)
        {
            this.tipoTaller = tipoTaller;
            this.activo = activo;
        }
    }
}