﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ITELCA_CLASSLIBRARY.Models;
using System.Web.Mvc;

namespace ITELCA_CONTROLESTUDIOS.Models.ViewModels
{
    public class AulaViewModel
    {
        public AULAS aula { get;set; }
        public SelectList sedes { get; set; }
        public SelectList tipoAula { get; set; }
        public SelectList activo { get; set; }
        
        public AulaViewModel(AULAS aula, SelectList activo,SelectList sedes,SelectList tipoAula)
        {
            this.aula = aula;
            this.activo = activo;
            this.sedes = sedes;
            this.tipoAula = tipoAula;
        }
    }
}