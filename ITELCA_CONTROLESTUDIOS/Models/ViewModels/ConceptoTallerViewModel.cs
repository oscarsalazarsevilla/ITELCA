﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ITELCA_CLASSLIBRARY.Models;
using System.Web.Mvc;

namespace ITELCA_CONTROLESTUDIOS.Models.ViewModels
{
    public class ConceptoTallerViewModel
    {
        public CONCEPTOS_TALLERES CONCEPTOS_TALLERES { get;set; }
        public SelectList activo { get; set; }
        public SelectList ListaTalleres { get; set; }
        public SelectList ListaConceptos { get; set; }
        public MultiSelectList talleres { get; set; }

        public ConceptoTallerViewModel( CONCEPTOS_TALLERES CONCEPTOS_TALLERES, SelectList activo, SelectList ListaTalleres,
                                        SelectList ListaConceptos, MultiSelectList talleres)
        {
            this.CONCEPTOS_TALLERES = CONCEPTOS_TALLERES;
            this.activo = activo;
            this.ListaConceptos = ListaConceptos;
            this.ListaTalleres = ListaTalleres;
            this.talleres = talleres;
        }
    }
}