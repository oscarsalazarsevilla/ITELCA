﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ITELCA_CLASSLIBRARY.Models;
using System.Web.Mvc;

namespace ITELCA_CONTROLESTUDIOS.Models.ViewModels
{
    public class ModuloTurnoViewModel
    {
        public MODULOS_TURNOS modulo_turno { get;set; }
        public SelectList activo { get; set; }
        public SelectList listaCurso { get; set; }
        public MultiSelectList modulos { get; set; }
        public MultiSelectList turnos { get; set; }
        
        public ModuloTurnoViewModel(    MODULOS_TURNOS modulo_turno, 
                                        SelectList activo, 
                                        MultiSelectList modulos,
                                        MultiSelectList turnos, SelectList listaCurso)
        {
            this.modulo_turno = modulo_turno;
            this.activo = activo;
            this.modulos = modulos;
            this.turnos = turnos;
            this.listaCurso = listaCurso;
        }
    }
}