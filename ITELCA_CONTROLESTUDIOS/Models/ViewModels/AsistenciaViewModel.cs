﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ITELCA_CLASSLIBRARY.Models;
using System.Web.Mvc;
using ITELCA_CLASSLIBRARY.CustomClasses;

namespace ITELCA_CONTROLESTUDIOS.Models.ViewModels
{
    public class AsistenciaViewModel {


        public List<DateTime?> lista { get; set; }
        public SelectList curso{ get; set; }
        public SelectList modulo { get; set; }
        public SelectList profesor { get; set; }
        public SelectList sede { get; set; }
        public USUARIOS USUARIO {get;set;}
        public AsistenciaViewModel( SelectList curso, SelectList modulo, USUARIOS usuario,SelectList profesor,SelectList sede)
        {

            this.curso = curso;
            this.modulo = modulo;
            this.USUARIO = usuario;
            this.profesor = profesor;
            this.sede = sede;
        }
    }
}