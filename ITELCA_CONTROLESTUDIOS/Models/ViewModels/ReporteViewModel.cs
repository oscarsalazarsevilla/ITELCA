﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ITELCA_CLASSLIBRARY.Models;
using System.Web.Mvc;

namespace ITELCA_CONTROLESTUDIOS.Models.ViewModels
{
    public class ReporteViewModel
    {
        public string totalReciboBlanco { get;set; }
        public string totalReciboAzul { get; set; }
        public List<PAGOS> detalleReciboBlanco {get;set;}
        public List<PAGOS> detalleReciboAzul{get;set;}
        public IEnumerable<MATRICULASCURSOS> totalInscritos { get; set; }
        public ReporteViewModel(string totalReciboBlanco, string totalReciboAzul, List<PAGOS> detalleReciboBlanco, List<PAGOS> detalleReciboAzul,IEnumerable<MATRICULASCURSOS> totalInscritos)
        {
            this.totalReciboAzul = totalReciboAzul;
            this.totalReciboBlanco = totalReciboBlanco;
            this.detalleReciboAzul = detalleReciboAzul;
            this.detalleReciboBlanco = detalleReciboBlanco;
            this.totalInscritos=totalInscritos;
        }
    }
}