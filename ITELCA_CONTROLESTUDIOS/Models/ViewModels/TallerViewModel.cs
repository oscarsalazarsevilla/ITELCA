﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ITELCA_CLASSLIBRARY.Models;
using System.Web.Mvc;

namespace ITELCA_CONTROLESTUDIOS.Models.ViewModels
{
    public class TallerViewModel
    {
        public TALLERES taller { get; set; }
        public SelectList activo { get; set; }
        public SelectList tipoTaller { get; set; }
        public TallerViewModel(TALLERES taller, SelectList activo, SelectList tipoTaller)
        {
            this.taller = taller;
            this.activo = activo;
            this.tipoTaller = tipoTaller;
            
        }
    }
}