﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ITELCA_CLASSLIBRARY.Models;
using System.Web.Mvc;

namespace ITELCA_CONTROLESTUDIOS.Models.ViewModels
{
    public class TestimonioViewModel
    {
        public TESTIMONIOS testimonio { get; set; }
        public TESTIMONIOS[] testimonio_vector { get; set; }
        public SelectList activo { get; set; }

        public TestimonioViewModel(TESTIMONIOS testimonio, SelectList activo, TESTIMONIOS[] testimonio_vector)
        {
            this.testimonio = testimonio;
            this.activo = activo;
            this.testimonio_vector = testimonio_vector;
        }
    }
}