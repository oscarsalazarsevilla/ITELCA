﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ITELCA_CLASSLIBRARY.Models;
using System.Web.Mvc;

namespace ITELCA_CONTROLESTUDIOS.Models.ViewModels
{
    public class ModuloViewModel
    {
        public MODULOS modulo { get;set; }
        public SelectList activo { get; set; }
        public SelectList listaCursos { get; set; }
        public MultiSelectList curso { get; set; }
        public ModuloViewModel(MODULOS modulo, SelectList activo, MultiSelectList curso, SelectList listaCursos)
        {
            this.modulo = modulo;
            this.activo = activo;
            this.curso = curso;
            this.listaCursos = listaCursos;
        }
    }
}