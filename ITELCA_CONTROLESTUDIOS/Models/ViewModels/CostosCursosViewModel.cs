﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ITELCA_CLASSLIBRARY.Models;
using System.Web.Mvc;

namespace ITELCA_CONTROLESTUDIOS.Models.ViewModels
{
    public class CostosCursosViewModel
    {
        public COSTOS_CURSOS COSTOS_CURSOS { get;set; }
        public SelectList ListaCursos { get; set; }
        public SelectList ListaTurnos { get; set; }


        public CostosCursosViewModel(COSTOS_CURSOS COSTOS_CURSOS, SelectList ListaCursos, SelectList ListaTurnos)
        {
            this.COSTOS_CURSOS = COSTOS_CURSOS;
            this.ListaCursos = ListaCursos;
            this.ListaTurnos = ListaTurnos;
        }
    }
}