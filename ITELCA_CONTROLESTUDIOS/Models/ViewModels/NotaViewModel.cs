﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ITELCA_CLASSLIBRARY.Models;
using System.Web.Mvc;
using ITELCA_CLASSLIBRARY.CustomClasses;

namespace ITELCA_CONTROLESTUDIOS.Models.ViewModels
{
    public class NotaModuloViewModel {

        public List<DateTime?> lista { get; set; }
        public SelectList sede { get; set; }
        public SelectList profesor { get; set; }
        public SelectList curso{ get; set; }
        public SelectList turno { get; set; }
        public SelectList modulo { get; set; }
        public USUARIOS USUARIO {get;set;}
        public bool esAdmin { get; set; }

        public NotaModuloViewModel(SelectList sede, SelectList curso, SelectList modulo, SelectList turno, SelectList profesor, USUARIOS usuario, bool esAdmin)
        {
            this.curso = curso;
            this.sede = sede;
            this.turno = turno;
            this.profesor = profesor;
            this.modulo = modulo;
            this.USUARIO = usuario;
            this.esAdmin = esAdmin;
        }
    }
}