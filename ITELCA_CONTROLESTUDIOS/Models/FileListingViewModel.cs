﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ITELCA_CLASSLIBRARY .Models.CustomClasses;

namespace ITELCA_CONTROLESTUDIOS.Models
{
    public class FileListingViewModel
    {
        public List<FileInformation> Files { get; set; }
        public string CKEditorFuncNum { get; set; }
    }
}