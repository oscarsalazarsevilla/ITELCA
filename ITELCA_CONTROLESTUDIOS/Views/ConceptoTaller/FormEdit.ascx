﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ITELCA_CONTROLESTUDIOS.Models.ViewModels.ConceptoTallerViewModel>" %>
       <div class ="span-24 column last">
            <div class="span-8 column last">
                 <div class="editor-label">
					    <%: Html.LabelFor(model => model.CONCEPTOS_TALLERES.CONCEPTO_ID)%>
			    </div>
                <div class="editor-field">
				    <%: Html.DropDownListFor(model => model.CONCEPTOS_TALLERES.CONCEPTO_ID, Model.ListaConceptos, "--Seleccione--")%>
				    <%: Html.ValidationMessageFor(model => model.CONCEPTOS_TALLERES.CONCEPTO_ID)%>
			    </div> 
            </div>
        </div>
        <div class ="span-24 column last">
             <div class="span-8 column last">
                <div class="editor-label">
				    <%: Html.LabelFor(model => model.CONCEPTOS_TALLERES.TALLER_ID)%>
			    </div>
                <div class="editor-field">
				    <%: Html.DropDownListFor(model => model.CONCEPTOS_TALLERES.TALLER_ID, Model.ListaTalleres, "-- Seleccione --")%>
				    <%: Html.ValidationMessageFor(model => model.CONCEPTOS_TALLERES.TALLER_ID)%>
			    </div> 
            </div>    
        </div>
   
    <div class ="span-24 column last">
        <div class="span-8 column last">
             <div class="editor-label">
					<%: Html.LabelFor(model => model.CONCEPTOS_TALLERES.FECHA_EFECTIVA)%>
			</div>
            <div class="editor-field">
				<%: Html.TextBoxFor(model => model.CONCEPTOS_TALLERES.FECHA_EFECTIVA, new { @class = "datepicker" })%>
				<%: Html.ValidationMessageFor(model => model.CONCEPTOS_TALLERES.FECHA_EFECTIVA)%>
			</div> 
        </div> 
     </div>
     <div class ="span-24 column last">    
            <div class="span-8 column last">
                 <div class="editor-label">
					   <%: Html.Label("Monto")%>
			    </div>
                <div class="editor-field">
				    <%: Html.TextBoxFor(model => model.CONCEPTOS_TALLERES.MONTO, new { @class="montos" })%>
				    <%: Html.ValidationMessageFor(model => model.CONCEPTOS_TALLERES.MONTO)%>
			    </div> 
            </div>     
      </div>
        
      