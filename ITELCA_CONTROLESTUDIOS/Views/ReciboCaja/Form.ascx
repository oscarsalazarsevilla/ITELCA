﻿   <%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ITELCA_CONTROLESTUDIOS.Models.ViewModels.ReciboCajaViewModel>" %>
    
    <div class ="span-24 column last">
        <fieldset>
            <legend> Buscar por alumno </legend>
            <div class="span-4 column append-1">
                <div class="display-label">Caja</div>
                <div class="display-field">
				    <%: (Model.caja != null) ? Model.caja.NOMBRE : "-----"%>
			    </div> 
            </div>  
            <div class="span-4 column append-1">
                <div class="editor-label"><%: Html.LabelFor(model => model.RECIBOS.TIPODOCUMENTO)%></div>
                <div class="editor-field">
				    <%: Html.DropDownListFor(model => model.RECIBOS.TIPODOCUMENTO, Model.TipoDocumento)%>
			    </div> 
            </div>    
            <div class="span-4 column append-1">
                 <div class="editor-label"><%: Html.LabelFor(model => model.RECIBOS.DOCUMENTO)%></div>
                <div class="editor-field">
				    <%: Html.TextBoxFor(model => model.RECIBOS.DOCUMENTO)%>
			    </div> 
            </div>    
                <div class="span-3 column">
                    <p>
				        <button class="BuscarUsuario">Buscar</button>
			        </p>
                </div>
                <div class="span-3 column last">
                    <p>
				        <button class="NuevoRecibo">Limpiar</button>
			        </p>
                </div>
                <div class="span-3 append-1 column">
                <p>
				    <button  type="button" id="inscribir">Alumno Pre-Inscrito</button>
			    </p>
                </div>
 
            
        </fieldset>
    </div>
    <div class ="span-24 column last">
        <fieldset>
            <legend> Datos del pago </legend>
            <div class="span-24 column last">
                <div class="span-5 column">
                     <div class="editor-label"><%: Html.LabelFor(model => model.RECIBOS.NOMBRECLIENTE)%></div>
                    <div class="editor-field">
				        <%: Html.TextBoxFor(model => model.RECIBOS.NOMBRECLIENTE)%>
			        </div> 
                </div>
                <div class="span-5 column">
                     <div class="editor-label"><%: Html.LabelFor(model => model.RECIBOS.TELEFONO)%></div>
                    <div class="editor-field">
				        <%: Html.TextBoxFor(model => model.RECIBOS.TELEFONO)%>
			        </div> 
                </div>   
                <div class="span-8 column">
                     <div class="editor-label"><%: Html.LabelFor(model => model.RECIBOS.DIRECCIONFISCAL)%></div>
                    <div class="editor-field">
				        <%: Html.TextAreaFor(model => model.RECIBOS.DIRECCIONFISCAL)%>
			        </div> 
                </div>
                 <div class="span-6 column">
                     <div class="editor-label">Comentario</div>
                    <div class="editor-field">
				        <%:Html.TextAreaFor(model=>model.RECIBOS.COLETILLA,new {maxlength="200"})%>
			        </div> 
                </div> 
            </div>
            <div class ="span-24 column last">
                <div class="span-5 column">
                     <div class="editor-label"><%: Html.LabelFor(model => model.RECIBOS.CONDICIONPAGO)%></div>
                    <div class="editor-field">
				        <%: Html.DropDownListFor(model => model.RECIBOS.CONDICIONPAGO,Model.TipoPago)%>
			        </div> 
                </div> 
                <div class="span-5 column">
                     <div class="editor-label"><%: Html.LabelFor(model => model.RECIBOS.MONTOTOTAL)%></div>
                    <div class="editor-field">
                        <input type="text" class="montos" id="RECIBOS_MONTOTOTAL" name="RECIBOS.MONTOTOTAL" readonly/>
			        </div> 
                </div>    
                 <div class="span-5 column">
                     <div class="editor-label"><%: Html.LabelFor(model => model.RECIBOS.ABONO)%></div>
                    <div class="editor-field">
				        <%: Html.TextBoxFor(model => model.RECIBOS.ABONO, new { @class = "montos", disabled="disabled" })%>
			        </div> 
                </div> 
                  <div class="span-4 column">
                        <p>
				            <button  type="button" id="btnMovimiento">Consultar Pagos</button>
			            </p>
                </div> 
                  <div class="span-4 column">
                        <p>
				            <button  type="button" id="btnPenalidad"> Agregar Penalidad</button>
			            </p>
                </div>     
            </div>
        </fieldset>    
    </div>