﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<ITELCA_CONTROLESTUDIOS.Models.ViewModels.ReciboCajaViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    [Itelca - Caja]
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
<div id="confirmar" title="Emitir Recibo">
<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>Esta seguro de Imprimir Recibo?</p>
</div>
<div id="confirmarProducto" title="Emitir Recibo">
<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>Esta seguro de Imprimir Recibo?</p>
</div>
<div class="span-22 append-1 prepend-1 column last ui-widget-content ui-corner-all">
	<span class="introForm">Recibo de Caja > Crear</span>
		<% using (Html.BeginForm("Crear", "ReciboCaja", FormMethod.Post, new { id = "formReciboCaja" })){     %>
		<% Html.EnableClientValidation(); %>
        <%:Html.ValidationSummary("Error en la aplicación") %>
        <%:Html.HiddenFor(model=>model.RECIBOS.RECIBO_ID) %>
        <%:Html.Hidden("detallesRecibo")%>
        <%:Html.Hidden("formaPagoRecibo")%>
        <%:Html.Hidden("alumno_id")%>
        <%:Html.Hidden("TipoCT")%>
        <%:Html.Hidden("tipoReport")%>
        <%:Html.Hidden("correlativo")%>
        <%:Html.Hidden("cajaAbierta", (Model.cajaAbierta) ? "1" : "0")%>
    <div id="dialog-message">
        <div id="delete-message"></div>    
    </div>
    <div id="dialog-edocuenta">
    </div>
    <div class="separator column span-24"></div>
       <div class="column span-24" align="right">
        <%: Html.ActionLink("Inscripción Asistida", "Index","MatriculaManual")%>
    </div>  
    <div  class="span-24 Formulario  column last">
        <% Html.RenderPartial("Form",Model); %>
    </div>
    <div id="DivDetallesRecibo" class="span-24 column last">
        <div id="tabRecibo">
            <ul>
                <li><a href="#tabMensualidad">Mensualidad</a></li>
                <li><a href="#tabProducto">Producto</a></li>
            </ul>
            
            <div id="tabMensualidad">
                <div id="Div1" class="span-24 column last">
                    <div class="span-20 column">
                        <fieldset>
                            <legend>Detalle Mensualidad</legend>
                            <div class="span-24 column last">
                                <div class="span-6 column prepend-14">
                                     <div class="editor-label"> Nro Recibo </div>
                                    <div class="editor-field">
				                        <%: Html.TextBox("nroReciboM", "")%>
                                    </div> 
                                </div>
                            </div>
                            <div class="separator column span-24"></div>
                            <div class="span-24 column last">
                                <table  id="TablaDetallesMensualidad">
                                    <thead>
                                        <tr>
                                            <th>Fila</th>
                                            <th>Id</th>
                                            <th>Concepto</th>
                                            <th>Cantidad</th>
                                            <th>Monto</th>
                                            <th>Seleccionar</th>
                                            <th>Eliminar</th>
                                            <th>ValorChequeado</th>
                                            <th>Vencido?</th>
                                        </tr>    
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>            
                            <div class="separator column span-24"></div>
                            <p>
                                <input type="button" value="Agregar Pago" class="button" id="EnviarFormularioM" />
                                <input type="button" value="Editar" class="button" id="EditarFormularioM" />
                                <input type="button" value="Imprimir" class="button" id="ImprimirReciboCajaM" />
				            
			                </p>
                        </fieldset>
                    </div>
                </div>
            </div>
            <div id="tabProducto">
                <div id="Div2" class="span-24 column last">
                    <div class="span-20 column">
                        <fieldset>
                            <legend>Detalle Producto</legend>
                             <div class="column span-24">
                                <div class="span-10 column">
                                    <div class="editor-label"> Producto </div>
                                    <div class="editor-field">
				                        <%: Html.DropDownList("Productos", Model.conceptosCursos, "-- Seleccione --", new { style = "width:95%;font-size:18px" })%>
                                    </div> 
                                </div> 
                                <div class="span-3 append-1 column">
                                    <div class="editor-label"> Cantidad </div>
                                    <div class="editor-field">
				                        <%: Html.TextBox("CantidadConcepto", "", new { @class ="numeros" })%>
                                    </div> 
                                </div>
                                <div class="span-6 column">
                                    <div class="editor-label"> Disponibilidad </div>
                                    <div class="editor-field">
				                        <%: Html.TextBox("Disponibilidad", "", new { @class ="numeros",maxlength="4" })%>
                                    </div> 
                                </div>
                                <div class="span-4 column">
                                    <p>
				                        <input type="button" value="Agregar" class="AgregarDetalle" id="AgregarProducto" />
                                    </p>
                                    <p>
				                        <input type="button" value="Editar" class="EditarDetalle" id="EditarDetalle"/>
                                    </p>
                                </div>
                            </div>
                          </fieldset>
                          <fieldset>
                            <div class="span-24 column last">
                             <%--   <div class="span-10 column">
                                    <div class="editor-label"> Cursos Inscritos </div>
                                    <div class="editor-field">
				                        <%: Html.DropDownList("cursosActivos",new SelectList( new Dictionary<decimal, string>(){}), "-- Seleccione --", new { style = "width:95%;font-size:18px" })%>
                                    </div> 
                                </div> --%>
                                <div class="span-6 column prepend-14">
                                        <div class="editor-label"> Nro Recibo </div>
                                    <div class="editor-field">
				                        <%: Html.TextBox("nroReciboP", "")%>
                                    </div> 
                                </div>
                            </div>
                            <div class="separator column span-24"></div>
                            <div class="span-24 column last">
                                <table  id="TablaDetallesProducto">
                                    <thead>
                                        <tr>
                                            <th>Fila</th>
                                            <th>Id</th>
                                            <th>Concepto</th>
                                            <th>Cantidad</th>
                                            <th>Monto</th>
                                            <th>Seleccionar</th>
                                            <th>Eliminar</th>
                                            <th>ValorChequeado</th>
                                            <th>Vencido?</th>
                                        </tr>    
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>            
                            <div class="separator column span-24"></div>
                            <p>
				               <input type="button" value="Agregar Pago" class="button" id="EnviarFormularioP" />
			                </p>
                        </fieldset>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="dialogPago">
        <fieldset>
            <div class="span-24 column Formulario last">
                <div class="span-8 column append-1">
                    <div class="editor-label"><%: Html.Label("Forma de Pago")%></div>
                    <div class="editor-field">
				        <%: Html.DropDownListFor(model => model.PAGO.FORMAPAGO_ID, Model.formaPago,"--Seleccione--")%>
			        </div> 
                </div>
                <div class="span-5 column append-1">
                    <div class="editor-label"><%: Html.Label("Monto")%></div>
                    <div class="editor-field">
				        <%: Html.TextBoxFor(model => model.PAGO.MONTO, new { @class="montos"})%>
			        </div> 
                </div>
                <div class="span-4 column">
                    <p>
				        <button id="GuardarPago">Agregar</button>
			        </p>
                </div>
            </div>
            <div class="span-24 Formulario column last" style="display:none" id="adicional">
                 <div class="span-8 column append-1">
                    <div class="editor-label"><%: Html.Label("Banco")%></div>
                    <div class="editor-field">
				        <%: Html.DropDownListFor(model => model.PAGO.BANCO_ID, Model.banco,"--Seleccione--")%>
			        </div> 
                </div>
                 <div class="span-5 column append-1">
                    <div class="editor-label"><%: Html.Label("Número")%></div>
                    <div class="editor-field">
				        <%: Html.TextBoxFor(model => model.PAGO.NUMERO)%>
			        </div> 
                </div>

            </div>
            <div class="separator column span-24"></div>
            <div class="span-20 column last">
                 <table  id="TablaPagos">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Tipo Pago</th>
                            <th>Monto</th>
                            <th>idBanco</th>
                            <th>Banco</th>
                            <th>Numero</th>
                            <th>Eliminar</th>
                        </tr>    
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </fieldset>
    </div>
    <% } %>
    <div id="dialogBuscar">
        <div  class="span-24 column last Formulario">
            <div class="span-12 column append-1">
                    <div class="editor-label"><%: Html.Label("Buscar")%></div>
                    <div class="editor-field">
				        <%: Html.TextBox("buscarGeneral","", new { maxlength="20"})%>
			        </div> 
                </div>
        </div>
         <div class="separator column span-24"></div>
         <div class="span-20 column last">
                 <table  id="tablaBuscar">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Número</th>
                            <th>Nombre</th>
                            <th>Seleccionar</th>
                        </tr>    
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
    </div>
    <div id="dialogPenalidad">
        <div  class="span-24 column last Formulario">
        <div  class="span-24 column last">
             <div class="span-8 column">
                    <div class="editor-label">
                        <%: Html.Label("Tipo")%></div>
                    <div class="editor-field">
			            <%: Html.DropDownList("penTipo", new SelectList(new Dictionary<string, string>() { {"0","Taller"},{"1","Curso"}}, "Key", "Value"), "--Seleccione--")%>
			        </div>
                </div>
                <div class="span-8 column">
                    <div class="editor-label">
                        <%: Html.Label("Por Concepto de:")%></div>
                    <div class="editor-field">
			            <%: Html.DropDownList("penMatricula", new SelectList(new Dictionary<string, string>() {  }), "--Seleccione--")%>
			        </div>
                </div>
            </div>
             <div  class="span-24 column last">
                 <div class="span-12 column">
                    <div class="editor-label">
                        <%: Html.Label("Descripción")%></div>
                    <div class="editor-field">
			            <%: Html.TextArea("penDescripcion", "", new  {maxlength="500"})%>
			        </div>
                </div>
            </div>
           <div  class="span-24 column last">
                <div class="span-8 column">
                    <div class="editor-label">
                        <%: Html.Label("Monto")%></div>
                    <div class="editor-field">
			            <%: Html.TextBox("penMonto", "", new { @class = "montos", maxlength = "20" })%>
			        </div>
                </div>
            </div>  
        </div>
         <div class="separator column span-24"></div>
         <div class="span-24 column last">
            <div class="span-4 column append-1">
                <p>
				    <button  type="button" id="penGuardar" class="boton">Guardar</button>
                </p>
            </div>
        </div>
    </div>
        <div id="Div3">
        <div  class="span-24 column last Formulario">
            <div class="span-12 column append-1">
                    <div class="editor-label"><%: Html.Label("Buscar")%></div>
                   <%-- <div class="editor-field">
				        <%: Html.TextBox("buscarMovimiento","", new { maxlength="20"})%>
			        </div> --%>
                </div>
        </div>
         <div class="separator column span-24"></div>
         <div class="span-24 column last">
                 <table  id="Table1">
                    <thead>
                        <tr>
                            <th>Fecha</th>
                            <th>Sede</th>
                            <th>Tipo Recibo</th>
                            <th>Nro Recibo</th>
                            <th>Comentario</th>
                            <th>Monto</th>
                            <th>Cajero</th>
                            <th>Condición</th>
                        </tr>    
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
    </div>
     <div id="dialogInscribir">
          <%:Html.Hidden("inscIdEstudiante")%>
        <div class="span-24 column last Formulario">
                <div class="span-8 column">
                    <div class="editor-label">
                        <%: Html.Label("Buscar")%></div>
                    <div class="editor-field">
			            <%: Html.TextBox("inscBuscar")%>
			        </div>
                 </div>
        </div>
        <div class="span-24 column last Formulario">
                  <div class="span-18 column">
                    <div class="editor-label">
                        <%: Html.Label("Alumno")%></div>
                    <div class="editor-field">
			            <%: Html.TextBox("inscAlumno", "", new { readOnly="readOnly"})%>
			        </div>
                 </div> 
            </div>
        <div  class="span-24 column last Formulario">
             <div class="span-24 column last">
                <div class="span-8 column">
                    <div class="editor-label">
                        <%: Html.Label("Sede")%></div>
                    <div class="editor-field">
			            <%: Html.DropDownList("inscSede", Model.sedes, "--Seleccione--")%>
			        </div>
                 </div>
                  <div class="span-8 column">
                    <div class="editor-label">
                        <%: Html.Label("Curso")%></div>
                    <div class="editor-field">
			            <%: Html.DropDownList("inscCurso", new SelectList(new Dictionary<string, string>() { }),"--Seleccione--")%>
			        </div>
                 </div> 
            </div>
            <div class="span-24 column last">
                 <div class="span-8 column">
                    <div class="editor-label">
                        <%: Html.Label("Turno")%></div>
                    <div class="editor-field">
			            <%: Html.DropDownList("inscTurno", new SelectList(new Dictionary<string, string>() { }), "--Seleccione--")%>
			        </div>
                 </div> 
                <div class="span-8 column">
                        <div class="editor-label">
                            <%: Html.Label("Modulo")%></div>
                        <div class="editor-field">
			                <%: Html.DropDownList("inscModulo", new SelectList(new Dictionary<string, string>() { }), "--Seleccione--")%>
			            </div>
                     </div>
             </div>
              <div class="span-24 column last"> 
                  <div class="span-4 column append-1">
                        <div class="editor-label"><%: Html.Label("Inicio Curso")%></div>
                        <div class="editor-field">
				            <%: Html.TextBox("inicio", "", new { maxlength = "10", @class = "datepicker" })%>
			            </div> 
                    </div>
                <div class="span-5 column append-1 ">
                        <div class="editor-label"><%: Html.Label("Cuotas Pagadas")%></div>
                        <div class="editor-field">
				            <%: Html.TextBox("cuota","", new { maxlength="2"})%>
			            </div> 
                    </div>
                 <div class="span-3 column append-1">
                        <div class="editor-label"><%: Html.Label("Monto")%></div>
                        <div class="editor-field">
				            <%: Html.TextBox("incsMonto", "", new { maxlength = "5", @class = "montos" })%>
			            </div> 
                    </div>
                     <div class="span-4 column append-1">
                        <div class="editor-label"><%: Html.Label("Nro Control")%></div>
                        <div class="editor-field">
				            <%: Html.TextBox("nroControl", "")%>
			            </div> 
                    </div>
                     <div class="span-4 column append-1">
                        <p>
				            <button  type="button" id="inscGuardar">Guardar</button>
                        </p>
                    </div>
            </div>
        </div>
    </div>
    <div class="column span-24">
        <%: Html.ActionLink("Volver al listado", "Listar")%>
    </div>             
</div>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="CssContent" runat="server">
    <link href="/Content/Css/itelca/demo_table_jui.css" rel="stylesheet" type="text/css" />
    <style  type="text/css">
        .numero
        {
            font-size:18px;
                
        }
         .descripcion
        {
            font-weight: bold;
                
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="JsContent" runat="server">
    <script src="/Scripts/jquery.maskedinput-1.2.2.js" type="text/javascript"></script>
    <script src="/Scripts/jquery.dataTables.js" type="text/javascript"></script>
    <script src="/Scripts/autoNumeric.js" type="text/javascript"></script>
    <script src="/Scripts/Views/ReciboCaja.js" type="text/javascript"></script>
</asp:Content>

