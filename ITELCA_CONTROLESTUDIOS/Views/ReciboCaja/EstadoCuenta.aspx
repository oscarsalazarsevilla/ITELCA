﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/SiteEdit.Master" Inherits="System.Web.Mvc.ViewPage<List<ITELCA_CLASSLIBRARY.Models.CONCEPTOS_ESTUDIANTES>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    EstadoCuenta
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2>EstadoCuenta</h2>

<table id='tableResults' cellpadding='0' cellspacing='0' border='0' class='display'>
    <thead>
        <tr>
            <th>Concepto curso</th>
            <th>Concepto taller</th>
            <th>Concepto solicitudes</th>
            <th>Descripción</th>
            <th>Monto</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <% foreach (var estado in Model)
           {
               %>
        <tr>
            <td><%: (estado.CONCEPTO_CURSO_ID.HasValue) ? estado.CONCEPTOS_CURSOS.CURSOS.NOMBRE + " - " + estado.CONCEPTOS_CURSOS.CONCEPTOS.NOMBRE : ""   %></td>
            <td><%: (estado.CONCEPTO_TALLER_ID.HasValue) ? estado.CONCEPTOS_TALLERES.TALLERES.NOMBRE + " - " + estado.CONCEPTOS_TALLERES.CONCEPTOS.NOMBRE : ""%></td>
            <td><%: (estado.CONCEPTO_SOLICITUD_ID.HasValue) ? estado.CONCEPTOS_TIPOSSOLICITUDES.TIPOSSOLICITUDES.NOMBRE + " - " + estado.CONCEPTOS_TIPOSSOLICITUDES.CONCEPTOS.NOMBRE : ""   %></td>
            <td><%: estado.DESCRIPCION %></td>
            <td><%: estado.MONTO  %></td>
            <td>Agregar</td>
        </tr>
               <%
           } %>
    </tbody>
</table>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="CssContent" runat="server">
    <link href="/Content/Css/itelca/demo_table_jui.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="JSContent" runat="server">
    <script src="/Scripts/jquery.dataTables.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#tableResults').dataTable({
                "bJQueryUI": true,
                "iDisplayLength": 5,
            });
        });
    </script>
</asp:Content>
