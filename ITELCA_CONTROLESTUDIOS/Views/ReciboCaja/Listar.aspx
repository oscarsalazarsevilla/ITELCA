﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<ITELCA_CONTROLESTUDIOS.Models.ViewModels.ReciboCajaViewModel>"%>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Listar
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<div class="span-22 append-1 prepend-1 column last ui-widget-content ui-corner-all">
	<span class="introForm">Recibo de Caja > Listar</span>
        
		<% using (Html.BeginForm("Listar", "ReciboCaja", FormMethod.Post, new { id = "formReciboCaja" }))
         {     %>
            <div class="span-24 column Formulario last">
                    <div class="span-8 column">
                         <div class="editor-label">
					            <%: Html.Label("Fecha")%>
			            </div>
                        <div class="editor-field">
				            <%: Html.TextBox("fecha","",new{ @class = "datepicker" })%>
			            </div> 
                    </div> 
                    <div class="span-8 column">
                        <p>
				        <input type="submit" value="Buscar" class="button" />
			            </p>
                    </div>
                    <div class="span-8 column">
                       <%if (TempData["msg"] != null)
                         { %> 
				            <h4>Reporte al:<%:DateTime.Parse(TempData["msg"].ToString()).ToShortDateString()%></h4>
                        <%}
                         else
                         { %>
                            <h4>Reporte al:<%:DateTime.Now.ToShortDateString() %></h4>
                        <%} %>
			           
                    </div>
            </div>
            <div class="span-24 column last">
                 <table  id="tablaMovimiento">
                    <thead>
                        <tr>
                            <th>Fecha</th>
                            <th>Sede</th>
                            <th>Concepto</th>
                            <th>Cedula</th>
                            <th>Alumno</th>
                            <th>Nro Recibo</th>
                            <th>Monto</th>
                            <th>Cajero</th>
                            <th>Condición</th>
                        </tr>    
                    </thead>
                    <tbody>
                    <%if (Model.ListaRecibos != null)
                      {foreach(var ope in Model.ListaRecibos){
                          %>
                        <tr>
                            <td><%:ope.FECHADOCUMENTO.ToShortDateString() %></td>
                            <td><%:ope.CAJAS.SEDES.NOMBRE %></td>
                            <%if (ope.DETALLESRECIBOS.Count()> 0)
                              { %>
                            <td><%:ope.DETALLESRECIBOS.FirstOrDefault().INV_ITEM_ID != null ? ope.DETALLESRECIBOS.FirstOrDefault().PRODUCTOS.DESCR : ope.DETALLESRECIBOS.FirstOrDefault().CONCEPTOS_ESTUDIANTES.DESCRIPCION%></td>
                            <%}
                              else
                              { %>
                              <td>Sin Descripcion</td>
                            <%} %>
                             <td><%:ope.DOCUMENTO %></td>
                            <td><%:ope.NOMBRECLIENTE %></td>
                            <td><%:ope.NRORECIBO %></td>
                            <td><%
                                  Response.Write(ope.MONTOTOTAL.ToString().Replace('.', ',')); %></td>
                            <td><%:ope.USUARIOS.NOMBREUSUARIO %></td>
                            <td><%:ope.CONDICION=="E"?"EMITIDO":"ANULADO" %></td>
                        </tr>
                    <%}} %>
                    </tbody>
                    <tfoot>
						<tr>
							<th colspan="6" style="text-align:right;font-size:16px">Total Pagina:</th>
							<th style="font-size:16px"></th>
                            <th style="text-align:right;font-size:16px">Total General</th>
                            <th style="font-size:16px"></th>
						</tr>
					</tfoot>
                </table>
        <%} %>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="CssContent" runat="server">
 <link href="/Content/Css/itelca/demo_table_jui.css" rel="stylesheet" type="text/css" />
     <link href="/Content/Css/itelca/dataTables.tableTools.min.css" rel="stylesheet" />
     <style media="all" type="text/css">
        .center { text-align: center; }
        .right { text-align: right; }
        .left { text-align: left; }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="JSContent" runat="server">
   <script src="/Scripts/jquery.maskedinput-1.2.2.js" type="text/javascript"></script>
    <script src="/Scripts/jquery.dataTables.js" type="text/javascript"></script>
    <script src="/Scripts/dataTables.tableTools.min.js" type="text/javascript"></script>
    <script src="../../Scripts/ZeroClipboard.js" type="text/javascript"></script>
    <script src="/Scripts/autoNumeric.js" type="text/javascript"></script>
<script type="text/javascript">
    function ObtenerFecha() {
        var f = new Date();

        return f.getDate() + "-" + (f.getMonth() + 1) + "-" + f.getFullYear();
    }
    function toClipboard(me, strMsg) {
        var clip = new ZeroClipboard.Client();
        clip.setHandCursor(true);
        clip.setText(strMsg);
        clip.addEventListener('complete', function (client, text) {
            alert("Copy Ok: " + text);
        });
        clip.glue(me);
    }
    $(document).ready(function () {

        $('.datepicker').datepicker({
            dateFormat: 'dd/mm/yy',
            yearRange: "0:+10",
            numberOfMonths: 1
        });
        $('.datepicker').mask("99/99/9999");
        tablaMovimiento = $("#tablaMovimiento").dataTable({
            "sDom": 'T<"clear">lfrtip',
            "bPaginate": true,
            "bFilter": true,
            "bInfo": true,
            "bJQueryUI": true,
            "bAutoWidth": false,
            "aaSorting": [[0, "desc"]],
            "aoColumns": [
        { sWidth: '220px' },
        { sWidth: '200px' },
        { sWidth: '300px' },
        { sWidth: '100px' },
        { sWidth: '300px' },
        { sWidth: '200px' },
        { sWidth: '100px', "sClass": "center" },
        { sWidth: '100px' },
        { sWidth: '100px' }
    ],
            "oLanguage": {
                "sProcessing": "Procesando...",
                "sLengthMenu": "Mostrar _MENU_ registros",
                "sZeroRecords": "No se encontraron resultados",
                "sEmptyTable": "Ningún dato disponible en esta tabla",
                "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix": "",
                "sSearch": "Buscar:",
                "sUrl": "",
                "sInfoThousands": ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst": "Primero",
                    "sLast": "Último",
                    "sNext": "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            },
            "oTableTools": {
                "sSwfPath": "/Scripts/swf/copy_csv_xls_pdf.swf",
                "aButtons": [
                        {
                            'sExtends': 'xls',
                            "sFileName": "CAJAITELCA" + ObtenerFecha() + ".xls",
                            "sButtonText": "Excel"
                        },
                        {
                            'sExtends': 'pdf',
                            "sFileName": "CAJAITELCA" + ObtenerFecha() + ".pdf",
                            "sButtonText": "Pdf"
                        }

                    ]
            },
            "fnFooterCallback": function (nRow, aaData, iStart, iEnd, aiDisplay) {
                /*
                * Calculate the total market share for all browsers in this table (ie inc. outside
                * the pagination)
                */
                var iTotalMarket = 0;
                var aux = "";
                for (var i = 0; i < aaData.length; i++) {
                    if (aaData[i][8] == "EMITIDO") {
                        aux = aaData[i][6].replace(",", ".");
                        iTotalMarket += aux * 1;
                    }
                }

                /* Calculate the market share for browsers on this page */
                var iPageMarket = 0;
                aux = "";
                for (var i = iStart; i < iEnd; i++) {
                    if (aaData[aiDisplay[i]][8] == "EMITIDO") {
                        aux = aaData[aiDisplay[i]][6].replace(",", ".");
                        iPageMarket += aux * 1;
                    }
                }
                /* Modify the footer row to match what we want */
                var nCells = nRow.getElementsByTagName('th');
                nCells[1].innerHTML = parseFloat(iPageMarket).toFixed(2);
                nCells[3].innerHTML = parseFloat(iTotalMarket).toFixed(2);
            }
        });


    });
</script>
</asp:Content>
