﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Anular
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2>Anular</h2>
<div id="dialog-message">
    <div id="delete-message"></div>    
</div>
<%
            try
            {

                if (TempData["mensaje"] != null && TempData["mensaje"]  != "")
                {%>
			    <div  class="ui-state-highlight ui-corner-all" style="margin-top: 10px; padding: 0 .3em; width:30%; height:20px;">
				    <span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
				    <div><%:TempData["mensaje"]  %></div>
			    </div>
		    
          <%
                }
            }
            catch{} 
            
           %>
<div class="span-22 append-1 prepend-1 column last ui-widget-content ui-corner-all">
	<span class="introForm">Recibo >Anular </span>
		<% using (Html.BeginForm()) { %>
		<% Html.EnableClientValidation(); %>
        <%:Html.ValidationSummary("Error en la aplicación") %>
        <%:Html.Hidden("idrecibo") %>
        <div class="span-24 column last Formulario">
            <div class ="span-24 last">
                <div class="span-8 column last">
                        <div class="editor-label">
					        <%: Html.Label("Facturo")%>
			        </div>
                    <div class="editor-field">
				        <select id="tipo" name="tipo">
                            <option value="0">Recibo Blanco</option>

                        </select>
				        <%: Html.ValidationMessage("tipo")%>
			        </div> 
                </div>
                <div class="span-8 column last">
                        <div class="editor-label">
					        <%: Html.Label("Nro Recibo")%>
			        </div>
                    <div class="editor-field">
				        <%: Html.TextBox("nroRecibo")%>
				        <%: Html.ValidationMessage("nroRecibo")%>
			        </div> 
                </div>
                <div class="span-8 column last">
                    <p>
				        <input type="button" value="Buscar" class="button" id="buscarR" />
			        </p>
                </div>
            </div>
            <div class="span-20   column">
                <div class ="span-24 last">
                    <div class="span-10 column last">
                            <div class="editor-label">
					            <%: Html.Label("Fecha Emision")%>
			            </div>
                        <div class="editor-field">
				                <%: Html.TextBox("fechaRecibo", "", new { @class = "Lectura" })%>
			            </div> 
                    </div>
                        <div class="span-10 column last">
                            <div class="editor-label">
					            <%: Html.Label("Cedula")%>
			            </div>
                        <div class="editor-field">
				                <%: Html.TextBox("cedula", "", new { @class = "Lectura" })%>
			            </div> 
                    </div>
                </div>
                <div class ="span-24 last">
                    <div class="span-10 column last">
                            <div class="editor-label">
					            <%: Html.Label("Nombre")%>
			            </div>
                        <div class="editor-field">
				                <%: Html.TextBox("nombre", "", new { @class = "Lectura" })%>
			            </div> 
                    </div>
                        <div class="span-10 column last">
                            <div class="editor-label">
					            <%: Html.Label("Monto")%>
			            </div>
                        <div class="editor-field">
				                <%: Html.TextBox("monto","",new { @class = "Lectura" })%>
			            </div> 
                    </div>
                </div>
                <div class ="span-24 last">
                    <div class="span-20 column ">
                            <div class="editor-label">
					            <%: Html.Label("Observaciones")%>
			            </div>
                        <div class="editor-field">
				                <%: Html.TextArea("observaciones")%>
			            </div> 
                    </div>
                </div>
                <div class="separator column span-24"></div>
                <p>
				    <input type="button" value="Anular" class="button" id="anular" />
			    </p>
			</div>
            <div class="span-4 column last">
                    <h4>Estado</h4>
                        <img src="/Content/Imagenes/noEncontrado.png"  alt="Estado" height="100px" width="100px" id="estado"/>
                    <span></span>
            </div>
        </div>
        <div class="column span-24">
            <%: Html.ActionLink("Volver Recibo Caja", "Index")%>
        </div>
          
			
    <% } %>
     
                
</div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="CssContent" runat="server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="JSContent" runat="server">
<script type="text/javascript">
    function mensaje(texto) {
        $("#delete-message").text(texto);
        $("#dialog-message").dialog({
            modal: true,
            buttons: {
                Ok: function () {
                    $(this).dialog("close");
                }
            }
        });
    }
    function buscar(tipo, nro) {
        $.post("/ReciboCaja/BuscarRecibo", { tipo: tipo, nroRecibo: nro }, function (data) {
            var tam = data.length;

            if (tam>0 && data[0].ok == 1) {
                $("#fechaRecibo").val(data[0].fecha);
                $("#cedula").val(data[0].nroDocumento);
                $("#nombre").val(data[0].nombre);
                $("#monto").val(data[0].montoTotal);
                $("#observaciones").val("");
                $("#idrecibo").val(data[0].recibo_id);
                if (data[0].estado == "ANULADO") {
                    $("#estado").attr("alt", "ANULADO");
                    $("#estado").attr("title", "ANULADO");
                    $("#estado").attr("src", "/Content/Imagenes/anulado.png");
                    $("#observaciones").val(data[0].observacion);
                    $("#anular").button("option", "disabled", true);
                }
                else {
                    $("#estado").attr("alt", "EMITIDO");
                    $("#estado").attr("title", "EMITIDO");
                    $("#estado").attr("src", "/Content/Imagenes/ok.jpg");
                    $("#anular").button("option", "disabled", false);
                }
                if (data[0].msg != "") {
                    mensaje(data[0].msg);
                    $("#anular").button("option", "disabled", true);
                }

            } else {
                $("#estado").attr("alt", "NO ENCONTRADO");
                $("#estado").attr("title", "NO ENCONTRADO");
                $("#fechaRecibo").val("");
                $("#cedula").val("");
                $("#nombre").val("");
                $("#monto").val("");
                $("#observaciones").val("");
                $("#idrecibo").val("");
                $("#estado").attr("src", "/Content/Imagenes/noEncontrado.png");
                mensaje(data[0].msg);
            }

        }, "json");
    
    }
    $(document).ready(function () {
        $(".button").button();
        $("#nroRecibo").keypress(function (event) {
            if (event.keyCode == 13) {
                event.preventDefault();
                var nro = $("#nroRecibo").val();
                var tipo = $("#tipo option:selected").val();
                if (nro != "" && tipo != "") {
                    buscar(tipo, nro);
                } else {
                    mensaje("Ingrese Número de Recibo y Tipo");
                }
            }
        });

        $("#buscarR").click(function () {
            var nro = $("#nroRecibo").val();
            var tipo = $("#tipo option:selected").val();
            if (nro != "" && tipo != "") {
                buscar(tipo, nro);
            } else {
                mensaje("Ingrese Número de Recibo y Tipo");
            }
        });

        $(".Lectura").keypress(function (event) {
            if (event.keyCode != 9) {
                event.preventDefault();
            }
        });

        $("#anular").click(function (event) {
            if ($("#idrecibo").val() != "") {
                if ($("#observaciones").val() != "") {
                    $("#delete-message").text("Desea Anular Recibo?");
                    $("#dialog-message").dialog({
                        modal: true,
                        buttons: {
                            Si: function () {
                                $("form").submit();
                                $(this).dialog("close");
                            },
                            No: function () {
                                $(this).dialog("close");
                            }
                        }
                    });
                } else {
                    mensaje("Ingrese Observación");
                }
            } else {
                mensaje("Ingrese Número de Recibo");
            }
        });

    });
</script>
</asp:Content>
