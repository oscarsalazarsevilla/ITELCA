﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<ITELCA_CONTROLESTUDIOS.Models.ViewModels.NotaModuloViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Index
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<div class="span-22 append-1 prepend-1 column last ui-widget-content ui-corner-all" style="min-height:300px">
	<% using (Html.BeginForm())
    { %>
	<% Html.EnableClientValidation(); %>
    <%:Html.ValidationSummary("Error en la aplicación")%>
            <span class="introForm">Cargar Notas</span>
            <div class="separator column span-24"></div>
            <% if (!Model.esAdmin)
               { %>
                <%: Html.Hidden("idProfesor", Model.USUARIO.USUARIO_ID)%>
            <% }
               else
               {%>
                 <%: Html.Hidden("idProfesor", "NO")%>
            <% } %>
            <div class="modal" id="cargando"><!-- Place at bottom of page --></div>
             <div class="span-24 column last Formulario">
                    <div class="span-24 column last">
                         <div class="span-8 column">
                            <div class="editor-label">
					           Sede
			                </div>
                            <div class="editor-field">
				                <%: Html.DropDownListFor(model => model.sede, Model.sede, "--Seleccione--")%>
				               
			                </div> 
                        </div>
                         <div class="span-8 column">
                            <div class="editor-label">
					           Curso
			                </div>
                            <div class="editor-field">
				                <%: Html.DropDownListFor(model => model.curso, Model.curso, "--Seleccione--")%>
			                </div> 
                        </div>

                         <div class="span-8 column">
                            <div class="editor-label">
					            Turno
			                </div>
                            <div class="editor-field">
				                <%: Html.DropDownListFor(model => model.turno, Model.turno, "--Seleccione--")%>
			                </div> 
                        </div>
                    </div>
                    <div class="span-24 column last">
                         <div class="span-8 column">
                            <div class="editor-label">
					         Módulo
			                </div>
                            <div class="editor-field">
				                <%: Html.DropDownListFor(model => model.modulo, Model.modulo, "--Seleccione--")%>
			                </div> 
                        </div>
                   
                    <% if (Model.esAdmin)
                       { %>
                   
                         <div class="span-8 column">
                            <div class="editor-label">
					             Profesor
			                </div>
                             <div class="editor-field">
				                <%: Html.DropDownListFor(model => model.profesor, Model.profesor, "--Seleccione--")%>
				                <%: Html.ValidationMessageFor(model => model.profesor)%>
			                </div> 
                        </div>
               
                    <%}
                       else
                       { %>
                        <div class="span-8 column">
                            <div class="editor-label">
					           Profesor
			                </div>
                            <div class="editor-field">
				                <%: Html.TextBoxFor(model => model.USUARIO.NOMBRE, new { Value = Model.USUARIO.NOMBRE + " " + Model.USUARIO.APELLIDO })%>
				                <%: Html.ValidationMessageFor(model => model.USUARIO.NOMBRE)%>
			                </div> 
                        </div>
                    
                    <% } %>
                     </div>
                </div>
            <div class="separator column span-24"></div>

            <div class="column span-24 last">   
                <input type="button" value="Guardar" class="boton" id="guardar"  />
            </div>
            
            <div class="separator column span-24"></div>
            
            <div class="separator column span-24"></div>
        <div class="column span-16 last">   
            <fieldset>
                
                <table id="tablaNota"  >
                    <thead><tr><th>Alumno</th></tr>
                    </thead>
                 </table>
             </fieldset>
           
        </div>
        <%} %>
        </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="CssContent" runat="server">
    <link href="/Content/Css/itelca/demo_table_jui.css" rel="stylesheet" type="text/css" />
</asp:Content> 

<asp:Content ID="Content4" ContentPlaceHolderID="JSContent" runat="server">
    <script src="/Scripts/jquery.dataTables.js" type="text/javascript"></script>
    <script src="/Scripts/Views/notaModulo.js" type="text/javascript"></script>
</asp:Content>
