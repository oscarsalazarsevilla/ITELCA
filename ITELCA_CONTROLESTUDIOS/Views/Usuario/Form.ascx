﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ITELCA_CONTROLESTUDIOS.Models.ViewModels.UsuarioViewModel>" %>
       <div class ="span-24 last">
        <div class="span-8 column">
             <div class="editor-label">
					<%: Html.LabelFor(model => model.USUARIO.NOMBRE)%>
			</div>
            <div class="editor-field">
				<%: Html.TextBoxFor(model => model.USUARIO.NOMBRE)%>
				<%: Html.ValidationMessageFor(model => model.USUARIO.NOMBRE)%>
			</div> 
        </div>
        <div class="span-8 column">
             <div class="editor-label">
					<%: Html.LabelFor(model => model.USUARIO.APELLIDO)%>
			</div>
            <div class="editor-field">
				<%: Html.TextBoxFor(model => model.USUARIO.APELLIDO)%>
				<%: Html.ValidationMessageFor(model => model.USUARIO.APELLIDO)%>
			</div> 
        </div>
         <div class="span-8 column ">
             <div class="editor-label">
					<%: Html.LabelFor(model => model.USUARIO.NRO_DOCUMENTO)%>
			</div>
            <div class="editor-field">
				<%: Html.TextBoxFor(model => model.USUARIO.NRO_DOCUMENTO)%>
				<%: Html.ValidationMessageFor(model => model.USUARIO.NRO_DOCUMENTO)%>
			</div> 
        </div>
   </div>
   
    <div class ="span-24 last">
     <div class="span-8 column ">
             <div class="editor-label">
				<%: Html.LabelFor(model => model.USUARIO.FECHADENACIMIENTO)%>
			</div>
            <div class="editor-field">
				<%: Html.TextBox("USUARIO.FECHADENACIMIENTO", (Model.USUARIO != null) ? Model.USUARIO.FECHADENACIMIENTO.ToString("dd/MM/yyyy") : "")%>
				<%: Html.ValidationMessageFor(model => model.USUARIO.FECHADENACIMIENTO)%>
			</div> 
        </div>
        <div class="span-8 column ">
             <div class="editor-label">
					<%: Html.LabelFor(model => model.USUARIO.EMAIL)%>
			</div>
            <div class="editor-field">
				<%: Html.TextBoxFor(model => model.USUARIO.EMAIL)%>
				<%: Html.ValidationMessageFor(model => model.USUARIO.EMAIL)%>
			</div> 
        </div>
         <div class="span-8 column last ">
             <div class="editor-label">
					<%: Html.LabelFor(model => model.USUARIO.TELEFONO)%>
			</div>
            <div class="editor-field">
				<%: Html.TextBoxFor(model => model.USUARIO.TELEFONO)%>
				<%: Html.ValidationMessageFor(model => model.USUARIO.TELEFONO)%>
			</div> 
        </div>
       
   </div>
    <div class="span-24 column last">
        <div class="span-8 column ">
             <div class="editor-label">
					<%: Html.LabelFor(model => model.USUARIO.TELEFONO2)%>
			</div>
            <div class="editor-field">
				<%: Html.TextBoxFor(model => model.USUARIO.TELEFONO2)%>
				<%: Html.ValidationMessageFor(model => model.USUARIO.TELEFONO2)%>
			</div> 
        </div>
        <div class="span-8 column">
            <div class="editor-label">
					<%: Html.LabelFor(model => model.USUARIO.DIRECCION)%>
			</div>
            <div class="editor-field">
				<%: Html.TextAreaFor(model => model.USUARIO.DIRECCION)%>
				<%: Html.ValidationMessageFor(model => model.USUARIO.DIRECCION)%>
			</div> 
        </div>
        <div class="span-8 column last">
            <div class="editor-label">
					<%: Html.LabelFor(model => model.USUARIO.GENERO_ID)%>
			</div>
            <div class="editor-field">
				<%: Html.DropDownListFor(model => model.USUARIO.GENERO_ID,Model.genero,"--Seleccione--")%>
				<%: Html.ValidationMessageFor(model => model.USUARIO.GENERO_ID)%>
			</div> 
        </div>
    </div>
    <div class="span-24 column">
        <div class="span-12 column ">
             <div class="editor-label">
					<%: Html.LabelFor(model => model.USUARIO.Rol_Id)%>
			</div>
            <div class="editor-field">
				<%: Html.ListBoxFor(model => model.USUARIO.Rol_Id, Model.listaRoles, new { @size = 3, @class = "multiselect", title = "Click para seleccionar una opción" })%>
				<%: Html.ValidationMessageFor(model => model.USUARIO.Rol_Id)%>
			</div> 
        </div>
        <div class="span-12 column ">
            <div class="editor-label">
					<%: Html.LabelFor(model => model.USUARIO.FOTO)%>
			</div>
            <div class="editor-field">
				<input class="input-background"  id="file1" name="file" type="file" accept="jpg|png|gif" />
                <% 
                    if (!String.IsNullOrEmpty(Model.USUARIO.FOTO))
           {%>
                        <br />
                        <a href="/Content/Imagenes/Estudiantes/<%=Model.USUARIO.FOTO%>" target="_blank">
                            Imagen actual
                        </a>
                    <%} %>
			</div>   
    </div>
    