﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<ITELCA_CONTROLESTUDIOS.Models.CambiarClaveModels>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Crear
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
<fieldset>
<div class="span-24 last Formulario">
       <% using (Html.BeginForm()) { %>
		<% Html.EnableClientValidation(); %>
        <%:Html.ValidationSummary("Error en la aplicación") %>
        <%if (TempData["mensaje"] != null && TempData["mensaje"] != "")
        { %>
            <div class="ui-widget">
			    <div class="ui-state-highlight ui-corner-all" style="margin-top: 10px; padding: 0 .3em; width:30%; height:20px;">
				    <span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
				    <%:TempData["mensaje"]%>
                    
			    </div>
		    </div>
    <%} %>
            <div class="span-24 column last ">
                <div class="span-8 column last">
                     <div class="editor-label">
                        <%: Html.LabelFor(model=>model.passActual) %>
                     </div>
                      <div class="editor-field">
                         <%: Html.PasswordFor(model => model.passActual)%>
                        <div><%: Html.ValidationMessageFor(model=>model.passActual) %></div>
                    </div>
                </div>
            </div>
            <div class="span-24 column last">
                <div class="span-8 column">
                    <div class="editor-label">
                        <%: Html.LabelFor(model=>model.passNueva) %>
                    </div>
                    <div class="editor-field">
                         <%: Html.PasswordFor(model=>model.passNueva)%>
                        <div><%: Html.ValidationMessageFor(model=>model.passNueva) %></div>
                    </div>
                </div>
            </div>
            <div class="span-24 column last">
                 <div class="span-8 column last">
                    <div class="editor-label">
                        <%: Html.LabelFor(model=>model.confirmPass) %>
                    </div>
                    <div class="editor-field">
                         <%: Html.PasswordFor(model => model.confirmPass)%>
                        <div><%: Html.ValidationMessageFor(model=>model.confirmPass) %></div>
                    </div>
                </div>
            </div>
            <div class="span-24 column last">
                <input type="submit" value="Guardar" class="boton"  id="Guardar"/>  
            </div>
        </div>
        <div class="column span-24 last">
            <%: Html.ActionLink("Volver al listado", "Listar")%>
        </div>
        
    <% } %>
 </fieldset>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="CssContent" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="JSContent" runat="server">
    
</asp:Content>
