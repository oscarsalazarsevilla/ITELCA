﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<ITELCA_CONTROLESTUDIOS.Models.ViewModels.UsuarioViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    [Itelca - Usuarios]
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
<div class="span-22 append-1 prepend-1 column last ui-widget-content ui-corner-all">
	<span class="introForm">Usuario > Crear</span>
	<% using (Html.BeginForm("Crear", "Usuario", FormMethod.Post, new { enctype = "multipart/form-data" })){%>
		<% Html.EnableClientValidation(); %>
        <%:Html.ValidationSummary("Error en la aplicación") %>
         <%:Html.HiddenFor(model => model.USUARIO.ESTADO, new { Value = "1" }) %>
            <div  class="span-24 Formulario  column last">
					<div class="span-8 column"> 
                        <%: Html.EditorFor(model => model.USUARIO.NOMBREUSUARIO)%>
					</div>
					<div class="span-8 column"> 
						<div class="editor-label">
							<%: Html.LabelFor(model => model.USUARIO.PASSWORD)%>
						</div>
						<div class="editor-field">
							<%: Html.PasswordFor(model => model.USUARIO.PASSWORD)%>
							<%: Html.ValidationMessageFor(model => model.USUARIO.PASSWORD)%>
						</div>
					</div>
                    <div class="span-8 column last"> 
                        <div class="editor-label">
							<%: Html.LabelFor(model => model.USUARIO.CONFIRMARPASSWORD)%>
						</div>
						<div class="editor-field">
							<%: Html.PasswordFor(model => model.USUARIO.CONFIRMARPASSWORD)%>
							<%: Html.ValidationMessageFor(model => model.USUARIO.CONFIRMARPASSWORD)%>
						</div>
                    </div>
         
				<% Html.RenderPartial("Form",Model); %>
			</div>
            <div class="separator column span-24"></div>
			<p>
				<input type="submit" value="Crear" class="button" />
			</p>
    <% } %>
    <div class="separator column span-24">
    </div>
    <div class="column span-24">
        <%: Html.ActionLink("Volver al listado", "Listar")%>
    </div>
</div>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="CssContent" runat="server">
    <link href="/Content/Css/multiselect.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="JsContent" runat="server">
    <script src="/Scripts/jquery.maskedinput-1.2.2.js" type="text/javascript"></script>
    <script src="/Scripts/jquery.multiselect.min.js" type="text/javascript"></script>
    <script src="/Scripts/jquery.multiselect.es.js" type="text/javascript"></script>
    <script src="/Scripts/Views/usuario.js" type="text/javascript"></script>
</asp:Content>

