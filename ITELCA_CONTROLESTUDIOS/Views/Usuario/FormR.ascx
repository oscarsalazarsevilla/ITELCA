﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ITELCA_CONTROLESTUDIOS.Models.ViewModels.UsuarioViewModel>" %>
       <div class ="span-24 last">
        <div class="span-8 column">
             <div class="editor-label">
					<%: Html.LabelFor(model => model.USUARIO.NOMBRE)%>
			</div>
            <div class="editor-field">
				<%: Html.TextBoxFor(model => model.USUARIO.NOMBRE)%>
				<%: Html.ValidationMessageFor(model => model.USUARIO.NOMBRE)%>
			</div> 
        </div>
        <div class="span-8 column">
             <div class="editor-label">
					<%: Html.LabelFor(model => model.USUARIO.APELLIDO)%>
			</div>
            <div class="editor-field">
				<%: Html.TextBoxFor(model => model.USUARIO.APELLIDO)%>
				<%: Html.ValidationMessageFor(model => model.USUARIO.APELLIDO)%>
			</div> 
        </div>
         <div class="span-8 column ">
             <div class="editor-label">
					<%: Html.LabelFor(model => model.USUARIO.NRO_DOCUMENTO)%>
			</div>
            <div class="editor-field">
				<%: Html.TextBoxFor(model => model.USUARIO.NRO_DOCUMENTO)%>
				<%: Html.ValidationMessageFor(model => model.USUARIO.NRO_DOCUMENTO)%>
			</div> 
        </div>
   </div>
   
    <div class ="span-24 last">
          <div class="span-8 column ">
             
			<%: Html.EditorFor(model => model.USUARIO.FECHADENACIMIENTO)%>
				 
        </div>
        <div class="span-8 column ">
             <div class="editor-label">
					<%: Html.LabelFor(model => model.USUARIO.EMAIL)%>
			</div>
            <div class="editor-field">
				<%: Html.TextBoxFor(model => model.USUARIO.EMAIL)%>
				<%: Html.ValidationMessageFor(model => model.USUARIO.EMAIL)%>
			</div> 
        </div>
         <div class="span-8 column ">
             <div class="editor-label">
					<%: Html.LabelFor(model => model.USUARIO.TELEFONO)%>
			</div>
            <div class="editor-field">
				<%: Html.TextBoxFor(model => model.USUARIO.TELEFONO, new { @class = "usuario_Phone" })%>
				<%: Html.ValidationMessageFor(model => model.USUARIO.TELEFONO)%>
			</div> 
        </div>
       
   </div>
    <div class="span-24">
        <div class="span-8 column ">
             <div class="editor-label">
					<%: Html.LabelFor(model => model.USUARIO.TELEFONO2)%>
			</div>
            <div class="editor-field">
				<%: Html.TextBoxFor(model => model.USUARIO.TELEFONO2, new { @class = "usuario_Phone" })%>
				<%: Html.ValidationMessageFor(model => model.USUARIO.TELEFONO2)%>
			</div> 
        </div>
        <div class="span-8 column">
            <div class="editor-label">
					<%: Html.LabelFor(model => model.USUARIO.DIRECCION)%>
			</div>
            <div class="editor-field">
				<%: Html.TextAreaFor(model => model.USUARIO.DIRECCION)%>
				<%: Html.ValidationMessageFor(model => model.USUARIO.DIRECCION)%>
			</div> 
        </div>
        <div class="span-8 column">
            <div class="editor-label">
					<%: Html.LabelFor(model => model.USUARIO.GENERO_ID)%>
			</div>
            <div class="editor-field">
				<%: Html.DropDownListFor(model => model.USUARIO.GENERO_ID,Model.genero,"--Seleccione--")%>
				<%: Html.ValidationMessageFor(model => model.USUARIO.GENERO_ID)%>
			</div> 
        </div>
    </div>
        
      