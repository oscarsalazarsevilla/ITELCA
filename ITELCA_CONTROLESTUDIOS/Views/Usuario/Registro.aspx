﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Web2.Master" Inherits="System.Web.Mvc.ViewPage<ITELCA_CONTROLESTUDIOS.Models.ViewModels.ViewModelForViewWrapper<ITELCA_CONTROLESTUDIOS.Models.ViewModels.MasterViewModel,ITELCA_CONTROLESTUDIOS.Models.ViewModels.UsuarioViewModel>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    [Itelca - Usuarios]
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
<div class="span-22 append-1 prepend-1 column last ui-widget-content ui-corner-all">
	<span class="introForm">Usuario > Registrarse</span>
		<% using (Html.BeginForm()) { %>
		<% Html.EnableClientValidation(); %>
        <%--<%:Html.ValidationSummary("Error en la aplicación") %>--%>
        <%:Html.HiddenFor(model => model.View.USUARIO.ESTADO, new { Value = "1" }) %>
        <%:Html.HiddenFor(model => model.View.USUARIO.ESTABLOQUEADO, new { Value = "0" }) %>
            <div  class="span-24 Formulario  column last" style="margin-left:50px;">
				<% Html.RenderPartial("FormR",Model.View); %>

			</div>
            <div class="separator column span-24"></div>
			<p>
				<input type="submit" value="Crear" class="button" />
			</p>
    <% } %>
  
</div>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="CssContent" runat="server">
    <link href="/Content/Css/multiselect.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="JsContent" runat="server">
    <script src="/Scripts/jquery.maskedinput-1.2.2.js" type="text/javascript"></script>
    <script src="/Scripts/jquery.multiselect.min.js" type="text/javascript"></script>
    <script src="/Scripts/Views/usuario.js" type="text/javascript"></script>
</asp:Content>

