﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<ITELCA_CONTROLESTUDIOS.Models.ViewModels.PrecioProductoViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    [Itelca - Precio de productos]
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
<div class="span-22 append-1 prepend-1 column last ui-widget-content ui-corner-all">
	<span class="introForm">Precios de productos > Crear</span>
		<% using (Html.BeginForm()) { %>
		<% Html.EnableClientValidation(); %>
        <%:Html.ValidationSummary("Error en la aplicación") %>
        <%:Html.HiddenFor(model => model.precioProducto.ESTADO, new { Value = "1" }) %>
        <%:Html.HiddenFor(model => model.precioProducto.SETID, new { Value = "ITELC" })%>
            <div  class="span-24 Formulario  column last">
				<% Html.RenderPartial("Form",Model); %>
			</div>
             <div class="separator column span-24">
            </div>
            <p>
				<input type="submit" value="Crear" class="button" />
			</p>
           
            <div class="column span-24">
                <%: Html.ActionLink("Volver al listado", "Listar")%>
            </div>
          
			
    <% } %>
     
                
</div>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="CssContent" runat="server">
 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="JsContent" runat="server">
    <script src="/Scripts/Views/precioProducto.js" type="text/javascript"></script>
    <script src="/Scripts/autoNumeric.js" type="text/javascript"></script>
</asp:Content>

