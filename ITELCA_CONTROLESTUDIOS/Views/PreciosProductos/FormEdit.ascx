﻿   <%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ITELCA_CONTROLESTUDIOS.Models.ViewModels.PrecioProductoViewModel>" %>
    <div class ="span-24 last">
        <div class="span-8 column last">
            <div class="display-label">
                <%: Html.LabelFor(model => model.precioProducto.INV_ITEM_ID)%>
			</div>
            <div class="display-field">
				<%: Model.precioProducto.PRODUCTOS.DESCR %>
			</div> 
        </div>
   </div>

   <div class ="span-24 last">
        <div class="span-8 column last">
            <div class="display-label">
                <%: Html.LabelFor(model => model.precioProducto.FECHA_EFECTIVA)%>
			</div>
            <div class="display-field">
				<%: Model.precioProducto.FECHA_EFECTIVA.ToString("dd/MM/yyyy")%>
			</div> 
        </div>
    </div>

    <div class ="span-24 last">
        <div class="span-8 column last">
             <div class="editor-label">
					<%: Html.LabelFor(model => model.precioProducto.MONTO)%>
			</div>
            <div class="editor-field">
				<%: Html.TextBoxFor(model => model.precioProducto.MONTO, new { @class = "montos" })%>
				<%: Html.ValidationMessageFor(model => model.precioProducto.MONTO)%>
			</div> 
        </div>
    </div>