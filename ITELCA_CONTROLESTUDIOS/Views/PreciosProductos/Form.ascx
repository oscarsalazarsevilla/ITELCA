﻿   <%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ITELCA_CONTROLESTUDIOS.Models.ViewModels.PrecioProductoViewModel>" %>
    <div class ="span-24 last">
        <div class="span-8 column last">
             <div class="editor-label">
					<%: Html.LabelFor(model => model.precioProducto.INV_ITEM_ID)%>
			</div>
            <div class="editor-field">
				<%: Html.DropDownListFor(model => model.precioProducto.INV_ITEM_ID, Model.productos, "-- Seleccione --")%>
				<%: Html.ValidationMessageFor(model => model.precioProducto.INV_ITEM_ID)%>
			</div> 
        </div>
   </div>

   <div class ="span-24 last">
        <div class="span-8 column last">
             <div class="editor-label">
                <%: Html.LabelFor(model => model.precioProducto.FECHA_EFECTIVA)%>
			</div>
            <div class="editor-field">
				<%: Html.TextBoxFor(model => model.precioProducto.FECHA_EFECTIVA)%>
				<%: Html.ValidationMessageFor(model => model.precioProducto.FECHA_EFECTIVA)%>
			</div> 
        </div>
    </div>

    <div class ="span-24 last">
        <div class="span-8 column last">
             <div class="editor-label">
					<%: Html.LabelFor(model => model.precioProducto.MONTO)%>
			</div>
            <div class="editor-field">
				<%: Html.TextBoxFor(model => model.precioProducto.MONTO, new {@class = "montos"})%>
				<%: Html.ValidationMessageFor(model => model.precioProducto.MONTO)%>
			</div> 
        </div>
    </div>