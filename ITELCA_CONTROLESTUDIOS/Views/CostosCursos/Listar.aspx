﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<ITELCA_CONTROLESTUDIOS.Models.ViewModels.CostosCursosViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Listar
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2>Listar</h2>
<fieldset>
<div class="span-24 last">
        <p>
            <%: Html.ActionLink("Nuevo", "Crear") %>
        </p>
        <div id="msg">
        <%
            try
            {

                if (TempData["mensaje"] != null && TempData["mensaje"]  != "")
                {%>
			    <div  class="ui-state-highlight ui-corner-all" style="margin-top: 10px; padding: 0 .3em; width:30%; height:20px;">
				    <span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
				    <div><%:TempData["mensaje"]  %></div>
			    </div>
		    
          <%
                }
            }
            catch{} 
            
           %>
        </div>

        <h4><%:Html.Label("Curso") %></h4>
        <%:Html.DropDownList("SelectListCurso", Model.ListaCursos,"Seleccione")%>
        </br>
            <table id="gridCurso">
            </table>
            <div id="pager"></div>
</div>
</fieldset>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="CssContent" runat="server">
    <link href="/Content/Css/ui.jqgrid.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="JSContent" runat="server">
    <script src="/Scripts/grid.locale-es.js" type="text/javascript"></script>
    <script src="/Scripts/jquery.jqGrid.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {

            var gridCurso = $("#gridCurso").jqGrid({
                url: '/CostosCursos/ObtenerData',
                datatype: 'json',
                mtype: 'POST',
                postData: { id: function () { return $("#SelectListCurso option:selected").val() } },
                colNames: ['Id', 'Curso','Turno','Monto', 'Editar', 'Eliminar'],
                colModel: [
		            { name: 'COSTO_CURSO_ID', index: 'COSTO_CURSO_ID', align: "left", width: 80, resizable: true, searchoptions: { sopt: ['cn']} },
		            { name: 'CURSO_NOMBRE', index: 'CURSO_NOMBRE', width: 200, align: "left", searchoptions: { sopt: ['cn']} },
                    { name: 'TURNO_NOMBRE', index: 'TURNO_NOMBRE', width: 200, searchoptions: { sopt: ['cn']} },
                    { name: 'MONTO', index: 'MONTO', width: 100, searchoptions: { sopt: ['cn']} },
		            { name: 'Action1', index: 'Action1', align: "left", width: 100, resizable: false, sortable: false, search: false },
                    { name: 'Action2', index: 'Action2', align: "left", width: 100, resizable: false, sortable: false, search: false}],
                pager: jQuery('#pager'),
                rowNum: 500,
                rowList: [500, 1000, 5000],
                sortname: 'COSTO_CURSO_ID',
                sortorder: 'desc',
                autowidth: true,
                viewrecords: true,
                caption: 'Listado de Costos por Cursos',
                loadComplete: function () {
                    $(".ui-icon-close").click(function () {
                        var gr = $(this).attr("id");
                        var r = confirm("Desea Eliminar el Costo Curso?");
                        if (r == true) {
                            $.post("/CostosCursos/Eliminar", { id: gr }, function (data) {
                                var dat = ' <div  class="ui-state-highlight ui-corner-all" style="margin-top: 10px; padding: 0 .3em; width:30%; height:20px;"><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span><div>';
                                dat += data[0].msg;
                                dat += '</div></div>';
                                $("#msg").empty().html(dat);
                                gridCurso.trigger('reloadGrid');

                            });
                        }
                        else {
                            x = "Cancelar";
                        }
                        // alert("adas");
                    });

                }
            }).navGrid('#pager',
            {
                edit: false, add: false, del: false, search: true, refresh: false
            },
            {}, //edit options
            {}, //add options
            {}, //del options
            {multipleSearch: true} // search options
            );

            $('#SelectListCurso').change(function () {
                jQuery("#gridCurso").trigger("reloadGrid");
            });
        });
    </script>
</asp:Content>
