﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ITELCA_CONTROLESTUDIOS.Models.ViewModels.CostosCursosViewModel>" %>
       <div class ="span-24 column last">
            <div class="span-8 column last">
                 <div class="editor-label">
					    <%: Html.LabelFor(model => model.COSTOS_CURSOS.CURSO_ID)%>
			    </div>
                <div class="editor-field">
				    <%: Html.DropDownListFor(model => model.COSTOS_CURSOS.CURSO_ID,Model.ListaCursos,"--Seleccione--")%>
				    <%: Html.ValidationMessageFor(model => model.COSTOS_CURSOS.CURSO_ID)%>
			    </div> 
            </div>
        </div>
       <div class ="span-24 column last">
            <div class="span-8 column">
                 <div class="editor-label">
					    <%: Html.LabelFor(model => model.COSTOS_CURSOS.TURNO_ID)%>
			    </div>
                <div class="editor-field">
				    <%: Html.DropDownListFor(model => model.COSTOS_CURSOS.TURNO_ID, Model.ListaTurnos, "--Seleccione--")%>
				    <%: Html.ValidationMessageFor(model => model.COSTOS_CURSOS.TURNO_ID)%>
			    </div> 
            </div>
        </div>

       <div class ="span-24 column last">
        <div class="span-8 column last">
             <div class="editor-label">
					<%: Html.Label("Monto")%>
			</div>
            <div class="editor-field">
				<%: Html.TextBoxFor(model => model.COSTOS_CURSOS.MONTO,  new { @class = "montos" })%>
				<%--<%: Html.ValidationMessageFor(model => model.COSTOS_CURSOS.MONTO)%>--%>
			</div> 
        </div>
    </div>

     <div class ="span-24 column last">    
        <div class="span-8 column last">
                 <div class="editor-label">
					    <%: Html.LabelFor(model => model.COSTOS_CURSOS.FECHA_EFECTIVA)%>
			    </div>
                <div class="editor-field">
				    <%: Html.TextBoxFor(model => model.COSTOS_CURSOS.FECHA_EFECTIVA, new { @class = "datepicker" })%>
				    <%: Html.ValidationMessageFor(model => model.COSTOS_CURSOS.FECHA_EFECTIVA)%>
			    </div> 
            </div>    
    </div>
     