﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ITELCA_CONTROLESTUDIOS.Models.ViewModels.OfertasTalleresViewModel>" %>

   
   <div class ="span-24 last">
        <div class="span-8 column ">
            <div class="editor-label">
				<%: Html.LabelFor(model => model.OFERTASTALLERES.TALLER_ID)%>
			</div>
            <div class="editor-field">
				<%: Html.DropDownListFor(model => model.OFERTASTALLERES.TALLER_ID, Model.listaTalleres)%>
				<%: Html.ValidationMessageFor(model => model.OFERTASTALLERES.TALLER_ID)%>
			</div> 
        </div>
         <div class="span-8 column ">
            <div class="editor-label">
				<%: Html.LabelFor(model => model.OFERTASTALLERES.AULA_ID)%>
			</div>
            <div class="editor-field">
				<%: Html.DropDownListFor(model => model.OFERTASTALLERES.AULA_ID, Model.listaAulas, "- Seleccione -")%>
				<%: Html.ValidationMessageFor(model => model.OFERTASTALLERES.AULA_ID)%>
			</div> 
        </div>
           <div class="span-8 column ">
                <div class="editor-label">
				    <%: Html.LabelFor(model => model.OFERTASTALLERES.PROFESOR_ID)%>
			    </div>
                <div class="editor-field">
				    <%: Html.DropDownListFor(model => model.OFERTASTALLERES.PROFESOR_ID, Model.profesores, "- Seleccione -")%>
				    <%: Html.ValidationMessageFor(model => model.OFERTASTALLERES.PROFESOR_ID)%>
			    </div> 
        </div>
    </div>


     <div class="span-24 ">
          <div class="span-8 column">
             <div class="editor-label">
					<%: Html.LabelFor(model => model.OFERTASTALLERES.FECHA_INICIO)%>
			</div>
            <div class="editor-field">
				<%: Html.TextBoxFor(model => model.OFERTASTALLERES.FECHA_INICIO, new { @class = "datepicker" })%>
				<%: Html.ValidationMessageFor(model => model.OFERTASTALLERES.FECHA_INICIO)%>
			</div> 
        </div>    
        <div class="span-8 column ">
             <div class="editor-label">
					<%: Html.LabelFor(model => model.OFERTASTALLERES.FECHA_FIN)%>
			</div>
            <div class="editor-field">
				<%: Html.TextBoxFor(model => model.OFERTASTALLERES.FECHA_FIN, new { @class = "datepicker" })%>
				<%: Html.ValidationMessageFor(model => model.OFERTASTALLERES.FECHA_FIN)%>
			</div> 
        </div> 
         <div class="span-8 column ">
             <div class="editor-label">
					<%: Html.LabelFor(model => model.OFERTASTALLERES.HORA)%>
			</div>
            <div class="editor-field">
				<%: Html.TextBoxFor(model => model.OFERTASTALLERES.HORA, new { @class = "hora" })%>
				<%: Html.ValidationMessageFor(model => model.OFERTASTALLERES.HORA)%>
			</div> 
        </div>    
     </div>
     <div class="span-24 column last">
           <div class="span-8 column last">
             <div class="editor-label">
					<%: Html.LabelFor(model => model.OFERTASTALLERES.MONTO)%>
			</div>
            <div class="editor-field">
				<%: Html.TextBoxFor(model => model.OFERTASTALLERES.MONTO, new { @class = "montos" })%>
				<%: Html.ValidationMessageFor(model => model.OFERTASTALLERES.MONTO)%>
			</div> 
        </div>    

        <div class="span-8 column last">
             <div class="editor-label">
					<%: Html.LabelFor(model => model.OFERTASTALLERES.CAPACIDAD)%>
			</div>
            <div class="editor-field">
				<%: Html.TextBoxFor(model => model.OFERTASTALLERES.CAPACIDAD)%>
				<%: Html.ValidationMessageFor(model => model.OFERTASTALLERES.CAPACIDAD)%>
			</div> 
        </div>    
     </div>
   
   
        
      