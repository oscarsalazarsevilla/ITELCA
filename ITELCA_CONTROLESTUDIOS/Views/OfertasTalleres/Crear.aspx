﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<ITELCA_CONTROLESTUDIOS.Models.ViewModels.OfertasTalleresViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    [Itelca - Ofertas Talleres]
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
<div class="span-22 append-1 prepend-1 column last ui-widget-content ui-corner-all">
	<span class="introForm">Oferta Talleres > Crear</span>
		<% using (Html.BeginForm()) { %>
		<% Html.EnableClientValidation(); %>
        <%:Html.ValidationSummary("Error en la aplicación") %>
       
            <div  class="span-24 Formulario  column last">
				<% Html.RenderPartial("Form",Model); %>
			</div>
             <div class="separator column span-24">
            </div>
             <p>
				<input type="submit" value="Crear" class="button" />
			</p>
            <div  class="span-24 column last" id="ggg">
                <div class="editor-label">
				    <%: Html.Label("Talleres")%>
			    </div>
                <div class="editor-field">
				    <%: Html.DropDownList("tallerGrid", Model.listaTalleres)%>
			    </div> 
             </div>
             <div  class="span-24 column last">

                <table id="OfertasTalleresGrid"></table>
                <div id="pager"></div> 
             </div>

            
           
            <div class="column span-24">
                <%: Html.ActionLink("Volver al listado", "Listar")%>
            </div>
          
			
    <% } %>
</div>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="CssContent" runat="server">
    <link href="/Content/Css/ui.jqgrid.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="JsContent" runat="server">
     <script src="/Scripts/grid.locale-es.js" type="text/javascript"></script>
    <script src="/Scripts/jquery.jqGrid.min.js" type="text/javascript"></script>
    <script src="/Scripts/autoNumeric.js" type="text/javascript"></script>
    <script src="/Scripts/jquery.maskedinput-1.2.2.js" type="text/javascript"></script>
    <script src="/Scripts/Views/OfertaTalleres.js" type="text/javascript"></script>
        <script type="text/javascript">
        var colAula, colFechaFin;
       
        $(document).ready(function () {

            function reload(rowid, result) {
                jQuery("#OfertasTalleresGrid").trigger("reloadGrid");
            }
            resetStatesValues = function () {
                // set 'value' property of the editoptions to initial state
                grid.jqGrid('setColProp', 'Aula', { editoptions: { dataUrl: "/OfertasModulos/ObtenerSelect"} });
            };
            var lastSelection, grid = jQuery("#OfertasTalleresGrid");
            grid.jqGrid({
                url: '/OfertasTalleres/ObtenerData',
                mtype: 'POST',
                postData: { id: function () { return $("#tallerGrid option:selected").val() } },
                datatype: 'json',
                colNames: ['idf', 'Aula', 'Capacidad', 'Fecha Inicio', 'Fecha Fin', 'Hora','Monto','Profesor', 'Eliminar'],
                colModel: [
                 {
                     name: "idf",
                     index: "idf",
                     editable: false,
                     key: true,
                     hidden: true
                 },
                {
                    name: 'AULA_ID',
                    index: 'AULA_ID',
                    editable: false,
                    editrules: { edithidden: true, required: false },
                    edittype: 'select',
                    sortable:false,
                    editoptions: {
                        dataInit: function (elem) { colAula = elem; $(elem).html("<select><option value=''>Cargando...</option></select>"); },
                        dataUrl: "/OfertasTalleres/ObtenerAula?sede=",
                        buildSelect: function (data) {
                            var response = jQuery.parseJSON(data);
                            var s = '<select>';
                            if (response && response.length) {
                                for (var i = 0, l = response.length; i < l; i++) {
                                    s += '<option value="' + response[i].key + '">' + response[i].value + '</option>';
                                }
                            }
                            $(colAula).empty();
                            return s + "</select>";
                        }
                    }

                },
                 { name: 'CAPACIDAD',
                     index: 'CAPACIDAD',
                     editable: true,
                     align: 'right',
                     sortable: false//,
                     //cellattr: function (rowId, tv, rawObject, cm, rdata) { return 'class="BankTranEdit"' },
                     //formatter: 'currency',
                     //formatoptions: { decimalSeparator: '.', thousandsSeparator: ',', decimalPlaces: 2, defaultValue: '&nbsp;' } 
                 },
                 { name: 'FECHA_INICIO',
                     index: 'FECHA_INICIO',
                     editable: true,
                     formatter: 'date', formatoptions: { srcformat: 'd-m-Y', newformat: 'd/m/Y' },
                     editrules: { required: true, date: true },
                     editoptions: {
                         dataInit: function (element) {
                             $(element).datepicker({
                                 dateFormat: "dd/m/yy",
                                 autoSize: true,
                                 changeYear: true,
                                 changeMonth: true,
                                 showButtonPanel: true,
                                 showWeek: true,
                                 onClose: function (dateText, inst) {
                                     $(this).focus();
                                     $(colFechaFin).val(dateText);
                                 }
                             });
                         }
                     }

                 },
                   { name: 'FECHA_FIN',
                       index: 'FECHA_FIN',
                       editable: true,
                       formatter: 'date', formatoptions: { srcformat: 'd-m-Y', newformat: 'd/m/Y' },
                       editrules: { required: true, date: true },
                       editoptions: {
                           dataInit: function (element) {
                               colFechaFin = element;
                               $(element).datepicker({
                                   dateFormat: "dd/m/yy",
                                   autoSize: true,
                                   changeYear: true,
                                   changeMonth: true,
                                   showButtonPanel: true,
                                   showWeek: true,
                                   onClose: function (dateText, inst) {
                                       $(this).focus();
                                       
                                   }
                               });
                           }
                       }

                   },
                    { name: 'HORA',
                        index: 'HORA',
                        editable: true,
                        formatoptions: { newformat: 'h:mm:TT' },
                        editrules: { required: true},
                        editoptions: {
                            dataInit: function (element) {
                                $(element).timepicker({
                                    timeFormat: 'h:mm:TT',
                                    stepHour: 1,
                                    stepMinute: 5,
                                    hourMin: 7,
                                    hourMax: 22,
                                    onClose: function (dateText, inst) {
                                        var id = $(this).attr("id");
                                        if (id.indexOf("hor_fin") != -1) {
                                            var et = dateText;
                                            var st = $("#" + id).parent().parent().find('input:text').first().val();
                                            if (st != "") {
                                                var stt = new Date("November 13, 2013 " + st);
                                                stt = stt.getTime();
                                                var endt = new Date("November 13, 2013 " + et);
                                                endt = endt.getTime();
                                                if (stt > endt) {
                                                    $("#" + id).parent().parent().find('input:text').addClass("input-validation-error").last().val("").attr("placeholder", "HORA FIN MENOR");
                                                }
                                                else {
                                                    $("#" + id).parent().parent().find('input:text').removeClass("input-validation-error").attr("placeholder", "");
                                                }
                                            }
                                        }
                                    }
                                });
                            }
                        }

                    },
                     { name: 'MONTO',
                         index: 'MONTO',
                         editable: true,
                         align: 'right',
                         formatter:'number',
                         formatoptions: { decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, defaultValue: '0.00' },
                         editrules: { required: true,number:true },
                         sortable: false//,
                         //cellattr: function (rowId, tv, rawObject, cm, rdata) { return 'class="BankTranEdit"' },
                         //formatter: 'currency',
                         //formatoptions: { decimalSeparator: '.', thousandsSeparator: ',', decimalPlaces: 2, defaultValue: '&nbsp;' } 
                     },
                    
                     {
                         name: 'PROFESOR_ID',
                         index: 'PROFESOR_ID',
                         editable: true,
                         edittype: 'select',
                         //formatter: 'select',
                         editrules: { required: true},
                         editoptions: {
                             dataInit: function (elem) { colProfesor = elem; $(elem).html("<select><option value=''>Cargando...</option></select>"); },
                             dataUrl: "/OfertasModulos/ObtenerProfesor",
                             buildSelect: function (data) {
                                 var response = jQuery.parseJSON(data);
                                 var s = '<select>';
                                 if (response && response.length) {
                                     for (var i = 0, l = response.length; i < l; i++) {
                                         s += '<option value="' + response[i].key + '">' + response[i].value + '</option>';
                                     }
                                 }
                                 $(colProfesor).empty();
                                 return s + "</select>";
                             }
                         }

                     },
                    { name: 'Action2',
                        index: 'Action2',
                        align: "left",
                        width: 50,
                        resizable: false,
                        sortable: false,
                        search: false
                    }
		    ],
                pager: '#pager', //jQuery('#pager'),
                rowNum: 500,
                rowList: [1000, 5000, 10000],
                editurl: "/OfertasTalleres/GuardarFila", // 'clientArray'
                sortname: 'Number',
                sortorder: "desc",
                viewrecords: true,
                // width: 450,
                height: 300, //'auto', //450,
                onCellSelect: function (rowid, iCol, cellContent, e) {

                    //                     if (lastSelection) {
                    //                         guardar(lastSelection);
                    //                     }

                },
                ondblClickRow: function (rowid, iCol, cellContent, e) {
                    grid.jqGrid('restoreRow', lastSelection);
                    grid.jqGrid('editRow', rowid, true, null, null, null, null, reload);
                    lastSelection = rowid;
                },
                onSelectRow: function (id) {
                    grid.jqGrid('restoreRow', lastSelection);
                },
                loadComplete: function () {
                    $(".ui-icon-close").click(function () {
                        var gr = $(this).attr("id");
                        var r = confirm("Desea Eliminar El Taller ");
                        if (r == true) {
                            $.post("/OfertasTalleres/Eliminar", { id: gr }, function (data) {
                                alert(data[0].msg);
                                grid.trigger('reloadGrid');

                            });

                        }
                        else {
                            x = "Cancelar";
                        }
                        // alert("adas");
                    });
                }
            }).navGrid('#pager',
            {
                edit: true, add: false, del: false, search: true, refresh: true
            },
            {
        }, //edit options
            {}, //add options
            {}, //del options
            {multipleSearch: true} // search options
            );
        //             $("#pager_left").hide(); // hide unused place in the pager

    });
       
     </script>
</asp:Content>

