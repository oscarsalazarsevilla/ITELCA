﻿   <%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ITELCA_CONTROLESTUDIOS.Models.ViewModels.FaqViewModel>" %>
    <div class ="span-24 column last">
        <div class="span-8 column ">
             <div class="editor-label">
					<%: Html.LabelFor(model => model.faq.TIPO_FAQS_ID)%>
			</div>
            <div class="editor-field">
				<%: Html.DropDownListFor(model => model.faq.TIPO_FAQS_ID, Model.listaTiposFaqs,"- Seleccione -")%>
				<%: Html.ValidationMessageFor(model => model.faq.TIPO_FAQS_ID)%>
			</div> 
        </div>
        <div class="span-8 column  ">
             <div class="editor-label">
					<%: Html.LabelFor(model => model.faq.ORDEN)%>
			</div>
            <div class="editor-field">
				<%: Html.TextBoxFor(model => model.faq.ORDEN)%>
				<%: Html.ValidationMessageFor(model => model.faq.ORDEN)%>
			</div> 
        </div>
    </div>
    <div class ="span-24 column last">
        <div class="editor-label">
			<%: Html.LabelFor(model => model.faq.PREGUNTA)%>
		</div>
        <div class="editor-field">
			<%: Html.TextAreaFor(model => model.faq.PREGUNTA)%>
			<%: Html.ValidationMessageFor(model => model.faq.PREGUNTA)%>
		</div> 
    </div>
    <div class ="span-24 column last">
        <div class="editor-label">
				<%: Html.LabelFor(model => model.faq.RESPUESTA)%>
		</div>
        <div class="editor-field">
			<%: Html.TextAreaFor(model => model.faq.RESPUESTA)%>
			<%: Html.ValidationMessageFor(model => model.faq.RESPUESTA)%>
		</div> 
    </div>