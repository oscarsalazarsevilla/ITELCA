﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<ITELCA_CONTROLESTUDIOS.Models.ViewModels.ModuloTurnoViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    [Itelca - Modulos turnos]
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
<div class="span-22 append-1 prepend-1 column last ui-widget-content ui-corner-all">
	<span class="introForm">Modulo > Editar</span>
		<% using (Html.BeginForm()) { %>
		<% Html.EnableClientValidation(); %>
        <%:Html.ValidationSummary("Error en la aplicación") %>
         <%:Html.HiddenFor(model => model.modulo_turno.MODULO_ID) %>
         <%:Html.HiddenFor(model => model.modulo_turno.TURNO_ID) %>
         <%:Html.HiddenFor(model => model.modulo_turno.USUARIO_ID)%>
            <div  class="span-24 Formulario  column last">
				<% Html.RenderPartial("FormEdit",Model); %>   
                <div class="span-24">
                    
                    <div class="span-8 column last">
                        <div class="editor-label">
				            <%: Html.LabelFor(model => model.modulo_turno.ESTADO)%>
			            </div>
                        <div class="editor-field">
				            <%: Html.DropDownListFor(model => model.modulo_turno.ESTADO, Model.activo)%>
				            <%: Html.ValidationMessageFor(model => model.modulo_turno.ESTADO)%>
			            </div> 
                    </div>
                </div>
			</div>
             <div class="separator column span-24">
            </div>
            <p>
				<input type="submit" value="Guardar" class="button" />
			</p>
           
            <div class="column span-24">
                <%: Html.ActionLink("Volver al listado", "Listar")%>
            </div>
         
			
    <% } %>
     
</div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="CssContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="JsContent" runat="server">
    <script src="/Scripts/Views/moduloTurno.js" type="text/javascript"></script>
</asp:Content>

