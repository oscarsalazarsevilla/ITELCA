﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<ITELCA_CONTROLESTUDIOS.Models.ViewModels.ModuloTurnoViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Listar
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2>Listar</h2>
<fieldset>
<div class="span-24 last">
        <p>
            <%: Html.ActionLink("Nuevo", "Crear") %>
        </p>
        <div id="msg">
        <%
            try
            {

                if (TempData["mensaje"] != null && TempData["mensaje"]  != "")
                {%>
			    <div  class="ui-state-highlight ui-corner-all" style="margin-top: 10px; padding: 0 .3em; width:30%; height:20px;">
				    <span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
				    <div><%:TempData["mensaje"]  %></div>
			    </div>
		    
          <%
                }
            }
            catch{} 
            
           %>
        </div>
        <div>
        <h4><%:Html.Label("Curso") %></h4>
        <%:Html.DropDownList("SelectListCurso", Model.listaCurso, "Seleccione")%>
           
          </br>
          </br>
          </br>
    
            <table id="gridModuloTurno">
            </table>
            <div id="pager"></div>
        </div>
</div>
</fieldset>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="CssContent" runat="server">
    <link href="/Content/Css/ui.jqgrid.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="JSContent" runat="server">
    <script src="/Scripts/grid.locale-es.js" type="text/javascript"></script>
    <script src="/Scripts/jquery.jqGrid.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            winHeight = window.innerHeight;
            wHeight = winHeight - 340;

            var gridModuloTurno = $("#gridModuloTurno").jqGrid({
                postData: { CursoId: function () { return $("#SelectListCurso option:selected").val() } },
                url: '/ModulosTurnos/ObtenerData',
                datatype: 'json',
                mtype: 'POST',
                colNames: ['Modulo Id', 'Turno Id', 'Módulo','Turno','N° clases','Estado','Editar', 'Eliminar'],
                colModel: [
		            { name: 'MODULO_ID', index: 'MODULO_ID', align: "right", resizable: true, width: 50, searchoptions: { sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']} },
		            { name: 'TURNO_ID', index: 'TURNO_ID', align: "right", resizable: true, width: 50, searchoptions: { sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']} },
		            { name: 'MODULOS.NOMBRE', index: 'MODULOS.NOMBRE', width: 200, searchoptions: { sopt: ['cn']} },
                    { name: 'TURNOS.NOMBRE', index: 'TURNOS.NOMBRE', width: 200, sortable: false },
                    { name: 'NRO_CLASES', index: 'NRO_CLASES', width: 50, searchoptions: { sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']} },
                    { name: 'ESTADO', index: 'ESTADO', width: 50, align: "center", resizable: true, formatter: 'checkbox', stype: "select", searchoptions: { sopt: ['eq', 'ne'], value: "1:Si;0:No"} },
		            { name: 'Action1', index: 'Action1', align: "center", width: 50, resizable: false, sortable: false, search: false },
                    { name: 'Action2', index: 'Action2', align: "center", width: 50, resizable: false, sortable: false, search: false}],
                pager: jQuery('#pager'),
                rowNum: 500,
                height: wHeight,
                rowList: [500, 1000, 5000],
                sortname: 'MODULOS.NOMBRE',
                sortorder: 'desc',
                autowidth: true,
                viewrecords: true,
                caption: 'Listado de módulos turnos',
                loadComplete: function () {
                    $(".ui-icon-close").click(function () {
                        var gr = $(this).attr("id");
                        var r = confirm("Desea eliminar el módulo - turno?");
                        if (r == true) {
                            $.post("/ModulosTurnos/Eliminar", { id: gr }, function (data) {
                                var dat = ' <div  class="ui-state-highlight ui-corner-all" style="margin-top: 10px; padding: 0 .3em; width:30%; height:20px;"><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span><div>';
                                dat += data[0].msg;
                                dat += '</div></div>';
                                $("#msg").empty().html(dat);
                                gridModuloTurno.trigger('reloadGrid');

                            });
                        }
                        else {
                            x = "Cancelar";
                        }
                        // alert("adas");
                    });

                }
            }).navGrid('#pager',
            {
                edit: false, add: false, del: false, search: true, refresh: true
            },
            {}, //edit options
            {}, //add options
            {}, //del options
            {multipleSearch: true} // search options
            );

            $('#SelectListCurso').change(function () {
                jQuery("#gridModuloTurno").trigger("reloadGrid");
            });
        });
    </script>
</asp:Content>
