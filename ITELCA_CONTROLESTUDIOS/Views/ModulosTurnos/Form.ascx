﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ITELCA_CONTROLESTUDIOS.Models.ViewModels.ModuloTurnoViewModel>" %>
       <div class ="span-24 last">
            <div class="span-8 column last">
                 <div class="editor-label">
                    <%: Html.LabelFor(model => model.modulo_turno.MODULO_ID)%>
			    </div>
                <div class="editor-field">
				    <%: Html.ListBoxFor(model => model.modulo_turno.MODULO_ID, Model.modulos, new { @size = 3, @class = "multiselect", title = "Click para seleccionar una opción" })%>
				    <%: Html.ValidationMessageFor(model => model.modulo_turno.MODULO_ID)%>
			    </div> 
            </div>
        </div>
       <div class ="span-24 last">
         <div class="span-8 column last">
             <div class="editor-label">
					<%: Html.LabelFor(model => model.modulo_turno.TURNO_ID)%>
			</div>
            <div class="editor-field">
				<%: Html.ListBoxFor(model => model.modulo_turno.TURNO_ID, Model.turnos, new { @size = 3, @class = "multiselect", title = "Click para seleccionar una opción" })%>
				    <%: Html.ValidationMessageFor(model => model.modulo_turno.TURNO_ID)%>
			</div> 
        </div>
       </div>
       <div class="span-24 last">
        <div class="span-8 column last">
                <%: Html.EditorFor(model => model.modulo_turno.NRO_CLASES)%> 
            </div>
        </div>
         

        
      