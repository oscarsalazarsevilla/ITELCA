﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ITELCA_CONTROLESTUDIOS.Models.ViewModels.ModuloTurnoViewModel>" %>
       <div class ="span-24 last">
            <div class="span-8 column last">
                 <div class="display-label">
                    <%: Html.LabelFor(model => model.modulo_turno.MODULO_ID)%>
			    </div>
                <div class="display-field">
				    <%: Model.modulo_turno.MODULOS.NOMBRE%>
			    </div> 
            </div>
        </div>
       <div class ="span-24 last">
         <div class="span-8 column last">
             <div class="display-label">
					<%: Html.LabelFor(model => model.modulo_turno.TURNO_ID)%>
			</div>
            <div class="display-field">
				<%: Model.modulo_turno.TURNOS.NOMBRE%>
			</div> 
        </div>
       </div>
       <div class="span-24 last">
            <div class="span-8 column last">
                <%: Html.EditorFor(model => model.modulo_turno.NRO_CLASES)%> 
            </div>
        </div>
         

        
      