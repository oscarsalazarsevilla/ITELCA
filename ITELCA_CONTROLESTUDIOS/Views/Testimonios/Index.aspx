﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Web.Master" Inherits="System.Web.Mvc.ViewPage<ITELCA_CONTROLESTUDIOS.Models.ViewModels.TestimonioViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    [Itelca - Testimonios]
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2>
        Testimonios</h2>
    <div id="pag">

        <%
            int auxiliar = Model.testimonio_vector.Length;
            while (auxiliar % 5 != 0)
            {
                auxiliar = auxiliar - 1;
            }
            int paginas = (auxiliar / 5) + 1;
             %>
        <div class="page-previous-outer">
            <div class="page-previous-inner">
                <% 
                    int aux = 0;
                    for (int i = 1; i <= paginas; i++)
                    {
                %>
                <div style="margin-left:0px; padding-right:0px;">
                    <ul>
                        <% 
                       for (int j = aux; j < aux + 5; j++)
                       {
                           if (Model.testimonio_vector.Length > j)
                           {%>
                        <div>
                            <li style="list-style: none"><em style="color: Blue;">
                                <%=Model.testimonio_vector[j].NOMBRE+":"%>
                            </em>
                                <br />
                                <p>
                                    <%=Model.testimonio_vector[j].TEXTO%>
                                </p>
                            </li>
                        </div>
                        <%} %>
                        <%
                           }
                            aux = aux + 5;
                        %>
                    </ul>
                </div>
                <%} %>
            </div>
        </div>
                <ul class="paginator">
            <%
                
                for (int i = 1; i <= paginas; i++)
                {
            %>
            <li><a page="<%=i %>" href="">
                <%=i %></a></li>
            <%   
               }
            %>
        </ul>
    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CssContent" runat="server">
    <link rel="stylesheet" href="/Content/Css/style-pag.css" type="text/css" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="JSContent" runat="server">
    <script type="text/javascript" src="/Scripts/jquery-1.4.2.min.js"></script>
    <script type="text/javascript" src="/Scripts/pagePreview.js"></script>
</asp:Content>
