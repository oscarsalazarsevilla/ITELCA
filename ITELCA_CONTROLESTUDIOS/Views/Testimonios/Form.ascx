﻿   <%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ITELCA_CONTROLESTUDIOS.Models.ViewModels.TestimonioViewModel>" %>
        <div class ="span-24 last">
        <div class="span-8 column ">
             <div class="editor-label">
					<%: Html.LabelFor(model => model.testimonio.NOMBRE)%>
			</div>
            <div class="editor-field">
				<%: Html.TextBoxFor(model => model.testimonio.NOMBRE)%>
				<%: Html.ValidationMessageFor(model => model.testimonio.NOMBRE)%>
			</div> 
        </div>
    </div>
    <div class ="span-24 column last">
        <div class="span-24 column last">
             <div class="editor-label">
				<%: Html.LabelFor(model => model.testimonio.TEXTO)%>
			</div>
            <div class="editor-field">
				<%: Html.TextAreaFor(model => model.testimonio.TEXTO)%>
				<%: Html.ValidationMessageFor(model => model.testimonio.TEXTO)%>
			</div> 
        </div>
   </div>
   
   
     