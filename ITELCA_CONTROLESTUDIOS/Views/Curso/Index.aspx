﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Web.Master" Inherits="System.Web.Mvc.ViewPage<ITELCA_CONTROLESTUDIOS.Models.ViewModels.CursoViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    index
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="div-cursos">
        <h2 class="titulo-cursos">
            Cursos</h2>
        <%
            List<string> tipos_cursos = new List<string>();
            String[] vector, cursos = null;
            bool bandera;
            foreach (var item in Model.ListaCursos)
            {

                vector = item.Split('*');
                if (vector.Length > 1)
                {
                    cursos = vector[1].Split('|');

                }
        %>

        <div class="descripcion">
            <ul class="viñeta">
                <div class="titulo-tipo">
                    <%=vector[0]%>
                </div>
                <%
                if (cursos != null)
                {
                    for (int i = 0; i < cursos.Length; i++)
                    {
                        if (cursos[i].CompareTo("") != 0)
                        {
                %>
                <li id="lista">
                    <%=cursos[i].Trim()%>
                </li>
                <%      }
                    }
                }
                %>
            </ul>
            <a class="descripcion-tipo" href="">Leer Mas
                <img src="/Content/Imagenes/flecha_mas.png" />
            </a>
        </div>
        <%        
            }
        %>

    </div>
    <ul class="paginator">
        <li class="previous disabled"><a style="text-decoration:none" href="#">&larr; Anterior</a></li>
        <li class="next"><a style="text-decoration:none" href="#">Siguiente &rarr;</a></li>
    </ul>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CssContent" runat="server">
    <link rel="stylesheet" href="/Content/Css/style-pag.css" type="text/css" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="JsContent" runat="server">
     <script type="text/javascript" src="/Scripts/jquery-1.4.2.min.js"></script>
    <script type="text/javascript" src="/Scripts/pagePreview.js"></script>
</asp:Content>
