﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ITELCA_CONTROLESTUDIOS.Models.ViewModels.CursoViewModel>" %>
    <div class ="span-24 column last">
        <div class="span-8 column">
             <div class="editor-label">
					<%: Html.LabelFor(model => model.curso.TIPO_CURSO_ID)%>
			</div>
            <div class="editor-field">
				<%: Html.DropDownListFor(model => model.curso.TIPO_CURSO_ID,Model.tipoCurso,"--Seleccione--")%>
				<%: Html.ValidationMessageFor(model => model.curso.TIPO_CURSO_ID)%>
			</div> 
        </div>
        <div class="span-8 column">
             <div class="editor-label">
					<%: Html.LabelFor(model => model.curso.NOMBRE)%>
			</div>
            <div class="editor-field">
				<%: Html.TextBoxFor(model => model.curso.NOMBRE)%>
				<%: Html.ValidationMessageFor(model => model.curso.NOMBRE)%>
			</div> 
        </div>
        <div class="span-8 column last">
             <div class="editor-label">
					<%: Html.LabelFor(model => model.curso.CODIGO)%>
			</div>
            <div class="editor-field">
				<%: Html.TextBoxFor(model => model.curso.CODIGO)%>
				<%: Html.ValidationMessageFor(model => model.curso.CODIGO)%>
			</div> 
        </div>
    </div>
    <div class ="span-24 column last">
        <div class="span-8 column ">
             <div class="editor-label">
					<%: Html.LabelFor(model => model.curso.DESCRIPCION)%>
			</div>
            <div class="editor-field">
				<%: Html.TextAreaFor(model => model.curso.DESCRIPCION)%>
				<%: Html.ValidationMessageFor(model => model.curso.DESCRIPCION)%>
			</div> 
        </div>
        <div class="span-8 column">
            <div class="editor-label"> <%: Html.Label("Talleres") %> </div>
            <%: Html.DropDownList("tallerMultiselect", Model.listaTalleres, new { multiple = "multiple" })%>
        </div>
        <div class="span-8 column last ">
             <div class="editor-label">
					<%: Html.LabelFor(model => model.curso.NRO_TALLERES)%>
			</div>
            <div class="editor-field">
				<%: Html.TextBoxFor(model => model.curso.NRO_TALLERES, new  { @text="0" })%>
				<%: Html.ValidationMessageFor(model => model.curso.NRO_TALLERES)%>
			</div> 
        </div>
    </div>
    <div class ="span-24 column last">
       <div class="span-8 column">
             <div class="editor-label">
					<%: Html.LabelFor(model => model.curso.DURACION)%>
			</div>
            <div class="editor-field">
				<%: Html.DropDownListFor(model => model.curso.DURACION,Model.duracion,"-- Seleccione --")%>
				<%: Html.ValidationMessageFor(model => model.curso.DURACION)%>
			</div> 
        </div>
        <div class="span-8 column">
             <div class="editor-label">
					<%: Html.LabelFor(model => model.curso.DURACION_HORAS)%>
			</div>
            <div class="editor-field">
				<%: Html.TextBoxFor(model => model.curso.DURACION_HORAS, Model.duracion)%>
				<%: Html.ValidationMessageFor(model => model.curso.DURACION_HORAS)%>
			</div> 
        </div>
        <div class="span-8 column last">
            &nbsp;
        </div>
    </div>
   </div>   