﻿   <%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ITELCA_CONTROLESTUDIOS.Models.ViewModels.AsistenciaTallerViewModel>" %>
         
<div class="span-24 column last">
    <div class="span-8 column">
            <div class="editor-label">
			    <%: Html.Label("Sedes")%>
		    </div>
        <div class="editor-field">
			<%: Html.DropDownList("sede", Model.sede, "--Seleccione--")%>
			<%: Html.ValidationMessage("sede")%>
		</div> 
    </div>  
       <div class="span-8 column last">
            <div class="editor-label">
				<%: Html.Label("Taller")%>
			</div>
            <div class="editor-field">
				<%: Html.DropDownListFor(model => model.talleres, Model.talleres, "--Seleccione--")%>
				<%: Html.ValidationMessageFor(model => model.talleres)%>
			</div> 
        </div>
  </div>
<div class="span-24 column last">
    <div class="span-8 column last">
        <div class="editor-label">
		    <%: Html.Label("Profesores")%>
	    </div>
        <div class="editor-field">
		    <%: Html.DropDownList("profesor", Model.profesor, "--Seleccione--")%>
		    <%: Html.ValidationMessage("profesor")%>
	    </div> 
    </div>
</div>