﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<ITELCA_CONTROLESTUDIOS.Models.ViewModels.AsistenciaTallerViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Index
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<div class="span-22 append-1 prepend-1 column last ui-widget-content ui-corner-all" style="min-height:300px">
	<% using (Html.BeginForm())
    { %>
	<% Html.EnableClientValidation(); %>
    <%:Html.ValidationSummary("Error en la aplicación")%>
            <span class="introForm">Asistencia Taller->Index</span>
            <div class="modal" id="cargando"><!-- Place at bottom of page --></div>
             <div class="span-24 column last Formulario">
                  <div class="span-24 column last Formulario">
                    <%if (Roles.IsUserInRole("PROFESOR"))
                      { %>
                      <%Html.RenderPartial("FormProf"); %>
                    <%}
                      else
                      { %>
                        <%Html.RenderPartial("Form"); %>
                    <%} %>       
              </div>
            <div class="separator column span-24"></div>
            <div class="column span-24 last">   
                <input type="button" value="Guardar" class="boton" id="guardar" />
            </div>
        <div class="column span-24 last">   
            <fieldset>
                
                <table id="tablaAsistencia"  >
                    <thead><tr><th>Alumno</th></tr>
                    </thead>
                 </table>
             </fieldset>
           
        </div>
        <%} %>
        </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="CssContent" runat="server">
    <link href="/Content/Css/itelca/demo_table_jui.css" rel="stylesheet" type="text/css" />
</asp:Content> 

<asp:Content ID="Content4" ContentPlaceHolderID="JSContent" runat="server">
    <script src="/Scripts/jquery.dataTables.js" type="text/javascript"></script>
    <script src="/Scripts/Views/asistenciaTaller.js" type="text/javascript"></script>
</asp:Content>
