﻿   <%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ITELCA_CONTROLESTUDIOS.Models.ViewModels.AsistenciaTallerViewModel>" %>
         
  <div class="span-24 column last">
         <div class="span-16 column">
            <div class="editor-label">
				<%: Html.Label("Taller")%>
			</div>
            <div class="editor-field">
				<%: Html.DropDownListFor(model => model.talleres, Model.talleres, "--Seleccione--")%>
				<%: Html.ValidationMessageFor(model => model.talleres)%>
			</div> 
        </div>
  </div>
<div class="span-24 column last">
    <h4>PROFESOR:<%:Model.USUARIO.NOMBRE%> <%:Model.USUARIO.APELLIDO %></h4>
</div>