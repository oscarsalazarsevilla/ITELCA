﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ITELCA_CONTROLESTUDIOS.Models.ViewModels.TipoCursoViewModel>" %>
       <div class ="span-24 last">
        <div class="span-8 column">
             <div class="editor-label">
					<%: Html.LabelFor(model => model.tipoCurso.NOMBRE)%>
			</div>
            <div class="editor-field">
				<%: Html.TextBoxFor(model => model.tipoCurso.NOMBRE)%>
				<%: Html.ValidationMessageFor(model => model.tipoCurso.NOMBRE)%>
			</div> 
        </div>
    </div>
     <div class ="span-24 last">
        <div class="span-8 column">
             <div class="editor-label">
					<%: Html.LabelFor(model => model.tipoCurso.DESCRIPCION)%>
			</div>
            <div class="editor-field">
				<%: Html.TextAreaFor(model => model.tipoCurso.DESCRIPCION)%>
				<%: Html.ValidationMessageFor(model => model.tipoCurso.DESCRIPCION)%>
			</div> 
        </div>
    </div>
     