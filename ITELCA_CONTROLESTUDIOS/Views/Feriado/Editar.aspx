﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<ITELCA_CONTROLESTUDIOS.Models.ViewModels.FeriadoViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    [Itelca - Feriados]
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
<div class="span-22 append-1 prepend-1 column last ui-widget-content ui-corner-all">
	<span class="introForm">Feriados > Editar</span>
		<% using (Html.BeginForm("Editar","Feriado",FormMethod.Post, new { id = "formFeriado" })){%>
		<% Html.EnableClientValidation(); %>
        <%:Html.ValidationSummary("Error en la aplicación") %>
        <%:Html.HiddenFor(model => model.FERIADOS.FERIADO_ID) %>
        <%:Html.HiddenFor(model => model.FERIADOS.USUARIO_ID)%>
        <%:Html.HiddenFor(model => model.FERIADOS.PERIODICO) %>
            <div  class="span-24 Formulario  column last">
				<% Html.RenderPartial("Form",Model); %>
                <div class ="span-24 last">
                    <div class="span-8 column last">
                        <div class="editor-label">
				            <%: Html.LabelFor(model => model.FERIADOS.ESTADO)%>
			            </div>
                        <div class="editor-field">
				            <%: Html.DropDownListFor(model => model.FERIADOS.ESTADO, Model.activo)%>
				            <%: Html.ValidationMessageFor(model => model.FERIADOS.ESTADO)%>
			            </div> 
                    </div>
                </div>
			</div>
             <div class="separator column span-24">
            </div>
            <p>
				<input type="submit" value="Guardar" class="button" />
			</p>
           
            <div class="column span-24">
                <%: Html.ActionLink("Volver al listado", "Listar")%>
            </div>
         
			
    <% } %>
</div>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="CssContent" runat="server">
 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="JsContent" runat="server">
 <script type="text/javascript">
     $(document).ready(function () {

         $("#formFeriado").submit(function (event) {
             if ($('#ANUAL').is(":checked")) {
                 $("#FERIADOS_PERIODICO").val(1);
             } else {
                 $("#FERIADOS_PERIODICO").val(0);
             }
         });

     });

 </script>
</asp:Content>

