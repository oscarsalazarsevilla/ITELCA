﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Web2.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Listar
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<div class="span-20 last" style="margin-left:auto; margin-right:auto;">
<h2>Lista de Tallleres</h2>


<fieldset>
<div class="span-20 last" style="margin-left:auto; margin-right:auto;">
        <p>
            <%: Html.ActionLink("Nuevo", "Inscripcion") %>
        </p>
        <div id="msg">
        <%
            try
            {

                if (TempData["mensaje"] != null && TempData["mensaje"]  != "")
                {%>
			    <div  class="ui-state-highlight ui-corner-all" style="margin-top: 10px; padding: 0 .3em; width:30%; height:20px;">
				    <span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
				    <div><%:TempData["mensaje"]  %></div>
			    </div>
		    
          <%
                }
            }
            catch{} 
            
           %>
        </div>
            <table id="gridMatriculaTaller">
            </table>
            <div id="pager"></div>
            <div id="dialogTaller">
                <div id="detalleCurso">
                    <div id="tabs">
                        <ul>
                            <li><a href="#tabs-1">Pagos</a></li>
                        </ul>
                        <div id="tabs-1">
                            <p>Proin elit arcu, rutrum commodo, vehicula tempus, commodo a, risus. Curabitur nec arcu. Donec sollicitudin mi sit amet mauris. Nam elementum quam ullamcorper ante. Etiam aliquet massa et lorem. Mauris dapibus lacus auctor risus. Aenean tempor ullamcorper leo. Vivamus sed magna quis ligula eleifend adipiscing. Duis orci. Aliquam sodales tortor vitae ipsum. Aliquam nulla. Duis aliquam molestie erat. Ut et mauris vel pede varius sollicitudin. Sed ut dolor nec orci tincidunt interdum. Phasellus ipsum. Nunc tristique tempus lectus.</p>
                        </div>
                    </div>
                </div>
            </div>
            </div>
</fieldset>
 </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="CssContent" runat="server">
    <link href="/Content/Css/ui.jqgrid.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="JSContent" runat="server">
    <script src="/Scripts/grid.locale-es.js" type="text/javascript"></script>
    <script src="/Scripts/jquery.jqGrid.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#tabs").tabs();
            $('#dialogTaller').dialog({
                height: 500,
                width: 'auto',
                modal: true,
                autoOpen: false
            });
            var grid = $("#gridMatriculaTaller").jqGrid({
                url: '/MatriculaTaller/ObtenerData',
                datatype: 'json',
                mtype: 'POST',
                colNames: ['Id', 'Taller', 'Sede', 'Fecha Inicio','Hora', 'Condición', 'Ver'],
                colModel: [
		            { name: 'OFERTA_CURSO_ID', index: 'OFERTA_CURSO_ID', align: "right", width: 100, resizable: true, searchoptions: { sopt: ['cn']} },
		            { name: 'CURSOS.NOMBRE', index: 'AULA_NOMBRE', width: 200, searchoptions: { sopt: ['cn']} },
                    { name: 'SEDES.NOMBRE', index: 'TURNO_NOMBRE', width: 200, searchoptions: { sopt: ['cn']} },
                    { name: 'TURNOS.NOMBRE', index: 'MODULO_NOMBRE', width: 200, searchoptions: { sopt: ['cn']} },
		            { name: 'FECHA_INSCRIPCION', index: 'MODULO_NOMBRE', width: 200, searchoptions: { sopt: ['cn']} },
		             { name: 'CONDICION', index: 'CONDICION', width: 200, searchoptions: { sopt: ['cn']} },
		            { name: 'Action2', index: 'Action2', align: "left", width: 100, resizable: false, sortable: false, search: false}],
                pager: jQuery('#pager'),
                rowNum: 500,
                rowList: [500, 1000, 5000],
                sortname: 'OFERTA_CURSO_ID',
                sortorder: 'desc',
                autowidth: true,
                viewrecords: true,
                caption: 'Listado de Talleres',
                loadComplete: function () {
                    $(".ui-icon-circle-plus").click(function () {
                        var gr = $(this).attr("id");
                        $('#dialogTaller').dialog('open');
                        //                        if (r == true) {
                        //                            $.post("/MatriculaCurso/CargarDataCurso", { id: gr }, function (data) {
                        //                                var dat = ' <div  class="ui-state-highlight ui-corner-all" style="margin-top: 10px; padding: 0 .3em; width:30%; height:20px;"><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span><div>';
                        //                                dat += data[0].msg;
                        //                                dat += '</div></div>';
                        //                                $("#msg").empty().html(dat);
                        //                                $("#msg").fadeOut(3000);
                        //                                grid.trigger('reloadGrid');

                        //                            });
                        //                        }
                        //                        else {
                        //                            x = "Cancelar";
                        //                        }
                        // alert("adas");
                    });

                }
            }).navGrid('#pager',
            {
                edit: false, add: false, del: false, search: true, refresh: false
            },
            {}, //edit options
            {}, //add options
            {}, //del options
            {multipleSearch: true} // search options
            );
        });
    </script>
</asp:Content>
