﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ITELCA_CONTROLESTUDIOS.Models.ViewModels.MatriculaTallerViewModel>" %>
            <div id="inscripcion" >
                <h2>Taller</h2>
                <section>
                    <div class ="span-24 last">
                        <div class="span-16 column ">
                            <div class="editor-label">
				                <%: Html.Label("Talleres")%>
			                </div>
                            <div class="editor-field">
				                <%: Html.DropDownListFor(model => model.MATRICULASTALLERES.OFERTA_TALLER_ID, Model.listaTaller,"--Seleccione--")%>
				                <%: Html.ValidationMessageFor(model => model.MATRICULASTALLERES.OFERTA_TALLER_ID)%>
			                </div> 
                        </div>

                </div>
                
                <div class="separator column span-24"></div>
                <div id="divTalleres" class="column span-24 last" style="height:300px;overflow:auto;">
                </div>
            </section>
                <h2>Pago</h2>
                <section> 
                    <div class ="span-24 last">
                        <div class="span-11 column ofertaI">
                            <h4>Monto</h4>
                            <h4 id="costo">0 Bs</h4>
                            <h4>Cuotas A Cancelar:</h4>
                            <select id="cuotas">
                            </select>
                            <h4>Total a Pagar:</h4>
                            <input id="total" />
                        </div>
                        <div class="span-12 column last">
                            <div id="div1" class="column span-10 prepend-2 last">
                                <div class="column span-4 "><input type="radio" name="pagoI" id="linea" alt="Pago en Linea" /></div>
                                <div class="column span-17 prepend-3 last "><img src="../../Content/Imagenes/pagoEnLinea.png" alt="Pago en linea" height="150px"/></div>
                            </div>
                            <div class="separator column span-24"></div>
                            <div id="div2" class="column span-10 prepend-2 last" >
                                <div class="column span-4 "><input type="radio" name="pagoI" id="taquilla" alt="Pago en Taquilla" /></div>
                                <div class="column span-20 last "><img src="../../Content/Imagenes/taquilla.png" alt="Taquilla" height="150px"/></div>
                            </div>
                        </div>
                    </div>
                </section>
                 <h2>Resumen</h2>
                <section id="resumen">
                </section>
            </div>
          
     <div class="separator column span-24"></div>
        <div class="column span-24">
            <%: Html.ActionLink("Volver al listado", "Listar")%>
        </div>