﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<ITELCA_CONTROLESTUDIOS.Models.ViewModels.OfertasModulosViewModel>" %>


<!DOCTYPE html>

<html>
<head id="Head1" runat="server">
    <title>Editar</title>
     <script type="text/javascript">
         var cursoOfertado = "<%:Model.OFERTASCURSOS.OFERTA_CURSO_ID%>";
    </script>
    <script src="/Scripts/Views/OfertaModulos.js" type="text/javascript"></script>
   
</head>
<body>
<div class="span-24 column last" style="background-color:Silver;">
    <div class="span-7 column append-1 "><strong>SEDE:</strong> <%:Model.OFERTASCURSOS.SEDES.NOMBRE%></div>
    <div class="span-7 column append-1 "><strong>CURSO:</strong> <%:Model.OFERTASCURSOS.CURSOS.NOMBRE%></div>
    <div class="span-7 column "><strong>TURNO:</strong> <%:Model.OFERTASCURSOS.TURNOS.NOMBRE%></div>
</div>
<div id="tabPlanModulo">
    <ul>
        <li><a href="#tabManual">Agregar módulo a planificación</a></li>
        <li><a href="#tabAuto">Agregar Auto Planificación</a></li>
    </ul>

    <div id="tabManual">
        <% using (Html.BeginForm("Modulo", "OfertaCurso", FormMethod.Post, new { id = "formOfertaModulo",@class="Formulario" }))
       {
     %>
         <%: Html.ValidationSummary(true)%>
        <% Html.EnableClientValidation(); %>
        <%:Html.Hidden("OFERTASMODULOS.CURSO_ID", Model.OFERTASCURSOS.CURSO_ID)%>
          <%:Html.Hidden("OFERTASMODULOS.OFERTA_CURSO_ID", Model.OFERTASCURSOS.OFERTA_CURSO_ID)%>
          <%:Html.Hidden("OFERTASMODULOS.SEDE_ID", Model.OFERTASCURSOS.SEDE_ID)%>
          <%:Html.Hidden("OFERTASMODULOS.TURNO_ID", Model.OFERTASCURSOS.TURNO_ID)%>
        <%:Html.Hidden("alt-date")%>

       <div class ="span-24 last">
            <div class="span-8 column last">
                <div class="editor-label">
				    <%: Html.LabelFor(model => model.OFERTASMODULOS.AULA_ID)%>
			    </div>
                <div class="editor-field">
				    <%: Html.DropDownListFor(model => model.OFERTASMODULOS.AULA_ID, Model.listaAulas, "--Seleccione--")%>
				    <%: Html.ValidationMessageFor(model => model.OFERTASMODULOS.AULA_ID)%>
			    </div> 
            </div>
            <div class="span-8 column ">
                <div class="editor-label">
				    <%: Html.LabelFor(model => model.OFERTASMODULOS.MODULO_ID)%>
			    </div>
                <div class="editor-field">
				    <%: Html.DropDownListFor(model => model.OFERTASMODULOS.MODULO_ID, Model.listaModulos, "--Seleccione--")%>
				    <%: Html.ValidationMessageFor(model => model.OFERTASMODULOS.MODULO_ID)%>
			    </div> 
            </div>
             <div class="span-8 column">
                     <div class="editor-label">
					        <%: Html.LabelFor(model => model.OFERTASMODULOS.FECHA_INICIO)%>
			        </div>
                    <div class="editor-field">
				        <%: Html.TextBoxFor(model => model.OFERTASMODULOS.FECHA_INICIO, new { @class = "datepickerM" })%>
				        <%: Html.ValidationMessageFor(model => model.OFERTASMODULOS.FECHA_INICIO)%>
			        </div> 
                </div> 
             <div class="span-8 column">
                 <div class="editor-label">
					    <%: Html.LabelFor(model => model.OFERTASMODULOS.FECHA_FIN)%>
			    </div>
                <div class="editor-field">
				    <%: Html.TextBoxFor(model => model.OFERTASMODULOS.FECHA_FIN, new { @class = "datepickerM" })%>
				    <%: Html.ValidationMessageFor(model => model.OFERTASMODULOS.FECHA_FIN)%>
			    </div> 
            </div>
               <div class="span-8 column ">
                 <div class="editor-label">
					    <%: Html.LabelFor(model => model.OFERTASMODULOS.PROXIMO_INICIO)%>
			    </div>
                <div class="editor-field">
				    <%: Html.TextBoxFor(model => model.OFERTASMODULOS.PROXIMO_INICIO, new { @class = "datepickerM" })%>
				    <%: Html.ValidationMessageFor(model => model.OFERTASMODULOS.PROXIMO_INICIO)%>
			    </div> 
            </div> 
              <div class="span-8 column last">
                 <div class="editor-label">
					    <%: Html.LabelFor(model => model.OFERTASMODULOS.MATRICULA_MINIMA)%>
			    </div>
                <div class="editor-field">
				    <%: Html.TextBoxFor(model => model.OFERTASMODULOS.MATRICULA_MINIMA, new { @min="5", @max="200"})%>
				    <%: Html.ValidationMessageFor(model => model.OFERTASMODULOS.MATRICULA_MINIMA)%>
			    </div> 
            </div>            
        </div>
         <div class="span-24 column last ">
              <div class="span-8 column ">
                 <div class="editor-label">
					    <%: Html.LabelFor(model => model.OFERTASMODULOS.PROFESOR_ID)%>
			    </div>
                <div class="editor-field">
				    <%: Html.DropDownListFor(model => model.OFERTASMODULOS.PROFESOR_ID, Model.listaProfesores,"--Seleccione--")%>
				    <%: Html.ValidationMessageFor(model => model.OFERTASMODULOS.PROFESOR_ID)%>
			    </div> 
            </div> 
             <div class="span-8 column last ">
            <p>
			    <input type="submit" value="Guardar" id="GuardarOfertaModulo3" class="button" />
		    </p>
        </div>     
         </div>
         
       
         <% } %>  
    </div>
    <div id="tabAuto">
        <% using (Html.BeginForm("Crear", "OfertaModulo", FormMethod.Post, new { id = "formOfertaAuto", @class = "Formulario" }))
       {
         %>
            <%: Html.ValidationSummary(true)%>
            <% Html.EnableClientValidation(); %>
            <%:Html.Hidden("AUTOFERTAMOD.CURSO_ID", Model.OFERTASCURSOS.CURSO_ID)%>
            <%:Html.Hidden("AUTOFERTAMOD.OFERTA_CURSO_ID", Model.OFERTASCURSOS.OFERTA_CURSO_ID)%>
            <%:Html.Hidden("AUTOFERTAMOD.SEDE_ID", Model.OFERTASCURSOS.SEDE_ID)%>
            <%:Html.Hidden("AUTOFERTAMOD.TURNO_ID", Model.OFERTASCURSOS.TURNO_ID)%>
            <div class ="span-24 last">
                <div class="span-8 column last">
                    <div class="editor-label">
				        <%: Html.LabelFor(model => model.AUTOFERTAMOD.AULA_ID)%>
			        </div>
                    <div class="editor-field">
				        <%: Html.DropDownListFor(model => model.AUTOFERTAMOD.AULA_ID, Model.listaAulas, "--Seleccione--")%>
				        <%: Html.ValidationMessageFor(model => model.AUTOFERTAMOD.AULA_ID)%>
			        </div> 
                </div>
                 <div class="span-8 column last ">
                     <div class="editor-label">
					        <%: Html.LabelFor(model => model.AUTOFERTAMOD.PROFESOR_ID)%>
			        </div>
                    <div class="editor-field">
				        <%: Html.DropDownListFor(model => model.AUTOFERTAMOD.PROFESOR_ID, Model.listaProfesores,"--Seleccione--")%>
				        <%: Html.ValidationMessageFor(model => model.AUTOFERTAMOD.PROFESOR_ID)%>
			        </div> 
                </div> 
                
            </div>
              <div class="span-24 column last ">
                 <div class="span-8 column last">
                     <div class="editor-label">
					        <%: Html.LabelFor(model => model.AUTOFERTAMOD.FECHA_INICIO)%>
			        </div>
                    <div class="editor-field">
				        <%: Html.TextBoxFor(model => model.AUTOFERTAMOD.FECHA_INICIO, new { @class = "datepickerMA" })%>
				        <%: Html.ValidationMessageFor(model => model.AUTOFERTAMOD.FECHA_INICIO)%>
			        </div> 
                </div> 
                  <div class="span-8 column">
                         <div class="editor-label">
					            <%: Html.LabelFor(model => model.AUTOFERTAMOD.MATRICULA_MINIMA)%>
			            </div>
                        <div class="editor-field">
				            <%: Html.TextBoxFor(model => model.AUTOFERTAMOD.MATRICULA_MINIMA, new { @min = "5", @max = "200" })%>
				            <%: Html.ValidationMessageFor(model => model.AUTOFERTAMOD.MATRICULA_MINIMA)%>
			            </div> 
                    </div>       
                <div class="span-8 column last ">
                <p>
			        <input type="submit" value="Guardar" id="autoGuardar" class="button" />
		        </p>
                </div>     
             </div>
     <%} %>
    </div>
</div>
<%--<fieldset>
<legend>Agregar módulo a planificación</legend>
 
</fieldset>
<fieldset>
<legend>Agregar Auto Planificación</legend>

</fieldset>--%>
 <div class="separator column span-24"></div>
 <div class="span-24 column last" style="background-color:Silver;">
    <div class="span-7 column append-1 "><strong>SEDE:</strong> <%:Model.OFERTASCURSOS.SEDES.NOMBRE%></div>
    <div class="span-7 column append-1 "><strong>CURSO:</strong> <%:Model.OFERTASCURSOS.CURSOS.NOMBRE%></div>
    <div class="span-7 column "><strong>TURNO:</strong> <%:Model.OFERTASCURSOS.TURNOS.NOMBRE%></div>
</div>
<fieldset>
    <legend>Planificar módulos</legend>
    <div  class="span-24 column last" id="ggg">
            
    <div  class="span-20 column">
        <div  class="span-20 column last">
             <div class="editor-label">
			    <%: Html.Label("Profesor")%>
		    </div>
            <div class="editor-field">
			    <%: Html.DropDownList("buscarGridProfesor", Model.listaProfesoresCurso)%>
		    </div> 
        </div>
         </br></br></br></br> 
         <div  class="span-24 column last">
            <table id="OfertasModulosGrid"></table>
            <div id="pagerModulo"></div> 
        </div> 
    </div>


     <div class="span-4 column last">
        <div class="editor-label">
			<%: Html.Label("Profesor")%>
		</div>
        <div class="editor-field">
			<%: Html.DropDownList("imprimirProfesor", Model.listaProfesoresCurso,"-- Seleccione --")%>
		</div> 
         <div class="editor-label">
			<%: Html.Label("Desde")%>
		</div>
        <div class="editor-field">
            <%: Html.TextBox("imprimirDesde", "",new { @class = "datepicker" })%>
		</div> 
        <input type="button" value="Imprimir" id="ImprimirBtn" class="button" />
   </div>

    <div id="dialog-message" title=""> </div>
    </div>
</fieldset>
</body>
</html>