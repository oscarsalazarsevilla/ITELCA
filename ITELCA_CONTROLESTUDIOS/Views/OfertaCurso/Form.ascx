﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ITELCA_CONTROLESTUDIOS.Models.ViewModels.OfertaCursoViewModel>" %>

   <fieldset>
        <legend>Crear Oferta Para Curso</legend>
          <div class ="span-24 column last">
            <div class="span-9 column ">
                <div class="editor-label">
				    <%: Html.LabelFor(model => model.OFERTASCURSOS.CURSO_ID)%>
			    </div>
                <div class="editor-field">
				    <%: Html.DropDownListFor(model => model.OFERTASCURSOS.CURSO_ID,Model.listaCursos,"--Seleccione--")%>
				    <%: Html.ValidationMessageFor(model => model.OFERTASCURSOS.CURSO_ID)%>
			    </div> 
            </div>

            <div class="span-9 column last ">
                <div class="editor-label">
				    <%: Html.LabelFor(model => model.OFERTASCURSOS.SEDE_ID)%>
			    </div>
                <div class="editor-field">
				    <%: Html.DropDownListFor(model => model.OFERTASCURSOS.SEDE_ID, Model.listaSedes,"--Seleccione--")%>
				    <%: Html.ValidationMessageFor(model => model.OFERTASCURSOS.SEDE_ID)%>
			    </div> 
            </div>
        </div>
       <div class ="span-24 column last">
            <div class="span-9 column">
            <div class="editor-label">
				<%: Html.LabelFor(model => model.OFERTASCURSOS.TURNO_ID)%>
			</div>
            <div class="editor-field">
				<%: Html.DropDownListFor(model => model.OFERTASCURSOS.TURNO_ID, Model.listaTurnos,"--Seleccione--")%>
				<%: Html.ValidationMessageFor(model => model.OFERTASCURSOS.TURNO_ID)%>
			</div> 
        </div>
        </div>
        <div class="span-24 column last ">
            <p>
			    <input type="submit" value="Guardar" class="button" />
		    </p>
        </div>
           
   </fieldset>   