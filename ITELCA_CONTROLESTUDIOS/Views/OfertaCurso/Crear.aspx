﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<ITELCA_CONTROLESTUDIOS.Models.ViewModels.OfertaCursoViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    [Itelca - Ofertas Curso]
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
<div class="span-22 append-1 prepend-1 column last ui-widget-content ui-corner-all">
	<span class="introForm">Oferta Curso > Planificación Curso</span>
		<% using (Html.BeginForm("Crear","OfertaCurso",FormMethod.Post, new { id = "formOfertaCurso",@class="Formulario" })){%>
		<% Html.EnableClientValidation(); %>
        <%:Html.ValidationSummary("Error en la aplicación") %>
            <div  class="span-24 Formulario  column last">
				<% Html.RenderPartial("Form",Model); %>
			</div>
             <div id="msg" class="span-24 column last">
                <%
                    try
                    {

                        if (TempData["mensaje"] != null && TempData["mensaje"]  != "")
                        {%>
			            <div  class="ui-state-highlight ui-corner-all" style="margin-top: 10px; padding: 0 .3em; width:30%; height:20px;">
				            <span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
				            <div><%:TempData["mensaje"]  %></div>
			            </div>
		    
                  <%
                        }
                    }
                    catch{} 
            
                   %>
        </div>
        <div class="span-24 column last">
            <fieldset>
                <legend>Planificar módulos</legend>
                  <div  class="span-24 column last" id="ggg">
                    <div class="editor-label">
				        <%: Html.Label("Curso")%>
			        </div>
                    <div class="editor-field">
				        <%: Html.DropDownList("cursoListaGrid", Model.listaCursosGrid)%>
			        </div> 
                </div>
               
                 <div class="separator column span-24"></div>
             <div  class="span-24 column last">
                <table id="OfertacursoListaGrid"></table>
                <div id="pager"></div> 
             </div>
            </fieldset>
        </div>
          <div id="dialogForm" title="Nuevo Módulo">         
            <div id="contenidoHtml" class="span-24 column last">
            </div>
          </div>

			
    <% } %>
      
</div>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="CssContent" runat="server">
    <link href="/Content/Css/ui.jqgrid.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="JsContent" runat="server">
    <script src="/Scripts/grid.locale-es.js" type="text/javascript"></script>
    <script src="/Scripts/jquery.jqGrid.min.js" type="text/javascript"></script>
    <script src="/Scripts/jquery.maskedinput-1.2.2.js" type="text/javascript"></script>
     <script src="/Scripts/autoNumeric.js" type="text/javascript"></script>
    <script src="/Scripts/Views/OfertaCurso.js" type="text/javascript"></script>
 
</asp:Content>
