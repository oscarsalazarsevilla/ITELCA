﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<ITELCA_CONTROLESTUDIOS.Models.ViewModels.OfertaCursoViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    [Itelca - Ofertas Cursos]
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
<div class="span-22 append-1 prepend-1 column last ui-widget-content ui-corner-all">
	<span class="introForm">Ofertas Cursos > Editar</span>
		<% using (Html.BeginForm()) { %>
		<% Html.EnableClientValidation(); %>
        <%:Html.ValidationSummary("Error en la aplicación") %>
        <%:Html.HiddenFor(model => model.OFERTASCURSOS.OFERTA_CURSO_ID) %>
        <%:Html.HiddenFor(model => model.OFERTASCURSOS.USUARIO_ID)%>
            <div  class="span-24 Formulario  column last">
				<% Html.RenderPartial("Form",Model); %>

			</div>
             <div class="separator column span-24">
            </div>
            <table id="BankTransactions"></table>
             <div id="pager"></div> 
            <p>
				<input type="submit" value="Guardar" class="button" />
			</p>
           
            <div class="column span-24">
                <%: Html.ActionLink("Volver al listado", "Listar")%>
            </div>
         
			
    <% } %>
     <div class=" prepend-2 separator column span-22" style="height:300px;" >
                <div id='map_canvas' style='width:90%; height:300px;'></div>
            </div>
</div>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="CssContent" runat="server">
    <link href="/Content/Css/ui.jqgrid.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="JsContent" runat="server">
     <script src="/Scripts/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
     <script src="/Scripts/jquery.maskedinput-1.2.2.js" type="text/javascript"></script>
    <script src="/Scripts/jquery.jqGrid.min.js" type="text/javascript"></script>

    </asp:Content>

