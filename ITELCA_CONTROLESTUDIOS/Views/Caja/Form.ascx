﻿   <%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ITELCA_CONTROLESTUDIOS.Models.ViewModels.CajaViewModel>" %>
       <div class ="span-24 last">
            <div class="span-8 column last">
                 <div class="editor-label">
					    <%: Html.LabelFor(model => model.caja.SEDE_ID)%>
			    </div>
                <div class="editor-field">
				    <%: Html.DropDownListFor(model => model.caja.SEDE_ID,Model.sedes,"--Seleccione--")%>
				    <%: Html.ValidationMessageFor(model => model.caja.SEDE_ID)%>
			    </div> 
            </div>
       </div>
   
   <div class ="span-24 last">
        <div class="span-8 column last ">
             <div class="editor-label">
					<%: Html.LabelFor(model => model.caja.CODIGO)%>
			</div>
            <div class="editor-field">
				<%: Html.TextBoxFor(model => model.caja.CODIGO)%>
				<%: Html.ValidationMessageFor(model => model.caja.CODIGO)%>
			</div> 
        </div>
   </div>

    <div class ="span-24 last">
        <div class="span-8 column last">
             <div class="editor-label">
					<%: Html.LabelFor(model => model.caja.NOMBRE)%>
			</div>
            <div class="editor-field">
				<%: Html.TextBoxFor(model => model.caja.NOMBRE)%>
				<%: Html.ValidationMessageFor(model => model.caja.NOMBRE)%>
			</div> 
        </div>
    </div>
    <div class ="span-24 last">
        <div class="span-8 column last">
             <div class="editor-label">
					<%: Html.LabelFor(model => model.caja.DESCRIPCION)%>
			</div>
            <div class="editor-field">
				<%: Html.TextBoxFor(model => model.caja.DESCRIPCION)%>
				<%: Html.ValidationMessageFor(model => model.caja.DESCRIPCION)%>
			</div> 
        </div>
    </div>
    
     