﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<ITELCA_CONTROLESTUDIOS.Models.FileListingViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Browse
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2>Browse</h2>

<div>
        
        <% foreach (var item in Model.Files) {%>
            <a href="<%:item.FileName %>" style="font-size: larger;" class="contenedorImagenes column">
                <img alt="" src="/Uploads/<%:item.FileName %>"/>
            </a>     
        <% } %>
        <%: Html.HiddenFor(m => m.CKEditorFuncNum)%>
    </div>

</asp:Content>



<asp:Content ID="Content4" ContentPlaceHolderID="CssContent" runat="server">
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="JsContent" runat="server">
    <script type="text/javascript">
        $(function () {
            $('a').click(function (e) {
                e.preventDefault();
                var ckEditorNum = parseInt($('#CKEditorFuncNum').val());
                window.opener.CKEDITOR.tools.callFunction(ckEditorNum, '/uploads/' + $(this).attr('href'), '');
                window.close();
            });
        });
    </script>
</asp:Content>
