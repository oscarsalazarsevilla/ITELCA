﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
  [ITELCA - Recuperar Contraseña]
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">

<div class="span-23 column last ui-widget-content ui-corner-all">
   <summary> <h2>Recuperar Contraseña</h2></summary> 
             <div class="span-20 prepend-2  column last">
                <% using (Html.BeginForm()) { %>
                <%: Html.ValidationSummary()%>   
                 <div class="Formulario">   
                        <div class="span-24 last button-padding-top">
                            <div class="editor-label">
                                <%:Html.Label("Nombre d usuario") %>
                            </div>
                             <div class="editor-field">
                                <%: Html.TextBox("UserName", "", new { @class = "input-background short" })%>
                             </div>
                        </div>
                        <div class="button-padding-top">
                            <input type="submit" class="button" value="Enviar" />
                        </div>
                  </div>

                <% } %>
             
            </div>
</div>


</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="CssContent" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="JsContent" runat="server">
</asp:Content>
