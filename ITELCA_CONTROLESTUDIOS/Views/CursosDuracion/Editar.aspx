﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<ITELCA_CONTROLESTUDIOS.Models.ViewModels.CursosDuracionViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    [Itelca - Duración Cursos]
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
<div class="span-22 append-1 prepend-1 column last ui-widget-content ui-corner-all">
	<span class="introForm">Duración Cursos > Editar</span>
		<% using (Html.BeginForm("Editar", "CursosDuracion", FormMethod.Post, new { id = "formCursosDuracion" }))
     { %>
		<% Html.EnableClientValidation(); %>
        <%:Html.ValidationSummary("Error en la aplicación") %>
            <div  class="span-24 Formulario  column last">
				<% Html.RenderPartial("FormEdit",Model); %>
              
			</div>
             <div class="separator column span-24">
            </div>
            <p>
				<input type="submit" value="Guardar" class="button" />
			</p>
           
            <div class="column span-24">
                <%: Html.ActionLink("Volver al listado", "Listar")%>
            </div>
         
			
    <% } %>
</div>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="CssContent" runat="server">
 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="JsContent" runat="server">
 <script src="/Scripts/autoNumeric.js" type="text/javascript"></script>
 <script src="/Scripts/Views/CursosDuracion.js" type="text/javascript"></script>

 <script type="text/javascript">
     $("#CURSOSDURACIONES_CURSO_ID").prop('disabled', true);
     $("#CURSOSDURACIONES_TIPOTURNO_ID").prop('disabled', true);
 </script>
</asp:Content>

