﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ITELCA_CONTROLESTUDIOS.Models.ViewModels.CursosDuracionViewModel>" %>

        <div class ="span-24 column last">
             <div class="span-8 column last">
                <div class="editor-label">
				    <%: Html.LabelFor(model => model.CURSOSDURACIONES.CURSO_ID)%>
			    </div>
                <div class="editor-field">
				    <%: Html.DropDownListFor(model => model.CURSOSDURACIONES.CURSO_ID, Model.ListaCursos, "--Seleccione--")%>
				    <%: Html.ValidationMessageFor(model => model.CURSOSDURACIONES.CURSO_ID)%>
			    </div> 
            </div>  
        </div>

        <div class ="span-24 column last">
            <div class="span-8 column last">
                <div class="editor-label">
				    <%: Html.LabelFor(model => model.CURSOSDURACIONES.TIPOTURNO_ID)%>
			    </div>
                <div class="editor-field">
				    <%: Html.DropDownListFor(model => model.CURSOSDURACIONES.TIPOTURNO_ID, Model.ListaTurnos, "--Seleccione--")%>
				    <%: Html.ValidationMessageFor(model => model.CURSOSDURACIONES.TIPOTURNO_ID)%>
			    </div> 
            </div>       
        </div>

        <div class ="span-24 column last">
            <div class="span-8 column last">
                 <div class="editor-label">
				        <%: Html.LabelFor(model => model.CURSOSDURACIONES.NRO_MESES)%>
			    </div>
                <div class="editor-field">
				    <%: Html.TextBoxFor(model => model.CURSOSDURACIONES.NRO_MESES)%>
				    <%: Html.ValidationMessageFor(model => model.CURSOSDURACIONES.NRO_MESES)%>
			    </div> 
            </div>  
        </div>   
        
      