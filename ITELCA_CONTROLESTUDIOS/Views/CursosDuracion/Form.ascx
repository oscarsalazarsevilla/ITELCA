﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ITELCA_CONTROLESTUDIOS.Models.ViewModels.CursosDuracionViewModel>" %>

        <div class ="span-24 column last">
             <div class="span-8 column last">
                <div class="editor-label">
				    <%: Html.LabelFor(model => model.CURSOSDURACIONES.CURSO_ID)%>
			    </div>
                <div class="editor-field">
				    <%: Html.ListBoxFor(model => model.CURSOSDURACIONES.CURSO_ID, Model.MultiCursos, new { @size = 3, @class = "multiselect", title = "Click para seleccionar una opción" })%>
				    <%: Html.ValidationMessageFor(model => model.CURSOSDURACIONES.CURSO_ID)%>
			    </div> 
            </div>  
        </div>
        <div class ="span-24 column last">
            <div class="span-8 column last">
                 <div class="editor-label">
					    <%: Html.LabelFor(model => model.CURSOSDURACIONES.TIPOTURNO_ID)%>
			    </div>
                <div class="editor-field">
				    <%: Html.ListBoxFor(model => model.CURSOSDURACIONES.TIPOTURNO_ID, Model.MultiTurnos, new { @size = 3, @class = "multiselect", title = "Click para seleccionar una opción" })%>
				    <%: Html.ValidationMessageFor(model => model.CURSOSDURACIONES.TIPOTURNO_ID)%>
			    </div> 
            </div>
        </div>

        <div class ="span-24 column last">
            <div class="span-8 column last">
                 <div class="editor-label">
				        <%: Html.LabelFor(model => model.CURSOSDURACIONES.NRO_MESES)%>
			    </div>
                <div class="editor-field">
				    <%: Html.TextBoxFor(model => model.CURSOSDURACIONES.NRO_MESES)%>
				    <%: Html.ValidationMessageFor(model => model.CURSOSDURACIONES.NRO_MESES)%>
			    </div> 
            </div>  
        </div>   
        
      