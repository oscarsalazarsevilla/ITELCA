﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    [Itelca - Interfaz People Soft]
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<div id="hero" class="span-22 append-1 prepend-1 column last alignCenter ui-widget-content ui-corner-all">
    <h1>Seleccione el ícono que se le presenta abajo para ejecutar la interfaz con PeopleSoft</h1>
    <div class="separator"></div>
    <div class="span-12 append-6 prepend-6 column">
        <fieldset>
            <legend>Interfaz</legend>
            <div id="k2QuickIcons">
                <div class="icon-wrapper">
                    <div class="icon">
                        <a href="<%= Url.Action("GenerarCorridaInterfaz","Interfaces") %>">
                            <img src="/Content/Imagenes/interface.png" alt="Ejecutar interfaz" />
                            <span>Ejecutar</span>
                        </a>
                    </div>
                </div>
            </div>
        </fieldset>
    </div>
</div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="CssContent" runat="server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="JSContent" runat="server">
</asp:Content>
