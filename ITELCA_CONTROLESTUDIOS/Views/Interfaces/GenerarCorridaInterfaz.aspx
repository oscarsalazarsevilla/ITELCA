﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<ITELCA_CONTROLESTUDIOS.Models.ViewModels.InterfazViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    [Itelca - Interfaz People Soft]
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">


<h2>Listar</h2>
<fieldset>
<div class="span-24 last">
        <div id="msg">
        <%
                if (Model.mensaje != null && Model.mensaje  != "")
                {%>
			    <div  class="ui-state-highlight ui-corner-all" style="margin-top: 10px; padding: 0 .3em; width:100%; height:50px;">
				    <span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
				    <div><%:Model.mensaje%></div>
			    </div>
		    
          <%
                }
            
           %>
        </div>
        <div class="span-9 column append-1 ">
            <h1> Productos </h1>
            <table id="inventario" class="dataTableInv">
                <thead>
                    <tr>
                        <th>Código</th>
                        <th>Producto</th>
                        <th>Sede</th>
                        <th>Cantidad</th>
                    </tr>
                </thead>
                <tbody>
                    <% foreach (var producto in Model.inventario)
                       { %>
                    <tr>
                        <td><%: producto.PRODUCTOS.INV_ITEM_ID %></td>
                        <td><%: producto.PRODUCTOS.DESCR %></td>
                        <td><%: producto.SEDES.NOMBRE %></td>
                        <td><%: producto.CANTIDAD %></td>
                    </tr>
                    <% } %>
                </tbody>
            </table>
        </div>  
        <div class="span-5 append-1 column ">
            <h1> Bancos </h1>
            <table id="bancos" class="dataTableInv">
                <thead>
                    <tr>
                        <th>Código</th>
                        <th>Nombre</th>
                    </tr>
                </thead>
                <tbody>
                    <% foreach (var banco in Model.bancos)
                       { %>
                    <tr>
                        <td><%: banco.CODIGO%></td>
                        <td><%: banco.NOMBRE%></td>
                    </tr>
                    <% } %>
                </tbody>
            </table>
        </div>

        <div class="span-6 column last ">
            <h1> Cuentas </h1>
            <table id="cuentas" class="dataTableInv">
                <thead>
                    <tr>
                        <th>Banco</th>
                        <th>Código</th>
                        <th>Número</th>
                        <th>Tipo</th>
                    </tr>
                </thead>
                <tbody>
                    <% foreach (var cuenta in Model.cuentas)
                       { %>
                    <tr>
                        <td><%: cuenta.BANCOS.NOMBRE%></td>
                        <td><%: cuenta.CUENTA%></td>
                        <td><%: cuenta.NUMERO%></td>
                        <td><%: cuenta.TIPO%></td>
                    </tr>
                    <% } %>
                </tbody>
            </table>
        </div>
</div>
</fieldset>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="CssContent" runat="server">
    <link href="/Content/Css/itelca/demo_table_jui.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="JSContent" runat="server">
    <script src="/Scripts/jquery.dataTables.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            tablaDetalle = $(".dataTableInv").dataTable({
                "bPaginate": true,
                "bFilter": true,
                "bInfo": true,
                "bJQueryUI": true,
                "bAutoWidth": true
            });

        });
    </script>
</asp:Content>
