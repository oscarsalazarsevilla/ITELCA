﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>

<div class="icon-wrapper">
                    <div class="icon">
                        <a href="<%= Url.Action("Listar","Categoria") %>">
                            <img src="/Content/Imagenes/categoria.png" alt="Categorías" />
                            <span>Categorías</span>
                        </a>
                    </div>
                </div>
                <div class="icon-wrapper">
                    <div class="icon">
                        <a href="<%= Url.Action("Listar","Seccion") %>">
                            <img src="/Content/Imagenes/secciones.png" alt="Secciones" />
                            <span>Secciones</span>
                        </a>
                    </div>
                </div>
                <div class="icon-wrapper">
                    <div class="icon">
                        <a href="<%= Url.Action("Listar","Documentos") %>">
                            <img src="/Content/Imagenes/documentos.png" alt="Documentos" />
                            <span>Documentos</span>
                        </a>
                    </div>
                </div>
                <div class="icon-wrapper">
                    <div class="icon">
                        <a href="<%= Url.Action("Listar","Banner") %>">
                            <img src="/Content/Imagenes/banners.png" alt="Banners" />
                            <span>Banners</span>
                        </a>
                    </div>
                </div>
                <div class="icon-wrapper">
                    <div class="icon">
                        <a href="#">
                            <img src="/Content/Imagenes/contactos.png" alt="Contactos" />
                            <span>Contactos</span>
                        </a>
                    </div>
                </div>
                <div class="icon-wrapper">
                    <div class="icon">
                        <a href="<%= Url.Action("Listar","Noticia") %>">
                            <img src="/Content/Imagenes/noticias.png" alt="Noticias" />
                            <span>Noticias</span>
                        </a>
                    </div>
                </div>