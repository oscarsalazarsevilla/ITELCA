﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>

<div class="icon-wrapper">
        <div class="icon">
                <a href="<%= Url.Action("Index","MatriculaManual") %>">
                    <img src="/Content/Imagenes/inscripciones.png" alt="Inscripciones" />
                    <span>Inscripciones</span>
                </a>
            </div>
        </div>