﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>


<ul id="css3menu1" class="topmenu">
    <li class="topmenu"><a href="#" style="height:15px;line-height:15px;"><span>Control estudios</span></a>
        <ul>
            <li class="subfirst"><a href="<%= Url.Action("Nueva","Solicitudes") %>" >Crear solicitud</a></li> 
        </ul>
    </li>
    <li class="toplast"><a href="#"  style="height:15px;line-height:15px;"><span>Mi cuenta</span></a>
	    <ul>
            <li class="subfirst"><a href="<%= Url.Action("IndexCDE","Home") %>">Ir a principal</a> </li>
		    <li class="submenu"><a href="<%= Url.Action("CambiarClave","Usuario") %>">Cambiar contraseña</a> </li>
            <li class="sublast"><a href="<%= Url.Action("LogOffHome","Account") %>">Salir</a> </li>
	    </ul>
	</li>
</ul>