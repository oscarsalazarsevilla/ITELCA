﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>

<div class="icon-wrapper">
                <div class="icon-wrapper">
                    <div class="icon">
                        <a href="<%= Url.Action("Listar","Turno") %>">
                            <img src="/Content/Imagenes/Turnos.png" alt="Turnos" />
                            <span>Turnos</span>
                        </a>
                    </div>
                </div>
                <div class="icon-wrapper">
                    <div class="icon">
                        <a href="<%= Url.Action("Listar","Taller") %>">
                            <img src="/Content/Imagenes/Talleres.png" alt="Talleres" />
                            <span>Talleres</span>
                        </a>
                    </div>
                </div>
                <div class="icon-wrapper">
                    <div class="icon">
                        <a href="<%= Url.Action("Listar","Curso") %>">
                            <img src="/Content/Imagenes/Cursos.png" alt="Cursos" />
                            <span>Cursos</span>
                        </a>
                    </div>
                </div>
                <div class="icon-wrapper">
                    <div class="icon">
                        <a href="<%= Url.Action("Listar","Modulo") %>">
                            <img src="/Content/Imagenes/Modulos.png" alt="Modulos" />
                            <span>Módulos</span>
                        </a>
                    </div>
                </div>
                <div class="icon-wrapper">
                    <div class="icon">
                        <a href="<%= Url.Action("Crear","OfertaCurso") %>">
                            <img src="/Content/Imagenes/ofertas.png" alt="Ofertas de curso" />
                            <span>Ofertas de curso</span>
                        </a>
                    </div>
                </div>
                <div class="icon-wrapper">
                    <div class="icon">
                        <a href="<%= Url.Action("Listar","Solicitudes") %>">
                            <img src="/Content/Imagenes/solicitudes.png" alt="Solicitudes" />
                            <span>Solicitudes</span>
                        </a>
                    </div>
                </div>
                <div class="icon-wrapper">
                    <div class="icon">
                        <a href="<%= Url.Action("Listar","Entrega") %>">
                            <img src="/Content/Imagenes/Entregas.png" alt="Entregas" />
                            <span>Entregas</span>
                        </a>
                    </div>
                </div>