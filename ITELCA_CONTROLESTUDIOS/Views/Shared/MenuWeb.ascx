﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>


<ul id="css3menu1" class="topmenu">
	<li class="topfirst"><a href="#" style="height:15px;line-height:15px;"><span>Página Web</span></a>
        <ul>
            <li class="subfirst"><a href="#" style="height:15px;line-height:15px;"><span>Contenido</span></a>
	            <ul>
                    <li class="subfirst"><a href="<%= Url.Action("Listar","Categoria") %>"><span>Gestor de categorías</span></a>
		                <ul> <li class="subfirst"><a href="<%= Url.Action("Crear","Categoria") %>" >Crear categoría</a></li> </ul>
                    </li>
                    <li class="submenu"><a href="<%= Url.Action("Listar","Seccion") %>"><span>Gestor de secciones</span></a>
		                <ul> <li class="subfirst"><a href="<%= Url.Action("Crear","Seccion") %>" >Crear sección</a></li> </ul>
                    </li>
                    <li class="submenu"><a href="<%= Url.Action("Listar","Testimonios") %>">Gestor de testimonios</a>
                    </li>
                    <li class="submenu"><a href="<%= Url.Action("Listar","TipoFaq") %>"><span>Gestor de Tipos de FAQ's</span></a>
		                <ul> <li class="subfirst"><a href="<%= Url.Action("Crear","TipoFaq") %>" >Crear Tipo FAQ</a></li> </ul>
                    </li>
                    <li class="sublast"><a href="<%= Url.Action("Listar","Faqs") %>"><span>Gestor de FAQ</span></a>
		                <ul> <li class="subfirst"><a href="<%= Url.Action("Crear","Faqs") %>" >Crear FAQ</a></li> </ul>
                    </li>
	            </ul>
	        </li>
            <li class="submenu"><a href="#" style="height:15px;line-height:15px;"><span>Multimedia</span></a>
	            <ul>
                    <li class="subfirst"><a href="<%= Url.Action("Listar","Banner") %>"><span>Gestor de banners</span></a>
		                <ul> <li class="subfirst"><a href="<%= Url.Action("Crear","Banner") %>" >Crear banner</a></li> </ul>
                    </li>
                    <li class="submenu"><a href="<%= Url.Action("Listar","Galeria") %>"><span>Gestor de galerías</span></a>
		                <ul> <li class="subfirst"><a href="<%= Url.Action("Crear","Galeria") %>" >Crear galería</a></li> </ul>
                    </li>
                    <%--<li class="sublast"><a href="<%= Url.Action("Listar","Documento") %>"><span>Gestor de documentos</span></a>
		                <ul> 
                            <li class="subfirst"><a href="<%= Url.Action("Crear","TipoDocumento") %>" ><span>Gestor de tipos doc.</span></a>
                                <ul> 
                                    <li class="subfirst"><a href="<%= Url.Action("Crear","TipoDocumento") %>" >Crear tipo doc.</a></li> 
                                </ul>
                            </li>
                            <li class="sublast"><a href="<%= Url.Action("Crear","Documento") %>" >Crear documento</a></li>  
                        </ul>
                    </li>--%>
	            </ul>
	        </li>
            <li class="sublast"><a href="#" style="height:15px;line-height:15px;"><span>Noticias</span></a>
	            <ul>
                    <li class="subfirst"><a href="<%= Url.Action("Listar","Noticia") %>"><span>Gestor de noticias</span></a>
		                <ul> <li class="subfirst"><a href="<%= Url.Action("Crear","Noticia") %>" >Crear noticia</a></li> </ul>
                    </li>
	            </ul>
	        </li>
        </ul>
    </li>
    <li class="toplast"><a href="#"  style="height:15px;line-height:15px;"><span>Mi cuenta</span></a>
	    <ul>
            <li class="subfirst"><a href="<%= Url.Action("IndexCDE","Home") %>">Ir a principal</a> </li>
		    <li class="submenu"><a href="<%= Url.Action("CambiarClave","Usuario") %>">Cambiar contraseña</a> </li>
            <li class="sublast"><a href="<%= Url.Action("LogOffHome","Account") %>">Salir</a> </li>
	    </ul>
	</li>
</ul>