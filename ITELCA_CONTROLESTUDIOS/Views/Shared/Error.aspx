﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<System.Web.Mvc.HandleErrorInfo>" %>

<asp:Content ID="errorTitle" ContentPlaceHolderID="TitleContent" runat="server">
   Itelca - Error
</asp:Content>

<asp:Content ID="errorContent" ContentPlaceHolderID="MainContent" runat="server">
     <div style="width:auto;" align="center">
            <img width="300" alt="" src="/Content/Imagenes/logo.png" />
            </br>
             <div id="mensaje">
                <b>Lo sentimos. Existe un error procesando la petición, Si el problema persiste comuniquese con el Administrador del Sistema</br> </b>
             </div>
             <div> <a href="http://bspitelcawebp.bsp.com.ve:8080/">Volver</a></div>
         </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CssContent" runat="server">
<style type="text/css">
body
{
    width:400px; 
    margin: 150px auto;
    font-size: 20px;
}
img
{
    margin:0 auto;
}
#mensaje
{
    margin:20px auto;
    border-radius: 10px;
    background-color: #DDDDDD;
    color:#61656B;
    padding:20px;
    width:360px;
}

</style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="JSContent" runat="server">
</asp:Content>
       

