﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>

<div class="icon-wrapper">
                <div class="icon">
                        <a href="<%= Url.Action("Index","AsistenciaTaller") %>">
                            <img src="/Content/Imagenes/asistencias.png" alt="Asistencias" />
                            <span>Asistencias talleres</span>
                        </a>
                    </div>
                </div>
                <div class="icon-wrapper">
                    <div class="icon">
                        <a href="<%= Url.Action("Index","AsistenciaCurso") %>">
                            <img src="/Content/Imagenes/asistencias.png" alt="Configuraciones" />
                            <span>Asistencias módulos</span>
                        </a>
                    </div>
                </div>
                <div class="icon-wrapper">
                    <div class="icon">
                        <a href="<%= Url.Action("Index","NotaModulo") %>">
                            <img src="/Content/Imagenes/cursos.png" alt="Notas" />
                            <span>Notas</span>
                        </a>
                    </div>
                </div>
                