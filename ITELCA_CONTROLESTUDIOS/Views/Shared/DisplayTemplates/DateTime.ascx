﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<DateTime?>" %>
   <div class="display-label">  
      <%: Html.LabelFor(model => model) %>  
  </div>  
  <div class="display-field">  
      <%: (Model.HasValue) ? Model.Value.ToString("dd/MM/yyyy") : "---" %>  
  </div>  



