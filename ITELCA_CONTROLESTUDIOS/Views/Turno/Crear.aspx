﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<ITELCA_CONTROLESTUDIOS.Models.ViewModels.TurnoViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    [Itelca - Turno]
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
<div class="span-22 append-1 prepend-1 column last ui-widget-content ui-corner-all">
	<span class="introForm">Turno > Crear</span>
		<% using (Html.BeginForm()) { %>
		<% Html.EnableClientValidation(); %>
        <%:Html.ValidationSummary("Error en la aplicación") %>
        <%:Html.HiddenFor(model => model.turno.ESTADO, new { Value = "1" }) %>
        <%:Html.Hidden("diaTurno", "") %>
            <div  class="span-24 Formulario  column last">
				<% Html.RenderPartial("Form",Model); %>
         <div class="span-24 last column">
            <div class="span-4 column"> Día</div>
            <div class="span-4 column"> Hora Inicio</div>
            <div class="span-4 column"> Hora Fin </div>
            <div class="span-8 last column"> Frecuencia </div>

        </div>
        
        <% string[] strArray = { "lunes", "martes", "miercoles", "jueves", "viernes","sabado","domingo" };
           SelectList periodo;
           Dictionary<int, string> per = new Dictionary<int, string>();
           per.Add(7, "SEMANAL");
           per.Add(14, "QUINCENAL");
           foreach (var ope in Model.diaTurno) {
               periodo = new SelectList(per, "Key", "Value",ope.periodo);   
               
               %>
            <div class="span-24 last column">
                <div class="span-4 column"> <%: Html.CheckBox(ope.dia,ope.ban, new { @class = "checkbox_dias", @id ="checkbox_"+ope.dia})%> <% Response.Write(ope.dia.ToUpper()); %> </div>
                <div class="span-4 column"> <%: Html.TextBox("hora_inicio",ope.inicio, new { @id = "hor_inicio_" + ope.dia, @class = "time", disabled = !ope.ban })%></div>
                <div class="span-4 column"> <%: Html.TextBox("hora_fin",ope.fin,new {@id="hor_fin_"+ope.dia , @class="time et ",disabled=!ope.ban})%></div>
                <div class="span-8 column last"> <%: Html.DropDownList("periodo",periodo,new {@id="periodo_"+ope.dia , @class="et ",disabled=!ope.ban})%></div>
            </div>
               
        <%   }
         %>
			</div>
             <div class="separator column span-24">
            </div>
            <p>
				<input type="button" value="Crear" class="button" id="guardar" />
			</p>
           
            <div class="column span-24">
                <%: Html.ActionLink("Volver al listado", "Listar")%>
            </div>
          
			
    <% } %>
     
                
</div>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="CssContent" runat="server">
   <link href="/Content/Css/multiselect.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="JsContent" runat="server">
    <script src="/Scripts/jquery.multiselect.min.js" type="text/javascript"></script>
    <script src="/Scripts/jquery.multiselect.es.js" type="text/javascript"></script>
    <script src="/Scripts/Views/turno.js" type="text/javascript"></script>
</asp:Content>

