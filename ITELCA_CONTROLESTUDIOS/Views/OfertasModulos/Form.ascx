﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ITELCA_CONTROLESTUDIOS.Models.ViewModels.OfertasModulosViewModel>" %>

   <fieldset>
          <div class ="span-24 last">
            <div class="span-8 column ">
                <div class="editor-label">
				    <%: Html.LabelFor(model => model.OFERTASMODULOS.CURSO_ID)%>
			    </div>
                <div class="editor-field">
				    <%: Html.DropDownListFor(model => model.OFERTASMODULOS.CURSO_ID, Model.listaCursos)%>
				    <%: Html.ValidationMessageFor(model => model.OFERTASMODULOS.CURSO_ID)%>
			    </div> 
            </div>

            <div class="span-8 column ">
                <div class="editor-label">
				    <%: Html.LabelFor(model => model.OFERTASMODULOS.SEDE_ID)%>
			    </div>
                <div class="editor-field">
				    <%: Html.DropDownListFor(model => model.OFERTASMODULOS.SEDE_ID, Model.listaSedes)%>
				    <%: Html.ValidationMessageFor(model => model.OFERTASMODULOS.SEDE_ID)%>
			    </div> 
            </div>
            <div class="span-8 column last">
                <div class="editor-label">
				    <%: Html.LabelFor(model => model.OFERTASMODULOS.TURNO_ID)%>
			    </div>
                <div class="editor-field">
				    <%: Html.DropDownListFor(model => model.OFERTASMODULOS.TURNO_ID, Model.listaTurnos)%>
				    <%: Html.ValidationMessageFor(model => model.OFERTASMODULOS.TURNO_ID)%>
			    </div> 
            </div>
        </div>
       <div class ="span-24 last">
            <div class="span-8 column last">
                <div class="editor-label">
				    <%: Html.LabelFor(model => model.OFERTASMODULOS.AULA_ID)%>
			    </div>
                <div class="editor-field">
				    <%: Html.DropDownListFor(model => model.OFERTASMODULOS.AULA_ID, Model.listaAulas)%>
				    <%: Html.ValidationMessageFor(model => model.OFERTASMODULOS.AULA_ID)%>
			    </div> 
            </div>
            <div class="span-8 column ">
                <div class="editor-label">
				    <%: Html.LabelFor(model => model.OFERTASMODULOS.MODULO_ID)%>
			    </div>
                <div class="editor-field">
				    <%: Html.DropDownListFor(model => model.OFERTASMODULOS.MODULO_ID, new SelectList(new Dictionary<string,string>()))%>
				    <%: Html.ValidationMessageFor(model => model.OFERTASMODULOS.MODULO_ID)%>
			    </div> 
            </div>
             <div class="span-8 column last">
                 <div class="editor-label">
					    <%: Html.LabelFor(model => model.OFERTASMODULOS.FECHA_INICIO)%>
			    </div>
                <div class="editor-field">
				    <%: Html.TextBoxFor(model => model.OFERTASMODULOS.FECHA_INICIO, new { @class = "datepicker" })%>
				    <%: Html.ValidationMessageFor(model => model.OFERTASMODULOS.FECHA_INICIO)%>
			    </div> 
            </div>    
        </div>
         <div class="span-24 column last ">
             
            <div class="span-8 column">
                 <div class="editor-label">
					    <%: Html.LabelFor(model => model.OFERTASMODULOS.FECHA_FIN)%>
			    </div>
                <div class="editor-field">
				    <%: Html.TextBoxFor(model => model.OFERTASMODULOS.FECHA_FIN, new { @class = "datepicker" })%>
				    <%: Html.ValidationMessageFor(model => model.OFERTASMODULOS.FECHA_FIN)%>
			    </div> 
            </div>    
            <div class="span-8 column">
                 <div class="editor-label">
					    <%: Html.LabelFor(model => model.OFERTASMODULOS.PROXIMO_INICIO)%>
			    </div>
                <div class="editor-field">
				    <%: Html.TextBoxFor(model => model.OFERTASMODULOS.PROXIMO_INICIO, new { @class = "datepicker" })%>
				    <%: Html.ValidationMessageFor(model => model.OFERTASMODULOS.PROXIMO_INICIO)%>
			    </div> 
            </div>  
             <div class="span-4 column last">
                 <div class="editor-label">
					    <%: Html.LabelFor(model => model.OFERTASMODULOS.MATRICULA_MINIMA)%>
			    </div>
                <div class="editor-field">
				    <%: Html.TextBoxFor(model => model.OFERTASMODULOS.MATRICULA_MINIMA)%>
				    <%: Html.ValidationMessageFor(model => model.OFERTASMODULOS.MATRICULA_MINIMA)%>
			    </div> 
            </div>      
         </div>
       
        <div class="span-24 column last ">
            <p>
			    <input type="submit" value="Guardar" class="button" />
		    </p>
        </div>
           
   </fieldset>   