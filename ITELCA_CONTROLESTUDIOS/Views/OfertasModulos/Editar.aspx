﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<ITELCA_CONTROLESTUDIOS.Models.ViewModels.OfertasModulosViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    [Itelca - Ofertas Modulos]
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
<div class="span-22 append-1 prepend-1 column last ui-widget-content ui-corner-all">
	<span class="introForm">Ofertas Modulos > Editar</span>
		<% using (Html.BeginForm()) { %>
		<% Html.EnableClientValidation(); %>
        <%:Html.ValidationSummary("Error en la aplicación") %>
        <%:Html.HiddenFor(model => model.OFERTASMODULOS.OFERTA_MODULO_ID) %>
        <%:Html.HiddenFor(model => model.OFERTASMODULOS.USUARIO_ID)%>
            <div  class="span-24 Formulario  column last">
				<% Html.RenderPartial("Form",Model); %>
			</div>
             <div class="separator column span-24">
            </div>
            <table id="BankTransactions"></table>
             <div id="pager"></div> 
            <p>
				<input type="submit" value="Guardar" class="button" />
			</p>
           
            <div class="column span-24">
                <%: Html.ActionLink("Volver al listado", "Listar")%>
            </div>
         
			
    <% } %>
     <div class=" prepend-2 separator column span-22" style="height:300px;" >
                <div id='map_canvas' style='width:90%; height:300px;'></div>
            </div>
</div>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="CssContent" runat="server">
    <link href="/Content/Css/ui.jqgrid.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="JsContent" runat="server">
     <script src="/Scripts/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
     <script src="/Scripts/jquery.maskedinput-1.2.2.js" type="text/javascript"></script>
    <script src="/Scripts/jquery.jqGrid.min.js" type="text/javascript"></script>
     <script type="text/javascript">
	    $(document).ready(function () {
		    var lastSelection, grid = jQuery("#BankTransactions");
		    grid.jqGrid({
		    url: '/OfertasModulos/ObtenerData',
		    datatype: 'json',
		    mtype: 'GET',
		    colNames: ['TransactionLineId', 'TransactionId', 'BankTransactionId', 'Number', 'Amount', 'Category'],
		    colModel: [
			    { name: 'transactionLineId', index: 'transactionLineId', editable: true, editrules: { edithidden: true }, hidden: true, width: 40 },
			    { name: 'transactionId', index: 'transactionId', editable: true, editrules: { edithidden: true }, hidden: true, width: 40 },
			    { name: 'bankTransactionId', index: 'bankTransactionId', editable: true, editrules: { edithidden: true }, hidden: true, width: 40 },
			    { name: 'Number', index: 'Number', width: 80, sortable: false },
			    { name: 'Amount', index: 'SubAmount', editable: true, width: 80, align: 'right', sortable: false, cellattr: function (rowId, tv, rawObject, cm, rdata) { return 'class="BankTranEdit"' }, formatter: 'currency', formatoptions: { decimalSeparator: '.', thousandsSeparator: ',', decimalPlaces: 2, defaultValue: '&nbsp;'} },
			    {
				    name: 'CategoryIdURL',
				    index: 'CategoryIdURL',
				    editable: true,
				    edittype: 'select',
				    //formatter: 'select',
				    editoptions: { dataUrl: "OptGroupSelect.htm" },
				    width: 190
			    },
		    ],
		    pager: '#pager', //jQuery('#pager'),
		    rowNum: 100,
		    rowList: [25, 50, 100],
		    editurl: "/Dropdown/GridSave", // 'clientArray'
		    sortname: 'Number',
		    sortorder: "desc",
		    viewrecords: true,
		    //width: 450,
		    height: 'auto', //450,
		    onCellSelect: function (rowid, iCol, cellContent, e) {
		    grid.jqGrid('restoreRow', lastSelection);
		    grid.jqGrid('editRow', rowid, true, null, null, null, null, null);
		    lastSelection = rowid;
		    }
	    });
		    $("#pager_left").hide(); // hide unused place in the pager
	    });
     </script>
</asp:Content>

