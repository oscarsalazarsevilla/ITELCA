﻿   <%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ITELCA_CONTROLESTUDIOS.Models.ViewModels.AulaViewModel>" %>
    <div class ="span-24 last">
        <div class="span-8 column last">
             <div class="editor-label">
					<%: Html.LabelFor(model => model.aula.SEDE_ID)%>
			</div>
            <div class="editor-field">
				<%: Html.DropDownListFor(model => model.aula.SEDE_ID,Model.sedes,"--Seleccione--")%>
				<%: Html.ValidationMessageFor(model => model.aula.SEDE_ID)%>
			</div> 
        </div>
   </div>

   <div class ="span-24 last">
        <div class="span-8 column last">
             <div class="editor-label">
					<%: Html.LabelFor(model => model.aula.TIPO_AULA_ID)%>
			</div>
            <div class="editor-field">
				<%: Html.DropDownListFor(model => model.aula.TIPO_AULA_ID,Model.tipoAula,"--Seleccione--")%>
				<%: Html.ValidationMessageFor(model => model.aula.TIPO_AULA_ID)%>
			</div> 
        </div>
    </div>

    <div class ="span-24 last">
        <div class="span-8 column last">
             <div class="editor-label">
					<%: Html.LabelFor(model => model.aula.CODIGO)%>
			</div>
            <div class="editor-field">
				<%: Html.TextBoxFor(model => model.aula.CODIGO)%>
				<%: Html.ValidationMessageFor(model => model.aula.CODIGO)%>
			</div> 
        </div>
    </div>
   
    <div class ="span-24 last">
        <div class="span-8 column last">
             <div class="editor-label">
					<%: Html.LabelFor(model => model.aula.NOMBRE)%>
			</div>
            <div class="editor-field">
				<%: Html.TextBoxFor(model => model.aula.NOMBRE)%>
				<%: Html.ValidationMessageFor(model => model.aula.NOMBRE)%>
			</div> 
        </div>
    </div>

    <div class ="span-24 last">
         <div class="span-8 column last ">
             <div class="editor-label">
					<%: Html.LabelFor(model => model.aula.PISO)%>
			</div>
            <div class="editor-field">
				<%: Html.TextBoxFor(model => model.aula.PISO)%>
				<%: Html.ValidationMessageFor(model => model.aula.PISO)%>
			</div> 
        </div>
    </div>


     <div class ="span-24 last">
        <div class="span-8 column last">
            <div class="editor-label">
				<%: Html.LabelFor(model => model.aula.CAPACIDAD)%>
			</div>
            <div class="editor-field">
				<%: Html.TextBoxFor(model => model.aula.CAPACIDAD)%>
				<%: Html.ValidationMessageFor(model => model.aula.CAPACIDAD)%>
			</div> 
        </div>
    </div>   