﻿   <%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ITELCA_CONTROLESTUDIOS.Models.ViewModels.CategoriaViewModel>" %>
         
    <div class ="span-24 last">
        <div class="span-8 last column ">
             <div class="editor-label">
					<%: Html.LabelFor(model => model.categoria.NOMBRE)%>
			</div>
            <div class="editor-field">
				<%: Html.TextBoxFor(model => model.categoria.NOMBRE)%>
				<%: Html.ValidationMessageFor(model => model.categoria.NOMBRE)%>
			</div> 
        </div>
        <div class="span-8 last column ">
             <div class="editor-label">
					<%: Html.LabelFor(model => model.categoria.ALIAS)%>
			</div>
            <div class="editor-field">
				<%: Html.TextBoxFor(model => model.categoria.ALIAS)%>
				<%: Html.ValidationMessageFor(model => model.categoria.ALIAS)%>
			</div> 
        </div>
        <div class="span-8 last column last ">
             <div class="editor-label">
					<%: Html.LabelFor(model => model.categoria.TIPOCATEGORIA_ID)%>
			</div>
            <div class="editor-field">
				<%: Html.DropDownListFor(model => model.categoria.TIPOCATEGORIA_ID, Model.listaTipos, "-- Seleccione --")%>
				<%: Html.ValidationMessageFor(model => model.categoria.TIPOCATEGORIA_ID)%>
			</div> 
        </div>
    </div>
    <div class ="span-24 last">
        <div class="span-8  column ">
             <div class="editor-label">
					<%: Html.LabelFor(model => model.categoria.FECHA_PUBLICACION)%>
			</div>
            <div class="editor-field">
				<%: Html.TextBoxFor(model => model.categoria.FECHA_PUBLICACION, new { @class = "datepicker" })%>
				<%: Html.ValidationMessageFor(model => model.categoria.FECHA_PUBLICACION)%>
			</div> 
        </div>
        <div class="span-8  column ">
             <div class="editor-label">
					<%: Html.LabelFor(model => model.categoria.FECHA_FIN_PUBLICACION)%>
			</div>
            <div class="editor-field">
				<%: Html.TextBoxFor(model => model.categoria.FECHA_FIN_PUBLICACION, new { @class = "datepicker" })%>
				<%: Html.ValidationMessageFor(model => model.categoria.FECHA_FIN_PUBLICACION)%>
			</div>
        </div>
       <div class="span-8 last column ">
             <div class="editor-label">
					<%: Html.LabelFor(model => model.categoria.PADRE_ID)%>
			</div>
            <div class="editor-field">
				<%: Html.DropDownListFor(model => model.categoria.PADRE_ID, Model.listaCategorias,"-- Seleccione --")%>
				<%: Html.ValidationMessageFor(model => model.categoria.PADRE_ID)%>
			</div> 
        </div>
    </div>
    <div class="span-24 column last">
        <div class="span-8  column ">
             <div class="editor-label">
					<%: Html.LabelFor(model => model.categoria.ORDEN)%>
			</div>
            <div class="editor-field">
				<%: Html.TextBoxFor(model => model.categoria.ORDEN)%>
				<%: Html.ValidationMessageFor(model => model.categoria.ORDEN)%>
			</div> 
        </div>
    </div>
    <div class="span-24 column last">
        <div class="editor-label">
				<%: Html.LabelFor(model => model.categoria.TEXTO)%>
		</div>
        <div class="editor-field">
			<%: Html.TextAreaFor(model => model.categoria.TEXTO)%>
			<%: Html.ValidationMessageFor(model => model.categoria.TEXTO)%>
		</div> 
    </div>