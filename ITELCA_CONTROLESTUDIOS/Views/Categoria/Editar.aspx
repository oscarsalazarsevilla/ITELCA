﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<ITELCA_CONTROLESTUDIOS.Models.ViewModels.CategoriaViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    [Itelca - Categoria]
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
<div class="span-22 append-1 prepend-1 column last ui-widget-content ui-corner-all">
	<span class="introForm">Categoria > Editar</span>
		<% using (Html.BeginForm("Editar", "Categoria", FormMethod.Post)) { %>
		<% Html.EnableClientValidation(); %>
        <%:Html.ValidationSummary("Error en la aplicación") %>
         <%:Html.HiddenFor(model => model.categoria.CATEGORIA_ID) %>
         <%:Html.HiddenFor(model => model.categoria.USUARIO_ID) %>
            <div  class="span-24 Formulario  column last">
             <div class="separator column span-24">
            </div>
				<% Html.RenderPartial("Form",Model); %>
                
                <div class ="span-24 last">
                    <div class="span-8 column last">
                        <div class="editor-label">
				            <%: Html.LabelFor(model => model.categoria.ESTADO)%>
			            </div>
                        <div class="editor-field">
				            <%: Html.DropDownListFor(model => model.categoria.ESTADO, Model.activo)%>
				            <%: Html.ValidationMessageFor(model => model.categoria.ESTADO)%>
			            </div> 
                    </div>
                </div>
			</div>
             <div class="separator column span-24">
            </div>
            <p>
				<input type="submit" value="Guardar" class="button" />
			</p>
           
            <div class="column span-24">
                <%: Html.ActionLink("Volver al listado", "Listar")%>
            </div>
         
			
    <% } %>
</div>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="CssContent" runat="server">
 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="JsContent" runat="server">
    <script src="/Scripts/jquery.maskedinput-1.2.2.js" type="text/javascript"></script>
    <script src="/Scripts/ckeditor/ckeditor.js" type="text/javascript"></script>
    <script src="/Scripts/Views/categoria.js" type="text/javascript"></script>
</asp:Content>

