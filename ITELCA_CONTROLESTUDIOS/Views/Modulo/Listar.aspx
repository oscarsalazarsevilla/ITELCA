﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<ITELCA_CONTROLESTUDIOS.Models.ViewModels.ModuloViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Listar
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2>Listar</h2>
<fieldset>
<div class="span-24 last">
        <p>
            <%: Html.ActionLink("Nuevo", "Crear") %>
        </p>
        <div id="msg">
        <%
            try
            {

                if (TempData["mensaje"] != null && TempData["mensaje"]  != "")
                {%>
			    <div  class="ui-state-highlight ui-corner-all" style="margin-top: 10px; padding: 0 .3em; width:30%; height:20px;">
				    <span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
				    <div><%:TempData["mensaje"]  %></div>
			    </div>
		    
          <%
                }
            }
            catch{} 
            
           %>
        </div>

        <h4><%:Html.Label("Curso") %></h4>
        <%:Html.DropDownList("SelectListCurso", Model.listaCursos, "Seleccione")%>
          </br>
          </br>
    

            <table id="gridModulo">
            </table>
            <div id="pager"></div>
</div>
</fieldset>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="CssContent" runat="server">
    <link href="/Content/Css/ui.jqgrid.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="JSContent" runat="server">
    <script src="/Scripts/grid.locale-es.js" type="text/javascript"></script>
    <script src="/Scripts/jquery.jqGrid.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            winHeight = window.innerHeight;
            wHeight = winHeight - 340;

            var gridModulo = $("#gridModulo").jqGrid({
                postData: { CursoId: function () { return $("#SelectListCurso option:selected").val() } },
                url: '/Modulo/ObtenerData',
                datatype: 'json',
                mtype: 'POST',
                colNames: ['Id', 'Nombre','Descripción','Curso(s)','Orden','Estado','Editar', 'Eliminar'],
                colModel: [
		            { name: 'MODULO_ID', index: 'MODULO_ID', align: "right", resizable: true, width: 50, searchoptions: { sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']} },
		            { name: 'NOMBRE', index: 'NOMBRE', width: 200, searchoptions: { sopt: ['cn']} },
                    { name: 'DESCRIPCION', index: 'DESCRIPCION', width: 300, sortable: true, searchoptions: { sopt: ['cn']} },
                    { name: 'CURSOS_MODULOS.FirstOrDefault().CURSOS.NOMBRE', index: 'CURSOS_MODULOS.FirstOrDefault().CURSOS.NOMBRE', width: 200, search: false },
                    { name: 'ORDEN', index: 'ORDEN', width: 200, searchoptions: { sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']} },
                    { name: 'ESTADO', index: 'ESTADO', width: 50, align: "center", resizable: true, formatter: 'checkbox', stype: "select", searchoptions: { sopt: ['eq', 'ne'], value: "1:Si;0:No"} },
		            { name: 'Action1', index: 'Action1', align: "center", width: 50, resizable: false, sortable: false, search: false },
                    { name: 'Action2', index: 'Action2', align: "center", width: 50, resizable: false, sortable: false, search: false}],
                pager: jQuery('#pager'),
                rowNum: 500,
                height: wHeight,
                rowList: [500, 1000, 5000],
                sortname: 'NOMBRE',
                sortorder: 'asc',
                autowidth: true,
                viewrecords: true,
                caption: 'Listado de Modulos',
                loadComplete: function () {
                    $(".ui-icon-close").click(function () {
                        var gr = $(this).attr("id");
                        var r = confirm("Desea Eliminar modulo ");
                        if (r == true) {
                            $.post("/Modulo/Eliminar", { id: gr }, function (data) {
                                var dat = ' <div  class="ui-state-highlight ui-corner-all" style="margin-top: 10px; padding: 0 .3em; width:30%; height:20px;"><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span><div>';
                                dat += data[0].msg;
                                dat += '</div></div>';
                                $("#msg").empty().html(dat);
                                gridModulo.trigger('reloadGrid');

                            });
                        }
                        else {
                            x = "Cancelar";
                        }
                        // alert("adas");
                    });

                }
            }).navGrid('#pager',
            {
                edit: false, add: false, del: false, search: true, refresh: true
            },
            {}, //edit options
            {}, //add options
            {}, //del options
            {multipleSearch: true} // search options
            );


            $('#SelectListCurso').change(function () {
                jQuery("#gridModulo").trigger("reloadGrid");
            });
        });
    </script>
</asp:Content>
