﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<ITELCA_CONTROLESTUDIOS.Models.ViewModels.ModuloViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    [Itelca - Modulo]
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
<div class="span-22 append-1 prepend-1 column last ui-widget-content ui-corner-all">
	<span class="introForm">Modulo > Editar</span>
		<% using (Html.BeginForm()) { %>
		<% Html.EnableClientValidation(); %>
        <%:Html.ValidationSummary("Error en la aplicación") %>
         <%:Html.HiddenFor(model => model.modulo.MODULO_ID) %>
         <%:Html.HiddenFor(model => model.modulo.USUARIO_ID) %>
            <div  class="span-24 Formulario  column last">
				<% Html.RenderPartial("Form",Model); %>   
                <div class="span-24">
                    
                    <div class="span-8 column last">
                        <div class="editor-label">
				            <%: Html.LabelFor(model => model.modulo.ESTADO)%>
			            </div>
                        <div class="editor-field">
				            <%: Html.DropDownListFor(model => model.modulo.ESTADO, Model.activo)%>
				            <%: Html.ValidationMessageFor(model => model.modulo.ESTADO)%>
			            </div> 
                    </div>
                </div>
			</div>
             <div class="separator column span-24">
            </div>
            <p>
				<input type="submit" value="Guardar" class="button" />
			</p>
           
            <div class="column span-24">
                <%: Html.ActionLink("Volver al listado", "Listar")%>
            </div>
         
			
    <% } %>
     
</div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="CssContent" runat="server">
     <link href="/Content/Css/multiselect.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="JsContent" runat="server">
    <script src="/Scripts/jquery.multiselect.min.js" type="text/javascript"></script>
    <script src="/Scripts/jquery.multiselect.es.js" type="text/javascript"></script>
    <script src="/Scripts/Views/modulo.js" type="text/javascript"></script>
</asp:Content>

