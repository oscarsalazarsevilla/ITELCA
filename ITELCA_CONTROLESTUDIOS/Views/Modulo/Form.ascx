﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ITELCA_CONTROLESTUDIOS.Models.ViewModels.ModuloViewModel>" %>
       <div class ="span-24 last">
            <div class="span-8 column last">
                 <div class="editor-label">
					    <%: Html.LabelFor(model => model.modulo.NOMBRE)%>
			    </div>
                <div class="editor-field">
				    <%: Html.TextBoxFor(model => model.modulo.NOMBRE)%>
				    <%: Html.ValidationMessageFor(model => model.modulo.NOMBRE)%>
			    </div> 
            </div>
        </div>
       <div class ="span-24 last">
         <div class="span-8 column last">
             <div class="editor-label">
					<%: Html.LabelFor(model => model.modulo.DESCRIPCION)%>
			</div>
            <div class="editor-field">
				<%: Html.TextAreaFor(model => model.modulo.DESCRIPCION)%>
				<%: Html.ValidationMessageFor(model => model.modulo.DESCRIPCION)%>
			</div> 
        </div>
       </div>
       <div class="span-24 last">
             <div class="span-8 column last">
                <div class="editor-label">
			        <%: Html.LabelFor(model => model.modulo.MINIMO_APROBATORIO)%>
		        </div>
                <div class="editor-field">
			        <%: Html.TextBoxFor(model => model.modulo.MINIMO_APROBATORIO)%>
			        <%: Html.ValidationMessageFor(model => model.modulo.MINIMO_APROBATORIO)%>
		        </div> 
            </div>
        </div>
    <div class="span-24 last">
        <div class="span-8 column last">
            <div class="editor-label">
				<%: Html.Label("Cursos")%>
			</div>
            <div class="editor-field">
				<%: Html.ListBox("CURSO_ID", Model.curso, "--Seleccione--")%>
				<%: Html.ValidationMessage("CURSO_ID")%>
			</div> 
        </div>
    </div>
     <div class ="span-24 last">
        <div class="span-8 column last ">
            <div class="editor-label">
			    <%: Html.LabelFor(model => model.modulo.ORDEN)%>
		    </div>
            <div class="editor-field">
			    <%: Html.TextBoxFor(model => model.modulo.ORDEN)%>
			    <%: Html.ValidationMessageFor(model => model.modulo.ORDEN)%>
		    </div> 
        </div>
    </div>

        
      