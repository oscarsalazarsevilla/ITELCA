﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ITELCA_CONTROLESTUDIOS.Models.ViewModels.TurnoViernesViewModel>" %>
        <div class="span-24 column last">
          <div class="span-8 column last">
                 <div class="editor-label">
					   Turno
			    </div>
                <div class="editor-field">
				    <%: Html.DropDownListFor(model => model.TURNOSVIERNES.TURNO_ID, Model.listaTurnos, new { @size = 3, @class = "multiselect", title = "Click para seleccionar una opción" })%>
				    <%: Html.ValidationMessageFor(model => model.TURNOSVIERNES.TURNO_ID)%>
			    </div> 
            </div> 
        </div>
        <div class="span-24 column">
            <div class="span-8 column last">
                <div class="editor-label">
					    <%: Html.LabelFor(model => model.TURNOSVIERNES.ANIO)%>
			    </div>
                <div class="editor-field">
				    <%: Html.DropDownListFor(model => model.TURNOSVIERNES.ANIO, Model.listaAnios, "-- Seleccione --")%>
			    <%: Html.ValidationMessageFor(model => model.TURNOSVIERNES.ANIO)%>
			    </div> 
            </div> 
        </div>
        <div class="span-24 last column ">
            <div class="span-8 column last">
                <div class="editor-label">
					   Viernes
			    </div>
                <div class="editor-field">
				    <%: Html.TextBoxFor(model => model.TURNOSVIERNES.MULTI_VIERNES, new { @class = "multidatespicker" })%>
				    <%: Html.ValidationMessageFor(model => model.TURNOSVIERNES.VIERNES)%>
			    </div> 
            </div> 
        </div> 