﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<ITELCA_CONTROLESTUDIOS.Models.ViewModels.TurnoViernesViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    [Itelca - Turno Viernes]
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
<div class="span-22 append-1 prepend-1 column last ui-widget-content ui-corner-all">
	<span class="introForm">Turno Viernes > Editar</span>
	<% using (Html.BeginForm("Editar", "TurnoViernes", FormMethod.Post, new { id = "formTurnoViernes" })) { %>
		<% Html.EnableClientValidation(); %>
        <%:Html.ValidationSummary("Error en la aplicación") %>
        <%:Html.HiddenFor(model => model.TURNOSVIERNES.ESTADO, new { Value = "1" })%>
        <%:Html.HiddenFor(model => model.TURNOSVIERNES.TURNOVIERNES_ID)%>
        <%:Html.HiddenFor(model => model.TURNOSVIERNES.USUARIO_ID)%>
            <div  class="span-24 Formulario  column last">
				<% Html.RenderPartial("FormEdit",Model); %>
			</div>
             <div class="separator column span-24">
            </div>
            <p>
				<input type="submit" value="Crear" class="button" />
			</p>
           
            <div class="column span-24">
                <%: Html.ActionLink("Volver al listado", "Listar")%>
            </div>
       
    <% } %>                     
</div>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="CssContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="JsContent" runat="server"> 
    
     <script type="text/javascript">
         $(document).ready(function () {
             $('.datepicker').datepicker({ 
                beforeShowDay: function(date) {
                    return [date.getDay() == 5];
                }
            });
         });
     </script>
</asp:Content>

