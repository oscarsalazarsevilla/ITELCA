﻿   <%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ITELCA_CONTROLESTUDIOS.Models.ViewModels.ConfiguracionViewModel>" %>
         
    <div class ="span-24 last">
     
       <div class="span-8 last column ">
             <div class="editor-label">
					<%: Html.LabelFor(model => model.configuracion.INSTITUCION)%>
			</div>
            <div class="editor-field">
				<%: Html.TextBoxFor(model => model.configuracion.INSTITUCION)%>
				<%: Html.ValidationMessageFor(model => model.configuracion.INSTITUCION)%>
			</div> 
        </div>
        <div class="span-8 last column ">
             <div class="editor-label">
					<%: Html.LabelFor(model => model.configuracion.NUMERO_ME)%>
			</div>
            <div class="editor-field">
				<%: Html.TextBoxFor(model => model.configuracion.NUMERO_ME)%>
				<%: Html.ValidationMessageFor(model => model.configuracion.NUMERO_ME)%>
			</div> 
        </div>
         <div class="span-8 last column ">
             <div class="editor-label">
					<%: Html.LabelFor(model => model.configuracion.DIRECCION_FISCAL)%>
			</div>
            <div class="editor-field">
				<%: Html.TextBoxFor(model => model.configuracion.DIRECCION_FISCAL)%>
				<%: Html.ValidationMessageFor(model => model.configuracion.DIRECCION_FISCAL)%>
			</div> 
        </div>
         <div class="span-8 last column ">
             <div class="editor-label">
					<%: Html.LabelFor(model => model.configuracion.DIRECTOR)%>
			</div>
            <div class="editor-field">
				<%: Html.TextBoxFor(model => model.configuracion.DIRECTOR)%>
				<%: Html.ValidationMessageFor(model => model.configuracion.DIRECTOR)%>
			</div> 
        </div>
          <div class="span-8 last column ">
             <div class="editor-label">
					<%: Html.LabelFor(model => model.configuracion.SUBDIRECTOR)%>
			</div>
            <div class="editor-field">
				<%: Html.TextBoxFor(model => model.configuracion.SUBDIRECTOR)%>
				<%: Html.ValidationMessageFor(model => model.configuracion.SUBDIRECTOR)%>
			</div> 
        </div>
         <div class="span-8 last column ">
             <div class="editor-label">
					<%: Html.LabelFor(model => model.configuracion.TELEFONO1)%>
			</div>
            <div class="editor-field">
				<%: Html.TextBoxFor(model => model.configuracion.TELEFONO1)%>
				<%: Html.ValidationMessageFor(model => model.configuracion.TELEFONO1)%>
			</div> 
        </div>
         <div class="span-8 last column ">
             <div class="editor-label">
					<%: Html.LabelFor(model => model.configuracion.TELEFONO2)%>
			</div>
            <div class="editor-field">
				<%: Html.TextBoxFor(model => model.configuracion.TELEFONO2)%>
				<%: Html.ValidationMessageFor(model => model.configuracion.TELEFONO2)%>
			</div> 
        </div>
        <div class="span-8 last column ">
             <div class="editor-label">
					<%: Html.LabelFor(model => model.configuracion.LOGO)%>
			</div>
            <div class="editor-field">
               <input class="input-background"  id="file2" name="file" type="file" accept="jpg|png|gif" />
				
				<%: Html.ValidationMessageFor(model => model.configuracion.LOGO)%>
			</div> 
        </div>

        <div class="span-8 last column ">
             <div class="editor-label">
					<%: Html.LabelFor(model => model.configuracion.VERSION)%>
			</div>
            <div class="editor-field">
				<%: Html.TextBoxFor(model => model.configuracion.VERSION)%>
				<%: Html.ValidationMessageFor(model => model.configuracion.VERSION)%>
			</div> 
        </div>

         

          <div class="span-8 last column ">
             <div class="editor-label">
					<%: Html.LabelFor(model => model.configuracion.CONTROLESTUDIOS)%>
			</div>
            <div class="editor-field">
				<%: Html.TextBoxFor(model => model.configuracion.CONTROLESTUDIOS)%>
				<%: Html.ValidationMessageFor(model => model.configuracion.CONTROLESTUDIOS)%>
			</div> 
        </div>
         <div class="span-8 last column ">
             <div class="editor-label">
					<%: Html.LabelFor(model => model.configuracion.WEB)%>
			</div>
            <div class="editor-field">
				<%: Html.TextBoxFor(model => model.configuracion.WEB)%>
				<%: Html.ValidationMessageFor(model => model.configuracion.WEB)%>
			</div> 
        </div>
        </div>
       <div class ="span-24 last">
         <div class="span-8 last column ">
             <div class="editor-label">
					<%: Html.LabelFor(model => model.configuracion.TWITTER)%>
			</div>
            <div class="editor-field">
				<%: Html.TextBoxFor(model => model.configuracion.TWITTER)%>
				<%: Html.ValidationMessageFor(model => model.configuracion.TWITTER)%>
			</div> 
        </div>
        <div class="span-8 last column ">
             <div class="editor-label">
					<%: Html.LabelFor(model => model.configuracion.FACEBOOK)%>
			</div>
            <div class="editor-field">
				<%: Html.TextBoxFor(model => model.configuracion.FACEBOOK)%>
				<%: Html.ValidationMessageFor(model => model.configuracion.FACEBOOK)%>
			</div> 
        </div>
         <div class="span-8 last column ">
             <div class="editor-label">
					<%: Html.LabelFor(model => model.configuracion.LINKEDIN)%>
			</div>
            <div class="editor-field">
				<%: Html.TextBoxFor(model => model.configuracion.LINKEDIN)%>
				<%: Html.ValidationMessageFor(model => model.configuracion.LINKEDIN)%>
			</div> 
        </div>
        </div>
        <div class ="span-24 last">
        <div class="span-8 last column ">
             <div class="editor-label">
					<%: Html.LabelFor(model => model.configuracion.SMTPSERVER)%>
			</div>
            <div class="editor-field">
				<%: Html.TextBoxFor(model => model.configuracion.SMTPSERVER)%>
				<%: Html.ValidationMessageFor(model => model.configuracion.SMTPSERVER)%>
			</div> 
        </div>

           <div class="span-8 last column ">
             <div class="editor-label">
					<%: Html.LabelFor(model => model.configuracion.SMTPPUERTO)%>
			</div>
            <div class="editor-field">
				<%: Html.TextBoxFor(model => model.configuracion.SMTPPUERTO)%>
				<%: Html.ValidationMessageFor(model => model.configuracion.SMTPPUERTO)%>
			</div> 
        </div>

         <div class="span-8 last column ">
             <div class="editor-label">
					<%: Html.LabelFor(model => model.configuracion.EMAIL)%>
			</div>
            <div class="editor-field">
				<%: Html.TextBoxFor(model => model.configuracion.EMAIL)%>
				<%: Html.ValidationMessageFor(model => model.configuracion.EMAIL)%>
			</div> 
        </div>

         <div class="span-8 last column ">
             <div class="editor-label">
					<%: Html.LabelFor(model => model.configuracion.PWDEMAIL)%>
			</div>
            <div class="editor-field">
				<%: Html.TextBoxFor(model => model.configuracion.PWDEMAIL)%>
				<%: Html.ValidationMessageFor(model => model.configuracion.PWDEMAIL)%>
			</div> 
        </div>
    </div>
    