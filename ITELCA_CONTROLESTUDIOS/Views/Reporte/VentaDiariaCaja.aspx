﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<ITELCA_CONTROLESTUDIOS.Models.ViewModels.ReporteViewModel>" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    VentaDiariaCaja
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<h2>Resumen Caja del Día : <%:DateTime.Now.ToShortDateString() %></h2>
<div class="span-23 prepend-1 column last ui-widget-content ui-corner-all" id="Div1">
    <div class="column span-8 ">
        <table class="span-20">
            <thead class="display-label">
                <tr>
                    <td>TOTAL GENERAL</td>
                </tr>
            </thead>
            <tbody  class="display-field">
                <tr><td><% var total=decimal.Parse(Model.totalReciboAzul)+decimal.Parse( Model.totalReciboBlanco);
                           Response.Write(total);%></td></tr>
            </tbody>
            </table>
    </div>
</div>
<div class="span-23 prepend-1 column last ui-widget-content ui-corner-all" id="resumen">
    <%--<div class="column span-8 ">
        <table class="span-20">
            <thead class="display-label">
                <tr>
                    <td>Tipo Documento</td><td colspan="3">Total</td>
                </tr>
            </thead>
            <tbody  class="display-field">
                <tr><td>Total Mensualidades</td><td colspan="3"><%:Model.totalReciboAzul %></td></tr>
                 <tr><td colspan="4">
                        <table class="span-20">
                            <thead class="display-label">
                                <tr><td>Forma de Pago</td><td>Total</td></tr>
                            </thead>
                            <tbody class="display-field">
                                 <%
                                     foreach(var ope in Model.detalleReciboAzul){
                                         if (!forma.Contains(ope.FORMAPAGO_ID))
                                         {
                                             forma.Add(ope.FORMAPAGO_ID);
                                    %>
                                    <tr>
                                        <td><%:ope.FORMASPAGOS.NOMBRE%></td>
                                        <td><%:Model.detalleReciboAzul.Where(u=>u.FORMAPAGO_ID==ope.FORMAPAGO_ID).Sum(u=>u.MONTO)%></td>
                                    </tr>
            
                                <%}
                                     } %>
                            </tbody>
                        </table>
                    </td></tr>   
            </tbody>
        </table>
    </div>--%>
    <div class="column span-8 ">
        <table class="span-20">
            <thead class="display-label">
                <tr>
                    <td>Tipo Documento</td><td colspan="3">Total</td>
                </tr>
            </thead>
            <tbody  class="display-field">
                <tr><td>Total en Productos</td><td colspan="3"><%:Model.totalReciboBlanco %></td></tr>
                 <tr><td colspan="4">
                        <table class="span-20">
                            <thead class="display-label">
                                <tr><td>Forma de Pago</td><td>Total</td></tr>
                            </thead>
                            <tbody class="display-field">
                                 <%
                                     List<decimal> forma = new List<decimal>();
                                     foreach(var ope in Model.detalleReciboBlanco){
                                         if (!forma.Contains(ope.FORMAPAGO_ID))
                                         {
                                             forma.Add(ope.FORMAPAGO_ID);
                                    %>
                                        <tr>
                                            <td><%:ope.FORMASPAGOS.NOMBRE%></td>
                                            <td><%:Model.detalleReciboBlanco.Where(u=>u.FORMAPAGO_ID==ope.FORMAPAGO_ID).Sum(u=>u.MONTO)%></td>
                                        </tr>
            
                                <%      }
                                     } %>
                            </tbody>
                        </table>
                    </td></tr>   
            </tbody>
        </table>
    </div>
    <div class="column span-6 last ">
        <table class="span-20">
            <thead class="display-label">
                <tr>
                    <td>Concepto</td><td colspan="3">Total</td>
                </tr>
            </thead>
            <tbody  class="display-field">
                <tr><td>Total Inscritos</td><td colspan="3"><%:Model.totalInscritos.Count() %></td></tr>
                 <tr><td colspan="4">
                        <table class="span-20">
                            <thead class="display-label">
                                <tr><td>Promotor</td><td>Total</td></tr>
                            </thead>
                            <tbody class="display-field">
                                 <%
                                    forma.Clear();
                                    decimal resp=0;
                                     foreach(var ope in Model.totalInscritos){
                                         resp = ope.EMPLEADO_ID.HasValue ? ope.EMPLEADO_ID.Value : 0;
                                         if (!forma.Contains(resp))
                                         {
                                             forma.Add(resp);
                                    %>
                                        <tr>
                                            <% if (ope.USUARIOS1 != null)
                                               {%>
                                                <td><%: ope.USUARIOS1.NOMBRE + " " + ope.USUARIOS1.APELLIDO%></td>
                                                <td><%:Model.totalInscritos.Where(u => u.EMPLEADO_ID == ope.EMPLEADO_ID).Count()%></td>
                                            <%}
                                               else
                                               {%>
                                                <td>PRE-INSCRITOS</td>
                                                <td><%:Model.totalInscritos.Where(u => u.EMPLEADO_ID == null).Count()%></td>
                                            <%} %>
                                        </tr>
            
                                <%      }
                                     } %>
                            </tbody>
                        </table>
                    </td></tr>   
            </tbody>
        </table>
    </div>
</div>




</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="CssContent" runat="server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="JSContent" runat="server">
</asp:Content>
