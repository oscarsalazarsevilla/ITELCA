﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    [Itelca - Reporte de Comisiones]
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
<div class="span-22 append-1 prepend-1 column last ui-widget-content ui-corner-all">
	<span class="introForm">Reporte > Comisiones</span>
		<% using (Html.BeginForm()) { %>
		<% Html.EnableClientValidation(); %>
        <%:Html.ValidationSummary("Error en la aplicación") %>
           <div class="span-24 column last " style="height:25px;"></div>
           <div  class="span-24 column last">
                <div  class="span-4 column">
                    <div class="editor-label">
				        <%: Html.Label("Desde")%>
			        </div>
                    <div class="editor-field">
				        <%: Html.TextBox("desde", "", new { @id="desde" })%>
			        </div> 
                </div>
                <div  class="span-4 column" >
                    <div class="editor-label">
				        <%: Html.Label("Hasta")%>
			        </div>
                    <div class="editor-field">
				        <%: Html.TextBox("hasta", "", new { @id = "hasta" })%>
			        </div> 
                </div>
                <div  class="span-16 column last" style="margin-top:15px;">
                    <input type="button" value="Consultar" class="button" id="consultarComisiones" />
                </div>
            </div>
            <div class="span-24 column last " style="height:25px;">
                <div id="red" style="color:red;">Debe especificar una fecha de inicio y una fecha fin.</div>
            </div>
            <div class="span-18 column last">
                <table  id="tablaComisiones">
                        <thead>
                            <tr>
                                <th>Usuario Id</th>
                                <th>Nombre</th>
                                <th>Apellido</th>
                                <th>Nro. Documento</th>
                                <th>Personas Inscritas</th>
                            </tr>    
                        </thead>
                        <tbody>

                        </tbody>
                    </table>

            </div>
            
				
          
			
    <% } %>
     
</div>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="CssContent" runat="server">
 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="JsContent" runat="server">
    <script src="/Scripts/jquery.dataTables.js" type="text/javascript"></script>
    <script src="/Scripts/Views/reportes.js" type="text/javascript"></script>
</asp:Content>

