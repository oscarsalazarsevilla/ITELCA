﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ITELCA_CONTROLESTUDIOS.Models.ViewModels.SedeViewModel>" %>
       <div class="span-24 column last">
            <div class="span-8 column">
                <div class ="span-24 last">
                    <div class="editor-label">
					    <%: Html.LabelFor(model => model.sede.NOMBRE)%>
			        </div>
                    <div class="editor-field">
				        <%: Html.TextBoxFor(model => model.sede.NOMBRE)%>
				        <%: Html.ValidationMessageFor(model => model.sede.NOMBRE)%>
			        </div> 
                </div>
                <div class ="span-24 last">
                    <div class="editor-label">
					    <%: Html.LabelFor(model => model.sede.DIRECCION)%>
			        </div>
                    <div class="editor-field">
				        <%: Html.TextAreaFor(model => model.sede.DIRECCION)%>
				        <%: Html.ValidationMessageFor(model => model.sede.DIRECCION)%>
			        </div> 
                </div>
                <div class ="span-24 last">                      
                    <div class="editor-label">
					    <%: Html.LabelFor(model => model.sede.TELEFONO1)%>
			        </div>
                    <div class="editor-field">
				        <%: Html.TextBoxFor(model => model.sede.TELEFONO1, new {@class="telefono" })%>
				        <%: Html.ValidationMessageFor(model => model.sede.TELEFONO1)%>
			        </div> 
                </div>
   
                <div class ="span-24 last">
                    <div class="editor-label">
					    <%: Html.LabelFor(model => model.sede.TELEFONO2)%>
			        </div>
                    <div class="editor-field">
				        <%: Html.TextBoxFor(model => model.sede.TELEFONO2, new { @class = "telefono" })%>
				        <%: Html.ValidationMessageFor(model => model.sede.TELEFONO2)%>
			        </div> 
                </div>
                 <div class ="span-24 last">
                    <div class="editor-label">
					    <%: Html.LabelFor(model => model.sede.LATITUD)%>
			        </div>
                    <div class="editor-field">
				        <%: Html.TextBoxFor(model => model.sede.LATITUD)%>
				        <%: Html.ValidationMessageFor(model => model.sede.LATITUD)%>
			        </div> 
                </div>
                 <div class ="span-24 last">
                        <div class="editor-label">
				            <%: Html.LabelFor(model => model.sede.LONGITUD)%>
			            </div>
                        <div class="editor-field">
				            <%: Html.TextBoxFor(model => model.sede.LONGITUD)%>
				            <%: Html.ValidationMessageFor(model => model.sede.LONGITUD)%>
			            </div> 
                </div>
                <div class="span-24 last">
                        <div class="editor-label">
				            <%: Html.LabelFor(model => model.sede.CODIGO)%>
			            </div>
                        <div class="editor-field">
				            <%: Html.TextBoxFor(model => model.sede.CODIGO)%>
				            <%: Html.ValidationMessageFor(model => model.sede.CODIGO)%>
			            </div> 
                </div>
            </div>
            <div class="span-15 prepend-1 column last">
                 <div class=" prepend-2 separator column span-22" style="height:400px;" >
                    <div id='map_canvas' style='width:90%; height:500px;'></div>
                </div>
            </div>
       </div>
       