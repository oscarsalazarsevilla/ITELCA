﻿   <%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ITELCA_CONTROLESTUDIOS.Models.ViewModels.AsistenciaViewModel>" %>
         
<div class="span-24 column last">
    <div class="span-8 column">
            <div class="editor-label">
			    <%: Html.Label("Sedes")%>
		    </div>
        <div class="editor-field">
			<%: Html.DropDownList("sede", Model.sede, "--Seleccione--")%>
			<%: Html.ValidationMessage("sede")%>
		</div> 
    </div>  
        <div class="span-8 column last">
            <div class="editor-label">
			    <%: Html.Label("Cursos")%>
		    </div>
        <div class="editor-field">
			<%: Html.DropDownList("curso", Model.curso, "--Seleccione--")%>
			<%: Html.ValidationMessage("curso")%>
		</div> 
    </div>
  </div>
<div class="span-24 column last">
    <div class="span-8 column">
        <div class="editor-label">
		    <%: Html.Label("Módulos")%>
	    </div>
        <div class="editor-field">
		    <%: Html.DropDownList("modulo", Model.modulo, "--Seleccione--")%>
		    <%: Html.ValidationMessage("modulo")%>
	    </div> 
    </div>
    <div class="span-8 column last">
        <div class="editor-label">
		    <%: Html.Label("Profesores")%>
	    </div>
        <div class="editor-field">
		    <%: Html.DropDownList("profesor", Model.profesor, "--Seleccione--")%>
		    <%: Html.ValidationMessage("profesor")%>
	    </div> 
    </div>
</div>