﻿   <%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ITELCA_CONTROLESTUDIOS.Models.ViewModels.AsistenciaViewModel>" %>
         
  <div class="span-24 column last">
        <div class="span-16 column">
            <div class="editor-label">
			    <%: Html.Label("Cursos")%>
		    </div>
        <div class="editor-field">
			<%: Html.DropDownListFor(model => model.curso, Model.curso, "--Seleccione--")%>
			<%: Html.ValidationMessageFor(model => model.curso)%>
		</div> 
    </div>
  </div>
<div class="span-24 column last">
    <div class="span-16 column">
        <div class="editor-label">
		    <%: Html.Label("Módulos")%>
	    </div>
        <div class="editor-field">
		    <%: Html.DropDownListFor(model => model.modulo, Model.modulo, "--Seleccione--")%>
		    <%: Html.ValidationMessageFor(model => model.modulo)%>
	    </div> 
    </div>
</div>
<div class="span-24 column last">
    <h4>PROFESOR:<%:Model.USUARIO.NOMBRE %></h4>
</div>