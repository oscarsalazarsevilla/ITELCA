﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ITELCA_CONTROLESTUDIOS.Models.ViewModels.ConceptoTipoSolicitudViewModel>" %>
       <div class ="span-24 column last">
            <div class="span-8 column last">
                 <div class="editor-label">
					    <%: Html.LabelFor(model => model.CONCEPTOS_TIPOSSOLICITUDES.CONCEPTO_ID)%>
			    </div>
                <div class="editor-field">
				    <%: Html.DropDownListFor(model => model.CONCEPTOS_TIPOSSOLICITUDES.CONCEPTO_ID, Model.ListaConceptos, "--Seleccione--")%>
				    <%: Html.ValidationMessageFor(model => model.CONCEPTOS_TIPOSSOLICITUDES.CONCEPTO_ID)%>
			    </div> 
            </div>
        </div>
        <div class ="span-24 column last">
             <div class="span-8 column last">
                <div class="editor-label">
				    <%: Html.LabelFor(model => model.CONCEPTOS_TIPOSSOLICITUDES.TIPOSOLICITUD_ID)%>
			    </div>
                <div class="editor-field">
				    <%: Html.DropDownListFor(model => model.CONCEPTOS_TIPOSSOLICITUDES.TIPOSOLICITUD_ID, Model.ListaTiposSolicitudes, "-- Seleccione --")%>
				    <%: Html.ValidationMessageFor(model => model.CONCEPTOS_TIPOSSOLICITUDES.TIPOSOLICITUD_ID)%>
			    </div> 
            </div>    
        </div>
   
    <div class ="span-24 column last">
        <div class="span-8 column last">
             <div class="editor-label">
					<%: Html.LabelFor(model => model.CONCEPTOS_TIPOSSOLICITUDES.FECHA_EFECTIVA)%>
			</div>
            <div class="editor-field">
				<%: Html.TextBoxFor(model => model.CONCEPTOS_TIPOSSOLICITUDES.FECHA_EFECTIVA, new { @class = "datepicker" })%>
				<%: Html.ValidationMessageFor(model => model.CONCEPTOS_TIPOSSOLICITUDES.FECHA_EFECTIVA)%>
			</div> 
        </div> 
     </div>
     <div class ="span-24 column last">    
            <div class="span-8 column last">
                 <div class="editor-label">
					   <%: Html.Label("Monto")%>
			    </div>
                <div class="editor-field">
				    <%: Html.TextBoxFor(model => model.CONCEPTOS_TIPOSSOLICITUDES.MONTO, new { @class="montos" })%>
				    <%: Html.ValidationMessageFor(model => model.CONCEPTOS_TIPOSSOLICITUDES.MONTO)%>
			    </div> 
            </div>     
      </div>
        
      