﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<ITELCA_CONTROLESTUDIOS.Models.ViewModels.SolicitudesViewModel>" %>


<!DOCTYPE html>

<html>
<head id="Head1" runat="server">
    <title>Constancia de Estudios</title>
    <script src="/Scripts/Views/Solicitudes.js" type="text/javascript"></script>
</head>

<body>

<div class="editor-label"><%: Html.Label("REPÚBLICA BOLIVARIANA DE VENEZUELA")%></div>
<div class="editor-label"><%: Html.Label("MINISTERIO DE EDUCACIÓN, CULTURA Y DEPORTE")%></div>
<div class="editor-label"><%: Html.Label("INSTITUTO TÉCNICO LUISA CÁCERES DE ARISMENDI")%></div>
<div class="editor-label" align="center" style="margin-top:20px;"><%: Html.Label("SOLICITUD DE CONSTANCIA DE ESTUDIOS")%></div>
<div class="separator column span-24"></div><div class="separator column span-24"></div>

 <% using (Html.BeginForm("ConstanciaEstudios", "Solicitudes", FormMethod.Post, new { id = "formConstanciaEstudios" }))
       {
     %>
         <%:Html.Hidden("usuarioId")%>    
         <%: Html.ValidationSummary(true)%>

<div class ="span-24 last column">
             <div class="span-6 column ">
                <div class="editor-label">
				    <%: Html.LabelFor(model => model.usuario.NRO_DOCUMENTO)%>
			    </div>
                <div class="editor-field">
                <% if (Model.esEstudiante == "Si")
                   { %>
				    <%: Html.TextBoxFor(model => model.usuario.NRO_DOCUMENTO, new { @disabled = "disabled" })%>
                    <%}
                   else
                   { %>
                   <%: Html.TextBoxFor(model => model.usuario.NRO_DOCUMENTO, new { @class="campo_documento" })%>
                   <%: Html.ValidationMessageFor(model => model.usuario.NRO_DOCUMENTO)%>
                    <% } %>
				    
			    </div> 
            </div>
             <% if (Model.esEstudiante == "No")
                   { %>
                <div class="span-6 last column">
                    <input type="button" value="Buscar" id="Buscar" name="buscarAlumno" class=" button buscarPorDocumento" />
                </div>
            <% } %>
            <div class="span-12 last column">
                &nbsp;
            </div>
         </div>
          <div class ="span-24 last">
            <div class="span-6 column ">
                <div class="editor-label">
				    <%: Html.Label("Codigo")%>
			    </div>
                <div class="editor-field">
				    <%: Html.TextBoxFor(model => model.usuario.USUARIO_ID, new { @disabled = "disabled" })%>
				    <%: Html.ValidationMessageFor(model => model.usuario.USUARIO_ID)%>
			    </div> 
            </div>
            <div class="span-6 column ">
                <div class="editor-label">
				    <%: Html.LabelFor(model => model.usuario.NOMBRE)%>
			    </div>
                <div class="editor-field">
				    <%: Html.TextBoxFor(model => model.usuario.NOMBRE, new { @disabled = "disabled" })%>
				    <%: Html.ValidationMessageFor(model => model.usuario.NOMBRE)%>
			    </div> 
            </div>
            <div class="span-6 column ">
                <div class="editor-label">
				    <%: Html.LabelFor(model => model.usuario.APELLIDO)%>
			    </div>
                <div class="editor-field">
				    <%: Html.TextBoxFor(model => model.usuario.APELLIDO, new { @disabled = "disabled" })%>
				    <%: Html.ValidationMessageFor(model => model.usuario.APELLIDO)%>
			    </div> 
            </div>           
        </div>
       <div class ="span-24 last">
           <div class="span-16 column last">
                <div class="editor-label">
				    <%: Html.Label("Curso")%>
			    </div>
                <div class="editor-field">
				    <%: Html.DropDownListFor(model => model.solicitud.MATRICULA_CURSO_ID, Model.ListaMatriculasCursos)%>
			    </div> 
            </div>  
        </div>
         <div class="span-24 column last ">
             
            <div class="span-24 column">
                 <div class="editor-label">
					    <%: Html.Label("Información Adicional")%>
			    </div>
                <div class="editor-field">
				     <%: Html.TextAreaFor(model => model.solicitud.OBSERVACIONES)%>
			    </div> 
            </div>    
                  
         </div>
          <div class="separator column span-24"></div>
        
        <div class="span-24 column last ">
            <p>
                <input type="button" value="Solicitar" id="GenerarConstanciaEstudios" name="ConstanciaEstudios" class=" button Generar" />
		    </p>
        </div>
         <% } %>  

<div id="dialog-message" title=""> </div>

</body>
</html>


