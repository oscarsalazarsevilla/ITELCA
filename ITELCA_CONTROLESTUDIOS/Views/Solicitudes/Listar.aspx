﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Listar
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2>Listar</h2>
<fieldset>
<div class="span-24 last">
        <p>
            <%: Html.ActionLink("Nuevo", "Crear") %>
        </p>
        <div id="msg">
        <%
            try
            {

                if (TempData["mensaje"] != null && TempData["mensaje"]  != "")
                {%>
			    <div  class="ui-state-highlight ui-corner-all" style="margin-top: 10px; padding: 0 .3em; width:30%; height:20px;">
				    <span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
				    <div><%:TempData["mensaje"]  %></div>
			    </div>
		    
          <%
                }
            }
            catch{} 
            
           %>
        </div>
            <table id="gridSolicitudes">
            </table>
            <div id="pager"></div>
</div>
</fieldset>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="CssContent" runat="server">
    <link href="/Content/Css/ui.jqgrid.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="JSContent" runat="server">
    <script src="/Scripts/grid.locale-es.js" type="text/javascript"></script>
    <script src="/Scripts/jquery.jqGrid.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            winHeight = window.innerHeight;
            wHeight = winHeight - 340;

            var gridSolicitudes = $("#gridSolicitudes").jqGrid({
                url: '/Solicitudes/ObtenerData',
                datatype: 'json',
                mtype: 'POST',
                colNames: ['Id', 'Tipo de Solicitud','Nro. Documento' , 'Alumno', 'Sede', 'Fecha Solicitud', 'Estado','Procesar', 'Eliminar'],
                colModel: [
		            { name: 'SOLICITUD_ID', index: 'SOLICITUD_ID', align: "right", resizable: true, width: 50, searchoptions: { sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']} },
		            { name: 'TIPO_SOLICITUD', index: 'TIPO_SOLICITUD', resizable: true, width: 150, searchoptions: { sopt: ['cn']} },
                    { name: 'ALUMNO_NOMBRE', index: 'ALUMNO_NOMBRE', width: 200, searchoptions: { sopt: ['cn']} },
                    { name: 'ALUMNO_NRO_DOCUMENTO', index: 'ALUMNO_NRO_DOCUMENTO', width: 100, searchoptions: { sopt: ['cn']} },
                    { name: 'MATRICULASCURSOS.OFERTASCURSOS.SEDES.NOMBRE', index: 'MATRICULASCURSOS.OFERTASCURSOS.SEDES.NOMBRE', width: 100, searchoptions: { sopt: ['cn']} },
                    { name: 'FECHA_CREACION', index: 'FECHA_CREACION', align: "right", resizable: true, width: 100, sorttype: 'date', searchoptions: { dataInit: function (elem) {
                        $(elem).datepicker({
                            changeYear: true,
                            changeMonth: true,
                            showButtonPanel: true,
                            onSelect: function () {
                                if (this.id.substr(0, 3) === "gs_") {
                                    // in case of searching toolbar
                                    setTimeout(function () {
                                        myGrid[0].triggerToolbar();
                                    }, 50);
                                } else {
                                    // refresh the filter in case of
                                    // searching dialog
                                    $(this).trigger('change');
                                }
                            }
                        });
                    }
                    }
                    },
                    { name: 'ESTADO_SOLICITUD', index: 'ESTADO_SOLICITUD', width: 100, align: "center", resizable: true, stype: "select", searchoptions: { sopt: ['eq', 'ne'], value: "0:PENDIENTE POR PAGAR;1:PAGADA;2:IMPRESA"} },
		            { name: 'Action1', index: 'Action1', align: "center", width: 50, resizable: false, sortable: false, search: false },
                    { name: 'Action2', index: 'Action2', align: "center", width: 50, resizable: false, sortable: false, search: false}],
                pager: jQuery('#pager'),
                rowNum: 500,
                height: wHeight,
                rowList: [500, 1000, 5000],
                sortname: 'SOLICITUD_ID',
                sortorder: 'desc',
                autowidth: true,
                viewrecords: true,
                caption: 'Listado de Solicitudes',
                loadComplete: function () {
                    $(".ui-icon-close").click(function () {
                        var gr = $(this).attr("id");
                        var r = confirm("Desea eliminar la solicitud?");
                        if (r == true) {
                            $.post("/Solicitudes/Eliminar", { id: gr }, function (data) {
                                var dat = ' <div  class="ui-state-highlight ui-corner-all" style="margin-top: 10px; padding: 0 .3em; width:30%; height:20px;"><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span><div>';
                                dat += data[0].msg;
                                dat += '</div></div>';
                                $("#msg").empty().html(dat);
                                gridSolicitudes.trigger('reloadGrid');

                            });
                        }
                        else {
                            x = "Cancelar";
                        }
                        // alert("adas");
                    });

                }
            }).navGrid('#pager',
            {
                edit: false, add: false, del: false, search: true, refresh: true
            },
            {}, //edit options
            {}, //add options
            {}, //del options
            {multipleSearch: true} // search options
            );
        });
    </script>
</asp:Content>
