﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<ITELCA_CONTROLESTUDIOS.Models.ViewModels.SolicitudesViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    [Itelca - Solicitudes]
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
<div class="span-22 append-1 prepend-1 column last ui-widget-content ui-corner-all">
	<span class="introForm"> Solicitudes > Procesar</span>
		<% using (Html.BeginForm()) { %>
		<% Html.EnableClientValidation(); %>
        <%:Html.HiddenFor(model => model.solicitud.SOLICITUD_ID) %>
        <%:Html.HiddenFor(model => model.solicitud.MATRICULA_CURSO_ID) %>
        <%:Html.HiddenFor(model => model.solicitud.ESTADO_SOLICITUD) %>
        <%:Html.ValidationSummary("Error en la aplicación") %>
            
            <% Html.RenderPartial("Form", Model); %>


            <% if (Model.solicitud.TIPOSOLICITUD_ID == 1)
                   {  %>
				<p><input type="button" value="Procesar" id="ProcesarSolicitud" name="EmitirConstanciaNotas" class="button" /></p>
                <%}   else if (Model.solicitud.TIPOSOLICITUD_ID == 2)
                   {  %>
				<p><input type="button" value="Procesar" id="ProcesarSolicitud" name="EmitirConstanciaEstudios" class="button" /></p>
                <%}    else if (Model.solicitud.TIPOSOLICITUD_ID == 3)
                   {  %>
				<p><input type="button" value="Procesar" id="ProcesarSolicitud" name="EmitirCartaPasantias" class="button" /></p>
                <%}    else if (Model.solicitud.TIPOSOLICITUD_ID == 4)
                   {  %>
				<p><input type="button" value="Procesar" id="ProcesarSolicitud" name="EmitirCambioTurno" class="button" /></p>
                <%} %>
        
			
    <% } %>
      <div id="msgForm" title="Información"></div>		
         <div class="column span-24">
                <%: Html.ActionLink("Volver al listado", "Listar")%>
            </div>        
</div>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="CssContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="JsContent" runat="server">
    <script src="/Scripts/jquery.maskedinput-1.2.2.js" type="text/javascript"></script>
    <script src="/Scripts/Views/Solicitudes.js" type="text/javascript"></script>
</asp:Content>

