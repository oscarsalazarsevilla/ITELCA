﻿   <%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ITELCA_CONTROLESTUDIOS.Models.ViewModels.SolicitudesViewModel>" %>
   <div class ="span-24 Formulario last">
   
   <div class ="span-24  last">
        <div class="span-5 column">
             <div class="editor-label">
					<%: Html.Label("Tipo de Solicitud")%>
			</div>
            <div class="editor-field">
                <% if (Model.solicitud.TIPOSOLICITUD_ID == 1)
                   {  %>
				<%: Html.Label("Constancia de Notas")%>
                <%}   else if (Model.solicitud.TIPOSOLICITUD_ID == 2)
                   {  %>
				<%: Html.Label("Constancia de Estudio")%>
                <%}
                   else if (Model.solicitud.TIPOSOLICITUD_ID == 3)
                   {  %>
				<%: Html.Label("Carta de Pasantias")%>
                <%}
                   else if (Model.solicitud.TIPOSOLICITUD_ID == 4)
                   {  %>
				<%: Html.Label("Cambio de Turno")%>
                <%} %>
			</div> 
        </div>
 
  
        <div class="span-5 append-1 column ">
             <div class="editor-label">
					<%: Html.LabelFor(model => model.usuario.NRO_DOCUMENTO)%>
			</div>
            <div class="editor-field">
				<%: Html.TextBoxFor(model => model.usuario.NRO_DOCUMENTO)%>
				<%: Html.ValidationMessageFor(model => model.usuario.NRO_DOCUMENTO)%>
			</div> 
        </div>
        <div class="span-6 column append-1 last">
             <div class="editor-label">
					<%: Html.LabelFor(model => model.usuario.NOMBRE)%>
			</div>
            <div class="editor-field">
				<%: Html.TextBoxFor(model => model.usuario.NOMBRE)%>
				<%: Html.ValidationMessageFor(model => model.usuario.NOMBRE)%>
			</div> 
        </div>
        <div class="span-6 column last">
             <div class="editor-label">
					<%: Html.LabelFor(model => model.usuario.APELLIDO)%>
			</div>
            <div class="editor-field">
				<%: Html.TextBoxFor(model => model.usuario.APELLIDO)%>
				<%: Html.ValidationMessageFor(model => model.usuario.APELLIDO)%>
			</div> 
        </div>
    </div>
        <div class ="span-24 last">
         <div class="span-14 column last">
                 <div class="editor-label">
					    <%: Html.Label("Curso")%>
			    </div>
                <div class="editor-field">
				    <%: Html.TextBoxFor(model => model.ofertaCurso.CURSOS.NOMBRE)%>
				    <%: Html.ValidationMessageFor(model => model.ofertaCurso.CURSOS.NOMBRE)%>
			    </div> 
            </div>
            <div class="span-10 column last">
                 <div class="editor-label">
					    <%: Html.Label("Turno")%>
			    </div>
                <div class="editor-field">
				    <%: Html.TextBoxFor(model => model.ofertaCurso.TURNOS.NOMBRE)%>
				    <%: Html.ValidationMessageFor(model => model.ofertaCurso.TURNOS.NOMBRE)%>
			    </div> 
            </div>
        </div>
        

        
        <% if (Model.solicitud.TIPOSOLICITUD_ID == 1 || Model.solicitud.TIPOSOLICITUD_ID == 2)
           {%>
        <div class="span-24 column last">
                <div class="editor-label">
					<%: Html.Label("Observaciones")%>
			</div>
            <div class="editor-field">
				<%: Html.TextAreaFor(model => model.solicitud.OBSERVACIONES)%>
				<%: Html.ValidationMessageFor(model => model.solicitud.OBSERVACIONES)%>
			</div> 
        </div>        
        <% }
           else if (Model.solicitud.TIPOSOLICITUD_ID == 3)
           {%>
        <div class ="span-24 last">
         <div class="span-8 column last">
                 <div class="editor-label">
					   <%: Html.Label("Institución")%>
			    </div>
                <div class="editor-field">
				    <%: Html.TextBoxFor(model => model.solicitud.NOMBRE_INSTITUCION_PASANTIA)%>
				    <%: Html.ValidationMessageFor(model => model.solicitud.NOMBRE_INSTITUCION_PASANTIA)%>
			    </div> 
            </div>
            <div class="span-8 column last">
                 <div class="editor-label">
					    <%: Html.Label("Director")%>
			    </div>
                <div class="editor-field">
				    <%: Html.TextBoxFor(model => model.solicitud.DIRECTOR_PASANTIA)%>
				    <%: Html.ValidationMessageFor(model => model.solicitud.DIRECTOR_PASANTIA)%>
			    </div> 
            </div>

              <div class="span-8 column last">
                 <div class="editor-label">
					    <%: Html.Label("Fecha Inicio Pasantias")%>
			    </div>
                <div class="editor-field">
				    <%: Html.TextBoxFor(model => model.solicitud.FECHA_INICIO_PASANTIA, new { @class = "datepicker" })%>
				    <%: Html.ValidationMessageFor(model => model.solicitud.FECHA_INICIO_PASANTIA)%>
			    </div> 
            </div>
        </div>
        
        <div class="span-24 column last">
                <div class="editor-label">
					<%: Html.Label("Observaciones")%>
			</div>
            <div class="editor-field">
				<%: Html.TextAreaFor(model => model.solicitud.OBSERVACIONES)%>
				<%: Html.ValidationMessageFor(model => model.solicitud.OBSERVACIONES)%>
			</div> 
        </div>   

        <%} else{ %>

        <div class="span-24 column last">
                <div class="editor-label">
					<%: Html.Label("Motivo del Cambio")%>
			</div>
            <div class="editor-field">
				<%: Html.TextAreaFor(model => model.solicitud.OBSERVACIONES)%>
				<%: Html.ValidationMessageFor(model => model.solicitud.OBSERVACIONES)%>
			</div> 
        </div>

        <div class="span-24 column last">
                <div class="editor-label">
					<%: Html.Label("Observaciones")%>
			</div>
            <div class="editor-field">
				<%: Html.TextAreaFor(model => model.solicitud.OBSERVACIONES)%>
				<%: Html.ValidationMessageFor(model => model.solicitud.OBSERVACIONES)%>
			</div> 
        </div>

        <% } %>
 
      </div>