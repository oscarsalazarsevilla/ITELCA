﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<ITELCA_CONTROLESTUDIOS.Models.ViewModels.SolicitudesViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    [Itelca - Solicitudes]
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
<div class="span-22 append-1 prepend-1 column last ui-widget-content ui-corner-all">
	<span class="introForm"> Solicitudes > Nueva</span>
		<% using (Html.BeginForm()) { %>
		<% Html.EnableClientValidation(); %>
        <%:Html.ValidationSummary("Error en la aplicación") %>
      

            <div id="k2QuickIcons">
                <div class="icon-wrapper"  id="btnConstanciaNotas">
                    <div class="icon">
                        <a href="#">
                            <img src="/Content/Imagenes/tabla_de_notas.png" width="50" alt="Constancia de Notas"/>
                            <span>Constancia de Notas</span>
                        </a>
                    </div>
                </div>
                 <div class="icon-wrapper" id="btnConstanciaEstudios">
                    <div class="icon">
                        <a href="#">
                            <img src="/Content/Imagenes/images.jpg" width="50" alt="Constancia de Estudios"/>
                            <span>Constancia de Estudios</span>
                        </a>
                    </div>
                </div>
                 <div class="icon-wrappe" id="btnCartaPasantias">
                    <div class="icon">
                        <a href="#">
                            <img src="/Content/Imagenes/id_card.png" width="50" alt="Constancia de Pasantias" />
                            <span>Constancia de Pasantias</span>
                        </a>
                    </div>
                </div>
                 <div class="icon-wrapper" id="btnCambioTurno">
                    <div class="icon">
                        <a href="#">
                            <img src="/Content/Imagenes/images_turno.jpg" width="50" alt="Cambio de Turno" />
                            <span>Cambio de Turno</span>
                        </a>
                    </div>
                </div>

           </div>
         
            <div id="dialogForm" title="Nueva Solicitud">         
                <div id="contenidoHtml" class="span-24 column last">
                </div>
            </div>
	        <div id="msgForm" title="Información"></div>		
    <% } %>
     
      <div class="span-24 last column">
        <% if (Model.esEstudiante == "Si") 
       { %>
		 <% Html.RenderPartial("gridSolicitudes"); %>
        <% } %>
	</div> 
    
                
</div>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="CssContent" runat="server">
    <link href="../../Content/Css/Site.css" rel="stylesheet" type="text/css" />
    <style rel="stylesheet" type="text/css">
    #k2QuickIcons div.icon a{
        height: 112px;
    }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="JsContent" runat="server">
<script src="/Scripts/jquery.maskedinput-1.2.2.js" type="text/javascript"></script>
    <script src="/Scripts/Views/Solicitudes.js" type="text/javascript"></script>
</asp:Content>

