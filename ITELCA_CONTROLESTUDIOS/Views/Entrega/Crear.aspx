﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<ITELCA_CONTROLESTUDIOS.Models.ViewModels.EntregaViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    [Itelca - Entrega]
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
<div class="span-22 append-1 prepend-1 column last ui-widget-content ui-corner-all">
	<span class="introForm">Entrega > Crear</span>
		<% using (Html.BeginForm()) { %>
		<% Html.EnableClientValidation(); %>
        <%:Html.ValidationSummary("Error en la aplicación") %>

            <div  class="span-24 Formulario  column last">
				<% Html.RenderPartial("Form",Model); %>
			</div>
             <div class="separator column span-24">
            </div>
            <p>
				<input type="submit" value="Crear" class="button" />
			</p>
           
            <div class="column span-24">
                <%: Html.ActionLink("Volver al listado", "Listar")%>
            </div>
          
			
    <% } %>
     
                
</div>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="CssContent" runat="server">
 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="JsContent" runat="server">
 <script src="/Scripts/jquery.maskedinput-1.2.2.js" type="text/javascript"></script>
<script type="text/javascript">
    function ObtenerFecha() {
        var f = new Date();
        var dia,mes,anio;
        if (f.getDate()<10)
        return f.getDate() + "/" + (f.getMonth() + 1) + "/" + f.getFullYear();
    }
    $(document).ready(function () {
        $('.datepicker').datepicker({ dateFormat: 'dd/mm/yy' });
        $("#entrega_CANTIDAD").val(1);
        $("#entrega_FECHA_ENTREGA").mask('99/99/999');
        $("#entrega_FECHA_ENTREGA").val(ObtenerFecha());
    });
</script>
</asp:Content>

