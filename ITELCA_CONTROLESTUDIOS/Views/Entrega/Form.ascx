﻿   <%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ITELCA_CONTROLESTUDIOS.Models.ViewModels.EntregaViewModel>" %>
       <div class ="span-24 last">
            <div class="span-8 column last">
                 <div class="editor-label">
					    <%: Html.LabelFor(model => model.entrega.DETALLERECIBO_ID)%>
			    </div>
                <div class="editor-field">
				    <%: Html.DropDownListFor(model => model.entrega.DETALLERECIBO_ID, Model.recibo, "-- Seleccione --")%>
				    <%: Html.ValidationMessageFor(model => model.entrega.DETALLERECIBO_ID)%>
			    </div> 
            </div>
        </div>
       <div class ="span-24 last">
            <div class="span-8 column last">
                 <div class="editor-label">
					    <%: Html.LabelFor(model => model.entrega.SEDE_ID)%>
			    </div>
                <div class="editor-field">
				    <%: Html.DropDownListFor(model => model.entrega.SEDE_ID,Model.sedes,"--Seleccione--")%>
				    <%: Html.ValidationMessageFor(model => model.entrega.SEDE_ID)%>
			    </div> 
            </div>
        </div>
         <div class ="span-24 last">
            <div class="span-8 column last">
                 <div class="editor-label">
					    <%: Html.LabelFor(model => model.entrega.CANTIDAD)%>
			    </div>
                <div class="editor-field">
				    <%: Html.TextBoxFor(model => model.entrega.CANTIDAD)%>
				    <%: Html.ValidationMessageFor(model => model.entrega.CANTIDAD)%>
			    </div> 
            </div>
       </div>
       <div class="span-24 last column ">
        <div class="span-8 column last">
             <div class="editor-label">
					<%: Html.LabelFor(model => model.entrega.FECHA_ENTREGA)%>
			</div>
            <div class="editor-field">
				<%: Html.TextBoxFor(model => model.entrega.FECHA_ENTREGA, new { @class = "datepicker" })%>
				<%: Html.ValidationMessageFor(model => model.entrega.FECHA_ENTREGA)%>
			</div> 
        </div>    
    </div> 
   
    