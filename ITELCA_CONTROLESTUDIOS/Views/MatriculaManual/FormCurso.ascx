﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ITELCA_CONTROLESTUDIOS.Models.ViewModels.MatriculaManualViewModel>" %>
            
            <div id="inscripcionCurso" >

                <h2>Alumno</h2>
                <section>
                 <% using (Html.BeginForm("InscripcionManualCurso", "MatriculaManual", FormMethod.Post, new { id = "formManualCurso" }))
                 {%>
                       <% Html.EnableClientValidation(); %>
                        <%: Html.Hidden("USUARIOCURSO.USUARIO_ID")%>
                        <div class ="span-24 last">
                            <div class="span-8 column ">
                                 <div class="editor-label">
					                    <%: Html.LabelFor(model => model.USUARIOCURSO.NRO_DOCUMENTO)%>
			                    </div>
                                <div class="editor-field">
				                    <%: Html.TextBoxFor(model => model.USUARIOCURSO.NRO_DOCUMENTO)%>
				                    <%: Html.ValidationMessageFor(model => model.USUARIOCURSO.NRO_DOCUMENTO)%>
			                    </div> 
                            </div>
                            <div class="span-8 column">
                                <div class="editor-label">
					                    <%: Html.LabelFor(model => model.USUARIOCURSO.APELLIDO)%>
			                    </div>
                                <div class="editor-field">
				                    <%: Html.TextBoxFor(model => model.USUARIOCURSO.APELLIDO)%>
				                    <%: Html.ValidationMessageFor(model => model.USUARIOCURSO.APELLIDO)%>
			                    </div>
                                 
                            </div>
                            <div class="span-8 column last">
                                <div class="editor-label">
					                    <%: Html.LabelFor(model => model.USUARIOCURSO.NOMBRE)%>
			                    </div>
                                <div class="editor-field">
				                    <%: Html.TextBoxFor(model => model.USUARIOCURSO.NOMBRE)%>
				                    <%: Html.ValidationMessageFor(model => model.USUARIOCURSO.NOMBRE)%>
			                    </div> 
                            </div>
                         
                       </div>
   
                        <div class ="span-24 last">
                             <div class="span-8 column ">
                                <div class="editor-label">
					                    <%: Html.LabelFor(model => model.USUARIOCURSO.GENERO_ID)%>
			                    </div>
                                <div class="editor-field">
				                    <%: Html.DropDownListFor(model => model.USUARIOCURSO.GENERO_ID, Model.generos)%>
				                    <%: Html.ValidationMessageFor(model => model.USUARIOCURSO.GENERO_ID)%>
			                    </div> 
                            </div>
                            <div class="span-8 column ">
                                <div class="editor-label">
					                    <%: Html.LabelFor(model => model.USUARIOCURSO.FECHADENACIMIENTO)%>
			                    </div>
                                <div class="editor-field">
				                    <%: Html.TextBoxFor(model => model.USUARIOCURSO.FECHADENACIMIENTO, new { @class = "datepicker", style = "position: relative; z-index: 100000;" })%>
				                    <%: Html.ValidationMessageFor(model => model.USUARIOCURSO.FECHADENACIMIENTO)%>
			                    </div> 
                            </div>
                             <div class="span-8 column last">
                                <div class="editor-label">
					                    <%: Html.LabelFor(model => model.USUARIOCURSO.DIRECCION)%>
			                    </div>
                                <div class="editor-field">
				                    <%: Html.TextAreaFor(model => model.USUARIOCURSO.DIRECCION)%>
				                    <%: Html.ValidationMessageFor(model => model.USUARIOCURSO.DIRECCION)%>
			                    </div> 
                            </div>
       
                       </div>
                        <div class="span-24 column last">
                            <div class="span-8 column ">
                                 <div class="editor-label">
					                    <%: Html.LabelFor(model => model.USUARIOCURSO.TELEFONO2)%>
			                    </div>
                                <div class="editor-field">
				                    <%: Html.TextBoxFor(model => model.USUARIOCURSO.TELEFONO2)%>
				                    <%: Html.ValidationMessageFor(model => model.USUARIOCURSO.TELEFONO2)%>
			                    </div> 
                            </div>
                            <div class="span-8 column">
                                <div class="editor-label">
					                    <%: Html.LabelFor(model => model.USUARIOCURSO.TELEFONO)%>
			                    </div>
                                <div class="editor-field">
				                    <%: Html.TextBoxFor(model => model.USUARIOCURSO.TELEFONO)%>
				                    <%: Html.ValidationMessageFor(model => model.USUARIOCURSO.TELEFONO)%>
			                    </div>
                            </div>
                            <div class="span-8 column last">
                                <div class="editor-label">
					                    <%: Html.LabelFor(model => model.USUARIOCURSO.EMAIL)%>
			                    </div>
                                <div class="editor-field">
				                    <%: Html.TextBoxFor(model => model.USUARIOCURSO.EMAIL)%>
				                    <%: Html.ValidationMessageFor(model => model.USUARIOCURSO.EMAIL)%>
			                    </div>
                            </div>
                        </div>
                    <%} %>
                </section>
                <h2>Curso</h2>
                <section>
                    <div class ="span-24 last">
                        <div class="span-8 column ">
                            <div class="editor-label">
				                <%: Html.LabelFor(model => model.MATRICULASCURSOS.CURSO_ID)%>
			                </div>
                            <div class="editor-field">
				                <%: Html.DropDownListFor(model => model.MATRICULASCURSOS.CURSO_ID, Model.listaCursos,"--Seleccione--")%>
				                <%: Html.ValidationMessageFor(model => model.MATRICULASCURSOS.CURSO_ID)%>
			                </div> 
                        </div>
                        <div class="span-8 column ">
                            <div class="editor-label">
				                <%: Html.LabelFor(model => model.MATRICULASCURSOS.SEDE_ID)%>
			                </div>
                            <div class="editor-field">
				                <%: Html.DropDownListFor(model => model.MATRICULASCURSOS.SEDE_ID, new SelectList(new Dictionary<string, string>()))%>
				                <%: Html.ValidationMessageFor(model => model.MATRICULASCURSOS.SEDE_ID)%>
			                </div> 
                        </div>
                        <div class="span-8 column last">
                            <div class="editor-label">
				                <%: Html.LabelFor(model => model.MATRICULASCURSOS.TURNO_ID)%>
			                </div>
                            <div class="editor-field">
				                <%: Html.DropDownListFor(model => model.MATRICULASCURSOS.TURNO_ID, new SelectList(new Dictionary<string, string>()))%>
				                <%: Html.ValidationMessageFor(model => model.MATRICULASCURSOS.TURNO_ID)%>
			                </div> 
                        </div>
                </div>
                
                <div class="separator column span-24"></div>
                <div id="divModulos" class="column span-24 last" style="height:200px;overflow:auto;">
                </div>
            </section>
               <h2>Resumen</h2>
                <section id="resumen">
                    <div  style="border: 3px #006699 solid; margin: 7px;" class="ofertaI span-24 column last" >
                        <div id="dataResumen" class="span-24 column last"></div>
                        <div class="span-24 column last">
                            <div class="span-8 column ">
                                <div class="editor-label">
				                    <%: Html.Label("Atendido Por:")%>
			                    </div>
                                <div class="editor-field">
				                    <%: Html.DropDownList("empleadoId",Model.listaEmpleados,"-Seleccione--")%>
			                    </div> 
                            </div>
                            <div class="span-4 prepend-1 column ">
                                <div class="editor-label">
				                    <%: Html.Label("Nro Control")%>
			                    </div>
                                <div class="editor-field">
				                    <%: Html.TextBox("nroControl","")%>
			                    </div> 
                            </div>
                        </div>
                    </div>
                </section>
            </div>
          
