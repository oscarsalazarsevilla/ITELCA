﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<ITELCA_CONTROLESTUDIOS.Models.ViewModels.MatriculaManualViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
  [Itelca - Inscipción]
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<h2>Index</h2>
 <div id="dialog-message">
        <div id="delete-message"></div>    
    </div>
<div class="span-24 column last">
    <div class="span-24 column last" id="acciones">
        <div class="span-4 column" >
            <input class="buttonManual" value="Inscripción Curso" id="curso" type="button"/>
        </div>
        <div class="span-4 column last" >
            <input class="buttonManual" value="Inscripción Taller"  id="taller" type="button"/>
        </div>

    </div>
    <div class="separator"></div>
    <div class="span-24 column last" id="divHtmlCurso">
        <div class="span-22 append-1 prepend-1 column last ui-widget-content ui-corner-all">
	        <span class="introForm">Matricula Curso > Inscripción</span>
                <div  class="span-24 Formulario  column last" style="font-size:110%">
				    <% Html.RenderPartial("FormCurso", Model); %>
			    </div>
            <div id="dialogConfirmar">
         
            </div>
        </div>
    </div>
    <div class="span-24 column last" id="divHtmlTaller" style="display:none">
        <div class="span-22 append-1 prepend-1 column last ui-widget-content ui-corner-all">
	        <span class="introForm">Matricula Taller > Inscripción</span>
      
	        <% using (Html.BeginForm("InscripcionManualTaller", "MatriculaManual", FormMethod.Post, new { id = "formManualTaller" }))
             {%>
		        <% Html.EnableClientValidation(); %>
                    <div  class="span-24 Formulario  column last" style="font-size:110%">
				        <% Html.RenderPartial("FormTaller", Model); %>
			        </div>
              <%} %>
              <div id="dialogConfirmarTaller">
         
              </div>
        </div>
    </div>    
</div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="CssContent" runat="server">
 <link href="/Content/Css/stepsCE.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="JSContent" runat="server">
    <script src="/Scripts/jquery.steps.min.js" type="text/javascript"></script>
    <script src="/Scripts/autoNumeric.js" type="text/javascript"></script>
    <script src="/Scripts/jquery.maskedinput-1.2.2.js" type="text/javascript"></script>
    <script src="/Scripts/Views/matriculaManual.js" type="text/javascript"></script>
    <script src="/Scripts/Views/matriculaManualCurso.js" type="text/javascript"></script>
    <script src="/Scripts/Views/matriculaManualTaller.js" type="text/javascript"></script>
</asp:Content>
