﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ITELCA_CONTROLESTUDIOS.Models.ViewModels.MatriculaManualViewModel>" %>
            <%: Html.Hidden("USUARIOTALLER.USUARIO_ID")%>
            <div id="inscripcionTaller" >
                <h2>Alumno</h2>
                <section>
                    <div class ="span-24 last">
                        <div class="span-8 column ">
                             <div class="editor-label">
					                <%: Html.LabelFor(model => model.USUARIOTALLER.NRO_DOCUMENTO)%>
			                </div>
                            <div class="editor-field">
				                <%: Html.TextBoxFor(model => model.USUARIOTALLER.NRO_DOCUMENTO)%>
				                <%: Html.ValidationMessageFor(model => model.USUARIOTALLER.NRO_DOCUMENTO)%>
			                </div> 
                        </div>
                        <div class="span-8 column">
                            <div class="editor-label">
					                <%: Html.LabelFor(model => model.USUARIOTALLER.NOMBRE)%>
			                </div>
                            <div class="editor-field">
				                <%: Html.TextBoxFor(model => model.USUARIOTALLER.NOMBRE)%>
				                <%: Html.ValidationMessageFor(model => model.USUARIOTALLER.NOMBRE)%>
			                </div> 
                        </div>
                        <div class="span-8 column last">
                             <div class="editor-label">
					                <%: Html.LabelFor(model => model.USUARIOTALLER.APELLIDO)%>
			                </div>
                            <div class="editor-field">
				                <%: Html.TextBoxFor(model => model.USUARIOTALLER.APELLIDO)%>
				                <%: Html.ValidationMessageFor(model => model.USUARIOTALLER.APELLIDO)%>
			                </div> 
                        </div>
                         
                   </div>
   
                    <div class ="span-24 last">
                         <div class="span-8 column ">
                             <div class="editor-label">
					                <%: Html.LabelFor(model => model.USUARIOTALLER.FECHADENACIMIENTO)%>
			                </div>
                            <div class="editor-field">
				                <%: Html.TextBoxFor(model => model.USUARIOTALLER.FECHADENACIMIENTO, new { @class = "datepicker" })%>
				                <%: Html.ValidationMessageFor(model => model.USUARIOTALLER.FECHADENACIMIENTO)%>
			                </div> 
                        </div>
                        <div class="span-8 column ">
                             <div class="editor-label">
					                <%: Html.LabelFor(model => model.USUARIOTALLER.EMAIL)%>
			                </div>
                            <div class="editor-field">
				                <%: Html.TextBoxFor(model => model.USUARIOTALLER.EMAIL)%>
				                <%: Html.ValidationMessageFor(model => model.USUARIOTALLER.EMAIL)%>
			                </div> 
                        </div>
                         <div class="span-8 column ">
                             <div class="editor-label">
					                <%: Html.LabelFor(model => model.USUARIOTALLER.TELEFONO)%>
			                </div>
                            <div class="editor-field">
				                <%: Html.TextBoxFor(model => model.USUARIOTALLER.TELEFONO)%>
				                <%: Html.ValidationMessageFor(model => model.USUARIOTALLER.TELEFONO)%>
			                </div> 
                        </div>
       
                   </div>
                    <div class="span-24">
                        <div class="span-8 column ">
                             <div class="editor-label">
					                <%: Html.LabelFor(model => model.USUARIOTALLER.TELEFONO2)%>
			                </div>
                            <div class="editor-field">
				                <%: Html.TextBoxFor(model => model.USUARIOTALLER.TELEFONO2)%>
				                <%: Html.ValidationMessageFor(model => model.USUARIOTALLER.TELEFONO2)%>
			                </div> 
                        </div>
                        <div class="span-8 column">
                            <div class="editor-label">
					                <%: Html.LabelFor(model => model.USUARIOTALLER.DIRECCION)%>
			                </div>
                            <div class="editor-field">
				                <%: Html.TextAreaFor(model => model.USUARIOTALLER.DIRECCION)%>
				                <%: Html.ValidationMessageFor(model => model.USUARIOTALLER.DIRECCION)%>
			                </div> 
                        </div>
                        <div class="span-8 column">
                            <div class="editor-label">
					                <%: Html.LabelFor(model => model.USUARIOTALLER.GENERO_ID)%>
			                </div>
                            <div class="editor-field">
				                <%: Html.DropDownListFor(model => model.USUARIOTALLER.GENERO_ID,Model.generos)%>
				                <%: Html.ValidationMessageFor(model => model.USUARIOTALLER.GENERO_ID)%>
			                </div> 
                        </div>
                    </div>
                </section>
                <h2>Taller</h2>
                <section>
                    <div class ="span-24 last">
                        <div class="span-16 column ">
                            <div class="editor-label">
				                <%: Html.Label("Talleres")%>
			                </div>
                            <div class="editor-field">
				                <%: Html.DropDownListFor(model => model.MATRICULASTALLERES.OFERTA_TALLER_ID, Model.listaTaller,"--Seleccione--")%>
				                <%: Html.ValidationMessageFor(model => model.MATRICULASTALLERES.OFERTA_TALLER_ID)%>
			                </div> 
                        </div>

                </div>
                
                <div class="separator column span-24"></div>
                <div id="divTalleres" class="column span-24 last" style="height:200px;overflow:auto;">
                </div>
            </section>
                <h2>Resumen</h2>
                <section id="resumenTaller">
                    <div  style="border: 3px #006699 solid; margin: 7px;" class="ofertaI span-24 column last" >
                        <div id="dataResumenTaller" class="span-24 column last"></div>
                    </div>
                </section>
            </div>
