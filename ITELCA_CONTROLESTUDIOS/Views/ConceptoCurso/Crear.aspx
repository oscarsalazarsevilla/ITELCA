﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<ITELCA_CONTROLESTUDIOS.Models.ViewModels.ConceptoCursoViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    [Itelca - Conceptos Cursos]
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
<div class="span-22 append-1 prepend-1 column last ui-widget-content ui-corner-all">
	<span class="introForm">Conceptos Cursos > Crear</span>
		<% using (Html.BeginForm("Crear", "ConceptoCurso", FormMethod.Post, new { id = "formConceptoCurso" })) { %>
		<% Html.EnableClientValidation(); %>
        <%:Html.ValidationSummary("Error en la aplicación") %>
        <%:Html.HiddenFor(model => model.CONCEPTOS_CURSOS.ESTADO, new { Value = "1" }) %>
            <div  class="span-24 Formulario  column last">
				<% Html.RenderPartial("Form",Model); %>
			</div>
             <div class="separator column span-24">
            </div>
            <p>
				<input type="submit" value="Crear" class="button" />
			</p>
           
            <div class="column span-24">
                <%: Html.ActionLink("Volver al listado", "Listar")%>
            </div>
          
			
    <% } %>
</div>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="CssContent" runat="server">
    <link href="/Content/Css/multiselect.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="JsContent" runat="server">
 <script src="/Scripts/autoNumeric.js" type="text/javascript"></script> 
    <script src="../../Scripts/jquery.maskedinput-1.2.2.js" type="text/javascript"></script>
 <script src="/Scripts/jquery.multiselect.min.js" type="text/javascript"></script>
 <script src="/Scripts/jquery.multiselect.es.js" type="text/javascript"></script>
 <script src="/Scripts/Views/ConceptoCurso.js" type="text/javascript"></script>
 <script type="text/javascript">

     $(document).ready(function () {
         $("#CONCEPTOS_CURSOS_CURSO_ID").multiselect({
             selectedText: "# de # seleccionado",
             selectedList: 5,
             checkAll: function (event, ui) {
             },
             click: function (event, ui) {
             }

         });

         $("#CONCEPTOS_CURSOS_TURNO_ID").multiselect({
             selectedText: "# de # seleccionado",
             selectedList: 5,
             checkAll: function (event, ui) {
             },
             click: function (event, ui) {
             }

         });
     });
 



 </script>
</asp:Content>

