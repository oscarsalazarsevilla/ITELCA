﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ITELCA_CONTROLESTUDIOS.Models.ViewModels.ConceptoCursoViewModel>" %>
       <div class ="span-24 column last">
            <div class="span-8 column last">
                 <div class="editor-label">
					    <%: Html.LabelFor(model => model.CONCEPTOS_CURSOS.CONCEPTO_ID)%>
			    </div>
                <div class="editor-field">
				    <%: Html.DropDownListFor(model => model.CONCEPTOS_CURSOS.CONCEPTO_ID, Model.ListaConceptos, "--Seleccione--")%>
				    <%: Html.ValidationMessageFor(model => model.CONCEPTOS_CURSOS.CONCEPTO_ID)%>
			    </div> 
            </div>
        </div>

        <div class ="span-24 column last">
             <div class="span-8 column last">
                <div class="editor-label">
				    <%: Html.LabelFor(model => model.CONCEPTOS_CURSOS.CURSO_ID)%>
			    </div>
                <div class="editor-field">
				    <%: Html.ListBoxFor(model => model.CONCEPTOS_CURSOS.CURSO_ID, Model.MultiCursos, new { @size = 3, @class = "multiselect", title = "Click para seleccionar una opción" })%>
				    <%: Html.ValidationMessageFor(model => model.CONCEPTOS_CURSOS.CURSO_ID)%>
			    </div> 
            </div>  
        </div>

        <div class ="span-24 column last">
            <div class="span-8 column last">
                <div class="editor-label">
				    <%: Html.LabelFor(model => model.CONCEPTOS_CURSOS.TURNO_ID)%>
			    </div>
                <div class="editor-field">
				    <%: Html.ListBoxFor(model => model.CONCEPTOS_CURSOS.TURNO_ID, Model.MultiTurnos, new { @size = 3, @class = "multiselect", title = "Click para seleccionar una opción" })%>
				    <%: Html.ValidationMessageFor(model => model.CONCEPTOS_CURSOS.TURNO_ID)%>
			    </div> 
            </div>       
        </div>
   
   
    <div class="span-24 last column ">
        <div class="span-8 column last">
             <div class="editor-label">
					<%: Html.LabelFor(model => model.CONCEPTOS_CURSOS.FECHA_EFECTIVA)%>
			</div>
            <div class="editor-field">
				<%: Html.TextBoxFor(model => model.CONCEPTOS_CURSOS.FECHA_EFECTIVA, new { @class = "datepicker" })%>
				<%: Html.ValidationMessageFor(model => model.CONCEPTOS_CURSOS.FECHA_EFECTIVA)%>
			</div> 
        </div>    
    </div> 
    <div class ="span-24 column last">
        <div class="span-8 column last">
             <div class="editor-label">
				<%: Html.Label("Monto")%>
			</div>
            <div class="editor-field">
				<%: Html.TextBoxFor(model => model.CONCEPTOS_CURSOS.MONTO, new { @class="montos" })%>
				<%: Html.ValidationMessageFor(model => model.CONCEPTOS_CURSOS.MONTO)%>
			</div> 
        </div>  
    </div>   
        
      