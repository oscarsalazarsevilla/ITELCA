﻿   <%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ITELCA_CONTROLESTUDIOS.Models.ViewModels.BannerViewModel>" %>
         
    <div class ="span-24 column last">
        <div class="span-8 column ">
             <div class="editor-label">
					<%: Html.LabelFor(model => model.banner.NOMBRE)%>
			</div>
            <div class="editor-field">
				<%: Html.TextBoxFor(model => model.banner.NOMBRE)%>
				<%: Html.ValidationMessageFor(model => model.banner.NOMBRE)%>
			</div> 
        </div>
        <div class="span-8 column ">
             <div class="editor-label">
					<%: Html.LabelFor(model => model.banner.FECHA_PUBLICACION)%>
			</div>
            <div class="editor-field">
				<%: Html.TextBoxFor(model => model.banner.FECHA_PUBLICACION)%>
				<%: Html.ValidationMessageFor(model => model.banner.FECHA_PUBLICACION)%>
			</div> 
        </div>
        <div class="span-8 column last ">
             <div class="editor-label">
					<%: Html.LabelFor(model => model.banner.FECHA_FIN_PUBLICACION)%>
			</div>
            <div class="editor-field">
				<%: Html.TextBoxFor(model => model.banner.FECHA_FIN_PUBLICACION)%>
				<%: Html.ValidationMessageFor(model => model.banner.FECHA_FIN_PUBLICACION)%>
			</div> 
        </div>
    </div>
    <div class="span-24 column last">
        <div class="span-8 column ">
            <div class="editor-label">
					<%: Html.LabelFor(model => model.banner.URL)%>
			</div>
            <div class="editor-field">
				<input class="input-background"  id="file1" name="file" type="file" accept="jpg|png|gif" />
                <% if (!String.IsNullOrEmpty(Model.banner.URL))
           {%>
                        <br />
                        <a href="/Content/Web/Banner/<%=Model.banner.URL%>" target="_blank">
                            Imagen actual
                        </a>
                    <%} %>
			</div>   
        </div>
        <div class="span-8 column ">
             <div class="editor-label">
					<%: Html.LabelFor(model => model.banner.ORDEN)%>
			</div>
            <div class="editor-field">
				<%: Html.TextBoxFor(model => model.banner.ORDEN)%>
				<%: Html.ValidationMessageFor(model => model.banner.ORDEN)%>
			</div> 
        </div>
        <div class="span-8 column last ">
             <div class="editor-label">
					<%: Html.LabelFor(model => model.banner.DURACION)%>
			</div>
            <div class="editor-field">
				<%: Html.TextBoxFor(model => model.banner.DURACION)%>
				<%: Html.ValidationMessageFor(model => model.banner.DURACION)%>
			</div> 
        </div>
    </div>
    <div class="span-24 column last">
        <div class="span-24 column last">
            <div class="editor-label">
				    <%: Html.LabelFor(model => model.banner.TEXTO)%>
		    </div>
            <div class="editor-field">
			    <%: Html.TextAreaFor(model => model.banner.TEXTO)%>
			    <%: Html.ValidationMessageFor(model => model.banner.TEXTO)%>
		    </div> 
        </div>
    </div>