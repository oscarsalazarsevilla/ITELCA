﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<ITELCA_CONTROLESTUDIOS.Models.ViewModels.BannerViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    [Itelca - Banner]
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
<div class="span-22 append-1 prepend-1 column last ui-widget-content ui-corner-all">
	<span class="introForm">Banner > Editar</span>
		<% using (Html.BeginForm("Editar", "Banner", FormMethod.Post, new { enctype = "multipart/form-data" })) { %>
		<% Html.EnableClientValidation(); %>
        <%:Html.ValidationSummary("Error en la aplicación") %>
         <%:Html.HiddenFor(model => model.banner.BANNER_ID) %>
         <%:Html.HiddenFor(model => model.banner.USUARIO_ID) %>
            <div  class="span-24 Formulario  column last">
				<% Html.RenderPartial("Form",Model); %>
                <div><% if (!string.IsNullOrEmpty(Model.banner.URL))
                    {%>
                    <%:Html.HiddenFor(model => model.banner.URL)%>
                    
                     <a href="/Content/Web/Banner/<%:Model.banner.URL%>" target="_blank">Ver imagen</a>
                <% }
                   %>
            </div>

                <div class ="span-24 last">
                    <div class="span-8 column last">
                        <div class="editor-label">
				            <%: Html.LabelFor(model => model.banner.ESTADO)%>
			            </div>
                        <div class="editor-field">
				            <%: Html.DropDownListFor(model => model.banner.ESTADO, Model.activo)%>
				            <%: Html.ValidationMessageFor(model => model.banner.ESTADO)%>
			            </div> 
                    </div>
                </div>
			</div>
             <div class="separator column span-24">
            </div>
            <p>
				<input type="submit" value="Guardar" class="button" />
			</p>
           
            <div class="column span-24">
                <%: Html.ActionLink("Volver al listado", "Listar")%>
            </div>
         
			
    <% } %>
</div>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="CssContent" runat="server">
 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="JsContent" runat="server">
    <script src="/Scripts/jqueryTimePicker.js" type="text/javascript"></script>
    <script src="/Scripts/Views/banner.js" type="text/javascript"></script>
</asp:Content>

