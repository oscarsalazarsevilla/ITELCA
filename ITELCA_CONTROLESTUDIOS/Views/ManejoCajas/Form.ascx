﻿   <%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ITELCA_CONTROLESTUDIOS.Models.ViewModels.ManejoCajaViewModel>" %>
       <div class ="span-24 last">
            <div class="span-8 column last">
                 <div class="editor-label">
					    <%: Html.LabelFor(model => model.manejoCaja.SEDE_ID)%>
			    </div>
                <div class="editor-field">
				    <%: Html.DropDownListFor(model => model.manejoCaja.SEDE_ID,Model._sedes,"-- Seleccione --")%>
				    <%: Html.ValidationMessageFor(model => model.manejoCaja.SEDE_ID)%>
			    </div> 
            </div>
       </div>

       <div class ="span-24 last">
            <div class="span-8 column last">
                 <div class="editor-label">
					    <%: Html.LabelFor(model => model.manejoCaja.CAJA_ID)%>
			    </div>
                <div class="editor-field">
				    <%: Html.DropDownListFor(model => model.manejoCaja.CAJA_ID,Model._cajas,"-- Seleccione --")%>
				    <%: Html.ValidationMessageFor(model => model.manejoCaja.CAJA_ID)%>
			    </div> 
            </div>
       </div>
   
   <div class ="span-24 last">
        <div class="span-8 column last ">
             <div class="editor-label">
					<%: Html.LabelFor(model => model.manejoCaja.OBSERVACIONES)%>
			</div>
            <div class="editor-field">
				<%: Html.TextAreaFor(model => model.manejoCaja.OBSERVACIONES)%>
				<%: Html.ValidationMessageFor(model => model.manejoCaja.OBSERVACIONES)%>
			</div> 
        </div>
   </div>
    
     