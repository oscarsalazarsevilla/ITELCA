﻿   <%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ITELCA_CONTROLESTUDIOS.Models.ViewModels.ManejoCajaViewModel>" %>
       <div class ="span-24 last">
            <div class="span-8 column last">
                 <div class="display-label">
					    <%: Html.LabelFor(model => model.manejoCaja.CAJA_ID)%>
			    </div>
                <div class="display-field">
				    <%: Model.manejoCaja.CAJAS.NOMBRE%>
			    </div> 
            </div>
       </div>
   
   <div class ="span-24 last">
        <div class="span-8 column last ">
             <div class="editor-label">
					<%: Html.LabelFor(model => model.manejoCaja.OBSERVACIONES)%>
			</div>
            <div class="editor-field">
				<%: Html.TextAreaFor(model => model.manejoCaja.OBSERVACIONES)%>
				<%: Html.ValidationMessageFor(model => model.manejoCaja.OBSERVACIONES)%>
			</div> 
        </div>
   </div>
    
     