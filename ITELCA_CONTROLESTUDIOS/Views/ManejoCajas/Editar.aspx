﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<ITELCA_CONTROLESTUDIOS.Models.ViewModels.ManejoCajaViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    [Itelca - Apertura Caja]
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
<div class="span-22 append-1 prepend-1 column last ui-widget-content ui-corner-all">
	<span class="introForm">Apertura de caja > Editar</span>
		<% using (Html.BeginForm()) { %>
		<% Html.EnableClientValidation(); %>
        <%:Html.ValidationSummary("Error en la aplicación") %>
         <%:Html.HiddenFor(model => model.manejoCaja.MANEJOCAJA_ID) %>
         <%:Html.HiddenFor(model => model.manejoCaja.CAJA_ID) %>
         <%:Html.HiddenFor(model => model.manejoCaja.SEDE_ID) %>
         <%:Html.HiddenFor(model => model.manejoCaja.USUARIO_ID) %>
            <div  class="span-24 Formulario  column last">
				<% Html.RenderPartial("FormEdit",Model); %>
			</div>
            <div class ="span-24 last">
            <div class="span-8 column last">
                 <div class="editor-label">
					    <%: Html.LabelFor(model => model.manejoCaja.CONDICION)%>
			    </div>
                <div class="editor-field">
				    <%: Html.DropDownListFor(model => model.manejoCaja.CONDICION, Model._condicion, "-- Seleccione --")%>
				    <%: Html.ValidationMessageFor(model => model.manejoCaja.CONDICION)%>
			    </div> 
            </div>
       </div>
             <div class="separator column span-24">
            </div>
            <p>
				<input type="submit" value="Guardar" class="button" />
			</p>
           
            <div class="column span-24">
                <%: Html.ActionLink("Volver al listado", "Listar")%>
            </div>
         
			
    <% } %>
</div>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="CssContent" runat="server">
 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="JsContent" runat="server">
</asp:Content>

