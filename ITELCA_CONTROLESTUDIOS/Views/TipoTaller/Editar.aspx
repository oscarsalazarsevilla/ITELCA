﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<ITELCA_CONTROLESTUDIOS.Models.ViewModels.TipoTallerViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    [Itelca -Tipo Taller]
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
<div class="span-22 append-1 prepend-1 column last ui-widget-content ui-corner-all">
	<span class="introForm">Tipo Taller > Editar</span>
		<% using (Html.BeginForm()) { %>
		<% Html.EnableClientValidation(); %>
        <%:Html.ValidationSummary("Error en la aplicación") %>
          <%=Html.HiddenFor(model => model.tipoTaller.TIPO_TALLER_ID)%>
          <%:Html.HiddenFor(model => model.tipoTaller.USUARIO_ID) %>
           <div  class="span-24 Formulario  column last">
					<% Html.RenderPartial("Form",Model); %>
                    <div  class="span-24  column last">
                        <div class="span-8 column last">
                                <div class="editor-label">
					                <%: Html.LabelFor(model => model.tipoTaller.ESTADO)%>
			                    </div>
                                <div class="editor-field">
				                    <%: Html.DropDownListFor(model => model.tipoTaller.ESTADO, Model.activo)%>
				                    <%: Html.ValidationMessageFor(model => model.tipoTaller.ESTADO)%>
			                    </div> 
                            </div>
                    </div>
                    
			</div>
            
            <div class="separator column span-24"></div>
			<p>
				<input type="submit" value="Guardar" class="button" />
			</p>
    <% } %>
    <div class="separator column span-24">
    </div>
    <div class="column span-24">
        <%: Html.ActionLink("Volver al listado", "Listar")%>
    </div>
</div>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="CssContent" runat="server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="JsContent" runat="server">
   
</asp:Content>