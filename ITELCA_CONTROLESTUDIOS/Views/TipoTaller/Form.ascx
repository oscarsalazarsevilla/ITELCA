﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ITELCA_CONTROLESTUDIOS.Models.ViewModels.TipoTallerViewModel>" %>
       <div class ="span-24 last">
        <div class="span-8 column">
             <div class="editor-label">
					<%: Html.LabelFor(model => model.tipoTaller.NOMBRE)%>
			</div>
            <div class="editor-field">
				<%: Html.TextBoxFor(model => model.tipoTaller.NOMBRE)%>
				<%: Html.ValidationMessageFor(model => model.tipoTaller.NOMBRE)%>
			</div> 
        </div>
    </div>
     <div class ="span-24 last">
        <div class="span-8 column">
             <div class="editor-label">
					<%: Html.LabelFor(model => model.tipoTaller.DESCRIPCION)%>
			</div>
            <div class="editor-field">
				<%: Html.TextAreaFor(model => model.tipoTaller.DESCRIPCION)%>
				<%: Html.ValidationMessageFor(model => model.tipoTaller.DESCRIPCION)%>
			</div> 
        </div>
    </div>