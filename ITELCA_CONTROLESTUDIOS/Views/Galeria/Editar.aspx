﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<ITELCA_CONTROLESTUDIOS.Models.ViewModels.GaleriaViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    [ITELCA - Galeria]
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<div class="span-22 append-1 prepend-1 column last ui-widget-content ui-corner-all">
    <span class="introForm">Galeria > Editar</span>
		<% using (Html.BeginForm("Editar", "Galeria", FormMethod.Post, new { enctype = "multipart/form-data" })) { %>
		<% Html.EnableClientValidation(); %>
        <%:Html.ValidationSummary("Error en la aplicación") %>
        <%:Html.HiddenFor(model => model.galeria.GALERIA_ID) %>
        <%:Html.HiddenFor(model => model.galeria.USUARIO_ID) %>
            <div  class="span-24 Formulario  column last">
				
				<% Html.RenderPartial("Form", Model); %>
                <div class ="span-24 last">
                    <div class="span-8 column last">
                        <div class="editor-label">
				            <%: Html.LabelFor(model => model.galeria.ESTADO)%>
			            </div>
                        <div class="editor-field">
				            <%: Html.DropDownListFor(model => model.galeria.ESTADO, Model.activo)%>
				            <%: Html.ValidationMessageFor(model => model.galeria.ESTADO)%>
			            </div> 
                    </div>
                </div>
			</div>
             <div class="separator column span-24"></div>
			<p>
				<input type="submit" value="Editar" class="button" />
			</p>
<% } %>
 <div class="separator column span-24">
 </div>
 <div class="column span-24">
        <%: Html.ActionLink("Volver al listado", "Index")%>
    </div>

</div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="CssContent" runat="server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="JsContent" runat="server">
    <script src="<%: Url.Content("~/Scripts/jquery.validate.min.js") %>" type="text/javascript"></script>
	<script src="<%: Url.Content("~/Scripts/jquery.validate.unobtrusive.min.js") %>" type="text/javascript"></script>
    <script src="/Scripts/jquery.maskedinput-1.2.2.js" type="text/javascript"></script>
    <script src="/Scripts/ckeditor/ckeditor.js" type="text/javascript"></script>

</asp:Content>
