﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ITELCA_CONTROLESTUDIOS.Models.ViewModels.GaleriaViewModel>" %>
        <div class ="span-24 column last">
            <div class="span-8 column ">
                 <div class="editor-label">
					    <%: Html.LabelFor(model => model.galeria.NOMBRE)%>
			    </div>
                <div class="editor-field">
				    <%: Html.TextBoxFor(model => model.galeria.NOMBRE)%>
				    <%: Html.ValidationMessageFor(model => model.galeria.NOMBRE)%>
			    </div> 
            </div>
            <div class="span-8 column ">
                 <div class="editor-label">
					  <%: Html.LabelFor(model => model.galeria.URL)%>
			    </div>
                <div class="editor-field">
				    <input class="input-background"  id="file2" name="file" type="file" accept="jpg|png|gif" />
				    <%: Html.ValidationMessageFor(model => model.galeria.URL)%>
                    <% if (!string.IsNullOrEmpty(Model.galeria.URL))
                    {%>
                     <%: Html.HiddenFor(model => model.galeria.URL)%>
                     <br /><a href="/Content/Web/Galeria/<%:Model.galeria.URL%>" target="_blank">Imagen actual</a>
                <% }
                   %>
			    </div> 
            </div>
            <div class="span-8 column last">
                 <div class="editor-label">
					    <%: Html.LabelFor(model => model.galeria.ORDEN)%>
			    </div>
                <div class="editor-field">
				    <%: Html.TextBoxFor(model => model.galeria.ORDEN)%>
				    <%: Html.ValidationMessageFor(model => model.galeria.ORDEN)%>
			    </div> 
            </div>
       </div>
       <div class ="span-24 last">
            <div class="editor-label">
					<%: Html.LabelFor(model => model.galeria.DESCRIPCION)%>
			</div>
            <div class="editor-field">

                    <%: Html.TextAreaFor(model => model.galeria.DESCRIPCION)%>
				<%: Html.ValidationMessageFor(model => model.galeria.DESCRIPCION)%>
			</div> 
       </div>