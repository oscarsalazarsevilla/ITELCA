﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Anular
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2>Anular Matricula</h2>
<div class="span-22 append-1 prepend-1 column last ui-widget-content ui-corner-all">
	<span class="introForm">Matricula  > Anular</span>
		<% using (Html.BeginForm()) { %>
		<% Html.EnableClientValidation(); %>
        <%:Html.ValidationSummary("Error en la aplicación") %>
            <div  class="span-24 Formulario  column last">
			    <div class="span-24 column last">
                      <div class="span-8 column last ">
                        <div class="editor-label">
					        <%: Html.Label("Nro Documento")%>
			            </div>
                        <div class="editor-field">
				            <%: Html.TextBox("documento")%>
				            <%: Html.ValidationMessage("documento")%>
			            </div> 
                    </div>
                </div>
                 <div class="span-24 column last">
                    <div class="span-8 column last">
                        <div class="editor-label">
					        <%: Html.Label("Inscrito En")%>
			            </div>
                        <div class="editor-field">
				            <%: Html.DropDownList("matriculaId","--Seleccione--")%>
				            <%: Html.ValidationMessage("matriculaId")%>
			            </div> 
                    </div>
                </div>
			</div>
             <div class="separator column span-24">
            </div>
            <p>
				<input type="submit" value="Crear" class="button" />
			</p>
           
            <div class="column span-24">
                <%: Html.ActionLink("Volver al listado", "Listar")%>
            </div>
          
			
    <% } %>
     
                
</div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="CssContent" runat="server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="JSContent" runat="server">
<script type="text/javascript">
    $(document).ready(function () { 
        $("#documento").keydown(function(event){
            if( event.keyCode==13){
                event.preventDefault();
                $.post("/MatriculaCurso/ObtenerCursoPorAlumno", { identidad: $(this).val() }, function (data) {
                    var tam=data.lenght;
                    if(tam>0){
                        var nodo="";
                        for(var i=0;i<tam;i++)
                            nodo+='<option value="'+data[i].key+'">'+data[i].value+'</option>';
                        $("matriculaId").html(nodo);
                    }else
                        $("matriculaId").html('<option value="">Sin Cursos Registrados');
                
                });
    });
</script>
</asp:Content>
