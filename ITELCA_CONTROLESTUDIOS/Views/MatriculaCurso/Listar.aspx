﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Web2.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Listar
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">



<div class="span-20 last" style="margin-left:auto; margin-right:auto;">
<h2>Lista de Cursos</h2>


<fieldset>
<div class="span-20 last" style="margin-left:auto; margin-right:auto;">
        <p>
            <%: Html.ActionLink("Nuevo", "Inscripcion") %>
        </p>
        <div id="msg">
        <%
            try
            {

                if (TempData["mensaje"] != null && TempData["mensaje"]  != "")
                {%>
			    <div  class="ui-state-highlight ui-corner-all" style="margin-top: 10px; padding: 0 .3em; width:30%; height:20px;">
				    <span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
				    <div><%:TempData["mensaje"]  %></div>
			    </div>
		    
          <%
                }
            }
            catch{} 
            
           %>
        </div>
            <table id="gridMatriculaCurso">
            </table>
            <div id="pager"></div>
            <div id="dialogCurso">
                <div id="detalleCurso">
                    <div id="tabs">
                        <ul>
                            <li><a href="#tabs-2">Pagos</a></li>
                        </ul>
                        <div id="tabs-2">
                            <div class="span-24 column last">
                                 <table id="pMensualidad">
                                <thead>
                                    <tr>
                                        <th>Mensualidad</th>
                                        <th>Fecha Inicio</th>
                                        <th>Fecha Vencimiento</th>
                                        <th>Fecha Pago</th>
                                        <th>Monto</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                                </table>
                            </div>
                               
                        </div>
                    </div>
                </div>
                </div>
</fieldset>
</div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="CssContent" runat="server">
    <link href="/Content/Css/ui.jqgrid.css" rel="stylesheet" type="text/css" />

</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="JSContent" runat="server">
    <script src="/Scripts/grid.locale-es.js" type="text/javascript"></script>
    <script src="/Scripts/jquery.jqGrid.min.js" type="text/javascript"></script>
    <script src="/Scripts/jquery.dataTables.js" type="text/javascript"></script>
    <script type="text/javascript">
        var otableMensualidad;
        var consultado = -1;
        $(document).ready(function () {


            otableMensualidad = $("#pMensualidad").dataTable({
                "bPaginate": false,
                "bFilter": false,
                "bJQueryUI": true,
                "bInfo": false,
                "bAutoWidth": false,
                "bSort": false,
                "aoColumns": [{ "sWidth": "30%", "sClass": "left", "bSortable": false },
                              { "sWidth": "15%", "sClass": "center", "bSortable": false },
                              { "sWidth": "15%", "sClass": "center", "bSortable": false },
                              { "sWidth": "15%", "sClass": "center", "bSortable": false },
                              { "sWidth": "15%", "sClass": "center", "bSortable": false}],
                "oLanguage": {
                    "sProcessing": "Procesando...",
                    "sLengthMenu": "Mostrar _MENU_ registros",
                    "sZeroRecords": "No se encontraron resultados",
                    "sEmptyTable": "Ningún dato disponible en esta tabla",
                    "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix": "",
                    "sSearch": "Buscar:",
                    "sUrl": "",
                    "sInfoThousands": ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst": "Primero",
                        "sLast": "Último",
                        "sNext": "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                }
            });

            $("#tabs").tabs();
            $('#dialogCurso').dialog({
                height: 500,
                width: 800,
                modal: true,
                autoOpen: false,
                buttons: [{ text: "Ok", click: function () { $(this).dialog("close"); } }]
            });
            var grid = $("#gridMatriculaCurso").jqGrid({
                url: '/MatriculaCurso/ObtenerData',
                datatype: 'json',
                mtype: 'POST',
                colNames: ['Id', 'Curso', 'Sede', 'Turno', 'Fecha Inscripción', 'Día Corte', 'Condición', 'Ver'],
                colModel: [
		            { name: 'OFERTA_CURSO_ID', index: 'OFERTA_CURSO_ID', align: "right", width: 100, resizable: true, searchoptions: { sopt: ['cn']} },
		            { name: 'CURSOS.NOMBRE', index: 'AULA_NOMBRE', width: 200, searchoptions: { sopt: ['cn']} },
                    { name: 'SEDES.NOMBRE', index: 'TURNO_NOMBRE', width: 200, searchoptions: { sopt: ['cn']} },
                    { name: 'TURNOS.NOMBRE', index: 'MODULO_NOMBRE', width: 200, searchoptions: { sopt: ['cn']} },
		            { name: 'FECHA_INSCRIPCION', index: 'MODULO_NOMBRE', width: 200, searchoptions: { sopt: ['cn']} },
		            { name: 'DIA_CORTE', index: 'DIA_CORTE', width: 200, searchoptions: { sopt: ['cn']} },
                     { name: 'CONDICION', index: 'CONDICION', width: 200, searchoptions: { sopt: ['cn']} },
		            { name: 'Action2', index: 'Action2', align: "left", width: 100, resizable: false, sortable: false, search: false}],
                pager: jQuery('#pager'),
                rowNum: 500,
                rowList: [500, 1000, 5000],
                sortname: 'OFERTA_CURSO_ID',
                sortorder: 'desc',
                autowidth: true,
                viewrecords: true,
                caption: 'Listado de Cursos',
                loadComplete: function () {
                    $(".ui-icon-circle-plus").click(function () {
                        var gr = $(this).attr("id");
                        $('#dialogCurso').dialog('open');
                        if (gr != consultado) {
                            consultado = gr;
                            otableMensualidad.fnClearTable();
                            $.post("/MatriculaCurso/ObtenerMensualidadPorMatricula", { idMatricula: gr }, function (data) {
                                var tam = data.length;
                                if (tam > 0) {
                                    var lista = new Array();
                                    for (var i = 0; i < tam; i++) {
                                        lista.push(data[i].desc, data[i].fechaI, data[i].fechaV, data[i].fechaP, data[i].monto);

                                    }
                                    otableMensualidad.dataTable().fnAddData(lista);
                                }

                            });
                        }

                    });

                }
            }).navGrid('#pager',
            {
                edit: false, add: false, del: false, search: true, refresh: false
            },
            {}, //edit options
            {}, //add options
            {}, //del options
            {multipleSearch: true} // search options
            );
        });
    </script>
</asp:Content>
