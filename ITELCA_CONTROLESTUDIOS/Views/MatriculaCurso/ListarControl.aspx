﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Listar
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">



<div class="span-24 last" style="margin-left:auto; margin-right:auto;">
<h2>Lista de Cursos</h2>


<fieldset>
<div class="span-24 last" style="margin-left:auto; margin-right:auto;">
        <p>
            <%: Html.ActionLink("Nuevo", "Inscripcion") %>
        </p>
        <div id="msg">
        <%
            try
            {

                if (TempData["mensaje"] != null && TempData["mensaje"]  != "")
                {%>
			    <div  class="ui-state-highlight ui-corner-all" style="margin-top: 10px; padding: 0 .3em; width:30%; height:20px;">
				    <span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
				    <div><%:TempData["mensaje"]  %></div>
			    </div>
		    
          <%
                }
            }
            catch{} 
            
           %>
        </div>
        <div class="span-24 last column">
            <table id="gridMatriculaCurso">
            </table>
            <div id="pager"></div>
        </div> 
            <div id="dialogCurso">
                
               <div id="detalleCurso" class="span-24 column" style="margin-bottom:10px;">
                    <div id="nombreCurso"></div>
                    <div id="nombreAlumno"></div>
                </div>
                <div class="span-24  column">
                    <table id="gridConceptos">
                    </table>
                    <div id="pager2"></div>
                </div> 
                
            </div>
</fieldset>
</div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="CssContent" runat="server">
    <link href="/Content/Css/ui.jqgrid.css" rel="stylesheet" type="text/css" />

</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="JSContent" runat="server">
    <script src="/Scripts/grid.locale-es.js" type="text/javascript"></script>
    <script src="/Scripts/jquery.jqGrid.min.js" type="text/javascript"></script>
    <script src="/Scripts/jquery.dataTables.js" type="text/javascript"></script>
    <script type="text/javascript">
        var otableMensualidad;
        var consultado = -1;
        $(document).ready(function () {

            winHeight = window.innerHeight;
            wHeight = winHeight - 340;

            $('#dialogCurso').dialog({
                height: 500,
                width: 800,
                modal: true,
                autoOpen: false,
                buttons: [{ text: "Ok", click: function () { $(this).dialog("close"); } }]
            });



            var grid = $("#gridMatriculaCurso").jqGrid({
                url: '/MatriculaCurso/ObtenerDataControl',
                datatype: 'json',
                mtype: 'POST',
                colNames: ['Id', 'Alumno', 'Nro. Documento', 'Curso', 'Sede', 'Turno', 'Fecha Inscripción', 'Dia Corte', 'Cajero', 'Condición', 'Mensualidades', 'Eliminar'],
                colModel: [
		            { name: 'OFERTA_CURSO_ID', index: 'OFERTA_CURSO_ID', align: "right", resizable: true, width: 50, searchoptions: { sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']} },
		            { name: 'USUARIOS.NOMBRE', index: 'USUARIOS.NOMBRE', resizable: true, width: 200, search: false },
                    { name: 'USUARIOS.NRO_DOCUMENTO', index: 'USUARIOS.NRO_DOCUMENTO', resizable: true, width: 200, searchoptions: { sopt: ['cn']} },
                    { name: 'CURSOS.NOMBRE', index: 'CURSOS.NOMBRE', resizable: true, width: 130, searchoptions: { sopt: ['cn']} },
                    { name: 'SEDES.NOMBRE', index: 'SEDES.NOMBRE', resizable: true, width: 125, searchoptions: { sopt: ['cn']} },
                    { name: 'TURNOS.NOMBRE', index: 'TURNOS.NOMBRE', resizable: true, width: 125, searchoptions: { sopt: ['cn']} },
                    { name: 'FECHA_INSCRIPCION', index: 'FECHA_INSCRIPCION', align: "right", resizable: true, width: 125, sorttype: 'date', searchoptions: { dataInit: function (elem) {
                        $(elem).datepicker({
                            changeYear: true,
                            changeMonth: true,
                            showButtonPanel: true,
                            onSelect: function () {
                                if (this.id.substr(0, 3) === "gs_") {
                                    // in case of searching toolbar
                                    setTimeout(function () {
                                        myGrid[0].triggerToolbar();
                                    }, 50);
                                } else {
                                    // refresh the filter in case of
                                    // searching dialog
                                    $(this).trigger('change');
                                }
                            }
                        });
                    }
                    }
                    },
                    { name: 'DIA_CORTE', index: 'DIA_CORTE', align: "right", resizable: true, width: 100, searchoptions: { sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']} },
                    { name: 'USUARIOS2.NOMBRE', index: 'USUARIOS2.NOMBRE', resizable: true, width: 150, search: false },
                    { name: 'CONDICION', index: 'CONDICION', align: "right", resizable: true, width: 100, searchoptions: { sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']} },
                    { name: 'Action1', index: 'Action1', align: "left", width: 50, resizable: false, sortable: false, search: false },
                    { name: 'Action2', index: 'Action2', align: "left", width: 50, resizable: false, sortable: false, search: false}],
                pager: jQuery('#pager'),
                rowNum: 500,
                height: wHeight,
                rowList: [500, 1000, 5000],
                sortname: 'FECHA_INSCRIPCION',
                sortorder: 'asc',
                autowidth: true,
                viewrecords: true,
                caption: 'Listado de Conceptos Cursos',
                loadComplete: function () {
                    $(".ui-icon-circle-plus").click(function () {
                        var gr = $(this).attr("id");

                        if (gr != consultado) {
                            trf = $("#gridConceptos tbody:first tr:first")[0];
                            $("#gridConceptos tbody:first").empty().append(trf);
                            $.post("/MatriculaCurso/ObtenerConceptosPorMatriculaCurso", { matriculaCurso: gr }, function (response) {
                                if (response && response.length) {
                                    var total = $("#gridConceptos").jqGrid('getGridParam', 'records');
                                    for (var i = 0; i < response.length; i++) {
                                        var datarow = { IDC: response[i].id, ORDEN: response[i].orden, NOMBRE: response[i].nombre, MONTO: response[i].monto, FECHA_VENCIMIENTO: response[i].fecha, CONDICION: response[i].condicion };
                                        var su = $("#gridConceptos").jqGrid('addRowData', i, datarow);

                                    }
                                    $("#gridConceptos").show();
                                    $("#nombreCurso").text("CURSO: "+response[0].nombre_curso);
                                    $("#nombreAlumno").text("ALUMNO: "+response[0].nombre_alumno);
                                }
                                
                            });

                        }
                        $('#dialogCurso').dialog('open');

                    });

                    $(".ui-icon-close").click(function () {
                        var gr = $(this).attr("id");
                        var r = confirm("Desea Eliminar la Matrícula? ");
                        if (r == true) {
                            $.post("/MatriculaCurso/EliminarMatricula", { id: gr }, function (data) {
                                var dat = ' <div  class="ui-state-highlight ui-corner-all" style="margin-top: 10px; padding: 0 .3em; width:30%; height:20px;"><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span><div>';
                                dat += data[0].msg;
                                dat += '</div></div>';
                                $("#msg").empty().html(dat);
                                grid.trigger('reloadGrid');

                            });
                        }
                        else {
                            x = "Cancelar";
                        }
                        // alert("adas");
                    });

                }
            }).navGrid('#pager',
            {
                edit: false, add: false, del: false, search: true, refresh: true
            },
            {}, //edit options
            {}, //add options
            {}, //del options
            {multipleSearch: true} // search options
            );

            var gridConceptos = $("#gridConceptos").jqGrid({
                colNames: ['Id', 'Cuota', 'NOMBRE', 'Monto', 'Fecha Vencimiento', 'Estado'],
                colModel: [
		                                { name: 'IDC', index: 'IDC', align: "right", width: 50, editable: true, hidden: true },
                                        { name: 'ORDEN', index: 'ORDEN', width: 50, align: "left", resizable: true, searchoptions: { sopt: ['cn']} },
                                        { name: 'NOMBRE', index: 'NOMBRE', width: 200, align: "left", resizable: true, searchoptions: { sopt: ['cn']} },
                                        { name: 'MONTO', index: 'MONTO', align: "right", width: 70, resizable: true, searchoptions: { sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']} },
                                        { name: 'FECHA_VENCIMIENTO', index: 'FECHA_VENCIMIENTO', align: "right", width: 120, resizable: true, searchoptions: { sopt: ['cn']} },
                                        { name: 'CONDICION', index: 'CONDICION', align: "right", width: 120, resizable: true, searchoptions: { sopt: ['cn']} },
                                      ],
                pager: jQuery('#pager2'),
                rowNum: 500,
                height: 250,
                rowList: [500, 1000, 5000],
                sortname: 'ID',
                sortorder: 'desc',
                autowidth: true,
                viewrecords: true,
                caption: 'Listado de Conceptos',
                loadComplete: function () {

                }
            }).navGrid('#pager2',
                                {
                                    edit: false, add: false, del: false, search: true, refresh: true
                                },
                                {}, //edit options
                                {}, //add options
                                {}, //del options
                                {multipleSearch: true} // search options
            );


        });



    </script>
</asp:Content>
