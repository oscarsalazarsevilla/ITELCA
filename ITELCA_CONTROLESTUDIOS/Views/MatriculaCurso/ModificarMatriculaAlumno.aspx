﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<ITELCA_CONTROLESTUDIOS.Models.ViewModels.MatriculaCursoViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    [Itelca - Matricula Curso]
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
<div class="span-22 append-1 prepend-1 column last ui-widget-content ui-corner-all">
	<span class="introForm">Matricula Curso > Modificar Matricula Alumnos</span>
      
      <div class="separator column span-24"></div>
		<% using (Html.BeginForm()) { %>
         <div  class="span-24 Formulario  column last">
            <div  class="span-24  column last">
                <div class="span-6 column ">
                    <div class="editor-label">
				        <%: Html.Label("Sede")%> 
			        </div>
                    <div class="editor-field" >
                        <%: Html.DropDownList("ListaSedes", Model.listaSedes,"-- Seleccione --")%>
                    </div>
                </div>
                <div class="span-12 column ">
                    <div class="editor-label">
				        <%: Html.Label("Curso")%> 
			        </div>
                    <div class="editor-field" >
                        <%: Html.DropDownList("ListaCursos", Model.listaVacia, new { @disabled="disabled"})%>
                    </div>
                </div>
                <div class="span-6 column last ">
                    <div class="editor-label">
				        <%: Html.Label("Turno")%> 
			        </div>
                    <div class="editor-field" >
                        <%: Html.DropDownList("ListaTurnos", Model.listaVacia, new { @disabled = "disabled" })%>
                    </div>
                </div>
            </div>
            <div  class="span-24  column last">
                
                <div class="span-12 column ">
                    <div class="editor-label">
				        <%: Html.Label("Alumno")%> 
			        </div>
                    <div class="editor-field" >
                        <%: Html.DropDownList("ListaAlumno", Model.listaVacia, new { @disabled = "disabled" })%>
                    </div>
                </div>
               <div class="span-8 last column" style="margin-top:2%">
                    <input type="button" value="Cambiar SEDE|TURNO|CONGELAR|ANULAR" class="button" id="btnModificar" />
                </div>
			</div>	
            </div>
    <% } %>

    </br>
    </br>
    </br>
    </br></br>
    </br>
        </br></br>
    <div class="span-24 last column">
        <input type="button" value="Agregar Módulo" class="button" id="btnAgregarModulo" />
    </div>

    </br></br></br>

            <div class="span-9 append-1 column">
                <table id="gridSeccion">
                </table>
                <div id="pager"></div>
            </div>

            <div class="span-13 last column">
                 <table id="gridConceptos">
                </table>
                <div id="pager2"></div>
            </div>

      </br></br></br>

      <div id="dialogConfirmar">
      </div>

      <div id="modalNuevoModulo" title="Nuevo Disponibles">
                     <div  class="span-24  column last">
                        <div class="span-6 column ">
                            <div class="editor-label">
				                <%: Html.Label("Sede")%> 
			                </div>
                            <div class="editor-field" >
                                <%: Html.DropDownList("ListaSedesModal", Model.listaSedes, "-- Seleccione --")%>
                            </div>
                        </div>                 
                        <div class="span-12 column ">
                            <div class="editor-label">
				                <%: Html.Label("Turno")%> 
			                </div>
                            <div class="editor-field" >
                                <%: Html.DropDownList("ListaTurnosModal", Model.listaVacia, "-- Seleccione --", new { @disabled = "disabled" })%>
                            </div>
                        </div>
                    </div>
                    

                 <div class="span-24 column last" style="margin-top:30px;">
                     <div class="span-17 column ">
                        <table id="ModulosNuevo">
                        </table>
                        <div id="pager3"></div>
                    </div>
                    <div class="span-6 column last">
                        <div class="editor-label">
				            <%: Html.Label("Número de Cuotas:")%> 
			            </div>
                        <div class="editor-field" >
                            <%: Html.TextBox("numeroCuotas", "", new { @class = "numero"})%>
                        </div>
                    </div>

                    <div class="span-6 column last">
                        <div class="editor-label">
				            <%: Html.Label("Monto:")%> 
			            </div>
                        <div class="editor-field" >
                            <%: Html.TextBox("montoCuotas","", new { @class = "montos",  @value="0"})%>
                        </div>
                    </div>
                </div>


                <div class="span-2 columm last">
                    <!--<input type="button" value="Agregar" class="button" id="btnInscribirModulo" /> -->
                </div>
      </div>
      
</div>
<div id="dialogModificar">
         <div  class="span-24 Formulario  column last">
            <div  class="span-24  column last">
                <div class="span-6 column ">
                    <div class="editor-label">
				        <%: Html.Label("Sede")%> 
			        </div>
                    <div class="editor-field" >
                        <%: Html.DropDownList("ListaSedesModificar", Model.listaSedes,"-- Seleccione --")%>
                    </div>
                </div>
                <div class="span-6 column last ">
                    <div class="editor-label">
				        <%: Html.Label("Turno")%> 
			        </div>
                    <div class="editor-field" >
                        <%: Html.DropDownList("ListaTurnosModificar", Model.listaVacia, new { @disabled = "disabled" })%>
                    </div>
                </div>
            </div>
            <div  class="span-24  column last">
                <div class="span-6 column ">
                    <div class="editor-label">
				        <%: Html.Label("Estado")%> 
			        </div>
                    <div class="editor-field" >
                        <select id="condicion">
                            <option value="">--Selecione--</option>
                            <option value="C">EN CURSO</option>
                            <option value="A">APROBADO</option>
                            <option value="E">ANULADO</option>
                            <option value="R">CONGELADO</option>
                        </select>
                    </div>
                </div>
                 <div class="span-4 last column" style="margin-top:2%">
                    <input type="button" value="Modificar" class="button" id="cambiarEdo" />
                </div>
			</div>	
            </div>
</div>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="CssContent" runat="server">
    <link href="/Content/Css/stepsCE.css" rel="stylesheet" type="text/css" />
     <link href="/Content/Css/ui.jqgrid.css" rel="stylesheet" type="text/css" />

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="JsContent" runat="server">
    <script src="../../Scripts/jquery.dataTables.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery.maskedinput-1.2.2.js" type="text/javascript"></script>
    <script src="/Scripts/grid.locale-es.js" type="text/javascript"></script>
    <script src="/Scripts/jquery.jqGrid.min.js" type="text/javascript"></script>
    <script src="/Scripts/jquery.steps.min.js" type="text/javascript"></script>
    <script src="/Scripts/autoNumeric.js" type="text/javascript"></script>
    <script src="/Scripts/Views/matriculaCurso.js" type="text/javascript"></script>

        <script type="text/javascript">
            var lastSelection;
            function reload(rowid, result) {
                jQuery("#gridConceptos").trigger("reloadGrid");
            }
            $(document).ready(function () {
                winHeight = window.innerHeight;
                wHeight = winHeight - 400;
                var gridSeccion = $("#gridSeccion").jqGrid({
                    colNames: ['Id', 'Nombre', 'Eliminar'],
                    colModel: [
		                                { name: 'ID', index: 'ID', align: "right", width: 50, resizable: true, searchoptions: { sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']} },
		                                { name: 'NOMBRE', index: 'NOMBRE', width: 200, align: "left", resizable: true, searchoptions: { sopt: ['cn']} },
                                        { name: 'Action2', index: 'Action2', align: "center", width: 50, resizable: false, sortable: false, search: false}],
                    pager: jQuery('#pager'),
                    rowNum: 500,
                    height: wHeight,
                    rowList: [500, 1000, 5000],
                    sortname: 'ID',
                    sortorder: 'desc',
                    autowidth: true,
                    viewrecords: true,
                    caption: 'Listado de Módulos Inscritos',
                    loadComplete: function () {

                    }

                }).navGrid('#pager',
                                {
                                    edit: false, add: false, del: false, search: true, refresh: true
                                },
                                {}, //edit options
                                {}, //add options
                                {}, //del options
                                {multipleSearch: true} // search options
                            );

                var gridConceptos = $("#gridConceptos").jqGrid({
                    colNames: ['Id', 'Cuota', 'NOMBRE', 'Monto', 'Fecha', 'Estado', 'Eliminar'],
                    colModel: [
		                                { name: 'IDC', index: 'IDC', align: "right", width: 50, editable: true, editrules: { edithidden: true, required: false, hidden: true }, hidden: true },
                                        { name: 'ORDEN', index: 'ORDEN', width: 50, align: "left", resizable: true, searchoptions: { sopt: ['cn'] }, editable: true },
                                        { name: 'NOMBRE', index: 'NOMBRE', width: 200, align: "left", resizable: true, searchoptions: { sopt: ['cn'] }, editable: true },
                                        { name: 'MONTO', index: 'MONTO', align: "right", width: 70, resizable: true, searchoptions: { sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge'] }, editable: true },
                                        { name: 'FECHA', index: 'FECHA', align: "right", width: 120, resizable: true, searchoptions: { sopt: ['cn'] }, editable: true },
                                        { name: 'CONDICION', index: 'CONDICION', align: "right", width: 120, resizable: true, searchoptions: { sopt: ['cn'] }, editable: true, edittype: "select", formatter: 'select', editoptions: { value: "P:PENDIENTE;C:CANCELADO"} },
                                        { name: 'Action2', index: 'Action2', align: "center", width: 50, resizable: false, sortable: false, search: false}],
                    pager: jQuery('#pager2'),
                    rowNum: 500,
                    height: wHeight,
                    rowList: [500, 1000, 5000],
                    sortname: 'ID',
                    sortorder: 'desc',
                    autowidth: true,
                    viewrecords: true,
                    editurl: "/MatriculaCurso/ModificarCuotasGrid", // 'clientArray'
                    caption: 'Listado de Conceptos',
                    ondblClickRow: function (rowid, iCol, cellContent, e) {
                        gridConceptos.jqGrid('restoreRow', lastSelection);
                        lastSelection = rowid;
                        gridConceptos.jqGrid('editRow', rowid, true, null, null, null, null, reload);

                    },
                    onSelectRow: function (id) {
                        gridConceptos.jqGrid('restoreRow', lastSelection);
                    },
                    loadComplete: function () {

                    }
                }).navGrid('#pager2',
                                {
                                    edit: false, add: false, del: false, search: true, refresh: true
                                },
                                {}, //edit options
                                {}, //add options
                                {}, //del options
                                {multipleSearch: true} // search options
                                );



                var gridModulosNuevos = $("#ModulosNuevo").jqGrid({
                    colNames: ['Id', 'Nombre', 'Fecha Inicio', 'Opción'],
                    colModel: [
		                        { name: 'ID', index: 'ID', align: "right", width: 50, resizable: true, searchoptions: { sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']} },
                                { name: 'NOMBRE', index: 'NOMBRE', width: 200, align: "left", resizable: true, searchoptions: { sopt: ['cn']} },
                                { name: 'FECHA_INICIO', index: 'FECHA_INICIO', align: "right", width: 70, resizable: true, searchoptions: { sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']} },
                                { name: 'Action2', index: 'Action2', align: "center", width: 50, resizable: false, sortable: false, search: false}],
                    pager: jQuery('#pager3'),
                    rowNum: 500,
                    height: 200,
                    width: 200,
                    rowList: [500, 1000, 5000],
                    sortname: 'ID',
                    sortorder: 'desc',
                    autowidth: true,
                    viewrecords: true,
                    caption: 'Listado de Módulos',
                    loadComplete: function () {

                    }
                }).navGrid('#pager3',
                                {
                                    edit: false, add: false, del: false, search: true, refresh: true
                                },
                                {}, //edit options
                                {}, //add options
                                {}, //del options
                                {multipleSearch: true} // search options
                                );


                $('#cambiarEdo').click(function () {
                    var idCurso = $("#ListaCursos option:selected").val();
                    var idSede = $("#ListaSedes option:selected").val();
                    var idTurno = $("#ListaTurnos option:selected").val();
                    var idSedeN = $("#ListaSedesModificar option:selected").val();
                    var idTurnoN = $("#ListaTurnosModificar option:selected").val();
                    var idAlumno = $("#ListaAlumno option:selected").val();
                    var cond = $("#condicion option:selected").val();
                    if (idCurso != "" && idSede != "" && idTurno != "" && idAlumno != "" && cond != "") {
                        var resp = confirm("Esta Seguro de Cambiar los valores de la Matricula?");
                        if (resp) {
                            $.post("/MatriculaCurso/ModificarEstadoMatricula", { sede: idSede, curso: idCurso, turno: idTurno, alumno: idAlumno, condicion: cond,sedeN:idSedeN,turnoN:idTurnoN }, function (response) {
                                if (response && response.length) {
                                    alert(response[0].msg);
                                } else {
                                    alert(response[0].msg);
                                }
                            });
                        }
                    } else {
                        alert("Existen Selectores sin valor");
                    }
                });
                $("#btnModificar").click(function () {
                    var idCurso = $("#ListaCursos option:selected").val();
                    var idSede = $("#ListaSedes option:selected").val();
                    var idTurno = $("#ListaTurnos option:selected").val();
                    var idAlumno = $("#ListaAlumno option:selected").val();
                    if (idCurso != "" && idSede != "" && idTurno != "" && idAlumno != "" ) {
                        $('#dialogModificar').dialog('open');
                    } else
                        alert("Debe llenar Selectores de Matricula y alumno");
                });
                $('#dialogModificar').dialog({
                    autoOpen: false,
                    width: '85%',
                    resizable: true,
                    title: 'Cambiar Alumno SEDE TURNO | CONGELAR',
                    modal: true,
                    open: function (event, ui) {
                        //$("body").addClass("loading");
                        // $(this).load("/ReciboCaja/EstadoCuenta/" + $("#alumno_id").val());
                        //$("body").removeClass("loading");
                    },
                    position: "top",
                    buttons: {
                        "Cerrar": function () {
                            $(this).dialog("close");
                        }
                    }
                });


                $('#ListaSedesModificar').change(function () {
                    var idCurso = $("#ListaCursos option:selected").val();
                    var idSede = $("#ListaSedesModificar option:selected").val();
                    if (idCurso != "" && idSede != "") {
                        $.post("/MatriculaCurso/ObtenerTurno", { sede: idSede, curso: idCurso }, function (response) {
                            if (response != "") {
                                var s = '<select><option>--Seleccione--</option>';
                                if (response && response.length) {
                                    for (var i = 0; i < response.length; i++) {
                                        s += '<option value="' + response[i].key + '">' + response[i].value + '</option>';
                                    }
                                }
                                s += "</select>";

                                $('#ListaTurnosModificar').html(s).prop("disabled", false);
                            } else {
                                $('#ListaTurnosModificar').html("<select><option>Sin Turnos Disponibles</option></select>");
                            }
                        });
                    } else {
                        $('#ListaTurnosModificar').html("<select></select>");
                    }
                });


            });
</script>
</asp:Content>
