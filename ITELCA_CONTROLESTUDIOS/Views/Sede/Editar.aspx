﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<ITELCA_CONTROLESTUDIOS.Models.ViewModels.SedeViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    [Itelca - Sedes]
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
<div class="span-22 append-1 prepend-1 column last ui-widget-content ui-corner-all">
	<span class="introForm">Sede > Editar</span>
		<% using (Html.BeginForm()) { %>
		<% Html.EnableClientValidation(); %>
        <%:Html.ValidationSummary("Error en la aplicación") %>
        <%:Html.HiddenFor(model => model.sede.SEDE_ID) %>
        <%:Html.HiddenFor(model => model.sede.USUARIO_ID) %>
            <div  class="span-24 Formulario  column last">
				<% Html.RenderPartial("Form",Model); %>
                <div class ="span-24 column last">
                    <div class="span-8 column">
                        <div class="editor-label">
				            <%: Html.LabelFor(model => model.sede.ESTADO)%>
			            </div>
                        <div class="editor-field">
				            <%: Html.DropDownListFor(model => model.sede.ESTADO,Model.activo)%>
				            <%: Html.ValidationMessageFor(model => model.sede.ESTADO)%>
			            </div> 
                    </div>
                </div>
			</div>
             <div class="separator column span-24">
            </div>
            <p>
				<input type="submit" value="Guardar" class="button" />
			</p>
           
            <div class="column span-24">
                <%: Html.ActionLink("Volver al listado", "Listar")%>
            </div>
         
			
    <% } %>
     <div class=" prepend-2 separator column span-22" style="height:300px;" >
                <div id='map_canvas' style='width:90%; height:300px;'></div>
            </div>
</div>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="CssContent" runat="server">
 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="JsContent" runat="server">
 <script src="/Scripts/jquery.maskedinput-1.2.2.js" type="text/javascript"></script>
 <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false" type="text/javascript"></script>
 <script src="/Scripts/Views/sede.js" type="text/javascript"></script>
</asp:Content>

