﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ITELCA_CONTROLESTUDIOS.Models.ViewModels.TallerViewModel>" %>
       <div class ="span-24 last">
            <div class="span-8 column">
                 <div class="editor-label">
					    <%: Html.LabelFor(model => model.taller.TIPO_TALLER_ID)%>
			    </div>
                <div class="editor-field">
				    <%: Html.DropDownListFor(model => model.taller.TIPO_TALLER_ID,Model.tipoTaller,"--Seleccione--")%>
				    <%: Html.ValidationMessageFor(model => model.taller.TIPO_TALLER_ID)%>
			    </div> 
            </div>
        </div>
        <div class ="span-24 last">
            <div class="span-8 column">
                 <div class="editor-label">
					    <%: Html.LabelFor(model => model.taller.NOMBRE)%>
			    </div>
                <div class="editor-field">
				    <%: Html.TextBoxFor(model => model.taller.NOMBRE)%>
				    <%: Html.ValidationMessageFor(model => model.taller.NOMBRE)%>
			    </div> 
            </div>
        </div>
         <div class ="span-24 last">
            <div class="span-8 column">
                 <div class="editor-label">
					    <%: Html.LabelFor(model => model.taller.DESCRIPCION)%>
			    </div>
                <div class="editor-field">
				    <%: Html.TextAreaFor(model => model.taller.DESCRIPCION)%>
				    <%: Html.ValidationMessageFor(model => model.taller.DESCRIPCION)%>
			    </div> 
            </div>
        </div>

   