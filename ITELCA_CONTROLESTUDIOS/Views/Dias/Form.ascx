﻿   <%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ITELCA_CONTROLESTUDIOS.Models.ViewModels.DiaViewModel>" %>
       <div class ="span-24 last">
        <div class="span-8 column">
             <div class="editor-label">
				<%: Html.LabelFor(model => model.dia.NOMBRE)%>
			</div>
            <div class="editor-field">
				<%: Html.TextBoxFor(model => model.dia.NOMBRE)%>
				<%: Html.ValidationMessageFor(model => model.dia.NOMBRE)%>
			</div> 
        </div>
        <div class="span-16 column last">
             &nbsp; 
        </div>
   </div>
   
    <div class ="span-24 last">
        <div class="span-8 column ">
             <div class="editor-label">
					<%: Html.LabelFor(model => model.dia.NOMBRECORTO)%>
			</div>
            <div class="editor-field">
				<%: Html.TextBoxFor(model => model.dia.NOMBRECORTO)%>
				<%: Html.ValidationMessageFor(model => model.dia.NOMBRECORTO)%>
			</div> 
        </div>
        <div class="span-16 column last">
             &nbsp; 
        </div>
    </div>
     