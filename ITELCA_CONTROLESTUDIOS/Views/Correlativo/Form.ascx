﻿   <%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ITELCA_CONTROLESTUDIOS.Models.ViewModels.CorrelativoViewModel>" %>
       <div class ="span-24 last">
            <div class="span-8 column last">
                 <div class="editor-label">
					    <%: Html.LabelFor(model => model.correlativo.SEDE_ID)%>
			    </div>
                <div class="editor-field">
				    <%: Html.DropDownListFor(model => model.correlativo.SEDE_ID,Model.sedes,"--Seleccione--")%>
				    <%: Html.ValidationMessageFor(model => model.correlativo.SEDE_ID)%>
			    </div> 
            </div>
       </div>
   
   <div class ="span-24 last">
        <div class="span-8 column last">
                 <div class="editor-label">
					    <%: Html.LabelFor(model => model.correlativo.TIPO_DOCUMENTO)%>
			    </div>
                <div class="editor-field">
				    <%: Html.DropDownListFor(model => model.correlativo.TIPO_DOCUMENTO, Model.tipoDocumento, "--Seleccione--")%>
				    <%: Html.ValidationMessageFor(model => model.correlativo.TIPO_DOCUMENTO)%>
			    </div> 
            </div>
   </div>

    <div class ="span-24 last">
        <div class="span-8 column last">
             <div class="editor-label">
					<%: Html.LabelFor(model => model.correlativo.NRO_CONTROL)%>
			</div>
            <div class="editor-field">
				<%: Html.TextBoxFor(model => model.correlativo.NRO_CONTROL)%>
				<%: Html.ValidationMessageFor(model => model.correlativo.NRO_CONTROL)%>
			</div> 
        </div>
    </div>
    
     