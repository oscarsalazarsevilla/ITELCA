﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<ITELCA_CONTROLESTUDIOS.Models.ViewModels.SeccionViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    [Itelca - Seccion]
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
<div class="span-22 append-1 prepend-1 column last ui-widget-content ui-corner-all">
	<span class="introForm">Seccion > Crear</span>
		<% using (Html.BeginForm("Crear", "Seccion", FormMethod.Post, new { enctype = "multipart/form-data" }))
     { %>
		<% Html.EnableClientValidation(); %>
        <%:Html.ValidationSummary("Error en la aplicación") %>
        <%:Html.HiddenFor(model => model.seccion.ESTADO, new { Value = "1" })%>
        <%:Html.HiddenFor(model => model.seccion.CONDICION, new { Value = "0" })%>
            <div  class="span-24 Formulario  column last">
				<% Html.RenderPartial("Form",Model); %>
			</div>
             <div class="separator column span-24">
            </div>
            <p>
				<input type="submit" value="Crear" class="button" />
			</p>
           
            <div class="column span-24">
                <%: Html.ActionLink("Volver al listado", "Listar")%>
            </div>
          
			
    <% } %>
     
                
</div>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="CssContent" runat="server">
 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="JsContent" runat="server">
    <script src="/Scripts/jquery.maskedinput-1.2.2.js" type="text/javascript"></script>
    <script src="/Scripts/ckeditor/ckeditor.js" type="text/javascript"></script>
    <script src="/Scripts/ckeditor/jqueryTimePicker.js" type="text/javascript"></script>
    <script src="/Scripts/Views/seccion.js" type="text/javascript"></script>
</asp:Content>

