﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<ITELCA_CONTROLESTUDIOS.Models.ViewModels.DescuentosCursosViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    [Itelca - Descuentos Cursos]
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
<div class="span-22 append-1 prepend-1 column last ui-widget-content ui-corner-all">
	<span class="introForm">Descuentos Cursos > Editar</span>
		<% using (Html.BeginForm("Editar", "DescuentosCursos", FormMethod.Post, new { id = "formDescuentosCursos" })) { %>
		<% Html.EnableClientValidation(); %>
        <%:Html.ValidationSummary("Error en la aplicación") %>
        <%:Html.HiddenFor(model => model.DESCUENTOS_CURSOS.DECUENTO_CURSO_ID) %>
        <%:Html.HiddenFor(model => model.DESCUENTOS_CURSOS.USUARIO_ID)%>
        <%-- <%:Html.Hidden("listacursos", Model.ListaCursosSeleccionados)%>--%>
            <div  class="span-24 Formulario  column last">

            <div class ="span-24 last">
                <div class="span-8 column last">
                     <div class="editor-label">
					        <%: Html.LabelFor(model => model.DESCUENTOS_CURSOS.CURSO_ID)%>
			        </div>
                    <div class="editor-field">
				        <%: Html.DropDownListFor(model => model.DESCUENTOS_CURSOS.CURSO_ID, Model.ListaCursos, "--Seleccione--")%>
				        <%: Html.ValidationMessageFor(model => model.DESCUENTOS_CURSOS.CURSO_ID)%>
			        </div> 
                </div>
            </div>
             <div class ="span-24 last">
               <div class="span-8 column last">
                     <div class="editor-label">
					        <%: Html.LabelFor(model => model.DESCUENTOS_CURSOS.TURNO_ID)%>
			        </div>
                    <div class="editor-field">
				        <%: Html.DropDownListFor(model => model.DESCUENTOS_CURSOS.TURNO_ID, Model.ListaTurnos, "--Seleccione--")%>
				        <%: Html.ValidationMessageFor(model => model.DESCUENTOS_CURSOS.TURNO_ID)%>
			        </div> 
                </div>
           </div>

				<% Html.RenderPartial("Form",Model); %>
			</div>
             <div class="separator column span-24">
            </div>
            <p>
				<input type="submit" value="Guardar" class="button" />
			</p>
           
            <div class="column span-24">
                <%: Html.ActionLink("Volver al listado", "Listar")%>
            </div>
         
			
    <% } %>
</div>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="CssContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="JsContent" runat="server">
    <script src="/Scripts/autoNumeric.js" type="text/javascript"></script>
    <script src="/Scripts/Views/DescuentosCursos.js" type="text/javascript"></script>
</asp:Content>

