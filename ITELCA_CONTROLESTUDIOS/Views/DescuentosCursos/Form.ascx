﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ITELCA_CONTROLESTUDIOS.Models.ViewModels.DescuentosCursosViewModel>" %>
       
   
    <div class ="span-24 last">
        <div class="span-8 column last ">
             <div class="editor-label">
					<%: Html.LabelFor(model => model.DESCUENTOS_CURSOS.MONTO)%>
			</div>
            <div class="editor-field">
				<%: Html.TextBoxFor(model => model.DESCUENTOS_CURSOS.MONTO, new { @class = "montos" })%>
				<%: Html.ValidationMessageFor(model => model.DESCUENTOS_CURSOS.MONTO)%>
			</div> 
        </div>    
    </div>
     <div class ="span-24 last">
        <div class="span-8 column last">
                 <div class="editor-label">
					    <%: Html.LabelFor(model => model.DESCUENTOS_CURSOS.FECHA_INIICIO)%>
			    </div>
                <div class="editor-field">
				    <%: Html.TextBoxFor(model => model.DESCUENTOS_CURSOS.FECHA_INIICIO, new { @class = "datepicker" })%>
				    <%: Html.ValidationMessageFor(model => model.DESCUENTOS_CURSOS.FECHA_INIICIO)%>
			    </div> 
            </div>   
    </div>
     <div class ="span-24 last">
        <div class="span-8 column last">
                <div class="editor-label">
					<%: Html.LabelFor(model => model.DESCUENTOS_CURSOS.FECHA_FIN)%>
			</div>
            <div class="editor-field">
				<%: Html.TextBoxFor(model => model.DESCUENTOS_CURSOS.FECHA_FIN, new { @class = "datepicker" })%>
				<%: Html.ValidationMessageFor(model => model.DESCUENTOS_CURSOS.FECHA_FIN)%>
			</div> 
        </div>    
    </div>
     