﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<ITELCA_CONTROLESTUDIOS.Models.ViewModels.DescuentosCursosViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    [Itelca - Descuento Curso]
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
<div class="span-22 append-1 prepend-1 column last ui-widget-content ui-corner-all">
	<span class="introForm">Descuentos Cursos > Crear</span>
		<% using (Html.BeginForm("Crear", "DescuentosCursos", FormMethod.Post, new { id = "formDescuentosCursos" })) { %>
		<% Html.EnableClientValidation(); %>
         <%:Html.Hidden("listacursos") %>
          <%:Html.Hidden("listaTurnos") %>
        <%:Html.ValidationSummary("Error en la aplicación") %>

            <div  class="span-24 Formulario  column last">

            <div class ="span-24 last">
                <div class="span-8 column last">
                     <div class="editor-label">
					        <%: Html.LabelFor(model => model.DESCUENTOS_CURSOS.CURSO_ID)%>
			        </div>
                    <div class="editor-field">
				        <%: Html.DropDownListFor(model => model.DESCUENTOS_CURSOS.CURSO_ID, Model.ListaCursos, new { multiple = "multiple", required = "required" })%>
				        <%: Html.ValidationMessageFor(model => model.DESCUENTOS_CURSOS.CURSO_ID)%>
			        </div> 
                </div>
            </div>
             <div class ="span-24 last">
               <div class="span-8 column last">
                     <div class="editor-label">
					        <%: Html.LabelFor(model => model.DESCUENTOS_CURSOS.TURNO_ID)%>
			        </div>
                    <div class="editor-field">
				        <%: Html.DropDownListFor(model => model.DESCUENTOS_CURSOS.TURNO_ID, Model.ListaTurnos, "--Seleccione--", new { multiple = "multiple", required = "required" })%>
				        <%: Html.ValidationMessageFor(model => model.DESCUENTOS_CURSOS.TURNO_ID)%>
			        </div> 
                </div>
           </div>
				<% Html.RenderPartial("Form",Model); %>
			</div>
             <div class="separator column span-24">
            </div>
            <p>
				<input type="submit" value="Crear" class="button" />
			</p>
           
            <div class="column span-24">
                <%: Html.ActionLink("Volver al listado", "Listar")%>
            </div>
          
			
    <% } %>
     
                
</div>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="CssContent" runat="server">
    <link href="/Content/Css/multiselect.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="JsContent" runat="server">
    <script src="/Scripts/jquery.multiselect.min.js" type="text/javascript"></script>
    <script src="/Scripts/jquery.multiselect.es.js" type="text/javascript"></script>
    <script src="/Scripts/jquery.maskedinput-1.2.2.js" type="text/javascript"></script>
    <script src="/Scripts/autoNumeric.js" type="text/javascript"></script>
    <script src="/Scripts/Views/DescuentosCursos.js" type="text/javascript"></script>
    <script type="text/javascript">

        function validarSeleccionados(mmultiselect_nombre,campo) {
            var array_of_checked_values = $("#" + mmultiselect_nombre).multiselect("getChecked").map(function () {
                return this.value;
            }).get();

            $("#"+campo).val(array_of_checked_values);

        }

        $(document).ready(function () {

            $("#DESCUENTOS_CURSOS_CURSO_ID").multiselect({
                selectedText: "# de # seleccionado",
                selectedList: 5,
                uncheckAll: function (event, ui) {

                },
                checkAll: function (event, ui) {
                    validarSeleccionados("DESCUENTOS_CURSOS_CURSO_ID", "listacursos");
                },
                click: function (event, ui) {
                    validarSeleccionados("DESCUENTOS_CURSOS_CURSO_ID", "listacursos");
                }
            });
            $("#DESCUENTOS_CURSOS_TURNO_ID").multiselect({
                selectedText: "# de # seleccionado",
                selectedList: 5,
                uncheckAll: function (event, ui) {

                },
                checkAll: function (event, ui) {
                    validarSeleccionados("DESCUENTOS_CURSOS_TURNO_ID", "listaTurnos");
                },
                click: function (event, ui) {
                    validarSeleccionados("DESCUENTOS_CURSOS_TURNO_ID","listaTurnos");
                }
            });

        });


 
    </script>
</asp:Content>

