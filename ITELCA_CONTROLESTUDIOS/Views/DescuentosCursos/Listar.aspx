﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<ITELCA_CONTROLESTUDIOS.Models.ViewModels.DescuentosCursosViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Listar
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2>Listar descuentos de los cursos</h2>
<fieldset>
<div class="span-24 last">
        <p>
            <%: Html.ActionLink("Nuevo", "Crear") %>
        </p>
        <div id="msg">
        <%
            try
            {

                if (TempData["mensaje"] != null && TempData["mensaje"]  != "")
                {%>
			    <div  class="ui-state-highlight ui-corner-all" style="margin-top: 10px; padding: 0 .3em; width:30%; height:20px;">
				    <span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
				    <div><%:TempData["mensaje"]  %></div>
			    </div>
		    
          <%
                }
            }
            catch{} 
            
           %>
        </div>
        <div>
                <h4><%:Html.Label("Curso") %></h4>
                <%:Html.DropDownList("SelectListCurso", Model.ListaCursos, "Seleccione")%>
           
                  </br>
                  </br>
                  </br>
                    <table id="gridCurso">
                    </table>
                    <div id="pager"></div>
            </div>
</div>
</fieldset>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="CssContent" runat="server">
    <link href="/Content/Css/ui.jqgrid.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="JSContent" runat="server">
    <script src="/Scripts/grid.locale-es.js" type="text/javascript"></script>
    <script src="/Scripts/jquery.jqGrid.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            winHeight = window.innerHeight;
            wHeight = winHeight - 340;

            var gridCurso = $("#gridCurso").jqGrid({
                postData: { cursoId: function () { return $("#SelectListCurso option:selected").val() } },
                url: '/DescuentosCursos/ObtenerData',
                datatype: 'json',
                mtype: 'POST',
                colNames: ['Id', 'Curso', 'Turno', 'Fecha Inicio', 'Fecha Fin', 'Monto', 'Editar', 'Eliminar'],
                colModel: [
		            { name: 'DECUENTO_CURSO_ID', index: 'DECUENTO_CURSO_ID', align: "right", resizable: true, width: 50, searchoptions: { sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']} },
		            { name: 'CURSOS.NOMBRE', index: 'CURSOS.NOMBRE', width: 200, align: "left", searchoptions: { sopt: ['cn']} },
                    { name: 'TURNOS.NOMBRE', index: 'TURNOS.NOMBRE', width: 200, searchoptions: { sopt: ['cn']} },
                    { name: 'FECHA_INICIO', index: 'FECHA_INICIO', align: "right", resizable: true, width: 100, sorttype: 'date', searchoptions: { dataInit: function (elem) {
                        $(elem).datepicker({
                            changeYear: true,
                            changeMonth: true,
                            showButtonPanel: true,
                            onSelect: function () {
                                if (this.id.substr(0, 3) === "gs_") {
                                    // in case of searching toolbar
                                    setTimeout(function () {
                                        myGrid[0].triggerToolbar();
                                    }, 50);
                                } else {
                                    // refresh the filter in case of
                                    // searching dialog
                                    $(this).trigger('change');
                                }
                            }
                        });
                    }
                    }
                    },
                    { name: 'FECHA_FIN', index: 'FECHA_FIN', align: "right", resizable: true, width: 100, sorttype: 'date', searchoptions: { dataInit: function (elem) {
                        $(elem).datepicker({
                            changeYear: true,
                            changeMonth: true,
                            showButtonPanel: true,
                            onSelect: function () {
                                if (this.id.substr(0, 3) === "gs_") {
                                    // in case of searching toolbar
                                    setTimeout(function () {
                                        myGrid[0].triggerToolbar();
                                    }, 50);
                                } else {
                                    // refresh the filter in case of
                                    // searching dialog
                                    $(this).trigger('change');
                                }
                            }
                        });
                    }
                    }
                    },
                    { name: 'MONTO', index: 'MONTO', align: "right", resizable: true, width: 100, searchoptions: { sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']} },
                    { name: 'Action1', index: 'Action1', align: "left", width: 50, resizable: false, sortable: false, search: false },
                    { name: 'Action2', index: 'Action2', align: "left", width: 50, resizable: false, sortable: false, search: false}],
                pager: jQuery('#pager'),
                rowNum: 500,
                rowList: [500, 1000, 5000],
                height: wHeight,
                sortname: 'CURSOS.NOMBRE',
                sortorder: 'asc',
                autowidth: true,
                viewrecords: true,
                caption: 'Listado de Descuentos por Cursos',
                loadComplete: function () {
                    $(".ui-icon-close").click(function () {
                        var gr = $(this).attr("id");
                        var r = confirm("Desea eliminar el descuento del curso?");
                        if (r == true) {
                            $.post("/DescuentosCursos/Eliminar", { id: gr }, function (data) {
                                var dat = ' <div  class="ui-state-highlight ui-corner-all" style="margin-top: 10px; padding: 0 .3em; width:30%; height:20px;"><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span><div>';
                                dat += data[0].msg;
                                dat += '</div></div>';
                                $("#msg").empty().html(dat);
                                gridCurso.trigger('reloadGrid');

                            });
                        }
                        else {
                            x = "Cancelar";
                        }
                        // alert("adas");
                    });

                }
            }).navGrid('#pager',
            {
                edit: false, add: false, del: false, search: true, refresh: true
            },
            {}, //edit options
            {}, //add options
            {}, //del options
            {multipleSearch: true} // search options
            );

            $('#SelectListCurso').change(function () {
                jQuery("#gridCurso").trigger("reloadGrid");
            });

        });
    </script>
</asp:Content>
