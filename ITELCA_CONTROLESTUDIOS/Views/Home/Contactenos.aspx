﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Web.Master" Inherits="System.Web.Mvc.ViewPage<ITELCA_CONTROLESTUDIOS.Models.ViewModels.ViewModelForViewWrapper<ITELCA_CONTROLESTUDIOS.Models.ViewModels.MasterViewModel,ITELCA_CONTROLESTUDIOS.Models.ViewModels.HomeViewModel>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Contacto
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<div class="span-20 prepend-4 column last">
    <div class="span-12 prepend-5">
        <div class="span-8 column">
         <img src="/Content/Imagenes/Contactenos.jpg" height="50px" width="50px"  alt="CONTACTO"/>
        </div>
        <div class="span-16 column ">
            <div id="Div1" ><h1>Planilla de Contacto</h1></div>
           
        </div>
    </div>
    <% foreach (var sede in Model.View.sedes) { %>
       <div class="divSedes" id="<%: sede.NOMBRE %>" lat="<%: sede.LATITUD %>" long="<%: sede.LONGITUD %>" telf="<%: sede.TELEFONO1 %>"></div>
    <% } %>

     <div  class="span-6 column prepend-1 last">
     <% using (Html.BeginForm()) { %>
		<% Html.EnableClientValidation(); %>
            <div  class="span-24 Formulario  column last">
				<div class="span-24 column ">
                     <div class="editor-label">
					    <%: Html.LabelFor(model => model.View.CONTACTO.NOMBRE)%>
			        </div>
                    <div class="editor-field">
				        <%: Html.TextBoxFor(model => model.View.CONTACTO.NOMBRE)%>
				        <%: Html.ValidationMessageFor(model => model.View.CONTACTO.NOMBRE)%>
			        </div> 
                </div>
                <div class="span-24 column ">
                     <div class="editor-label">
					    <%: Html.LabelFor(model => model.View.CONTACTO.EMAIL)%>
			        </div>
                    <div class="editor-field">
				        <%: Html.TextBoxFor(model => model.View.CONTACTO.EMAIL)%>
				        <%: Html.ValidationMessageFor(model => model.View.CONTACTO.EMAIL)%>
			        </div> 
                </div>
                <div class="span-24 column ">
                     <div class="editor-label">
					    <%: Html.LabelFor(model => model.View.CONTACTO.TELEFONO)%>
			        </div>
                    <div class="editor-field">
				        <%: Html.TextBoxFor(model => model.View.CONTACTO.TELEFONO)%>
				        <%: Html.ValidationMessageFor(model => model.View.CONTACTO.TELEFONO)%>
			        </div> 
                </div>
                <div class="span-24 column ">
                     <div class="editor-label">
					    <%: Html.LabelFor(model => model.View.CONTACTO.ASUNTO)%>
			        </div>
                    <div class="editor-field">
				        <%: Html.TextBoxFor(model => model.View.CONTACTO.ASUNTO)%>
				        <%: Html.ValidationMessageFor(model => model.View.CONTACTO.ASUNTO)%>
			        </div> 
                </div>
                <div class="span-24 column ">
                     <div class="editor-label">
					    <%: Html.LabelFor(model => model.View.CONTACTO.TEXTO)%>
			        </div>
                    <div class="editor-field">
				        <%: Html.TextAreaFor(model => model.View.CONTACTO.TEXTO)%>
				        <%: Html.ValidationMessageFor(model => model.View.CONTACTO.TEXTO)%>
			        </div> 
                </div>
			</div>

            <p>
				<input type="submit" value="Crear" class="button" />
			</p>
    <% } %>
         </div>     
    <div  class="span-12  prepend-1 column last">
        <div class="span-24 column last" style="font-size:9pt;  line-height: 15px;">
            <%= Model.View.contactenos.TEXTO%>
        </div>
        
    </div>
    
     <div  class="span-12  prepend-1 column last">
        <div id='location-canvas' style='width:100%;height:300px;'></div>
     </div>

</div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="CssContent" runat="server">
<style type="text/css">
 input, textarea
 {
  width:95%;   
  }
  
  .button
  {
    width:25%;    
  }
  
  .field-validation-error{
    font-size:12px;   
  }
</style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="JsContent" runat="server">
 <script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>



<script type="text/javascript">

    $(document).ready(function () {

        $("#btnEnviar").click(function (event) {

            var nombre = $("#Nombre").val();
            var email = $("#email").val();
            var telefono = $("#telefono").val();
            var asunto = $("#asunto").val();
            var texto = $("#Textarea1").val();

            $.post("/Home/guardarContacto", { nombre: nombre, email: email, telefono: telefono, asunto: asunto, texto: texto }, function (data) {
                if (data == true) {

                } else {

                }
                $(".camposForm").val("");

            });
        });

        function initialize() {

            var myLatlng = new google.maps.LatLng(10.4980754, -66.8931579);

            var mapOptions = {
                zoom: 14,
                center: myLatlng
            }

            var map = new google.maps.Map(document.getElementById('location-canvas'), mapOptions);

            $(".divSedes").each(function (index) {
                var lat = $(this).attr("lat");
                var long = $(this).attr("long");
                var nombre = 'SEDE ' + $(this).attr("id");
                var telefono = $(this).attr("telf");

                var varPosicion = new google.maps.LatLng(lat, long);


                var contentString = '<div id="contentsmall" style="width:auto; overflow:hidden;">' +
              '<h5 id="firstHeading" class="firstHeading">' + nombre + '</h5>' +
              '<div id="bodyContent" style="font-size:10pt;">' +
              'Teléfono:' + telefono +
              '</div>' +
              '</div>';

                var infowindow = new google.maps.InfoWindow({
                    content: contentString
                });

                var marker = new google.maps.Marker({
                    position: varPosicion,
                    map: map,
                    title: nombre
                });

                google.maps.event.addListener(marker, 'click', function () {
                    infowindow.open(map, marker);
                });


            });
        }

        google.maps.event.addDomListener(window, 'resize', initialize);
        google.maps.event.addDomListener(window, 'load', initialize);
    });


</script>
</asp:Content>
