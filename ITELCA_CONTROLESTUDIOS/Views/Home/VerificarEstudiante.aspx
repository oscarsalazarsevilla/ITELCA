﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    VerificarEstudiante
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2>VerificarEstudiante</h2>
    <div id="dialog-message">
        <div id="delete-message"></div>    
    </div>
<div class="span-24 column last" >
    <div class="prepend-1 span-21 column last">
    <fieldset>
        <div class="span-24 column last">
            <div class="prepend-8 span-8 column">
                 <div class="display-label">
					    <%: Html.Label("Buscar")%>
			    </div>
                <div class="display-field" style="height:30px;">
				    <%: Html.TextBox("data")%>
			    </div> 
            </div>
             <div class="span-8 column">
                <div class="display-field">
				     <h4 style="height:11px;"  id="mensajeError">&nbsp;</h4>
			    </div> 
            </div>
        </div>
        <div class="span-24 column last">
            <div class="span-8 column" >
                <div class="span-22 last" id="foto" style="margin-top:5%;border: 2px solid;border-radius: 25px;">
                    <div class="span-22 prepend-2">
                        <img src="/Content/Imagenes/fotoE.png"  height="250px" width="200px" id="imgFoto" />
                    </div>
                </div>
                <div class="span-24 last" id="acceso">
                    <h4>MENSAJE</h4>
                </div>
            </div>
            <div class="span-16 column last">
                <div class="span-24 column last">
                    <div class="span-12 column ">
                        <div class="display-label">
					        <%: Html.Label("Nombre")%>
			            </div>
                        <div class="display-field">
				            <h4 style="height:35px;" id="nombre">&nbsp;</h4>
			            </div> 
                        </div>
                    <div class="span-12 column last">
                        <div class="display-label">
					        <%: Html.Label("Cédula")%>
			            </div>
                        <div class="display-field">
				            <h4 style="height:35px;" id="cedula">&nbsp;</h4>
			            </div> 
                    </div>
                </div>
                <div class="span-24 column last">
                    <div class="span-12 column">
                         <div class="display-label">
					            <%: Html.Label("Correo")%>
			            </div>
                        <div class="display-field">
				            <h4 style="height:35px;" id="correo">&nbsp;</h4>
			            </div> 
                    </div>
                    <div class="span-12 column last">
                         <div class="display-label">
					            <%: Html.Label("Curso")%>
			            </div>
                        <div class="display-field">
				            <h4 style="height:35px;" id="curso"><select id="ListaCursos" disabled="disabled"></select> </h4>
			                  
                        </div> 
                    </div>
                </div>
                 <div class="span-24 column last">
                    <div class="span-12 column ">
                         <div class="display-label">
					            <%: Html.Label("Último Pago")%>
			            </div>
                        <div class="display-field">
				            <h4 style="height:35px;" id="fechaUpago">&nbsp;</h4>
			            </div> 
                    </div>
                    <div class="span-12 column last">
                         <div class="display-label">
					            <%: Html.Label("Descripción")%>
			            </div>
                        <div class="display-field">
				            <h4 style="height:35px;" id="descUPago">&nbsp;</h4>
			            </div> 
                    </div>
                </div>
                  <div class="span-24 last">
                    <div class="span-12 column ">
                         <div class="display-label">
					            <%: Html.Label("Próximo Pago")%>
			            </div>
                        <div class="display-field">
				            <h4  style="height:35px;" id="fechaPPago">&nbsp;</h4>
			            </div> 
                    </div>
                    <div class="span-12 column  last">
                         <div class="display-label">
					            <%: Html.Label("Descripción")%>
			            </div>
                        <div class="display-field">
				            <h4 style="height:35px;" id="descPPago">&nbsp;</h4>
			            </div> 
                    </div>
                </div>
            </div>
        </div>
        </fieldset>
    </div>
</div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="CssContent" runat="server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="JSContent" runat="server">
    <script src="/Scripts/Views/home.js" type="text/javascript"></script>
</asp:Content>
