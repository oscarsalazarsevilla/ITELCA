﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Web.Master" Inherits="System.Web.Mvc.ViewPage<ITELCA_CONTROLESTUDIOS.Models.ViewModels.ViewModelForViewWrapper<ITELCA_CONTROLESTUDIOS.Models.ViewModels.MasterViewModel,ITELCA_CONTROLESTUDIOS.Models.ViewModels.HomeViewModel>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Curso
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<div class="common-box2">
    <div id="letterpress"><h1><%= Model.View.curso.NOMBRE %></h1></div>
    <div id="descripcion"><p><%= Model.View.curso.DESCRIPCION%></p></div>
</div>


</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="CssContent" runat="server">

        <style>
        
        #letterpress h1 {
            color: #006699;
            font-family: Arial,Helvetica,sans-serif;
            font-size: 22px;
            text-shadow: 0 2px 1px #bbbaba;
        }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="JsContent" runat="server">
</asp:Content>
