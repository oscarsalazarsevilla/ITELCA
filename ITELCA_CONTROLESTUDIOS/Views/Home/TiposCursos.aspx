﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Web.Master" Inherits="System.Web.Mvc.ViewPage<ITELCA_CONTROLESTUDIOS.Models.ViewModels.ViewModelForViewWrapper<ITELCA_CONTROLESTUDIOS.Models.ViewModels.MasterViewModel,ITELCA_CONTROLESTUDIOS.Models.ViewModels.HomeViewModel>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Tipos de Cursos
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<div class="common-box2">
    <div id="letterpress"><h1><%= Model.View.tipoCurso.NOMBRE %></h1></div>
    <div id="descripcion"><p><%= Model.View.tipoCurso.DESCRIPCION%></p></div>
</div>

<div id="idTipoCurso" value="<%= Model.View.tipoCurso.TIPO_CURSO_ID%>"></div>
<%if (Model.View.tipoCurso != null)
  {
      if (Model.View.tipoCurso.CURSOS != null && Model.View.tipoCurso.CURSOS.Count() > 0)
      {
          %>
            <div class="common-box2 titulo-taller">
            <ul>
          <%
          bool ban = true;
          foreach (var aux in Model.View.tipoCurso.CURSOS)
          {
              if (ban)
              {
                  ban = false;
        %>
        <div class="span-24 column last">
           <li class="span-11 column"><a href="/Home/Cursos/<%:aux.CURSO_ID%>"><%:aux.NOMBRE%></a></li>     
        <%
              }
              else
              {
               %>
                 <li class="span-11 column last"><a href="/Home/Cursos/<%:aux.CURSO_ID%>"><%:aux.NOMBRE%></a></li>
                 </div>     
               <%
                ban = true;
              }
        }       
        %>  
        </ul>
        </div>
        <%
      }
        }%>

         </br>
        </br>
        </br>
        </br>
        </br>
        <div class="common-box2" >
          <div id="letterpress" style="  padding-top: 15px;"><h1> Próximos Talleres</h1></div>
        <table id="example" class="display" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>Curso</th>
                    <th>Módulo</th>
                    <th>Descripción</th>
                    <th>Fecha Inicio</th>
                    <th>Fecha Fin</th>
                </tr>
            </thead> 
            <tbody>
           <% 
             if (Model.View.proximosCursos.Count() > 0)
              {
                  foreach (var modulo in Model.View.proximosCursos)
                  {
                      if (modulo.FECHA_INICIO >= Model.View.fechaActual)
                      { %>
                       <tr>
                            <td><%:modulo.CURSOS_MODULOS.CURSOS.NOMBRE%></td>
                            <td><%:modulo.CURSOS_MODULOS.MODULOS.NOMBRE%></td>
                            <td><%:modulo.CURSOS_MODULOS.MODULOS.DESCRIPCION%></td>
                            <td><%:modulo.FECHA_INICIO.ToShortDateString()%></td>
                             <td><%:DateTime.Parse(modulo.FECHA_FIN.ToString()).ToShortDateString()%></td>
                       </tr>
          <% 
                        }
                    }
              }%>
            </tbody>
            </table>
        </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="CssContent" runat="server">

    <link href="../../Content/Css/itelca/demo_table_jui.css" rel="stylesheet" type="text/css" />
    <style>
        .titulo-taller a 
        {
            text-decoration: none;  
            color: #000000;  
            cursor:pointer;
            font-size: 14px;
            
        }
        a:visited
        {
             text-decoration: none;  
            color: #000000;  
            cursor:pointer;
            font-size: 14px;
        }
        

        
        #letterpress h1 {
            color: #006699;
            font-family: Arial,Helvetica,sans-serif;
            font-size: 22px;
            text-shadow: 0 2px 1px #bbbaba;
        }
        
        table.display
        {
            border: 1px solid #E2E4FF;    
        }
        
        .dataTables_filter, .dataTables_info, .dataTables_length { display: none; }
        
        li { 
           color: #006699; 
        }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="JsContent" runat="server">

    <script src="../../Scripts/jquery.dataTables.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#example').dataTable({ bFilter: false, bInfo: false,
                "oLanguage": {
                    "sZeroRecords": "No hay Cursos Planificados",
                    "oPaginate": {
                        "sFirst": "<<", // This is the link to the first page
                        "sPrevious": "Anterior     ", // This is the link to the previous page
                        "sNext": "Siguiente", // This is the link to the next page
                        "sLast": ">>" // This is the link to the last page
                    }
                },
                "aoColumns": [
                { "bSortable": false },
                { "bSortable": false },
                { "bSortable": false },
                { "bSortable": false },
                { "bSortable": false }
            ]
            });
        });
          
    </script>
</asp:Content>
