﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    [Itelca - Control de estudios]
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2>Sistema de control de estudios</h2>

<div id="hero" class="span-22 append-1 prepend-1 column last alignCenter ui-widget-content ui-corner-all">
    <h1><%= ViewBag.Mensaje %></h1>
    <div class="separator"></div>
    <div class="span-12 append-6 prepend-6 column">
        <fieldset>
            <legend>Accesos directos</legend>
            <div id="k2QuickIcons">
                <% if (User.IsInRole("WEB")) 
                   {
                       Html.RenderPartial("AccesosWeb");
                   }
                   else if (User.IsInRole("ADMINISTRADOR"))
                   {
                       Html.RenderPartial("AccesosAdmin");
                   }
                   else if (User.IsInRole("COORDINADOR"))
                   {
                       Html.RenderPartial("AccesosControlEstudios");
                   }
                   else if (User.IsInRole("ESTUDIANTE"))
                   {
                       Html.RenderPartial("AccesosEstudiante");
                   }
                   else if (User.IsInRole("PROFESOR"))
                   {
                       Html.RenderPartial("AccesosProfesor");
                   }
                   else if (User.IsInRole("CAJERO"))
                   {
                       Html.RenderPartial("AccesosCajero");
                   }
                   else if (User.IsInRole("PROMOTOR"))
                   {
                       Html.RenderPartial("AccesosPromotor");
                   }
                   
                   
                   %>
                <div class="icon-wrapper">
                    <div class="icon">
                        <a href="<%= Url.Action("CambiarClave","Usuario") %>">
                            <img src="/Content/Imagenes/contrasena.png" alt="Cambiar contraseña" />
                            <span>Cambiar contraseña</span>
                        </a>
                    </div>
                </div>
                <div class="icon-wrapper">
                    <div class="icon">
                        <a href="/Help/index.html" target="_blank">
                            <img src="/Content/Imagenes/ayuda.png" alt="Ayuda" />
                            <span>Ayuda</span>
                        </a>
                    </div>
                </div>
            </div> 
        </fieldset>
    </div>
</div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="CssContent" runat="server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="JSContent" runat="server">
</asp:Content>
