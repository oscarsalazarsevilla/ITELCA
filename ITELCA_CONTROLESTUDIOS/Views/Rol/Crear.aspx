﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<ITELCA_CONTROLESTUDIOS.Models.ViewModels.RolViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    [Itelca - Rol]
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
<div class="span-22 append-1 prepend-1 column last ui-widget-content ui-corner-all">
	<span class="introForm">Rol > Crear</span>
		<% using (Html.BeginForm()) { %>
		<% Html.EnableClientValidation(); %>
        <%:Html.ValidationSummary("Error en la aplicación") %>
        <%:Html.HiddenFor(model => model.rol.ESTADO, new { Value = "1" }) %>
            <div  class="span-24 Formulario  column last">
					<div class="span-8 column"> 
                        <%: Html.EditorFor(model => model.rol.NOMBRE)%>
					</div>
			</div>
            <div class="separator column span-24"></div>
			<p>
				<input type="submit" value="Crear" class="button" />
			</p>
    <% } %>
    <div class="separator column span-24">
    </div>
    <div class="column span-24">
        <%: Html.ActionLink("Volver al listado", "Listar")%>
    </div>
</div>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="CssContent" runat="server">
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="JsContent" runat="server">
  
</asp:Content>