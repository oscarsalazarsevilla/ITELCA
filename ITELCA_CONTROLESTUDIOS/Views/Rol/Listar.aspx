﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Listar
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2>Listar</h2>
<fieldset>
<div class="span-24 last">
        <p>
            <%: Html.ActionLink("Nuevo", "Crear") %>
        </p>
        <div id="msg">
        <%
            try
            {

                if (TempData["mensaje"] != null && TempData["mensaje"]  != "")
                {%>
			    <div  class="ui-state-highlight ui-corner-all" style="margin-top: 10px; padding: 0 .3em; width:30%; height:20px;">
				    <span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
				    <div><%:TempData["mensaje"]  %></div>
			    </div>
		    
          <%
                }
            }
            catch{} 
            
           %>
        </div>
            <table id="gridRol">
            </table>
            <div id="pager"></div>
</div>
</fieldset>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="CssContent" runat="server">
    <link href="/Content/Css/ui.jqgrid.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="JSContent" runat="server">
    <script src="/Scripts/grid.locale-es.js" type="text/javascript"></script>
    <script src="/Scripts/jquery.jqGrid.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {

            winHeight = window.innerHeight;
            wHeight = winHeight - 340;

            var gridUsuario = $("#gridRol").jqGrid({
                url: '/Rol/ObtenerData',
                datatype: 'json',
                mtype: 'POST',
                colNames: ['Id', 'Nombre','Estado','Editar', 'Eliminar'],
                colModel: [
		            { name: 'NOMBREUSUARIO', index: 'NOMBREUSUARIO', align: "right", resizable: true, width: 50, searchoptions: { sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']} },
		            { name: 'NOMBRE', index: 'NOMBRE', width: 200, searchoptions: { sopt: ['cn']} },
                    { name: 'ESTADO', index: 'ESTADO', width: 50, align: "center", resizable: true, formatter: 'checkbox', stype: "select", searchoptions: { sopt: ['eq', 'ne'], value: "1:Si;0:No"} },
		            { name: 'Action1', index: 'Action1', align: "center", width: 50, resizable: false, sortable: false, search: false },
                    { name: 'Action2', index: 'Action2', align: "center", width: 50, resizable: false, sortable: false, search: false}],
                pager: jQuery('#pager'),
                rowNum: 500,
                height: wHeight,
                rowList: [500, 1000, 5000],
                sortname: 'ROL_ID',
                sortorder: 'desc',
                autowidth: true,
                viewrecords: true,
                caption: 'Listado de Roles',
                loadComplete: function () {
                    $(".ui-icon-close").click(function () {
                        var gr = $(this).attr("id");
                        var r = confirm("Desea Eliminar El Rol ");
                        if (r == true) {
                            $.post("/Rol/Eliminar", { id: gr }, function (data) {
                                var dat = ' <div  class="ui-state-highlight ui-corner-all" style="margin-top: 10px; padding: 0 .3em; width:30%; height:20px;"><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span><div>';
                                dat += data[0].msg;
                                dat += '</div></div>';
                                $("#msg").empty().html(dat);
                                gridUsuario.trigger('reloadGrid');
                               
                            });
                        }
                        else {
                            x = "Cancelar";
                        }
                        // alert("adas");
                    });

                }
            }).navGrid('#pager',
            {
                edit: false, add: false, del: false, search: true, refresh: false
            },
            {}, //edit options
            {}, //add options
            {}, //del options
            {multipleSearch: true} // search options
            );
        });
    </script>
</asp:Content>
