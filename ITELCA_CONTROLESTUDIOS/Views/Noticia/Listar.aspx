﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Listar
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2>Listar</h2>
<fieldset>
<div class="span-24 last">
        <p>
            <%: Html.ActionLink("Nuevo", "Crear") %>
        </p>
        <div id="msg">
        <%
            try
            {

                if (TempData["mensaje"] != null && TempData["mensaje"]  != "")
                {%>
			    <div  class="ui-state-highlight ui-corner-all" style="margin-top: 10px; padding: 0 .3em; width:30%; height:20px;">
				    <span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
				    <div><%:TempData["mensaje"]  %></div>
			    </div>
		    
          <%
                }
            }
            catch{} 
            
           %>
        </div>
            <table id="gridNoticia">
            </table>
            <div id="pager"></div>
</div>
</fieldset>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="CssContent" runat="server">
    <link href="/Content/Css/ui.jqgrid.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="JSContent" runat="server">
    <script src="/Scripts/grid.locale-es.js" type="text/javascript"></script>
    <script src="/Scripts/jquery.jqGrid.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            winHeight = window.innerHeight;
            wHeight = winHeight - 340;

            var gridNoticia = $("#gridNoticia").jqGrid({
                url: '/Noticia/ObtenerData',
                datatype: 'json',
                mtype: 'POST',
                colNames: ['Id', 'Título', 'Resumen', 'Publicación', 'Fin publicación', 'Activo?','Editar', 'Eliminar'],
                colModel: [
		            { name: 'NOTICIA_ID', index: 'NOTICIA_ID', align: "right", width: 50, resizable: true, searchoptions: { sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']} },
		            { name: 'TITULO', index: 'TITULO', width: 200, resizable: true, searchoptions: { sopt: ['cn']} },
		            { name: 'RESUMEN', index: 'RESUMEN', width: 200, resizable: true, searchoptions: { sopt: ['cn']} },
                    { name: 'FECHA_PUBLICACION', index: 'FECHA_PUBLICACION', width: 100, align: "left", resizable: true, sorttype: 'date', searchoptions: { dataInit: function (elem) {
                        $(elem).datepicker({
                            changeYear: true,
                            changeMonth: true,
                            showButtonPanel: true,
                            onSelect: function () {
                                if (this.id.substr(0, 3) === "gs_") {
                                    // in case of searching toolbar
                                    setTimeout(function () {
                                        myGrid[0].triggerToolbar();
                                    }, 50);
                                } else {
                                    // refresh the filter in case of
                                    // searching dialog
                                    $(this).trigger('change');
                                }
                            }
                        });
                    }
                    }
                    },
		            { name: 'FECHA_FIN_PUBLICACION', index: 'FECHA_FIN_PUBLICACION', width: 100, align: "left", resizable: true, sorttype: 'date', searchoptions: { dataInit: function (elem) {
		                $(elem).datepicker({
		                    changeYear: true,
		                    changeMonth: true,
		                    showButtonPanel: true,
		                    onSelect: function () {
		                        if (this.id.substr(0, 3) === "gs_") {
		                            // in case of searching toolbar
		                            setTimeout(function () {
		                                myGrid[0].triggerToolbar();
		                            }, 50);
		                        } else {
		                            // refresh the filter in case of
		                            // searching dialog
		                            $(this).trigger('change');
		                        }
		                    }
		                });
		            }
		            }
		            },
		            { name: 'ESTADO', index: 'ESTADO', width: 50, align: "center", resizable: true, formatter: 'checkbox', stype: "select", searchoptions: { sopt: ['eq', 'ne'], value: "1:Si;0:No"} },
		            { name: 'Action1', index: 'Action1', align: "left", width: 50, resizable: false, sortable: false, search: false },
                    { name: 'Action2', index: 'Action2', align: "left", width: 50, resizable: false, sortable: false, search: false}],
                pager: jQuery('#pager'),
                rowNum: 500,
                height: wHeight,
                rowList: [500, 1000, 5000],
                sortname: 'NOTICIA_ID',
                sortorder: 'desc',
                autowidth: true,
                viewrecords: true,
                caption: 'Listado de Noticias',
                loadComplete: function () {
                    $(".ui-icon-close").click(function () {
                        var gr = $(this).attr("id");
                        var r = confirm("Desea Eliminar la Noticia ");
                        if (r == true) {
                            $.post("/Noticia/Eliminar", { id: gr }, function (data) {
                                var dat = ' <div  class="ui-state-highlight ui-corner-all" style="margin-top: 10px; padding: 0 .3em; width:30%; height:20px;"><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span><div>';
                                dat += data[0].msg;
                                dat += '</div></div>';
                                $("#msg").empty().html(dat);
                                gridNoticia.trigger('reloadGrid');

                            });
                        }
                        else {
                            x = "Cancelar";
                        }
                        // alert("adas");
                    });

                }
            }).navGrid('#pager',
            {
                edit: false, add: false, del: false, search: true, refresh: true
            },
            {}, //edit options
            {}, //add options
            {}, //del options
            {multipleSearch: true} // search options
            );
        });
    </script>
</asp:Content>
