﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ITELCA_CONTROLESTUDIOS.Models.ViewModels.NoticiaViewModel>" %>
    <div class ="span-24 column last">
        <div class="span-8 column last">
             <div class="editor-label">
					<%: Html.LabelFor(model => model.noticiaCMS.TITULO)%>
			</div>
            <div class="editor-field">
				<%: Html.TextBoxFor(model => model.noticiaCMS.TITULO)%>
				<%: Html.ValidationMessageFor(model => model.noticiaCMS.TITULO)%>
			</div> 
        </div>
        <div class="span-8 column last">
            <div class="editor-label">
			    <%: Html.LabelFor(model => model.noticiaCMS.IMAGEN_RESUMEN)%>
            </div>
            <div class="editor-field">
                <input class="input-background"  id="file2" name="file" type="file" accept="jpg|png|gif" />
				<%: Html.ValidationMessageFor(model => model.noticiaCMS.IMAGEN_RESUMEN)%>
                <% if (!string.IsNullOrEmpty(Model.noticiaCMS.IMAGEN_RESUMEN))
                    {%>
                     <%: Html.HiddenFor(model => model.noticiaCMS.IMAGEN_RESUMEN)%>
                     <br />
                <a href="/Content/Web/Noticias/<%:Model.noticiaCMS.IMAGEN_RESUMEN%>" target="_blank">Imagen actual</a>
                <% }
                   %>
            </div> 
        </div>
    </div>
    <div class ="span-24 column last">
        <div class="span-8 column ">
             <div class="editor-label">
					<%: Html.LabelFor(model => model.noticiaCMS.FECHA_PUBLICACION)%>
			</div>
            <div class="editor-field">
				<%: Html.TextBoxFor(model => model.noticiaCMS.FECHA_PUBLICACION, new { @class = "datepicker" })%>
				<%: Html.ValidationMessageFor(model => model.noticiaCMS.FECHA_PUBLICACION)%>
			</div> 
        </div>
        <div class="span-8 column ">
             <div class="editor-label">
					<%: Html.LabelFor(model => model.noticiaCMS.FECHA_FIN_PUBLICACION)%>
			</div>
            <div class="editor-field">
				<%: Html.TextBoxFor(model => model.noticiaCMS.FECHA_FIN_PUBLICACION, new { @class = "datepicker" })%>
				<%: Html.ValidationMessageFor(model => model.noticiaCMS.FECHA_FIN_PUBLICACION)%>
			</div> 
        </div>
    </div>
    <div class="separator column span-24"></div>
    <div class ="span-24 column last">
        <div class="editor-label">
		    <%: Html.LabelFor(model => model.noticiaCMS.RESUMEN)%>
        </div>
        <div class="editor-field">
            <%: Html.TextAreaFor(model => model.noticiaCMS.RESUMEN)%>
			<%: Html.ValidationMessageFor(model => model.noticiaCMS.RESUMEN)%>
        </div> 
    </div>
    <div class="separator column span-24"></div>
    <div class ="span-24 column last">
        <div class="editor-label">
		    <%: Html.LabelFor(model => model.noticiaCMS.TEXTO)%>
        </div>
        <div class="editor-field">
		    <%: Html.TextAreaFor(model => model.noticiaCMS.TEXTO)%>
			<%: Html.ValidationMessageFor(model => model.noticiaCMS.TEXTO)%>
        </div> 
    </div>

       