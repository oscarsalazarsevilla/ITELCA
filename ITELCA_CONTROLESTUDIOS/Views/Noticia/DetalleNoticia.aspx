﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Web.Master" Inherits="System.Web.Mvc.ViewPage<ITELCA_CONTROLESTUDIOS.Models.ViewModels.NoticiaViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Detalle_Noticia
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="news">
        <div class="Noticias">
            <div style="margin-top: 0%;">
                <%= Model.noticia_detalle.TITULO %>
            </div>
            
        </div>
        <div>
                <%= Model.noticia_detalle.TEXTO %>
        </div>
        <div style="padding-left:20%">
        
            <img src="/Content/Imagenes/<%=Model.noticia_detalle.IMAGEN_RESUMEN %>" height="500px" width="600px" style="margin-top: 40px;
                margin-left: 30px;" border="0" />
        </div>
        
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CssContent" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="JSContent" runat="server">
</asp:Content>
