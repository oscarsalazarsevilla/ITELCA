﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Web.Master" Inherits="System.Web.Mvc.ViewPage<ITELCA_CONTROLESTUDIOS.Models.ViewModels.NoticiaViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Index
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="news">
        <div class="Noticias" style="height:30px;">
            <div style="margin-top: 0%;">
                Noticias</div>
        </div>
        <%
            if (Model != null)
                foreach (var item in Model.noticia)
                { 
        %>
        <div id="detalle-noticia" style="display: block">
            <% if (item.IMAGEN_RESUMEN != null)
               { %>
            <img src="/Content/Imagenes/<%= item.IMAGEN_RESUMEN %>" height="120px" width="180px" style="margin-top: 40px;
                margin-left: 30px;" border="0" />
                <%
               }
               else { 
               %>
               <img src="/Content/Imagenes/No_Photo.png" height="120px" width="180px" style="margin-top: 40px;
                margin-left: 30px;" border="0" />
                   <%
               } %>
            <div id="texto-noticia">
                <h3>
                    <%= item.TITULO %></h3>
                <p>
                    <%= item.RESUMEN %>
                </p>
            </div>
            <%= Html.ActionLink("Leer Mas", "DetalleNoticia/" + item.NOTICIA_ID,"Noticia")%>
            <!--
            <a href="#" style="display: block; margin-right: 0px; margin-top: 1%; margin-left: 90%;">
                Leer Mas</a>-->
        </div>
        <%
                } %>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CssContent" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="JSContent" runat="server">
</asp:Content>
