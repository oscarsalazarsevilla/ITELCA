using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ITELCA_CLASSLIBRARY.Models;

namespace ITELCA_CLASSLIBRARY.Repositories
{
    public class UnitOfWork:IDisposable
    {
        private ItelcaEntities contexto = new ItelcaEntities();
        private RepositoryBase<USUARIOS> repositorioUsuario;
        private RepositoryBase<ROLES> repositorioRoles;
        private RepositoryBase<GENEROS> repositorioGeneros;
        private RepositoryBase<TIPOSCURSOS> repositorioTipoCurso;
        private RepositoryBase<TIPOSAULAS> repositorioTipoAula;
        private RepositoryBase<SEDES> repositorioSede;
        private RepositoryBase<MODULOS> repositorioModulo;
        private RepositoryBase<AULAS> repositorioAula;
        private RepositoryBase<CAJAS> repositorioCaja;
        private RepositoryBase<CURSOS> repositorioCurso;
        private RepositoryBase<TURNOS> repositorioTurno;
        private RepositoryBase<TURNOSVIERNES> repositorioTurnoViernes;
        private RepositoryBase<DIAS> repositorioDia;
        private RepositoryBase<TALLERES> repositorioTaller;
        private RepositoryBase<TESTIMONIOS> repositorioTestimonio;
        private RepositoryBase<TIPOSTALLERES> repositorioTipoTaller;
        private RepositoryBase<TIPOS_FAQS> repositorioTipoFaqs;
        private RepositoryBase<DIAS_TURNOS> repositorioDiaTurno;
        private RepositoryBase<FERIADOS> repositorioFeriado;
        private RepositoryBase<CONCEPTOS> repositorioConcepto;
        private RepositoryBase<CONCEPTOS_CURSOS> repositorioConceptoCurso;
        private RepositoryBase<CONCEPTOS_TALLERES> repositorioConceptoTaller;
        private RepositoryBase<CONCEPTOS_TIPOSSOLICITUDES> repositorioConceptoTipoSolicitud;
        private RepositoryBase<CONCEPTOS_ESTUDIANTES> repositorioConceptosEstudiantes;
        private RepositoryBase<SOLICITUDES> repositorioSolicitudes;
        private RepositoryBase<OFERTASMODULOS> repositorioOfertasModulos;
        private RepositoryBase<OFERTASTALLERES> repositorioOfertasTalleres;
        private RepositoryBase<NOTICIAS> repositorioNoticias;
        private RepositoryBase<FAQS> repositorioFaq;
        private RepositoryBase<GALERIAS> repositorioGalerias;
        public RepositoryBase<CURSOS_MODULOS> repositorioCursoModulo;
        public RepositoryBase<OFERTASCURSOS> repositorioOfertaCurso;
        public RepositoryBase<MATRICULASCURSOS> repositorioMatriculaCurso;
        public RepositoryBase<MATRICULASMODULOS> repositorioMatriculaModulo;
        private RepositoryBase<COSTOS_CURSOS> repositorioCostosCursos;
        private RepositoryBase<DESCUENTOS_CURSOS> repositorioDescuentosCursos;
        private RepositoryBase<ASISTENCIAS> repositorioAsistencia;
		public RepositoryBase<ENTREGAS> repositorioEntrega;
        public RepositoryBase<BANNERS> repositorioBanners;
        public RepositoryBase<CATEGORIAS> repositorioCategorias;
        public RepositoryBase<SECCIONES> repositorioSecciones;
        public RepositoryBase<NOTASMODULO> repositorioNotaModulo;
        public RepositoryBase<RECIBOS> repositorioRecibos;
        public RepositoryBase<DETALLESRECIBOS> repositorioDetallesRecibos;
        public RepositoryBase<MATRICULASTALLERES> repositorioMatriculaTalleres;
        public RepositoryBase<ASISTENCIASTALLER> repositorioAsistenciaTalleres;
        public RepositoryBase<TIPOSCATEGORIAS> repositorioTiposCategorias;
        public RepositoryBase<MODULOS_TURNOS> repositorioModulosTurnos;
        public RepositoryBase<TIPOSSOLICITUDES> repositorioTiposSolicitudes;
        public RepositoryBase<PRODUCTOS> repositorioProductos;
        public RepositoryBase<PRECIOSPRODUCTOS> repositorioPreciosProductos;
        public RepositoryBase<MANEJOCAJAS> repositorioManejoCajas;
        public RepositoryBase<INVENTARIOS> repositorioInventarios;
        public RepositoryBase<BANCOS> repositorioBancos;
        public RepositoryBase<CUENTAS> repositorioCuentas;
        public RepositoryBase<CUENTAS_CONTABLES> repositorioCuentasContables;
        public RepositoryBase<CONFIGURACIONES> repositorioConfiguraciones;
        public RepositoryBase<CORRELATIVOS> repositorioCorrelativos;
        public RepositoryBase<FORMASPAGOS> repositorioFormaPago;
        public RepositoryBase<PAGOS> repositorioPagos;
        public RepositoryBase<CURSOSDURACIONES> repositorioCursosDuraciones;
        public RepositoryBase<TIPOSTURNOS> repositorioTiposTurnos;
        public RepositoryBase<CONTACTO> repositorioContacto;


        public RepositoryBase<CONTACTO> RepositorioContacto
        {
            get
            {
                if (this.repositorioContacto == null)
                {
                    this.repositorioContacto = new RepositoryBase<CONTACTO>(contexto);
                }
                return repositorioContacto;
            }
        }


        public RepositoryBase<USUARIOS> RepositorioUsuario
        {
            get
            {
                if (this.repositorioUsuario == null)
                {
                    this.repositorioUsuario = new RepositoryBase<USUARIOS>(contexto);
                }
                return repositorioUsuario;
            }
        }

        public RepositoryBase<ROLES> RepositorioRoles
        {
            get
            {
                if (this.repositorioRoles == null)
                {
                    this.repositorioRoles = new RepositoryBase<ROLES>(contexto);
                }
                return repositorioRoles;
            }
        }

        public RepositoryBase<GENEROS> RepositorioGeneros
        {
            get
            {
                if (this.repositorioGeneros == null)
                {
                    this.repositorioGeneros = new RepositoryBase<GENEROS>(contexto);
                }
                return repositorioGeneros;
            }
        }

        public RepositoryBase<TIPOSCURSOS> RepositorioTipoCurso
        {
            get
            {
                if (this.repositorioTipoCurso == null)
                {
                    this.repositorioTipoCurso = new RepositoryBase<TIPOSCURSOS>(contexto);
                }
                return repositorioTipoCurso;
            }
        }
        public RepositoryBase<TIPOSAULAS> RepositorioTipoAula
        {
            get
            {
                if (this.repositorioTipoAula == null)
                {
                    this.repositorioTipoAula = new RepositoryBase<TIPOSAULAS>(contexto);
                }
                return repositorioTipoAula;
            }
        }



        public RepositoryBase<TIPOS_FAQS> RepositorioTipoFaqs
        {
            get
            {
                if (this.repositorioTipoFaqs == null)
                {
                    this.repositorioTipoFaqs = new RepositoryBase<TIPOS_FAQS>(contexto);
                }
                return repositorioTipoFaqs;
            }
        }

        public RepositoryBase<SEDES> RepositorioSede
        {
            get
            {
                if (this.repositorioSede == null)
                {
                    this.repositorioSede = new RepositoryBase<SEDES>(contexto);
                }
                return repositorioSede;
            }
        }
        public RepositoryBase<MODULOS> RepositorioModulo
        {
            get
            {
                if (this.repositorioModulo == null)
                {
                    this.repositorioModulo = new RepositoryBase<MODULOS>(contexto);
                }
                return repositorioModulo;
            }
        }

        public RepositoryBase<AULAS> RepositorioAula
        {
            get
            {
                if (this.repositorioAula == null)
                {
                    this.repositorioAula = new RepositoryBase<AULAS>(contexto);
                }
                return repositorioAula;
            }
        }

        public RepositoryBase<CAJAS> RepositorioCaja
        {
            get
            {
                if (this.repositorioCaja == null)
                {
                    this.repositorioCaja = new RepositoryBase<CAJAS>(contexto);
                }
                return repositorioCaja;
            }
        }

        public RepositoryBase<CURSOS> RepositorioCurso { 
            get
            {
                if(this.repositorioCurso==null)
                {
                    this.repositorioCurso=new RepositoryBase<CURSOS>(contexto);
                }
                return repositorioCurso;
            }
        }

        public RepositoryBase<TURNOS> RepositorioTurno {
            get
            {
                if (this.repositorioTurno == null) {
                    this.repositorioTurno = new RepositoryBase<TURNOS>(contexto);
                }
                return repositorioTurno;
            }
        }

        public RepositoryBase<TURNOSVIERNES> RepositorioTurnoViernes
        {
            get
            {
                if (this.repositorioTurnoViernes == null)
                {
                    this.repositorioTurnoViernes = new RepositoryBase<TURNOSVIERNES>(contexto);
                }
                return repositorioTurnoViernes;
            }
        }
        public RepositoryBase<DIAS> RepositorioDia {
            get {
                if (this.repositorioDia == null) {
                    this.repositorioDia = new RepositoryBase<DIAS>(contexto);
                }
                return repositorioDia;
            }
        }

        public RepositoryBase<DIAS_TURNOS> RepositorioDiaTurno {
            get {
                if (repositorioDiaTurno == null) {
                    this.repositorioDiaTurno = new RepositoryBase<DIAS_TURNOS>(contexto);
                }
                return repositorioDiaTurno;
            }
        }

        public RepositoryBase<TALLERES> RepositorioTaller {
            get {
                if (repositorioTaller == null) {
                    this.repositorioTaller = new RepositoryBase<TALLERES>(contexto);
                }
                return repositorioTaller;
            }
        }

        public RepositoryBase<TESTIMONIOS> RepositorioTestimonio
        {
            get
            {
                if (repositorioTestimonio == null)
                {
                    this.repositorioTestimonio = new RepositoryBase<TESTIMONIOS>(contexto);
                }
                return repositorioTestimonio;
            }
        }

        public RepositoryBase<FAQS> RepositorioFaq
        {
            get
            {
                if (repositorioFaq == null)
                {
                    this.repositorioFaq = new RepositoryBase<FAQS>(contexto);
                }
                return repositorioFaq;
            }
        }
        public RepositoryBase<TIPOSTALLERES> RepositorioTipoTaller {
            get {
                if (repositorioTipoTaller == null) {
                    this.repositorioTipoTaller = new RepositoryBase<TIPOSTALLERES>(contexto);
                }
                return repositorioTipoTaller;
            }
        }
        

        public RepositoryBase<CONCEPTOS> RepositorioConcepto
        {
            get
            {
                if (this.repositorioConcepto== null)
                {
                    this.repositorioConcepto = new RepositoryBase<CONCEPTOS>(contexto);
                }
                return repositorioConcepto;
            }
        }

        public RepositoryBase<CONCEPTOS_CURSOS> RepositorioConceptoCurso
        {
            get
            {
                if (this.repositorioConceptoCurso == null)
                {
                    this.repositorioConceptoCurso = new RepositoryBase<CONCEPTOS_CURSOS>(contexto);
                }
                return repositorioConceptoCurso;
            }
        }

        public RepositoryBase<CONCEPTOS_TALLERES> RepositorioConceptoTaller
        {
            get
            {
                if (this.repositorioConceptoTaller == null)
                {
                    this.repositorioConceptoTaller = new RepositoryBase<CONCEPTOS_TALLERES>(contexto);
                }
                return repositorioConceptoTaller;
            }
        }

        public RepositoryBase<CONCEPTOS_TIPOSSOLICITUDES> RepositorioConceptoTipoSolicitud
        {
            get
            {
                if (this.repositorioConceptoTipoSolicitud == null)
                {
                    this.repositorioConceptoTipoSolicitud = new RepositoryBase<CONCEPTOS_TIPOSSOLICITUDES>(contexto);
                }
                return repositorioConceptoTipoSolicitud;
            }
        }

        public RepositoryBase<CONCEPTOS_ESTUDIANTES> RepositorioConceptosEstudiantes
        {
            get
            {
                if (this.repositorioConceptosEstudiantes == null)
                {
                    this.repositorioConceptosEstudiantes = new RepositoryBase<CONCEPTOS_ESTUDIANTES>(contexto);
                }
                return repositorioConceptosEstudiantes;
            }
        }
        
        public RepositoryBase<FERIADOS> RepositorioFeriado
        {
            get
            {
                if (this.repositorioFeriado == null)
                {
                    this.repositorioFeriado = new RepositoryBase<FERIADOS>(contexto);
                }
                return repositorioFeriado;
            }
        }

        public RepositoryBase<SOLICITUDES> RepositorioSolicitudes
        {
            get
            {
                if (this.repositorioSolicitudes == null)
                {
                    this.repositorioSolicitudes = new RepositoryBase<SOLICITUDES>(contexto);
                }
                return repositorioSolicitudes;
            }
        }

        public RepositoryBase<OFERTASMODULOS> RepositorioOfertasModulos
        {
            get
            {
                if (this.repositorioOfertasModulos == null)
                {
                    this.repositorioOfertasModulos = new RepositoryBase<OFERTASMODULOS>(contexto);
                }
                return repositorioOfertasModulos;
            }
        }

        public RepositoryBase<OFERTASTALLERES> RepositorioOfertasTalleres
        {
            get
            {
                if (this.repositorioOfertasTalleres == null)
                {
                    this.repositorioOfertasTalleres = new RepositoryBase<OFERTASTALLERES>(contexto);
                }
                return repositorioOfertasTalleres;
            }
        }

        public RepositoryBase<NOTICIAS> RepositorioNoticias {
            get {
                if (this.repositorioNoticias == null) {
                    this.repositorioNoticias = new RepositoryBase<NOTICIAS>(contexto);
                }
                return repositorioNoticias;
            }
        }

        public RepositoryBase<CURSOS_MODULOS> RepositorioCursoModulo {
            get
            {
                if (this.repositorioCursoModulo == null)
                {
                    this.repositorioCursoModulo = new RepositoryBase<CURSOS_MODULOS>(contexto);
                }
                return repositorioCursoModulo;
            }
        
        }

        public RepositoryBase<OFERTASCURSOS> RepositorioOfertaCurso {
            get {
                if (this.repositorioOfertaCurso == null) {
                    this.repositorioOfertaCurso = new RepositoryBase<OFERTASCURSOS>(contexto);
                }
                return repositorioOfertaCurso;
            }
           
        }

        public RepositoryBase<MATRICULASCURSOS> RepositorioMatriculaCurso
        {
            get
            {
                if (this.repositorioMatriculaCurso == null)
                {
                    this.repositorioMatriculaCurso = new RepositoryBase<MATRICULASCURSOS>(contexto);
                }
                return repositorioMatriculaCurso;
            }

        }

        public RepositoryBase<MATRICULASMODULOS> RepositorioMatriculaModulo
        {
            get
            {
                if (this.repositorioMatriculaModulo == null)
                {
                    this.repositorioMatriculaModulo = new RepositoryBase<MATRICULASMODULOS>(contexto);
                }
                return repositorioMatriculaModulo;
            }

        }

        public RepositoryBase<COSTOS_CURSOS> RepositorioCostosCursos
        {
            get
            {
                if (this.repositorioCostosCursos == null)
                {
                    this.repositorioCostosCursos = new RepositoryBase<COSTOS_CURSOS>(contexto);
                }
                return repositorioCostosCursos;
            }
        }

        public RepositoryBase<DESCUENTOS_CURSOS> RepositorioDescuentosCursos
        {
            get
            {
                if (this.repositorioDescuentosCursos == null)
                {
                    this.repositorioDescuentosCursos = new RepositoryBase<DESCUENTOS_CURSOS>(contexto);
                }
                return repositorioDescuentosCursos;
            }
        }

        public RepositoryBase<ASISTENCIAS> RepositorioAsistencias {
            get {
                if (this.repositorioAsistencia == null) {
                    this.repositorioAsistencia = new RepositoryBase<ASISTENCIAS>(contexto);
                }
                return repositorioAsistencia;
            }
        }
		
		public RepositoryBase<ENTREGAS> RepositorioEntrega
        {
            get
            {
                if (this.repositorioEntrega == null)
                {
                    this.repositorioEntrega = new RepositoryBase<ENTREGAS>(contexto);
                }
                return repositorioEntrega;
            }
        }

        public RepositoryBase<BANNERS> RepositorioBanners
        {
            get
            {
                if (this.repositorioBanners == null)
                {
                    this.repositorioBanners = new RepositoryBase<BANNERS>(contexto);
                }
                return repositorioBanners;
            }
        }

        public RepositoryBase<CATEGORIAS> RepositorioCategorias
        {
            get
            {
                if (this.repositorioCategorias == null)
                {
                    this.repositorioCategorias = new RepositoryBase<CATEGORIAS>(contexto);
                }
                return repositorioCategorias;
            }
        }

        public RepositoryBase<GALERIAS> RepositorioGalerias
        {
            get
            {
                if (this.repositorioGalerias == null)
                {
                    this.repositorioGalerias = new RepositoryBase<GALERIAS>(contexto);
                }
                return repositorioGalerias;
            }
        }

        public RepositoryBase<SECCIONES> RepositorioSecciones
        {
            get
            {
                if (this.repositorioSecciones == null)
                {
                    this.repositorioSecciones = new RepositoryBase<SECCIONES>(contexto);
                }
                return repositorioSecciones;
            }
        }
        public RepositoryBase<NOTASMODULO> RepositorioNotaModulo {
            get {
                if (this.repositorioNotaModulo == null) {
                    this.repositorioNotaModulo = new RepositoryBase<NOTASMODULO>(contexto);
                }
                return repositorioNotaModulo;
            }
        }

        public RepositoryBase<RECIBOS> RepositorioRecibos
        {
            get
            {
                if (this.repositorioRecibos == null)
                {
                    this.repositorioRecibos = new RepositoryBase<RECIBOS>(contexto);
                }
                return repositorioRecibos;
            }
        }

        public RepositoryBase<DETALLESRECIBOS> RepositorioDetallesRecibos
        {
            get
            {
                if (this.repositorioDetallesRecibos == null)
                {
                    this.repositorioDetallesRecibos = new RepositoryBase<DETALLESRECIBOS>(contexto);
                }
                return repositorioDetallesRecibos;
            }
        }

        public RepositoryBase<MATRICULASTALLERES> RepositorioMatriculaTalleres { 
            get{
                if(this.repositorioMatriculaTalleres==null){
                    this.repositorioMatriculaTalleres=new RepositoryBase<MATRICULASTALLERES>(contexto);
                }
                return repositorioMatriculaTalleres;
            }
        }

        public RepositoryBase<ASISTENCIASTALLER> RepositorioAsistenciaTalleres {
            get {
                if (this.repositorioAsistenciaTalleres == null) {
                    this.repositorioAsistenciaTalleres = new RepositoryBase<ASISTENCIASTALLER>(contexto);
                }
                return repositorioAsistenciaTalleres;
            }
        }

        public RepositoryBase<TIPOSCATEGORIAS> RepositorioTiposCategorias
        {
            get
            {
                if (this.repositorioTiposCategorias == null)
                {
                    this.repositorioTiposCategorias = new RepositoryBase<TIPOSCATEGORIAS>(contexto);
                }
                return repositorioTiposCategorias;
            }
        }

        public RepositoryBase<MODULOS_TURNOS> RepositorioModulosTurnos
        {
            get
            {
                if (this.repositorioModulosTurnos == null)
                {
                    this.repositorioModulosTurnos = new RepositoryBase<MODULOS_TURNOS>(contexto);
                }
                return repositorioModulosTurnos;
            }
        }

        public RepositoryBase<TIPOSSOLICITUDES> RepositorioTiposSolicitudes
        {
            get
            {
                if (this.repositorioTiposSolicitudes == null)
                {
                    this.repositorioTiposSolicitudes = new RepositoryBase<TIPOSSOLICITUDES>(contexto);
                }
                return repositorioTiposSolicitudes;
            }
        }

        public RepositoryBase<PRODUCTOS> RepositorioProductos
        {
            get
            {
                if (this.repositorioProductos == null)
                {
                    this.repositorioProductos = new RepositoryBase<PRODUCTOS>(contexto);
                }
                return repositorioProductos;
            }
        }

        public RepositoryBase<PRECIOSPRODUCTOS> RepositorioPreciosProductos
        {
            get
            {
                if (this.repositorioPreciosProductos == null)
                {
                    this.repositorioPreciosProductos = new RepositoryBase<PRECIOSPRODUCTOS>(contexto);
                }
                return repositorioPreciosProductos;
            }
        }

        public RepositoryBase<MANEJOCAJAS> RepositorioManejoCajas
        {
            get
            {
                if (this.repositorioManejoCajas == null)
                {
                    this.repositorioManejoCajas = new RepositoryBase<MANEJOCAJAS>(contexto);
                }
                return repositorioManejoCajas;
            }
        }

        public RepositoryBase<INVENTARIOS> RepositorioInventarios
        {
            get
            {
                if (this.repositorioInventarios == null)
                {
                    this.repositorioInventarios = new RepositoryBase<INVENTARIOS>(contexto);
                }
                return repositorioInventarios;
            }
        }

        public RepositoryBase<BANCOS> RepositorioBancos
        {
            get
            {
                if (this.repositorioBancos == null)
                {
                    this.repositorioBancos = new RepositoryBase<BANCOS>(contexto);
                }
                return repositorioBancos;
            }
        }

        public RepositoryBase<CUENTAS> RepositorioCuentas
        {
            get
            {
                if (this.repositorioCuentas == null)
                {
                    this.repositorioCuentas = new RepositoryBase<CUENTAS>(contexto);
                }
                return repositorioCuentas;
            }
        }

        public RepositoryBase<CUENTAS_CONTABLES> RepositorioCuentasContables
        {
            get
            {
                if (this.repositorioCuentasContables == null)
                {
                    this.repositorioCuentasContables = new RepositoryBase<CUENTAS_CONTABLES>(contexto);
                }
                return repositorioCuentasContables;
            }
        }

        public RepositoryBase<CONFIGURACIONES> RepositorioConfiguraciones
        {
            get
            {
                if (this.repositorioConfiguraciones == null)
                {
                    this.repositorioConfiguraciones = new RepositoryBase<CONFIGURACIONES>(contexto);
                }
                return repositorioConfiguraciones;
            }
        }

        public RepositoryBase<CORRELATIVOS> RepositorioCorrelativos
        {
            get
            {
                if (this.repositorioCorrelativos == null)
                {
                    this.repositorioCorrelativos = new RepositoryBase<CORRELATIVOS>(contexto);
                }
                return repositorioCorrelativos;
            }
        }
        public RepositoryBase<FORMASPAGOS> RepositorioFormaPago
        {
            get
            {
                if (this.repositorioFormaPago == null)
                {
                    this.repositorioFormaPago = new RepositoryBase<FORMASPAGOS>(contexto);
                }
                return repositorioFormaPago;
            }
        }

        public RepositoryBase<PAGOS> RepositorioPagos
        {
            get
            {
                if (this.repositorioPagos == null)
                {
                    this.repositorioPagos = new RepositoryBase<PAGOS>(contexto);
                }
                return repositorioPagos;
            }
        }
        public RepositoryBase<CURSOSDURACIONES> RepositorioCursosDuraciones
        {
            get
            {
                if (this.repositorioCursosDuraciones == null)
                {
                    this.repositorioCursosDuraciones = new RepositoryBase<CURSOSDURACIONES>(contexto);
                }
                return repositorioCursosDuraciones;
            }
        }

        public RepositoryBase<TIPOSTURNOS> RepositorioTiposTurnos
        {
            get
            {
                if (this.repositorioTiposTurnos == null)
                {
                    this.repositorioTiposTurnos = new RepositoryBase<TIPOSTURNOS>(contexto);
                }
                return repositorioTiposTurnos;
            }
        }

        public void Save()
        {
            try
            {
                contexto.SaveChanges();
            }
            catch (InvalidCastException e)
            {
                throw (e);    // Rethrowing exception e
            }
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    contexto.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
