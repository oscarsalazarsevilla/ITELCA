﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using ITELCA_CLASSLIBRARY.Models;

namespace ITELCA_CLASSLIBRARY.Repositories
{
    public class RepositoryBase<T> where T : class
    {
        internal ItelcaEntities _dataContext;
        internal DbSet<T> _dbset;
        public RepositoryBase(ItelcaEntities context)
        {
            this._dataContext = context;
            this._dbset = context.Set<T>();           
        }
        public virtual List<T> Get(
            Expression<Func<T, bool>> filter = null,
            Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
            string includeProperties = "")
        {
            IQueryable<T> query = _dbset;

            if (filter != null)
            {
                query = query.Where(filter);
            }

            foreach (var includeProperty in includeProperties.Split
                (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }

            if (orderBy != null)
            {
                return orderBy(query).ToList();
            }
            else
            {
                return query.ToList();
            }
        }


        public virtual void Add(T entity)
        {
            _dbset.Add(entity);
        }
        
        public virtual void Delete(T entity)
        {
            _dbset.Remove(entity);
        }
        
        public virtual void Delete(Expression<Func<T, bool>> where)
        {
            IEnumerable<T> objects = _dbset.Where<T>(where).AsEnumerable();
            foreach (T obj in objects)
                _dbset.Remove(obj);
        }
        
        public virtual T GetById(long id)
        {
            return _dbset.Find(id);
        }
        
        public virtual T GetById(string id)
        {
            return _dbset.Find(id);
        }
        
        public virtual IQueryable<T> GetAll()
        {
            return _dbset.AsQueryable();
        }
        
        public virtual IQueryable<T> GetMany(Expression<Func<T, bool>> where)
        {
            return _dbset.AsQueryable().Where(where);
        }
        
        public T Get(Expression<Func<T, bool>> where)
        {
            return _dbset.Where(where).FirstOrDefault<T>();
        }

        public virtual void Modify(T entity)
        {
            _dataContext.Entry(entity).State = EntityState.Modified;
            //_dataContext.SaveChanges();
            //_dbset.ChangeObjectState(entity, System.Data.EntityState.Modified);
        }
        
        public virtual void Attach(T entity)
        {
            _dbset.Attach(entity);
        }
    }

}
