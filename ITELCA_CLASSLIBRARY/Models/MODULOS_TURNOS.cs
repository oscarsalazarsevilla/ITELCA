//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ITELCA_CLASSLIBRARY.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class MODULOS_TURNOS
    {
        public decimal MODULO_ID { get; set; }
        public decimal TURNO_ID { get; set; }
        public decimal NRO_CLASES { get; set; }
        public string CONDICION { get; set; }
        public decimal ESTADO { get; set; }
        public System.DateTime FECHA_CREACION { get; set; }
        public decimal USUARIO_ID { get; set; }
    
        public virtual MODULOS MODULOS { get; set; }
        public virtual TURNOS TURNOS { get; set; }
        public virtual USUARIOS USUARIOS { get; set; }
    }
}
