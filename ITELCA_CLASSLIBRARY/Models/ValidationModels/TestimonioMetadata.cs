﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace ITELCA_CLASSLIBRARY.Models
{
    [MetadataType(typeof(TESTIMONIOSMetadata))]
    public partial class TESTIMONIOS
    {
        public class TESTIMONIOSMetadata
        {
            [Required(ErrorMessage = "Texto requerido.")]
            [DisplayName("Texto")]
            public string TEXTO { get; set; }

            [Required(ErrorMessage = "Nombre requerido.")]
            [StringLength(100, ErrorMessage = "El nombre no puede ser mayor a 100 caracteres.")]
            [DisplayName("Nombre")]
            public string NOMBRE { get; set; }
        }
    }
}