﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace ITELCA_CLASSLIBRARY.Models
{
    [MetadataType(typeof(TIPOSAULASMetadata))]
    public partial class TIPOSAULAS
    {

      
        public class TIPOSAULASMetadata
        {
            [Required(ErrorMessage="Nombre de aula requerido.")]
            [StringLength(40, ErrorMessage = "El nombre de Aula no puede ser mayor a 40 caracteres.")]
            [DisplayName("Nombre")]
            public string NOMBRE { get; set; }
           
        }
    }
}
