﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace ITELCA_CLASSLIBRARY.Models
{
    [MetadataType(typeof(MANEJOCAJASMetadata))]
    public partial class MANEJOCAJAS
    {
        public decimal SEDE_ID { get; set; }
        public class MANEJOCAJASMetadata
        {
            [Required(ErrorMessage = "Sede requerida")]
            [DisplayName("Sede")]
            public decimal SEDE_ID { get; set; }

            [Required(ErrorMessage = "Caja requerida")]
            [DisplayName("Caja")]
            public decimal CAJA_ID { get; set; }

            [DisplayName("Observaciones")]
            public string OBSERVACIONES { get; set; }
        }
    }
}
