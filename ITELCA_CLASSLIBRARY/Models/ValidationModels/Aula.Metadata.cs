﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace ITELCA_CLASSLIBRARY.Models
{
    [MetadataType(typeof(AULASMetadata))]
    public partial class AULAS
    {
    
        public class AULASMetadata
        {
            [Required(ErrorMessage = "Sede requerida")]
            [DisplayName("Sede")]
            public int SEDE_ID   { get; set; }

            [Required(ErrorMessage = "Tipo Aula requerido")]
            [DisplayName("Tipo Aula")]
            public int TIPO_AULA_ID { get; set; }

            [Required(ErrorMessage="Nombre de sede requerido")]
            [StringLength(40, ErrorMessage = "El nombre de la sede no puede ser mayor a 15 caracteres")]
            [DisplayName("Nombre")]
            public string NOMBRE { get; set; }

            [Required(ErrorMessage = "Codigo requerido")]
            [StringLength(40, ErrorMessage = "El codigo del aula no puede ser mayor a 15 caracteres")]
            [DisplayName("Codigo")]
            public string CODIGO { get; set; }

            [Required(ErrorMessage = "Piso de aula requerido")]
            [Range(0, 100, ErrorMessage = "debe ser un numero entre 0 -100")]
            [DisplayName("Piso")]
            public int PISO { get; set; }

            [Required(ErrorMessage = "Capacidad de aula requerida")]
            [Range(1, 1000, ErrorMessage = "debe ser un numero entre 1 -1000")]
            [DisplayName("Capacidad")]
            public int CAPACIDAD { get; set; }

            [DisplayName("esta Activo ?")]
            public int ESTADO { get; set; }
        }
    }
}
