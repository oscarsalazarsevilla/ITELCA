﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace ITELCA_CLASSLIBRARY.Models
{
    [MetadataType(typeof(CATEGORIASMetadata))]
    public partial class CATEGORIAS
    {
        public class CATEGORIASMetadata
        {
            [Required(ErrorMessage="Nombre requerido")]
            [StringLength(100, ErrorMessage = "El nombre no puede ser mayor a 100 caracteres")]
            [DisplayName("Nombre")]
            public string NOMBRE { get; set; }

            [Required(ErrorMessage = "Texto requerido")]
            [StringLength(1000, ErrorMessage = "El texto no puede ser mayor a 1000 caracteres")]
            [DisplayName("Texto")]
            public string TEXTO { get; set; }

            [DisplayName("Categoría padre")]
            public int? PADRE_ID { get; set; }

            [Required(ErrorMessage = "Texto requerido")]
            [DisplayName("Tipo")]
            public int TIPOCATEGORIA_ID { get; set; }

            [DisplayName("Orden")]
            public string ORDEN { get; set; }

            [DisplayName("Alias")]
            [StringLength(50, ErrorMessage = "El alias no puede ser mayor a 50 caracteres")]
            public string ALIAS { get; set; }

            [StringLength(2, ErrorMessage = "El Campo Condición no puede ser mayor a 2 caracteres")]
            [DisplayName("Condición")]
            public string CONDICION { get; set; }

            [DisplayName("Esta Activo?")]
            public int ESTADO { get; set; }

            [DisplayName("Fecha Publicación")]
            public DateTime FECHA_PUBLICACION { get; set; }

            [DisplayName("Fecha Fin Publicación")]
            public DateTime? FECHA_FIN_PUBLICACION { get; set; }
        }
    }
}
