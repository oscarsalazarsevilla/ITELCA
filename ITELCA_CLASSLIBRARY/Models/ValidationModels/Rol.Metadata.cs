﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace ITELCA_CLASSLIBRARY.Models
{
    [MetadataType(typeof(ROLESMetadata))]
    public partial class ROLES
    {
        public class ROLESMetadata
        {
            [Required(ErrorMessage="Nombre de Rol Requerido")]
            [StringLength(40, ErrorMessage = "El nombre de rol no puede ser mayor a 40 caracteres")]
            [DisplayName("Nombre")]
            public string NOMBRE { get; set; }
            
            [DisplayName("Activo?")]
            public int ESTADO { get; set; }

           
        }
    }
}
