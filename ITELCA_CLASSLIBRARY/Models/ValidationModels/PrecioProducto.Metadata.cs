﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace ITELCA_CLASSLIBRARY.Models
{
    [MetadataType(typeof(PRECIOSPRODUCTOSMetadata))]
    public partial class PRECIOSPRODUCTOS
    {
        public class PRECIOSPRODUCTOSMetadata
        {
            [Required(ErrorMessage = "SETID requerido")]
            [DisplayName("SETID")]
            public string SETID   { get; set; }

            [Required(ErrorMessage = "Producto requerido")]
            [DisplayName("Producto")]
            public string INV_ITEM_ID { get; set; }

            [Required(ErrorMessage="Fecha efectiva requerida")]
            [DisplayName("Fecha efectiva")]
            public DateTime FECHA_EFECTIVA { get; set; }

            [Required(ErrorMessage = "Monto requerido")]
            [DisplayName("Monto")]
            public decimal MONTO { get; set; }
        }
    }
}
