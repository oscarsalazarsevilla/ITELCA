﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace ITELCA_CLASSLIBRARY.Models
{
    [MetadataType(typeof(FAQSMetadata))]
    public partial class FAQS
    {
        public class FAQSMetadata
        {
            [Required(ErrorMessage = "Pregunta requerida")]
            [DisplayName("Pregunta")]
            public string PREGUNTA { get; set; }

            [Required(ErrorMessage = "Respuesta requerida")]
            [DisplayName("Respuesta")]
            public string RESPUESTA { get; set; }

            [Required(ErrorMessage = "Categoria requerida")]
            [DisplayName("Categoría")]
            public string TIPO_FAQS_ID { get; set; }

            [DisplayName("Orden")]
            [Range(1, 9999, ErrorMessage = "Orden Debe ser un número entre 1 - 9999")]
            public decimal ORDEN { get; set; }
        }
    }
}