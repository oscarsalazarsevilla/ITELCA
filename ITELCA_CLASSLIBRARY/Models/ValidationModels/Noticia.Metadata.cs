﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace ITELCA_CLASSLIBRARY.Models
{
    [MetadataType(typeof(NOTICIASMetadata))]
    public partial class NOTICIAS
    {

        public class NOTICIASMetadata
        {
            [Required(ErrorMessage = "Titulo requerido")]
            [StringLength(200, ErrorMessage = "Titulo no puede ser mayor a 200 caracteres")]
            [DisplayName("Título")]
            public string TITULO   { get; set; }

            [Required(ErrorMessage = "Resumen requerido")]
            [StringLength(500, ErrorMessage = "Resumen  no puede ser mayor a 500 caracteres")]
            [DisplayName("Resumen")]
            public string RESUMEN { get; set; }

            [Required(ErrorMessage = "Texto requerido")]
            [StringLength(8000, ErrorMessage = "Texto  no puede ser mayor a 8000 caracteres")]
            [DisplayName("Texto")]
            public string TEXTO { get; set; }

            [DisplayName("Imagen Resumen")]
            [Required(ErrorMessage = "Imagen Resumen Requerida")]
            public string IMAGEN_RESUMEN { get; set; }

            [DisplayName("Fecha Publicación")]
            public DateTime FECHA_PUBLICACION { get; set; }

            [DisplayName("Fecha Fin Publicación")]
            public DateTime? FECHA_FIN_PUBLICACION { get; set; }
        }
    }
}
