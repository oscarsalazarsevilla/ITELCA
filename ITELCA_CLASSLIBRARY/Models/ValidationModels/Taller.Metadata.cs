﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace ITELCA_CLASSLIBRARY.Models
{
    [MetadataType(typeof(TALLERESMetadata))]
    public partial class TALLERES
    {

      
        public class TALLERESMetadata
        {
            [Required(ErrorMessage="Nombre requerido.")]
            [StringLength(400, ErrorMessage = "El nombre de Taller no puede ser mayor a 400 caracteres")]
            [DisplayName("Nombre")]
            public string NOMBRE { get; set; }

            [Required(ErrorMessage = "Descripción requerida.")]
            [StringLength(1000, ErrorMessage = "Descripción no puede ser mayor a 1000 caracteres")]
            [DisplayName("Descripción")]
            public string DESCRIPCION { get; set; }

            [Required(ErrorMessage = "Tipo Taller requerido")]
            [DisplayName("Tipo Taller")]
            public decimal TIPO_TALLER_ID { get; set; }

        }
    }
}
