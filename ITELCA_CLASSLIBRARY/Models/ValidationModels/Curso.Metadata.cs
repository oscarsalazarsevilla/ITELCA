﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace ITELCA_CLASSLIBRARY.Models
{
    [MetadataType(typeof(CURSOSMetadata))]
    public partial class CURSOS
    {
        public class CURSOSMetadata
        {
            [Required(ErrorMessage="Tipo de Curso requerido")]
            [DisplayName("Tipo Curso")]
            public int TIPO_CURSO_ID { get; set; }

            [Required(ErrorMessage = "Duración del curso en horas requerida")]
            [DisplayName("Duración en horas")]
            public decimal DURACION_HORAS { get; set; }

           /* [Required(ErrorMessage = "Duración del curso en meses requerida")]
            [DisplayName("Duración en meses")]
            public decimal DURACION_MESES { get; set; }*/

            [Required(ErrorMessage = "Duración del Curso requerida")]
            [DisplayName("Tipo Duración")]
            public string DURACION { get; set; }

            [StringLength(40, ErrorMessage = "El codigo  no puede ser mayor a 40 caracteres")]
            [Required(ErrorMessage = "Código es requerido")]
            [DisplayName("Código")]
            public string CODIGO { get; set; }

            [Required(ErrorMessage = "Nombre es requerido")]
            [StringLength(400, ErrorMessage = "El Nombre  no puede ser mayor a 400 caracteres")]
            [DisplayName("Nombre")]
            public string NOMBRE { get; set; }

            [StringLength(1000, ErrorMessage = "La descripción  no puede ser mayor a 1000 caracteres")]
            [DisplayName("Descripción")]
            public string DESCRIPCION { get; set; }

            [Range(0,10, ErrorMessage = "El número de talleres debe ser entre 0-10")]
            [Required(ErrorMessage = "Números de Talleres es requerido")]
            [DisplayName("Núm. Talleres")]
            public string NRO_TALLERES { get; set; }

            [DisplayName("Esta Activo ?")]
            public decimal ESTADO { get; set; }
        }
    }
}
