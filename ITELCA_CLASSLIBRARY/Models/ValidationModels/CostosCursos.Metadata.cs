﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace ITELCA_CLASSLIBRARY.Models
{
    [MetadataType(typeof(COSTOS_CURSOSMetadata))]
    public partial class COSTOS_CURSOS
    {
        public class COSTOS_CURSOSMetadata
        {
            [Required(ErrorMessage="Curso requerido")]
            [DisplayName("Curso")]
            public decimal CURSO_ID { get; set; }

            [Required(ErrorMessage = "Turno requerido")]
            [DisplayName("Turno")]
            public decimal TURNO_ID { get; set; }

            //[Required(ErrorMessage = "Monto requerido")]
            //[DisplayName("Monto")]
            //public decimal MONTO { get; set; }

            [Required(ErrorMessage = "Fecha requerida")]
            [DisplayName("Fecha Efectiva")]
            public DateTime FECHA_EFECTIVA { get; set; }
        }
    }
}
