﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace ITELCA_CLASSLIBRARY.Models
{
    [MetadataType(typeof(DIASMetadata))]
    public partial class DIAS
    {
        public class DIASMetadata
        {
            [Required(ErrorMessage = "Nombre requerido")]
            [DisplayName("Nombre")]
            public string NOMBRE { get; set; }

            [Required(ErrorMessage = "Nombre Corto requerido")]
            [DisplayName("Nombre Corto")]
            public string NOMBRECORTO { get; set; }
        }
    }
}
