﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace ITELCA_CLASSLIBRARY.Models
{
    [MetadataType(typeof(OFERTASTALLERESMetadata))]
    public partial class OFERTASTALLERES
    {
        public class OFERTASTALLERESMetadata
        {


            
            [Required(ErrorMessage = " Hora requerida")]
            [DisplayName("Hora")]
            public string HORA { get; set; }

            [Required(ErrorMessage = "Fecha Inicio requerida")]
            [DisplayName("Fecha de Inicio")]
            public DateTime FECHA_INICIO { get; set; }

            [Required(ErrorMessage = "Fecha Fin requerida")]
            [DisplayName("Fecha de Fin")]
            public DateTime FECHA_FIN { get; set; }

            [DisplayName("Aula")]
            public decimal AULA_ID { get; set; }

            [Required(ErrorMessage = "Taller requerido")]
            [DisplayName("Taller")]
            public decimal TALLER_ID { get; set; }

            [DisplayName("Profesor")]
            public decimal PROFESOR_ID { get; set; }

            [Required(ErrorMessage = "Monto requerido")]
            [DisplayName("Monto")]
            public string MONTO { get; set; }

            [Required(ErrorMessage = "Capacidad requerida")]
            [DisplayName("Capacidad")]
            [Range(1, 1000, ErrorMessage = "debe ser un numero entre 1 -1000")]
            public decimal CAPACIDAD { get; set; }

        }
    }
}
