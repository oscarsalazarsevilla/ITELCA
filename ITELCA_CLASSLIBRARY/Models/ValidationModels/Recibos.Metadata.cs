﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace ITELCA_CLASSLIBRARY.Models
{
    [MetadataType(typeof(RECIBOSMetadata))]
    public partial class RECIBOS
    {
    
        public class RECIBOSMetadata
        {

            [DisplayName("Nro. Recibo")]
            public string NRORECIBO { get; set; }

            [Required(ErrorMessage="Nombre de cliente requerido")]
            [DisplayName("Nombre Cliente")]
            public string NOMBRECLIENTE { get; set; }

            [Required(ErrorMessage = "Tipo de Documento requerido")]
            [DisplayName("Tipo de Documento")]
            public string TIPODOCUMENTO { get; set; }

            [Required(ErrorMessage = "Nro. Documento requerido")]
            [DisplayName("Nro. Documento")]
            public string DOCUMENTO { get; set; }

            [DisplayName("Dirección Fiscal")]
            public string DIRECCIONFISCAL { get; set; }

            [DisplayName("Teléfono")]
            public string TELEFONO { get; set; }

            [Required(ErrorMessage = "Condición de Pago requerido")]
            [DisplayName("Condición de Pago")]
            public string CONDICIONPAGO { get; set; }

            [DisplayName("Abono")]
            public string ABONO { get; set; }

            [DisplayName("Monto Total")]
            public string MONTOTOTAL { get; set; }

            [DisplayName("Exonerado")]
            public string EXONERADO { get; set; }
          }
    }
}
