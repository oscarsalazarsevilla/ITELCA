﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace ITELCA_CLASSLIBRARY.Models
{
    [MetadataType(typeof(MODULOSMetadata))]
    public partial class MODULOS
    {
        
        public class MODULOSMetadata
        {
            [Required(ErrorMessage = "Nombre de Módulo requerido")]
            [StringLength(400, ErrorMessage = "El nombre del modulo no puede ser mayor a 400 caracteres")]
            [DisplayName("Nombre")]
            public string NOMBRE { get; set; }

            [StringLength(1000, ErrorMessage = "La descripción del Módulo no puede ser mayor a 1000 caracteres")]
            [DisplayName("Descripción")]
            public string DESCRIPCION { get; set; }

            [Required(ErrorMessage = "Mínimo Aprobatorio requerido")]
            [Range(1, 100, ErrorMessage = "Debe ser un número entre 1 -100")]
            [DisplayName("Mínimo Aprobatorio")]
            public int MINIMO_APROBATORIO { get; set; }

            /*[Required(ErrorMessage = "Número de Clases requerido")]
            [Range(1, 100, ErrorMessage = "Debe ser un número entre 1 -100")]
            [DisplayName("Número de Clases")]
            public int NUMERO_CLASES { get; set; }*/
            
            [DisplayName("Esta Activo ?")]
            public int ESTADO { get; set; }

            [DisplayName("Orden")]
            [Range(1, 100, ErrorMessage = "Debe ser un número entre 1 -30")]
            public decimal ORDEN { get; set; }
        }
    }
}
