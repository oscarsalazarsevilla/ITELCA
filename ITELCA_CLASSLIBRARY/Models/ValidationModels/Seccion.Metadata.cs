﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace ITELCA_CLASSLIBRARY.Models
{
    [MetadataType(typeof(SECCIONESMetadata))]
    public partial class SECCIONES
    {

        public class SECCIONESMetadata
        {

            [Required(ErrorMessage="Nombre requerido")]
            [StringLength(100, ErrorMessage = "El nombre no puede ser mayor a 100 caracteres")]
            [DisplayName("Nombre")]
            public string NOMBRE { get; set; }

            [Required(ErrorMessage = "Texto requerido")]
            [StringLength(8000, ErrorMessage = "El texto no puede ser mayor a 8000 caracteres")]
            [DisplayName("Texto")]
            public string TEXTO { get; set; }

            [Required(ErrorMessage = "Texto requerido")]
            [StringLength(5000, ErrorMessage = "El texto no puede ser mayor a 5000 caracteres")]
            [DisplayName("Resumen")]
            public string RESUMEN { get; set; }

            [Required(ErrorMessage = "Categoría requerida")]
            [DisplayName("Categoría")]
            public int CATEGORIA_ID { get; set; }

            [DisplayName("Imagen resumen")]
            public string IMAGEN_RESUMEN { get; set; }

            [DisplayName("Metadata")]
            public string METADATA { get; set; }
           
            [DisplayName("Fecha Publicación")]
            public DateTime FECHA_PUBLICACION { get; set; }

            [DisplayName("Fecha Fin Publicación")]
            public DateTime? FECHA_FIN_PUBLICACION { get; set; }

            [StringLength(2, ErrorMessage = "El Campo Condición no puede ser mayor a 2 caracteres")]
            [DisplayName("Condición")]
            public string CONDICION { get; set; }

            [DisplayName("Esta Activo?")]
            public int ESTADO { get; set; }

            [DisplayName("Orden")]
            public string ORDEN { get; set; }

            [DisplayName("Alias")]
            [StringLength(50, ErrorMessage = "El alias no puede ser mayor a 50 caracteres")]
            public string ALIAS { get; set; }

            [DisplayName("Está en principal")]
            public decimal ESTAENPRINCIPAL{ get; set; }
        }
    }
}
