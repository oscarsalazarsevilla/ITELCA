﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace ITELCA_CLASSLIBRARY.Models
{
    [MetadataType(typeof(MODULOS_TURNOSMetadata))]
    public partial class MODULOS_TURNOS
    {
        public string MULTI_MODULO_ID { get; set; }
        public string MULTI_TURNO_ID { get; set; }

        public class MODULOS_TURNOSMetadata
        {
            [Required(ErrorMessage = "Módulo requerido")]
            [DisplayName("Módulo")]
            public decimal MODULO_ID { get; set; }

            [Required(ErrorMessage = "Turno requerido")]
            [DisplayName("Turno")]
            public decimal TURNO_ID { get; set; }

            [Required(ErrorMessage = "Número de Clases requerido")]
            [Range(1, 100, ErrorMessage = "Debe ser un número entre 1 -100")]
            [DisplayName("Número de Clases")]
            public int NRO_CLASES { get; set; }
            
            [DisplayName("Esta Activo?")]
            public int ESTADO { get; set; }
        }
    }
}
