﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace ITELCA_CLASSLIBRARY.Models
{
    [MetadataType(typeof(DETALLESRECIBOSMetadata))]
    public partial class DETALLESRECIBOS
    {
        public class DETALLESRECIBOSMetadata
        {
            [Required(ErrorMessage = "Recibo del Curso requerido")]
            [DisplayName("Recibo del Curso")]
            public decimal RECIBO_ID { get; set; }

            [DisplayName("Concepto a cancelar (Servicio)")]
            public decimal CONCEPTO_ESTUDIANTE_ID { get; set; }

            [Required(ErrorMessage = "Usuario requerido")]
            [DisplayName("Usuario")]
            public decimal USUARIO_ID { get; set; }

            [Required(ErrorMessage = "Cantidad requerida")]
            [DisplayName("Cantidad")]
            public decimal CANTIDAD { get; set; }

            [Required(ErrorMessage = "Monto requerido")]
            [DisplayName("Monto")]
            public decimal MONTO { get; set; }

            [Required(ErrorMessage = "Fecha de creación requerida")]
            [DisplayName("Fecha de Creación")]
            public DateTime FECHA_CREACION { get; set; }
        }
    }
}
