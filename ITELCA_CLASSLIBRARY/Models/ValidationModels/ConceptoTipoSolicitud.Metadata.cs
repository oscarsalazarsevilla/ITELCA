﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace ITELCA_CLASSLIBRARY.Models
{
    [MetadataType(typeof(CONCEPTOS_TIPOSSOLICITUDESMetadata))]
    public partial class CONCEPTOS_TIPOSSOLICITUDES
    {
        public string MULTI_TIPOSOLICITUD_ID { get; set; }
        public class CONCEPTOS_TIPOSSOLICITUDESMetadata
        {
            [Required(ErrorMessage = "Concepto requerido.")]
            [DisplayName("Concepto")]
            public decimal CONCEPTO_ID { get; set; }

            [Required(ErrorMessage="Tipo solicitud requerido.")]
            [DisplayName("Tipo solicitud")]
            public decimal TIPOSOLICITUD_ID { get; set; }

            [Required(ErrorMessage = "Monto requerido.")]
            [DisplayName("Monto")]
            public decimal MONTO { get; set; }

            [Required(ErrorMessage = "Fecha requerida.")]
            [DisplayName("Fecha Efectiva")]
            public DateTime FECHA_EFECTIVA { get; set; }
        }
    }
}
