﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace ITELCA_CLASSLIBRARY.Models
{
    [MetadataType(typeof(TURNOSMetadata))]
    public partial class TURNOS
    {
       
        public int PERIODO { get; set; }
        public class TURNOSMetadata
        {

            [StringLength(40, ErrorMessage = "El código  no puede ser mayor a 40 caracteres")]
            [Required(ErrorMessage = "Código es requerido")]
            [DisplayName("Código")]
            public string CODIGO { get; set; }

            [Required(ErrorMessage = "Nombre es requerido")]
            [StringLength(400, ErrorMessage = "El Nombre  no puede ser mayor a 400 caracteres")]
            [DisplayName("Nombre")]
            public string NOMBRE { get; set; }

            [DisplayName("esta Activo ?")]
            public decimal ESTADO { get; set; }
            
        
        }
    }
}
