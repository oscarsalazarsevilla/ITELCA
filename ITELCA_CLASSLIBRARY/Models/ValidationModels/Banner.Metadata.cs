﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace ITELCA_CLASSLIBRARY.Models
{
    [MetadataType(typeof(BANNERSMetadata))]
    public partial class BANNERS
    {
    
        public class BANNERSMetadata
        {
            
            [Required(ErrorMessage="Nombre de Banner requerido")]
            [StringLength(100, ErrorMessage = "El nombre no puede ser mayor a 100 caracteres")]
            [DisplayName("Nombre")]
            public string NOMBRE { get; set; }

            [Required(ErrorMessage = "Texto requerido")]
            [StringLength(500, ErrorMessage = "El texto no puede ser mayor a 500 caracteres")]
            [DisplayName("Texto")]
            public string TEXTO { get; set; }
           
            [DisplayName("URL")]
            public string URL { get; set; }

            [DisplayName("Esta Activo?")]
            public int ESTADO { get; set; }

            [DisplayName("Duración(en segundos)")]
            public int DURACION { get; set; }

            [DisplayName("Orden")]
            [Required(ErrorMessage = "Orden requerido")]
            [Range(1,999,ErrorMessage="El orden debe ser un número entre 1 y 999")]
            public int ORDEN { get; set; }

            [DisplayName("Fecha Publicación")]
            public DateTime FECHA_PUBLICACION { get; set; }

            [DisplayName("Fecha Fin Publicación")]
            public DateTime? FECHA_FIN_PUBLICACION { get; set; }
        }
    }
}
