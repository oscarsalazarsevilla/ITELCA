﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace ITELCA_CLASSLIBRARY.Models
{
    [MetadataType(typeof(TIPOSCURSOSMetadata))]
    public partial class TIPOSCURSOS
    {

      
        public class TIPOSCURSOSMetadata
        {
            [Required(ErrorMessage="Nombre requerido.")]
            [StringLength(100, ErrorMessage = "Nombre no puede ser mayor a 100 caracteres.")]
            [DisplayName("Nombre")]
            public string NOMBRE { get; set; }

            [StringLength(500, ErrorMessage = "Descripción no puede ser mayor a 500 caracteres.")]
            [DisplayName("Descripción")]
            public string DESCRIPCION { get; set; }

        }
    }
}
