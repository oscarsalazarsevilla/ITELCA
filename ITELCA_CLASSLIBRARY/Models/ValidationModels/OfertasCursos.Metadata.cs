﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace ITELCA_CLASSLIBRARY.Models
{
    [MetadataType(typeof(OFERTASCURSOSMetadata))]
    public partial class OFERTASCURSOS
    {
        public class OFERTASCURSOSMetadata
        {
           [Required(ErrorMessage = "Curso requerido")]
           [DisplayName("Curso")]
           public decimal CURSO_ID { get; set; }

           [Required(ErrorMessage = "Sede requerida")]
           [DisplayName("Sede")]
           public decimal SEDE_ID { get; set; }

           [Required(ErrorMessage = "Turno requerido")]
           [DisplayName("Turno")]
           public decimal TURNO_ID { get; set; }

           }
    }
}
