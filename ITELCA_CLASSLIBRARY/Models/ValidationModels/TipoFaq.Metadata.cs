﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace ITELCA_CLASSLIBRARY.Models
{
    [MetadataType(typeof(TIPOS_FAQSMetadata))]
    public partial class TIPOS_FAQS
    {
        public class TIPOS_FAQSMetadata
        {
            [Required(ErrorMessage="Nombre requerido.")]
            [StringLength(100, ErrorMessage = "El nombre no puede ser mayor a 100 caracteres.")]
            [DisplayName("Nombre")]
            public string NOMBRE { get; set; }

            [Required(ErrorMessage = "Descripción requerida.")]
            [StringLength(500, ErrorMessage = "La descripción no puede ser mayor a 500 caracteres.")]
            [DisplayName("Descripción")]
            public string DESCRIPCION { get; set; }
           
        }
    }
}
