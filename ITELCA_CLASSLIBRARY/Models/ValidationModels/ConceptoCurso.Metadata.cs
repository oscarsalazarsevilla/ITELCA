﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace ITELCA_CLASSLIBRARY.Models
{
    [MetadataType(typeof(CONCEPTOS_CURSOSMetadata))]
    public partial class CONCEPTOS_CURSOS
    {
        public string MULTI_CURSO_ID { get; set; }
        public string MULTI_TURNO_ID { get; set; }

        public class CONCEPTOS_CURSOSMetadata
        {
            [Required(ErrorMessage = "Concepto requerido.")]
            [DisplayName("Concepto")]
            public decimal CONCEPTO_ID { get; set; }

            [Required(ErrorMessage="Curso requerido.")]
            [DisplayName("Curso")]
            public string CURSO_ID { get; set; }

            [Required(ErrorMessage = "Turno requerido.")]
            [DisplayName("Turno")]
            public string TURNO_ID { get; set; }

            [Required(ErrorMessage = "Monto requerido.")]
            [DisplayName("Monto")]
            public decimal MONTO { get; set; }

            [Required(ErrorMessage = "Fecha requerida.")]
            [DisplayName("Fecha Efectiva")]
            public DateTime FECHA_EFECTIVA { get; set; }
        }
    }
}
