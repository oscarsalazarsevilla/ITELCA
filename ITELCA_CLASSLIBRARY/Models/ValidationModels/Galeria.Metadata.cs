﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace ITELCA_CLASSLIBRARY.Models
{
    [MetadataType(typeof(GALERIASMetadata))]
    public partial class GALERIAS
    {

        public class GALERIASMetadata
        {
            [Required(ErrorMessage = "Nombre requerido")]
            [StringLength(100, ErrorMessage = "Nombre no puede ser mayor a 100 caracteres")]
            [DisplayName("Nombre")]
            public string NOMBRE   { get; set; }

            [Required(ErrorMessage = "Descripción requerida")]
            [StringLength(1000, ErrorMessage = "Descripción  no puede ser mayor a 1000 caracteres")]
            [DisplayName("Descripción")]
            public string DESCRIPCION { get; set; }

            [DisplayName("Imagen")]
            public string URL { get; set; }

            [DisplayName("Orden")]
            [Range(1, 9999, ErrorMessage = "Orden Debe ser un número entre 1 - 9999")]
            public int ORDEN { get; set; }
        }
    }
}
