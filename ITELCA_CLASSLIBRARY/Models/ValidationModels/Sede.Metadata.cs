﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace ITELCA_CLASSLIBRARY.Models
{
    [MetadataType(typeof(SEDESMetadata))]
    public partial class SEDES
    {
        public class SEDESMetadata
        {
            [Required(ErrorMessage="Nombre de sede requerido")]
            [StringLength(40, ErrorMessage = "El nombre de la sede no puede ser mayor a 40 caracteres")]
            [DisplayName("Nombre")]
            public string NOMBRE { get; set; }

           [StringLength(1000, ErrorMessage = "La dirección de la sede no puede ser mayor a 1000 caracteres")]
            [DisplayName("Dirección")]
            public string DIRECCION { get; set; }

            [Required(ErrorMessage = "Teléfono 1 requerido")]
            [DisplayName("Teléfono 1")]
            public string TELEFONO1 { get; set; }

            [DisplayName("Teléfono 2")]
            public string TELEFONO2 { get; set; }

            [StringLength(40, ErrorMessage = "La latitud de la sede no puede ser mayor a 40 caracteres")]
            [DisplayName("Latitud")]
            public string LATITUD { get; set; }

            [StringLength(40, ErrorMessage = "La Longitud de la sede no puede ser mayor a 40 caracteres")]
            [DisplayName("Longitud")]
            public string LONGITUD { get; set; }

            [Required(ErrorMessage = "Código requerido")]
            [StringLength(40, ErrorMessage = "El código no puede ser mayor a 40 caracteres")]
            [DisplayName("Código")]
            public string CODIGO { get; set; }
            
            [DisplayName("Esta Activo ?")]
            public decimal ESTADO { get; set; }
        }
    }
}
