﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace ITELCA_CLASSLIBRARY.Models
{
    [MetadataType(typeof(ENTREGASMetadata))]
    public partial class ENTREGAS
    {
        public class ENTREGASMetadata
        {
            [Required(ErrorMessage = "Detalle de recibo requerido")]
            [DisplayName("Detalle de Recibo")]
            public decimal DETALLERECIBO_ID { get; set; }

            [Required(ErrorMessage = "Sede requerida")]
            [DisplayName("Sede")]
            public decimal SEDE_ID { get; set; }

            [Required(ErrorMessage = "Fecha de entrega requerida")]
            [DisplayName("Fecha de Entrega")]
            public DateTime FECHA_ENTREGA { get; set; }

            [Required(ErrorMessage = "Cantidad requerida")]
            [DisplayName("Cantidad")]
            public decimal CANTIDAD { get; set; }

            [Required(ErrorMessage = "Usuario requerido")]
            [DisplayName("Usuario")]
            public decimal USUARIO_ID { get; set; }
        }
    }
}
