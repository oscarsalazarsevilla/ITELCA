﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace ITELCA_CLASSLIBRARY.Models
{
    [MetadataType(typeof(DESCUENTOS_CURSOSMetadata))]
    public partial class DESCUENTOS_CURSOS
    {
        public class DESCUENTOS_CURSOSMetadata
        {
            [Required(ErrorMessage="Curso requerido")]
            [DisplayName("Curso")]
            public decimal CURSO_ID { get; set; }

            [Required(ErrorMessage = "Turno requerido")]
            [DisplayName("Turno")]
            public decimal TURNO_ID { get; set; }

            [Required(ErrorMessage = "Porcentaje requerido")]
            [DisplayName("Porcentaje")]
            public decimal MONTO { get; set; }

            [Required(ErrorMessage = "Fecha Inicio requerida")]
            [DisplayName("Fecha Inicio")]
            public DateTime FECHA_INIICIO { get; set; }

            [Required(ErrorMessage = "Fecha Fin requerida")]
            [DateGreaterThanAttribute("FECHA_INIICIO","Fecha inicio")] 
            [DisplayName("Fecha Fin")]
            public DateTime FECHA_FIN { get; set; }
        }
    }

    public sealed class DateGreaterThanAttribute : ValidationAttribute
    {
        private const string _defaultErrorMessage = "'{0}' debe ser mayor que '{1}'";
        private string _basePropertyName;
        private string _campo;

        public DateGreaterThanAttribute(string basePropertyName, string campo)
            : base(_defaultErrorMessage)
        {
            _basePropertyName = basePropertyName;
            _campo = campo;
        }

        //Override default FormatErrorMessage Method  
        public override string FormatErrorMessage(string name)
        {
            return string.Format(_defaultErrorMessage, name, _campo);
        }

        //Override IsValid  
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            //Get PropertyInfo Object  
            var basePropertyInfo = validationContext.ObjectType.GetProperty(_basePropertyName);

            //Get Value of the property  
            var startDate = (DateTime)basePropertyInfo.GetValue(validationContext.ObjectInstance, null);


            var thisDate = (DateTime)value;

            //Actual comparision  
            if (thisDate <= startDate)
            {
                var message = FormatErrorMessage(validationContext.DisplayName);
                return new ValidationResult(message);
            }

            //Default return - This means there were no validation error  
            return null;
        }

    }  
}
