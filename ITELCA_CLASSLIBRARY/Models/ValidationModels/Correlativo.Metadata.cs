﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace ITELCA_CLASSLIBRARY.Models
{
    [MetadataType(typeof(CORRELATIVOSMetadata))]
    public partial class CORRELATIVOS
    {

        public class CORRELATIVOSMetadata
        {
            [Required(ErrorMessage = "Sede requerida")]
            [DisplayName("Sede")]
            public int SEDE_ID   { get; set; }

            [Required(ErrorMessage="Recibo requerido")]
            [DisplayName("Tipo Recibo")]
            public string TIPO_DOCUMENTO { get; set; }

            [Required(ErrorMessage = "Número requerido")]
            [DisplayName("Número de Control")]
            public decimal NRO_CONTROL { get; set; }
        }
    }
}
