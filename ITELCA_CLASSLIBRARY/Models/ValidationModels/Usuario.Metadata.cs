﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace ITELCA_CLASSLIBRARY.Models
{
    [MetadataType(typeof(USUARIOSMetadata))]
    public partial class USUARIOS
    {
        public string CONFIRMARPASSWORD { get; set; }
        public string Rol_Id { get; set; }
     
        public class USUARIOSMetadata
        {
            [Required(ErrorMessage="Nombre de usuario requerido")]
            [StringLength(15, ErrorMessage = "El nombre de usuario no puede ser mayor a 15 caracteres")]
            [DisplayName("Usuario")]
            public string NOMBREUSUARIO { get; set; }

            [Required(ErrorMessage = "Contraseña requerida")]
            [StringLength(80, ErrorMessage = "La contraseña no puede ser mayor a 80 caracteres")]
            [DisplayName("Contraseña")]
            public string PASSWORD { get; set; }

            [Required(ErrorMessage = "Confirmación de contraseña requerida")]
            [StringLength(80, ErrorMessage = "La confirmación de la contraseña no puede ser mayor a 80 caracteres")]
            [DisplayName("Confirmación contraseña")]
            public string CONFIRMARPASSWORD { get; set; }

            [Required(ErrorMessage = "Fecha de nacimiento es requerida")]
            [DisplayName("Fecha de Nacimiento")]
            public string FECHADENACIMIENTO { get; set; } 

            [Required(ErrorMessage = "Nombre(s) requerido")]
            [StringLength(50, ErrorMessage = "El(los) nombre(s) no puede ser mayor a 50 caracteres")]
            [DisplayName("Nombre(s)")]
            public string NOMBRE { get; set; }

            [StringLength(50, ErrorMessage = "El(los) nombre(s) no puede ser mayor a 50 caracteres")]
            [DisplayName("Apellido(s)")]
            public string APELLIDO { get; set; }

            [StringLength(500, ErrorMessage = "La dirección no puede ser mayor a 500 caracteres")]
            [DisplayName("Dirección")]
            public string DIRECCION { get; set; }

            [StringLength(40, ErrorMessage = "Número de Cédula no puede ser mayor a 40 caracteres")]
            [DisplayName("Cédula")]
            [Required(ErrorMessage = "Cédula requerida")]
            public string NRO_DOCUMENTO { get; set; }

            [StringLength(50, ErrorMessage = "El email no puede ser mayor a 50 caracteres")]
            [RegularExpression(@"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*", ErrorMessage = "Email en formato incorrecto")]
            [DisplayName("Correo Electrónico")]
            public string EMAIL { get; set; }

            [Required(ErrorMessage = "Género es requerido")]
            [DisplayName("Género")]
            public string GENERO_ID { get; set; }

            [DisplayName("Teléfono")]
            public string TELEFONO { get; set; }

            [DisplayName("Tlf. Movil")]
            public string TELEFONO2 { get; set; }

            [DisplayName("Está bloqueado?")]
            public string ESTABLOQUEADO { get; set; }

            [Required(ErrorMessage = "Rol es requerido")]
            [DisplayName("Roles")]
            public string Rol_Id { get; set; } 
        }
    }
}
