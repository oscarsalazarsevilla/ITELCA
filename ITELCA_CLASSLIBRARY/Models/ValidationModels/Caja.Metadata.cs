﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace ITELCA_CLASSLIBRARY.Models
{
    [MetadataType(typeof(CAJASMetadata))]
    public partial class CAJAS
    {
    
        public class CAJASMetadata
        {
            [Required(ErrorMessage = "Sede requerida")]
            [DisplayName("Sede")]
            public int SEDE_ID   { get; set; }

            [Required(ErrorMessage="Nombre requerido")]
            [StringLength(40, ErrorMessage = "El nombre de la caja no puede ser mayor a 15 caracteres")]
            [DisplayName("Nombre")]
            public string NOMBRE { get; set; }

            [Required(ErrorMessage = "Cóodigo requerido")]
            [StringLength(40, ErrorMessage = "El código no puede ser mayor a 15 caracteres")]
            [DisplayName("Código")]
            public string CODIGO { get; set; }

            [Required(ErrorMessage = "Descripción de Caja requerida")]
            [DisplayName("Descripción")]
            public string DESCRIPCION { get; set; }

            [StringLength(2, ErrorMessage = "El Campo Condición no puede ser mayor a 2 caracteres")]
            [DisplayName("Condición")]
            public string CONDICION { get; set; }

            [DisplayName("Esta Activo ?")]
            public int ESTADO { get; set; }
        }
    }
}
