﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace ITELCA_CLASSLIBRARY.Models
{
    [MetadataType(typeof(CONCEPTOS_TALLERESMetadata))]
    public partial class CONCEPTOS_TALLERES
    {
        public string MULTI_TALLER_ID { get; set; }
        public class CONCEPTOS_TALLERESMetadata
        {
            [Required(ErrorMessage = "Concepto requerido.")]
            [DisplayName("Concepto")]
            public decimal CONCEPTO_ID { get; set; }

            [Required(ErrorMessage="Taller requerido.")]
            [DisplayName("Taller")]
            public decimal TALLER_ID { get; set; }

            [Required(ErrorMessage = "Monto requerido.")]
            [DisplayName("Monto")]
            public decimal MONTO { get; set; }

            [Required(ErrorMessage = "Fecha requerida.")]
            [DisplayName("Fecha Efectiva")]
            public DateTime FECHA_EFECTIVA { get; set; }
        }
    }
}
