﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace ITELCA_CLASSLIBRARY.Models
{
    [MetadataType(typeof(CONTACTOMetadata))]
    public partial class CONTACTO
    {
        public class CONTACTOMetadata
        {
            [Required(ErrorMessage = "Nombre requerido.")]
            [StringLength(100, ErrorMessage = "El Nombre no puede ser mayor a 100 caracteres")]
            [DisplayName("Nombre")]
            public string NOMBRE { get; set; }

            [Required(ErrorMessage = "Asunto requerido.")]
            [StringLength(100, ErrorMessage = "El Asunto no puede ser mayor a 100 caracteres")]
            [DisplayName("Asunto")]
            public string ASUNTO { get; set; }

            [Required(ErrorMessage = "Mensaje requerido.")]
            [StringLength(500, ErrorMessage = "El mensaje no puede ser mayor a 100 caracteres")]
            [DisplayName("Texto")]
            public string TEXTO { get; set; }


            [Required(ErrorMessage = "Teléfono requerido")]
            [DisplayName("Teléfono")]
            public string TELEFONO { get; set; }

            [Required(ErrorMessage = "Email requerido")]
            [DisplayName("Email")]
            public string EMAIL { get; set; }
       

          

        }
    }
}
