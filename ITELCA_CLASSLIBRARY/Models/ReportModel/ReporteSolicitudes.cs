﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ITELCA_CLASSLIBRARY.Repositories;

namespace ITELCA_CLASSLIBRARY.Models.ReportModel
{

    public class ReporteSolicitudes
    {
        /*USUARIO*/
        public string codigo { get; set; }
        public string apellido { get; set; }
        public string nombre { get; set; }
        public string nro_documento { get; set; }


        /*CURSOS*/
        public string codigo_curso { get; set; }
        public string nombre_curso { get; set; }
        public string turno { get; set; }
        public string dias { get; set; }
        public string duracion { get; set; }
        public string desde { get; set; }
        public string hasta { get; set; }
        public string director { get; set; }

        /*FECHA ACTUAL*/
        public string dia_letra { get; set; }
        public string mes_letra { get; set; }
        public string anio_letra { get; set; }

        public string matriculacurso_id { get; set; }


        public ReporteSolicitudes(  string codigo, string apellido, string nombre, string nro_documento,
                                    string codigo_curso, string nombre_curso, string turno, string dias,
                                    string dia_letra, string mes_letra, string anio_letra, string duracion,
                                    string desde, string hasta, string director, string matriculacurso_id)
        {

            this.codigo = codigo;
            this.apellido = apellido;
            this.nombre = nombre;
            this.nro_documento = nro_documento;
            this.codigo_curso = codigo_curso;
            this.nombre_curso = nombre_curso;
            this.turno = turno;
            this.dias = dias;
            this.dia_letra = dia_letra;
            this.mes_letra = mes_letra;
            this.anio_letra = anio_letra;
            this.duracion = duracion;
            this.desde = desde;
            this.hasta = hasta;
            this.director = director;
            this.matriculacurso_id = matriculacurso_id;
        }


    }

    public class DataPasantias
    {
        /*FECHA ACTUAL*/
        public string dia_numero { get; set; }
        public string mes_letra { get; set; }
        public string anio_numero { get; set; }

        /*SOLICITUD*/
        public string solicitud_id { get; set; }
        public string fecha_inicio { get; set; }
        public string fecha_fin { get; set; }
        public string fecha_creacion { get; set; }
        public string observacion { get; set; }
        public string institucion { get; set; }
        public string director { get; set; }
        public string matriculacurso_id { get; set; }
        public string turno_id { get; set; }
        public string ofertacurso_id { get; set; }
        public string alumno_id { get; set; }
        public string tipo_solicitud { get; set; }


        public DataPasantias(string solicitud_id, string institucion, string director,
                                  string dia_numero, string mes_letra, string anio_numero, string matriculacurso_id)
        {
            this.solicitud_id = solicitud_id;
            this.institucion = institucion;
            this.director = director;
            this.dia_numero = dia_numero;
            this.mes_letra = mes_letra;
            this.anio_numero = anio_numero;
            this.matriculacurso_id = matriculacurso_id;
        }


    }

    public class DataConstanciaNotas
    {
       
        /*SOLICITUD*/
        public string nombre_modulo { get; set; }
        public string nota_modulo { get; set; }

        

        public DataConstanciaNotas(string nombre_modulo, string nota_modulo)
        {
            this.nombre_modulo = nombre_modulo;
            this.nota_modulo = nota_modulo;
        }


    }

    public class DataReciboProducto
    {


        public string cedula { get; set; }
        public string id_recibo { get; set; }
        public string nombre_alumno { get; set; }
        public string monto { get; set; }
        public string curso { get; set; }
        public string monto_letras { get; set; }
        public string concepto { get; set; }
        public string dia_numero { get; set; }
        public string mes_letra { get; set; }
        public string anio_numero { get; set; }



        public DataReciboProducto(string cedula, string id_recibo, string nombre_alumno, string monto, string curso,string monto_letras,
                                string concepto,string dia_numero, string mes_letra, string anio_numero)
        {
            this.cedula = cedula;
            this.id_recibo = id_recibo;
            this.nombre_alumno = nombre_alumno;
            this.monto = monto;
            this.curso = curso;
            this.monto_letras = monto_letras;
            this.concepto = concepto;
            this.dia_numero = dia_numero;
            this.mes_letra = mes_letra;
            this.anio_numero = anio_numero;
        }


    }

    public class DataReciboCaja
    {

        /*SOLICITUD*/
        public string alumno_nombre { get; set; }
        public string alumno_id { get; set; }
        public string monto_numero { get; set; }
        public string monto_letra { get; set; }
        public string id_recibo { get; set; }
        public string periodo_cancelado { get; set; }
        public string turno { get; set; }
        public string cuota { get; set; }
        public string curso { get; set; }
        public string fecha_inicio { get; set; }
        public string fecha_pago { get; set; }
        public string fecha_cancelo { get; set; }



        public DataReciboCaja(string alumno_nombre, string alumno_id, string monto_numero, string monto_letra, string id_recibo,
                              string periodo_cancelado, string turno, string cuota, string curso, string fecha_inicio,
                              string fecha_pago, string fecha_cancelo)
        {
            this.alumno_nombre = alumno_nombre;
            this.alumno_id = alumno_id;
            this.monto_numero = monto_numero;
            this.monto_letra = monto_letra;
            this.id_recibo = id_recibo;
            this.periodo_cancelado = periodo_cancelado;
            this.turno = turno;
            this.cuota = cuota;
            this.curso = curso;
            this.fecha_inicio = fecha_inicio;
            this.fecha_pago = fecha_pago;
            this.fecha_cancelo = fecha_cancelo;
        }


    }

    public class DataPlanificacion
    {

        /*Planificacion*/
        public string nombre_sede { get; set; }
        public string nombre_curso { get; set; }
        public string nombre_turno { get; set; }
        public string ofertaCurso{ get; set; }


        public DataPlanificacion(string nombre_sede, string nombre_curso, string nombre_turno, string ofertaCurso)
        {
            this.nombre_turno = nombre_turno;
            this.nombre_curso = nombre_curso;
            this.nombre_sede = nombre_sede;
            this.ofertaCurso = ofertaCurso;
        }


    }

    public class ModulosPlanificacion
    {

        /*Planificacion*/
        public string  nombre_modulo{ get; set; }
        public string fecha_inicio { get; set; }
        public string fecha_fin { get; set; }
        public string matricula_minima { get; set; }
        public string profesor_nombre { get; set; }


        public ModulosPlanificacion(string nombre_modulo, string fecha_inicio, string fecha_fin, string matricula_minima, string profesor_nombre)
        {
            this.nombre_modulo = nombre_modulo;
            this.fecha_inicio = fecha_inicio;
            this.fecha_fin = fecha_fin;
            this.matricula_minima = matricula_minima;
            this.profesor_nombre = profesor_nombre;
        }


    }

}