//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ITELCA_CLASSLIBRARY.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class MATRICULASCURSOS
    {
        public MATRICULASCURSOS()
        {
            this.MATRICULASMODULOS = new HashSet<MATRICULASMODULOS>();
            this.SOLICITUDES = new HashSet<SOLICITUDES>();
            this.DETALLESRECIBOS = new HashSet<DETALLESRECIBOS>();
            this.CONCEPTOS_ESTUDIANTES = new HashSet<CONCEPTOS_ESTUDIANTES>();
        }
    
        public decimal MATRICULA_CURSO_ID { get; set; }
        public decimal OFERTA_CURSO_ID { get; set; }
        public decimal ALUMNO_ID { get; set; }
        public Nullable<decimal> CURSO_ID { get; set; }
        public Nullable<decimal> SEDE_ID { get; set; }
        public Nullable<decimal> TURNO_ID { get; set; }
        public Nullable<decimal> EMPLEADO_ID { get; set; }
        public Nullable<System.DateTime> FECHA_INSCRIPCION { get; set; }
        public Nullable<decimal> DIA_CORTE { get; set; }
        public Nullable<decimal> TIENE_DCTO { get; set; }
        public Nullable<decimal> DESCUENTO { get; set; }
        public Nullable<decimal> COSTO_MENSUALIDAD { get; set; }
        public Nullable<System.DateTime> FECHA_CREACION { get; set; }
        public Nullable<decimal> USUARIO_ID { get; set; }
        public string CONDICION { get; set; }
        public string NRO_CONTROL { get; set; }
    
        public virtual CURSOS CURSOS { get; set; }
        public virtual USUARIOS USUARIOS { get; set; }
        public virtual USUARIOS USUARIOS1 { get; set; }
        public virtual ICollection<MATRICULASMODULOS> MATRICULASMODULOS { get; set; }
        public virtual ICollection<SOLICITUDES> SOLICITUDES { get; set; }
        public virtual OFERTASCURSOS OFERTASCURSOS { get; set; }
        public virtual SEDES SEDES { get; set; }
        public virtual TURNOS TURNOS { get; set; }
        public virtual USUARIOS USUARIOS2 { get; set; }
        public virtual ICollection<DETALLESRECIBOS> DETALLESRECIBOS { get; set; }
        public virtual ICollection<CONCEPTOS_ESTUDIANTES> CONCEPTOS_ESTUDIANTES { get; set; }
    }
}
