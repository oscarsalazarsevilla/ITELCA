//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ITELCA_CLASSLIBRARY.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class CONCEPTOS_TALLERES
    {
        public CONCEPTOS_TALLERES()
        {
            this.CONCEPTOS_ESTUDIANTES = new HashSet<CONCEPTOS_ESTUDIANTES>();
        }
    
        public decimal CONCEPTO_TALLER_ID { get; set; }
        public decimal CONCEPTO_ID { get; set; }
        public decimal TALLER_ID { get; set; }
        public System.DateTime FECHA_EFECTIVA { get; set; }
        public decimal MONTO { get; set; }
        public decimal CANTIDAD { get; set; }
        public System.DateTime FECHA_CREACION { get; set; }
        public decimal ESTADO { get; set; }
        public decimal USUARIO_ID { get; set; }
        public string CONDICION { get; set; }
    
        public virtual CONCEPTOS CONCEPTOS { get; set; }
        public virtual TALLERES TALLERES { get; set; }
        public virtual USUARIOS USUARIOS { get; set; }
        public virtual ICollection<CONCEPTOS_ESTUDIANTES> CONCEPTOS_ESTUDIANTES { get; set; }
    }
}
