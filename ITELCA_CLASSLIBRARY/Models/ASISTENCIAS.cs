//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ITELCA_CLASSLIBRARY.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class ASISTENCIAS
    {
        public decimal MATRICULAMODULO_ID { get; set; }
        public decimal ALUMNO_ID { get; set; }
        public System.DateTime FECHA_CLASE { get; set; }
        public decimal ASISTIO { get; set; }
        public Nullable<System.DateTime> FECHA_CREACION { get; set; }
        public decimal USUARIO_ID { get; set; }
    
        public virtual MATRICULASMODULOS MATRICULASMODULOS { get; set; }
        public virtual USUARIOS USUARIOS { get; set; }
    }
}
