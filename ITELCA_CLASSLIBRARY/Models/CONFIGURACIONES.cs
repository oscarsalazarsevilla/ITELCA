//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ITELCA_CLASSLIBRARY.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class CONFIGURACIONES
    {
        public string INSTITUCION { get; set; }
        public string NUMERO_ME { get; set; }
        public string DIRECCION_FISCAL { get; set; }
        public string TELEFONO1 { get; set; }
        public string TELEFONO2 { get; set; }
        public string WEB { get; set; }
        public string TWITTER { get; set; }
        public string FACEBOOK { get; set; }
        public string LINKEDIN { get; set; }
        public string SMTPSERVER { get; set; }
        public decimal SMTPPUERTO { get; set; }
        public string EMAIL { get; set; }
        public string PWDEMAIL { get; set; }
        public string LOGO { get; set; }
        public string VERSION { get; set; }
        public string DIRECTOR { get; set; }
        public string SUBDIRECTOR { get; set; }
        public string CONTROLESTUDIOS { get; set; }
    }
}
