//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ITELCA_CLASSLIBRARY.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class TIPOSDOCUMENTOS
    {
        public decimal TIPODOCUMENTO_ID { get; set; }
        public string NOMBRE { get; set; }
        public string IMAGEN { get; set; }
        public string CONDICION { get; set; }
        public System.DateTime FECHA_CREACION { get; set; }
        public decimal ESTADO { get; set; }
        public Nullable<decimal> USUARIO_ID { get; set; }
    }
}
