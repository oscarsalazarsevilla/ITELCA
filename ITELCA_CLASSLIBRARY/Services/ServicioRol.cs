﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using System.Data.Entity;
using ITELCA_CLASSLIBRARY.Models;
using ITELCA_CLASSLIBRARY.Repositories;
using ITELCA_CLASSLIBRARY.CustomClasses;
using System.Collections;
using System.Web;
using UmbrellaClassLibrary.CustomClasses;


namespace ITELCA_CLASSLIBRARY.Services
{
     public class ServicioRol
    {
        private UnitOfWork unidad;
        public ServicioRol()
        {
            this.unidad = new UnitOfWork();
        }
        public bool Guardar(ROLES rol)
        {
            try
            {
                rol.CONDICION ="C";
                unidad.RepositorioRoles.Add(rol);
                unidad.Save();
                return true;
            }
            catch
            {
                return false;
            }
        }
        public bool Modificar(ROLES entity)
        {
            try
            {
                entity.CONDICION = "A";
                unidad.RepositorioRoles.Modify(entity);
                unidad.Save();
                return true;
            }
            catch
            {
                return false;
            }
        }


        public List<object> Eliminar(decimal id)
        {
            List<object> lista = new List<object>();
            try
            {
                var aux = unidad.RepositorioRoles.GetById((int)id);
                if (aux != null)
                {
                    lista.Add(new
                    {
                        ok = 1,
                        msg = "Rol Eliminado"
                    });
                    aux.CONDICION = "E";
                    aux.ESTADO = 0;
                    unidad.RepositorioRoles.Modify(aux);
                    //unidad.RepositorioRoles.Delete(aux);
                    unidad.Save();
                    return lista;
                }
                else
                {
                    lista.Add(new
                    {
                        ok = 0,
                        msg = "Rol No encontrado"
                    });
                    return lista;
                }
            }
            catch
            {
                lista.Add(new
                {
                    ok = 0,
                    msg = "Error Eliminando Rol"
                });
                return lista;
            }

        }

        public object ObtenerDataGrid( string sidx, string sord, int page, int rows, string filters)
        {
            int totalPages = 0;
            int totalRecords = 0;
            IEnumerable<ROLES> roles;
            roles = unidad.RepositorioRoles.GetAll().Where(u => u.ESTADO == 1 || (u.ESTADO == 0 && u.CONDICION != "E"));
            roles = JqGrid<ROLES>.GetFilteredContent(sidx, sord, page, rows, filters, roles.AsQueryable(), ref totalPages, ref totalRecords);
           
            var rowsModel = (
                from rol in roles.ToList()
                select new
                {
                    i = rol.ROL_ID,
                    cell = new string[] {
                           rol.ROL_ID.ToString(),
                           rol.NOMBRE.ToString(),
                           (rol.ESTADO == 1) ? "Activo" : "Inactivo",
                            "<a title=\"Editar\" href=\"/Rol/Editar/"+ rol.ROL_ID+"\"><span id=\""+rol.ROL_ID+"\" class=\"ui-icon ui-icon-pencil\"></a>",                          
                            "<span id=\""+rol.ROL_ID+"\"  class=\"ui-icon ui-icon-close\" ></span>" }
                }).ToArray();
            return JqGrid<ROLES>.SetJsonData(totalPages, totalRecords, page, rowsModel);
        }

        public ROLES ObtenerPorClave(int id)
        {
            try
            {
                return unidad.RepositorioRoles.GetById(id);
                
            }
            catch 
            {
                return null;
            }
        }
        public ROLES ObtenerPorNombre(string nombre)
        {
            try
            {
                return unidad.RepositorioRoles.GetAll().Where(u=>u.NOMBRE.ToLower()==u.NOMBRE.ToLower() && u.ESTADO==1).FirstOrDefault();

            }
            catch
            {
                return null;
            }
        }
       
        public IEnumerable<ROLES> ObtenerTodos()
        {
            try
            {
                IEnumerable<ROLES> roles= unidad.RepositorioRoles.GetAll().Where(u=>u.ESTADO==1);
                if (roles == null)
                    return Enumerable.Empty<ROLES>();
                else
                    return roles;
            }
            catch  {
                return Enumerable.Empty<ROLES>();
            }
        }
         
        public bool Duplicado(ROLES rol)
        {
            try
            {
                if (unidad.RepositorioRoles.GetAll().Where(u => u.NOMBRE.ToLower() == rol.NOMBRE.ToLower() &&
                                                                 u.ROL_ID!=rol.ROL_ID && u.ESTADO==1).Count() > 0)
                    return true;
                else
                    return false;

            }
            catch
            {
                return false;
            }
        }
       
    }
}
