﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using System.Data.Entity;
using ITELCA_CLASSLIBRARY.Models;
using ITELCA_CLASSLIBRARY.Repositories;
using ITELCA_CLASSLIBRARY.CustomClasses;
using System.Collections;
using System.Web;
using UmbrellaClassLibrary.CustomClasses;
using System.Data.Entity.Validation;

namespace ITELCA_CLASSLIBRARY.Services
{
     public class ServicioCategoria
    {
        private UnitOfWork unidad;
        public ServicioCategoria()
        {
            this.unidad = new UnitOfWork();
        }
        public bool Guardar(CATEGORIAS categoria)
        {
            try
            {
                var padre = categoria.PADRE_ID;
                categoria.PADRE_ID = null;
                categoria.CONDICION = "C";
                unidad.RepositorioCategorias.Add(categoria);
                unidad.Save();

                categoria.CONDICION = "A";
                categoria.PADRE_ID = padre;
                unidad.RepositorioCategorias.Modify(categoria);
                unidad.Save();
                return true;
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                return false;

            }
        }
        public bool Modificar(CATEGORIAS categoria)
        {
            try
            {
                categoria.CONDICION = "A";
                unidad.RepositorioCategorias.Modify(categoria);
                unidad.Save();
                return true;
            }
            catch
            {
                return false;
            }
        }


        public List<object> Eliminar(decimal id)
        {
            List<object> lista = new List<object>();
            try
            {
                var aux = unidad.RepositorioCategorias.GetById((int)id);
                if (aux != null)
                {
                    lista.Add(new
                    {
                        ok = 1,
                        msg = "Categoria Eliminada"
                    });
                    aux.CONDICION = "E";
                    aux.ESTADO = 0;
                    unidad.RepositorioCategorias.Modify(aux);
                    //unidad.RepositorioCategorias.Delete(aux);
                    unidad.Save();
                    return lista;
                }
                else
                {
                    lista.Add(new
                    {
                        ok = 0,
                        msg = "Categoria No encontrada"
                    });
                    return lista;
                }
            }
            catch
            {
                lista.Add(new
                {
                    ok = 0,
                    msg = "Error Eliminando Categoria"
                });
                return lista;
            }

        }

        public Dictionary<decimal, string> ObtenerDiccionarioCategorias()
        {
            Dictionary<decimal, string> lista = new Dictionary<decimal, string>();
            foreach (var entity in ObtenerTodos().ToList())
            {
                lista.Add(entity.CATEGORIA_ID, entity.NOMBRE);
            }
            return lista;
        }

        public object ObtenerDataGrid( string sidx, string sord, int page, int rows, string filters)
        {
            int totalPages = 0;
            int totalRecords = 0;
            IEnumerable<CATEGORIAS> categorias;
            categorias = unidad.RepositorioCategorias.GetAll().Where(u => u.ESTADO == 1 || (u.ESTADO == 0 && u.CONDICION != "E"));
            categorias = JqGrid<CATEGORIAS>.GetFilteredContent(sidx, sord, page, rows, filters, categorias.AsQueryable(), ref totalPages, ref totalRecords);
           
            var rowsModel = (
                from categoria in categorias.ToList()
                select new
                {
                    i = categoria.CATEGORIA_ID,
                    cell = new string[] {
                           categoria.CATEGORIA_ID.ToString(),
                           categoria.NOMBRE.ToString(),
                           categoria.ALIAS.ToString(),
                           categoria.TIPOSCATEGORIAS.NOMBRE,
                           categoria.ORDEN.ToString(),
                           (categoria.PADRE_ID.HasValue) ? categoria.CATEGORIAS2.NOMBRE : "---",
                           categoria.FECHA_PUBLICACION.ToString("dd/MM/yyyy"),
                           (categoria.FECHA_FIN_PUBLICACION.HasValue) ? categoria.FECHA_FIN_PUBLICACION.Value.ToString("dd/MM/yyyy") : "---",
                           categoria.ESTADO.ToString(),
                           "<a title=\"Editar\" href=\"/Categoria/Editar/"+ categoria.CATEGORIA_ID+"\"><span id=\""+categoria.CATEGORIA_ID+"\" class=\"ui-icon ui-icon-pencil\"></a>",                          
                           "<span id=\""+categoria.CATEGORIA_ID+"\"  class=\"ui-icon ui-icon-close\" ></span>" }
                }).ToArray();
            return JqGrid<CATEGORIAS>.SetJsonData(totalPages, totalRecords, page, rowsModel);
        }

        public CATEGORIAS ObtenerPorClave(int id)
        {
            try
            {
                return unidad.RepositorioCategorias.GetById(id);
                
            }
            catch 
            {
                return null;
            }
        }

        public CATEGORIAS ObtenerPorNombre(string nombre)
        {
            try
            {
                return unidad.RepositorioCategorias.GetAll().Where( u=> u.NOMBRE == nombre.ToUpper()).FirstOrDefault();

            }
            catch
            {
                return null;
            }
        }

       
        public IEnumerable<CATEGORIAS> ObtenerTodos()
        {
            try
            {
                IEnumerable<CATEGORIAS> sedes= unidad.RepositorioCategorias.GetAll().Where(u=>u.ESTADO==1);
                if (sedes == null)
                    return Enumerable.Empty<CATEGORIAS>();
                else
                    return sedes;
            }
            catch {
                return Enumerable.Empty<CATEGORIAS>();
            }
        }

        public bool Duplicado(CATEGORIAS categoria)
        {
            try
            {
                if (unidad.RepositorioCategorias.GetAll().Where(u => u.NOMBRE.ToLower() == categoria.NOMBRE.ToLower() && u.CATEGORIA_ID != categoria.CATEGORIA_ID && categoria.ESTADO==1).Count() > 0)
                    return true;
                else
                    return false;

            }
            catch 
            {
                return false;
            }
        }

        public List<CATEGORIAS> ObtenerOrdenadas()
        {
            return ObtenerTodos().OrderBy(c => c.ORDEN).ToList();
        }

        
        public List<object> ObtenerObjeto()
        {
            List<object> lista = new List<object>();
            try
            {
                foreach (var ope in ObtenerTodos())
                    lista.Add(new
                    {
                        key = ope.CATEGORIA_ID,
                        value = ope.NOMBRE
                    });
                return lista;
            }
            catch
            {
                return null;
            }
        }

        public List<CATEGORIAS> ObtenerPadres()
        {
            return ObtenerOrdenadas().Where(c=>c.PADRE_ID == null).ToList();
        }

        public List<CATEGORIAS> ObtenerHijos(decimal? padreId)
        {
            if (padreId.HasValue)
                return ObtenerOrdenadas().Where(c=>c.PADRE_ID == padreId.Value).ToList();
            else
                return ObtenerOrdenadas().Where(c => !c.PADRE_ID.HasValue || c.PADRE_ID == null).ToList();
        }

        public string ObtenerMenu()
        {
            string padres = "", hijos = "", jscript = "";
            ImprimirPadres(ref padres, ref hijos, ref jscript);
            return padres + hijos + "<script type=\"text/javascript\">$(document).ready(function(){" + jscript + "});</script>";
        }

        public void ImprimirPadres(ref string padres, ref string hijos, ref string jscript)
        {
            string categoria = "", enlace = "";
            int cont = 1;
            List<CATEGORIAS> padresList = ObtenerHijos(null);

            padres = "<div id=\"menu\" class=\"common-box1\"><div class=\"common-box2\"><a href=\" / \">Inicio</a> ";

            foreach (var padre in padresList)
            {
                //obtenemos el nombre de la categoria en el idioma actual
                categoria = padre.NOMBRE;

                //obtenemos la sección de la categoría si aplica
                enlace = ObtenerEnlace(padre);

                padres += "<a href=\""+enlace+"\" id=\"p"+cont+"\">"+categoria+"</a>";

                List<CATEGORIAS> hijosList = ObtenerHijos(padre.CATEGORIA_ID);

                //Armamaos el menú para las categorías regulares
                if (padre.TIPOCATEGORIA_ID == 0)
                {
                    //tiene hijos
                    if (hijosList.Count() > 0)
                    {
                        jscript += " $(\"#p" + cont + "\").click(function () {$(\"#h" + cont + "\").slideToggle(\"slow\");}); $(\"#h" + cont + "\").hide();";
                        hijos += "<div id=\"submenu\" class=\"common-box2 \"><div id=\"h" + cont + "\" class=\"sm-c\"><div>";
                        hijos += ImprimirHijo(padre.CATEGORIA_ID);
                        hijos += "</div></div></div>";
                    }
                }
                //Categorías tipo Curso
                else if (padre.TIPOCATEGORIA_ID == 1)
                {
                    jscript += " $(\"#p" + cont + "\").click(function () {$(\"#h" + cont + "\").slideToggle(\"slow\");}); $(\"#h" + cont + "\").hide();";
                    hijos += "<div id=\"submenu\" class=\"common-box2\"><div id=\"h" + cont + "\" class=\"sm-c\">";
                    hijos += new ServicioCursos().ImprimirCursosParaMenu();
                    hijos += "</div></div></div>";
                }
                //Categorías tipo Taller
                else if (padre.TIPOCATEGORIA_ID == 5)
                {
                    jscript += " $(\"#p" + cont + "\").click(function () {$(\"#h" + cont + "\").slideToggle(\"slow\");}); $(\"#h" + cont + "\").hide();";
                    hijos += "<div id=\"submenu\" class=\"common-box2\"><div id=\"h" + cont + "\" class=\"sm-c\">";
                    hijos += new ServicioTaller().ImprimirTalleresParaMenu();
                    hijos += "</div></div></div>";
                }
                cont++;
            }
            padres += "</div></div>";
        }

        public string ImprimirHijo(decimal padreId)
        {
            List<CATEGORIAS> hijos = ObtenerHijos(padreId);
            string menu = "", categoria = "", enlace = "";

            foreach (var hijo in hijos)
            {
                //obtenemos el nombre de la categoria en el idioma actual
                categoria = hijo.NOMBRE;

                //obtenemos la sección de la categoría si aplica
                enlace = ObtenerEnlace(hijo);

                if (ObtenerHijos(hijo.CATEGORIA_ID).Count() <= 0)
                {
                    menu += "<a href=\"" + enlace + "\">" + categoria + "</a>";
                }
                else
                {
                    menu += "<strong><a href=\"" + enlace + "\">" + categoria + "</a></strong>";
                    menu += ImprimirHijo(hijo.CATEGORIA_ID);
                    menu += "</br>";
                }
            }
            return menu;
        }

        public string ObtenerEnlace(CATEGORIAS categoria)
        {
            if (categoria.SECCIONES != null)
            {
                if (categoria.TIPOCATEGORIA_ID == 2)
                    return "/Home/Noticias/";
                else if (categoria.TIPOCATEGORIA_ID == 3)
                    return "/Home/Contactenos/";
                else if (categoria.TIPOCATEGORIA_ID == 4)
                    return "/Home/Galerias/";
                else
                {
                    if (categoria.SECCIONES.Count() > 0)
                        return "/Home/Seccion/" + categoria.SECCIONES.FirstOrDefault().SECCION_ID;
                    else
                        return "#";
                }
            }
            else
            {
                return "#";
            }
        }
       
    }
}
