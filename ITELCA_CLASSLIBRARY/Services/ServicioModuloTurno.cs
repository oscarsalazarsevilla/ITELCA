﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using System.Data.Entity;
using ITELCA_CLASSLIBRARY.Models;
using ITELCA_CLASSLIBRARY.Repositories;
using ITELCA_CLASSLIBRARY.CustomClasses;
using System.Collections;
using System.Web;
using UmbrellaClassLibrary.CustomClasses;


namespace ITELCA_CLASSLIBRARY.Services
{
     public class ServicioModuloTurno
    {
        private UnitOfWork unidad;
        public ServicioModuloTurno()
        {
            this.unidad = new UnitOfWork();
        }
        public bool Guardar(MODULOS_TURNOS modulo_turno)
        {
            try
            {
                string[] arregloModulos = modulo_turno.MULTI_MODULO_ID.Split(',');
                string[] arregloTurnos = modulo_turno.MULTI_TURNO_ID.Split(',');

                foreach (var modulo in arregloModulos)
                {
                    foreach (var turno in arregloTurnos)
                    {
                        MODULOS_TURNOS mod_tur = new MODULOS_TURNOS();
                        mod_tur.CONDICION = "C";
                        mod_tur.ESTADO = 1;
                        mod_tur.MODULO_ID = decimal.Parse(modulo);
                        mod_tur.TURNO_ID = decimal.Parse(turno);
                        mod_tur.USUARIO_ID = modulo_turno.USUARIO_ID;
                        mod_tur.NRO_CLASES = modulo_turno.NRO_CLASES;

                        unidad.RepositorioModulosTurnos.Add(mod_tur);
                    }
                }
                
                unidad.Save();
                return true;
            }
            catch
            {
                return false;
            }
        }
        
        public bool Modificar(MODULOS_TURNOS entity)
        {
            try
            {
                entity.CONDICION = "A";
                unidad.RepositorioModulosTurnos.Modify(entity);
                unidad.Save();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public List<object> Eliminar(MODULOS_TURNOS entity)
         {
             List<object> lista = new List<object>();
            try
            {
                lista.Add(new
                {
                    ok = 1,
                    msg = "Módulo - Turno Eliminado"
                });
                entity.ESTADO = 0;
                entity.CONDICION = "E";
                unidad.RepositorioModulosTurnos.Modify(entity);
                unidad.Save();
                return lista;
            }
            catch
            {
                lista.Clear();
                lista.Add(new
                {
                    ok = 0,
                    msg = "Error Eliminando Módulo - Turno"
                });
                return lista;
            }
         }

        public object ObtenerDataGrid( string sidx, string sord, int page, int rows, string filters, string CursoId)
        {
            int totalPages = 0;
            int totalRecords = 0;
            IEnumerable<MODULOS_TURNOS> modulosTurnos;
            
            modulosTurnos = unidad.RepositorioModulosTurnos.GetAll().Where(u => u.ESTADO == 1 || (u.ESTADO == 0 && u.CONDICION != "E"));
            List<MODULOS_TURNOS> lista = new List<MODULOS_TURNOS>();

            if (CursoId != "")
            {
                decimal curso = decimal.Parse(CursoId);
                foreach (var algo in modulosTurnos.Where(e => e.MODULOS.CURSOS_MODULOS.Any(u => u.CURSO_ID == curso)).ToList())
                {
                    lista.Add(algo);
                }
            }
            IQueryable<MODULOS_TURNOS> otraLista = JqGrid<MODULOS_TURNOS>.GetFilteredContent(sidx, sord, page, rows, filters, lista.AsQueryable(), ref totalPages, ref totalRecords);
            var rowsModel = (
                from moduloTurno in otraLista.ToList()
                select new
                {
                    i = moduloTurno.MODULO_ID,
                    cell = new string[] {
                           moduloTurno.MODULO_ID.ToString(),
                           moduloTurno.TURNO_ID.ToString(),
                           moduloTurno.MODULOS.NOMBRE,
                           moduloTurno.TURNOS.NOMBRE,
                           moduloTurno.NRO_CLASES.ToString(),
                           (moduloTurno.ESTADO == 1) ? "Activo" : "Inactivo",
                            "<a title=\"Editar\" href=\"/ModulosTurnos/Editar?modulo_id="+ moduloTurno.MODULO_ID+"&turno_id="+moduloTurno.TURNO_ID+"\"><span id=\""+moduloTurno.MODULO_ID+"\" class=\"ui-icon ui-icon-pencil\"></a>",                          
                            "<span id=\""+moduloTurno.MODULO_ID+"|"+moduloTurno.TURNO_ID+"\"  class=\"ui-icon ui-icon-close\" ></span>" }
                }).ToArray();
            return JqGrid<MODULOS_TURNOS>.SetJsonData(totalPages, totalRecords, page, rowsModel);
        }


        public MODULOS_TURNOS ObtenerPorClave(string id)
        {
            try
            {
                return unidad.RepositorioModulosTurnos.GetById(id);
                
            }
            catch 
            {
                return null;
            }
        }

        public MODULOS_TURNOS ObtenerPorClave(decimal modulo_id, decimal turno_id)
        {
            return ObtenerTodos().Where(mt => mt.MODULO_ID == modulo_id && mt.TURNO_ID == turno_id).FirstOrDefault();
        }

       
        public IEnumerable<MODULOS_TURNOS> ObtenerTodos()
        {
            try
            {
                IEnumerable<MODULOS_TURNOS> modulos = unidad.RepositorioModulosTurnos.GetAll().Where(u=>u.ESTADO==1);
                if (modulos == null)
                    return Enumerable.Empty<MODULOS_TURNOS>();
                else
                    return modulos;
            }
            catch  {
                return Enumerable.Empty<MODULOS_TURNOS>();
            }
        }

        public bool EsDuplicado(decimal modulo_id, decimal turno_id)
        {
            return ObtenerPorClave(modulo_id, turno_id) != null;
        }
    }
}
