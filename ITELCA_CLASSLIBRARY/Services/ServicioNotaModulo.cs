﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using System.Data.Entity;
using ITELCA_CLASSLIBRARY.Models;
using ITELCA_CLASSLIBRARY.Repositories;
using ITELCA_CLASSLIBRARY.CustomClasses;
using System.Collections;
using System.Web;
using UmbrellaClassLibrary.CustomClasses;

namespace ITELCA_CLASSLIBRARY.Services
{
     public class ServicioNotaModulo
    {
        private UnitOfWork unidad;
        public ServicioNotaModulo()
        {
            this.unidad = new UnitOfWork();
        }
        public bool Guardar(NOTASMODULO notaModulo)
        {
            try
            {
                unidad.RepositorioNotaModulo.Add(notaModulo);
                unidad.Save();
                return true;
            }
            catch
            {
                return false;
            }
        }
        public bool Modificar(NOTASMODULO notaModulo)
        {
            try
            {

                unidad.RepositorioNotaModulo.Modify(notaModulo);
                unidad.Save();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public NOTASMODULO ObtenerPorClave(decimal alumnoId,decimal matriculaModuloId)
        {
            try
            {
                return unidad.RepositorioNotaModulo.GetAll().Where(u => u.ALUMNO_ID == alumnoId &&
                                                                       u.MATRICULAMODULO_ID==matriculaModuloId).FirstOrDefault();

            }
            catch
            {
                return null;
            }
        }
   
         public List<object> ObtenerTablaNota(decimal idOfertaModulo){
              List<object> tablaNota=new List<object>();
             try{
                 IEnumerable<MATRICULASMODULOS> lista = unidad.RepositorioMatriculaModulo.GetAll().Where(u => u.OFERTA_MODULO_ID == idOfertaModulo);

                 if (lista != null)
                 {
                     foreach (var ope in lista)
                     {
                         if (ope.NOTASMODULO.Count>0)
                             tablaNota.Add(new
                             {
                                 tipo = "detalle",
                                 id = ope.ALUMNO_ID + "_" + ope.MATRICULAMODULO_ID,
                                 nombre = ope.MATRICULASCURSOS.USUARIOS.NOMBRE + " " + ope.MATRICULASCURSOS.USUARIOS.APELLIDO,
                                 nota = ope.NOTASMODULO.FirstOrDefault().NOTA
                             });
                         else
                             tablaNota.Add(new
                             {
                                 tipo = "detalle",
                                 id = ope.ALUMNO_ID + "_" + ope.MATRICULAMODULO_ID,
                                 nombre = ope.MATRICULASCURSOS.USUARIOS.NOMBRE + " " + ope.MATRICULASCURSOS.USUARIOS.APELLIDO,
                                 nota = ""
                             });

                     }
                    }
                    return tablaNota;
                }catch{
                    return null;
                }
         }
        public List<DateTime> ObtenerFechasClase(decimal idAlumno,decimal matriculaModuloId,decimal idModulo) {

            try {
                List<DateTime> lista=new List<DateTime>();
                 IEnumerable<ASISTENCIAS> aux= unidad.RepositorioAsistencias.GetAll().Where(u => u.ALUMNO_ID == idAlumno &&
                                                                                           u.MATRICULAMODULO_ID== matriculaModuloId &&
                                                                                           (u.FECHA_CLASE >= DateTime.Today
                                                                                           )).Take(5).OrderBy(u => u.FECHA_CLASE);
                 if (aux != null)
                     lista = aux.Select(u => u.FECHA_CLASE).ToList();
                 return lista;
            }
            catch { return null; }
        }  
        public List<DateTime> ObtenerListaClases(OFERTASMODULOS ofertaModulo) {
            List<DateTime> lista = new List<DateTime>();
            //fechaInicio.
            if (ofertaModulo.FECHA_INICIO != null && ofertaModulo.FECHA_FIN != null)
            {
                DateTime fechaIni = ofertaModulo.FECHA_INICIO;
                long idModulo = (long)ofertaModulo.MODULO_ID;
                long idturno = (long)ofertaModulo.TURNO_ID;
                MODULOS modulo = new ServicioModulo().ObtenerPorClave(idModulo);
                TURNOS turno = new ServicioTurno().ObtenerPorClave(idturno);
                MODULOS_TURNOS mt = new ServicioModuloTurno().ObtenerPorClave(modulo.MODULO_ID, turno.TURNO_ID);
                
                decimal cantidadClases = (mt!= null) ? mt.NRO_CLASES : 0;
                decimal claseEncontrada = 0M;
                int cont = 0;
                bool enc = false;
                int diaSemana = 0;
                int diaAux = 0;
                List<DIAClASE> list = new List<DIAClASE>();
                foreach (var ope in turno.DIAS_TURNOS)
                    list.Add(new DIAClASE { dia = ope.DIAS.NOMBRE, periodo = ope.PERIODICIDAD, habilitado = 1 });

                //fechaIni.ToString("dddd");//DIA DE MI PRIMERA CLASE
                DateTime fechaAux = fechaIni;
                ServicioFeriado feriado = new ServicioFeriado();
                while (cantidadClases > claseEncontrada)
                {
                    string dia = EliminaAcentos(fechaAux.ToString("dddd"));//DIA DE MI PRIMERA CLASE
                    enc = false;
                    if (feriado.VerificarFeriado(fechaAux))
                        foreach (var ope in list)
                        {
                            if (dia.ToLower() == ope.dia.ToLower())
                            {
                                if (ope.periodo > 7)
                                {
                                    double resp = fechaAux.Subtract(fechaIni).TotalDays;
                                    if (resp < 7)//primera clase quincenal
                                    {
                                        claseEncontrada++;
                                        lista.Add(fechaAux);
                                    }
                                    else
                                    {
                                        int diasTranscurridos = fechaAux.DayOfYear - diaSemana;
                                        if (diasTranscurridos == 14)
                                            diaAux = fechaAux.DayOfYear;
                                        if (diasTranscurridos >= 14 && fechaAux.DayOfYear - diaSemana < 21)
                                        {
                                            lista.Add(fechaAux);
                                            claseEncontrada++;
                                        }

                                        if (diasTranscurridos == 20)
                                            diaSemana = diaAux + 1;
                                    }
                                }
                                else
                                {
                                    claseEncontrada++;
                                    lista.Add(fechaAux);
                                }
                                enc = true;
                            }
                            if (enc)
                                break;
                        }
                    if (cantidadClases != claseEncontrada)
                    {
                        fechaAux = fechaAux.AddDays(1);
                        cont++;
                    }
                }
               
            }
            return lista;
        }
        public static string EliminaAcentos(string texto)
        {
            if (string.IsNullOrEmpty(texto))
                return texto;

            byte[] tempBytes = System.Text.Encoding.GetEncoding("ISO-8859-8").GetBytes(texto);
            return System.Text.Encoding.UTF8.GetString(tempBytes);
        }
       
    }
    
}
