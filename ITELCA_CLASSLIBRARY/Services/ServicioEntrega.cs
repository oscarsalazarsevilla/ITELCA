﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using System.Data.Entity;
using ITELCA_CLASSLIBRARY.Models;
using ITELCA_CLASSLIBRARY.Repositories;
using ITELCA_CLASSLIBRARY.CustomClasses;
using System.Collections;
using System.Web;
using UmbrellaClassLibrary.CustomClasses;
using System.Data.Entity.Validation;


namespace ITELCA_CLASSLIBRARY.Services
{
    public class ServicioEntrega
    {
        private UnitOfWork unidad;
        public ServicioEntrega()
        {
            this.unidad = new UnitOfWork();
        }
        public bool Guardar(ENTREGAS entrega)
        {
            try
            {
                unidad.RepositorioEntrega.Add(entrega);
                unidad.Save();

                List<ENTREGAS> entregas = new List<ENTREGAS>();
                entregas.Add(entrega);

                // Hacemos la conexion con la interfaz de peoplesoft
                if (new InterfazPeople().IngresarEntregaProductos(entregas))
                    return true;
                else
                    return false;

                return true;
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                return false;
            }
        }
        public bool Modificar(ENTREGAS entrega)
        {
            try
            {
                unidad.RepositorioEntrega.Modify(entrega);
                unidad.Save();
                return true;
            }
            catch
            {
                return false;
            }
        }


        public List<object> Eliminar(decimal id)
        {
            List<object> lista = new List<object>();
            try
            {
                //var aux = unidad.RepositorioEntrega.GetById((int)id);
                if (new InterfazPeople().DevolverProductos(id))
                {
                    lista.Add(new
                    {
                        ok = 1,
                        msg = "Devolución ingresada"
                    });
                    //unidad.RepositorioEntrega.Delete(aux);
                    //unidad.Save();
                    return lista;
                }
                else
                {
                    lista.Add(new
                    {
                        ok = 0,
                        msg = "Entrega no encontrada"
                    });
                    return lista;
                }
            }
            catch
            {
                lista.Clear();
                lista.Add(new
                {
                    ok = 0,
                    msg = "Error devolviendo la entrega"
                });
                return lista;
            }

        }

        public object ObtenerDataGrid(string sidx, string sord, int page, int rows, string filters)
        {
            int totalPages = 0;
            int totalRecords = 0;
            IEnumerable<ENTREGAS> entregas;
            entregas = unidad.RepositorioEntrega.GetAll();
            var rowsModel = (
                from entrega in entregas.ToList()
                select new
                {
                    i = entrega.DETALLERECIBO_ID + "=" + entrega.SEDE_ID,
                    cell = new string[] {
                           entrega.SEDES.NOMBRE,
                           entrega.DETALLESRECIBOS.PRODUCTOS.DESCR,
                           entrega.CANTIDAD.ToString(),
                           entrega.USUARIOS.NOMBRE,
                           entrega.FECHA_ENTREGA.ToString("dd/MM/yyyy"),
                            "<span title=\"Devolución\" id=\""+entrega.DETALLERECIBO_ID+"\"  class=\"ui-icon ui-icon-circle\" ></span>" }
                }).ToArray();
            return JqGrid<ENTREGAS>.SetJsonData(totalPages, totalRecords, page, rowsModel);
        }

        public ENTREGAS ObtenerPorClave(int id)
        {
            try
            {
                return unidad.RepositorioEntrega.GetById(id);

            }
            catch
            {
                return null;
            }
        }

        public ENTREGAS ObtenerPorClave(decimal detalle_recibo, decimal sede_id)
        {
            try
            {
                return ObtenerTodos().Where(e => e.SEDE_ID == sede_id && e.DETALLERECIBO_ID == detalle_recibo).FirstOrDefault();

            }
            catch
            {
                return null;
            }
        }

        public IEnumerable<ENTREGAS> ObtenerTodos()
        {
            try
            {
                IEnumerable<ENTREGAS> entreg = unidad.RepositorioEntrega.GetAll();
                if (entreg == null)
                    return Enumerable.Empty<ENTREGAS>();
                else
                    return entreg;
            }
            catch
            {
                return Enumerable.Empty<ENTREGAS>();
            }
        }

    }
}
