﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using System.Data.Entity;
using ITELCA_CLASSLIBRARY.Models;
using ITELCA_CLASSLIBRARY.Repositories;
using ITELCA_CLASSLIBRARY.CustomClasses;
using System.Collections;
using System.Web;
using UmbrellaClassLibrary.CustomClasses;
using System.Data.Entity.Validation;


namespace ITELCA_CLASSLIBRARY.Services
{
     public class ServicioOfertasModulos
    {
        private UnitOfWork unidad;
        public ServicioOfertasModulos()
        {
            this.unidad = new UnitOfWork();
        }
        public bool Guardar(OFERTASMODULOS ofertaModulos)
        {
            try
            {
                
                unidad.RepositorioOfertasModulos.Add(ofertaModulos);
                
                unidad.Save();
                return true;
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                return false;
            }
        }
        public bool Modificar(OFERTASMODULOS ofertaModulos)
        {
            try
            {

                unidad.RepositorioOfertasModulos.Modify(ofertaModulos);
                unidad.Save();
                return true;
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                return false;
            }
        }


        public List<object> Eliminar(decimal id)
        {
            List<object> lista = new List<object>();
            try
            {
                var aux = unidad.RepositorioOfertasModulos.GetById((int)id);
                if(aux!=null){
                     if (aux.MATRICULASMODULOS != null && aux.MATRICULASMODULOS.Count() > 0)
                    {
                        lista.Add(new
                        {
                            ok = 0,
                            msg = "Modulo Ya Tiene Personas Inscritas, No se puede Eliminar"
                        });

                    }else{
                         lista.Add(new
                        {
                            ok = 1,
                            msg = "Oferta Modulo Eliminado"
                        });
                        unidad.RepositorioOfertasModulos.Delete(aux);
                        unidad.Save();
                    }
                }
                else
                {
                    lista.Add(new
                    {
                        ok = 0,
                        msg = "Oferta Modulo No encontrado"
                    });
                }
                return lista;
            }
            catch
            {
                lista.Clear();
                lista.Add(new
                {
                    ok = 0,
                    msg = "Error Eliminando OFERTASMODULOS"
                });
                return lista;
            }

        }

        public object ObtenerDataGrid( decimal id,string profesorId,string sidx, string sord, int page, int rows, string filters)
        {
            int totalPages = 0;
            int totalRecords = 0;
            IEnumerable<OFERTASMODULOS> ofertasModulos;
            DateTime fechaFiltro = DateTime.Now.AddMonths(-2);
            if (profesorId != "Todos") {
                decimal profe_id = decimal.Parse(profesorId);
                ofertasModulos = ObtenerTodos().Where(u => u.OFERTA_CURSO_ID == id && u.PROFESOR_ID==profe_id && u.FECHA_FIN >= fechaFiltro).OrderBy(u => u.CURSOS_MODULOS.MODULOS.NOMBRE);
            }else
                ofertasModulos = ObtenerTodos().Where(u=>u.OFERTA_CURSO_ID==id && u.FECHA_FIN>=fechaFiltro).OrderBy( u => u.CURSOS_MODULOS.MODULOS.NOMBRE);
            ofertasModulos = JqGrid<OFERTASMODULOS>.GetFilteredContent(sidx, sord, page, rows, filters, ofertasModulos.AsQueryable(), ref totalPages, ref totalRecords);
          
            var rowsModel = (
                from oferta in ofertasModulos.ToList()
                select new
                {
                    i = oferta.OFERTA_MODULO_ID,
                    cell = new string[] {
                           oferta.OFERTA_MODULO_ID.ToString(),
                           oferta.SEDES.NOMBRE,
                           oferta.TURNOS.NOMBRE,
                           oferta.AULAS.NOMBRE.ToString(),
                           oferta.CURSOS_MODULOS.MODULOS.NOMBRE,
                           oferta.FECHA_INICIO!=null?oferta.FECHA_INICIO.ToShortDateString():"",
                           oferta.FECHA_FIN.HasValue?oferta.FECHA_FIN.Value.ToShortDateString():"",
                           oferta.MATRICULA_MINIMA.ToString(),
                           oferta.USUARIOS.NOMBRE+" "+oferta.USUARIOS.APELLIDO,
                            "<span id=\""+oferta.OFERTA_MODULO_ID+"\"  class=\"ui-icon ui-icon-close\" ></span>" }
                }).ToArray();
            return JqGrid<OFERTASMODULOS>.SetJsonData(totalPages, totalRecords, page, rowsModel);
        }

        public OFERTASMODULOS ObtenerPorClave(long id)
        {
            try
            {
                return unidad.RepositorioOfertasModulos.GetById(id);
                
            }
            catch 
            {
                return null;
            }
        }

       
        public IEnumerable<OFERTASMODULOS> ObtenerTodos()
        {
            try
            {
                IEnumerable<OFERTASMODULOS> ofertaModulos = unidad.RepositorioOfertasModulos.GetAll();
                if (ofertaModulos == null)
                    return Enumerable.Empty<OFERTASMODULOS>();
                else
                    return ofertaModulos;
            }
            catch  {
                return Enumerable.Empty<OFERTASMODULOS>();
            }
        }
        public string ValidarModulo(OFERTASMODULOS ofertaModulos)
        {
            try
            {

                IEnumerable<OFERTASMODULOS> modulo = unidad.RepositorioOfertasModulos.GetAll().Where(u => u.MODULO_ID == ofertaModulos.MODULO_ID &&
                                                                          u.OFERTA_CURSO_ID == ofertaModulos.OFERTA_CURSO_ID &&
                                                                          ((u.FECHA_INICIO <= ofertaModulos.FECHA_INICIO &&
                                                                          u.FECHA_FIN >= ofertaModulos.FECHA_INICIO) ||
                                                                          (u.FECHA_INICIO <= ofertaModulos.FECHA_FIN &&
                                                                          u.FECHA_FIN >= ofertaModulos.FECHA_FIN)));
                string cadena="";
                if (modulo == null)
                    return cadena;
                else
                {
                    ServicioModulo servicio = new ServicioModulo();
                    foreach (var ope in modulo)
                    {
                        cadena += servicio.ObtenerPorClave(long.Parse(ope.MODULO_ID.Value.ToString())).NOMBRE+"  ";
                    }
                    return cadena;
                }   
                        


            }
            catch (DbEntityValidationException e)
            {
                
                return "";
            }
        }
         public List<object> ObtenerProximoModulo(long cursoId, long sedeId,long turnoId,DateTime fechaFiltro){
             List<object> lista = new List<object>();
             /*OFERTASCURSOS ofertaCurso = unidad.RepositorioOfertaCurso.GetAll().Where(u => u.CURSO_ID == cursoId &&
                                                                                      u.SEDE_ID == sedeId &&
                                                                                      u.OFERTASMODULOS.Any(a=>a.FECHA_INICIO>=DateTime.Today)).FirstOrDefault();*/
             OFERTASCURSOS ofertaCurso = unidad.RepositorioOfertaCurso.GetAll().Where(u => u.CURSO_ID == cursoId &&
                                                                                      u.SEDE_ID == sedeId &&
                                                                                      u.TURNO_ID == turnoId).FirstOrDefault();
             if (ofertaCurso != null)
             {
                 IEnumerable<OFERTASMODULOS> ofertasModulos = ofertaCurso.OFERTASMODULOS;
                 if (ofertasModulos != null)
                 {
                     //if (ofertaCurso.CURSOS.DURACION == "C")
                     //    if (ofertasModulos.Select(u => u.TURNO_ID == turnoId).Count() > 0)
                     //        ofertasModulos = ofertasModulos.OrderBy(u => u.CURSOS_MODULOS.MODULOS.ORDEN).Take(1);
                     //    else
                     //        ofertasModulos = null;

                     //else
                     //{
                     //    if (ofertasModulos.Select(u => u.TURNO_ID == turnoId).Count() > 0)
                     //        ofertasModulos = ofertasModulos.Where(u => u.TURNO_ID == turnoId).OrderBy(u => u.FECHA_INICIO);
                     //}

                     //if(ofertasModulos!=null)
                         foreach (var ope in ofertasModulos.Where(u => u.FECHA_INICIO >= fechaFiltro).OrderBy(u=>u.FECHA_INICIO))
                         {
                             lista.Add(new
                             {
                                 inicio = ope.FECHA_INICIO.ToShortDateString(),
                                 fin = ope.FECHA_FIN.HasValue ? ope.FECHA_FIN.Value.ToShortDateString() : "",
                                 nombre = ope.CURSOS_MODULOS.MODULOS.NOMBRE,
                                 turno = ope.TURNOS.NOMBRE,
                                 ofertaModuloId = ope.OFERTA_MODULO_ID,
                                 profesor=ope.USUARIOS.NOMBRE+" "+ope.USUARIOS.APELLIDO,
                                 capacidad=ope.MATRICULA_MINIMA,
                                 inscritos=ope.MATRICULASMODULOS.Count()
                             });
                         }
                 }
             }
                 return lista;
            
         }

         //Obtener Modulos por Oferta Cursos iniciados hace 4 meses.
         public List<object> ObtenerModulosNoInscritos(long cursoId, long sedeId, long turnoId,long alumnoId, long sedeIdNueva, long turnoIdNueva)
         {
            int seisMesesAntes = DateTime.Now.AddMonths(-4).Month;
             
             List<object> lista = new List<object>();
             OFERTASCURSOS ofertaCurso = unidad.RepositorioOfertaCurso.GetAll().Where(u => u.CURSO_ID == cursoId &&
                                                                                      u.SEDE_ID == sedeId && u.TURNO_ID == turnoId).FirstOrDefault();
             OFERTASCURSOS ofertaCursoNuevo = unidad.RepositorioOfertaCurso.GetAll().Where(u => u.CURSO_ID == cursoId &&
                                                                                     u.SEDE_ID == sedeIdNueva && u.TURNO_ID == turnoIdNueva).FirstOrDefault();

             
             if (ofertaCurso != null)
             {
                 MATRICULASCURSOS matriculaCursos = unidad.RepositorioMatriculaCurso.GetAll().Where(u => u.OFERTA_CURSO_ID == ofertaCurso.OFERTA_CURSO_ID && u.ALUMNO_ID == alumnoId).FirstOrDefault();

                 IEnumerable<decimal> matriculaModulo = unidad.RepositorioMatriculaModulo.GetAll().Where(u => u.MATRICULA_CURSO_ID == matriculaCursos.MATRICULA_CURSO_ID && u.CONDICION == "P").Select(u => u.MODULO_ID);


                 IEnumerable<OFERTASMODULOS> ofertasModulos = unidad.RepositorioOfertasModulos.GetAll().Where( u => u.CURSO_ID == cursoId && u.SEDE_ID == sedeIdNueva && u.TURNO_ID == turnoIdNueva);
                 //IEnumerable<OFERTASMODULOS> ofertasModulos = ofertaCursoNuevo.OFERTASMODULOS;
                 if (ofertasModulos != null)
                 {
                     foreach (var ope in ofertasModulos.Where(u => !(matriculaModulo.Contains(decimal.Parse(u.MODULO_ID.ToString())))).OrderBy( u => u.CURSOS_MODULOS.MODULOS.NOMBRE))
                     {


                         lista.Add(new
                         {
                            key = ope.OFERTA_MODULO_ID.ToString(),
                            value = ope.CURSOS_MODULOS.MODULOS.NOMBRE,
                            fecha_inicio = ope.FECHA_INICIO.ToShortDateString()

                         });
                   
                        
                     }

                 }
             }
             return lista;

         }

         public List<object> ObtenerObjetoPorId(long id)
         {
             List<object> lista = new List<object>();
             OFERTASMODULOS ope = unidad.RepositorioOfertasModulos.GetById(id);
             if (ope != null)
             {
                     lista.Add(new
                     {
                         curso=ope.CURSOS_MODULOS.CURSOS.NOMBRE,
                         inicio = ope.FECHA_INICIO.ToShortDateString(),
                         fin = ope.FECHA_FIN.HasValue ? ope.FECHA_FIN.Value.ToShortDateString() : "",
                         sede=ope.SEDES.NOMBRE,
                         modulo = ope.CURSOS_MODULOS.MODULOS.NOMBRE,
                         turno = ope.TURNOS.NOMBRE,
                         ofertaModuloId = ope.OFERTA_MODULO_ID,
                     });
                 
             }
             return lista;

         }
         public IEnumerable<OFERTASMODULOS> ObtenerProximosModulos(long idOfertaModulo)
         {
             try
             {
                 OFERTASMODULOS ope = unidad.RepositorioOfertasModulos.GetById(idOfertaModulo);
                 return unidad.RepositorioOfertasModulos.GetAll().Where(u=>u.FECHA_INICIO>ope.FECHA_INICIO && 
                                                                           u.CURSO_ID==ope.CURSO_ID &&
                                                                           u.SEDE_ID==ope.SEDE_ID &&
                                                                           u.TURNO_ID==ope.TURNO_ID &&
                                                                           u.MODULO_ID!=ope.MODULO_ID).OrderBy(u=>u.FECHA_INICIO);
             }
             catch
             {
                 return null;
             }
         }

         public List<object> ObtenerTodosModulosPorOferta(decimal idOfertaCurso)
         {
             List<object> lista = new List<object>();
             try
             {

                 IEnumerable<OFERTASMODULOS> oferta = unidad.RepositorioOfertasModulos.GetAll().Where(u => u.OFERTA_CURSO_ID == idOfertaCurso && u.MATRICULASMODULOS.Count()>0);
                 if (oferta != null) {
                     foreach (var ope in oferta) { 
                        lista.Add(new
                        {
                            id = ope.OFERTA_MODULO_ID,
                            nombre = ope.CURSOS_MODULOS.MODULOS.NOMBRE
                        });
                     }
                 }
                 return lista;
             }
             catch
             {
                 return null;
             }
         }

         public Dictionary<string, string> ObtenerProfesoresPorOfertaCurso(decimal idOfertaCurso)
         {

             Dictionary<string, string> lista = new Dictionary<string, string>();
             ServicioUsuario _serviciousuario = new ServicioUsuario();
             try
             {                 
                 IEnumerable<decimal> oferta = unidad.RepositorioOfertasModulos.GetAll().Where(u => u.OFERTA_CURSO_ID == idOfertaCurso).Select(u => u.PROFESOR_ID).Distinct();
                 if (oferta != null)
                 {
                     lista.Add("Todos","Todos");
                     foreach (var profesor in oferta)
                     {
                         USUARIOS profe = _serviciousuario.ObtenerPorClave(profesor);
                         lista.Add(profe.USUARIO_ID.ToString(), profe.NOMBRE + " " + profe.APELLIDO);
                        
                     }
                 }
                 return lista;
             }
             catch
             {
                 return null;
             }
         }


         public List<objeto> ObtenerCursosPorSedeEnOfertaModulo(decimal idSede, string idProfesor)
         {
             List<objeto> lista = new List<objeto>();
             try
             {
                 IEnumerable<OFERTASCURSOS> curso;
                 if (idProfesor == "NO")
                     curso = unidad.RepositorioOfertaCurso.GetAll().Where(u => u.OFERTASMODULOS.Any(a => u.SEDE_ID == idSede)).OrderBy(u => u.CURSOS.NOMBRE);
                 else
                 {
                     decimal prof = decimal.Parse(idProfesor);
                     curso = unidad.RepositorioOfertaCurso.GetAll().Where(u => u.OFERTASMODULOS.Any(a => a.SEDE_ID == idSede && a.PROFESOR_ID == prof));

                 } foreach (var ope in curso)
                 {
                     if (!lista.Any(u => u.key == ope.CURSO_ID.ToString()))
                     {
                         objeto obj = new objeto();
                         obj.key = ope.CURSO_ID.ToString();
                         obj.value = ope.CURSOS.NOMBRE;
                         lista.Add(obj);
                     }
                 }

                 return lista;
             }
             catch
             {
                 return lista;
             }

         }


         public List<object> ObtenerTurnoEnOfertaModulo(long idCurso, long idSede, string idProfesor)
         {
             List<object> lista = new List<object>();
             try
             {
                 IEnumerable<OFERTASCURSOS> curso;
                 if (idProfesor == "NO")
                     curso = unidad.RepositorioOfertaCurso.GetAll().Where(u => u.CURSO_ID == idCurso && u.SEDE_ID == idSede).OrderBy(o => o.TURNOS.NOMBRE);
                 else
                 {
                     decimal prof = decimal.Parse(idProfesor);
                     curso = unidad.RepositorioOfertaCurso.GetAll().Where(u => u.OFERTASMODULOS.Any(a => a.SEDE_ID == idSede && a.CURSO_ID == idCurso && a.PROFESOR_ID == prof));

                 } 

                 foreach (var ope in curso)
                     lista.Add(new
                     {
                         key = ope.TURNO_ID,
                         value = ope.TURNOS.NOMBRE
                     });

                 return lista;
             }
             catch
             {
                 return null;
             }
         }

         public List<object> ObtenerModulosEnOfertaModulo(decimal idOfertaCurso, string idProfesor)
         {
             List<object> lista = new List<object>();
             try
             {

                 IEnumerable<OFERTASMODULOS> oferta;
                
                 if (idProfesor == "NO")
                     oferta = unidad.RepositorioOfertasModulos.GetAll().Where(u => u.OFERTA_CURSO_ID == idOfertaCurso && u.MATRICULASMODULOS.Count() > 0);
                 else
                 {
                     decimal prof = decimal.Parse(idProfesor);
                     oferta = unidad.RepositorioOfertasModulos.GetAll().Where(u => u.OFERTA_CURSO_ID == idOfertaCurso && u.PROFESOR_ID == prof && u.MATRICULASMODULOS.Count() > 0);
                 } 
                 
                 if (oferta != null)
                 {
                     foreach (var ope in oferta)
                     {
                         lista.Add(new
                         {
                             id = ope.MODULO_ID,
                             nombre = ope.CURSOS_MODULOS.MODULOS.NOMBRE
                         });
                     }
                 }
                 return lista;
             }
             catch
             {
                 return null;
             }
         }
         public List<object> ObtenerProfesoresPorOfertaModulo(decimal idOfertaCurso, decimal modulo)
         {
             try
             {
                 List<object> lista = new List<object>();
                // OFERTASMODULOS ofertaModulo = unidad.RepositorioOfertasModulos.GetAll().Where( u => u.OFERTA_CURSO_ID == idOfertaCurso && u.MODULO_ID == modulo).FirstOrDefault();
                 var profesores = unidad.RepositorioOfertasModulos.GetAll().Where(u => u.MODULO_ID == modulo && u.OFERTA_CURSO_ID == idOfertaCurso);
                 foreach (var prof in profesores)
                 {
                     lista.Add(new
                     {
                         id = prof.PROFESOR_ID,
                         nombre = prof.USUARIOS.NOMBRE+" "+prof.USUARIOS.APELLIDO
                     });
                 }

                 return lista;
             
             }
             catch
             {
                 return null;
             }
         }
       

         public IEnumerable<OFERTASMODULOS> ObtenerTodosModulosPorOfertaPorProfesor(decimal idOfertaCurso, decimal idProfesor)
         {
             try
             {
                 return unidad.RepositorioOfertasModulos.GetAll().Where(u => u.OFERTA_CURSO_ID == idOfertaCurso &&
                                                                             u.PROFESOR_ID == idProfesor);
             }
             catch
             {
                 return null;
             }
         }
         public Dictionary<decimal, string> ObtenerDiccionarioModulosPorOferta(decimal idOfertaCurso)
         {
             Dictionary<decimal, string> lista = new Dictionary<decimal, string>();

             try
             {

                 IEnumerable<OFERTASMODULOS> oferta = unidad.RepositorioOfertasModulos.GetAll().Where(u => u.OFERTA_CURSO_ID == idOfertaCurso);
                 if (oferta != null)
                 {
                     foreach (var ope in oferta)
                     {
                         CURSOS_MODULOS modulo = ope.OFERTASCURSOS.CURSOS.CURSOS_MODULOS.Where(u => u.MODULO_ID == ope.MODULO_ID).FirstOrDefault();
                         lista.Add(ope.MODULO_ID.Value, modulo.MODULOS.NOMBRE);
                     }
                 }
                 return lista;
             }
             catch
             {
                 return null;
             }
         }

         public IEnumerable<OFERTASMODULOS> ObtenerListaModulosPorOferta(decimal idOfertaCurso)
         {
             
             return unidad.RepositorioOfertasModulos.GetAll().Where(u => u.OFERTA_CURSO_ID == idOfertaCurso);
                
         }

        public void ActualizarFechasProximosModulos(decimal idOfertaModulo,DateTime fechaInicio,DateTime fechaFin,decimal profesorId,decimal moduloInicialId){

                OFERTASMODULOS ofertaModulo=unidad.RepositorioOfertasModulos.GetById(long.Parse(idOfertaModulo.ToString()));
                //obtengo todos los modulos que comienzan despues
                IEnumerable<OFERTASMODULOS> listadoActualizar;
                DateTime[] vectorProximoInicio = ObtenerVectorFechaOferta(ofertaModulo.FECHA_INICIO, long.Parse(ofertaModulo.MODULO_ID.ToString()), long.Parse(ofertaModulo.TURNO_ID.ToString()), 0);
                    
                if (fechaInicio.Date != ofertaModulo.FECHA_INICIO.Date)
                {
                    ofertaModulo.FECHA_FIN =vectorProximoInicio[0];
                    //listadoActualizar = unidad.RepositorioOfertasModulos.GetAll().Where(u => u.FECHA_INICIO > fechaInicio && u.PROFESOR_ID == profesorId && ofertaModulo.OFERTA_CURSO_ID == u.OFERTA_CURSO_ID );
                    Modificar(ofertaModulo);
                }
                listadoActualizar = unidad.RepositorioOfertasModulos.GetAll().Where(u => u.FECHA_INICIO > fechaInicio && u.PROFESOR_ID == profesorId && ofertaModulo.OFERTA_CURSO_ID == u.OFERTA_CURSO_ID && ofertaModulo.OFERTA_MODULO_ID!=u.OFERTA_MODULO_ID);

                if (listadoActualizar != null && listadoActualizar.Count ()> 0) {
                    //obtengo la proxima clase despues del idOfertaModulo
                    //DateTime[] vectorProximoInicio=ObtenerVectorFechaOferta(ofertaModulo.FECHA_FIN.Value,long.Parse(ofertaModulo.MODULO_ID.ToString()),long.Parse(ofertaModulo.TURNO_ID.ToString()),1);
                    foreach (var modulo in listadoActualizar.OrderBy(u=>u.FECHA_INICIO)) {
                        modulo.FECHA_INICIO = vectorProximoInicio[1];
                        vectorProximoInicio = ObtenerVectorFechaOferta(vectorProximoInicio[1], long.Parse(modulo.MODULO_ID.ToString()), long.Parse(ofertaModulo.TURNO_ID.ToString()), 0);
                        modulo.FECHA_FIN = vectorProximoInicio[0];
                        Modificar(modulo);
                    }
                
                }   

         }
        public List<object> GenerarOfertaAutomatica(OFERTASMODULOS oferta)
        {
            List<object> lista = new List<object>();
            try
            {
               
                DateTime fechaInicioModulo = oferta.FECHA_INICIO;
                //if (unidad.RepositorioOfertasModulos.GetAll().Where(u => u.PROFESOR_ID == oferta.PROFESOR_ID  && u.OFERTA_CURSO_ID==oferta.OFERTA_CURSO_ID && u.FECHA_FIN < fechaInicioModulo).Count() > 0)
                //{
                    IEnumerable<MODULOS> modulos = unidad.RepositorioCursoModulo.GetAll().Where(u => u.CURSO_ID == oferta.CURSO_ID).Select(a => a.MODULOS).OrderBy(u => u.ORDEN);

                    if (modulos != null && modulos.Count() > 0)
                    {
                        foreach (var ope in modulos)
                        {
                            //obtengo la proxima el fin de la clase y la proxima clase
                            MODULOS_TURNOS mt = new ServicioModuloTurno().ObtenerPorClave(ope.MODULO_ID, oferta.TURNO_ID);
                            decimal cantidadClases = (mt != null) ? mt.NRO_CLASES : 1;
                            DateTime[] vectorProximoInicio = ObtenerVectorFechaOferta(fechaInicioModulo, long.Parse(ope.MODULO_ID.ToString()), long.Parse(oferta.TURNO_ID.ToString()), (int)cantidadClases);
                            //DateTime[] vectorProximoInicio = ObtenerVectorFechaOferta(fechaInicioModulo, long.Parse(ope.MODULO_ID.ToString()), long.Parse(oferta.TURNO_ID.ToString()), 10);
                            OFERTASMODULOS ofertaModulo = new OFERTASMODULOS();
                            ofertaModulo.FECHA_INICIO = fechaInicioModulo;
                            ofertaModulo.FECHA_FIN = vectorProximoInicio[0];
                            ofertaModulo.PROXIMO_INICIO = vectorProximoInicio[1];
                            ofertaModulo.MATRICULA_MINIMA = oferta.MATRICULA_MINIMA;
                            ofertaModulo.CURSO_ID = oferta.CURSO_ID;
                            ofertaModulo.AULA_ID = oferta.AULA_ID;
                            ofertaModulo.MODULO_ID = ope.MODULO_ID;
                            ofertaModulo.OFERTA_CURSO_ID = oferta.OFERTA_CURSO_ID;
                            ofertaModulo.PROFESOR_ID = oferta.PROFESOR_ID;
                            ofertaModulo.SEDE_ID = oferta.SEDE_ID;
                            ofertaModulo.TURNO_ID = oferta.TURNO_ID;
                            ofertaModulo.USUARIO_ID = oferta.USUARIO_ID;
                            fechaInicioModulo = vectorProximoInicio[1];
                            unidad.RepositorioOfertasModulos.Add(ofertaModulo);

                        }
                        unidad.Save();
                        lista.Add(new
                        {
                            msg = "Planificación Modulos Generada",
                            ok = 1
                        });
                    }
                    else
                    {
                        lista.Add(new
                        {
                            msg = "Curso sin Modulos Planificados",
                            ok = 0
                        });
                    }
                //}
                //else {
                //    lista.Add(new
                //    {
                //        msg = "Profesor Ya esta dando un modulo para la Fecha de Inicio Indicada",
                //        ok = 0
                //    });
                //}
            }
            catch {
                lista.Add(new
                {
                    msg = "Error Generando Oferta",
                    ok = 0
                });

                return lista;
            }
            return lista;
        }
         
         public List<object> CalcularFechaOferta(DateTime fechaIni, long idModulo,long idTurno) {
             List<object> objeto = new List<object>();
             try
             {
                
                 //le sumo 1 para obtener la siguiente clase
                 TURNOS turno = new ServicioTurno().ObtenerPorClave(idTurno);
                 int cantidadClases;
                 MODULOS modulo = new ServicioModulo().ObtenerPorClave(idModulo);
                 MODULOS_TURNOS mt = new ServicioModuloTurno().ObtenerPorClave(idModulo, idTurno);
                 if (mt != null)
                 {
                     ServicioFeriado feriado = new ServicioFeriado();
                     ServicioTurnoViernes servicioTurnopViernes = new ServicioTurnoViernes();
                     //cantidadClases = 10;
                     decimal claseEncontrada = 0M;
                     int cont = 0;
                     bool enc = false; bool encInicio = true;
                     int diaSemana = 0;
                     int diaAux = 0;
                     DateTime fechaInicio = DateTime.Now;
                     DateTime fechaFin = DateTime.Now;
                     List<DIAClASE> list = new List<DIAClASE>();
                     cantidadClases = (int)(((mt.NRO_CLASES != null) ? mt.NRO_CLASES : 0) + 1);

                     foreach (var ope in turno.DIAS_TURNOS)
                         list.Add(new DIAClASE { dia = ope.DIAS.NOMBRE, periodo = ope.PERIODICIDAD, habilitado = 1 });

                     //fechaIni.ToString("dddd");//DIA DE MI PRIMERA CLASE
                     DateTime fechaAux = fechaIni;

                     while (cantidadClases > claseEncontrada)
                     {
                         string dia = EliminaAcentos(fechaAux.ToString("dddd"));//DIA DE MI PRIMERA CLASE
                         enc = false;
                         if (feriado.VerificarFeriado(fechaAux))
                             foreach (var ope in list)
                             {
                                 if (dia.ToLower() == ope.dia.ToLower())
                                 {
                                     if (ope.periodo > 7)
                                     {
                                         if (dia.ToLower() == "viernes")
                                         {
                                             if (servicioTurnopViernes.ValidarViernes(fechaAux, idTurno))
                                                 claseEncontrada++;
                                         }
                                         else
                                         {
                                             double resp = fechaAux.Subtract(fechaIni).TotalDays;
                                             if (resp < 7)//primera clase quincenal
                                             {
                                                 claseEncontrada++;
                                                 diaSemana = (int)fechaAux.DayOfYear;
                                             }
                                             else
                                             {
                                                 int diasTranscurridos = fechaAux.DayOfYear - diaSemana;
                                                 if (diasTranscurridos == 14)
                                                     diaAux = fechaAux.DayOfYear;
                                                 if (diasTranscurridos >= 14 && fechaAux.DayOfYear - diaSemana < 21)
                                                     claseEncontrada++;
                                                 if (diasTranscurridos == 20)
                                                     diaSemana = diaAux + 1;
                                             }
                                         }
                                     }
                                     else
                                         claseEncontrada++;
                                     enc = true;
                                 }
                                 if (enc)
                                     break;
                             }
                         //guardo la fecha de inicio
                         if (cantidadClases - 1 == claseEncontrada && encInicio)
                         {
                             fechaInicio = fechaAux;
                             encInicio = false;
                         }
                         //guardo la fecha de fin
                         if (cantidadClases == claseEncontrada)
                         {
                             fechaFin = fechaAux;
                             break;
                         }
                         if (cantidadClases != claseEncontrada)
                         {
                             fechaAux = fechaAux.AddDays(1);
                             cont++;
                         }
                     }

                     objeto.Add(new
                     {
                         ok = 1,
                         inicio = fechaInicio.ToShortDateString(),
                         fin = fechaFin.ToShortDateString(),
                         msg = ""
                     });
                 }
                 else {
                     objeto.Add(new
                     {
                         ok = 0,
                         inicio ="",
                         fin = "",
                         msg = "Modulos Turno No se Encuentra configurado para ese modulo"
                     });
                 }
                 return objeto;

             }
             catch {
                 objeto.Clear();
                 objeto.Add(new
                 {
                     ok = 0,
                     inicio ="",
                     fin = "",
                     msg = "Error calculando la fecha de fin"
                 });

                 return objeto;
             }
            }

         public DateTime [] ObtenerVectorFechaOferta(DateTime fechaIni, long idModulo, long idTurno, int numeroClases)
         {
             DateTime [] objeto=new DateTime[2];
             //le sumo 1 para obtener la siguiente clase
             TURNOS turno = new ServicioTurno().ObtenerPorClave(idTurno);
             int cantidadClases;
             if (numeroClases == 0)
             {
                 MODULOS modulo = new ServicioModulo().ObtenerPorClave(idModulo);
                 MODULOS_TURNOS mt = new ServicioModuloTurno().ObtenerPorClave(modulo.MODULO_ID, turno.TURNO_ID);
                 cantidadClases = (int)((mt != null) ? mt.NRO_CLASES : 0) + 1;
                 //cantidadClases = (int)(modulo.NUMERO_CLASES + 1);
                 //cantidadClases = 10;
             }
             else
                 cantidadClases = numeroClases + 1;
             decimal claseEncontrada = 0M;
             int cont = 0;
             bool enc = false; bool encInicio = true;
             int diaSemana = 0;
             int diaAux = 0;
             DateTime fechaInicio = DateTime.Now;
             DateTime fechaFin = DateTime.Now;
             List<DIAClASE> list = new List<DIAClASE>();
             foreach (var ope in turno.DIAS_TURNOS)
                 list.Add(new DIAClASE { dia = ope.DIAS.NOMBRE, periodo = ope.PERIODICIDAD, habilitado = 1 });

             //fechaIni.ToString("dddd");//DIA DE MI PRIMERA CLASE
             DateTime fechaAux = fechaIni;
             ServicioFeriado feriado = new ServicioFeriado();
             ServicioTurnoViernes servicioTurnopViernes=new ServicioTurnoViernes();
             while (cantidadClases > claseEncontrada)
             {
                 string dia = EliminaAcentos(fechaAux.ToString("dddd"));//DIA DE MI PRIMERA CLASE
                 enc = false;
                 if (feriado.VerificarFeriado(fechaAux))
                     foreach (var ope in list)
                     {
                         if (dia.ToLower() == ope.dia.ToLower())
                         {
                             if (ope.periodo > 7)
                             {
                                if(dia.ToLower()=="viernes"){
                                        if(servicioTurnopViernes.ValidarViernes(fechaAux,idTurno))
                                            claseEncontrada++;
                                }else{
                                    double resp = fechaAux.Subtract(fechaIni).TotalDays;
                                    if (resp < 7)//primera clase quincenal
                                    {
                                        claseEncontrada++;
                                        diaSemana = (int)fechaAux.DayOfYear;
                                    }
                                    else
                                    {
                                        int diasTranscurridos = fechaAux.DayOfYear - diaSemana;
                                        if (diasTranscurridos == 14)
                                            diaAux = fechaAux.DayOfYear;
                                        if (diasTranscurridos >= 14 && fechaAux.DayOfYear - diaSemana < 21)
                                            claseEncontrada++;
                                        if (diasTranscurridos == 20)
                                            diaSemana = diaAux + 1;
                                    }
                                }
                             }
                             else
                                 claseEncontrada++;
                             enc = true;
                         }
                         if (enc)
                             break;
                     }
                 //guardo la fecha de inicio
                 if (cantidadClases - 1 == claseEncontrada && encInicio)
                 {
                     fechaInicio = fechaAux;
                     encInicio = false;
                 }
                 //guardo la fecha de fin
                 if (cantidadClases == claseEncontrada)
                 {
                     fechaFin = fechaAux;
                     break;
                 }
                 if (cantidadClases != claseEncontrada)
                 {
                     fechaAux = fechaAux.AddDays(1);
                     cont++;
                 }
             }

             objeto[0]=fechaInicio;
             objeto[1]=fechaFin;
             return objeto;
         }
             public static string EliminaAcentos(string texto)
            {
                if (string.IsNullOrEmpty(texto))
                    return texto;

                byte[] tempBytes = System.Text.Encoding.GetEncoding("ISO-8859-8").GetBytes(texto);
                return System.Text.Encoding.UTF8.GetString(tempBytes);
            }

             
        }
    }
