﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using System.Data.Entity;
using ITELCA_CLASSLIBRARY.Models;
using ITELCA_CLASSLIBRARY.Repositories;
using ITELCA_CLASSLIBRARY.CustomClasses;
using System.Collections;
using System.Web;
using UmbrellaClassLibrary.CustomClasses;


namespace ITELCA_CLASSLIBRARY.Services
{
     public class ServicioTipoAula
    {
        private UnitOfWork unidad;
        public ServicioTipoAula()
        {
            this.unidad = new UnitOfWork();
        }
        public bool Guardar(TIPOSAULAS tipoAula)
        {
            try
            {
                tipoAula.CONDICION = "C";
                unidad.RepositorioTipoAula.Add(tipoAula);
                unidad.Save();
                return true;
            }
            catch
            {
                return false;
            }
        }
        public bool Modificar(TIPOSAULAS entity)
        {
            try
            {
                entity.CONDICION = "A";
                unidad.RepositorioTipoAula.Modify(entity);
                unidad.Save();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public List<object> Eliminar(decimal tipoA)
        {
            List<object> lista = new List<object>();
            try
            {
                var aux = unidad.RepositorioTipoAula.GetById((int)tipoA);
                if (aux != null)
                {
                    lista.Add(new
                    {
                        ok = 1,
                        msg = "Tipo Aula Eliminada"
                    });
                    aux.CONDICION = "E";
                    aux.ESTADO = 0;
                    unidad.RepositorioTipoAula.Modify(aux);
                    unidad.Save();
                    return lista;
                }
                else
                {
                    lista.Add(new
                    {
                        ok = 0,
                        msg = "Tipo Aula No encontrada"
                    });
                    return lista;
                }
            }
            catch
            {
                lista.Clear();
                lista.Add(new
                {
                    ok = 0,
                    msg = "Error Eliminando Tipo Aula"
                });
                return lista;
            }

        }

        public object ObtenerDataGrid( string sidx, string sord, int page, int rows, string filters)
        {
            int totalPages = 0;
            int totalRecords = 0;
            IEnumerable<TIPOSAULAS> tipoAulas;
            tipoAulas = unidad.RepositorioTipoAula.GetAll().Where(u => u.ESTADO == 1 || (u.ESTADO == 0 && u.CONDICION != "E"));
            tipoAulas = JqGrid<TIPOSAULAS>.GetFilteredContent(sidx, sord, page, rows, filters, tipoAulas.AsQueryable(), ref totalPages, ref totalRecords);
          
            var rowsModel = (
                from tipoAula in tipoAulas.ToList()
                select new
                {
                    i = tipoAula.TIPO_AULA_ID,
                    cell = new string[] {
                           tipoAula.TIPO_AULA_ID.ToString(),
                           tipoAula.NOMBRE.ToString(),
                           (tipoAula.ESTADO == 1) ? "Activo" : "Inactivo",
                            "<a title=\"Editar\" href=\"/TipoAula/Editar/"+ tipoAula.TIPO_AULA_ID+"\"><span id=\""+tipoAula.TIPO_AULA_ID+"\" class=\"ui-icon ui-icon-pencil\"></a>",                          
                            "<span id=\""+tipoAula.TIPO_AULA_ID+"\"  class=\"ui-icon ui-icon-close\" ></span>" }
                }).ToArray();
            return JqGrid<TIPOSAULAS>.SetJsonData(totalPages, totalRecords, page, rowsModel);
        }

        public TIPOSAULAS ObtenerPorClave(int id)
        {
            try
            {
                return unidad.RepositorioTipoAula.GetById(id);
                
            }
            catch
            {
                return null;
            }
        }

       
        public IEnumerable<TIPOSAULAS> ObtenerTodos()
        {
            try
            {
                IEnumerable<TIPOSAULAS> tiposAulas= unidad.RepositorioTipoAula.GetAll().Where(u=>u.ESTADO==1);
                if (tiposAulas == null)
                    return Enumerable.Empty<TIPOSAULAS>();
                else
                    return tiposAulas;
            }
            catch {
                return Enumerable.Empty<TIPOSAULAS>();
            }
        }

        public bool Duplicado(TIPOSAULAS tipoAula)
        {
            try
            {
                if (unidad.RepositorioTipoAula.GetAll().Where(u => u.NOMBRE.ToLower() == tipoAula.NOMBRE.ToLower() &&
                                                                 u.TIPO_AULA_ID!=tipoAula.TIPO_AULA_ID && u.ESTADO==1).Count() > 0)
                    return true;
                else
                    return false;

            }
            catch 
            {
                return false;
            }
        }
      
    }
}
