﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using System.Data.Entity;
using ITELCA_CLASSLIBRARY.Models;
using ITELCA_CLASSLIBRARY.Repositories;
using ITELCA_CLASSLIBRARY.CustomClasses;
using System.Collections;
using System.Web;
using UmbrellaClassLibrary.CustomClasses;

namespace ITELCA_CLASSLIBRARY.Services
{
     public class ServicioDetalleRecibo
    {
        private UnitOfWork unidad;
        public ServicioDetalleRecibo()
        {
            this.unidad = new UnitOfWork();
        }
        public bool Guardar(DETALLESRECIBOS aula)
        {
            try
            {
                unidad.RepositorioDetallesRecibos.Add(aula);
                unidad.Save();
                return true;
            }
            catch
            {
                return false;
            }
        }
        public bool Modificar(DETALLESRECIBOS aula)
        {
            try
            {
                unidad.RepositorioDetallesRecibos.Modify(aula);
                unidad.Save();
                return true;
            }
            catch
            {
                return false;
            }
        }


        public List<object> Eliminar(decimal id)
        {
            List<object> lista = new List<object>();
            try
            {
                var aux = unidad.RepositorioDetallesRecibos.GetById((int)id);
                if (aux != null)
                {
                    lista.Add(new
                    {
                        ok = 1,
                        msg = "Aula Eliminada"
                    });
                    unidad.RepositorioDetallesRecibos.Modify(aux);
                    //unidad.RepositorioDetallesRecibos.Delete(aux);
                    unidad.Save();
                    return lista;
                }
                else
                {
                    lista.Add(new
                    {
                        ok = 0,
                        msg = "Aula No encontrada"
                    });
                    return lista;
                }
            }
            catch
            {
                lista.Add(new
                {
                    ok = 0,
                    msg = "Error Eliminando Aula"
                });
                return lista;
            }

        }

        /*public object ObtenerDataGrid( string sidx, string sord, int page, int rows, string filters)
        {
            int totalPages = 0;
            int totalRecords = 0;
            IEnumerable<DETALLESRECIBOS> aulas;
            aulas = unidad.RepositorioDetallesRecibos.GetAll().Where(u => u.ESTADO == 1 || (u.ESTADO == 0 && u.CONDICION != "E"));
            aulas = JqGrid<DETALLESRECIBOS>.GetFilteredContent(sidx, sord, page, rows, filters, aulas.AsQueryable(), ref totalPages, ref totalRecords);
            var rowsModel = (
                from aula in aulas.ToList()
                select new
                {
                    i = aula.AULA_ID,
                    cell = new string[] {
                           aula.AULA_ID.ToString(),
                           aula.NOMBRE,
                           aula.TIPOSDETALLESRECIBOS.NOMBRE,
                           aula.CODIGO,
                           aula.PISO.ToString(),
                           aula.CAPACIDAD.ToString(),
                           (aula.ESTADO == 1) ? "Activo" : "Inactivo",
                            "<a title=\"Editar\" href=\"/Aula/Editar/"+ aula.AULA_ID+"\"><span id=\""+aula.AULA_ID+"\" class=\"ui-icon ui-icon-pencil\"></a>",                          
                            "<span id=\""+aula.AULA_ID+"\"  class=\"ui-icon ui-icon-close\" ></span>" }
                }).ToArray();
            return JqGrid<DETALLESRECIBOS>.SetJsonData(totalPages, totalRecords, page, rowsModel);
        }*/

        public DETALLESRECIBOS ObtenerPorClave(int id)
        {
            try
            {
                return unidad.RepositorioDetallesRecibos.GetById(id);
                
            }
            catch 
            {
                return null;
            }
        }

       
        public IEnumerable<DETALLESRECIBOS> ObtenerTodos()
        {
            try
            {
                IEnumerable<DETALLESRECIBOS> sedes= unidad.RepositorioDetallesRecibos.GetAll();
                if (sedes == null)
                    return Enumerable.Empty<DETALLESRECIBOS>();
                else
                    return sedes;
            }
            catch {
                return Enumerable.Empty<DETALLESRECIBOS>();
            }
        }

        public Dictionary<decimal, string> ObtenerPendientesPorEntregar()
        {
            Dictionary<decimal, string> dic = new Dictionary<decimal, string>();
            List<DETALLESRECIBOS> lista = ObtenerTodos().Where(dr => dr.ENTREGAS.Count() <= 0 && 
                                                                    !string.IsNullOrEmpty(dr.SETID) &&
                                                                    !string.IsNullOrEmpty(dr.INV_ITEM_ID)).ToList();

            foreach (var detalle in lista)
            {
                dic.Add(detalle.DETALLERECIBO_ID, detalle.RECIBOS.NRORECIBO + " ---- " + detalle.PRODUCTOS.DESCR + " ---- " + detalle.CANTIDAD);
            }
            return dic;
        }
       
    }
}
