﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using System.Data.Entity;
using ITELCA_CLASSLIBRARY.Models;
using ITELCA_CLASSLIBRARY.Repositories;
using ITELCA_CLASSLIBRARY.CustomClasses;
using System.Collections;
using System.Web;
using UmbrellaClassLibrary.CustomClasses;


namespace ITELCA_CLASSLIBRARY.Services
{
     public class ServicioSolicitudes
    {
        private UnitOfWork unidad;
        public ServicioSolicitudes()
        {
            this.unidad = new UnitOfWork();
        }
        public string Guardar(SOLICITUDES solicitud)
        {
            try
            {
                solicitud.CONDICION = "C";
                solicitud.ESTADO_SOLICITUD = 0;
                solicitud.FECHA_CREACION = DateTime.Now;
                //Creamos el concepto a pagar por el estudiante
                CONCEPTOS_TIPOSSOLICITUDES ct = new ServicioConceptoTipoSolicitud().ObtenerPorTipoSolicitud(solicitud.TIPOSOLICITUD_ID.Value);
                TIPOSSOLICITUDES tipoSolicitud = new ServicioTipoSolicitud().ObtenerPorClave(int.Parse(solicitud.TIPOSOLICITUD_ID.Value.ToString()));
                if(ct!= null && tipoSolicitud != null){
                    CONCEPTOS_ESTUDIANTES ce = new CONCEPTOS_ESTUDIANTES();
                    ce.CONCEPTO_SOLICITUD_ID = ct.CONCEPTO_SOLICITUD_ID;
                    ce.MONTO = ct.MONTO;
                    ce.USUARIO_ID = solicitud.USUARIO_ID;
                    ce.CONDICION = "P";
                    ce.FECHA_VENCIMIENTO = DateTime.Now;
                    ce.DESCRIPCION = tipoSolicitud.NOMBRE + " SOLICITADA EL " + DateTime.Now.ToString("dd/MM/yyyy");
                    solicitud.CONCEPTOS_ESTUDIANTES.Add(ce);
                }else{
                    if(ct==null)
                        return "No hay Conceptos definidos para este tipo de solicitud.";
                    else
                        return "Este Tipo de Solicitud no esta definido.";
                }               
                unidad.RepositorioSolicitudes.Add(solicitud);
                unidad.Save();
                return "true";
            }
            catch
            {
                return "";
            }
        }

        public bool Modificar(SOLICITUDES solicitud)
        {
            try
            {

                unidad.RepositorioSolicitudes.Modify(solicitud);
                unidad.Save();
                return true;
            }
            catch
            {
                return false;
            }
        }


        public List<object> Eliminar(decimal id)
        {
            List<object> lista = new List<object>();
            try
            {
                var aux = unidad.RepositorioSolicitudes.GetById((int)id);
                if (aux != null)
                {
                    lista.Add(new
                    {
                        ok = 1,
                        msg = "Solicitud Eliminado"
                    });
                    unidad.RepositorioSolicitudes.Delete(aux);
                    unidad.Save();
                    return lista;
                }
                else
                {
                    lista.Add(new
                    {
                        ok = 0,
                        msg = "Solicitud No encontrado"
                    });
                    return lista;
                }
            }
            catch
            {
                lista.Clear();
                lista.Add(new
                {
                    ok = 0,
                    msg = "Error Eliminando Concepto"
                });
                return lista;
            }

        }

        public object ObtenerDataGrid( string sidx, string sord, int page, int rows, string filters)
        {
            int totalPages = 0;
            int totalRecords = 0;
            IEnumerable<SOLICITUDES> SOLICITUDES;
            SOLICITUDES = ObtenerTodos();
            ServicioUsuario _servicioUsuario= new ServicioUsuario();
            var rowsModel = (
                from solicitud in SOLICITUDES.ToList()
                select new
                {
                    i = solicitud.SOLICITUD_ID,
                    cell = new string[] {
                           solicitud.SOLICITUD_ID.ToString(),
                           solicitud.TIPOSSOLICITUDES.NOMBRE,
                           _servicioUsuario.ObtenerPorClave(solicitud.MATRICULASCURSOS.ALUMNO_ID).NRO_DOCUMENTO.ToString(),
                           _servicioUsuario.ObtenerPorClave(solicitud.MATRICULASCURSOS.ALUMNO_ID).NOMBRE.ToString() +" "+_servicioUsuario.ObtenerPorClave(solicitud.MATRICULASCURSOS.ALUMNO_ID).APELLIDO.ToString(),
                           solicitud.MATRICULASCURSOS.OFERTASCURSOS.SEDES.NOMBRE,
                           solicitud.FECHA_CREACION.ToString("dd/MM/yyyy"),
                           this.Estado(solicitud.ESTADO_SOLICITUD.ToString()),
                            "<a title=\"Editar\" href=\"/Solicitudes/Procesar/"+ solicitud.SOLICITUD_ID+"\"><span id=\""+solicitud.SOLICITUD_ID+"\" class=\"ui-icon ui-icon-check\"></a>",                          
                            "<span id=\""+solicitud.SOLICITUD_ID+"\" href=\"/Solicitudes/Eliminar/"+ solicitud.SOLICITUD_ID+"\" class=\"ui-icon ui-icon-close\" ></span>" }
                }).ToArray();
            return JqGrid<SOLICITUDES>.SetJsonData(totalPages, totalRecords, page, rowsModel);
        }

        public string Estado(string estado_solicitud)
        {
            if (estado_solicitud == "0")
                return "PENDIENTE POR PAGAR";
            else if (estado_solicitud == "1")
                return "PAGADA";
            else
                return "IMPRESA";
        }

        public SOLICITUDES ObtenerPorClave(int id)
        {
            try
            {
                return unidad.RepositorioSolicitudes.GetById(id);

            }
            catch
            {
                return null;
            }
        }


        public IEnumerable<SOLICITUDES> ObtenerTodos()
        {
            try
            {
                IEnumerable<SOLICITUDES> SOLICITUDES = unidad.RepositorioSolicitudes.GetAll();
                if (SOLICITUDES == null)
                    return Enumerable.Empty<SOLICITUDES>();
                else
                    return SOLICITUDES;
            }
            catch
            {
                return Enumerable.Empty<SOLICITUDES>();
            }
        }

        //public bool Duplicado(SOLICITUDES SOLICITUDES)
        //{
        //    try
        //    {
        //        if (unidad.RepositorioSolicitudes.GetAll().Where(u => u.CURSO_ID == SOLICITUDES.CURSO_ID && u.USUARIO_ID == u.USUARIO_ID &&
        //                                                         u.SOLICITUD_ID != SOLICITUDES.SOLICITUD_ID).Count() > 0)
        //            return true;
        //        else
        //            return false;

        //    }
        //    catch
        //    {
        //        return false;
        //    }
        //}


        public bool ValidarReimpresion(int solicitud_id, string login)
        {
            SOLICITUDES solicitud = this.ObtenerPorClave(solicitud_id);
            USUARIOS usuario = new ServicioUsuario().ObtenerPorLogin(login);
            int esAdmin = usuario.ROLES.Where(u => u.ROL_ID == 1).Count();
            if(solicitud.ESTADO_SOLICITUD == 2 && esAdmin > 0) // Si el Estado de la Solicitud es "IMPRESA" y el usuario es ADMIN
                return true; // Puedo ReImprimir

            return false; //False
        }
    }
}
