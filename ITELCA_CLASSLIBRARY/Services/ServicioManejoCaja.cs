﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using System.Data.Entity;
using ITELCA_CLASSLIBRARY.Models;
using ITELCA_CLASSLIBRARY.Repositories;
using ITELCA_CLASSLIBRARY.CustomClasses;
using System.Collections;
using System.Web;
using UmbrellaClassLibrary.CustomClasses;

namespace ITELCA_CLASSLIBRARY.Services
{
     public class ServicioManejoCaja
    {
        private UnitOfWork _unidad;
        
        public ServicioManejoCaja()
        {
            this._unidad = new UnitOfWork();
        }
        
         public bool Guardar(MANEJOCAJAS manejoCaja)
        {
            try
            {
                manejoCaja.CONDICION = "A";
                _unidad.RepositorioManejoCajas.Add(manejoCaja);
                _unidad.Save();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Modificar(MANEJOCAJAS manejoCaja)
        {
            try
            {
                if (manejoCaja.CONDICION == "C" && !manejoCaja.FECHA_CIERRE.HasValue)
                    manejoCaja.FECHA_CIERRE = DateTime.Now;
                _unidad.RepositorioManejoCajas.Modify(manejoCaja);
                _unidad.Save();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Cerrar(MANEJOCAJAS manejoCaja)
        {
            try
            {
                manejoCaja.FECHA_CIERRE = DateTime.Now;
                manejoCaja.CONDICION = "C";
                _unidad.RepositorioManejoCajas.Modify(manejoCaja);
                _unidad.Save();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public List<object> Eliminar(decimal id)
        {
            List<object> lista = new List<object>();
            try
            {
                var aux = _unidad.RepositorioManejoCajas.GetById((int)id);
                if (aux != null)
                {
                    lista.Add(new
                    {
                        ok = 1,
                        msg = "Manejo de caja eliminado"
                    });
                    _unidad.RepositorioManejoCajas.Delete(aux);
                    _unidad.Save();
                    return lista;
                }
                else
                {
                    lista.Add(new
                    {
                        ok = 0,
                        msg = "Registro de manejo de caja no encontrado"
                    });
                    return lista;
                }
            }
            catch
            {
                lista.Add(new
                {
                    ok = 0,
                    msg = "Error eliminando el registro de manejo de caja"
                });
                return lista;
            }

        }

        public object ObtenerDataGrid( string sidx, string sord, int page, int rows, string filters)
        {
            int totalPages = 0;
            int totalRecords = 0;
            IEnumerable<MANEJOCAJAS> manejosCajas;
            manejosCajas = ObtenerTodos();
            manejosCajas = JqGrid<MANEJOCAJAS>.GetFilteredContent(sidx, sord, page, rows, filters, manejosCajas.AsQueryable(), ref totalPages, ref totalRecords);
            var rowsModel = (
                from manejoCaja in manejosCajas.ToList()
                select new
                {
                    i = manejoCaja.MANEJOCAJA_ID,
                    cell = new string[] {
                           manejoCaja.MANEJOCAJA_ID.ToString(),
                           manejoCaja.CAJAS.NOMBRE,
                           manejoCaja.CAJAS.SEDES.NOMBRE,
                           manejoCaja.USUARIOS.NOMBRE + " " + manejoCaja.USUARIOS.APELLIDO,
                           manejoCaja.FECHA_APERTURA.ToString("dd/MM/yyyy"),
                           (manejoCaja.FECHA_CIERRE.HasValue) ? manejoCaja.FECHA_CIERRE.Value.ToString("dd/MM/yyyy") : "-----",
                           (manejoCaja.CONDICION == "A") ? "ABIERTA" : "CERRADA",
                            "<a title=\"Editar\" href=\"/ManejoCajas/Editar/"+ manejoCaja.MANEJOCAJA_ID+"\"><span id=\""+manejoCaja.MANEJOCAJA_ID+"\" class=\"ui-icon ui-icon-pencil\"></a>",                          
                            "<span id=\""+manejoCaja.MANEJOCAJA_ID+"\"  class=\"ui-icon ui-icon-close\" ></span>" }
                }).ToArray();
            return JqGrid<MANEJOCAJAS>.SetJsonData(totalPages, totalRecords, page, rowsModel);
        }

        public MANEJOCAJAS ObtenerPorClave(int id)
        {
            try
            {
                return _unidad.RepositorioManejoCajas.GetById(id);
                
            }
            catch 
            {
                return null;
            }
        }

        public MANEJOCAJAS ObtenerManejoCajaAbierta(string usuario)
        {
            USUARIOS usu = new ServicioUsuario().ObtenerPorLogin(usuario);
            return ObtenerTodos().Where(m => m.USUARIO_ID == usu.USUARIO_ID &&
                                            !m.FECHA_CIERRE.HasValue &&
                                            m.CONDICION == "A").FirstOrDefault();
        }
        public MANEJOCAJAS ObtenerManejoUltimaCajaAbierta(string usuario)
        {
            USUARIOS usu = new ServicioUsuario().ObtenerPorLogin(usuario);
            return ObtenerTodos().Where(m => m.USUARIO_ID == usu.USUARIO_ID &&
                                            m.CONDICION == "C").OrderByDescending(u=>u.FECHA_CIERRE).FirstOrDefault();
        }
        public IEnumerable<MANEJOCAJAS> ObtenerTodos()
        {
            try
            {
                IEnumerable<MANEJOCAJAS> sedes= _unidad.RepositorioManejoCajas.GetAll();
                if (sedes == null)
                    return Enumerable.Empty<MANEJOCAJAS>();
                else
                    return sedes;
            }
            catch {
                return Enumerable.Empty<MANEJOCAJAS>();
            }
        }

        public bool EstaCerrada(MANEJOCAJAS manejoCajas)
        {
            try
            {
                return (ObtenerTodos().Where(m => m.CAJA_ID == manejoCajas.CAJA_ID && m.CONDICION == "A").FirstOrDefault() == null);
            }
            catch 
            {
                return false;
            }
        }       
    }
}
