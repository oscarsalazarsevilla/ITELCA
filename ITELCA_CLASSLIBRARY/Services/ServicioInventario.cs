﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using System.Data.Entity;
using ITELCA_CLASSLIBRARY.Models;
using ITELCA_CLASSLIBRARY.Repositories;
using ITELCA_CLASSLIBRARY.CustomClasses;
using System.Collections;
using System.Web;
using UmbrellaClassLibrary.CustomClasses;

namespace ITELCA_CLASSLIBRARY.Services
{
     public class ServicioInventario
    {
        private UnitOfWork unidad;
        public ServicioInventario()
        {
            this.unidad = new UnitOfWork();
        }
        public bool Guardar(INVENTARIOS inventario)
        {
            try
            {
                unidad.RepositorioInventarios.Add(inventario);
                unidad.Save();
                return true;
            }
            catch
            {
                return false;
            }
        }
        public bool Modificar(INVENTARIOS inventario)
        {
            try
            {
                unidad.RepositorioInventarios.Modify(inventario);
                unidad.Save();
                return true;
            }
            catch
            {
                return false;
            }
        }


        public List<object> Eliminar(decimal id)
        {
            List<object> lista = new List<object>();
            try
            {
                var aux = unidad.RepositorioInventarios.GetById((int)id);
                if (aux != null)
                {
                    lista.Add(new
                    {
                        ok = 1,
                        msg = "Registro eliminado del inventario"
                    });
                    unidad.RepositorioInventarios.Delete(aux);
                    unidad.Save();
                    return lista;
                }
                else
                {
                    lista.Add(new
                    {
                        ok = 0,
                        msg = "Registro del inventario no encontrado"
                    });
                    return lista;
                }
            }
            catch
            {
                lista.Add(new
                {
                    ok = 0,
                    msg = "Error eliminando registro del inventario"
                });
                return lista;
            }

        }

        public INVENTARIOS ObtenerPorClave(int id)
        {
            try
            {
                return unidad.RepositorioInventarios.GetById(id);
                
            }
            catch 
            {
                return null;
            }
        }

       
        public IEnumerable<INVENTARIOS> ObtenerTodos()
        {
            try
            {
                IEnumerable<INVENTARIOS> sedes= unidad.RepositorioInventarios.GetAll();
                if (sedes == null)
                    return Enumerable.Empty<INVENTARIOS>();
                else
                    return sedes;
            }
            catch {
                return Enumerable.Empty<INVENTARIOS>();
            }
        }

        public List<INVENTARIOS> ObtenerInventarioPorFecha(DateTime fecha)
        {
            return ObtenerTodos().Where(i => i.FECHA.Day == fecha.Day &&
                                            i.FECHA.Month == fecha.Month &&
                                            i.FECHA.Year == fecha.Year).ToList();
        }
    }
}
