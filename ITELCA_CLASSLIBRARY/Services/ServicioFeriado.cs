﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using System.Data.Entity;
using ITELCA_CLASSLIBRARY.Models;
using ITELCA_CLASSLIBRARY.Repositories;
using ITELCA_CLASSLIBRARY.CustomClasses;
using System.Collections;
using System.Web;
using UmbrellaClassLibrary.CustomClasses;
using System.Data.Entity.Validation;


namespace ITELCA_CLASSLIBRARY.Services
{
     public class ServicioFeriado
    {
        private UnitOfWork unidad;
        public ServicioFeriado()
        {
            this.unidad = new UnitOfWork();
        }
        public bool Guardar(FERIADOS feriado)
        {
            try
            {
                feriado.FECHA_CREACION = DateTime.Now;
                unidad.RepositorioFeriado.Add(feriado);
                unidad.Save();
                return true;
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                return false;

            }
        }
        public bool Modificar(FERIADOS entity)
        {
            try
            {

                unidad.RepositorioFeriado.Modify(entity);
                unidad.Save();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public List<object> Eliminar(decimal id)
        {
            List<object> lista = new List<object>();
            try
            {
                var aux = unidad.RepositorioFeriado.GetById((int)id);
                if (aux != null)
                {
                    lista.Add(new
                    {
                        ok = 1,
                        msg = "Feriado Eliminado"
                    });
                    unidad.RepositorioFeriado.Delete(aux);
                    unidad.Save();
                    return lista;
                }
                else
                {
                    lista.Add(new
                    {
                        ok = 0,
                        msg = "Feriado No encontrado"
                    });
                    return lista;
                }
            }
            catch
            {
                lista.Clear();
                lista.Add(new
                {
                    ok = 0,
                    msg = "Error Eliminando Feriado"
                });
                return lista;
            }

        }

        public object ObtenerDataGrid( string sidx, string sord, int page, int rows, string filters)
        {
            int totalPages = 0;
            int totalRecords = 0;
            IEnumerable<FERIADOS> feriados;
            feriados = unidad.RepositorioFeriado.GetAll().Where(u => u.ESTADO == 1 || (u.ESTADO == 0 && u.CONDICION != "E"));
            feriados = JqGrid<FERIADOS>.GetFilteredContent(sidx, sord, page, rows, filters, feriados.AsQueryable(), ref totalPages, ref totalRecords);
           
            var rowsModel = (
                from feriado in feriados.ToList()
                select new
                {
                    i = feriado.FERIADO_ID,
                    cell = new string[] {
                           feriado.FERIADO_ID.ToString(),
                           feriado.NOMBRE.ToString(),
                           feriado.DIA.ToString(),
                           feriado.MES.ToString(),
                           (feriado.ANIO.HasValue) ? feriado.ANIO.Value.ToString() : "---",
                           (feriado.PERIODICO.HasValue) ? (feriado.PERIODICO.Value== 0 ? "": "Anual") : "" ,
                           (feriado.ESTADO == 1) ? "Activo" : "Inactivo",
                            "<a title=\"Editar\" href=\"/Feriado/Editar/"+ feriado.FERIADO_ID+"\"><span id=\""+ feriado.FERIADO_ID+"\" class=\"ui-icon ui-icon-pencil\"></a>",                          
                            "<span id=\""+  feriado.FERIADO_ID+"\"  class=\"ui-icon ui-icon-close\" ></span>" }
                }).ToArray();
            return JqGrid<FERIADOS>.SetJsonData(totalPages, totalRecords, page, rowsModel);
        }

        public FERIADOS ObtenerPorClave(int id)
        {
            try
            {
                return unidad.RepositorioFeriado.GetById(id);
                
            }
            catch
            {
                return null;
            }
        }

       
        public IEnumerable<FERIADOS> ObtenerTodos()
        {
            try
            {
                IEnumerable<FERIADOS> feriado = unidad.RepositorioFeriado.GetAll();
                if (feriado == null)
                    return Enumerable.Empty<FERIADOS>();
                else
                    return feriado;
            }
            catch {
                return Enumerable.Empty<FERIADOS>();
            }
        }

        public bool Duplicado(decimal FERIADO_ID, string NOMBRE)
        {
            try
            {
                if (unidad.RepositorioFeriado.GetAll().Where(u => u.NOMBRE.ToLower() == NOMBRE.ToLower() &&
                                                        u.FERIADO_ID != FERIADO_ID).FirstOrDefault() != null)
                    return true;
                else
                    return false;

            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                return false;

            }
        }
      public bool VerificarFeriado(DateTime fecha){
          if (unidad.RepositorioFeriado.GetAll().Where(u => u.DIA == fecha.Day && u.MES == fecha.Month).Count() > 0)
              return false;
          else
              return true;
      
      }
      public List<string> ObtenerListaFeriados()
      {
          List<string> lista = new List<string>();
          try
          {
              IEnumerable<FERIADOS> aux = ObtenerTodos();;
              foreach(var ope in aux){
                    lista.Add(ope.DIA+"/"+ope.MES+"/"+ope.ANIO);
              }
              return lista;
          }
          catch { return lista; }
      }
    }
}
