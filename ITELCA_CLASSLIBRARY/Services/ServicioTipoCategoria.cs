﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using System.Data.Entity;
using ITELCA_CLASSLIBRARY.Models;
using ITELCA_CLASSLIBRARY.Repositories;
using ITELCA_CLASSLIBRARY.CustomClasses;
using System.Collections;
using System.Web;
using UmbrellaClassLibrary.CustomClasses;


namespace ITELCA_CLASSLIBRARY.Services
{
     public class ServicioTipoCategoria
    {
        private UnitOfWork unidad;
        public ServicioTipoCategoria()
        {
            this.unidad = new UnitOfWork();
        }
        public bool Guardar(TIPOSCATEGORIAS tipoCategoria)
        {
            try
            {
                unidad.RepositorioTiposCategorias.Add(tipoCategoria);
                unidad.Save();
                return true;
            }
            catch
            {
                return false;
            }
        }
        public bool Modificar(TIPOSCATEGORIAS entity)
        {
            try
            {
                unidad.RepositorioTiposCategorias.Modify(entity);
                unidad.Save();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public List<object> Eliminar(decimal tipoA)
        {
            List<object> lista = new List<object>();
            try
            {
                var aux = unidad.RepositorioTiposCategorias.GetById((int)tipoA);
                if (aux != null)
                {
                    lista.Add(new
                    {
                        ok = 1,
                        msg = "Tipo categoría eliminada"
                    });
                    unidad.RepositorioTiposCategorias.Modify(aux);
                    unidad.Save();
                    return lista;
                }
                else
                {
                    lista.Add(new
                    {
                        ok = 0,
                        msg = "Tipo categoría no encontrada"
                    });
                    return lista;
                }
            }
            catch
            {
                lista.Clear();
                lista.Add(new
                {
                    ok = 0,
                    msg = "Error eliminando tipo categoría"
                });
                return lista;
            }

        }

        public object ObtenerDataGrid( string sidx, string sord, int page, int rows, string filters)
        {
            int totalPages = 0;
            int totalRecords = 0;
            IEnumerable<TIPOSCATEGORIAS> tiposCategorias;
            tiposCategorias = unidad.RepositorioTiposCategorias.GetAll();
            tiposCategorias = JqGrid<TIPOSCATEGORIAS>.GetFilteredContent(sidx, sord, page, rows, filters, tiposCategorias.AsQueryable(), ref totalPages, ref totalRecords);
          
            var rowsModel = (
                from tipoCategoria in tiposCategorias.ToList()
                select new
                {
                    i = tipoCategoria.TIPOCATEGORIA_ID,
                    cell = new string[] {
                           tipoCategoria.TIPOCATEGORIA_ID.ToString(),
                           tipoCategoria.NOMBRE.ToString(),
                           "<a title=\"Editar\" href=\"/TipoCategoria/Editar/"+ tipoCategoria.TIPOCATEGORIA_ID+"\"><span id=\""+tipoCategoria.TIPOCATEGORIA_ID+"\" class=\"ui-icon ui-icon-pencil\"></a>",                          
                           "<span id=\""+tipoCategoria.TIPOCATEGORIA_ID+"\"  class=\"ui-icon ui-icon-close\" ></span>" }
                }).ToArray();
            return JqGrid<TIPOSCATEGORIAS>.SetJsonData(totalPages, totalRecords, page, rowsModel);
        }

        public TIPOSCATEGORIAS ObtenerPorClave(int id)
        {
            try
            {
                return unidad.RepositorioTiposCategorias.GetById(id);
                
            }
            catch
            {
                return null;
            }
        }

       
        public IEnumerable<TIPOSCATEGORIAS> ObtenerTodos()
        {
            try
            {
                IEnumerable<TIPOSCATEGORIAS> TIPOSCATEGORIAS= unidad.RepositorioTiposCategorias.GetAll();
                if (TIPOSCATEGORIAS == null)
                    return Enumerable.Empty<TIPOSCATEGORIAS>();
                else
                    return TIPOSCATEGORIAS;
            }
            catch {
                return Enumerable.Empty<TIPOSCATEGORIAS>();
            }
        }

        public Dictionary<decimal, string> ObtenerDiccionarioTiposCategorias()
        {
            Dictionary<decimal, string> lista = new Dictionary<decimal, string>();
            foreach (var entity in ObtenerTodos().OrderBy(c=>c.NOMBRE).ToList())
            {
                lista.Add(entity.TIPOCATEGORIA_ID, entity.NOMBRE);
            }
            return lista;
        }
    }
}
