﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using System.Data.Entity;
using ITELCA_CLASSLIBRARY.Models;
using ITELCA_CLASSLIBRARY.Repositories;
using ITELCA_CLASSLIBRARY.CustomClasses;
using System.Collections;
using System.Web;
using UmbrellaClassLibrary.CustomClasses;

namespace ITELCA_CLASSLIBRARY.Services
{
    public class ServicioFaq
    {
        private UnitOfWork unidad;
        public ServicioFaq()
        {
            this.unidad = new UnitOfWork();
        }
        public bool Guardar(FAQS faq)
        {
            try
            {
                faq.CONDICION = "C";
                unidad.RepositorioFaq.Add(faq);
                unidad.Save();
                return true;
            }
            catch
            {
                return false;
            }
        }
        public bool Modificar(FAQS entity)
        {
            try
            {
                entity.CONDICION = "A";
                unidad.RepositorioFaq.Modify(entity);
                unidad.Save();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public List<object> Eliminar(decimal faq)
        {
            List<object> lista = new List<object>();
            try
            {
                var aux = unidad.RepositorioFaq.GetById((int)faq);
                if (aux != null)
                {
                    lista.Add(new
                    {
                        ok = 1,
                        msg = "Faq Eliminada"
                    });
                    unidad.RepositorioFaq.Delete(aux);
                    unidad.Save();
                    return lista;
                }
                else
                {
                    lista.Add(new
                    {
                        ok = 0,
                        msg = "Faq No encontrada"
                    });
                    return lista;
                }
            }
            catch
            {
                lista.Clear();
                lista.Add(new
                {
                    ok = 0,
                    msg = "Error Eliminando Faq"
                });
                return lista;
            }

        }

        public object ObtenerDataGrid(string sidx, string sord, int page, int rows, string filters)
        {
            int totalPages = 0;
            int totalRecords = 0;
            IEnumerable<FAQS> faqs;
            faqs = unidad.RepositorioFaq.GetAll().Where(u => u.ESTADO == 1 || (u.ESTADO == 0 && u.CONDICION != "E"));
            faqs = JqGrid<FAQS>.GetFilteredContent(sidx, sord, page, rows, filters, faqs.AsQueryable(), ref totalPages, ref totalRecords);
           
            var rowsModel = (
                from faq in faqs.ToList()
                select new
                {
                    i = faq.FAQ_ID,
                    cell = new string[] {
                           faq.FAQ_ID.ToString(),
                           faq.PREGUNTA.ToString(),
                           faq.RESPUESTA.ToString(),
                           faq.TIPOS_FAQS.NOMBRE.ToString(),
                           faq.ORDEN.ToString(),
                           faq.ESTADO.ToString(),
                            "<a title=\"Editar\" href=\"/Faqs/Editar/"+  faq.FAQ_ID+"\"><span id=\""+  faq.FAQ_ID+"\" class=\"ui-icon ui-icon-pencil\"></a>",                          
                            "<span id=\""+  faq.FAQ_ID +"\"  class=\"ui-icon ui-icon-close\" ></span>" }
                }).ToArray();
            return JqGrid<TIPOSCURSOS>.SetJsonData(totalPages, totalRecords, page, rowsModel);
        }

        public FAQS ObtenerPorClave(int id)
        {
            try
            {
                return unidad.RepositorioFaq.GetById(id);

            }
            catch
            {
                return null;
            }
        }
        public FAQS ObtenerPorNombre(string nombre)
        {
            try
            {
                FAQS faq = unidad.RepositorioFaq.GetAll().Where(u => u.PREGUNTA.ToLower() == nombre.ToLower()).FirstOrDefault();
                if (faq != null)
                    return faq;
                else
                    return null;

            }
            catch
            {
                return null;
            }
        }

        public IEnumerable<IGrouping<string, FAQS>> ObtenerTodosOrdenados()
        {
            try
            {
                IEnumerable<IGrouping<string, FAQS>> faqs = unidad.RepositorioFaq.GetAll().OrderBy(u => u.ORDEN).GroupBy(u => u.TIPOS_FAQS.NOMBRE);

                if (faqs == null)
                    return null;
                else
                    return faqs;
            }
            catch
            {
                return null;
            }
        }

        public IEnumerable<FAQS> ObtenerTodos()
        {
            try
            {
                IEnumerable<FAQS> faqs = unidad.RepositorioFaq.GetAll();
                if (faqs == null)
                    return Enumerable.Empty<FAQS>();
                else
                    return faqs;
            }
            catch
            {
                return Enumerable.Empty<FAQS>();
            }
        }

        public bool Duplicado(FAQS faq)
        {
            try
            {
                if (unidad.RepositorioFaq.GetAll().Where(u => u.PREGUNTA.ToLower() == faq.PREGUNTA.ToLower() &&
                                                                 u.FAQ_ID != faq.FAQ_ID).Count() > 0)
                    return true;
                else
                    return false;

            }
            catch
            {
                return false;
            }
        }



    }
}
