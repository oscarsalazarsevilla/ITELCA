﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using System.Data.Entity;
using ITELCA_CLASSLIBRARY.Models;
using ITELCA_CLASSLIBRARY.Repositories;
using ITELCA_CLASSLIBRARY.CustomClasses;
using System.Collections;
using System.Web;
using UmbrellaClassLibrary.CustomClasses;
using System.Data.Entity.Validation;


namespace ITELCA_CLASSLIBRARY.Services
{
     public class ServicioConceptoCurso
    {
        private UnitOfWork unidad;
        public ServicioConceptoCurso()
        {
            this.unidad = new UnitOfWork();
        }
        public bool Guardar(CONCEPTOS_CURSOS conceptoCurso)
        {
            try
            {
                string[] arregloCursos = conceptoCurso.MULTI_CURSO_ID.Split(',');
                string[] arregloTurnos = conceptoCurso.MULTI_TURNO_ID.Split(',');

                foreach (var curso in arregloCursos)
                {
                    foreach (var turno in arregloTurnos)
                    {
                        CONCEPTOS_CURSOS con_cur = new CONCEPTOS_CURSOS();
                        con_cur.FECHA_EFECTIVA = conceptoCurso.FECHA_EFECTIVA;
                        con_cur.ESTADO = 1;
                        con_cur.CONCEPTO_ID = conceptoCurso.CONCEPTO_ID;
                        con_cur.MONTO = conceptoCurso.MONTO;
                        con_cur.TURNO_ID = decimal.Parse(turno);
                        con_cur.CURSO_ID = decimal.Parse(curso);
                        con_cur.USUARIO_ID = conceptoCurso.USUARIO_ID;
                        con_cur.CONDICION = "C";

                        CONCEPTOS_CURSOS aux = ObtenerTodos().Where(c => c.CURSO_ID == con_cur.CURSO_ID &&
                                                                            c.TURNO_ID == con_cur.TURNO_ID &&
                                                                            c.CONCEPTO_ID == con_cur.CONCEPTO_ID &&
                                                                            c.FECHA_EFECTIVA == con_cur.FECHA_EFECTIVA).FirstOrDefault();
                        if (aux == null)
                            unidad.RepositorioConceptoCurso.Add(con_cur);
                    }
                }
                
                unidad.Save();
                return true;
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                return false;
            }
        }
        public bool Modificar(CONCEPTOS_CURSOS conceptoCurso)
        {
            try
            {
                conceptoCurso.CONDICION = "A";
                unidad.RepositorioConceptoCurso.Modify(conceptoCurso);
                unidad.Save();
                return true;
            }
            catch
            {
                return false;
            }
        }


        public List<object> Eliminar(decimal id)
        {
            List<object> lista = new List<object>();
            try
            {
                var aux = unidad.RepositorioConceptoCurso.GetById((int)id);
                if (aux != null)
                {
                    lista.Add(new
                    {
                        ok = 1,
                        msg = "ConceptoCurso Eliminado"
                    });
                    aux.ESTADO = 0;
                    aux.CONDICION = "E";
                    unidad.RepositorioConceptoCurso.Modify(aux);
                    //unidad.RepositorioConceptoCurso.Delete(aux);
                    unidad.Save();
                    return lista;
                }
                else
                {
                    lista.Add(new
                    {
                        ok = 0,
                        msg = "ConceptoCurso No encontrado"
                    });
                    return lista;
                }
            }
            catch
            {
                lista.Clear();
                lista.Add(new
                {
                    ok = 0,
                    msg = "Error Eliminando ConceptoCurso"
                });
                return lista;
            }

        }

        public object ObtenerDataGrid( string sidx, string sord, int page, int rows, string filters)
        {
            int totalPages = 0;
            int totalRecords = 0;
            IEnumerable<CONCEPTOS_CURSOS> conceptosCursos;
            conceptosCursos = ObtenerTodos().Where(cc => cc.ESTADO == 1 || (cc.ESTADO == 0 && cc.CONDICION != "E")).OrderBy(u => u.CURSOS.NOMBRE);
            conceptosCursos = JqGrid<CONCEPTOS_CURSOS>.GetFilteredContent(sidx, sord, page, rows, filters, conceptosCursos.AsQueryable(), ref totalPages, ref totalRecords);

            var rowsModel = (
                from conceptoCurso in conceptosCursos.ToList()
                select new
                {
                    i = conceptoCurso.CONCEPTO_ID,
                    cell = new string[] {
                           conceptoCurso.CONCEPTO_ID.ToString(),
                           conceptoCurso.CURSOS.NOMBRE,
                           conceptoCurso.TURNOS.NOMBRE,
                           conceptoCurso.CONCEPTOS.NOMBRE,
                           conceptoCurso.FECHA_EFECTIVA.ToString("dd/MM/yyyy"),
                           String.Format("{0:N2}", conceptoCurso.MONTO),
                           (conceptoCurso.ESTADO == 1) ? "Activo" : "Inactivo",
                            "<a title=\"Editar\" href=\"/ConceptoCurso/Editar/"+ conceptoCurso.CONCEPTO_CURSO_ID+"\"><span id=\""+conceptoCurso.CONCEPTO_CURSO_ID+"\" class=\"ui-icon ui-icon-pencil\"></a>",                          
                            "<span id=\""+conceptoCurso.CONCEPTO_CURSO_ID+"\"  class=\"ui-icon ui-icon-close\" ></span>" }
                }).ToArray();
            return JqGrid<CONCEPTOS_CURSOS>.SetJsonData(totalPages, totalRecords, page, rowsModel);
        }

        public CONCEPTOS_CURSOS ObtenerPorClave(int id)
        {
            try
            {
                return unidad.RepositorioConceptoCurso.GetById(id);
                
            }
            catch 
            {
                return null;
            }
        }

        public CONCEPTOS_CURSOS ObtenerConceptoCursoPorId(int id)
        {
            try
            {
                return unidad.RepositorioConceptoCurso.GetById(id);

            }
            catch
            {
                return null;
            }
        }

       
        public IEnumerable<CONCEPTOS_CURSOS> ObtenerTodos()
        {
            try
            {
                IEnumerable<CONCEPTOS_CURSOS> CONCEPTOS_CURSOS = unidad.RepositorioConceptoCurso.GetAll().Where(u=>u.ESTADO==1).OrderBy(u=> u.CURSOS.NOMBRE);
                if (CONCEPTOS_CURSOS == null)
                    return Enumerable.Empty<CONCEPTOS_CURSOS>();
                else
                    return CONCEPTOS_CURSOS;
            }
            catch  {
                return Enumerable.Empty<CONCEPTOS_CURSOS>();
            }
        }

        public bool Duplicado(CONCEPTOS_CURSOS CONCEPTOS_CURSOS)
        {
            try
            {
                if (unidad.RepositorioConceptoCurso.GetAll().Where(u => u.CONCEPTO_ID == CONCEPTOS_CURSOS.CONCEPTO_ID &&
                                                                    u.CURSO_ID == CONCEPTOS_CURSOS.CURSO_ID &&
                                                                 u.CONCEPTO_CURSO_ID != CONCEPTOS_CURSOS.CONCEPTO_CURSO_ID && CONCEPTOS_CURSOS.ESTADO==1).Count() > 0)
                    return true;
                else
                    return false;

            }
            catch
            {
                return false;
            }
        }

        public List<LISTACONCEPTOS> ListaConceptoCursosCursos(string curso_id)
        {

                List<LISTACONCEPTOS> listaConceptoCursos = new List<LISTACONCEPTOS>();
                Decimal curso = decimal.Parse(curso_id);
                IEnumerable<CONCEPTOS_CURSOS> conceptoCursos = unidad.RepositorioConceptoCurso.GetAll().Where( u => u.CURSO_ID == curso && u.ESTADO==1);

                foreach (var ope in conceptoCursos)
                {
                    LISTACONCEPTOS obj = new LISTACONCEPTOS();
                    obj.id = ope.CONCEPTO_CURSO_ID.ToString();
                    obj.nombre = ope.CONCEPTOS.NOMBRE.ToString();
                    listaConceptoCursos.Add(obj);
                }

                return listaConceptoCursos;
              

                    }

        public List<CONCEPTOS_CURSOS> ListaConceptosCursosPorCurso(decimal curso_id, decimal turno_id, DateTime fecha)
        {
            List<CONCEPTOS_CURSOS> conceptosLista = new List<CONCEPTOS_CURSOS>();
            var conceptos = ObtenerTodos().Where(c =>   c.CURSO_ID == curso_id &&
                                                        c.TURNO_ID == turno_id &&
                                                        c.ESTADO == 1).Select(c=> new { curso_id = c.CURSO_ID,
                                                                                        turno_id = c.TURNO_ID}).Distinct();
            foreach (var conc in conceptos)
            {
                CONCEPTOS_CURSOS concepto = new CONCEPTOS_CURSOS();

                concepto = ObtenerTodos().Where(c => c.CURSO_ID == conc.curso_id &&
                                                        c.TURNO_ID == conc.turno_id &&
                                                        c.ESTADO == 1 &&
                                                        c.FECHA_EFECTIVA <= fecha).OrderByDescending(c => c.FECHA_EFECTIVA).FirstOrDefault();

                if (concepto != null)
                {
                    conceptosLista.Add(concepto);
                }
            }
            return conceptosLista;
        }

        public string ObtenerMontoLineaDetale(decimal id_conceptoCurso, string cantidad)
        {
            CONCEPTOS_CURSOS conceptoCurso = unidad.RepositorioConceptoCurso.GetAll().Where(u => u.CONCEPTO_CURSO_ID == id_conceptoCurso && u.ESTADO==1).FirstOrDefault();
            return (conceptoCurso.MONTO * Int32.Parse(cantidad)).ToString();

        }

        public CONCEPTOS_CURSOS ObtenerConceptoCursoPorCursoPorFechaEfectiva(decimal curso_id)
        {
            return ObtenerTodos().Where(c => c.CURSO_ID == curso_id &&
                                             c.ESTADO == 1 ).OrderByDescending(u=>u.FECHA_EFECTIVA).FirstOrDefault();
        }
       
    }
}
