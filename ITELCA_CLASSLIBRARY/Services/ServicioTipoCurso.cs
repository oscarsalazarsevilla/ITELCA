﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using System.Data.Entity;
using ITELCA_CLASSLIBRARY.Models;
using ITELCA_CLASSLIBRARY.Repositories;
using ITELCA_CLASSLIBRARY.CustomClasses;
using System.Collections;
using System.Web;
using UmbrellaClassLibrary.CustomClasses;


namespace ITELCA_CLASSLIBRARY.Services
{
     public class ServicioTipoCurso
    {
        private UnitOfWork unidad;
        public ServicioTipoCurso()
        {
            this.unidad = new UnitOfWork();
        }
        public bool Guardar(TIPOSCURSOS tipoCurso)
        {
            try
            {
                tipoCurso.CONDICION = "C";
                unidad.RepositorioTipoCurso.Add(tipoCurso);
                unidad.Save();
                return true;
            }
            catch
            {
                return false;
            }
        }
        public bool Modificar(TIPOSCURSOS entity)
        {
            try
            {
                entity.CONDICION = "A";
                unidad.RepositorioTipoCurso.Modify(entity);
                unidad.Save();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public List<object> Eliminar(decimal tipoA)
        {
            List<object> lista = new List<object>();
            try
            {
                var aux = unidad.RepositorioTipoCurso.GetById((int)tipoA);
                if (aux != null)
                {
                    lista.Add(new
                    {
                        ok = 1,
                        msg = "Tipo Curso Eliminado"
                    });
                    aux.CONDICION = "E";
                    aux.ESTADO = 0;
                    unidad.RepositorioTipoCurso.Modify(aux);
                    unidad.Save();
                    return lista;
                }
                else
                {
                    lista.Add(new
                    {
                        ok = 0,
                        msg = "Tipo Curso No encontrada"
                    });
                    return lista;
                }
            }
            catch
            {
                lista.Clear();
                lista.Add(new
                {
                    ok = 0,
                    msg = "Error Eliminando Tipo Curso"
                });
                return lista;
            }

        }

        public object ObtenerDataGrid( string sidx, string sord, int page, int rows, string filters)
        {
            int totalPages = 0;
            int totalRecords = 0;
            IEnumerable<TIPOSCURSOS> tipoCursos;
            tipoCursos = unidad.RepositorioTipoCurso.GetAll().Where(u => u.ESTADO == 1 || (u.ESTADO == 0 && u.CONDICION != "E"));
            tipoCursos = JqGrid<TIPOSCURSOS>.GetFilteredContent(sidx, sord, page, rows, filters, tipoCursos.AsQueryable(), ref totalPages, ref totalRecords);
          
            var rowsModel = (
                from tipoCurso in tipoCursos.ToList()
                select new
                {
                    i = tipoCurso.TIPO_CURSO_ID,
                    cell = new string[] {
                           tipoCurso.TIPO_CURSO_ID.ToString(),
                           tipoCurso.NOMBRE,
                           tipoCurso.DESCRIPCION,
                           (tipoCurso.ESTADO == 1) ? "Activo" : "Inactivo",
                            "<a title=\"Editar\" href=\"/TipoCurso/Editar/"+  tipoCurso.TIPO_CURSO_ID+"\"><span id=\""+ tipoCurso.TIPO_CURSO_ID+"\" class=\"ui-icon ui-icon-pencil\"></a>",                          
                            "<span id=\""+ tipoCurso.TIPO_CURSO_ID+"\"  class=\"ui-icon ui-icon-close\" ></span>" }
                }).ToArray();
            return JqGrid<TIPOSCURSOS>.SetJsonData(totalPages, totalRecords, page, rowsModel);
        }

        public TIPOSCURSOS ObtenerPorClave(int id)
        {
            try
            {
                return unidad.RepositorioTipoCurso.GetById(id);
                
            }
            catch
            {
                return null;
            }
        }

       
        public IEnumerable<TIPOSCURSOS> ObtenerTodos()
        {
            try
            {
                IEnumerable<TIPOSCURSOS> tipoCursos= unidad.RepositorioTipoCurso.GetAll().Where(u=>u.ESTADO==1).OrderBy(c=>c.NOMBRE);
                if (tipoCursos == null)
                    return Enumerable.Empty<TIPOSCURSOS>();
                else
                    return tipoCursos;
            }
            catch {
                return Enumerable.Empty<TIPOSCURSOS>();
            }
        }

        public bool Duplicado(TIPOSCURSOS tipoCurso)
        {
            try
            {
                if (unidad.RepositorioTipoCurso.GetAll().Where(u => u.NOMBRE.ToLower() == tipoCurso.NOMBRE.ToLower() &&
                                                                 u.TIPO_CURSO_ID != tipoCurso.TIPO_CURSO_ID && u.ESTADO==1).Count() > 0)
                    return true;
                else
                    return false;

            }
            catch 
            {
                return false;
            }
        }


        public IEnumerable<OFERTASMODULOS> ObtenerProximosCursos(decimal tipoCursoId)
        {
            return unidad.RepositorioOfertasModulos.GetAll().Where(u => u.CURSOS_MODULOS.CURSOS.TIPO_CURSO_ID == tipoCursoId && u.CURSOS_MODULOS.CURSOS.CONDICION != "E").OrderBy(u=> u.FECHA_INICIO);
            //return unidad.RepositorioOfertaCurso.GetAll().Where(u => u.CURSOS.TIPO_CURSO_ID == tipoCursoId && u.CURSOS.CONDICION != "E");
        }
      
    }
}
