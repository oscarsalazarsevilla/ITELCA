﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using System.Data.Entity;
using ITELCA_CLASSLIBRARY.Models;
using ITELCA_CLASSLIBRARY.Repositories;
using ITELCA_CLASSLIBRARY.CustomClasses;
using System.Collections;
using System.Web;
using UmbrellaClassLibrary.CustomClasses;


namespace ITELCA_CLASSLIBRARY.Services
{
     public class ServicioSede
    {
        private UnitOfWork unidad;
        public ServicioSede()
        {
            this.unidad = new UnitOfWork();
        }
        public bool Guardar(SEDES sede)
        {
            try
            {
                sede.CONDICION = "C";
                unidad.RepositorioSede.Add(sede);
                
                unidad.Save();
                return true;
            }
            catch
            {
                return false;
            }
        }
        public bool Modificar(SEDES entity)
        {
            try
            {
                entity.CONDICION = "A";
                unidad.RepositorioSede.Modify(entity);
                unidad.Save();
                return true;
            }
            catch
            {
                return false;
            }
        }


        public List<object> Eliminar(decimal id)
        {
            List<object> lista = new List<object>();
            try
            {
                var aux = unidad.RepositorioSede.GetById((int)id);
                if (aux != null)
                {
                    lista.Add(new
                    {
                        ok = 1,
                        msg = "Sede Eliminada"
                    });
                    aux.ESTADO=0;
                    aux.CONDICION = "E";
                    unidad.RepositorioSede.Modify(aux);
                    //unidad.RepositorioSede.Delete(aux);
                    unidad.Save();
                    return lista;
                }
                else
                {
                    lista.Add(new
                    {
                        ok = 0,
                        msg = "Sede No encontrada"
                    });
                    return lista;
                }
            }
            catch
            {
                lista.Clear();
                lista.Add(new
                {
                    ok = 0,
                    msg = "Error Eliminando Sede"
                });
                return lista;
            }

        }

        public object ObtenerDataGrid( string sidx, string sord, int page, int rows, string filters)
        {
            int totalPages = 0;
            int totalRecords = 0;
            IEnumerable<SEDES> sedes;
            sedes = unidad.RepositorioSede.GetAll().Where(u => u.ESTADO == 1 || (u.ESTADO == 0 && u.CONDICION != "E"));
            sedes = JqGrid<SEDES>.GetFilteredContent(sidx, sord, page, rows, filters, sedes.AsQueryable(), ref totalPages, ref totalRecords);
           
            var rowsModel = (
                from sede in sedes.ToList()
                select new
                {
                    i = sede.SEDE_ID,
                    cell = new string[] {
                           sede.SEDE_ID.ToString(),
                           sede.NOMBRE,
                           sede.CODIGO,
                           sede.TELEFONO1,
                           sede.TELEFONO2,
                           sede.DIRECCION,
                           (sede.ESTADO == 1) ? "Activo" : "Inactivo",
                            "<a title=\"Editar\" href=\"/Sede/Editar/"+ sede.SEDE_ID+"\"><span id=\""+sede.SEDE_ID+"\" class=\"ui-icon ui-icon-pencil\"></a>",                          
                            "<span id=\""+sede.SEDE_ID+"\"  class=\"ui-icon ui-icon-close\" ></span>" }
                }).ToArray();
            return JqGrid<SEDES>.SetJsonData(totalPages, totalRecords, page, rowsModel);
        }

        public SEDES ObtenerPorClave(int id)
        {
            try
            {
                return unidad.RepositorioSede.GetById(id);
                
            }
            catch 
            {
                return null;
            }
        }

        public List<object> ObtenerObjeto()
        {
            List<object> lista = new List<object>();
            try
            {
                

                foreach (var ope in ObtenerTodos()) 
                    lista.Add(new{
                        key=ope.SEDE_ID,
                        value=ope.NOMBRE
                      });
                        
                        
                
                
                return lista;
            }
            catch
            {
                return null;
            }
        }

        public List<object> ObtenerSedePorCurso(long id)
        {
            List<object> lista = new List<object>();
            try
            {

                foreach (var ope in unidad.RepositorioSede.GetAll().Where(u=>u.SEDE_ID==id && u.ESTADO==1))
                    lista.Add(new
                    {
                        key = ope.SEDE_ID,
                        value = ope.NOMBRE
                    });




                return lista;
            }
            catch
            {
                return null;
            }
        }
       
        public IEnumerable<SEDES> ObtenerTodos()
        {
            try
            {
                IEnumerable<SEDES> sedes= unidad.RepositorioSede.GetAll().Where(u=>u.ESTADO==1);
                if (sedes == null)
                    return Enumerable.Empty<SEDES>();
                else
                    return sedes;
            }
            catch  {
                return Enumerable.Empty<SEDES>();
            }
        }

        public IEnumerable<SEDES> ObtenerSedesPorProfesor(decimal profesor)
        {
            try
            {
                IEnumerable<SEDES> sedes = unidad.RepositorioOfertasModulos.GetAll().Where(u => u.PROFESOR_ID == profesor).Select(a => a.SEDES).Distinct();
                if (sedes == null)
                    return Enumerable.Empty<SEDES>();
                else
                    return sedes;
            }
            catch
            {
                return Enumerable.Empty<SEDES>();
            }
        }

        public bool Duplicado(SEDES sedes)
        {
            try
            {
                if (unidad.RepositorioSede.GetAll().Where(u => u.NOMBRE.ToLower() == sedes.NOMBRE.ToLower() &&
                                                                 u.SEDE_ID!=sedes.SEDE_ID && u.ESTADO==1).Count() > 0)
                    return true;
                else
                    return false;

            }
            catch
            {
                return false;
            }
        }
       
    }
}
