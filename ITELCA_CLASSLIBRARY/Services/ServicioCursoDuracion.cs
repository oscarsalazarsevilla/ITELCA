﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using System.Data.Entity;
using ITELCA_CLASSLIBRARY.Models;
using ITELCA_CLASSLIBRARY.Repositories;
using ITELCA_CLASSLIBRARY.CustomClasses;
using System.Collections;
using System.Web;
using UmbrellaClassLibrary.CustomClasses;
using System.Data.Entity.Validation;

namespace ITELCA_CLASSLIBRARY.Services
{
     public class ServicioCursoDuracion
    {
        private UnitOfWork unidad;
        public ServicioCursoDuracion()
        {
            this.unidad = new UnitOfWork();
        }

         public int ObtenerDuracionCurso(decimal cursoId,decimal tipoTurnoId){

             try
             {
                 CURSOSDURACIONES cursoDuracion= unidad.RepositorioCursosDuraciones.Get(u => u.CURSO_ID == cursoId && u.TIPOTURNO_ID == tipoTurnoId);
                 if (cursoDuracion != null)
                     return int.Parse(cursoDuracion.NRO_MESES.Value.ToString());
                 else
                     return 0;

             }
             catch {
                 return 0;
             }
         
         
         }

         public bool Guardar(CURSOSDURACIONES cursosDuraciones)
         {
             try
             {
                 string[] arregloCursos = cursosDuraciones.MULTI_CURSO_ID.Split(',');
                 string[] arregloTurnos = cursosDuraciones.MULTI_TIPOTURNO_ID.Split(',');

                 foreach (var curso in arregloCursos)
                 {
                     foreach (var turno in arregloTurnos)
                     {
                         CURSOSDURACIONES con_cur = new CURSOSDURACIONES();
                         con_cur.FECHA_CREACION = DateTime.Now;
                         con_cur.TIPOTURNO_ID = decimal.Parse(turno);
                         con_cur.CURSO_ID = decimal.Parse(curso);
                         con_cur.NRO_MESES = cursosDuraciones.NRO_MESES;

                         CURSOSDURACIONES aux = ObtenerTodos().Where(c => c.CURSO_ID == con_cur.CURSO_ID &&
                                                                             c.TIPOTURNO_ID == con_cur.TIPOTURNO_ID).FirstOrDefault();
                         if (aux == null)
                             unidad.RepositorioCursosDuraciones.Add(con_cur);
                     }
                 }

                 unidad.Save();
                 return true;
             }
             catch (DbEntityValidationException e)
             {
                 foreach (var eve in e.EntityValidationErrors)
                 {
                     Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                         eve.Entry.Entity.GetType().Name, eve.Entry.State);
                     foreach (var ve in eve.ValidationErrors)
                     {
                         Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                             ve.PropertyName, ve.ErrorMessage);
                     }
                 }
                 return false;
             }
         }

         public bool Modificar(CURSOSDURACIONES cursosDuracion)
         {
             try
             {

                 unidad.RepositorioCursosDuraciones.Modify(cursosDuracion);
                 unidad.Save();
                 return true;
             }
             catch
             {
                 return false;
             }
         }

         public List<object> Eliminar(decimal curso_id, decimal turno_id)
         {

             List<object> lista = new List<object>();
             try
             {
                 var aux = unidad.RepositorioCursosDuraciones.Get().Where(u => u.CURSO_ID == curso_id && u.TIPOTURNO_ID == turno_id).FirstOrDefault();
                 if (aux != null)
                 {
                     lista.Add(new
                     {
                         ok = 1,
                         msg = "Duración Curso Eliminado"
                     });

                     //unidad.RepositorioCursosDuraciones.Modify(aux);
                     unidad.RepositorioCursosDuraciones.Delete(aux);
                     unidad.Save();
                     return lista;
                 }
                 else
                 {
                     lista.Add(new
                     {
                         ok = 0,
                         msg = "Duración Curso No encontrado"
                     });
                     return lista;
                 }
             }
             catch
             {
                 lista.Clear();
                 lista.Add(new
                 {
                     ok = 0,
                     msg = "Error Eliminando Duración Curso"
                 });
                 return lista;
             }

         }

         public object ObtenerDataGrid(string sidx, string sord, int page, int rows, string filters)
         {
             int totalPages = 0;
             int totalRecords = 0;
             IEnumerable<CURSOSDURACIONES> cursosDuracion;
             cursosDuracion = ObtenerTodos();
             cursosDuracion = JqGrid<CURSOSDURACIONES>.GetFilteredContent(sidx, sord, page, rows, filters, cursosDuracion.AsQueryable(), ref totalPages, ref totalRecords);

             var rowsModel = (
                 from cursoDuracion in cursosDuracion.ToList()
                 select new
                 {
                     i = cursoDuracion.CURSO_ID,
                     cell = new string[] {
                           cursoDuracion.CURSOS.NOMBRE,
                           cursoDuracion.TIPOSTURNOS.NOMBRE,
                           cursoDuracion.NRO_MESES.ToString(),
                            "<a title=\"Editar\" href=\"/CursosDuracion/Editar/"+cursoDuracion.CURSO_ID+"_"+cursoDuracion.TIPOTURNO_ID+"\"><span id=\""+cursoDuracion.CURSO_ID+"_"+cursoDuracion.TIPOTURNO_ID+"\" class=\"ui-icon ui-icon-pencil\"></a>",                          
                            "<span id=\""+cursoDuracion.CURSO_ID+"_"+cursoDuracion.TIPOTURNO_ID+"\"  class=\"ui-icon ui-icon-close\" ></span>" }
                 }).ToArray();
             return JqGrid<CURSOSDURACIONES>.SetJsonData(totalPages, totalRecords, page, rowsModel);
         }

         public IEnumerable<CURSOSDURACIONES> ObtenerTodos()
         {
             try
             {
                 IEnumerable<CURSOSDURACIONES> CURSOSDURACIONES = unidad.RepositorioCursosDuraciones.GetAll();
                 if (CURSOSDURACIONES == null)
                     return Enumerable.Empty<CURSOSDURACIONES>();
                 else
                     return CURSOSDURACIONES;
             }
             catch
             {
                 return Enumerable.Empty<CURSOSDURACIONES>();
             }
         }

         public CURSOSDURACIONES ObtenerPorId( decimal curso_id, decimal tipoturno_id)
         {
             try
             {
                 CURSOSDURACIONES CURSOSDURACIONES = unidad.RepositorioCursosDuraciones.GetAll().Where(u => u.CURSO_ID == curso_id && u.TIPOTURNO_ID == tipoturno_id).FirstOrDefault();
                 
                     return CURSOSDURACIONES;
             }
             catch
             {
                 return null;
             }
         }
       
    }
}
