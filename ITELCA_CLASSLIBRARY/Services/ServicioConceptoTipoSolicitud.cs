﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using System.Data.Entity;
using ITELCA_CLASSLIBRARY.Models;
using ITELCA_CLASSLIBRARY.Repositories;
using ITELCA_CLASSLIBRARY.CustomClasses;
using System.Collections;
using System.Web;
using UmbrellaClassLibrary.CustomClasses;
using System.Data.Entity.Validation;


namespace ITELCA_CLASSLIBRARY.Services
{
     public class ServicioConceptoTipoSolicitud
    {
        private UnitOfWork unidad;
        public ServicioConceptoTipoSolicitud()
        {
            this.unidad = new UnitOfWork();
        }
        
        public bool Guardar(CONCEPTOS_TIPOSSOLICITUDES conceptoTipoSolicitud)
        {
            try
            {
                string[] arregloTiposSolicitudes = conceptoTipoSolicitud.MULTI_TIPOSOLICITUD_ID.Split(',');
                
                foreach (var tipoSolicitud in arregloTiposSolicitudes)
                {
                    CONCEPTOS_TIPOSSOLICITUDES con_tal = new CONCEPTOS_TIPOSSOLICITUDES();
                    con_tal.FECHA_EFECTIVA = conceptoTipoSolicitud.FECHA_EFECTIVA;
                    con_tal.ESTADO = 1;
                    con_tal.CONCEPTO_ID = conceptoTipoSolicitud.CONCEPTO_ID;
                    con_tal.MONTO = conceptoTipoSolicitud.MONTO;
                    con_tal.TIPOSOLICITUD_ID = decimal.Parse(tipoSolicitud);
                    con_tal.USUARIO_ID = conceptoTipoSolicitud.USUARIO_ID;
                    con_tal.CONDICION = "C";

                    CONCEPTOS_TIPOSSOLICITUDES aux = ObtenerTodos().Where(c => c.TIPOSOLICITUD_ID == con_tal.TIPOSOLICITUD_ID &&
                                                                        c.CONCEPTO_ID == con_tal.CONCEPTO_ID &&
                                                                        c.FECHA_EFECTIVA == con_tal.FECHA_EFECTIVA).FirstOrDefault();
                    if (aux == null)
                        unidad.RepositorioConceptoTipoSolicitud.Add(con_tal);
                }

                unidad.Save();
                return true;
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                return false;
            }
        }
        public bool Modificar(CONCEPTOS_TIPOSSOLICITUDES conceptoTipoSolicitud)
        {
            try
            {
                conceptoTipoSolicitud.CONDICION = "A";
                unidad.RepositorioConceptoTipoSolicitud.Modify(conceptoTipoSolicitud);
                unidad.Save();
                return true;
            }
            catch
            {
                return false;
            }
        }


        public List<object> Eliminar(decimal id)
        {
            List<object> lista = new List<object>();
            try
            {
                var aux = unidad.RepositorioConceptoTipoSolicitud.GetById((int)id);
                if (aux != null)
                {
                    lista.Add(new
                    {
                        ok = 1,
                        msg = "Concepto - Tipo solicitud eliminado"
                    });
                    aux.ESTADO = 0;
                    aux.CONDICION = "E";
                    unidad.RepositorioConceptoTipoSolicitud.Modify(aux);
                    //unidad.RepositorioConceptoTipoSolicitud.Delete(aux);
                    unidad.Save();
                    return lista;
                }
                else
                {
                    lista.Add(new
                    {
                        ok = 0,
                        msg = "Concepto - Tipo solicitud no encontrado"
                    });
                    return lista;
                }
            }
            catch
            {
                lista.Clear();
                lista.Add(new
                {
                    ok = 0,
                    msg = "Error eliminando Concepto - Tipo solicitud"
                });
                return lista;
            }

        }

        public object ObtenerDataGrid( string sidx, string sord, int page, int rows, string filters)
        {
            int totalPages = 0;
            int totalRecords = 0;
            IEnumerable<CONCEPTOS_TIPOSSOLICITUDES> conceptosTalleres;
            conceptosTalleres = ObtenerTodos().Where(ct => ct.ESTADO == 1 || (ct.ESTADO == 0 && ct.CONDICION != "E"));
            conceptosTalleres = JqGrid<CONCEPTOS_TIPOSSOLICITUDES>.GetFilteredContent(sidx, sord, page, rows, filters, conceptosTalleres.AsQueryable(), ref totalPages, ref totalRecords);
            var rowsModel = (
                from conceptoTipoSolicitud in conceptosTalleres.ToList()
                select new
                {
                    i = conceptoTipoSolicitud.CONCEPTO_ID,
                    cell = new string[] {
                           conceptoTipoSolicitud.CONCEPTO_SOLICITUD_ID.ToString(),
                           conceptoTipoSolicitud.TIPOSSOLICITUDES.NOMBRE,
                           conceptoTipoSolicitud.CONCEPTOS.NOMBRE,
                           conceptoTipoSolicitud.FECHA_EFECTIVA.ToString("dd/MM/yyyy"),
                           String.Format("{0:N2}", conceptoTipoSolicitud.MONTO),
                           (conceptoTipoSolicitud.ESTADO == 1) ? "Activo" : "Inactivo",
                            "<a title=\"Editar\" href=\"/ConceptosTiposSolicitudes/Editar/"+ conceptoTipoSolicitud.CONCEPTO_SOLICITUD_ID+"\"><span id=\""+conceptoTipoSolicitud.CONCEPTO_SOLICITUD_ID+"\" class=\"ui-icon ui-icon-pencil\"></a>",                          
                            "<span id=\""+conceptoTipoSolicitud.CONCEPTO_SOLICITUD_ID+"\"  class=\"ui-icon ui-icon-close\" ></span>" }
                }).ToArray();
            return JqGrid<CONCEPTOS_TIPOSSOLICITUDES>.SetJsonData(totalPages, totalRecords, page, rowsModel);
        }

        public CONCEPTOS_TIPOSSOLICITUDES ObtenerPorClave(int id)
        {
            try
            {
                return unidad.RepositorioConceptoTipoSolicitud.GetById(id);
                
            }
            catch 
            {
                return null;
            }
        }

        public CONCEPTOS_TIPOSSOLICITUDES ObtenerConceptoTallerPorId(int id)
        {
            try
            {
                return unidad.RepositorioConceptoTipoSolicitud.GetById(id);

            }
            catch
            {
                return null;
            }
        }

       
        public IEnumerable<CONCEPTOS_TIPOSSOLICITUDES> ObtenerTodos()
        {
            try
            {
                IEnumerable<CONCEPTOS_TIPOSSOLICITUDES> CONCEPTOS_TIPOSSOLICITUDES = unidad.RepositorioConceptoTipoSolicitud.GetAll().Where(u=>u.ESTADO==1);
                if (CONCEPTOS_TIPOSSOLICITUDES == null)
                    return Enumerable.Empty<CONCEPTOS_TIPOSSOLICITUDES>();
                else
                    return CONCEPTOS_TIPOSSOLICITUDES;
            }
            catch  {
                return Enumerable.Empty<CONCEPTOS_TIPOSSOLICITUDES>();
            }
        }

        public bool Duplicado(CONCEPTOS_TIPOSSOLICITUDES CONCEPTOS_TIPOSSOLICITUDES)
        {
            try
            {
                if (unidad.RepositorioConceptoTipoSolicitud.GetAll().Where(ct =>    ct.CONCEPTO_ID == CONCEPTOS_TIPOSSOLICITUDES.CONCEPTO_ID &&
                                                                                    ct.TIPOSOLICITUD_ID == CONCEPTOS_TIPOSSOLICITUDES.TIPOSOLICITUD_ID &&
                                                                                    ct.CONCEPTO_SOLICITUD_ID != CONCEPTOS_TIPOSSOLICITUDES.CONCEPTO_SOLICITUD_ID &&
                                                                                    ct.ESTADO==1).Count() > 0)
                    return true;
                else
                    return false;

            }
            catch
            {
                return false;
            }
        }

        public List<LISTACONCEPTOS> ListaConceptosTiposSolicitudes(string TIPOSOLICITUD_ID)
        {

                List<LISTACONCEPTOS> listaConceptoCursos = new List<LISTACONCEPTOS>();
                Decimal tipoSolicitud_Id = decimal.Parse(TIPOSOLICITUD_ID);
                IEnumerable<CONCEPTOS_TIPOSSOLICITUDES> conceptoTipoSolicituds = unidad.RepositorioConceptoTipoSolicitud.GetAll().Where(u => u.CONCEPTO_SOLICITUD_ID == tipoSolicitud_Id && u.ESTADO==1);

                foreach (var ope in conceptoTipoSolicituds)
                {
                    LISTACONCEPTOS obj = new LISTACONCEPTOS();
                    obj.id = ope.CONCEPTO_SOLICITUD_ID.ToString();
                    obj.nombre = ope.CONCEPTOS.NOMBRE.ToString();
                    listaConceptoCursos.Add(obj);
                }

                return listaConceptoCursos;
        }

        public string ObtenerMontoLineaDetale(decimal id_conceptoTipoSolicitud, string cantidad)
        {
            CONCEPTOS_TIPOSSOLICITUDES conceptoTipoSolicitud = unidad.RepositorioConceptoTipoSolicitud.GetAll().Where(u => u.CONCEPTO_SOLICITUD_ID == id_conceptoTipoSolicitud && u.ESTADO==1).FirstOrDefault();
            return (conceptoTipoSolicitud.MONTO * Int32.Parse(cantidad)).ToString();
        }

        public CONCEPTOS_TIPOSSOLICITUDES ObtenerPorTipoSolicitud(decimal tipoSolicitud)
        {
            return ObtenerTodos().Where(c => c.TIPOSOLICITUD_ID == tipoSolicitud && c.FECHA_EFECTIVA <= DateTime.Now).OrderByDescending(c => c.FECHA_EFECTIVA).FirstOrDefault();
        }
       
    }
}
