﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using System.Data.Entity;
using ITELCA_CLASSLIBRARY.Models;
using ITELCA_CLASSLIBRARY.Repositories;
using ITELCA_CLASSLIBRARY.CustomClasses;
using System.Collections;
using System.Web;
using UmbrellaClassLibrary.CustomClasses;

namespace ITELCA_CLASSLIBRARY.Services
{
     public class ServicioCuenta
    {
        private UnitOfWork unidad;
        public ServicioCuenta()
        {
            this.unidad = new UnitOfWork();
        }
        public bool Guardar(CUENTAS cuenta)
        {
            try
            {
                unidad.RepositorioCuentas.Add(cuenta);
                unidad.Save();
                return true;
            }
            catch
            {
                return false;
            }
        }
        public bool Modificar(CUENTAS cuenta)
        {
            try
            {
                unidad.RepositorioCuentas.Modify(cuenta);
                unidad.Save();
                return true;
            }
            catch
            {
                return false;
            }
        }


        public List<object> Eliminar(decimal id)
        {
            List<object> lista = new List<object>();
            try
            {
                var aux = unidad.RepositorioCuentas.GetById((int)id);
                if (aux != null)
                {
                    lista.Add(new
                    {
                        ok = 1,
                        msg = "Cuenta eliminada"
                    });
                    unidad.RepositorioCuentas.Delete(aux);
                    unidad.Save();
                    return lista;
                }
                else
                {
                    lista.Add(new
                    {
                        ok = 0,
                        msg = "Cuenta no encontrada"
                    });
                    return lista;
                }
            }
            catch
            {
                lista.Add(new
                {
                    ok = 0,
                    msg = "Error eliminando la cuenta"
                });
                return lista;
            }

        }

        public CUENTAS ObtenerPorClave(int id)
        {
            try
            {
                return unidad.RepositorioCuentas.GetById(id);
                
            }
            catch 
            {
                return null;
            }
        }

        public CUENTAS ObtenerPorClave(string cuenta)
        {
            try
            {
                return ObtenerTodos().Where(c => c.CUENTA == cuenta).FirstOrDefault();

            }
            catch
            {
                return null;
            }
        }

       
        public IEnumerable<CUENTAS> ObtenerTodos()
        {
            try
            {
                IEnumerable<CUENTAS> cuentas = unidad.RepositorioCuentas.GetAll();
                if (cuentas == null)
                    return Enumerable.Empty<CUENTAS>();
                else
                    return cuentas;
            }
            catch {
                return Enumerable.Empty<CUENTAS>();
            }
        }       
    }
}
