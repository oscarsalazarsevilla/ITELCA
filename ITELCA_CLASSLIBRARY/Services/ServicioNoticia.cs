﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using System.Data.Entity;
using ITELCA_CLASSLIBRARY.Models;
using ITELCA_CLASSLIBRARY.Repositories;
using ITELCA_CLASSLIBRARY.CustomClasses;
using System.Collections;
using System.Web;
using UmbrellaClassLibrary.CustomClasses;

namespace ITELCA_CLASSLIBRARY.Services
{
    public class ServicioNoticia
    {
        private UnitOfWork unidad;
        public ServicioNoticia()
        {
            this.unidad = new UnitOfWork();
        }
        public bool Guardar(NOTICIAS noticia)
        {
            try
            {
                noticia.CONDICION = "C";
                unidad.RepositorioNoticias._dataContext.Configuration.ValidateOnSaveEnabled = false;
                unidad.RepositorioNoticias.Add(noticia);
                unidad.Save();
                return true;
            }
            catch
            {
                return false;
            }
        }
        public bool Modificar(NOTICIAS noticia)
        {
            try
            {
                noticia.CONDICION = "A";
                unidad.RepositorioNoticias._dataContext.Configuration.ValidateOnSaveEnabled = false;
                unidad.RepositorioNoticias.Modify(noticia);
                unidad.Save();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public NOTICIAS ObtenerPorClave(int id)
        {
            try
            {
                return unidad.RepositorioNoticias.GetById(id);

            }
            catch
            {
                return null;
            }
        }

        public IEnumerable<NOTICIAS> ObtenerTodos()
        {
            try
            {
                IEnumerable<NOTICIAS> noticias = unidad.RepositorioNoticias.GetAll().Where(u=>u.ESTADO==1).OrderByDescending(M => M.FECHA_CREACION);
                if (noticias == null)
                    return Enumerable.Empty<NOTICIAS>();
                else
                    return noticias;
            }
            catch
            {
                return Enumerable.Empty<NOTICIAS>();
            }
        }
        public IEnumerable<NOTICIAS> ObtenerTodos_Activos()
        {
            try
            {
                IEnumerable<NOTICIAS> noticias = unidad.RepositorioNoticias.GetAll().Where(M => M.ESTADO==1).OrderByDescending(M => M.FECHA_CREACION);
                if (noticias == null)
                    return Enumerable.Empty<NOTICIAS>();
                else
                    return noticias;
            }
            catch
            {
                return Enumerable.Empty<NOTICIAS>();
            }
        }
        public bool Duplicado(NOTICIAS noticias)
        {
            try
            {
                if (unidad.RepositorioNoticias.GetAll().Where(u => u.TITULO.ToLower() == noticias.TITULO.ToLower() &&
                                                                 u.NOTICIA_ID != noticias.NOTICIA_ID && u.ESTADO==1).Count() > 0)
                    return true;
                else
                    return false;

            }
            catch
            {
                return false;
            }
        }

        public List<object> Eliminar(decimal id)
        {
            List<object> lista = new List<object>();
            try
            {
                var aux = unidad.RepositorioNoticias.GetById((int)id);
                if (aux != null)
                {
                    lista.Add(new
                    {
                        ok = 1,
                        msg = "Noticia Eliminada"
                    });
                    aux.ESTADO = 0;
                    aux.CONDICION = "E";
                    unidad.RepositorioNoticias.Modify(aux);
                    //taunidad.RepositorioNoticias.Delete(aux);
                    unidad.Save();
                    return lista;
                }
                else
                {
                    lista.Add(new
                    {
                        ok = 0,
                        msg = "Noticia No encontrada"
                    });
                    return lista;
                }
            }
            catch
            {
                lista.Clear();
                lista.Add(new
                {
                    ok = 0,
                    msg = "Error Eliminando Noticia"
                });
                return lista;
            }

        }

        public object ObtenerDataGrid(string sidx, string sord, int page, int rows, string filters)
        {
            int totalPages = 0;
            int totalRecords = 0;
            IEnumerable<NOTICIAS> noticias;
            noticias = unidad.RepositorioNoticias.GetAll().Where(u => u.ESTADO == 1 || (u.ESTADO == 0 && u.CONDICION != "E"));
            noticias = JqGrid<NOTICIAS>.GetFilteredContent(sidx, sord, page, rows, filters, noticias.AsQueryable(), ref totalPages, ref totalRecords);
           
            var rowsModel = (
                from noticia in noticias.ToList()
                select new
                {
                    i = noticia.NOTICIA_ID,
                    cell = new string[] {
                           noticia.NOTICIA_ID.ToString(),
                           noticia.TITULO.ToString(),
                           noticia.RESUMEN.ToString(),
                           noticia.FECHA_PUBLICACION.ToString("dd/MM/yyyy"),
                           (noticia.FECHA_FIN_PUBLICACION.HasValue) ? noticia.FECHA_FIN_PUBLICACION.Value.ToString("dd/MM/yyyy") : "---",
                           noticia.ESTADO.ToString(),
                            "<a title=\"Editar\" href=\"/Noticia/Editar/"+  noticia.NOTICIA_ID+"\"><span id=\""+  noticia.NOTICIA_ID+"\" class=\"ui-icon ui-icon-pencil\"></a>",                          
                            "<span id=\""+  noticia.NOTICIA_ID+"\"  class=\"ui-icon ui-icon-close\" ></span>" }
                }).ToArray();
            return JqGrid<NOTICIAS>.SetJsonData(totalPages, totalRecords, page, rowsModel);
        }
    }
}
