﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using System.Data.Entity;
using ITELCA_CLASSLIBRARY.Models;
using ITELCA_CLASSLIBRARY.Repositories;
using ITELCA_CLASSLIBRARY.CustomClasses;
using System.Collections;
using System.Web;
using UmbrellaClassLibrary.CustomClasses;

namespace ITELCA_CLASSLIBRARY.Services
{
     public class ServicioPago
    {
        private UnitOfWork unidad;
        public ServicioPago()
        {
            this.unidad = new UnitOfWork();
        }
        public bool Guardar(PAGOS Pago)
        {
            try
            {
                Pago.CONDICION = "C";
                unidad.RepositorioPagos.Add(Pago);
                unidad.Save();
                return true;
            }
            catch
            {
                return false;
            }
        }
        public bool Modificar(PAGOS entity)
        {
            try
            {
                entity.CONDICION = "A";
                unidad.RepositorioPagos.Modify(entity);
                unidad.Save();
                return true;
            }
            catch
            {
                return false;
            }
        }

       

        public PAGOS ObtenerPorClave(int id)
        {
            try
            {
                return unidad.RepositorioPagos.GetById(id);
                
            }
            catch
            {
                return null;
            }
        }

        public List<PAGOS> ObtenerPagosPorTipoRecibo(decimal tipoRecibo,bool ban,decimal userId) {
            try {
                if(!ban)
                    return unidad.RepositorioPagos.GetAll().Where(u => u.RECIBOS.TIPODOCUMENTO == tipoRecibo && u.FECHA_CREACION >= DateTime.Today && u.USUARIO_ID==userId).ToList();
                else
                    return unidad.RepositorioPagos.GetAll().Where(u => u.RECIBOS.TIPODOCUMENTO == tipoRecibo && u.FECHA_CREACION >= DateTime.Today ).ToList();
            }
            catch {
                return null;
            }
        
        }
      
    }
}
