﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using System.Data.Entity;
using ITELCA_CLASSLIBRARY.Models;
using ITELCA_CLASSLIBRARY.Repositories;
using ITELCA_CLASSLIBRARY.CustomClasses;
using System.Collections;
using System.Web;
using UmbrellaClassLibrary.CustomClasses;

namespace ITELCA_CLASSLIBRARY.Services
{
     public class ServicioCorrelativo
    {
        private UnitOfWork unidad;
        public ServicioCorrelativo()
        {
            this.unidad = new UnitOfWork();
        }
        public bool Guardar(CORRELATIVOS correlativo)
        {
            try
            {
                unidad.RepositorioCorrelativos.Add(correlativo);
                unidad.Save();
                return true;
            }
            catch
            {
                return false;
            }
        }
        public bool Modificar(CORRELATIVOS correlativo)
        {
            try
            {
                unidad.RepositorioCorrelativos.Modify(correlativo);
                unidad.Save();
                return true;
            }
            catch
            {
                return false;
            }
        }


        public List<object> Eliminar(decimal id)
        {
            List<object> lista = new List<object>();
            return lista;
            //try
            //{
            //    var aux = unidad.RepositorioCaja.GetById((int)id);
            //    if (aux != null)
            //    {
            //        lista.Add(new
            //        {
            //            ok = 1,
            //            msg = "Caja Eliminada"
            //        });
            //        aux.CONDICION = "E";
            //        aux.ESTADO = 0;
            //        unidad.RepositorioCaja.Modify(aux);
            //        //unidad.RepositorioCaja.Delete(aux);
            //        unidad.Save();
            //        return lista;
            //    }
            //    else
            //    {
            //        lista.Add(new
            //        {
            //            ok = 0,
            //            msg = "Caja No encontrada"
            //        });
            //        return lista;
            //    }
            //}
            //catch
            //{
            //    lista.Add(new
            //    {
            //        ok = 0,
            //        msg = "Error Eliminando Caja"
            //    });
            //    return lista;
            //}

        }

    

        public object ObtenerDataGrid( string sidx, string sord, int page, int rows, string filters)
        {
            int totalPages = 0;
            int totalRecords = 0;
            IEnumerable<CORRELATIVOS> correlativos;
            correlativos = unidad.RepositorioCorrelativos.GetAll();
            correlativos = JqGrid<CORRELATIVOS>.GetFilteredContent(sidx, sord, page, rows, filters, correlativos.AsQueryable(), ref totalPages, ref totalRecords);
           
            var rowsModel = (
                from correlativo in correlativos.ToList()
                select new
                {
                    i = correlativo.CORRELATIVO_ID,
                    cell = new string[] {
                           correlativo.CORRELATIVO_ID.ToString(),
                           correlativo.SEDES.NOMBRE.ToString(),
                           correlativo.TIPO_DOCUMENTO=="PP"?"RECIBO DE PAGO PRODUCTO":"RECIBO PAGO MENSUALIDAD",
                           correlativo.NRO_CONTROL.ToString(),
                            "<a title=\"Editar\" href=\"/Correlativo/Editar/"+ correlativo.CORRELATIVO_ID+"\"><span id=\""+correlativo.CORRELATIVO_ID+"\" class=\"ui-icon ui-icon-pencil\"></a>",                          
                            }
                }).ToArray();
            return JqGrid<CORRELATIVOS>.SetJsonData(totalPages, totalRecords, page, rowsModel);
        }

        public CORRELATIVOS ObtenerPorClave(int id)
        {
            try
            {
                CORRELATIVOS corre = unidad.RepositorioCorrelativos.Get(u => u.CORRELATIVO_ID==id);
                if (corre != null)
                    return corre;
                return null;
            }
            catch
            {
                return null;
            }
        }

        public string ObtenerCorrelativo(string documento,decimal sede)
        {
            try
            {
                CORRELATIVOS corre= unidad.RepositorioCorrelativos.Get(u=>u.TIPO_DOCUMENTO==documento && u.SEDE_ID.Value==sede);
                if (corre != null)
                    return corre.NRO_CONTROL.ToString();
                return "0";
            }
            catch 
            {
                return "0";
            }
        }

        public IEnumerable<CORRELATIVOS> ObtenerTodos()
        {
            try
            {
                IEnumerable<CORRELATIVOS> correlativo = unidad.RepositorioCorrelativos.GetAll();
                if (correlativo == null)
                    return Enumerable.Empty<CORRELATIVOS>();
                else
                    return correlativo;
            }
            catch
            {
                return Enumerable.Empty<CORRELATIVOS>();
            }
        }
        public bool IncrementarCorrelativo(string documento, decimal sede)
        {
            try
            {
                CORRELATIVOS corre = unidad.RepositorioCorrelativos.Get(u => u.TIPO_DOCUMENTO == documento && u.SEDE_ID.Value == sede);
                if (corre != null)
                {
                    corre.NRO_CONTROL += 1;
                    unidad.RepositorioCorrelativos.Modify(corre);
                    unidad.Save();
                    return true;
                }
                else
                    return false;
                
            }
            catch
            {
                return false;
            }
        }

        public bool Duplicado(CORRELATIVOS correlativo, string accion)
        {
            try
            {
                if (accion == "C")
                {
                    if (ObtenerTodos().Where(c => c.TIPO_DOCUMENTO.ToLower() == correlativo.TIPO_DOCUMENTO.ToLower() &&
                                                    c.SEDE_ID == correlativo.SEDE_ID).Count() > 0)
                        return true;
                    else
                        return false;
                }
                else
                {
                    if (ObtenerTodos().Where(c => c.TIPO_DOCUMENTO.ToLower() == correlativo.TIPO_DOCUMENTO.ToLower() &&
                                                    c.SEDE_ID == correlativo.SEDE_ID &&
                                                    c.CORRELATIVO_ID != correlativo.CORRELATIVO_ID).Count() > 0)
                        return true;
                    else
                        return false;
                }

            }
            catch
            {
                return false;
            }
        }
       
    }
}
