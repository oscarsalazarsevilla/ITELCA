﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using System.Data.Entity;
using ITELCA_CLASSLIBRARY.Models;
using ITELCA_CLASSLIBRARY.Repositories;
using ITELCA_CLASSLIBRARY.CustomClasses;
using System.Collections;
using System.Web;
using UmbrellaClassLibrary.CustomClasses;

namespace ITELCA_CLASSLIBRARY.Services
{
     public class ServicioAsistenciaTaller
    {
        private UnitOfWork unidad;
        public ServicioAsistenciaTaller()
        {
            this.unidad = new UnitOfWork();
        }
        public bool Guardar(ASISTENCIASTALLER asistenciaTaller)
        {
            try
            {
                unidad.RepositorioAsistenciaTalleres.Add(asistenciaTaller);
                unidad.Save();
                return true;
            }
            catch
            {
                return false;
            }
        }
        public bool Modificar(ASISTENCIASTALLER asistenciaTaller)
        {
            try
            {

                unidad.RepositorioAsistenciaTalleres.Modify(asistenciaTaller);
                unidad.Save();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public ASISTENCIASTALLER ObtenerPorClave(decimal ofertaTallerId,decimal alumnoId)
        {
            try
            {
                return unidad.RepositorioAsistenciaTalleres.GetAll().Where(u=>u.ALUMNO_ID==alumnoId &&
                                                                           u.OFERTA_TALLER_ID == ofertaTallerId).FirstOrDefault();

            }
            catch
            {
                return null;
            }
        }
      

         
        public List<object> Eliminar(decimal id)
        {
            List<object> lista = new List<object>();
            try
            {
                var aux = unidad.RepositorioAula.GetById((int)id);
                if (aux != null)
                {
                    lista.Add(new
                    {
                        ok = 1,
                        msg = "Aula Eliminada"
                    });
                    unidad.RepositorioAula.Delete(aux);
                    unidad.Save();
                    return lista;
                }
                else
                {
                    lista.Add(new
                    {
                        ok = 0,
                        msg = "Aula No encontrada"
                    });
                    return lista;
                }
            }
            catch
            {
                lista.Add(new
                {
                    ok = 0,
                    msg = "Error Eliminando Aula"
                });
                return lista;
            }

        }
      
         public List<object> ObtenerTablaAsistencia( decimal ofertaTallerId){
              List<object> tablaAsistencia=new List<object>();
             try{
                 IEnumerable<MATRICULASTALLERES> listaInscritos = unidad.RepositorioMatriculaTalleres.GetAll().Where(u => u.OFERTA_TALLER_ID == ofertaTallerId);
                                                                                                                     
                 if (listaInscritos != null)
                 {
                     MATRICULASTALLERES alumno = listaInscritos.FirstOrDefault();
                     IEnumerable<ASISTENCIASTALLER> listadoAsistencia = unidad.RepositorioAsistenciaTalleres.GetAll().Where(u => u.OFERTA_TALLER_ID == alumno.OFERTA_TALLER_ID);


                     var todo = 0;var asistio = 0;                                                                            
                    if(listadoAsistencia.Where(u => u.ASISTIO==1
                                                    ).Count() == listaInscritos.Count())
                        todo=1;
                    else
                        todo= 0;
                    tablaAsistencia.Add(new
                    {
                        tipo = "cabecera",
                        fecha = alumno.OFERTASTALLERES.FECHA_INICIO.ToShortDateString(),
                        id = "all",
                        all = todo
                    });

                   
                     foreach (var ope in listaInscritos)
                     {
                        ASISTENCIASTALLER asistente= listadoAsistencia.Where(u=>ope.ALUMNO_ID == u.ALUMNO_ID).FirstOrDefault();
                        if (asistente != null)
                            asistio = (int)asistente.ASISTIO; 
                        tablaAsistencia.Add(new
                        {
                            tipo = "detalle",
                            fecha = ope.OFERTASTALLERES.FECHA_INICIO.ToShortDateString(),
                            nombre = ope.USUARIOS.NOMBRE + " " + ope.USUARIOS.APELLIDO,
                            id = alumno.OFERTA_TALLER_ID + "_" + alumno.ALUMNO_ID,
                            alumno = ope.ALUMNO_ID,
                            asistio = asistio
                        });
                        asistio = 0;
                    }
                     
                 }
                 return tablaAsistencia;
                }catch{
                    return null;
                }
         }
      
    }
     
}
