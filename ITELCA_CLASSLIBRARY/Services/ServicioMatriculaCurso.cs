﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using System.Data.Entity;
using ITELCA_CLASSLIBRARY.Models;
using ITELCA_CLASSLIBRARY.Repositories;
using ITELCA_CLASSLIBRARY.CustomClasses;
using System.Collections;
using System.Web;
using UmbrellaClassLibrary.CustomClasses;
using System.Data.Objects;
using System.Data.Entity.Validation;


namespace ITELCA_CLASSLIBRARY.Services
{
     public class ServicioMatriculaCurso
    {
        private UnitOfWork unidad;
        public ServicioMatriculaCurso()
        {
            this.unidad = new UnitOfWork();
        }
        public bool Guardar(MATRICULASCURSOS matriculaCurso)
        {
            try
            {

                unidad.RepositorioMatriculaCurso.Add(matriculaCurso);
                
                unidad.Save();
                return true;
            }
            catch
            {
                return false;
            }
        }
        public MATRICULASCURSOS GuardarPK(MATRICULASCURSOS matriculaCurso)
        {
            try
            {

                unidad.RepositorioMatriculaCurso.Add(matriculaCurso);
                unidad.Save();
                return matriculaCurso;
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                return null;
            }
        }
        public bool Modificar(MATRICULASCURSOS matriculaCurso)
        {
            try
            {

                unidad.RepositorioMatriculaCurso.Modify(matriculaCurso);
                unidad.Save();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public List<object> EliminarMatricula(decimal id)
        {
            List<object> lista = new List<object>();
            try
            { 
                var aux = unidad.RepositorioMatriculaCurso.GetById((int)id);
                var recibosGenerados = unidad.RepositorioDetallesRecibos.GetAll().Where(u => u.CONCEPTOS_ESTUDIANTES.MATRICULA_CURSO_ID == aux.MATRICULA_CURSO_ID).Count();
                var conceptosCancelados = unidad.RepositorioConceptosEstudiantes.GetAll().Where(u => u.MATRICULA_CURSO_ID == aux.MATRICULA_CURSO_ID && u.CONDICION == "C").Count();
                if (aux != null && recibosGenerados == 0 && conceptosCancelados == 0)
                {
                    //Elimino Pagos
                    unidad.RepositorioPagos.Delete(u => u.RECIBOS.DETALLESRECIBOS.Any( a => a.CONCEPTOS_ESTUDIANTES.MATRICULA_CURSO_ID == aux.MATRICULA_CURSO_ID));

                    //Elimino Detalles Recibos
                    unidad.RepositorioDetallesRecibos.Delete(u => u.CONCEPTOS_ESTUDIANTES.MATRICULA_CURSO_ID == aux.MATRICULA_CURSO_ID);

                    //Elimino Conceptos Estudiantes
                    unidad.RepositorioConceptosEstudiantes.Delete(u => u.MATRICULA_CURSO_ID == aux.MATRICULA_CURSO_ID);
      
                    //Elimino Solicitudes
                    unidad.RepositorioSolicitudes.Delete(u => u.MATRICULA_CURSO_ID == aux.MATRICULA_CURSO_ID);

                    //Elimino Matricula Curso
                    unidad.RepositorioMatriculaCurso.Delete(aux);
                    unidad.Save();

                    lista.Add(new
                    {
                        ok = 1,
                        msg = "Matrícula Eliminada"
                    });
                    return lista;
                }
                else
                {
                    if (aux == null)
                    {
                        lista.Add(new
                        {
                            ok = 0,
                            msg = "Matrícula No encontrada"
                        });
                    }
                    else if (recibosGenerados > 0)
                    {
                        lista.Add(new
                        {
                            ok = 0,
                            msg = "Hay recibos Generados para esta matrícula."
                        });
                    
                    }
                    else if (conceptosCancelados > 0)
                    {
                        lista.Add(new
                        {
                            ok = 0,
                            msg = "Hay Conceptos cancelados para esta matrícula."
                        });

                    }
                    return lista;
                }
            }
            catch
            {
                lista.Clear();
                lista.Add(new
                {
                    ok = 0,
                    msg = "Error Eliminando Matricula"
                });
                return lista;
            }

        }

        public List<object> Eliminar(decimal id)
        {
            List<object> lista = new List<object>();
            try
            {
                var aux = unidad.RepositorioMatriculaCurso.GetById((int)id);
                if (aux != null)
                {
                    lista.Add(new
                    {
                        ok = 1,
                        msg = "Matricula Eliminada"
                    });
                    unidad.RepositorioMatriculaCurso.Delete(aux);
                    unidad.Save();
                    return lista;
                }
                else
                {
                    lista.Add(new
                    {
                        ok = 0,
                        msg = "Matricula No encontrado"
                    });
                    return lista;
                }
            }
            catch
            {
                lista.Clear();
                lista.Add(new
                {
                    ok = 0,
                    msg = "Error Eliminando Matricula"
                });
                return lista;
            }

        }

        public object ObtenerDataGrid( decimal idAlumno,string sidx, string sord, int page, int rows, string filters)
        {
            int totalPages = 0;
            int totalRecords = 0;
            IEnumerable<MATRICULASCURSOS> matriculasCursos;
            if(idAlumno!=0)
                matriculasCursos =unidad.RepositorioMatriculaCurso.GetAll().Where(u=>u.ALUMNO_ID==idAlumno).OrderByDescending(u=>u.FECHA_CREACION);
            else
                matriculasCursos = unidad.RepositorioMatriculaCurso.GetAll().OrderByDescending(u=>u.FECHA_CREACION);

            matriculasCursos = JqGrid<MATRICULASCURSOS>.GetFilteredContent(sidx, sord, page, rows, filters, matriculasCursos.AsQueryable(), ref totalPages, ref totalRecords);

            var rowsModel = (
                from matricula in matriculasCursos.ToList()
                select new
                {
                    i = matricula.OFERTA_CURSO_ID,
                    cell = new string[] {
                           matricula.OFERTA_CURSO_ID.ToString(),
                           matricula.USUARIOS.NOMBRE+" "+matricula.USUARIOS.APELLIDO,
                           matricula.USUARIOS.NRO_DOCUMENTO.ToString(),
                           matricula.CURSOS.NOMBRE,
                           matricula.SEDES.NOMBRE,
                           matricula.TURNOS.NOMBRE,
                           matricula.FECHA_INSCRIPCION.HasValue?matricula.FECHA_INSCRIPCION.Value.ToShortDateString():"",
                           matricula.DIA_CORTE.HasValue?matricula.DIA_CORTE.Value.ToString():"",
                           matricula.USUARIOS1!=null?matricula.USUARIOS2.NOMBRE+" "+matricula.USUARIOS2.APELLIDO:"",
                           "",
                           "<span id=\""+matricula.MATRICULA_CURSO_ID+"\"  class=\"ui-icon ui-icon-circle-plus\" ></span>",
                           "<span id=\""+matricula.MATRICULA_CURSO_ID+"\"  class=\"ui-icon ui-icon-close\" ></span>" }
                }).ToArray();
            return JqGrid<OFERTASMODULOS>.SetJsonData(totalPages, totalRecords, page, rowsModel);
        }

        public MATRICULASCURSOS ObtenerPorClave(int id)
        {
            try
            {
                return unidad.RepositorioMatriculaCurso.GetById(id);
                
            }
            catch 
            {
                return null;
            }
        }

        public MATRICULASCURSOS ObtenerPorClaves(int oferta_curso_id, int alumno_id)
        {
            try
            {
                return unidad.RepositorioMatriculaCurso.GetAll().Where(u => u.OFERTA_CURSO_ID == oferta_curso_id && u.ALUMNO_ID == alumno_id).FirstOrDefault();

            }
            catch
            {
                return null;
            }
        }
        public IEnumerable<MATRICULASCURSOS> ObtenerTodos()
        {
            try
            {
                IEnumerable<MATRICULASCURSOS> matriculaCurso = unidad.RepositorioMatriculaCurso.GetAll();
                if (matriculaCurso == null)
                    return Enumerable.Empty<MATRICULASCURSOS>();
                else
                    return matriculaCurso;
            }
            catch  {
                return Enumerable.Empty<MATRICULASCURSOS>();
            }
        }

        public MATRICULASCURSOS ObtenerMatriculaCursoPreInscritoPorAlumno(decimal documento, decimal curso_id, decimal turno_id)
        {

            try
            {
                USUARIOS usuario = new ServicioUsuario().ObtenerObjetoPorCedula(documento.ToString());
                MATRICULASCURSOS matriculaCursos = unidad.RepositorioMatriculaCurso.GetAll().Where(u => u.ALUMNO_ID == usuario.USUARIO_ID
                                                    && u.CURSO_ID == curso_id && u.TURNO_ID == turno_id
                                                   ).FirstOrDefault();


                return matriculaCursos;
            }
            catch {

                return null;
            }


        }

        public MATRICULASCURSOS ObtenerMatriculaCursoPorAlumno(decimal alumno_id, decimal curso_id, decimal turno_id,decimal sede_id)
        {

            try
            {
                MATRICULASCURSOS matriculaCursos = unidad.RepositorioMatriculaCurso.GetAll().Where(u => u.ALUMNO_ID == alumno_id
                                                    && u.CURSO_ID == curso_id && u.TURNO_ID == turno_id && sede_id==u.SEDE_ID 
                                                   ).FirstOrDefault();


                return matriculaCursos;
            }
            catch
            {

                return null;
            }


        }
        public MATRICULASCURSOS ValidarAlumnoInscritoEnCurso(decimal alumnoId, decimal curso_id, decimal turno_id)
        {

            try
            {
                //C=CURSANDO R=RETIRADO F=CONGELADO P=PRE-INSCRITO 
                MATRICULASCURSOS matriculaCursos = unidad.RepositorioMatriculaCurso.GetAll().Where(u => u.ALUMNO_ID == alumnoId
                                                    && u.CURSO_ID == curso_id && u.TURNO_ID == turno_id && u.CONDICION=="C"
                                                   ).FirstOrDefault();


                return matriculaCursos;
            }
            catch
            {

                return null;
            }


        }
        
        public List<LISTACURSOS> ObtenerCursosPreInscritosPorAlumno(decimal documento)
        {

            List<LISTACURSOS> listaCursos = new List<LISTACURSOS>();

           USUARIOS usuario = new ServicioUsuario().ObtenerObjetoPorCedula(documento.ToString());
           IEnumerable<MATRICULASMODULOS> matriculaCursos = unidad.RepositorioMatriculaModulo.GetAll().Where(u => u.ALUMNO_ID == usuario.USUARIO_ID && u.CONDICION == "P");
           
            foreach (var ope in matriculaCursos)
                {
                    LISTACURSOS obj = new LISTACURSOS();
                    obj.id = ope.OFERTASMODULOS.OFERTASCURSOS.CURSO_ID.ToString();
                    obj.nombre = ope.OFERTASMODULOS.OFERTASCURSOS.CURSOS.NOMBRE.ToString();
                    listaCursos.Add(obj);
                }

            return listaCursos;
              

        }

        public List<LISTACURSOS> ObtenerCursosInscritosPorAlumno(decimal documento)
        {

            USUARIOS usuario = new ServicioUsuario().ObtenerObjetoPorCedula(documento.ToString());
            IEnumerable<MATRICULASCURSOS> matriculaCursos = unidad.RepositorioMatriculaCurso.GetAll().Where(u => u.ALUMNO_ID == usuario.USUARIO_ID && u.CONDICION == "C").OrderBy(u=>u.CURSOS.NOMBRE);

            List<LISTACURSOS> listaCursos = new List<LISTACURSOS>();

            
            if (usuario != null && matriculaCursos != null)
            {
                foreach (var ope in matriculaCursos)
                {
                    LISTACURSOS obj = new LISTACURSOS();
                    obj.id = ope.MATRICULA_CURSO_ID.ToString();
                    obj.nombre = ope.OFERTASCURSOS.CURSOS.NOMBRE.ToString();
                    listaCursos.Add(obj);
                }

                return listaCursos;
            }
            else {
                if (usuario == null)
                {
                    LISTACURSOS obj = new LISTACURSOS();
                    obj.id = "error";
                    obj.nombre = "Usuario No encontrado.";
                    listaCursos.Add(obj);
                    return listaCursos;
                }
                else{
                    LISTACURSOS obj = new LISTACURSOS();
                    obj.id = "error";
                    obj.nombre = "Matriculas Cursos No encontrada.";
                    listaCursos.Add(obj);
                    return listaCursos;
                }
            }

        }

        public bool ObtenerNotasporAlumnoyCurso(decimal documento, int matriculaCurso)
        {

            List<LISTACURSOS> listaCursos = new List<LISTACURSOS>();

            MATRICULASCURSOS matriculaCursos = this.ObtenerPorClave(matriculaCurso);
            foreach (var modulo in matriculaCursos.MATRICULASMODULOS) {
                
                if (modulo.NOTASMODULO.Count != 0)
                {
                    //if (modulo.NOTASMODULO.FirstOrDefault().NOTA > modulo.MODULOS.MINIMO_APROBATORIO) 
                    //{
                        return true;
                    //}
                    
                }
            }
            return false;

        }

        public Dictionary<string, string> ObtenerTodosPorAlumno(decimal idAlumno)
        {

            Dictionary<string, string> Dictionary = new Dictionary<string, string>();
            var lista = unidad.RepositorioMatriculaCurso.GetAll().Where(u=>u.ALUMNO_ID==idAlumno);
            foreach (var matricula in lista)
            {
                Dictionary.Add(matricula.MATRICULA_CURSO_ID.ToString(), matricula.CURSOS.NOMBRE.ToString() + " - "+ matricula.TURNOS.NOMBRE.ToString());

            }
            return Dictionary;
        }

        public IEnumerable<decimal> ObtenerCursosActivosPorAlumno(decimal idAlumno)
        {
            try
            {
                //condicion de curso:pre-inscrito,cursando,anulado,aprobado
                IEnumerable<decimal> idCursos = unidad.RepositorioMatriculaCurso.GetAll().Where(u=>u.ALUMNO_ID==idAlumno && u.CONDICION=="C").Select(u=>u.CURSO_ID.HasValue?u.CURSO_ID.Value:-1M);
                if (idCursos == null)
                    return Enumerable.Empty<decimal>();
                else
                    return idCursos;
            }
            catch
            {
                return Enumerable.Empty<decimal>();
            }
        }

        public IEnumerable<decimal> ObtenerModulosActivosPorAlumno(decimal idAlumno)
        {
            try
            {
                //condicion de curso:pre-inscrito,cursando,anulado,aprobado
                IEnumerable<decimal> idModulos = unidad.RepositorioMatriculaModulo.GetAll().Where(u => u.ALUMNO_ID == idAlumno && u.CONDICION == "C").Select(u => u.OFERTA_MODULO_ID);
                if (idModulos == null)
                    return Enumerable.Empty<decimal>();
                else
                    return idModulos;
            }
            catch
            {
                return Enumerable.Empty<decimal>();
            }
        }
        public IEnumerable<MATRICULASCURSOS> ObtenerObjetoCursosActivosPorAlumno(decimal idAlumno)
        {
            try
            {
                //condicion de curso:pre-inscrito,cursando,anulado,aprobado
                IEnumerable<MATRICULASCURSOS> cursos = unidad.RepositorioMatriculaCurso.GetAll().Where(u => u.ALUMNO_ID == idAlumno && u.CONDICION=="C");
                if (cursos == null)
                    return Enumerable.Empty<MATRICULASCURSOS>();
                else
                    return cursos;
            }
            catch
            {
                return Enumerable.Empty<MATRICULASCURSOS>();
            }
        }
        public IEnumerable<IGrouping<decimal?, MATRICULASCURSOS>> ObtenerComisionesTodos(string fecha_desde, string fecha_hasta)
        {
            try
            {
                DateTime desde = Convert.ToDateTime(fecha_desde);
                DateTime hasta = Convert.ToDateTime(fecha_hasta);

                return unidad.RepositorioMatriculaCurso.GetAll().Where(u => u.FECHA_INSCRIPCION > desde && u.FECHA_INSCRIPCION < hasta).GroupBy(u => u.EMPLEADO_ID); 
            }
            catch
            {
                return null;
            }
        }


        public List<LISTACURSOS> ObtenerAlumnosPorOfertaCurso(decimal? sede, decimal? curso, decimal? turno)
        {

            List<LISTACURSOS> listaCursos = new List<LISTACURSOS>();


            IEnumerable<MATRICULASCURSOS> matriculaCursos = unidad.RepositorioMatriculaCurso.GetAll().Where(u => u.CURSO_ID == curso && u.TURNO_ID == turno && u.SEDE_ID == sede).OrderBy(u=>u.USUARIOS.NOMBRE);

            foreach (var ope in matriculaCursos)
            {
                LISTACURSOS obj = new LISTACURSOS();
                obj.id = ope.ALUMNO_ID.ToString();
                obj.nombre = ope.USUARIOS.NOMBRE.ToString() + " " + ope.USUARIOS.APELLIDO.ToString() + " " + ope.USUARIOS.NRO_DOCUMENTO.ToString();
                listaCursos.Add(obj);
            }

            
            return listaCursos;
        }

        public IEnumerable<MATRICULASCURSOS> ObtenerInscritosDelDia(bool esAdmin, decimal userId)
        {
            try
            {
                //DateTime desde = Convert.ToDateTime(fecha_desde);
                //DateTime hasta = Convert.ToDateTime(fecha_hasta);
                if (!esAdmin)
                    return unidad.RepositorioMatriculaCurso.GetAll().Where(u => EntityFunctions.TruncateTime(u.FECHA_INSCRIPCION.Value) == DateTime.Today && u.USUARIO_ID == userId);
                else
                    return unidad.RepositorioMatriculaCurso.GetAll().Where(u => EntityFunctions.TruncateTime(u.FECHA_INSCRIPCION.Value) == DateTime.Today); 

            }
            catch
            {
                return null;
            }
        }


        public List<LISTACONCEPTOS> ObtenerSedesCurso( decimal curso)
        {

            List<LISTACONCEPTOS> listaSedes = new List<LISTACONCEPTOS>();
            IEnumerable<OFERTASCURSOS> ofertaCursos = unidad.RepositorioOfertaCurso.GetAll().Where(u => u.CURSO_ID == curso).Distinct() ;

            foreach (var sede in ofertaCursos.Select(u => u.SEDES).Distinct())
            {
                LISTACONCEPTOS obj = new LISTACONCEPTOS();
                obj.id = sede.SEDE_ID.ToString();
                obj.nombre = sede.NOMBRE.ToString();
                listaSedes.Add(obj);
            }

            return listaSedes;
        }

        public List<LISTACONCEPTOS> ObtenerTurnosSedesyCurso(decimal curso, decimal sede)
        {

            List<LISTACONCEPTOS> listaTurnos = new List<LISTACONCEPTOS>();
            IEnumerable<OFERTASCURSOS> ofertaCursos = unidad.RepositorioOfertaCurso.GetAll().Where(u => u.CURSO_ID == curso && u.SEDE_ID == sede).Distinct();

            foreach (var turno in ofertaCursos.Select(u => u.TURNOS).Distinct().OrderBy(u => u.NOMBRE))
            {
                LISTACONCEPTOS obj = new LISTACONCEPTOS();
                obj.id = turno.TURNO_ID.ToString();
                obj.nombre = turno.NOMBRE.ToString();
                listaTurnos.Add(obj);
            }

            return listaTurnos;
        }
  
    }
}
