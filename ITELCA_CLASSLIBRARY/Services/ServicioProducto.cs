﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using System.Data.Entity;
using ITELCA_CLASSLIBRARY.Models;
using ITELCA_CLASSLIBRARY.Repositories;
using ITELCA_CLASSLIBRARY.CustomClasses;
using System.Collections;
using System.Web;
using UmbrellaClassLibrary.CustomClasses;

namespace ITELCA_CLASSLIBRARY.Services
{
     public class ServicioProducto
    {
        private UnitOfWork unidad;
        public ServicioProducto()
        {
            this.unidad = new UnitOfWork();
        }
        public bool Guardar(PRODUCTOS precioProducto)
        {
            try
            {
                unidad.RepositorioProductos.Add(precioProducto);
                unidad.Save();
                return true;
            }
            catch
            {
                return false;
            }
        }
        public bool Modificar(PRODUCTOS precioProducto)
        {
            try
            {
                unidad.RepositorioProductos.Modify(precioProducto);
                unidad.Save();
                return true;
            }
            catch
            {
                return false;
            }
        }


        public List<object> Eliminar(decimal id)
        {
            List<object> lista = new List<object>();
            try
            {
                var aux = unidad.RepositorioProductos.GetById((int)id);
                if (aux != null)
                {
                    lista.Add(new
                    {
                        ok = 1,
                        msg = "Producto eliminado"
                    });
                    unidad.RepositorioProductos.Delete(aux);
                    unidad.Save();
                    return lista;
                }
                else
                {
                    lista.Add(new
                    {
                        ok = 0,
                        msg = "Producto no encontrado"
                    });
                    return lista;
                }
            }
            catch
            {
                lista.Add(new
                {
                    ok = 0,
                    msg = "Error eliminando producto"
                });
                return lista;
            }

        }

        public PRODUCTOS ObtenerPorClave(int id)
        {
            try
            {
                return unidad.RepositorioProductos.GetById(id);
                
            }
            catch 
            {
                return null;
            }
        }

        public PRODUCTOS ObtenerPorClave(string setId, string invItemId)
        {
            try
            {
                return ObtenerTodos().Where(p => p.INV_ITEM_ID == invItemId &&
                                                p.SETID == setId).FirstOrDefault();
            }
            catch
            {
                return null;
            }
        }

       
        public IEnumerable<PRODUCTOS> ObtenerTodos()
        {
            try
            {
                IEnumerable<PRODUCTOS> sedes= unidad.RepositorioProductos.GetAll().OrderBy(p=>p.DESCR);
                if (sedes == null)
                    return Enumerable.Empty<PRODUCTOS>();
                else
                    return sedes;
            }
            catch {
                return Enumerable.Empty<PRODUCTOS>();
            }
        }       
    }
}
