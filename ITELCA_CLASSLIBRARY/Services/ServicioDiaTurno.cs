﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using System.Data.Entity;
using ITELCA_CLASSLIBRARY.Models;
using ITELCA_CLASSLIBRARY.Repositories;
using ITELCA_CLASSLIBRARY.CustomClasses;
using System.Collections;
using System.Web;
using UmbrellaClassLibrary.CustomClasses;


namespace ITELCA_CLASSLIBRARY.Services
{
     public class ServicioDiaTurno
    {
        private UnitOfWork unidad;
        public ServicioDiaTurno()
        {
            this.unidad = new UnitOfWork();
        }
        public bool Guardar(DIAS_TURNOS diaTurno)
        {
            try
            {

                unidad.RepositorioDiaTurno.Add(diaTurno);
                unidad.Save();
                return true;
            }
            catch
            {
                return false;
            }
        }
        public bool Modificar(DIAS_TURNOS entity)
        {
            try
            {

                unidad.RepositorioDiaTurno.Modify(entity);
                unidad.Save();
                return true;
            }
            catch
            {
                return false;
            }
        }

       
      
    }
}
