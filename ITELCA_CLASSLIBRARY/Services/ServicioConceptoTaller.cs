﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using System.Data.Entity;
using ITELCA_CLASSLIBRARY.Models;
using ITELCA_CLASSLIBRARY.Repositories;
using ITELCA_CLASSLIBRARY.CustomClasses;
using System.Collections;
using System.Web;
using UmbrellaClassLibrary.CustomClasses;
using System.Data.Entity.Validation;


namespace ITELCA_CLASSLIBRARY.Services
{
     public class ServicioConceptoTaller
    {
        private UnitOfWork unidad;
        public ServicioConceptoTaller()
        {
            this.unidad = new UnitOfWork();
        }
        
        public bool Guardar(CONCEPTOS_TALLERES conceptoTaller)
        {
            try
            {
                string[] arregloTalleres = conceptoTaller.MULTI_TALLER_ID.Split(',');
                
                foreach (var taller in arregloTalleres)
                {
                    CONCEPTOS_TALLERES con_tal = new CONCEPTOS_TALLERES();
                    con_tal.FECHA_EFECTIVA = conceptoTaller.FECHA_EFECTIVA;
                    con_tal.ESTADO = 1;
                    con_tal.CONCEPTO_ID = conceptoTaller.CONCEPTO_ID;
                    con_tal.MONTO = conceptoTaller.MONTO;
                    con_tal.TALLER_ID = decimal.Parse(taller);
                    con_tal.USUARIO_ID = conceptoTaller.USUARIO_ID;
                    con_tal.CONDICION = "C";

                    CONCEPTOS_TALLERES aux = ObtenerTodos().Where(c => c.TALLER_ID == con_tal.TALLER_ID &&
                                                                        c.CONCEPTO_ID == con_tal.CONCEPTO_ID &&
                                                                        c.FECHA_EFECTIVA == con_tal.FECHA_EFECTIVA).FirstOrDefault();
                    if (aux == null)
                        unidad.RepositorioConceptoTaller.Add(con_tal);
                }

                unidad.Save();
                return true;
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                return false;
            }
        }
        public bool Modificar(CONCEPTOS_TALLERES conceptoTaller)
        {
            try
            {
                conceptoTaller.CONDICION = "A";
                unidad.RepositorioConceptoTaller.Modify(conceptoTaller);
                unidad.Save();
                return true;
            }
            catch
            {
                return false;
            }
        }


        public List<object> Eliminar(decimal id)
        {
            List<object> lista = new List<object>();
            try
            {
                var aux = unidad.RepositorioConceptoTaller.GetById((int)id);
                if (aux != null)
                {
                    lista.Add(new
                    {
                        ok = 1,
                        msg = "Concepto Taller Eliminado"
                    });
                    aux.ESTADO = 0;
                    aux.CONDICION = "E";
                    unidad.RepositorioConceptoTaller.Modify(aux);
                    //unidad.RepositorioConceptoTaller.Delete(aux);
                    unidad.Save();
                    return lista;
                }
                else
                {
                    lista.Add(new
                    {
                        ok = 0,
                        msg = "Concepto Taller No encontrado"
                    });
                    return lista;
                }
            }
            catch
            {
                lista.Clear();
                lista.Add(new
                {
                    ok = 0,
                    msg = "Error Eliminando Concepto Taller"
                });
                return lista;
            }

        }

        public object ObtenerDataGrid( string sidx, string sord, int page, int rows, string filters)
        {
            int totalPages = 0;
            int totalRecords = 0;
            IEnumerable<CONCEPTOS_TALLERES> conceptosTalleres;
            conceptosTalleres = ObtenerTodos().Where(ct => ct.ESTADO == 1 || (ct.ESTADO == 0 && ct.CONDICION != "E"));
            conceptosTalleres = JqGrid<CONCEPTOS_TALLERES>.GetFilteredContent(sidx, sord, page, rows, filters, conceptosTalleres.AsQueryable(), ref totalPages, ref totalRecords);
            var rowsModel = (
                from conceptoTaller in conceptosTalleres.ToList()
                select new
                {
                    i = conceptoTaller.CONCEPTO_ID,
                    cell = new string[] {
                           conceptoTaller.CONCEPTO_TALLER_ID.ToString(),
                           conceptoTaller.TALLERES.NOMBRE,
                           conceptoTaller.CONCEPTOS.NOMBRE,
                           conceptoTaller.FECHA_EFECTIVA.ToString("dd/MM/yyyy"),
                           String.Format("{0:N2}", conceptoTaller.MONTO),
                           (conceptoTaller.ESTADO == 1) ? "Activo" : "Inactivo",
                            "<a title=\"Editar\" href=\"/ConceptoTaller/Editar/"+ conceptoTaller.CONCEPTO_TALLER_ID+"\"><span id=\""+conceptoTaller.CONCEPTO_TALLER_ID+"\" class=\"ui-icon ui-icon-pencil\"></a>",                          
                            "<span id=\""+conceptoTaller.CONCEPTO_TALLER_ID+"\"  class=\"ui-icon ui-icon-close\" ></span>" }
                }).ToArray();
            return JqGrid<CONCEPTOS_TALLERES>.SetJsonData(totalPages, totalRecords, page, rowsModel);
        }

        public CONCEPTOS_TALLERES ObtenerPorClave(int id)
        {
            try
            {
                return unidad.RepositorioConceptoTaller.GetById(id);
                
            }
            catch 
            {
                return null;
            }
        }

        public CONCEPTOS_TALLERES ObtenerConceptoTallerPorId(int id)
        {
            try
            {
                return unidad.RepositorioConceptoTaller.GetById(id);

            }
            catch
            {
                return null;
            }
        }

        public IEnumerable< CONCEPTOS_TALLERES> ObtenerConceptoTallerPorIdDeTaller(decimal tallerId)
        {
            try
            {
                return unidad.RepositorioConceptoTaller.GetAll().Where(u=>u.TALLER_ID==tallerId);

            }
            catch
            {
                return null;
            }
        }
        public IEnumerable<CONCEPTOS_TALLERES> ObtenerTodos()
        {
            try
            {
                IEnumerable<CONCEPTOS_TALLERES> CONCEPTOS_TALLERES = unidad.RepositorioConceptoTaller.GetAll().Where(u=>u.ESTADO==1);
                if (CONCEPTOS_TALLERES == null)
                    return Enumerable.Empty<CONCEPTOS_TALLERES>();
                else
                    return CONCEPTOS_TALLERES;
            }
            catch  {
                return Enumerable.Empty<CONCEPTOS_TALLERES>();
            }
        }

        public bool Duplicado(CONCEPTOS_TALLERES CONCEPTOS_TALLERES)
        {
            try
            {
                if (unidad.RepositorioConceptoTaller.GetAll().Where(u => u.CONCEPTO_ID == CONCEPTOS_TALLERES.CONCEPTO_ID &&
                                                                    u.TALLER_ID == CONCEPTOS_TALLERES.TALLER_ID &&
                                                                 u.CONCEPTO_TALLER_ID != CONCEPTOS_TALLERES.CONCEPTO_TALLER_ID && u.ESTADO==1).Count() > 0)
                    return true;
                else
                    return false;

            }
            catch
            {
                return false;
            }
        }

        public List<LISTACONCEPTOS> ListaConceptoTalleres(string TALLER_ID)
        {

                List<LISTACONCEPTOS> listaConceptoCursos = new List<LISTACONCEPTOS>();
                Decimal TALLER = decimal.Parse(TALLER_ID);
                IEnumerable<CONCEPTOS_TALLERES> conceptoTallers = unidad.RepositorioConceptoTaller.GetAll().Where(u => u.TALLER_ID == TALLER && u.ESTADO==1);

                foreach (var ope in conceptoTallers)
                {
                    LISTACONCEPTOS obj = new LISTACONCEPTOS();
                    obj.id = ope.CONCEPTO_TALLER_ID.ToString();
                    obj.nombre = ope.CONCEPTOS.NOMBRE.ToString();
                    listaConceptoCursos.Add(obj);
                }

                return listaConceptoCursos;
              

                    }

        public string ObtenerMontoLineaDetale(decimal id_conceptoTaller, string cantidad)
        {
            CONCEPTOS_TALLERES conceptoTaller = unidad.RepositorioConceptoTaller.GetAll().Where(u => u.CONCEPTO_TALLER_ID == id_conceptoTaller && u.ESTADO==1).FirstOrDefault();
            return (conceptoTaller.MONTO * Int32.Parse(cantidad)).ToString();

        }
       
    }
}
