﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using System.Data.Entity;
using ITELCA_CLASSLIBRARY.Models;
using ITELCA_CLASSLIBRARY.Repositories;
using ITELCA_CLASSLIBRARY.CustomClasses;
using System.Collections;
using System.Web;
using UmbrellaClassLibrary.CustomClasses;


namespace ITELCA_CLASSLIBRARY.Services
{
     public class ServicioGenero
    {
        private UnitOfWork _unidad;
        public ServicioGenero()
        {
            this._unidad = new UnitOfWork();
        }
        public bool Guardar(GENEROS gen)
        {
            try
            {
                _unidad.RepositorioGeneros.Add(gen);
                _unidad.Save();
                return true;
            }
            catch
            {
                return false;
            }
        }
        public bool Modificar(GENEROS entity)
        {
            try
            {

                _unidad.RepositorioGeneros.Modify(entity);
                _unidad.Save();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public IEnumerable<GENEROS> ObtenerLista() {
            IEnumerable<GENEROS> lista = _unidad.RepositorioGeneros.GetAll();

            if (lista != null)
                return lista;
            else
                return Enumerable.Empty<GENEROS>();
            
        }
       
      
       
    }
}
