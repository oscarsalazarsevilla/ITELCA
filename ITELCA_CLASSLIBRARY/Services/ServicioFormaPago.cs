﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using System.Data.Entity;
using ITELCA_CLASSLIBRARY.Models;
using ITELCA_CLASSLIBRARY.Repositories;
using ITELCA_CLASSLIBRARY.CustomClasses;
using System.Collections;
using System.Web;
using UmbrellaClassLibrary.CustomClasses;

namespace ITELCA_CLASSLIBRARY.Services
{
     public class ServicioFormaPago
    {
        private UnitOfWork unidad;
        public ServicioFormaPago()
        {
            this.unidad = new UnitOfWork();
        }
        public bool Guardar(FORMASPAGOS formaPago)
        {
            try
            {
                formaPago.CONDICION = "C";
                unidad.RepositorioFormaPago.Add(formaPago);
                unidad.Save();
                return true;
            }
            catch
            {
                return false;
            }
        }
        public bool Modificar(FORMASPAGOS entity)
        {
            try
            {
                entity.CONDICION = "A";
                unidad.RepositorioFormaPago.Modify(entity);
                unidad.Save();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public List<object> Eliminar(decimal dia)
        {
            List<object> lista = new List<object>();
            try
            {
                var aux = unidad.RepositorioDia.GetById((int)dia);
                if (aux != null)
                {
                    if (aux.DIAS_TURNOS.Count() > 0)
                    {
                        lista.Add(new
                        {
                            ok = 1,
                            msg = "Dia Tiene,Turno Asignado"
                        });
                    }
                    else {
                        lista.Add(new
                        {
                            ok = 1,
                            msg = "Dia Eliminado"
                        });
                        aux.CONDICION="E";
                        aux.ESTADO=0;
                        unidad.RepositorioDia.Modify(aux);
                    }
                    //unidad.RepositorioDia.Delete(aux);
                    unidad.Save();
                    return lista;
                }
                else
                {
                    lista.Add(new
                    {
                        ok = 0,
                        msg = "Dia No encontrado"
                    });
                    return lista;
                }
            }
            catch
            {
                lista.Clear();
                lista.Add(new
                {
                    ok = 0,
                    msg = "Error Eliminando Dia"
                });
                return lista;
            }

        }

        public object ObtenerDataGrid( string sidx, string sord, int page, int rows, string filters)
        {
            int totalPages = 0;
            int totalRecords = 0;
            IEnumerable<DIAS> dias;
            dias = unidad.RepositorioDia.GetAll().Where(u => u.ESTADO == 1 || (u.ESTADO == 0 && u.CONDICION != "E"));
            dias = JqGrid<DIAS>.GetFilteredContent(sidx, sord, page, rows, filters, dias.AsQueryable(), ref totalPages, ref totalRecords);
           
            var rowsModel = (
                from dia in dias.ToList()
                select new
                {
                    i = dia.DIA_ID,
                    cell = new string[] {
                           dia.DIA_ID.ToString(),
                           dia.NOMBRE.ToString(),
                           dia.NOMBRECORTO.ToString(),
                           (dia.ESTADO == 1) ? "Activo" : "Inactivo",
                            "<a title=\"Editar\" href=\"/Dias/Editar/"+  dia.DIA_ID+"\"><span id=\""+  dia.DIA_ID+"\" class=\"ui-icon ui-icon-pencil\"></a>",                          
                            "<span id=\""+  dia.DIA_ID+"\"  class=\"ui-icon ui-icon-close\" ></span>" }
                }).ToArray();
            return JqGrid<DIAS>.SetJsonData(totalPages, totalRecords, page, rowsModel);
        }

        public DIAS ObtenerPorClave(int id)
        {
            try
            {
                return unidad.RepositorioDia.GetById(id);
                
            }
            catch
            {
                return null;
            }
        }
        public Dictionary<string,string> ObtenerLista()
        {
            Dictionary<string, string> lista = new Dictionary<string, string>();
            try
            {
                IEnumerable<FORMASPAGOS> formasdePago= unidad.RepositorioFormaPago.GetAll();
                if (formasdePago != null)
                    foreach(var formaP in formasdePago)
                        lista.Add(formaP.FORMAPAGO_ID.ToString(),formaP.NOMBRE);
                return lista;

            }
            catch
            {
                return lista;
            }
        }
      
        public IEnumerable<DIAS> ObtenerTodos()
        {
            try
            {
                IEnumerable<DIAS> dias= unidad.RepositorioDia.GetAll().Where(u=>u.ESTADO== 1);
                if (dias == null)
                    return Enumerable.Empty<DIAS>();
                else
                    return dias;
            }
            catch {
                return Enumerable.Empty<DIAS>();
            }
        }

        public bool Duplicado(DIAS dias)
        {
            try
            {
                if (unidad.RepositorioDia.GetAll().Where(u => u.NOMBRE.ToLower() == dias.NOMBRE.ToLower() &&
                                                                 u.DIA_ID != dias.DIA_ID && u.ESTADO==1).Count() > 0)
                    return true;
                else
                    return false;

            }
            catch 
            {
                return false;
            }
        }
       
      
    }
}
