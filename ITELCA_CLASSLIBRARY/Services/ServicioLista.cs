﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ITELCA_CLASSLIBRARY.Services
{
    public class ServicioLista
    {
        public ServicioLista() { 
        
        }
        public Dictionary<string, string> ObtenerDiccionarioHoras()
        {
            Dictionary<string, string> dictime = new Dictionary<string, string>();

            dictime.Add("07:00", "07:00 a.m.");
            dictime.Add("07:30", "07:30 a.m.");
            dictime.Add("08:00", "08:00 a.m.");
            dictime.Add("08:30", "08:30 a.m.");
            dictime.Add("09:00", "09:00 a.m.");
            dictime.Add("09:30", "09:30 a.m.");
            dictime.Add("10:00", "10:00 a.m.");
            dictime.Add("10:30", "10:30 a.m.");
            dictime.Add("11:00", "11:00 a.m.");
            dictime.Add("11:30", "11:30 a.m.");
            dictime.Add("12:00", "12:00 p.m.");
            dictime.Add("13:00", "01:00 p.m.");
            dictime.Add("13:30", "01:30 p.m.");
            dictime.Add("14:00", "02:00 p.m.");
            dictime.Add("14:30", "02:30 p.m.");
            dictime.Add("15:00", "03:00 p.m.");
            dictime.Add("15:30", "03:30 p.m.");
            dictime.Add("16:00", "04:00 p.m.");
            dictime.Add("16:30", "04:30 p.m.");
            dictime.Add("17:00", "05:00 p.m.");
            dictime.Add("17:30", "05:30 p.m.");
            dictime.Add("18:00", "06:00 p.m.");
            dictime.Add("18:30", "06:30 p.m.");
            dictime.Add("19:00", "07:00 p.m.");
            dictime.Add("19:30", "07:30 p.m.");
            dictime.Add("20:00", "08:00 p.m.");
            return dictime;
        }
    }
}
