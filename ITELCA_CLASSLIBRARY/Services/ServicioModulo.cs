﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using System.Data.Entity;
using ITELCA_CLASSLIBRARY.Models;
using ITELCA_CLASSLIBRARY.Repositories;
using ITELCA_CLASSLIBRARY.CustomClasses;
using System.Collections;
using System.Web;
using UmbrellaClassLibrary.CustomClasses;


namespace ITELCA_CLASSLIBRARY.Services
{
     public class ServicioModulo
    {
        private UnitOfWork unidad;
        public ServicioModulo()
        {
            this.unidad = new UnitOfWork();
        }
        public bool Guardar(MODULOS modulo)
        {
            try
            {
                modulo.CONDICION = "C";
                unidad.RepositorioModulo.Add(modulo);
                
                unidad.Save();
                return true;
            }
            catch
            {
                return false;
            }
        }
        public bool Guardar(MODULOS modulo,string curso)
        {
            modulo.CONDICION = "C";
            if (modulo.ORDEN.ToString() =="") {
                modulo.ORDEN = 0;
            }

            try
            {
                /*unidad.RepositorioModulo.Add(modulo);
                unidad.Save();

                MODULOS mod = unidad.RepositorioModulo.GetAll().Where(m => m.NOMBRE == modulo.NOMBRE).FirstOrDefault();

                CURSOS_MODULOS cm = new CURSOS_MODULOS();
                cm.CURSO_ID = decimal.Parse(curso);
                cm.MODULO_ID = mod.MODULO_ID;
                
                unidad.RepositorioCursoModulo.Add(cm);
                unidad.Save();*/

                CURSOS_MODULOS cm = new CURSOS_MODULOS();
                cm.CURSO_ID = decimal.Parse(curso);
                modulo.CURSOS_MODULOS.Add(cm);

                unidad.RepositorioModulo.Add(modulo);
                unidad.Save();

                return true;
            }
            catch
            {
                return false;
            }
        }
        public bool Modificar(MODULOS modulo,string curso)
        {
            try
            {
                modulo.CONDICION = "A";
                if (modulo.ORDEN.ToString() == "")
                {
                    modulo.ORDEN = 0;
                }
                modulo.CURSOS_MODULOS.Clear();
                unidad.Save();

                string[] curArreglo = curso.Split(',');

                foreach (var curso_id in curArreglo)
                {
                    CURSOS_MODULOS cm = new CURSOS_MODULOS();
                    cm.CURSO_ID = decimal.Parse(curso_id);
                    cm.MODULO_ID = modulo.MODULO_ID;
                    modulo.CURSOS_MODULOS.Add(cm);
                }
                unidad.RepositorioModulo.Modify(modulo);
                unidad.Save();
                return true;
            }
            catch
            {
                return false;
            }
        }
        public bool Modificar(MODULOS entity)
        {
            try
            {
                entity.CONDICION = "A";
                unidad.RepositorioModulo.Modify(entity);
                unidad.Save();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public List<object> Eliminar(decimal id)
        {
            List<object> lista = new List<object>();
            try
            {
                var aux = unidad.RepositorioModulo.GetById((int)id);
                if (aux != null)
                {
                    if (aux.MATRICULASMODULOS != null && aux.MATRICULASMODULOS.Count() > 0)
                    {
                        lista.Add(new
                        {
                            ok = 0,
                            msg = "Módulo Tiene Alumnos Inscritos"
                        });
                    }
                    else {
                        lista.Add(new
                        {
                            ok = 0,
                            msg = "Módulo Eliminado"
                        });
                        aux.ESTADO = 0;
                        aux.CONDICION = "E";
                        unidad.RepositorioModulo.Modify(aux);
                    }
                   
                    unidad.Save();
                    return lista;
                }
                else
                {
                    lista.Add(new
                    {
                        ok = 0,
                        msg = "Módulo No encontrado"
                    });
                    return lista;
                }
            }
            catch
            {
                lista.Clear();
                lista.Add(new
                {
                    ok = 0,
                    msg = "Error Eliminando Módulo"
                });
                return lista;
            }

        }

        public object ObtenerDataGrid(string CursoId, string sidx, string sord, int page, int rows, string filters)
        {
            int totalPages = 0;
            int totalRecords = 0;
            IEnumerable<MODULOS> modulos;
            if (CursoId != "")
            {
                decimal curso = decimal.Parse(CursoId);
               // modulos = unidad.RepositorioModulo.GetAll().Where(u => u.ESTADO == 1 || (u.ESTADO == 0 && u.CONDICION != "E") && u.CURSOS_MODULOS.Any(a => a.CURSO_ID == curso)).OrderBy(u => u.NOMBRE);
                CURSOS cur = unidad.RepositorioCurso.GetAll().Where(u => u.CURSO_ID == curso).FirstOrDefault();
                modulos = cur.CURSOS_MODULOS.Where(u=>u.MODULOS.ESTADO==1).Select(a => a.MODULOS);

                modulos = JqGrid<MODULOS>.GetFilteredContent(sidx, sord, page, rows, filters, modulos.AsQueryable(), ref totalPages, ref totalRecords);
                var rowsModel = (
                    from modulo in modulos.ToList()
                    select new
                    {
                        i = modulo.MODULO_ID,
                        cell = new string[] {
                            modulo.MODULO_ID.ToString(),
                            modulo.NOMBRE,
                            modulo.DESCRIPCION,
                            modulo.CURSOS_MODULOS.Count()>0?modulo.CURSOS_MODULOS.FirstOrDefault().CURSOS.NOMBRE:"--Sin Curso--", 
                            modulo.ORDEN.ToString(),
                            (modulo.ESTADO == 1) ? "Activo" : "Inactivo",
                            "<a title=\"Editar\" href=\"/Modulo/Editar/"+ modulo.MODULO_ID+"\"><span id=\""+modulo.MODULO_ID+"\" class=\"ui-icon ui-icon-pencil\"></a>",                          
                            "<span id=\""+modulo.MODULO_ID+"\"  class=\"ui-icon ui-icon-close\" ></span>" }
                    }).ToArray();
                return JqGrid<MODULOS>.SetJsonData(totalPages, totalRecords, page, rowsModel);
            }
            return null;
        }

        public MODULOS ObtenerPorClave(long id)
        {
            try
            {
                return unidad.RepositorioModulo.GetById(id);
                
            }
            catch 
            {
                return null;
            }
        }

       
        public IEnumerable<MODULOS> ObtenerTodos()
        {
            try
            {
                IEnumerable<MODULOS> modulos = unidad.RepositorioModulo.GetAll().Where(u=>u.ESTADO==1).OrderBy(m=>m.NOMBRE);
                if (modulos == null)
                    return Enumerable.Empty<MODULOS>();
                else
                    return modulos;
            }
            catch  {
                return Enumerable.Empty<MODULOS>();
            }
        }

        public bool Duplicado(MODULOS modulo, string opcion)
        {
            try
            {
                if (opcion == "C")
                {
                    if (unidad.RepositorioModulo.GetAll().Where(u => u.NOMBRE.ToLower() == modulo.NOMBRE.ToLower() &&
                                                                     u.ESTADO == 1).Count() > 0)
                        return true;
                    else
                        return false;
                }
                else
                {
                    if (unidad.RepositorioModulo.GetAll().Where(u => u.NOMBRE.ToLower() == modulo.NOMBRE.ToLower() &&
                                                                     u.MODULO_ID != modulo.MODULO_ID && u.ESTADO == 1).Count() > 0)
                        return true;
                    else
                        return false;
                }

            }
            catch 
            {
                return false;
            }
        }

        public Dictionary<decimal, string> ObtenerDiccionarioModulo()
        {
            Dictionary<decimal, string> lista = new Dictionary<decimal, string>();
            foreach (var entity in ObtenerTodos().ToList())
            {
                lista.Add(entity.MODULO_ID, entity.NOMBRE);
            }
            return lista;
        }

        public Dictionary<decimal, string> ObtenerDiccionarioModuloPorCurso(decimal curso)
        {
            Dictionary<decimal, string> lista = new Dictionary<decimal, string>();
            foreach (var entity in unidad.RepositorioCursoModulo.GetAll().Where(u => u.CURSO_ID == curso && u.MODULOS.ESTADO==1).OrderBy(u=> u.MODULOS.NOMBRE).ToList())
            {
                lista.Add(entity.MODULO_ID, entity.MODULOS.NOMBRE);
            }
            return lista;
        }

        public string ObtenerSelectPorCurso(decimal curso)
        {
            Dictionary<decimal, string> lista = new Dictionary<decimal, string>();
            string select="";
            
            foreach (var entity in unidad.RepositorioCursoModulo.GetAll().Where(u=>u.CURSO_ID==curso))
            {
                select+="<option value="+entity.MODULO_ID+">"+entity.MODULOS.NOMBRE+"</option>";
                
            }
            return select;
        }

        public List<object> ObtenerObjetoModuloPorCurso(decimal cursoId)
        {
            List<object> lista = new List<object>();
            try
            {

                foreach (var ope in unidad.RepositorioCursoModulo.GetAll().Where(u => u.CURSO_ID == cursoId))
                    lista.Add(new
                    {
                        key = ope.MODULO_ID,
                        value = ope.MODULOS.NOMBRE
                    });




                return lista;
            }
            catch
            {
                return null;
            }
        }
    }
}
