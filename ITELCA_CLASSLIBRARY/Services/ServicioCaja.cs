﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using System.Data.Entity;
using ITELCA_CLASSLIBRARY.Models;
using ITELCA_CLASSLIBRARY.Repositories;
using ITELCA_CLASSLIBRARY.CustomClasses;
using System.Collections;
using System.Web;
using UmbrellaClassLibrary.CustomClasses;

namespace ITELCA_CLASSLIBRARY.Services
{
     public class ServicioCaja
    {
        private UnitOfWork unidad;
        public ServicioCaja()
        {
            this.unidad = new UnitOfWork();
        }
        public bool Guardar(CAJAS caja)
        {
            try
            {
                caja.CONDICION = "C";
                unidad.RepositorioCaja.Add(caja);
                unidad.Save();
                return true;
            }
            catch
            {
                return false;
            }
        }
        public bool Modificar(CAJAS caja)
        {
            try
            {
                caja.CONDICION = "A";
                unidad.RepositorioCaja.Modify(caja);
                unidad.Save();
                return true;
            }
            catch
            {
                return false;
            }
        }


        public List<object> Eliminar(decimal id)
        {
            List<object> lista = new List<object>();
            try
            {
                var aux = unidad.RepositorioCaja.GetById((int)id);
                if (aux != null)
                {
                    lista.Add(new
                    {
                        ok = 1,
                        msg = "Caja Eliminada"
                    });
                    aux.CONDICION = "E";
                    aux.ESTADO = 0;
                    unidad.RepositorioCaja.Modify(aux);
                    //unidad.RepositorioCaja.Delete(aux);
                    unidad.Save();
                    return lista;
                }
                else
                {
                    lista.Add(new
                    {
                        ok = 0,
                        msg = "Caja No encontrada"
                    });
                    return lista;
                }
            }
            catch
            {
                lista.Add(new
                {
                    ok = 0,
                    msg = "Error Eliminando Caja"
                });
                return lista;
            }

        }

        public Dictionary<decimal, string> ObtenerDiccionarioCajas()
        {
            Dictionary<decimal, string> lista = new Dictionary<decimal, string>();
            foreach (var entity in ObtenerTodos().ToList())
            {
                lista.Add(entity.CAJA_ID, entity.NOMBRE);
            }
            return lista;
        }

        public object ObtenerDataGrid( string sidx, string sord, int page, int rows, string filters)
        {
            int totalPages = 0;
            int totalRecords = 0;
            IEnumerable<CAJAS> cajas;
            cajas = unidad.RepositorioCaja.GetAll().Where(u => u.ESTADO == 1 || (u.ESTADO == 0 && u.CONDICION != "E"));
            cajas = JqGrid<CAJAS>.GetFilteredContent(sidx, sord, page, rows, filters, cajas.AsQueryable(), ref totalPages, ref totalRecords);
           
            var rowsModel = (
                from caja in cajas.ToList()
                select new
                {
                    i = caja.CAJA_ID,
                    cell = new string[] {
                           caja.CAJA_ID.ToString(),
                           caja.NOMBRE.ToString(),
                           caja.CODIGO.ToString(),
                           caja.SEDES.NOMBRE.ToString(),
                            (caja.ESTADO == 1) ? "Activo" : "Inactivo",
                            "<a title=\"Editar\" href=\"/Caja/Editar/"+ caja.CAJA_ID+"\"><span id=\""+caja.CAJA_ID+"\" class=\"ui-icon ui-icon-pencil\"></a>",                          
                            "<span id=\""+caja.CAJA_ID+"\"  class=\"ui-icon ui-icon-close\" ></span>" }
                }).ToArray();
            return JqGrid<CAJAS>.SetJsonData(totalPages, totalRecords, page, rowsModel);
        }

        public CAJAS ObtenerPorClave(int id)
        {
            try
            {
                return unidad.RepositorioCaja.GetById(id);
                
            }
            catch 
            {
                return null;
            }
        }

       
        public IEnumerable<CAJAS> ObtenerTodos()
        {
            try
            {
                IEnumerable<CAJAS> sedes= unidad.RepositorioCaja.GetAll().Where(u=>u.ESTADO==1);
                if (sedes == null)
                    return Enumerable.Empty<CAJAS>();
                else
                    return sedes;
            }
            catch {
                return Enumerable.Empty<CAJAS>();
            }
        }

        public IEnumerable<CAJAS> ObtenerCajasPorSede(decimal sede_id)
        {
            try
            {
                IEnumerable<CAJAS> cajas = unidad.RepositorioCaja.GetAll().Where(c =>   c.ESTADO == 1 && 
                                                                                        c.SEDE_ID == sede_id).OrderBy(c=>c.NOMBRE);
                if (cajas == null)
                    return Enumerable.Empty<CAJAS>();
                else
                    return cajas;
            }
            catch
            {
                return Enumerable.Empty<CAJAS>();
            }
        }

        public bool Duplicado(CAJAS caja, string accion)
        {
            try
            {
                if (accion == "C")
                {
                    if (ObtenerTodos().Where(c =>   c.NOMBRE.ToLower() == caja.NOMBRE.ToLower() &&
                                                    c.SEDE_ID == caja.SEDE_ID).Count() > 0)
                        return true;
                    else
                        return false;
                }
                else
                {
                    if (ObtenerTodos().Where(c =>   c.NOMBRE.ToLower() == caja.NOMBRE.ToLower() &&
                                                    c.SEDE_ID == caja.SEDE_ID &&
                                                    c.CAJA_ID != caja.CAJA_ID).Count() > 0)
                        return true;
                    else
                        return false;
                }

            }
            catch 
            {
                return false;
            }
        }

        public string ObtenerCajaPorSede(decimal idSede)
        {
            try
            {
                 IEnumerable<CAJAS> caja=unidad.RepositorioCaja.GetAll().Where(u => u.SEDE_ID==idSede);
                string resp="<select>"; 
                if (caja != null)
                 {
                     foreach (var ope in caja)
                         resp += "<option value=\"" + ope.CAJA_ID + "\">" + ope.NOMBRE + "</option>";

                     resp += "</select>";

                     return resp;
                 }
                 else
                     return "";
            }
            catch
            {
                return "";
            }
        }

        public List<CAJAS> ObtenerParaListaPorSede(int sede_id)
        {
            return ObtenerTodos().Where(c=>c.SEDE_ID == sede_id && c.ESTADO == 1).OrderBy(c=>c.NOMBRE).ToList();
        }

        public List<object> ObtenerObjeto()
        {
            List<object> lista = new List<object>();
            try
            {

                foreach (var ope in ObtenerTodos())
                    lista.Add(new
                    {
                        key = ope.CAJA_ID,
                        value = ope.NOMBRE
                    });




                return lista;
            }
            catch
            {
                return null;
            }
        }
       
    }
}
