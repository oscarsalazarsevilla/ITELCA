﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using System.Data.Entity;
using ITELCA_CLASSLIBRARY.Models;
using ITELCA_CLASSLIBRARY.Repositories;
using ITELCA_CLASSLIBRARY.CustomClasses;
using System.Collections;
using System.Web;
using UmbrellaClassLibrary.CustomClasses;

namespace ITELCA_CLASSLIBRARY.Services
{
     public class ServicioPrecioProducto
    {
        private UnitOfWork unidad;
        public ServicioPrecioProducto()
        {
            this.unidad = new UnitOfWork();
        }
        public bool Guardar(PRECIOSPRODUCTOS precioProducto)
        {
            try
            {
                precioProducto.CONDICION = "C";
                unidad.RepositorioPreciosProductos.Add(precioProducto);
                unidad.Save();
                return true;
            }
            catch
            {
                return false;
            }
        }
        public bool Modificar(PRECIOSPRODUCTOS precioProducto)
        {
            try
            {
                precioProducto.CONDICION = "A";
                unidad.RepositorioPreciosProductos.Modify(precioProducto);
                unidad.Save();
                return true;
            }
            catch
            {
                return false;
            }
        }


        public List<object> Eliminar(decimal id)
        {
            List<object> lista = new List<object>();
            try
            {
                var aux = unidad.RepositorioPreciosProductos.GetById((int)id);
                if (aux != null)
                {
                    lista.Add(new
                    {
                        ok = 1,
                        msg = "Precio del producto eliminado"
                    });
                    aux.CONDICION = "E";
                    aux.ESTADO = 0;
                    unidad.RepositorioPreciosProductos.Modify(aux);
                    //unidad.RepositorioPreciosProductos.Delete(aux);
                    unidad.Save();
                    return lista;
                }
                else
                {
                    lista.Add(new
                    {
                        ok = 0,
                        msg = "Precio del producto no encontrado"
                    });
                    return lista;
                }
            }
            catch
            {
                lista.Add(new
                {
                    ok = 0,
                    msg = "Error eliminando precio del producto"
                });
                return lista;
            }

        }

        public List<object> Eliminar(string setid, string inv_item_id, DateTime fecha_efectiva)
        {
            List<object> lista = new List<object>();
            try
            {
                var aux = ObtenerPorClave(setid, inv_item_id, fecha_efectiva);
                if (aux != null)
                {
                    lista.Add(new
                    {
                        ok = 1,
                        msg = "Precio del producto eliminado"
                    });
                    aux.CONDICION = "E";
                    aux.ESTADO = 0;
                    unidad.RepositorioPreciosProductos.Modify(aux);
                    //unidad.RepositorioPreciosProductos.Delete(aux);
                    unidad.Save();
                    return lista;
                }
                else
                {
                    lista.Add(new
                    {
                        ok = 0,
                        msg = "Precio del producto no encontrado"
                    });
                    return lista;
                }
            }
            catch
            {
                lista.Add(new
                {
                    ok = 0,
                    msg = "Error eliminando precio del producto"
                });
                return lista;
            }

        }

        public object ObtenerDataGrid(string sidx, string sord, int page, int rows, string filters)
        {
            int totalPages = 0;
            int totalRecords = 0;
            IEnumerable<PRECIOSPRODUCTOS> preciosProductos;
            preciosProductos = unidad.RepositorioPreciosProductos.GetAll().Where(u => u.ESTADO == 1 || (u.ESTADO == 0 && u.CONDICION != "E"));
            preciosProductos = JqGrid<PRECIOSPRODUCTOS>.GetFilteredContent(sidx, sord, page, rows, filters, preciosProductos.AsQueryable(), ref totalPages, ref totalRecords);
            var rowsModel = (
                from precioProducto in preciosProductos.ToList()
                select new
                {
                    i = precioProducto.INV_ITEM_ID,
                    cell = new string[] {
                           precioProducto.INV_ITEM_ID,
                           precioProducto.PRODUCTOS.DESCR,
                           String.Format("{0:N2}", precioProducto.MONTO),
                           precioProducto.FECHA_EFECTIVA.ToString("dd/MM/yyyy"),
                           (precioProducto.ESTADO == 1) ? "Activo" : "Inactivo",
                            "<a title=\"Editar\" href=\"/PreciosProductos/Editar?setid="+ precioProducto.SETID +"&inv_item_id="+precioProducto.INV_ITEM_ID+"&fecha_efectiva="+precioProducto.FECHA_EFECTIVA.ToString("dd/MM/yyyy")+"\"><span id=\""+precioProducto.INV_ITEM_ID+"\" class=\"ui-icon ui-icon-pencil\"></a>",                          
                            "<span id=\""+precioProducto.SETID +"|"+precioProducto.INV_ITEM_ID+"|"+precioProducto.FECHA_EFECTIVA.ToString("dd/MM/yyyy")+"\"  class=\"ui-icon ui-icon-close\" ></span>" }
                }).ToArray();
            return JqGrid<PRECIOSPRODUCTOS>.SetJsonData(totalPages, totalRecords, page, rowsModel);
        }

        public PRECIOSPRODUCTOS ObtenerPorClave(int id)
        {
            try
            {
                return unidad.RepositorioPreciosProductos.GetById(id);
                
            }
            catch 
            {
                return null;
            }
        }

        public PRECIOSPRODUCTOS ObtenerPorClave(string setid, string inv_item_id, DateTime fecha_efectiva)
        {
            try
            {
                return ObtenerTodos().Where(p=>p.SETID == setid &&
                                               p.INV_ITEM_ID == inv_item_id &&
                                               p.FECHA_EFECTIVA == fecha_efectiva).FirstOrDefault();

            }
            catch
            {
                return null;
            }
        }

       
        public IEnumerable<PRECIOSPRODUCTOS> ObtenerTodos()
        {
            try
            {
                IEnumerable<PRECIOSPRODUCTOS> sedes= unidad.RepositorioPreciosProductos.GetAll().Where(u=>u.ESTADO==1);
                if (sedes == null)
                    return Enumerable.Empty<PRECIOSPRODUCTOS>();
                else
                    return sedes;
            }
            catch {
                return Enumerable.Empty<PRECIOSPRODUCTOS>();
            }
        }

        public decimal ObtenerPrecioProducto(string inv_item_id)
        {
            PRECIOSPRODUCTOS p = ObtenerTodos().Where(pp => pp.INV_ITEM_ID == inv_item_id &&
                                                          pp.FECHA_EFECTIVA <= DateTime.Now).OrderByDescending(pp => pp.FECHA_EFECTIVA).FirstOrDefault();

            if (p == null)
                return 0;
            else
                return p.MONTO;
        }
    }
}
