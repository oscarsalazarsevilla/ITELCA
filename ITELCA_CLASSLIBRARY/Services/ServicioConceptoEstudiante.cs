﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using System.Data.Entity;
using ITELCA_CLASSLIBRARY.Models;
using ITELCA_CLASSLIBRARY.Repositories;
using ITELCA_CLASSLIBRARY.CustomClasses;
using System.Collections;
using System.Web;
using UmbrellaClassLibrary.CustomClasses;
using System.Data.Entity.Validation;

namespace ITELCA_CLASSLIBRARY.Services
{
     public class ServicioConceptoEstudiante
    {
        private UnitOfWork unidad;
        public ServicioConceptoEstudiante()
        {
            this.unidad = new UnitOfWork();
        }
        public bool Guardar(CONCEPTOS_ESTUDIANTES entidad)
        {
            try
            {
                unidad.RepositorioConceptosEstudiantes.Add(entidad);
                unidad.Save();
                return true;
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                return false;
            }
        }
        public bool Modificar(CONCEPTOS_ESTUDIANTES entidad)
        {
            try
            {

                unidad.RepositorioConceptosEstudiantes.Modify(entidad);
                unidad.Save();
                return true;
            }
            catch
            {
                return false;
            }
        }


        public List<object> Eliminar(decimal id)
        {
            List<object> lista = new List<object>();
            try
            {
                var aux = unidad.RepositorioConceptosEstudiantes.GetById((int)id);
                if (aux != null)
                {
                    lista.Add(new
                    {
                        ok = 1,
                        msg = "Concepto estudiante eliminado"
                    });
                    unidad.RepositorioConceptosEstudiantes.Delete(aux);
                    unidad.Save();
                    return lista;
                }
                else
                {
                    lista.Add(new
                    {
                        ok = 0,
                        msg = "Concepto estudiante no encontrado"
                    });
                    return lista;
                }
            }
            catch
            {
                lista.Add(new
                {
                    ok = 0,
                    msg = "Error eliminando concepto estudiante"
                });
                return lista;
            }

        }

        /*public object ObtenerDataGrid( string sidx, string sord, int page, int rows, string filters)
        {
            int totalPages = 0;
            int totalRecords = 0;
            IEnumerable<CONCEPTOS_ESTUDIANTES> aulas;
            aulas = ObtenerTodos();
            var rowsModel = (
                from aula in aulas.ToList()
                select new
                {
                    i = aula.MENSUALIDAD_ID,
                    cell = new string[] {
                           aula.MENSUALIDAD_ID.ToString(),
                           aula.MONTO.ToString(),
                            "<a title=\"Editar\" href=\"/Aula/Editar/"+ aula.MENSUALIDAD_ID+"\"><span id=\""+aula.MENSUALIDAD_ID+"\" class=\"ui-icon ui-icon-pencil\"></a>",                          
                            "<span id=\""+aula.MENSUALIDAD_ID+"\"  class=\"ui-icon ui-icon-close\" ></span>" }
                }).ToArray();
            return JqGrid<CONCEPTOS_ESTUDIANTES>.SetJsonData(totalPages, totalRecords, page, rowsModel);
        }*/

        public CONCEPTOS_ESTUDIANTES ObtenerPorClave(int id)
        {
            try
            {
                return unidad.RepositorioConceptosEstudiantes.GetById(id);
                
            }
            catch 
            {
                return null;
            }
        }

       
        public IEnumerable<CONCEPTOS_ESTUDIANTES> ObtenerTodos()
        {
            try
            {
                IEnumerable<CONCEPTOS_ESTUDIANTES> concEst = unidad.RepositorioConceptosEstudiantes.GetAll();
                if (concEst == null)
                    return Enumerable.Empty<CONCEPTOS_ESTUDIANTES>();
                else
                    return concEst;
            }
            catch {
                return Enumerable.Empty<CONCEPTOS_ESTUDIANTES>();
            }
        }

        public DateTime ObtenerPrimeraMensualidad(decimal matriculaCursoId)
        {
            List<object> lista = new List<object>();
            try
            {
                CONCEPTOS_ESTUDIANTES mensualidades = unidad.RepositorioConceptosEstudiantes.GetAll().Where(u => u.MATRICULA_CURSO_ID == matriculaCursoId).OrderBy(u=>u.FECHA_VENCIMIENTO).FirstOrDefault();
                
                return mensualidades.FECHA_VENCIMIENTO;
            }
            catch
            {
                return DateTime.Now;
            }
        }

        public List<object> ObtenerConceptosCursosPorMatricula(decimal idMatricula) {
            List<object> lista = new List<object>();
            try
            {
                IEnumerable<CONCEPTOS_ESTUDIANTES> conceptosCursos = unidad.RepositorioConceptosEstudiantes.GetAll().Where(u => u.MATRICULA_CURSO_ID == idMatricula).OrderBy(u=>u.FECHA_VENCIMIENTO);
                if (conceptosCursos != null)
                {
                    foreach (var concepto in conceptosCursos)
                    {
                        lista.Add(new
                        {
                            fechaV = concepto.FECHA_VENCIMIENTO.ToShortDateString(),
                            monto = concepto.MONTO.ToString("N"),
                            desc = concepto.DESCRIPCION
                        });

                    }
                }
                return lista;
            }
            catch {
                return lista;
            }
        }

        public List<CONCEPTOS_ESTUDIANTES> ObtenerPorUsuario(decimal idUsuario)
        {
            try {
                return ObtenerTodos().Where(m => (  (m.MATRICULA_CURSO_ID.HasValue && m.MATRICULASCURSOS.ALUMNO_ID == idUsuario   && m.MATRICULASCURSOS.CONDICION=="C") ||
                                                    (m.MATRICULATALLER_ID.HasValue && m.MATRICULASTALLERES.ALUMNO_ID == idUsuario) ||
                                                    (m.SOLICITUD_ID.HasValue && m.SOLICITUDES.MATRICULASCURSOS.ALUMNO_ID == idUsuario)) && 
                                                    m.CONDICION == "P").OrderBy(m => m.FECHA_VENCIMIENTO).ToList();
            }
            catch { return null; }
        }

        public string[] ObtenerProximoPago(decimal idAlumno)
        {
            string[] resp=new string[2];
            try
            {
                CONCEPTOS_ESTUDIANTES proximoPago = unidad.RepositorioConceptosEstudiantes.GetAll().Where(u => u.MATRICULASCURSOS.ALUMNO_ID == idAlumno && u.CONDICION == "P").OrderBy(u => u.FECHA_VENCIMIENTO).FirstOrDefault();
                if (proximoPago != null)
                {
                    resp[0] = proximoPago.FECHA_VENCIMIENTO.ToShortDateString();
                    resp[1] = proximoPago.DESCRIPCION;

                }
                else
                {
                    resp[0] = "NO ENCONTRADO";
                    resp[1] = "NO ENCONTRADO";
                }
                return resp;
            }
            catch {
                resp[0] = "NO ENCONTRADO";
                resp[1] = "NO ENCONTRADO";

                return resp;
            }
        }


        public string[] ObtenerProximoPagoPorMatricula(decimal matriculaCursoid)
        {
            string[] resp = new string[2];
            try
            {
                CONCEPTOS_ESTUDIANTES proximoPago = unidad.RepositorioConceptosEstudiantes.GetAll().Where(u => u.MATRICULA_CURSO_ID == matriculaCursoid && u.CONDICION == "P").OrderBy(u => u.FECHA_VENCIMIENTO).FirstOrDefault();
                if (proximoPago != null)
                {
                    resp[0] = proximoPago.FECHA_VENCIMIENTO.ToShortDateString();
                    resp[1] = proximoPago.DESCRIPCION;

                }
                else
                {
                    resp[0] = "NO ENCONTRADO";
                    resp[1] = "NO ENCONTRADO";
                }
                return resp;
            }
            catch
            {
                resp[0] = "NO ENCONTRADO";
                resp[1] = "NO ENCONTRADO";

                return resp;
            }
        }

        public string [] ObtenerUltimoPago(decimal idAlumno) {
            string[] resp = new string[2];
            try
            {
                CONCEPTOS_ESTUDIANTES ultimoPago = unidad.RepositorioConceptosEstudiantes.GetAll().Where(u => u.MATRICULASCURSOS.ALUMNO_ID == idAlumno && u.CONDICION == "C").OrderByDescending(u => u.FECHA_VENCIMIENTO).FirstOrDefault();
                if (ultimoPago != null)
                {
                    resp[0] = ultimoPago.FECHA_VENCIMIENTO.ToShortDateString();
                    resp[1] = ultimoPago.DESCRIPCION;
                }
                else
                {
                    resp[0] = "PAGO NO ENCONTRADO";
                    resp[1] = "PAGO NO ENCONTRADO";
                }
                return resp;
            }
            catch {
                resp[0] = "PAGO NO ENCONTRADO";
                resp[1] = "PAGO NO ENCONTRADO";
                
                return resp;
            }
            
        }

        public string[] ObtenerUltimoPagoPorMatricula(decimal matriculaCursoId)
        {
            string[] resp = new string[2];
            try
            {
                CONCEPTOS_ESTUDIANTES ultimoPago = unidad.RepositorioConceptosEstudiantes.GetAll().Where(u => u.MATRICULA_CURSO_ID == matriculaCursoId && u.CONDICION == "C").OrderByDescending(u => u.FECHA_VENCIMIENTO).FirstOrDefault();
                if (ultimoPago != null)
                {
                    resp[0] = ultimoPago.FECHA_VENCIMIENTO.ToShortDateString();
                    resp[1] = ultimoPago.DESCRIPCION;
                }
                else
                {
                    resp[0] = "PAGO NO ENCONTRADO";
                    resp[1] = "PAGO NO ENCONTRADO";
                }
                return resp;
            }
            catch
            {
                resp[0] = "PAGO NO ENCONTRADO";
                resp[1] = "PAGO NO ENCONTRADO";

                return resp;
            }

        }


    }
}
