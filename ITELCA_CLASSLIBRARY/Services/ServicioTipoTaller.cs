﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using System.Data.Entity;
using ITELCA_CLASSLIBRARY.Models;
using ITELCA_CLASSLIBRARY.Repositories;
using ITELCA_CLASSLIBRARY.CustomClasses;
using System.Collections;
using System.Web;
using UmbrellaClassLibrary.CustomClasses;


namespace ITELCA_CLASSLIBRARY.Services
{
     public class ServicioTipoTaller
    {
        private UnitOfWork unidad;
        public ServicioTipoTaller()
        {
            this.unidad = new UnitOfWork();
        }
        public bool Guardar(TIPOSTALLERES tipoTaller)
        {
            try
            {
                tipoTaller.CONDICION = "C";
                unidad.RepositorioTipoTaller.Add(tipoTaller);
                unidad.Save();
                return true;
            }
            catch
            {
                return false;
            }
        }
        public bool Modificar(TIPOSTALLERES entity)
        {
            try
            {
                entity.CONDICION = "A";
                unidad.RepositorioTipoTaller.Modify(entity);
                unidad.Save();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public List<object> Eliminar(decimal tipoT)
        {
            List<object> lista = new List<object>();
            try
            {
                var aux = unidad.RepositorioTipoTaller.GetById((int)tipoT);
                if (aux != null)
                {
                    lista.Add(new
                    {
                        ok = 1,
                        msg = "Tipo Taller Eliminado"
                    });
                    aux.CONDICION = "E";
                    aux.ESTADO = 0;
                    unidad.RepositorioTipoTaller.Modify(aux);
                    unidad.Save();
                    return lista;
                }
                else
                {
                    lista.Add(new
                    {
                        ok = 0,
                        msg = "Tipo Taller No encontrado"
                    });
                    return lista;
                }
            }
            catch
            {
                lista.Clear();
                lista.Add(new
                {
                    ok = 0,
                    msg = "Error Eliminando Tipo Taller"
                });
                return lista;
            }

        }

        public object ObtenerDataGrid( string sidx, string sord, int page, int rows, string filters)
        {
            int totalPages = 0;
            int totalRecords = 0;
            IEnumerable<TIPOSTALLERES> tipoTalleres;
            tipoTalleres = unidad.RepositorioTipoTaller.GetAll().Where(u => u.ESTADO == 1 || (u.ESTADO == 0 && u.CONDICION != "E"));
            tipoTalleres = JqGrid<TIPOSTALLERES>.GetFilteredContent(sidx, sord, page, rows, filters, tipoTalleres.AsQueryable(), ref totalPages, ref totalRecords);
          
            var rowsModel = (
                from tipoTaller in tipoTalleres.ToList()
                select new
                {
                    i = tipoTaller.TIPO_TALLER_ID,
                    cell = new string[] {
                           tipoTaller.TIPO_TALLER_ID.ToString(),
                           tipoTaller.NOMBRE,
                           tipoTaller.DESCRIPCION,
                           (tipoTaller.ESTADO == 1) ? "Activo" : "Inactivo",
                            "<a title=\"Editar\" href=\"/TipoTaller/Editar/"+tipoTaller.TIPO_TALLER_ID+"\"><span id=\""+tipoTaller.TIPO_TALLER_ID+"\" class=\"ui-icon ui-icon-pencil\"></a>",                          
                            "<span id=\""+tipoTaller.TIPO_TALLER_ID+"\"  class=\"ui-icon ui-icon-close\" ></span>" }
                }).ToArray();
            return JqGrid<TIPOSTALLERES>.SetJsonData(totalPages, totalRecords, page, rowsModel);
        }

        public TIPOSTALLERES ObtenerPorClave(int id)
        {
            try
            {
                return unidad.RepositorioTipoTaller.GetById(id);
                
            }
            catch
            {
                return null;
            }
        }

       
        public IEnumerable<TIPOSTALLERES> ObtenerTodos()
        {
            try
            {
                IEnumerable<TIPOSTALLERES> tiposTaller= unidad.RepositorioTipoTaller.GetAll().Where(u=>u.ESTADO==1).OrderBy(t=>t.NOMBRE);
                if (tiposTaller == null)
                    return Enumerable.Empty<TIPOSTALLERES>();
                else
                    return tiposTaller;
            }
            catch {
                return Enumerable.Empty<TIPOSTALLERES>();
            }
        }

        public bool Duplicado(TIPOSTALLERES tipoTaller)
        {
            try
            {
                if (unidad.RepositorioTipoTaller.GetAll().Where(u => u.NOMBRE.ToLower() == tipoTaller.NOMBRE.ToLower() &&
                                                                 u.TIPO_TALLER_ID != tipoTaller.TIPO_TALLER_ID && u.ESTADO==1).Count() > 0)
                    return true;
                else
                    return false;

            }
            catch 
            {
                return false;
            }
        }

        public IEnumerable<OFERTASTALLERES> ObtenerProximosTalleres(decimal tipoTallerId)
        {
            return unidad.RepositorioOfertasTalleres.GetAll().Where(u => u.TALLERES.TIPO_TALLER_ID == tipoTallerId && u.CONDICION != "E").OrderBy(u=> u.FECHA_INICIO);
        }

      
    }
}
