﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using System.Data.Entity;
using ITELCA_CLASSLIBRARY.Models;
using ITELCA_CLASSLIBRARY.Repositories;
using ITELCA_CLASSLIBRARY.CustomClasses;
using System.Collections;
using System.Web;
using UmbrellaClassLibrary.CustomClasses;
using System.Data.Entity.Validation;
using System.Web.Mvc;
using System.Net.Mail;

namespace ITELCA_CLASSLIBRARY.Services
{
     public class ServicioContacto
    {
        private UnitOfWork unidad;
        public ServicioContacto()
        {
            this.unidad = new UnitOfWork();
        }
       
        public bool Guardar(CONTACTO contacto)
        {
            try
            {
                
                unidad.RepositorioContacto.Add(contacto);
                unidad.Save();
                return true;
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }

                return false;
            }
        }

        

    }
}
