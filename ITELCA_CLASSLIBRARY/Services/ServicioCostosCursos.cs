﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using System.Data.Entity;
using ITELCA_CLASSLIBRARY.Models;
using ITELCA_CLASSLIBRARY.Repositories;
using ITELCA_CLASSLIBRARY.CustomClasses;
using System.Collections;
using System.Web;
using UmbrellaClassLibrary.CustomClasses;
using System.Web.Mvc;


namespace ITELCA_CLASSLIBRARY.Services
{
     public class ServicioCostosCursos
    {
        private UnitOfWork unidad;
        public ServicioCostosCursos()
        {
            this.unidad = new UnitOfWork();
        }
        public bool Guardar(COSTOS_CURSOS curso)
        {
            try
            {
              unidad.RepositorioCostosCursos.Add(curso);          
                unidad.Save();
                return true;
            }
            catch
            {
                return false;
            }
        }
        public bool Modificar(COSTOS_CURSOS entity)
        {
            try
            {

                unidad.RepositorioCostosCursos.Modify(entity);
                unidad.Save();
                return true;
            }
            catch
            {
                return false;
            }
        }


        public List<object> Eliminar(decimal id)
        {
            List<object> lista = new List<object>();
            try
            {
                var aux = unidad.RepositorioCostosCursos.GetById((int)id);
                if (aux != null)
                {
                    lista.Add(new
                    {
                        ok = 1,
                        msg = "Costo Curso Eliminado"
                    });
                    unidad.RepositorioCostosCursos.Delete(aux);
                    unidad.Save();
                    return lista;
                }
                else
                {
                    lista.Add(new
                    {
                        ok = 0,
                        msg = "Costo Curso No encontrado"
                    });
                    return lista;
                }
            }
            catch
            {
                lista.Clear();
                lista.Add(new
                {
                    ok = 0,
                    msg = "Error Eliminando Costo Curso"
                });
                return lista;
            }

        }

        public object ObtenerDataGrid(decimal id, string sidx, string sord, int page, int rows, string filters)
        {
            int totalPages = 0;
            int totalRecords = 0;
            IEnumerable<COSTOS_CURSOS> cursos;
            cursos = unidad.RepositorioCostosCursos.GetAll().Where(u => u.CURSO_ID == id);
            var rowsModel = (
                from curso in cursos.ToList()
                select new
                {
                    i = curso.COSTO_CURSO_ID,
                    cell = new string[] {
                           curso.COSTO_CURSO_ID.ToString(),
                           curso.CURSOS.NOMBRE.ToString(),
                           curso.TURNOS.NOMBRE.ToString(),
                           curso.MONTO.ToString(),
                            "<a title=\"Editar\" href=\"/CostosCursos/Editar/"+ curso.COSTO_CURSO_ID+"\"><span id=\""+curso.COSTO_CURSO_ID+"\" class=\"ui-icon ui-icon-pencil\"></a>",                          
                            "<span id=\""+curso.COSTO_CURSO_ID+"\"  class=\"ui-icon ui-icon-close\" ></span>" }
                }).ToArray();
            return JqGrid<COSTOS_CURSOS>.SetJsonData(totalPages, totalRecords, page, rowsModel);
        }

        public COSTOS_CURSOS ObtenerPorClave(int id)
        {
            try
            {
                return unidad.RepositorioCostosCursos.GetById(id);
                
            }
            catch
            {
                return null;
            }
        }

        public COSTOS_CURSOS ObtenerPorCursoyTurno(int CURSO_ID, int TURNO_ID)
        {
            try
            {
                return unidad.RepositorioCostosCursos.GetAll().Where( u=> u.CURSO_ID == CURSO_ID && u.TURNO_ID == TURNO_ID).FirstOrDefault();

            }
            catch
            {
                return null;
            }
        }

       
        public IEnumerable<COSTOS_CURSOS> ObtenerTodos()
        {
            try
            {
                IEnumerable<COSTOS_CURSOS> cursos = unidad.RepositorioCostosCursos.GetAll();
                if (cursos == null)
                    return Enumerable.Empty<COSTOS_CURSOS>();
                else
                    return cursos;
            }
            catch  {
                return Enumerable.Empty<COSTOS_CURSOS>();
            }
        }

        public bool Duplicado(COSTOS_CURSOS cursos)
        {
            try
            {
                if (unidad.RepositorioCostosCursos.GetAll().Where(u => u.MONTO == cursos.MONTO &&
                                                                 u.CURSO_ID!=cursos.CURSO_ID).Count() > 0)
                    return true;
                else
                    return false;

            }
            catch 
            {
                return false;
            }
        }
        public List<object> ObtenerObjeto()
        {
            List<object> lista = new List<object>();
            try
            {

                foreach (var ope in unidad.RepositorioCostosCursos.GetAll())
                    lista.Add(new
                    {
                        key = ope.COSTO_CURSO_ID,
                        value = ope.CURSOS.NOMBRE
                    });




                return lista;
            }
            catch
            {
                return null;
            }
        }

       
    }
}
