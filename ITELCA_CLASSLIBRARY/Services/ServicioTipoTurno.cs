﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using System.Data.Entity;
using ITELCA_CLASSLIBRARY.Models;
using ITELCA_CLASSLIBRARY.Repositories;
using ITELCA_CLASSLIBRARY.CustomClasses;
using System.Collections;
using System.Web;
using UmbrellaClassLibrary.CustomClasses;

namespace ITELCA_CLASSLIBRARY.Services
{
     public class ServicioTipoTurno
    {
        private UnitOfWork unidad;
        public ServicioTipoTurno()
        {
            this.unidad = new UnitOfWork();
        }

         public Dictionary<string,string> ObtenerLista(){

             try
             {
                 Dictionary<string, string> lista = new Dictionary<string, string>();
                 IEnumerable<TIPOSTURNOS> tiposTurnos= unidad.RepositorioTiposTurnos.GetAll();
                 foreach (var ope in tiposTurnos) {
                     lista.Add(ope.TIPOTURNO_ID.ToString(), ope.NOMBRE);
                 }
                 return lista;

             }
             catch {
                 return new Dictionary<string, string>() ;
             }
         
         
         }

         public IEnumerable<TIPOSTURNOS> ObtenerTodos()
         {
             try
             {
                 IEnumerable<TIPOSTURNOS> turnos = unidad.RepositorioTiposTurnos.GetAll().OrderBy(t => t.NOMBRE);
                 if (turnos == null)
                     return Enumerable.Empty<TIPOSTURNOS>();
                 else
                     return turnos;
             }
             catch
             {
                 return Enumerable.Empty<TIPOSTURNOS>();
             }
         }
       
    }
}
