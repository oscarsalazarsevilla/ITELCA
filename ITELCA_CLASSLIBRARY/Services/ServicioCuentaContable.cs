﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using System.Data.Entity;
using ITELCA_CLASSLIBRARY.Models;
using ITELCA_CLASSLIBRARY.Repositories;
using ITELCA_CLASSLIBRARY.CustomClasses;
using System.Collections;
using System.Web;
using UmbrellaClassLibrary.CustomClasses;

namespace ITELCA_CLASSLIBRARY.Services
{
     public class ServicioCuentasContables
    {
        private UnitOfWork unidad;
        public ServicioCuentasContables()
        {
            this.unidad = new UnitOfWork();
        }
        public bool Guardar(CUENTAS_CONTABLES cuenta_contables)
        {
            try
            {
                unidad.RepositorioCuentasContables.Add(cuenta_contables);
                unidad.Save();
                return true;
            }
            catch
            {
                return false;
            }
        }
        public bool Modificar(CUENTAS_CONTABLES cuenta_contables)
        {
            try
            {
                unidad.RepositorioCuentasContables.Modify(cuenta_contables);
                unidad.Save();
                return true;
            }
            catch
            {
                return false;
            }
        }


        public List<object> Eliminar(decimal id)
        {
            List<object> lista = new List<object>();
            try
            {
                var aux = unidad.RepositorioCuentasContables.GetById((int)id);
                if (aux != null)
                {
                    lista.Add(new
                    {
                        ok = 1,
                        msg = "Cuenta contable eliminada"
                    });
                    unidad.RepositorioCuentasContables.Delete(aux);
                    unidad.Save();
                    return lista;
                }
                else
                {
                    lista.Add(new
                    {
                        ok = 0,
                        msg = "Cuenta contable no encontrada"
                    });
                    return lista;
                }
            }
            catch
            {
                lista.Add(new
                {
                    ok = 0,
                    msg = "Error eliminando la cuenta contable"
                });
                return lista;
            }

        }

        public CUENTAS_CONTABLES ObtenerPorClave(int id)
        {
            try
            {
                return unidad.RepositorioCuentasContables.GetById(id);
                
            }
            catch 
            {
                return null;
            }
        }
       
        public IEnumerable<CUENTAS_CONTABLES> ObtenerTodos()
        {
            try
            {
                IEnumerable<CUENTAS_CONTABLES> cuenta_contabless_contables = unidad.RepositorioCuentasContables.GetAll();
                if (cuenta_contabless_contables == null)
                    return Enumerable.Empty<CUENTAS_CONTABLES>();
                else
                    return cuenta_contabless_contables;
            }
            catch {
                return Enumerable.Empty<CUENTAS_CONTABLES>();
            }
        }

        public CUENTAS_CONTABLES ObtenerPorTipoYSede(decimal sede_id, string tipo)
        {
            return ObtenerTodos().Where(c => c.TIPO == tipo &&
                                            c.SEDE_ID == sede_id).FirstOrDefault();
        }
    }
}
