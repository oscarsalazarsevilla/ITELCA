﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using System.Data.Entity;
using ITELCA_CLASSLIBRARY.Models;
using ITELCA_CLASSLIBRARY.Repositories;
using ITELCA_CLASSLIBRARY.CustomClasses;
using System.Collections;
using System.Web;
using UmbrellaClassLibrary.CustomClasses;
using System.Data.Entity.Validation;


namespace ITELCA_CLASSLIBRARY.Services
{
     public class ServicioTurnoViernes
    {
        private UnitOfWork unidad;
        public ServicioTurnoViernes()
        {
            this.unidad = new UnitOfWork();
        }
        public bool Guardar(TURNOSVIERNES turnoViernes)
        {
            try
            {
                turnoViernes.CONDICION = "C";
                string[] arregloTurnos = turnoViernes.MULTI_TURNO_ID.Split(',');
                string[] arregloViernes = turnoViernes.MULTI_VIERNES.Split(',');

                foreach (var turno in arregloTurnos)
                {
                    foreach (var fecha in arregloViernes)
                    {
                        TURNOSVIERNES turnoV = new TURNOSVIERNES();
                        turnoV.FECHA_CREACION = DateTime.Now;
                        turnoV.ANIO = turnoViernes.ANIO;
                        turnoV.CONDICION = turnoViernes.CONDICION;
                        turnoV.ESTADO = 1;
                        turnoV.TURNO_ID = Decimal.Parse(turno);
                        turnoV.VIERNES = DateTime.Parse(fecha);
                        turnoV.USUARIO_ID = turnoViernes.USUARIO_ID;

                        TURNOSVIERNES aux = unidad.RepositorioTurnoViernes.GetAll().Where(c => c.TURNO_ID == turnoV.TURNO_ID &&
                                                                            c.VIERNES == turnoV.VIERNES && c.ESTADO == 1).FirstOrDefault();
                        if (aux == null)
                            unidad.RepositorioTurnoViernes.Add(turnoV);
                    }
                }

                unidad.Save();
                return true;
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                return false;
            }
        }
        public bool Modificar(TURNOSVIERNES entity)
        {
            try
            {
                TURNOSVIERNES turnoViernes = unidad.RepositorioTurnoViernes.GetById((int)entity.TURNOVIERNES_ID);
                turnoViernes.CONDICION = "M";
                turnoViernes.ESTADO = entity.ESTADO;
                turnoViernes.VIERNES = entity.VIERNES;
                turnoViernes.TURNO_ID = entity.TURNO_ID;
                turnoViernes.ANIO = entity.ANIO;      
         
                unidad.RepositorioTurnoViernes.Modify(turnoViernes);
                unidad.Save();
                return true;
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                return false;
            }
        }


        public List<object> Eliminar(decimal id)
        {
            List<object> lista = new List<object>();
            try
            {
                var aux = unidad.RepositorioTurnoViernes.GetById((int)id);
                if (aux != null)
                {
                    lista.Add(new
                    {
                        ok = 1,
                        msg = "Turno Viernes Eliminado"
                    });
                    aux.CONDICION = "E";
                    aux.ESTADO = 0;
                    unidad.RepositorioTurnoViernes.Modify(aux);
                    unidad.Save();
                    return lista;
                }
                else
                {
                    lista.Add(new
                    {
                        ok = 0,
                        msg = "Turno Viernes No encontrado"
                    });
                    return lista;
                }
            }
            catch
            {
                lista.Clear();
                lista.Add(new
                {
                    ok = 0,
                    msg = "Error Eliminando Turno Viernes"
                });
                return lista;
            }

        }

        public object ObtenerDataGrid( string sidx, string sord, int page, int rows, string filters)
        {
            int totalPages = 0;
            int totalRecords = 0;
            IEnumerable<TURNOSVIERNES> turnoVierness;
            turnoVierness = unidad.RepositorioTurnoViernes.GetAll().Where(u => u.ESTADO == 1 || (u.ESTADO == 0 && u.CONDICION != "E"));
            turnoVierness = JqGrid<TURNOSVIERNES>.GetFilteredContent(sidx, sord, page, rows, filters, turnoVierness.AsQueryable(), ref totalPages, ref totalRecords);
          
            var rowsModel = (
                from turnoViernes in turnoVierness.ToList()
                select new
                {
                    i = turnoViernes.TURNOVIERNES_ID,
                    cell = new string[] {
                           turnoViernes.TURNOVIERNES_ID.ToString(),
                           turnoViernes.TURNOS.NOMBRE.ToString(),
                           turnoViernes.ANIO.ToString(),
                           turnoViernes.VIERNES.ToShortDateString(),
                           (turnoViernes.ESTADO == 1) ? "Activo" : "Inactivo",
                            "<a title=\"Editar\" href=\"/TurnoViernes/Editar/"+ turnoViernes.TURNOVIERNES_ID+"\"><span id=\""+turnoViernes.TURNOVIERNES_ID+"\" class=\"ui-icon ui-icon-pencil\"></a>",                          
                            "<span id=\""+turnoViernes.TURNOVIERNES_ID+"\"  class=\"ui-icon ui-icon-close\" ></span>" }
                }).ToArray();
            return JqGrid<CURSOS>.SetJsonData(totalPages, totalRecords, page, rowsModel);
        }

        public TURNOSVIERNES ObtenerPorClave(long id)
        {
            try
            {
                return unidad.RepositorioTurnoViernes.GetById(id);
                
            }
            catch 
            {
                return null;
            }
        }

       
        public IEnumerable<TURNOSVIERNES> ObtenerTodos()
        {
            try
            {
                IEnumerable<TURNOSVIERNES> turnoVierness= unidad.RepositorioTurnoViernes.GetAll().Where(u=>u.ESTADO==1);
                if (turnoVierness == null)
                    return Enumerable.Empty<TURNOSVIERNES>();
                else
                    return turnoVierness;
            }
            catch {
                return Enumerable.Empty<TURNOSVIERNES>();
            }
        }

        public bool Duplicado(TURNOSVIERNES turnoVierness)
        {
            try
            {
                if (unidad.RepositorioTurnoViernes.GetAll().Where(u => u.VIERNES == turnoVierness.VIERNES 
                                                                    && u.ANIO == turnoVierness.ANIO 
                                                                    && u.TURNO_ID == turnoVierness.TURNO_ID 
                                                                    && u.TURNOVIERNES_ID != turnoVierness.TURNOVIERNES_ID
                                                                    && u.ESTADO == 1).Count() > 0)
                    return true;
                else
                    return false;

            }
            catch
            {
                return false;
            }
        }

        public Dictionary<decimal, string> ObtenerDiccionarioTurnoViernes()
        {
            Dictionary<decimal, string> lista = new Dictionary<decimal, string>();
            foreach (var entity in ObtenerTodos().ToList())
            {
                lista.Add(entity.TURNOVIERNES_ID, entity.VIERNES.ToShortDateString());
            }
            return lista;
        }

        public List<object> ObtenerObjeto()
        {
            List<object> lista = new List<object>();
            try
            {

                foreach (var ope in ObtenerTodos())
                    lista.Add(new
                    {
                        key = ope.TURNOVIERNES_ID,
                        value = ope.VIERNES
                    });
                return lista;
            }
            catch
            {
                return null;
            }
        }
        public bool ValidarViernes(DateTime fecha,decimal turnoId) {
            try
            {
                return unidad.RepositorioTurnoViernes.GetAll().Where(u => u.VIERNES.Day == fecha.Day &&
                                                                 u.VIERNES.Month == fecha.Month &&
                                                                 u.VIERNES.Year == fecha.Year &&
                                                                 u.TURNO_ID == turnoId).Count() > 0;
            }
            catch {
                return false;
            
            }
        }
       
    }
}
