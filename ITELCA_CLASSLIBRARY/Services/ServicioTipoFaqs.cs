﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using System.Data.Entity;
using ITELCA_CLASSLIBRARY.Models;
using ITELCA_CLASSLIBRARY.Repositories;
using ITELCA_CLASSLIBRARY.CustomClasses;
using System.Collections;
using System.Web;
using UmbrellaClassLibrary.CustomClasses;


namespace ITELCA_CLASSLIBRARY.Services
{
     public class ServicioTipoFaqs
    {
        private UnitOfWork unidad;
        public ServicioTipoFaqs()
        {
            this.unidad = new UnitOfWork();
        }
        public bool Guardar(TIPOS_FAQS tipoFaq)
        {
            try
            {
                tipoFaq.CONDICION = "C";
                unidad.RepositorioTipoFaqs.Add(tipoFaq);
                unidad.Save();
                return true;
            }
            catch
            {
                return false;
            }
        }
        public bool Modificar(TIPOS_FAQS entity)
        {
            try
            {
                entity.CONDICION = "A";
                unidad.RepositorioTipoFaqs.Modify(entity);
                unidad.Save();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public List<object> Eliminar(decimal tipoA)
        {
            List<object> lista = new List<object>();
            try
            {
                var aux = unidad.RepositorioTipoFaqs.GetById((int)tipoA);
                if (aux != null)
                {
                    lista.Add(new
                    {
                        ok = 1,
                        msg = "Tipo Faq Eliminada"
                    });
                    unidad.RepositorioTipoFaqs.Delete(aux);
                    unidad.Save();
                    return lista;
                }
                else
                {
                    lista.Add(new
                    {
                        ok = 0,
                        msg = "Tipo Faq No encontrada"
                    });
                    return lista;
                }
            }
            catch
            {
                lista.Clear();
                lista.Add(new
                {
                    ok = 0,
                    msg = "Error Eliminando Tipo Faq"
                });
                return lista;
            }

        }

        public object ObtenerDataGrid( string sidx, string sord, int page, int rows, string filters)
        {
            int totalPages = 0;
            int totalRecords = 0;
            IEnumerable<TIPOS_FAQS> tipoFaqs;
            tipoFaqs = unidad.RepositorioTipoFaqs.GetAll().Where(u => u.ESTADO == 1 || (u.ESTADO == 0 && u.CONDICION != "E"));
            tipoFaqs = JqGrid<TIPOS_FAQS>.GetFilteredContent(sidx, sord, page, rows, filters, tipoFaqs.AsQueryable(), ref totalPages, ref totalRecords);
          
            var rowsModel = (
                from tipoFaq in tipoFaqs.ToList()
                select new
                {
                    i = tipoFaq.TIPO_FAQS_ID,
                    cell = new string[] {
                           tipoFaq.TIPO_FAQS_ID.ToString(),
                           tipoFaq.NOMBRE,
                           tipoFaq.DESCRIPCION,
                           tipoFaq.ESTADO.ToString(),
                           "<a title=\"Editar\" href=\"/TipoFaq/Editar/"+ tipoFaq.TIPO_FAQS_ID+"\"><span id=\""+tipoFaq.TIPO_FAQS_ID+"\" class=\"ui-icon ui-icon-pencil\"></a>",                          
                           "<span id=\""+tipoFaq.TIPO_FAQS_ID+"\"  class=\"ui-icon ui-icon-close\" ></span>" }
                }).ToArray();
            return JqGrid<TIPOS_FAQS>.SetJsonData(totalPages, totalRecords, page, rowsModel);
        }

        public TIPOS_FAQS ObtenerPorClave(int id)
        {
            try
            {
                return unidad.RepositorioTipoFaqs.GetById(id);
                
            }
            catch
            {
                return null;
            }
        }

       
        public IEnumerable<TIPOS_FAQS> ObtenerTodos()
        {
            try
            {
                IEnumerable<TIPOS_FAQS> tiposFaqs= unidad.RepositorioTipoFaqs.GetAll();
                if (tiposFaqs == null)
                    return Enumerable.Empty<TIPOS_FAQS>();
                else
                    return tiposFaqs;
            }
            catch {
                return Enumerable.Empty<TIPOS_FAQS>();
            }
        }

        public bool Duplicado(TIPOS_FAQS tipoFaq)
        {
            try
            {
                if (unidad.RepositorioTipoFaqs.GetAll().Where(u => u.NOMBRE.ToLower() == tipoFaq.NOMBRE.ToLower() &&
                                                                 u.TIPO_FAQS_ID != tipoFaq.TIPO_FAQS_ID).Count() > 0)
                    return true;
                else
                    return false;

            }
            catch 
            {
                return false;
            }
        }
      
    }
}
