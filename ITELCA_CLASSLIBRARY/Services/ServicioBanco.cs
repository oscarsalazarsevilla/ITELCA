﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using System.Data.Entity;
using ITELCA_CLASSLIBRARY.Models;
using ITELCA_CLASSLIBRARY.Repositories;
using ITELCA_CLASSLIBRARY.CustomClasses;
using System.Collections;
using System.Web;
using UmbrellaClassLibrary.CustomClasses;

namespace ITELCA_CLASSLIBRARY.Services
{
     public class ServicioBanco
    {
        private UnitOfWork unidad;
        public ServicioBanco()
        {
            this.unidad = new UnitOfWork();
        }
        public bool Guardar(BANCOS banco)
        {
            try
            {
                unidad.RepositorioBancos.Add(banco);
                unidad.Save();
                return true;
            }
            catch
            {
                return false;
            }
        }
        public bool Modificar(BANCOS banco)
        {
            try
            {
                unidad.RepositorioBancos.Modify(banco);
                unidad.Save();
                return true;
            }
            catch
            {
                return false;
            }
        }


        public List<object> Eliminar(decimal id)
        {
            List<object> lista = new List<object>();
            try
            {
                var aux = unidad.RepositorioBancos.GetById((int)id);
                if (aux != null)
                {
                    lista.Add(new
                    {
                        ok = 1,
                        msg = "Banco eliminado"
                    });
                    unidad.RepositorioBancos.Delete(aux);
                    unidad.Save();
                    return lista;
                }
                else
                {
                    lista.Add(new
                    {
                        ok = 0,
                        msg = "Banco no encontrado"
                    });
                    return lista;
                }
            }
            catch
            {
                lista.Add(new
                {
                    ok = 0,
                    msg = "Error eliminando el banco"
                });
                return lista;
            }

        }

        public BANCOS ObtenerPorClave(int id)
        {
            try
            {
                return unidad.RepositorioBancos.GetById(id);
                
            }
            catch 
            {
                return null;
            }
        }

        public BANCOS ObtenerPorClave(string codigo)
        {
            try
            {
                return ObtenerTodos().Where(b => b.CODIGO.ToLower() == codigo.ToLower()).FirstOrDefault();

            }
            catch
            {
                return null;
            }
        }

       
        public IEnumerable<BANCOS> ObtenerTodos()
        {
            try
            {
                IEnumerable<BANCOS> bancos= unidad.RepositorioBancos.GetAll();
                if (bancos == null)
                    return Enumerable.Empty<BANCOS>();
                else
                    return bancos;
            }
            catch {
                return Enumerable.Empty<BANCOS>();
            }
        }
        public Dictionary<string, string> ObtenerLista()
        {
            Dictionary<string, string> lista = new Dictionary<string, string>();
            try
            {
                IEnumerable<BANCOS> bancos = unidad.RepositorioBancos.GetAll();
                if (bancos != null)
                    foreach (var banco in bancos)
                        lista.Add(banco.BANCO_ID.ToString(), banco.NOMBRE);
                return lista;

            }
            catch
            {
                return lista;
            }
        }
    }
}
