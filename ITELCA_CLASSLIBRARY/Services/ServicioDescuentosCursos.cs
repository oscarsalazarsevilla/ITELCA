﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using System.Data.Entity;
using ITELCA_CLASSLIBRARY.Models;
using ITELCA_CLASSLIBRARY.Repositories;
using ITELCA_CLASSLIBRARY.CustomClasses;
using System.Collections;
using System.Web;
using UmbrellaClassLibrary.CustomClasses;
using System.Web.Mvc;
using System.Globalization;


namespace ITELCA_CLASSLIBRARY.Services
{
     public class ServicioDescuentosCursos
    {
        private UnitOfWork unidad;
        public ServicioDescuentosCursos()
        {
            this.unidad = new UnitOfWork();
        }
        public bool Guardar(DESCUENTOS_CURSOS curso, FormCollection form)
        {
            try
            {
                string cursos = form["listacursos"];
                string turnos = form["listaTurnos"];
                string[] listaCursos = cursos.Split(',');
                string[] listaTurnos = turnos.Split(',');

                if (listaCursos != null)
                {
                    foreach (var lista in listaCursos)
                    {
                        DESCUENTOS_CURSOS cursoG = curso;
                        cursoG.CURSO_ID = Int32.Parse(lista);
                        if(listaTurnos!=null){
                            foreach (var ope in listaTurnos)
                            {
                                cursoG.TURNO_ID = decimal.Parse(ope);
                                unidad.RepositorioDescuentosCursos.Add(cursoG);
                                unidad.Save();
                            }
                        }
                    }
                }
                     
                return true;
            }
            catch
            {
                return false;
            }
        }
        public bool Modificar(DESCUENTOS_CURSOS entity)
        {
            try
            {

                unidad.RepositorioDescuentosCursos.Modify(entity);
                unidad.Save();
                return true;
            }
            catch
            {
                return false;
            }
        }


        public List<object> Eliminar(decimal id)
        {
            List<object> lista = new List<object>();
            try
            {
                var aux = unidad.RepositorioDescuentosCursos.GetById((int)id);
                if (aux != null)
                {
                    lista.Add(new
                    {
                        ok = 1,
                        msg = "Costo Curso Eliminado"
                    });
                    unidad.RepositorioDescuentosCursos.Delete(aux);
                    unidad.Save();
                    return lista;
                }
                else
                {
                    lista.Add(new
                    {
                        ok = 0,
                        msg = "Costo Curso No encontrado"
                    });
                    return lista;
                }
            }
            catch
            {
                lista.Clear();
                lista.Add(new
                {
                    ok = 0,
                    msg = "Error Eliminando Costo Curso"
                });
                return lista;
            }

        }

        public object ObtenerDataGrid(string cursoId,string sidx, string sord, int page, int rows, string filters)
        {
            int totalPages = 0;
            int totalRecords = 0;
            IEnumerable<DESCUENTOS_CURSOS> descuentos;
            if (cursoId != "")
            {
                decimal curso = decimal.Parse(cursoId);
                descuentos = ObtenerTodos().Where(u => u.CURSO_ID == curso).OrderBy(u => u.CURSOS.NOMBRE);
            }
            else {
                descuentos = ObtenerTodos();
            }

           
            descuentos = JqGrid<DESCUENTOS_CURSOS>.GetFilteredContent(sidx, sord, page, rows, filters, descuentos.AsQueryable(), ref totalPages, ref totalRecords); 
            var rowsModel = (
                from descuento in descuentos.ToList()
                select new
                {
                    i = descuento.DECUENTO_CURSO_ID,
                    cell = new string[] {
                           descuento.DECUENTO_CURSO_ID.ToString(),
                           descuento.CURSOS.NOMBRE,
                           descuento.TURNOS.NOMBRE,
                           descuento.FECHA_INIICIO.ToString("dd/MM/yyyy"),
                           descuento.FECHA_FIN.ToString("dd/MM/yyyy"),
                           String.Format("{0:N2}",descuento.MONTO),
                           "<a title=\"Editar\" href=\"/DescuentosCursos/Editar/"+ descuento.DECUENTO_CURSO_ID+"\"><span id=\""+descuento.DECUENTO_CURSO_ID+"\" class=\"ui-icon ui-icon-pencil\"></a>",                          
                           "<span id=\""+descuento.DECUENTO_CURSO_ID+"\"  class=\"ui-icon ui-icon-close\" ></span>" }
                }).ToArray();
            return JqGrid<DESCUENTOS_CURSOS>.SetJsonData(totalPages, totalRecords, page, rowsModel);
        }

       
        public DESCUENTOS_CURSOS ObtenerPorClave(int id)
        {
            try
            {
                return unidad.RepositorioDescuentosCursos.GetById(id);
                
            }
            catch
            {
                return null;
            }
        }

        public DESCUENTOS_CURSOS ObtenerTurnoYCurso(decimal cursoId, decimal turnoId, DateTime fechaInicioCurso)
        {
            try
            {
                return unidad.RepositorioDescuentosCursos.GetAll().Where(u => u.CURSO_ID == cursoId && u.TURNO_ID == turnoId && u.FECHA_FIN >= fechaInicioCurso).FirstOrDefault();

            }
            catch
            {
                return null;
            }
        }

       
        public IEnumerable<DESCUENTOS_CURSOS> ObtenerTodos()
        {
            try
            {
                IEnumerable<DESCUENTOS_CURSOS> cursos = unidad.RepositorioDescuentosCursos.GetAll();
                if (cursos == null)
                    return Enumerable.Empty<DESCUENTOS_CURSOS>();
                else
                    return cursos;
            }
            catch  {
                return Enumerable.Empty<DESCUENTOS_CURSOS>();
            }
        }


        //public IEnumerable<DESCUENTOS_CURSOS> ObtenerCursosPorDescuentosCursos(Int32 id_descuento_curso)
        //{
        //    try
        //    {
        //        IEnumerable<DESCUENTOS_CURSOS> cursos = unidad.RepositorioDescuentosCursos.GetAll().Where(u => u. == id_curso);
        //        if (cursos == null)
        //            return Enumerable.Empty<DESCUENTOS_CURSOS>();
        //        else
        //            return cursos;
        //    }
        //    catch
        //    {
        //        return Enumerable.Empty<DESCUENTOS_CURSOS>();
        //    }
        //}

       

        public bool Duplicado(DESCUENTOS_CURSOS cursos)
        {
            try
            {
                if (unidad.RepositorioCostosCursos.GetAll().Where(u => u.MONTO == cursos.MONTO &&
                                                                 u.CURSO_ID!=cursos.CURSO_ID).Count() > 0)
                    return true;
                else
                    return false;

            }
            catch 
            {
                return false;
            }
        }
        public List<object> ObtenerObjeto()
        {
            List<object> lista = new List<object>();
            try
            {

                foreach (var ope in unidad.RepositorioDescuentosCursos.GetAll())
                    lista.Add(new
                    {
                        key = ope.DECUENTO_CURSO_ID,
                        value = ope.CURSOS.NOMBRE
                    });




                return lista;
            }
            catch
            {
                return null;
            }
        }
       
    }
}
