﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using System.Data.Entity;
using ITELCA_CLASSLIBRARY.Models;
using ITELCA_CLASSLIBRARY.Repositories;
using ITELCA_CLASSLIBRARY.CustomClasses;
using System.Collections;
using System.Web;
using UmbrellaClassLibrary.CustomClasses;
using System.Data.Entity.Validation;

namespace ITELCA_CLASSLIBRARY.Services
{
     public class ServicioConfiguracion
    {
        private UnitOfWork unidad;
        public ServicioConfiguracion()
        {
            this.unidad = new UnitOfWork();
        }
       
        public bool Modificar(CONFIGURACIONES configuracion)
        {
            try
            {
                var existeConfig = this.ObtenerConfiguracion();
                if (existeConfig == null)
                {
                    unidad.RepositorioConfiguraciones.Add(configuracion);
                }
                else
                {
                    existeConfig.INSTITUCION = configuracion.INSTITUCION;
                    existeConfig.LINKEDIN = configuracion.LINKEDIN;
                    existeConfig.FACEBOOK = configuracion.FACEBOOK;
                    existeConfig.TWITTER = configuracion.TWITTER;
                    existeConfig.SMTPSERVER = configuracion.SMTPSERVER;
                    existeConfig.SMTPPUERTO = configuracion.SMTPPUERTO;
                    existeConfig.LOGO = configuracion.LOGO;
                    existeConfig.PWDEMAIL = configuracion.PWDEMAIL;
                    existeConfig.EMAIL = configuracion.EMAIL;
                    existeConfig.DIRECTOR = configuracion.DIRECTOR;
                    existeConfig.SUBDIRECTOR = configuracion.SUBDIRECTOR;
                    existeConfig.TELEFONO1 = configuracion.TELEFONO1;
                    existeConfig.TELEFONO2 = configuracion.TELEFONO2;
                    existeConfig.VERSION = configuracion.VERSION;
                    existeConfig.WEB = configuracion.WEB;
                    existeConfig.DIRECCION_FISCAL = configuracion.DIRECCION_FISCAL;
                    existeConfig.CONTROLESTUDIOS = configuracion.CONTROLESTUDIOS;

                    unidad.RepositorioConfiguraciones.Modify(existeConfig);
                }
                unidad.Save();
                return true;
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }

                return false;
            }
        }


      

        public CONFIGURACIONES ObtenerConfiguracion()
        {
            try
            {
                return unidad.RepositorioConfiguraciones.GetAll().FirstOrDefault();

            }
            catch
            {
                return null;
            }
        }

    }
}
