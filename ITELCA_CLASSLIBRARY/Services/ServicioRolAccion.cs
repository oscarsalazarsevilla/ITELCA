﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ITELCA_CLASSLIBRARY.Repositories;
using ITELCA_CLASSLIBRARY.Models;
using ITELCA_CLASSLIBRARY.CustomClasses;
using System.Collections;
using System.Data.Objects;
using System.Data.Entity.Validation;

namespace ITELCA_CLASSLIBRARY.Services
{
    public class ServicioRolAccion
    {
        private UnitOfWork unidad;
        private ServicioRol _servicioRol = new ServicioRol();
        public ServicioRolAccion()
        {
            this.unidad = new UnitOfWork();
        }

        public bool Guardar(decimal ROL_ID, string acciones)
        {

            try
            {
                string[] accId = acciones.Split(',');
                ROLES rol = unidad.RepositorioRoles.GetAll().Where(u => u.ROL_ID == ROL_ID).FirstOrDefault();
            
                unidad.Save();
                return true;
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                return false;

            }
        }

        
    }

    

}