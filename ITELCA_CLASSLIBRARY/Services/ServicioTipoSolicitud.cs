﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using System.Data.Entity;
using ITELCA_CLASSLIBRARY.Models;
using ITELCA_CLASSLIBRARY.Repositories;
using ITELCA_CLASSLIBRARY.CustomClasses;
using System.Collections;
using System.Web;
using UmbrellaClassLibrary.CustomClasses;


namespace ITELCA_CLASSLIBRARY.Services
{
     public class ServicioTipoSolicitud
    {

         private UnitOfWork _unidad;
        
         public ServicioTipoSolicitud()
        {
            this._unidad = new UnitOfWork();
        }
        
         public bool Guardar(TIPOSSOLICITUDES tipoSolicitud)
        {
            try
            {
                _unidad.RepositorioTiposSolicitudes.Add(tipoSolicitud);
                _unidad.Save();
                return true;
            }
            catch
            {
                return false;
            }
        }
        public bool Modificar(TIPOSSOLICITUDES entity)
        {
            try
            {
                _unidad.RepositorioTiposSolicitudes.Modify(entity);
                _unidad.Save();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public List<object> Eliminar(decimal tipoT)
        {
            List<object> lista = new List<object>();
            try
            {
                var aux = _unidad.RepositorioTiposSolicitudes.GetById((int)tipoT);
                if (aux != null)
                {
                    lista.Add(new
                    {
                        ok = 1,
                        msg = "Tipo de solicitud eliminado"
                    });
                    _unidad.RepositorioTiposSolicitudes.Modify(aux);
                    _unidad.Save();
                    return lista;
                }
                else
                {
                    lista.Add(new
                    {
                        ok = 0,
                        msg = "Tipo de solicitud no encontrado"
                    });
                    return lista;
                }
            }
            catch
            {
                lista.Clear();
                lista.Add(new
                {
                    ok = 0,
                    msg = "Error eliminando el tipo de solicitud"
                });
                return lista;
            }

        }


        public TIPOSSOLICITUDES ObtenerPorClave(int id)
        {
            try
            {
                return _unidad.RepositorioTiposSolicitudes.GetById(id);
                
            }
            catch
            {
                return null;
            }
        }

       
        public IEnumerable<TIPOSSOLICITUDES> ObtenerTodos()
        {
            try
            {
                IEnumerable<TIPOSSOLICITUDES> tiposSolicitudes= _unidad.RepositorioTiposSolicitudes.GetAll().OrderBy(t=>t.NOMBRE);
                if (tiposSolicitudes == null)
                    return Enumerable.Empty<TIPOSSOLICITUDES>();
                else
                    return tiposSolicitudes;
            }
            catch {
                return Enumerable.Empty<TIPOSSOLICITUDES>();
            }
        }      
    }
}
