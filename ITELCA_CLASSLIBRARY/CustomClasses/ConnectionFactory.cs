﻿using System.Configuration;
using System.Data;
using System.Data.Common;

namespace ITELCA_CLASSLIBRARY.CustomClasses
{
    public class ConnectionFactory : IConnectionFactory
    {
        #region "Propiedades privadas"
        private static DbProviderFactory frameworkProviderFactory;
        private static ConnectionStringSettings settings;
        private static IDbConnection connection;
        private static IDbTransaction transaction;
        static readonly ConnectionFactory Instance = new ConnectionFactory();
        #endregion

        #region "Constructor"
        /// <summary>
        /// Constructor 
        /// </summary>
        static ConnectionFactory()
        {
            settings = ConfigurationManager.ConnectionStrings[1];
            frameworkProviderFactory = DbProviderFactories.GetFactory(settings.ProviderName);
        }
        #endregion

        #region "Singleton"
        public static ConnectionFactory GetInstance
        {
            get { return Instance; }
        }
        #endregion

        #region "Metodos publicos"
        /// <summary>
        /// Metodo para la creacion y ejecucion de un comando
        /// </summary>
        /// <returns></returns>
        public IDbCommand CreateCommand()
        {
            return frameworkProviderFactory.CreateCommand();
        }

        /// <summary>
        /// Metodo que genera y devuelve la conexion a la base de datos
        /// </summary>
        /// <returns></returns>
        public IDbConnection CreateConnection()
        {
            connection = frameworkProviderFactory.CreateConnection();
            connection.ConnectionString = settings.ConnectionString;
            connection.Open();
            return connection;
        }

        /// <summary>
        /// Metodo que genera y devuelve la conexion a la base de datos CEL
        /// </summary>
        /// <returns></returns>
        public IDbConnection CreateConnectionCEL()
        {
            connection = frameworkProviderFactory.CreateConnection();
            connection.ConnectionString = ConfigurationManager.ConnectionStrings[2].ConnectionString;
            connection.Open();
            return connection;
        }

        public IDbTransaction CreateTransaction()
        {
            IDbTransaction st = connection.BeginTransaction();
            transaction = st;
            return transaction;
        }

        /// <summary>
        /// Limpia la conexion y transaccion
        /// </summary>
        public void Dispose()
        {
            if (transaction != null && transaction.Connection != null)
            {
                transaction.Rollback();
            }
            if (connection != null && connection.State == ConnectionState.Open)
            {
                connection.Close();
            }
        }
        #endregion
    }
}
