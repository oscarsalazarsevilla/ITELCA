﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

namespace ITELCA_CLASSLIBRARY.CustomClasses
{
    public class Auditoria
    {
        public string BuscarDiferencias<T>(T originalValuesObj, T newValuesObj, params string[] ignore)
        {
            // check for null arguments
            string cadena = "";
            if (originalValuesObj == null || newValuesObj == null)
            {
                throw new ArgumentNullException(originalValuesObj == null ? "desde " : "a");
            }

            // working variable(s)
            Type type = typeof(T);
            List<string> ignoreList = new List<string>(ignore ?? new string[] { });

            // iterate through each of the properties on the object
            foreach (MemberInfo pi in type.GetProperties(BindingFlags.Public | BindingFlags.Instance))
            {
                if (pi.MemberType == MemberTypes.Property)
                {
                    // check whether we should be ignoring this property
                    if (ignoreList.Contains(pi.Name))
                    {
                        continue;
                    }
                    var prop = (PropertyInfo)pi;
                    if (prop.CanRead && prop.GetGetMethod().GetParameters().Length == 0)
                    {
                        var xValue = prop.GetValue(originalValuesObj, null);
                        var yValue = prop.GetValue(newValuesObj, null);
                        try
                        {
                            if (!yValue.Equals(xValue))
                                cadena += prop.Name+":"+xValue + ",";
                        }
                        catch
                        {
                            continue;

                        }
                    }
                }
            }

            return cadena;
        }
    }
}
