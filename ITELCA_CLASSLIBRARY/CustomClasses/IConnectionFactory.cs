﻿using System.Data;
using System;

namespace ITELCA_CLASSLIBRARY.CustomClasses
{
    public interface IConnectionFactory : IDisposable
    {
        /// <summary>
        /// Metodo para la ejecucion de un comando
        /// </summary>
        /// <returns></returns>
        IDbCommand CreateCommand();

        /// <summary>
        /// Metodo para crear una conexion
        /// </summary>
        /// <returns></returns>
        IDbConnection CreateConnection();

        /// <summary>
        /// Metodo para crear una conexion en CEL
        /// </summary>
        /// <returns></returns>
        IDbConnection CreateConnectionCEL();
    }
}
