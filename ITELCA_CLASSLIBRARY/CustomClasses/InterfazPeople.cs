﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oracle.ManagedDataAccess.Client;
using System.Data;
using ITELCA_CLASSLIBRARY.Models;
using ITELCA_CLASSLIBRARY.Services;

namespace ITELCA_CLASSLIBRARY.CustomClasses
{
    public class InterfazPeople
    {
        public InterfazPeople()
        {
        }
      
        private string GetConnectionString()
        {
            return "Data Source=WEBITCAP;Persist Security Info=True;" +
                   "User ID=ITELCAUSER;Password=pepe123;";
           // con.ConnectionString = "Data Source=(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=myHost)(PORT=myPort)))(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=myService)));User Id=myId;Password=myPass;";   
        }

        /*public List<PRODUCTOS> ObtenerProductos()
        {
            List<PRODUCTOS> lista = new List<PRODUCTOS>();
            string connectionString = GetConnectionString();
            using (OracleConnection connection = new OracleConnection())
            {
                connection.ConnectionString = connectionString;
                connection.Open();
                Console.WriteLine("State: {0}", connection.State);
                Console.WriteLine("ConnectionString: {0}",
                                  connection.ConnectionString);

                OracleCommand command = connection.CreateCommand();
                string sql = " SELECT * fROM INT_ITELCA.IT_PRODUCTOS@PS_ITELCA_PRD";
                command.CommandText = sql;

                OracleDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    PRODUCTOS p = new PRODUCTOS();
                    p.SETID=(string)reader.GetValue(0);
                    p.INV_ITEM_ID = (string)reader.GetValue(1);
                    p.DESCR = (string)reader.GetValue(2);
                    lista.Add(p);
                 
                }
                connection.Dispose();
                connection.Close();
                return lista;
            }
        }*/
        
        public List<INVENTARIO> ObtenerInventario()
        {
            List<INVENTARIO> lista = new List<INVENTARIO>();
            string connectionString = GetConnectionString();
            using (OracleConnection connection = new OracleConnection())
            {
                connection.ConnectionString = connectionString;
                connection.Open();
                Console.WriteLine("State: {0}", connection.State);
                Console.WriteLine("ConnectionString: {0}",
                                  connection.ConnectionString);

                OracleCommand command = connection.CreateCommand();
                string sql = "SELECT * fROM INT_ITELCA.IT_PRODUCTOS_DTL@PS_ITELCA_PRD";
                command.CommandText = sql;

                OracleDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    INVENTARIO inv = new INVENTARIO();
                    inv.SETID = (string)reader.GetValue(0);
                    inv.BUSINESS_UNIT = (string)reader.GetValue(1);
                    inv.INV_ITEM_ID= (string)reader.GetValue(2);
                    inv.QTY_AVAILABLE = (string)reader.GetValue(3);
                    inv.CM_UNIT_COST = (string)reader.GetValue(4);
                    lista.Add(inv);
                }
                connection.Dispose();
                connection.Close();
                return lista;
            }
        }

        public INVENTARIO ObtenerInventarioProducto(string inv_item_id,string sede)
        {
            INVENTARIO inventario = new INVENTARIO();
            string connectionString = GetConnectionString();
            using (OracleConnection connection = new OracleConnection())
            {
                connection.ConnectionString = connectionString;
                connection.Open();
                Console.WriteLine("State: {0}", connection.State);
                Console.WriteLine("ConnectionString: {0}",
                                  connection.ConnectionString);

                OracleCommand command = connection.CreateCommand();
                string sql = "SELECT * fROM INT_ITELCA.IT_PRODUCTOS_DTL@PS_ITELCA_PRD WHERE INV_ITEM_ID='" + inv_item_id + "' AND BUSINESS_UNIT = '" + sede + "'";
                command.CommandText = sql;

                OracleDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    inventario.SETID = (string)reader.GetValue(0);
                    inventario.BUSINESS_UNIT = (string)reader.GetValue(1);
                    inventario.INV_ITEM_ID = (string)reader.GetValue(2);
                    inventario.QTY_AVAILABLE = reader.GetValue(4).ToString();
                    inventario.CM_UNIT_COST = reader.GetValue(5).ToString();
                }
                connection.Dispose();
                connection.Close();
                return inventario;
            }
        }

        public bool ObtenerInventarioDiarioPeople(DateTime fecha, decimal usuario_id)
        {
            try
            {
                ServicioInventario servInventario = new ServicioInventario();
                string connectionString = GetConnectionString();
                using (OracleConnection connection = new OracleConnection())
                {
                    connection.ConnectionString = connectionString;
                    connection.Open();
                    Console.WriteLine("State: {0}", connection.State);
                    Console.WriteLine("ConnectionString: {0}",
                                      connection.ConnectionString);

                    OracleCommand command = connection.CreateCommand();
                    string sql = "";

                    sql = "SELECT   I.SETID, S.SEDE_ID, I.INV_ITEM_ID, I.QTY_AVAILABLE, I.CM_UNIT_COST " +
                          "FROM     INT_ITELCA.IT_PRODUCTOS_DTL@PS_ITELCA_PRD I, " +
                          "         SEDES S " + 
                          "WHERE    I.BUSINESS_UNIT = S.CODIGO";
                    command.CommandText = sql;
                    command.ExecuteReader();
                    OracleDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        INVENTARIOS aux = servInventario.ObtenerTodos().Where(i => i.FECHA.Day == fecha.Day &&
                                                                                i.FECHA.Month == fecha.Month &&
                                                                                i.FECHA.Year == fecha.Year &&
                                                                                i.INV_ITEM_ID == reader.GetValue(2).ToString() &&
                                                                                i.SEDE_ID == (decimal)reader.GetValue(1)).FirstOrDefault();
                        if (aux == null)
                        {
                            INVENTARIOS inventario = new INVENTARIOS();
                            inventario.SETID = (string)reader.GetValue(0);
                            inventario.SEDE_ID = (decimal)reader.GetValue(1);
                            inventario.INV_ITEM_ID = (string)reader.GetValue(2);
                            inventario.CANTIDAD = decimal.Parse(reader.GetValue(3).ToString());
                            inventario.COSTO = (decimal)reader.GetValue(4);
                            inventario.FECHA = fecha;
                            inventario.USUARIO_ID = usuario_id;
                            servInventario.Guardar(inventario);
                        }
                        else
                        {
                            aux.CANTIDAD = decimal.Parse(reader.GetValue(3).ToString());
                            aux.COSTO = (decimal)reader.GetValue(4);
                            servInventario.Modificar(aux);
                        }
                    }
                    connection.Dispose();
                    connection.Close();
                    return true;
                }
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public bool DevolverProductos(decimal detalle_id)
        {
            try
            {
                string connectionString = GetConnectionString();
                int rowsaffected = 0;
                
                using (OracleConnection connection = new OracleConnection())
                {
                    connection.ConnectionString = connectionString;
                    connection.Open();

                    OracleCommand command = connection.CreateCommand();
                    command.CommandType = CommandType.Text;
                    ENTREGAS entregas = new ServicioEntrega().ObtenerTodos().Where(e=>e.DETALLERECIBO_ID == detalle_id).FirstOrDefault();

                    command.CommandText = "INSERT INTO INT_ITELCA.PS_ST_AJUSTE_INT@PS_ITELCA_PRD(BUSINESS_UNIT, INV_ITEM_ID, ADJUST_TYPE, ADJUST_QTY, DISTRIB_TYPE, STATUS, PROCESS_INSTANCE, LAST_OPRID, CREATION_DT,UPDATE_DTTM) " +
                                            "VALUES('" + entregas.DETALLESRECIBOS.RECIBOS.CAJAS.SEDES.CODIGO + "','" + entregas.DETALLESRECIBOS.PRODUCTOS.INV_ITEM_ID + "','I'," + entregas.DETALLESRECIBOS.CANTIDAD + ",'DEV_VENTAS','P',0,'VP1',SYSDATE, NULL)";
                    rowsaffected = command.ExecuteNonQuery();
                }
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public bool ChequearDataBancos(decimal usuario_id)
        {
            try
            {
                string connectionString = GetConnectionString();
                ServicioBanco servBanco = new ServicioBanco();
                ServicioCuenta servCuenta = new ServicioCuenta();
                using (OracleConnection connection = new OracleConnection())
                {
                    connection.ConnectionString = connectionString;
                    connection.Open();
                    Console.WriteLine("State: {0}", connection.State);
                    Console.WriteLine("ConnectionString: {0}",
                                      connection.ConnectionString);

                    OracleCommand command = connection.CreateCommand();
                    string sql = "";

                    //Chequeamos la data de los bancos. Si esta creado se omite, si no existe se crea
                    sql = " SELECT  DISTINCT DESCR, BANK_CD" +
                          " FROM    INT_ITELCA.IT_BANCOS_CTAS@PS_ITELCA_PRD";
                    command.CommandText = sql;
                    command.ExecuteReader();
                    OracleDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        BANCOS banco = servBanco.ObtenerPorClave((string)reader.GetValue(1));

                        if (banco == null)
                        {
                            BANCOS bancoObj = new BANCOS();
                            bancoObj.CODIGO = (string)reader.GetValue(1);
                            bancoObj.CONDICION = "C";
                            bancoObj.ESTADO = 1;
                            bancoObj.NOMBRE = (string)reader.GetValue(0);
                            bancoObj.USUARIO_ID = usuario_id;

                            servBanco.Guardar(bancoObj);
                        }
                    }

                    //Chequeamos la data de las cuentas. Si estan creadas se omite, si no existen se crean
                    sql = " SELECT  B.BANCO_ID, 'CUENTA ' || DESCR, I.BANK_ACCT_KEY, I.BANK_ACCOUNT_NUM, I.ACCOUNT " +
                          " FROM    INT_ITELCA.IT_BANCOS_CTAS@PS_ITELCA_PRD I, BANCOS B " +
                          " WHERE   B.CODIGO = I.BANK_CD";
                    command.CommandText = sql;
                    command.ExecuteReader();
                    reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        CUENTAS cuenta = servCuenta.ObtenerPorClave((string)reader.GetValue(4));

                        if (cuenta == null)
                        {
                            CUENTAS cuentaObj = new CUENTAS();
                            cuentaObj.BANCO_ID = (decimal)reader.GetValue(0);
                            cuentaObj.CONDICION = "C";
                            cuentaObj.CUENTA = (string)reader.GetValue(4);
                            cuentaObj.DESCRIPCION = (string)reader.GetValue(1);
                            cuentaObj.NUMERO = (string)reader.GetValue(2);
                            cuentaObj.CONDICION = "C";
                            cuentaObj.ESTADO = 1;
                            cuentaObj.USUARIO_ID = usuario_id;

                            servCuenta.Guardar(cuentaObj);
                        }
                    }

                    connection.Dispose();
                    connection.Close();
                    return true;
                }
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public bool ChequearDataProductos()
        {
            try
            {
                ServicioProducto servicio = new ServicioProducto();
                string connectionString = GetConnectionString();
                using (OracleConnection connection = new OracleConnection())
                {
                    connection.ConnectionString = connectionString;
                    connection.Open();
                    Console.WriteLine("State: {0}", connection.State);
                    Console.WriteLine("ConnectionString: {0}",
                                      connection.ConnectionString);

                    OracleCommand command = connection.CreateCommand();
                    string sql = " SELECT * fROM INT_ITELCA.IT_PRODUCTOS@PS_ITELCA_PRD";
                    command.CommandText = sql;

                    OracleDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        PRODUCTOS p = servicio.ObtenerPorClave((string)reader.GetValue(0), (string)reader.GetValue(1));

                        if (p == null)
                        {
                            p = new PRODUCTOS();
                            p.SETID = (string)reader.GetValue(0);
                            p.INV_ITEM_ID = (string)reader.GetValue(1);
                            p.DESCR = (string)reader.GetValue(3);
                            servicio.Guardar(p);
                        }

                    }
                    connection.Dispose();
                    connection.Close();

                    return true;
                }
            }
            catch
            {
                return false;
            }
        }

        public bool IngresarVenta(decimal recibo_id)
        {
            try
            {
                INVENTARIO inventario = new INVENTARIO();
                string connectionString = GetConnectionString();
                ServicioCuentasContables servCuentas = new ServicioCuentasContables();
                using (OracleConnection connection = new OracleConnection())
                {
                    connection.ConnectionString = connectionString;
                    connection.Open();
                    Console.WriteLine("State: {0}", connection.State);
                    Console.WriteLine("ConnectionString: {0}",
                                      connection.ConnectionString);

                    RECIBOS recibo = new ServicioReciboCaja().ObtenerPorClave((int)recibo_id);
                    USUARIOS cajero = new ServicioUsuario().ObtenerPorClave(recibo.CAJERO_ID);
                    OracleCommand command = connection.CreateCommand();
                    string sql = "", tipo = "";
                    int contProd = 0, contVenta = 0, contLine = 1;

                    //obtenemos el máximo de las líneas del recibo

                    sql = " SELECT MAX(JOURNAL_LINE) FROM INT_ITELCA.ST_JRNL_LN@PS_ITELCA_PRD " +
                          " WHERE JOURNAL_ID_OR = 'V" + recibo.CAJAS.SEDES.SEDE_ID + DateTime.Now.ToString("ddMMyy") + "'"; ;

                    command.CommandText = sql;
                    OracleDataReader reader = command.ExecuteReader();
                    
                    while (reader.Read())
                    {
                        if (!String.IsNullOrEmpty(reader.GetValue(0).ToString()))
                        {
                            contLine = int.Parse(((decimal)reader.GetValue(0)).ToString()) + 1;
                        }
                    }

                    foreach (DETALLESRECIBOS dr in recibo.DETALLESRECIBOS)
                    { 
                        string concepto="";
                        if (!string.IsNullOrEmpty(dr.INV_ITEM_ID))
                        {
                            concepto = "VENTA PRODUCTO " + dr.PRODUCTOS.DESCR;
                            if (contProd == 0)
                            {
                                sql = "INSERT  INTO  INT_ITELCA.PS_ST_EXPD_HDR@PS_ITELCA_PRD(BUSINESS_UNIT, ID_EXPD,REQUEST_TYPE,LOCATION,STATUS,PROCESS_INSTANCE,LAST_OPRID,CREATION_DT,UPDATE_DTTM )" +
                                     "VALUES('" + recibo.CAJAS.SEDES.CODIGO + "','" + recibo.RECIBO_ID + "','I','" + recibo.CAJAS.SEDES.CODIGO + "','P',0,' ',SYSDATE, NULL)";
                                command.CommandText = sql;
                                command.ExecuteNonQuery();
                            }

                            /*if (dr.PRODUCTOS.DESCR.ToLower().Contains("uniforme"))
                                tipo = "U";
                            else if (dr.PRODUCTOS.DESCR.ToLower().Contains("kit"))
                                tipo = "K";
                            else if (dr.PRODUCTOS.DESCR.ToLower().Contains("util"))
                                tipo = "L";
                            else
                                tipo = "C";*/
                            tipo = "U";

                            sql = "INSERT  INTO  INT_ITELCA.PS_ST_EXPD_LINE@PS_ITELCA_PRD(BUSINESS_UNIT, ID_EXPD,INV_ITEM_ID,QTY_PICKED)" +
                                         "VALUES('" + recibo.CAJAS.SEDES.CODIGO + "','" + recibo.RECIBO_ID + "','" + dr.INV_ITEM_ID + "'," + dr.CANTIDAD + ")";
                            command.CommandText = sql;
                            command.ExecuteNonQuery();
                            contProd++;
                        }
                        else
                        {
                            concepto = dr.CONCEPTOS_ESTUDIANTES.DESCRIPCION;
                            tipo = "S";
                        }

                        sql = " SELECT * FROM INT_ITELCA.ST_JRNL_HEADER@PS_ITELCA_PRD " +
                              " WHERE BIT_STS_ACT_REGS_I = 'C' " +
                              " AND JOURNAL_ID_OR = 'V" + recibo.CAJAS.SEDES.SEDE_ID + DateTime.Now.ToString("ddMMyy") + "'";

                        int contInt = 0;
                        command.CommandText = sql;
                        reader = command.ExecuteReader();
                        while (reader.Read())
                        {
                            contInt++;
                        }

                        if (contInt == 0)
                        {
                            sql = "INSERT  INTO INT_ITELCA.ST_JRNL_HEADER@PS_ITELCA_PRD(BUSINESS_UNIT, JOURNAL_ID, JOURNAL_DATE, UNPOST_SEQ, JOURNAL_ID_OR, DESCR254, LEDGER_GROUP, SOURCE, IU_TRAN_CD, OPRID_ENTERED_BY, PROCESS_INSTANCE, LAST_OPRID, CREATION_DT, UPDATE_DTTM,BIT_STS_ACT_REGS_I)" +
                                    "VALUES ('ITELC', 'NEXT', SYSDATE, 0, 'V" +recibo.CAJAS.SEDES.SEDE_ID + DateTime.Now.ToString("ddMMyy") + "', 'VENTAS " + recibo.CAJAS.SEDES.NOMBRE + " "+DateTime.Now.ToString("dd/MM/yyyy")+"', 'REAL', 'ONL', 'GENERAL', '" + cajero.NOMBREUSUARIO + "', 0, '', SYSDATE, NULL, 'C')";
                            command.CommandText = sql;
                            command.ExecuteNonQuery();
                        }

                        //sacamos la cuenta de ingreso
                        var cuenta = servCuentas.ObtenerPorTipoYSede(recibo.CAJAS.SEDE_ID, "V");
                        string cuentaIngreso = "";

                        if (cuenta == null)
                        {
                            return false;
                        }
                        else
                        {
                            cuentaIngreso = cuenta.ACCOUNT;
                        }

                        //sacamos la cuenta tipo 4
                        var cuenta2 = servCuentas.ObtenerPorTipoYSede(recibo.CAJAS.SEDE_ID, tipo);
                        string cuentaEgreso = "";

                        if (cuenta2 == null)
                        {
                            return false;
                        }
                        else
                        {
                            cuentaEgreso = cuenta2.ACCOUNT;
                        }

                        string conceptoLinea = (concepto.Length >= 30) ? concepto.Substring(0, 30) : concepto.Substring(0, concepto.Length);
                        sql = "INSERT  INTO INT_ITELCA.ST_JRNL_LN@PS_ITELCA_PRD(BUSINESS_UNIT, JOURNAL_ID, JOURNAL_DATE, UNPOST_SEQ, JOURNAL_ID_OR, JOURNAL_LINE, LEDGER, ACCOUNT, DEPTID, CURRENCY_CD, MONETARY_AMOUNT, JRNL_LN_REF, LINE_DESCR, JRNL_LINE_STATUS,RT_TYPE, OPRID_ENTERED_BY,PROCESS_INSTANCE,LAST_OPRID,CREATION_DT,UPDATE_DTTM,OPEN_ITEM_KEY,BIT_STS_ACT_REGS_I) " +
                            "VALUES ('ITELC', 'NEXT', SYSDATE, 0, 'V" + recibo.CAJAS.SEDES.SEDE_ID + DateTime.Now.ToString("ddMMyy") + "', " + contLine.ToString() + ", '', '" + cuentaIngreso + "', '', 'VEF', " + dr.MONTO + ", '" + dr.RECIBOS.NRORECIBO + "', '" + conceptoLinea + "', 0, '', '" + cajero.NOMBREUSUARIO + "', 0, '', SYSDATE, NULL, '', 'C')";
                        command.CommandText = sql;
                        command.ExecuteNonQuery();
                        contLine++;

                        sql = "INSERT  INTO INT_ITELCA.ST_JRNL_LN@PS_ITELCA_PRD(BUSINESS_UNIT, JOURNAL_ID, JOURNAL_DATE, UNPOST_SEQ, JOURNAL_ID_OR, JOURNAL_LINE, LEDGER, ACCOUNT, DEPTID, CURRENCY_CD, MONETARY_AMOUNT, JRNL_LN_REF, LINE_DESCR, JRNL_LINE_STATUS,RT_TYPE, OPRID_ENTERED_BY,PROCESS_INSTANCE,LAST_OPRID,CREATION_DT,UPDATE_DTTM,OPEN_ITEM_KEY,BIT_STS_ACT_REGS_I) " +
                                "VALUES ('ITELC', 'NEXT', SYSDATE, 0, 'V" + recibo.CAJAS.SEDES.SEDE_ID + DateTime.Now.ToString("ddMMyy") + "', " + contLine.ToString() + ", '', '" + cuentaEgreso + "', '" + "COORPRINCS" + "', 'VEF', -" + dr.MONTO + ", '" + dr.RECIBOS.NRORECIBO + "', '" + conceptoLinea + "', 0, '', '" + cajero.NOMBREUSUARIO + "', 0, '', SYSDATE, NULL, '', 'C')";
                        command.CommandText = sql;
                        command.ExecuteNonQuery();

                        contLine++;
                        contVenta++;
                    }

                    connection.Dispose();
                    connection.Close();
                    return true;
                }
            }
            catch{ return false; }
        }

        public bool AnularVenta(decimal recibo_id)
        {
            try
            {
                ServicioProducto servicio = new ServicioProducto();
                ServicioReciboCaja servicioRecibo = new ServicioReciboCaja();
                List<DETALLESRECIBOS> lista = servicioRecibo.ObtenerDetallePorRecibo((int)recibo_id);
                string connectionString = GetConnectionString();
                using (OracleConnection connection = new OracleConnection())
                {
                    connection.ConnectionString = connectionString;
                    connection.Open();
                    Console.WriteLine("State: {0}", connection.State);
                    Console.WriteLine("ConnectionString: {0}",
                                      connection.ConnectionString);

                    OracleCommand command = connection.CreateCommand();
                    string sql = "";
                    foreach (DETALLESRECIBOS ope in lista)
                    {
                        sql = "DELETE FROM INT_ITELCA.ST_JRNL_LN@PS_ITELCA_PRD WHERE JOURNAL_ID_OR= 'V" + ope.RECIBOS.CAJAS.SEDES.SEDE_ID + DateTime.Now.ToString("ddMMyy") + "' AND JRNL_LN_REF=" + ope.RECIBO_ID;
                        command.CommandText = sql;
                        command.ExecuteNonQuery();

                        if (ope.INV_ITEM_ID != null)
                        {

                            sql = "DELETE FROM   INT_ITELCA.PS_ST_EXPD_HDR@PS_ITELCA_PRD WHERE  ID_EXPD IN " +
                                         " (SELECT ID_EXP " +
                                         " FROM   INT_ITELCA.PS_ST_EXPD_LINE@PS_ITELCA_PRD " +
                                         " WHERE  BUSSINESS_UNIT='" + ope.RECIBOS.CAJAS.SEDES.CODIGO + "' AND " +
                                         "        INV_ITEM_ID='" + ope.INV_ITEM_ID + "' AND" +
                                          "       QTY_PICKED=" + ope.CANTIDAD + ")";
                            command.CommandText = sql;

                            sql = "DELETE FROM   INT_ITELCA.PS_ST_EXPD_LINE@PS_ITELCA_PRD " +
                                        " WHERE  BUSSINESS_UNIT='" + ope.RECIBOS.CAJAS.SEDES.CODIGO + "' AND " +
                                        "        INV_ITEM_ID='" + ope.INV_ITEM_ID + "' AND" +
                                        "         QTY_PICKED=" + ope.CANTIDAD;
                            command.ExecuteNonQuery();


                        }
                    }
                    connection.Dispose();
                    connection.Close();

                    return true;
                }
            }
            catch
            {
                return false;
            }
        }

        public bool IngresarEntregaProductos(List<ENTREGAS> entregas)
        {
            try
            {
                /*string connectionString = GetConnectionString();
                int rowsaffected = 0, cont = 0;
                
                using (OracleConnection connection = new OracleConnection())
                {
                    connection.ConnectionString = connectionString;
                    connection.Open();

                    OracleCommand command = connection.CreateCommand();
                    command.CommandType = CommandType.Text;
                    SEDES sede = new ServicioSede().ObtenerPorClave(int.Parse(entregas.First().SEDE_ID.ToString()));
                    DETALLESRECIBOS detalle = new ServicioDetalleRecibo().ObtenerPorClave(int.Parse(entregas.First().DETALLERECIBO_ID.ToString()));

                    foreach (var entrega in entregas)
                    {
                        if (cont == 0)
                        {
                            command.CommandText =   "INSERT  INTO  INT_ITELCA.PS_ST_EXPD_HDR@PS_ITELCA(BUSINESS_UNIT, ID_EXPD,REQUEST_TYPE,LOCATION,STATUS,PROCESS_INSTANCE,LAST_OPRID,CREATION_DT,UPDATE_DTTM ) " +
                                                    "VALUES('" + sede.CODIGO + "','" + detalle.RECIBOS.RECIBO_ID + "','I','" + sede.CODIGO + "','P',0,' ',SYSDATE, NULL)";
                            rowsaffected = command.ExecuteNonQuery();
                        }

                        command.CommandText = "INSERT  INTO  INT_ITELCA.PS_ST_EXPD_LINE@PS_ITELCA(BUSINESS_UNIT, ID_EXPD,INV_ITEM_ID,QTY_PICKED ) " +
                                                    "VALUES('" + sede.CODIGO + "','" + detalle.RECIBOS.RECIBO_ID + "','" + entrega.DETALLERECIBO_ID + "'," + entrega.CANTIDAD + ")";
                        rowsaffected = command.ExecuteNonQuery();
                        cont++;
                    }
                }*/
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }
    }

}
