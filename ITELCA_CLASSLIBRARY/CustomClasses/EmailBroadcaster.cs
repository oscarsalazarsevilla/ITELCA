﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;
using System.Net;

namespace ITELCA_CLASSLIBRARY.Models.Classes
{
    public class EmailBroadcaster
    {
        public static void SendEmail(string subject, string body, string email)
        {
            var message = new MailMessage("soporte@ramar.com.ve", email)
            {
                Subject = subject,
                Body = body,
                IsBodyHtml = false
            };

            NetworkCredential SMTPUserInfo = new NetworkCredential("soporte@ramar.com.ve", "12345");
            var client = new SmtpClient("mail.ramar.com.ve");
            client.UseDefaultCredentials = false;
            client.Credentials = SMTPUserInfo;
            client.Send(message);
        }

        public static void SendEmail(string subject, string body, string email, bool isHtml)
        {
            var message = new MailMessage("soporte@ramar.com.ve", email)
            {
                Subject = subject,
                Body = body,
                IsBodyHtml = isHtml
            };

            NetworkCredential SMTPUserInfo = new NetworkCredential("soporte@ramar.com.ve", "12345");
            var client = new SmtpClient("mail.ramar.com.ve");
            client.UseDefaultCredentials = false;
            client.Credentials = SMTPUserInfo;
            client.Send(message);
        }
    }
}


